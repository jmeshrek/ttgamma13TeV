CXX = g++

SRC_DIR = src
BIN_DIR = bin
OBJ_DIR = obj
UTIL_DIR = util
INC_DIR = include
#KLFITTER = /afs/cern.ch/work/y/yili/private/MySVN/Code/KLFitter

ROOTINCLUDE  = $(shell root-config --incdir)
ROOTCXXFLAGS = $(shell root-config --cflags) 
ROOTLIBS     = $(shell root-config --libs) -lRooFitCore -lRooFit -lRooStats  -lThread -lMinuit -lFoam -lTreePlayer -lTMVA -lMinuit -lXMLIO -lMLP -lHistPainter
NEWROOTVERSION  = $(shell root-config --version | grep -c "5.24" )
ifeq ($(NEWROOTVERSION),1)
	ROOTLIBS +=  -lRIO
endif

BATCFLAGS = -I$(BATINSTALLDIR)/include
#BATLIBS   = -L$(BATINSTALLDIR)/lib -lBAT

INCLUDE  = -I$(INC_DIR) -I$(ROOTINCLUDE) 
#INCLUDE2  = -I$(INC_DIR) -I$(ROOTINCLUDE) -I$(KLFITTER)/KLFitter $(BATCFLAGS) #-std=c++11
LDLIBS   = $(ROOTLIBS) 
#LDLIBS2   = $(ROOTLIBS) -L$(KLFITTER) -lKLFitter $(BATLIBS)
#CXXFLAGS = -O2 -Wall -Wno-unused-variable -fPIC -fopenmp $(ROOTCXXFLAGS) 
#CXXFLAGS = $(ROOTCXXFLAGS) -std=c++11
CXXFLAGS = $(ROOTCXXFLAGS) -std=c++0x
#CXXFLAGS = $(ROOTCXXFLAGS) -std=c++1y

OBJECTS = $(OBJ_DIR)/WorkspaceMaker.src.o $(OBJ_DIR)/WorkspaceAnalyzor.src.o $(OBJ_DIR)/SampleAnalyzor.src.o $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/ConfigReader.src.o $(OBJ_DIR)/PlotComparor.src.o $(OBJ_DIR)/PL.src.o $(OBJ_DIR)/PL2.src.o $(OBJ_DIR)/RecoLevel.src.o $(OBJ_DIR)/SumWeightTree.src.o $(OBJ_DIR)/ParticleLevel.src.o $(OBJ_DIR)/PartonLevel.src.o

$(OBJ_DIR)/%.src.o: $(SRC_DIR)/%.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -c $^ -o $@  

TO_TEST = $(UTIL_DIR)/test4.cxx

ALL = $(BIN_DIR)/analysisworkspace $(BIN_DIR)/analysissample $(BIN_DIR)/analysislhe $(BIN_DIR)/convertmarkus $(BIN_DIR)/makeworkspace $(BIN_DIR)/drawstack $(BIN_DIR)/drawstack2 $(BIN_DIR)/printcutflow $(BIN_DIR)/drawefakesf $(BIN_DIR)/compareshapes $(BIN_DIR)/comparecf $(BIN_DIR)/egammafakestudy_sf $(BIN_DIR)/efakestudy $(BIN_DIR)/efakestudy2 $(BIN_DIR)/efaketableplot $(BIN_DIR)/egammacalibration $(BIN_DIR)/subtracthistogram $(BIN_DIR)/dividehistogram $(BIN_DIR)/unfoldinginputs $(BIN_DIR)/compareqcd $(BIN_DIR)/summarizeegamma $(BIN_DIR)/egammafakestudy_mcclosure $(BIN_DIR)/egammafakestudy_correlation $(BIN_DIR)/pseudodata $(BIN_DIR)/binningopt $(BIN_DIR)/eftfit $(BIN_DIR)/calcusys $(BIN_DIR)/calcusigsys $(BIN_DIR)/calcusigsys2 $(BIN_DIR)/calcukfactor $(BIN_DIR)/comparemg5 $(BIN_DIR)/markus $(BIN_DIR)/h7bug $(BIN_DIR)/eldshapesys $(BIN_DIR)/efakeclosure $(BIN_DIR)/drawbootstrapcorr $(BIN_DIR)/drawefficiency $(BIN_DIR)/calcuefficiency $(BIN_DIR)/comparecutflow1314 $(BIN_DIR)/comparecutflowlargeeta $(BIN_DIR)/makeinputs $(BIN_DIR)/comparefiduprecision $(BIN_DIR)/compareshapes1314 $(BIN_DIR)/compareshapes13142 $(BIN_DIR)/compareshapes1314signal $(BIN_DIR)/checkhfake $(BIN_DIR)/drawupgradesys $(BIN_DIR)/checkonline $(BIN_DIR)/derivehfakecorr $(BIN_DIR)/ttgor $(BIN_DIR)/rankingsys $(BIN_DIR)/compareeft14tev $(BIN_DIR)/compareeft14tev2

all: $(ALL)

clean:
	$(shell rm $(OBJ_DIR)/*)
	$(shell rm $(BIN_DIR)/*)
	@echo -e "\n\n===> cleaning directories"

$(BIN_DIR)/unfoldinginputs: $(OBJ_DIR)/RecoLevel.src.o $(OBJ_DIR)/ParticleLevel.src.o $(OBJ_DIR)/PartonLevel.src.o $(UTIL_DIR)/UnfoldingInputs.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/ttgor: $(OBJ_DIR)/RecoLevel2.src.o $(OBJ_DIR)/ParticleLevel2.src.o $(OBJ_DIR)/PartonLevel2.src.o $(UTIL_DIR)/TTGOR.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/binningopt: $(UTIL_DIR)/BinningOpt.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/egammafakestudy_correlation: $(UTIL_DIR)/EGammaFakeStudy_Correlation.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/pseudodata: $(UTIL_DIR)/PseudoData.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/eftfit: $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/PlotComparor.src.o $(UTIL_DIR)/EFTFit.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/calcusys: $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/PlotComparor.src.o $(UTIL_DIR)/CalcuSys.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/calcukfactor: $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/PlotComparor.src.o $(UTIL_DIR)/CalcuKfactor.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/comparemg5: $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/PlotComparor.src.o $(UTIL_DIR)/CompareMG5.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/markus: $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/PlotComparor.src.o $(UTIL_DIR)/Markus.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/h7bug: $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/PlotComparor.src.o $(UTIL_DIR)/H7Bug.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/calcusigsys: $(OBJ_DIR)/SumWeightTree.src.o $(UTIL_DIR)/CalcuSigSys.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/calcusigsys2: $(OBJ_DIR)/SumWeightTree.src.o $(UTIL_DIR)/CalcuSigSysII.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/egammafakestudy_mcclosure: $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/PlotComparor.src.o $(UTIL_DIR)/EGammaFakeStudy_MCClosure.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/compareqcd: $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/PlotComparor.src.o $(UTIL_DIR)/CompareQCD3.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/summarizeegamma: $(UTIL_DIR)/SummarizeEGamma.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/compareshapes: $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/ConfigReader.src.o $(OBJ_DIR)/PlotComparor.src.o $(UTIL_DIR)/CompareShapes.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/compareshapes1314: $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/ConfigReader.src.o $(OBJ_DIR)/PlotComparor.src.o $(UTIL_DIR)/CompareShapes1314.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/compareeft14tev: $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/ConfigReader.src.o $(OBJ_DIR)/PlotComparor.src.o $(UTIL_DIR)/CompareEFT14TeV.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/compareeft14tev2: $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/ConfigReader.src.o $(OBJ_DIR)/PlotComparor.src.o $(UTIL_DIR)/CompareEFT14TeV2.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/compareshapes13142: $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/ConfigReader.src.o $(OBJ_DIR)/PlotComparor.src.o $(UTIL_DIR)/CompareShapes13142.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/compareshapes1314signal: $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/ConfigReader.src.o $(OBJ_DIR)/PlotComparor.src.o $(UTIL_DIR)/CompareShapes1314Signal.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/comparecf: $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/PlotComparor.src.o $(UTIL_DIR)/CompareCF.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/drawstack: $(OBJ_DIR)/PlotComparor.src.o $(OBJ_DIR)/Logger.src.o  $(OBJ_DIR)/ConfigReader.src.o $(UTIL_DIR)/DrawStack.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/drawstack2: $(OBJ_DIR)/PlotComparor.src.o $(OBJ_DIR)/Logger.src.o  $(OBJ_DIR)/ConfigReader.src.o $(UTIL_DIR)/DrawStackII.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/printcutflow: $(OBJ_DIR)/ConfigReader.src.o $(OBJ_DIR)/Logger.src.o $(UTIL_DIR)/PrintCutflow.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/drawefakesf: $(OBJ_DIR)/PlotComparor.src.o $(OBJ_DIR)/Logger.src.o $(UTIL_DIR)/DrawEFakeSF.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/makeworkspace: $(OBJ_DIR)/WorkspaceMaker.src.o $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/ConfigReader.src.o $(UTIL_DIR)/MakeWorkspace.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/analysisworkspace: $(OBJ_DIR)/WorkspaceAnalyzor.src.o $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/ConfigReader.src.o $(UTIL_DIR)/AnalysisWorkspace.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/analysislhe: $(UTIL_DIR)/AnalysisLHE.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS) 
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/convertmarkus: $(UTIL_DIR)/ConvertMarkus.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS) 
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/analysissample: $(OBJ_DIR)/SampleAnalyzor.src.o $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/ConfigReader.src.o $(UTIL_DIR)/AnalysisSample.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS) 
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/egammafakestudy_sf: $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/ConfigReader.src.o $(UTIL_DIR)/EGammaFakeStudy_SF.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/efakestudy: $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/ConfigReader.src.o $(UTIL_DIR)/EFakeStudy.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/efakestudy2: $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/ConfigReader.src.o $(UTIL_DIR)/EFakeStudyII.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/efaketableplot: $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/PlotComparor.src.o $(UTIL_DIR)/EFakeTablePlot.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/egammacalibration: $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/ConfigReader.src.o $(UTIL_DIR)/EGammaCalibration.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/subtracthistogram: $(UTIL_DIR)/SubtractHistogram.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/dividehistogram: $(UTIL_DIR)/DivideHistogram.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/eldshapesys: $(UTIL_DIR)/ELDShapeSys.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS) 
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/efakeclosure: $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/PlotComparor.src.o $(UTIL_DIR)/EFakeClosure.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS) 
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/drawbootstrapcorr: $(UTIL_DIR)/DrawBootStrapCorr.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS) 
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/drawefficiency: $(UTIL_DIR)/DrawEfficiency.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS) 
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/calcuefficiency: $(OBJ_DIR)/SumWeightTree.src.o $(UTIL_DIR)/CalcuEfficiency.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS) 
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/comparecutflow1314: $(UTIL_DIR)/CompareCutflow1314.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS) 
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/comparecutflowlargeeta: $(UTIL_DIR)/CompareCutflowLargeEta.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS) 
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/makeinputs: $(UTIL_DIR)/MakeInputs.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS) 
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/comparefiduprecision: $(UTIL_DIR)/CompareFiduPrecision.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS) 
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/checkonline: $(UTIL_DIR)/CheckOnline.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS) 
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/drawupgradesys:  $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/ConfigReader.src.o $(OBJ_DIR)/PlotComparor.src.o  $(UTIL_DIR)/DrawUpgradeSys.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS) 
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/checkhfake: $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/PlotComparor.src.o $(UTIL_DIR)/CheckHFake.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS) 
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/rankingsys: $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/PlotComparor.src.o $(UTIL_DIR)/RankingSys.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS) 
	@echo "===> your compilation of "$@" succeeded!"

$(BIN_DIR)/derivehfakecorr: $(OBJ_DIR)/Logger.src.o $(OBJ_DIR)/PlotComparor.src.o $(UTIL_DIR)/DeriveHFakeCorr.cxx
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -g $^ $(LDLIBS) 
	@echo "===> your compilation of "$@" succeeded!"

#$(BIN_DIR)/test: $(OBJECTS) $(TO_TEST)
#	$(CXX) $(INCLUDE) -o $@ -g $^ $(LDLIBS)
#	@echo "===> your compilation of "$@" succeeded!"

