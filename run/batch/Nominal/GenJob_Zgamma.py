#!/bin/env python

import os,sys,string,argparse,commands
import shlex
from subprocess import Popen, PIPE

List=[
"CutZGammaCR",
]     

def getJobDef(name):    

    text = """
#!/bin/bash

#BSUB -J %s
#BSUB -o stdout_%s.out
#BSUB -e stderr_%s.out
#BSUB -q 8nh
#BSUB -u $USER@cern.ch

stagein()
{
  echo "Start";
}

runcode()
{
  echo "Running the code";
  cd /afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/run;
  set -x
  selection=%s
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Data --Selection $selection --DoHist --SaveTag Test01 --Variation Nominal
  
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process Signal --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Test01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process TTBar --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Test01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process ZGammajetsElElNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Test01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process ZGammajetsMuMuNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Test01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process ZGammajetsTauTauNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Test01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process ZjetsElEl --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Test01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process ZjetsMuMu --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Test01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process ZjetsTauTau --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Test01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process STWT --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Test01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process Diboson --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Test01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process STWT --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Test01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process Diboson --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Test01 --Variation Nominal

  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Data --Selection $selection --DoHist --SaveTag Test01 --Variation Nominal
  
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process Signal --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Test01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process TTBar --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Test01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process ZGammajetsElElNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Test01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process ZGammajetsMuMuNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Test01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process ZGammajetsTauTauNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Test01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process ZjetsElEl --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Test01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process ZjetsMuMu --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Test01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process ZjetsTauTau --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Test01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process STWT --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Test01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process Diboson --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Test01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process STWT --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Test01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process Diboson --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Test01 --Variation Nominal
}

stageout()
{
  echo "Finished";
}

stagein
runcode
stageout

exit
""" % (name, name, name, name) 

    return text

def submitJob(name):

    bsubFile = open( name + ".bsub", "w")          
    text = getJobDef(name)            
    bsubFile.write(text)
    bsubFile.close()            
    os.system("chmod 775 -R *")            

for term in List:
    submitJob(term)
    command="bsub -q 8nh "+ term + ".bsub"
    print command
    os.system(command)

