#!/bin/env python

import os,sys,string,argparse,commands
import shlex
from subprocess import Popen, PIPE

List=[
"CutSR",
#"CutSRPh50",
#"CutSRPh65",
#"CutEGammaVR",
#"CutWGammaVR",
#"CutWGammaVR2",
#"CutWGammaVR3",
#"CutSRNoIDNoIso",
#"CutSRNoIso",
#"CutHFake",
#"CutSRNph",
#"CutSRNoID",
#"CutSRCvt",
#"CutSRNoIsoCvt",
#"CutHFakeCvt",
#"CutSRNphCvt",
#"CutSRNoIDCvt",
#"CutSRUnCvt",
#"CutSRNoIsoUnCvt",
#"CutHFakeUnCvt",
#"CutSRNphUnCvt",
#"CutSRNoIDUnCvt",
]     

def getJobDef(name):    

    text = """
#!/bin/bash

#BSUB -J %s
#BSUB -o stdout_%s.out
#BSUB -e stderr_%s.out
#BSUB -q 8nh
#BSUB -u $USER@cern.ch

stagein()
{
  echo "Start";
}

runcode()
{
  echo "Running the code";
  cd /afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/run;
  set -x

  selection=%s
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ejets --Type Data --Selection $selection --DoHist --SaveTag Final06 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ejets --Type QCD --QCDPara eta:mtw --Selection $selection --DoHist --UseWeight --SaveTag Final06 --Variation Nominal --ZGammaReweight

  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process Signal --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process Signal --Selection $selection --PhMatch Ancestor1 --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process Signal --Selection $selection --PhMatch Ancestor2 --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process Signal --Selection $selection --PhMatch Ancestor3 --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process TTBar --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process WGammajetsElNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process WGammajetsMuNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process WGammajetsTauNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process WjetsEl --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process WjetsMu --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process WjetsTau --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process ZGammajetsElElNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process ZGammajetsMuMuNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process ZGammajetsTauTauNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process ZjetsElEl --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process ZjetsMuMu --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process ZjetsTauTau --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process STOthers --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process STWT --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process Diboson --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process TTBar --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process WjetsEl --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process WjetsMu --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process WjetsTau --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process ZjetsElEl --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process ZjetsMuMu --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process ZjetsTauTau --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process STOthers --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process STWT --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process Diboson --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process TTBar --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process WjetsEl --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process WjetsMu --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process WjetsTau --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process ZjetsElEl --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process ZjetsMuMu --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process ZjetsTauTau --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process STOthers --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process STWT --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  #./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion ejets --Type Reco --Process Diboson --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
}

stageout()
{
  echo "Finished";
}

stagein
runcode
stageout

exit
""" % (name, name, name, name) 

    return text

def submitJob(name):

    bsubFile = open( name + "_ejets.bsub", "w")          
    text = getJobDef(name)            
    bsubFile.write(text)
    bsubFile.close()            
    os.system("chmod 775 -R *")            

for term in List:
    submitJob(term)
    command="bsub -q 8nh "+ term + "_ejets.bsub"
    print command
    os.system(command)

