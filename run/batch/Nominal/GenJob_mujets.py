#!/bin/env python

import os,sys,string,argparse,commands
import shlex
from subprocess import Popen, PIPE

List=[
"CutSR2b",
#"CutSRPh50",
#"CutSRPh65",
#"CutEGammaVR",
#"CutWGammaVR",
#"CutWGammaVR2",
#"CutWGammaVR3",
#"CutSRNoIDNoIso",
#"CutSRNoIso",
#"CutHFake",
#"CutSRNph",
#"CutSRNoID",
#"CutSRCvt",
#"CutSRNoIsoCvt",
#"CutHFakeCvt",
#"CutSRNphCvt",
#"CutSRNoIDCvt",
#"CutSRUnCvt",
#"CutSRNoIsoUnCvt",
#"CutHFakeUnCvt",
#"CutSRNphUnCvt",
#"CutSRNoIDUnCvt",
]     

def getJobDef(name):    

    text = """
#!/bin/bash

#BSUB -J %s
#BSUB -o stdout_%s.out
#BSUB -e stderr_%s.out
#BSUB -q 8nh
#BSUB -u $USER@cern.ch

stagein()
{
  echo "Start";
}

runcode()
{
  echo "Running the code";
  cd /afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/run;
  set -x

  selection=%s
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Data --Selection $selection --DoHist --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type QCD --UsePS --QCDPara pt:mtw --Selection $selection --DoHist --UseWeight --SaveTag Kerim01 --Variation Nominal
  
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process Signal --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process Signal --Selection $selection --PhMatch Ancestor1 --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process Signal --Selection $selection --PhMatch Ancestor2 --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process Signal --Selection $selection --PhMatch Ancestor3 --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process TTBar --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process WGammajetsElNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process WGammajetsMuNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process WGammajetsTauNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process WjetsEl --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process WjetsMu --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process WjetsTau --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process ZGammajetsElElNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process ZGammajetsMuMuNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process ZGammajetsTauTauNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process ZjetsElEl --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process ZjetsMuMu --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process ZjetsTauTau --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process STOthers --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process STWT --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process Diboson --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process TTBar --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process WjetsEl --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process WjetsMu --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process WjetsTau --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process ZjetsElEl --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process ZjetsMuMu --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process ZjetsTauTau --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process STOthers --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process STWT --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process Diboson --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process TTBar --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process WjetsEl --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process WjetsMu --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process WjetsTau --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process ZjetsElEl --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process ZjetsMuMu --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process ZjetsTauTau --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process STOthers --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process STWT --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type Reco --Process Diboson --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Kerim01 --Variation Nominal
}

stageout()
{
  echo "Finished";
}

stagein
runcode
stageout

exit
""" % (name, name, name, name) 

    return text

def submitJob(name):

    bsubFile = open( name + "_mujets.bsub", "w")          
    text = getJobDef(name)            
    bsubFile.write(text)
    bsubFile.close()            
    os.system("chmod 775 -R *")            

for term in List:
    submitJob(term)
    command="bsub -q 8nh "+ term + "_mujets.bsub"
    print command
    os.system(command)

