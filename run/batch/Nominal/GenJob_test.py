#!/bin/env python

import os,sys,string,argparse,commands
import shlex
from subprocess import Popen, PIPE

List=[
"CutSR",
]     

def getJobDef(name):    

    text = """
#!/bin/bash

#BSUB -J %s
#BSUB -o stdout_%s.out
#BSUB -e stderr_%s.out
#BSUB -q 8nh
#BSUB -u $USER@cern.ch

stagein()
{
  echo "Start";
}

runcode()
{
  echo "Running the code";
  cd /tmp/yili;
  cp /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/bin/analysissample ./;
  set -x
  selection=%s
  mkdir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion emu --Type Data --Selection $selection --DoHist --SaveTag Final02 --Variation Nominal
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process Diboson --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation Nominal
  
}

stageout()
{
  echo "Finished";
}

stagein
runcode
stageout

exit
""" % (name, name, name, name) 

    return text

def submitJob(name):

    bsubFile = open( name + "_ll.bsub", "w")          
    text = getJobDef(name)            
    bsubFile.write(text)
    bsubFile.close()            
    os.system("chmod 775 -R *")            

for term in List:
    submitJob(term)
    command="bsub -q 8nh "+ term + "_ll.bsub"
    print command
    os.system(command)

