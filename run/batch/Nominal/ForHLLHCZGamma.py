#!/bin/env python

import os,sys,string,argparse,commands
import shlex
from subprocess import Popen, PIPE

List=[
["ZGammajetsElElNLO", "ejets"],
["ZGammajetsTauTauNLO", "ejets"],
["ZGammajetsMuMuNLO", "mujets"],
["ZGammajetsTauTauNLO", "mujets"],
["ZGammajetsElElNLO", "ee"],
["ZGammajetsTauTauNLO", "ee"],
["ZGammajetsTauTauNLO", "emu"],
["ZGammajetsMuMuNLO", "mumu"],
["ZGammajetsTauTauNLO", "mumu"],
]     

def getJobDef(sample, channel):    

    text = """
#!/bin/bash

stagein()
{
  echo "Start";
}

runcode()
{
  echo "Running the code";
  cd /afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/run;
  set -x

  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --Process %s --Type Reco --SubRegion %s --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final08 --Variation Nominal --ZGammaReweight
}

stageout()
{
  echo "Finished";
}

stagein
runcode
stageout

exit
""" % (sample, channel) 

    return text

def submitJob(sample, channel):

    bsubFile = open( sample +"." + channel + ".bsub", "w")          
    text = getJobDef(sample, channel)            
    bsubFile.write(text)
    bsubFile.close()            
    os.system("chmod 775 -R *")            

for term in List:
    submitJob(term[0], term[1])
    command="bsub -q 8nh "+ term[0]+"." + term[1] + ".bsub"
    print command
    os.system(command)

