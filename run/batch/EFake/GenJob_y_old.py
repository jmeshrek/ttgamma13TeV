#!/bin/env python

import os,sys,string,argparse,commands
import shlex
from subprocess import Popen, PIPE

List=[
"CutZeg",
"CutZegReverse",
"CutZegCvt",
"CutZegCvtReverse",
"CutZegUnCvt",
"CutZegUnCvtReverse"
]     

def getJobDef(name):    

    text = """
#!/bin/bash

#BSUB -J %s
#BSUB -o stdout_%s.out
#BSUB -e stderr_%s.out
#BSUB -q 8nh
#BSUB -u $USER@cern.ch

stagein()
{
  echo "Start";
}

runcode()
{
  echo "Running the code";
  cd /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run;
  set -x
  selection=%s
  mkdir results/Nominal
  ../bin/analysissample --EGammaReweight --SaveDir results/Nominal/ --Region EF1 --SubRegion zeg --Type Reco --Process ZjetsElEl --Selection $selection --PhMatch EFakeTypeA --DoHist --UseWeight --LumiWeight --SaveTag Final03 --Variation Nominal --EGammaHist
  ../bin/analysissample --EGammaReweight --SaveDir results/Nominal/ --Region EF1 --SubRegion zeg --Type Reco --Process ZjetsElEl --Selection $selection --PhMatch EFakeTypeB --DoHist --UseWeight --LumiWeight --SaveTag Final03 --Variation Nominal --EGammaHist
  ../bin/analysissample --EGammaReweight --SaveDir results/Nominal/ --Region EF1 --SubRegion zeg --Type Reco --Process ZjetsElEl --Selection $selection --PhMatch EFakeTypeC --DoHist --UseWeight --LumiWeight --SaveTag Final03 --Variation Nominal --EGammaHist
  ../bin/analysissample --EGammaReweight --SaveDir results/Nominal/ --Region EF1 --SubRegion zeg --Type Reco --Process ZjetsElEl --Selection $selection --PhMatch EFakeTypeD --DoHist --UseWeight --LumiWeight --SaveTag Final03 --Variation Nominal --EGammaHist
}

stageout()
{
  echo "Finished";
}

stagein
runcode
stageout

exit
""" % (name, name, name, name) 

    return text

def submitJob(name):

    bsubFile = open( name + ".bsub", "w")          
    text = getJobDef(name)            
    bsubFile.write(text)
    bsubFile.close()            
    os.system("chmod 775 -R *")            

for term in List:
    submitJob(term)
    command="bsub -q 8nh "+ term + ".bsub"
    print command
    os.system(command)

