#!/bin/env python

import os,sys,string,argparse,commands
import shlex
from subprocess import Popen, PIPE

List=[
"CutTTmue",
"CutTTmueReverse",
]     

def getJobDef(name):    

    text = """
#!/bin/bash

#BSUB -J %s
#BSUB -o stdout_%s.out
#BSUB -e stderr_%s.out
#BSUB -q 8nh
#BSUB -u $USER@cern.ch

stagein()
{
  echo "Start";
}

runcode()
{
  echo "Running the code";
  cd /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run;
  set -x
  mkdir results/Nominal/
  selection=%s
  #../bin/analysissample --SaveDir results/Nominal/ --Region EF1 --SubRegion ttel_mue --Type Data --Selection $selection --DoHist --SaveTag Final04 --Variation Nominal --EGammaHist
  #../bin/analysissample --SaveDir results/Nominal/ --Region EF1 --SubRegion ttel_mue --Type Reco --Process ZjetsMuMu --Selection $selection  --DoHist --UseWeight --LumiWeight --SaveTag Final04 --Variation Nominal --EGammaHist
  #../bin/analysissample --SaveDir results/Nominal/ --Region EF1 --SubRegion ttel_mue --Type Reco --Process ZjetsTauTau --Selection $selection  --DoHist --UseWeight --LumiWeight --SaveTag Final04 --Variation Nominal --EGammaHist
  ../bin/analysissample --SaveDir results/Nominal/ --Region EF1 --SubRegion ttel_mue --Type Reco --Process TTBar --Selection $selection  --DoHist --UseWeight --LumiWeight --SaveTag Final04 --Variation Nominal --EGammaHist
  #../bin/analysissample --SaveDir results/Nominal/ --Region EF1 --SubRegion ttel_mue --Type Reco --Process WjetsMu --Selection $selection  --DoHist --UseWeight --LumiWeight --SaveTag Final04 --Variation Nominal --EGammaHist
  #../bin/analysissample --SaveDir results/Nominal/ --Region EF1 --SubRegion ttel_mue --Type Reco --Process WjetsTau --Selection $selection  --DoHist --UseWeight --LumiWeight --SaveTag Final04 --Variation Nominal --EGammaHist
  #../bin/analysissample --SaveDir results/Nominal/ --Region EF1 --SubRegion ttel_mue --Type Reco --Process Diboson --Selection $selection  --DoHist --UseWeight --LumiWeight --SaveTag Final04 --Variation Nominal --EGammaHist
  #../bin/analysissample --SaveDir results/Nominal/ --Region EF1 --SubRegion ttel_mue --Type Reco --Process STOthers --Selection $selection  --DoHist --UseWeight --LumiWeight --SaveTag Final04 --Variation Nominal --EGammaHist
  #../bin/analysissample --SaveDir results/Nominal/ --Region EF1 --SubRegion ttel_mue --Type Reco --Process STWT --Selection $selection  --DoHist --UseWeight --LumiWeight --SaveTag Final04 --Variation Nominal --EGammaHist
}

stageout()
{
  echo "Finished";
}

stagein
runcode
stageout

exit
""" % (name, name, name, name) 

    return text

def submitJob(name):

    bsubFile = open( name + ".bsub", "w")          
    text = getJobDef(name)            
    bsubFile.write(text)
    bsubFile.close()            
    os.system("chmod 775 -R *")            

for term in List:
    submitJob(term)
    command="bsub -q 8nh "+ term + ".bsub"
    print command
    os.system(command)

