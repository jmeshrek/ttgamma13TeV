#!/bin/env python

import os,sys,string,argparse,commands
import shlex
from subprocess import Popen, PIPE

List=[
["Signal", "ejets", "TruePh", "TrueLp"],
["TTBar", "ejets", "EFake", "TrueLp"],
["Zjets", "ejets", "TruePh", "TrueLp"],
["Zjets", "ejets", "EFake", "TrueLp"],
["Wjets", "ejets", "TruePh", "TrueLp"],
["ST", "ejets", "TruePh", "TrueLp"],
["Diboson", "ejets", "TruePh", "TrueLp"],

["Signal", "mujets", "TruePh", "TrueLp"],
["TTBar", "mujets", "EFake", "TrueLp"],
["Zjets", "mujets", "TruePh", "TrueLp"],
["Wjets", "mujets", "TruePh", "TrueLp"],
["ST", "mujets", "TruePh", "TrueLp"],
["Diboson", "mujets", "TruePh", "TrueLp"],

["Signal", "ee", "TruePh", "TrueLp"],
["Zjets", "ee", "TruePh", "TrueLp"],
["ST", "ee", "TruePh", "TrueLp"],
["Diboson", "ee", "TruePh", "TrueLp"],

["Signal", "mumu", "TruePh", "TrueLp"],
["Zjets", "mumu", "TruePh", "TrueLp"],
["ST", "mumu", "TruePh", "TrueLp"],
["Diboson", "mumu", "TruePh", "TrueLp"],

["Signal", "emu", "TruePh", "TrueLp"],
["ST", "emu", "TruePh", "TrueLp"],
["Diboson", "emu", "TruePh", "TrueLp"],
]     

def getJobDef(sample, channel, phtype, lptype):    

    text = """
#!/bin/bash

stagein()
{
  echo "Start";
}

runcode()
{
  echo "Running the code";
  cd /afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/run;
  set -x

  ../bin/analysissample --SaveDir results/Upgrade/ --Region UR --Process %s --Type Upgrade --SubRegion %s --Selection CutSRUpgrade --DoHist --UseWeight --LumiWeight --SaveTag U08 --Variation Nominal --PhMatch %s --LpMatch %s --CME 14TeV
}

stageout()
{
  echo "Finished";
}

stagein
runcode
stageout

exit
""" % (sample, channel, phtype, lptype) 
#""" % (sample, channel, phtype, lptype, sample, channel, phtype, lptype, sample, channel, phtype, lptype) 

    return text

def submitJob(sample, channel, phtype, lptype):

    bsubFile = open( sample +"." + channel + "." + phtype + "." + lptype + ".bsub", "w")          
    text = getJobDef(sample, channel, phtype, lptype)            
    bsubFile.write(text)
    bsubFile.close()            
    os.system("chmod 775 -R *")            

for term in List:
    submitJob(term[0], term[1], term[2], term[3])
    command="bsub -q 8nh "+ term[0]+"." + term[1] + "." + term[2] + "." + term[3] + ".bsub"
    print command
    os.system(command)

