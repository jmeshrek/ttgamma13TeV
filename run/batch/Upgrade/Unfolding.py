#!/bin/env python

import os,sys,string,argparse,commands
import shlex
from subprocess import Popen, PIPE

List=[
["", ""],
["--Upgrade2", ""],
["--Upgrade3", ""],
["", "--emu"],
["--Upgrade2", "--emu"],
["--Upgrade3", "--emu"],
]     

def getJobDef(tag1, tag2):    

    text = """
#!/bin/bash

stagein()
{
  echo "Start";
}

runcode()
{
  echo "Running the code";
  cd /afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/run;
  set -x

  ../bin/unfoldinginputs --Upgrade --pt2000 %s %s
}

stageout()
{
  echo "Finished";
}

stagein
runcode
stageout

exit
""" % (tag1, tag2) 
#""" % (sample, channel, phtype, lptype, sample, channel, phtype, lptype, sample, channel, phtype, lptype) 

    return text

def submitJob(tag1,tag2):

    bsubFile = open("Unfolding" + tag1 + tag2 + ".bsub", "w")          
    text = getJobDef(tag1,tag2)            
    bsubFile.write(text)
    bsubFile.close()            
    os.system("chmod 775 -R *")            

for term in List:
    submitJob(term[0], term[1])
    command="bsub -q 8nh Unfolding"+ term[0]+ term[1] + ".bsub"
    print command
    os.system(command)
