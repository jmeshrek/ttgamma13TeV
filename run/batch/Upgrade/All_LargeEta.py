#!/bin/env python

import os,sys,string,argparse,commands
import shlex
from subprocess import Popen, PIPE

List=[
["Signal", "ejets", "TruePh", "TrueLp"],
#["TTBar", "ejets", "HFake", "TrueLp"],
#["TTBar", "ejets", "EFake", "TrueLp"],
#["Zjets", "ejets", "TruePh", "TrueLp"],
#["Zjets", "ejets", "HFake", "TrueLp"],
#["Zjets", "ejets", "EFake", "TrueLp"],
#["Wjets", "ejets", "TruePh", "TrueLp"],
#["Wjets", "ejets", "HFake", "TrueLp"],
#["ST", "ejets", "TruePh", "TrueLp"],
#["ST", "ejets", "HFake", "TrueLp"],
#["Diboson", "ejets", "TruePh", "TrueLp"],
#["Diboson", "ejets", "HFake", "TrueLp"],
#
["Signal", "mujets", "TruePh", "TrueLp"],
#["TTBar", "mujets", "HFake", "TrueLp"],
#["TTBar", "mujets", "EFake", "TrueLp"],
#["Zjets", "mujets", "TruePh", "TrueLp"],
#["Zjets", "mujets", "HFake", "TrueLp"],
#["Wjets", "mujets", "TruePh", "TrueLp"],
#["Wjets", "mujets", "HFake", "TrueLp"],
#["ST", "mujets", "TruePh", "TrueLp"],
#["ST", "mujets", "HFake", "TrueLp"],
#["Diboson", "mujets", "TruePh", "TrueLp"],
#["Diboson", "mujets", "HFake", "TrueLp"],
#
["Signal", "ee", "TruePh", "TrueLp"],
#["TTBar", "ee", "HFake", "TrueLp"],
#["Zjets", "ee", "TruePh", "TrueLp"],
#["Zjets", "ee", "HFake", "TrueLp"],
#["ST", "ee", "TruePh", "TrueLp"],
#["ST", "ee", "HFake", "TrueLp"],
#["Diboson", "ee", "TruePh", "TrueLp"],
#["Diboson", "ee", "HFake", "TrueLp"],
#
["Signal", "mumu", "TruePh", "TrueLp"],
#["TTBar", "mumu", "HFake", "TrueLp"],
#["Zjets", "mumu", "TruePh", "TrueLp"],
#["Zjets", "mumu", "HFake", "TrueLp"],
#["ST", "mumu", "TruePh", "TrueLp"],
#["ST", "mumu", "HFake", "TrueLp"],
#["Diboson", "mumu", "TruePh", "TrueLp"],
#["Diboson", "mumu", "HFake", "TrueLp"],
#
["Signal", "emu", "TruePh", "TrueLp"],
#["TTBar", "emu", "HFake", "TrueLp"],
#["ST", "emu", "TruePh", "TrueLp"],
#["ST", "emu", "HFake", "TrueLp"],
#["Diboson", "emu", "TruePh", "TrueLp"],
#["Diboson", "emu", "HFake", "TrueLp"],
]     

def getJobDef(sample, channel, phtype, lptype):    

    text = """
#!/bin/bash

stagein()
{
  echo "Start";
}

runcode()
{
  echo "Running the code";
  cd /afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/run;
  set -x

  ../bin/analysissample --SaveDir results/Upgrade/ --Region UR --Process %s --Type Upgrade --SubRegion %s --Selection CutSRUpgrade --DoHist --UseWeight --LumiWeight --SaveTag U06 --Variation Nominal --PhMatch %s --LpMatch %s --CME 14TeV --LargeEta
}

stageout()
{
  echo "Finished";
}

stagein
runcode
stageout

exit
""" % (sample, channel, phtype, lptype) 
#""" % (sample, channel, phtype, lptype, sample, channel, phtype, lptype, sample, channel, phtype, lptype) 

    return text

def submitJob(sample, channel, phtype, lptype):

    bsubFile = open( sample +"." + channel + "." + phtype + "." + lptype + ".bsub", "w")          
    text = getJobDef(sample, channel, phtype, lptype)            
    bsubFile.write(text)
    bsubFile.close()            
    os.system("chmod 775 -R *")            

for term in List:
    submitJob(term[0], term[1], term[2], term[3])
    command="bsub -q 8nh "+ term[0]+"." + term[1] + "." + term[2] + "." + term[3] + ".bsub"
    print command
    os.system(command)

