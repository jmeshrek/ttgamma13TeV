import os

ToCompare = [ 
	['DoPt DoEta MCTemp DoEGammaCali1', '9'],
	['DoPt DoEta MCTemp Reverse DoEGammaCali1', '9'],
	['DoPt DoEta MCTemp DoEGammaCali2', '9'],
	['DoPt DoEta MCTemp Reverse DoEGammaCali2', '9'],
	['DoPt DoEta MCTemp DoEGammaCali3', '9'],
	['DoPt DoEta MCTemp Reverse DoEGammaCali3', '9'],
	['DoPt DoEta MCTemp DoEGammaCali4', '9'],
	['DoPt DoEta MCTemp Reverse DoEGammaCali4', '9'],
]

for tocompare in ToCompare:
    option = "_Options " 
    option += tocompare[0]
    version = "_Version "
    version += tocompare[1]
    print option
    print version
    
    run = "cd ../../config;"
    run += "echo \"" + option + "\" > 13TeV_egammafake_study_tmp.cfg;"
    run += "echo \"" + version + "\" >> 13TeV_egammafake_study_tmp.cfg;"
    run += "cd ../run; ../bin/egammafakestudy ../config/13TeV_egammafake_study_tmp.cfg;"
    success = os.system(run)

    if success !=  0:
        break

    run = "rm ../../config/13TeV_egammafake_study_tmp.cfg;"
    os.system(run)
