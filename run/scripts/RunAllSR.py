import os

Save = "V010.02"
gen_option = "DoHist UseWeight LumiWeight"
ToRun = [ 

	['', 'CR1','ejets','CutSR','','','Data','','','',], 
	['', 'CR1','ejets','CutSR','','','QCD','','eta:mtw',''], 
        ['PhMatch UseKfactor', 'CR1','ejets','CutSR','TruePh','Signal','Reco','FULL','','',], 
        ['PhMatch UseKfactor', 'CR1','ejets','CutSR','Ancestor1','Signal','Reco','FULL','','',], 
        ['PhMatch UseKfactor', 'CR1','ejets','CutSR','Ancestor2','Signal','Reco','FULL','','',], 
        ['PhMatch UseKfactor', 'CR1','ejets','CutSR','Ancestor3','Signal','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ejets','CutSR','TruePh','Signal','Reco','FULL','','Old',], 
        ['PhMatch', 'CR1','ejets','CutSR','TruePh','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ejets','CutSR','Ancestor1','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ejets','CutSR','Ancestor2','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ejets','CutSR','Ancestor3','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ejets','CutSR','HFake','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ejets','CutSR','EFake','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ejets','CutSR','TruePh','WGammajetsElNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ejets','CutSR','TruePh','WGammajetsMuNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ejets','CutSR','TruePh','WGammajetsTauNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ejets','CutSR','HFake','WjetsEl','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ejets','CutSR','HFake','WjetsMu','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ejets','CutSR','HFake','WjetsTau','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ejets','CutSR','EFake','WjetsEl','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ejets','CutSR','EFake','WjetsMu','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ejets','CutSR','EFake','WjetsTau','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ejets','CutSR','TruePh','ZGammajetsElElNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ejets','CutSR','TruePh','ZGammajetsMuMuNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ejets','CutSR','TruePh','ZGammajetsTauTauNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ejets','CutSR','HFake','ZjetsElEl','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ejets','CutSR','HFake','ZjetsMuMu','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ejets','CutSR','HFake','ZjetsTauTau','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ejets','CutSR','EFake','ZjetsElEl','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ejets','CutSR','EFake','ZjetsMuMu','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ejets','CutSR','EFake','ZjetsTauTau','Reco','FULL','','',], 
	['PhMatch', 'CR1','ejets','CutSR','TruePh','Diboson','Reco','FULL','','',], 
	['PhMatch', 'CR1','ejets','CutSR','TruePh','STOthers','Reco','FULL','','',], 
	['PhMatch', 'CR1','ejets','CutSR','TruePh','STWT','Reco','FULL','','',], 
	['PhMatch', 'CR1','ejets','CutSR','HFake','Diboson','Reco','FULL','','',], 
	['PhMatch', 'CR1','ejets','CutSR','HFake','STOthers','Reco','FULL','','',], 
	['PhMatch', 'CR1','ejets','CutSR','HFake','STWT','Reco','FULL','','',], 
	['PhMatch', 'CR1','ejets','CutSR','EFake','Diboson','Reco','FULL','','',], 
	['PhMatch', 'CR1','ejets','CutSR','EFake','STOthers','Reco','FULL','','',], 
	['PhMatch', 'CR1','ejets','CutSR','EFake','STWT','Reco','FULL','','',], 

	['', 'CR1','mujets','CutSR','','','Data','','','',], 
	['UsePS', 'CR1','mujets','CutSR','','','QCD','','pt:mtw',''], 
        ['PhMatch UseKfactor', 'CR1','mujets','CutSR','TruePh','Signal','Reco','FULL','','',], 
        ['PhMatch UseKfactor', 'CR1','mujets','CutSR','Ancestor1','Signal','Reco','FULL','','',], 
        ['PhMatch UseKfactor', 'CR1','mujets','CutSR','Ancestor2','Signal','Reco','FULL','','',], 
        ['PhMatch UseKfactor', 'CR1','mujets','CutSR','Ancestor3','Signal','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mujets','CutSR','TruePh','Signal','Reco','FULL','','Old',], 
        ['PhMatch', 'CR1','mujets','CutSR','TruePh','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mujets','CutSR','Ancestor1','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mujets','CutSR','Ancestor2','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mujets','CutSR','Ancestor3','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mujets','CutSR','HFake','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mujets','CutSR','EFake','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mujets','CutSR','TruePh','WGammajetsElNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mujets','CutSR','TruePh','WGammajetsMuNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mujets','CutSR','TruePh','WGammajetsTauNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mujets','CutSR','HFake','WjetsEl','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mujets','CutSR','HFake','WjetsMu','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mujets','CutSR','HFake','WjetsTau','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mujets','CutSR','EFake','WjetsEl','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mujets','CutSR','EFake','WjetsMu','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mujets','CutSR','EFake','WjetsTau','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mujets','CutSR','TruePh','ZGammajetsElElNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mujets','CutSR','TruePh','ZGammajetsMuMuNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mujets','CutSR','TruePh','ZGammajetsTauTauNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mujets','CutSR','HFake','ZjetsElEl','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mujets','CutSR','HFake','ZjetsMuMu','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mujets','CutSR','HFake','ZjetsTauTau','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mujets','CutSR','EFake','ZjetsElEl','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mujets','CutSR','EFake','ZjetsMuMu','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mujets','CutSR','EFake','ZjetsTauTau','Reco','FULL','','',], 
	['PhMatch', 'CR1','mujets','CutSR','TruePh','Diboson','Reco','FULL','','',], 
	['PhMatch', 'CR1','mujets','CutSR','TruePh','STOthers','Reco','FULL','','',], 
	['PhMatch', 'CR1','mujets','CutSR','TruePh','STWT','Reco','FULL','','',], 
	['PhMatch', 'CR1','mujets','CutSR','HFake','Diboson','Reco','FULL','','',], 
	['PhMatch', 'CR1','mujets','CutSR','HFake','STOthers','Reco','FULL','','',], 
	['PhMatch', 'CR1','mujets','CutSR','HFake','STWT','Reco','FULL','','',], 
	['PhMatch', 'CR1','mujets','CutSR','EFake','Diboson','Reco','FULL','','',], 
	['PhMatch', 'CR1','mujets','CutSR','EFake','STOthers','Reco','FULL','','',], 
	['PhMatch', 'CR1','mujets','CutSR','EFake','STWT','Reco','FULL','','',], 

	['', 'CR1','ee','CutSR','','','Data','','','',], 
        ['PhMatch UseKfactor', 'CR1','ee','CutSR','TruePh','Signal','Reco','FULL','','',], 
        ['PhMatch UseKfactor', 'CR1','ee','CutSR','Ancestor1','Signal','Reco','FULL','','',], 
        ['PhMatch UseKfactor', 'CR1','ee','CutSR','Ancestor2','Signal','Reco','FULL','','',], 
        ['PhMatch UseKfactor', 'CR1','ee','CutSR','Ancestor3','Signal','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ee','CutSR','TruePh','Signal','Reco','FULL','','Old',], 
        ['PhMatch', 'CR1','ee','CutSR','TruePh','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ee','CutSR','Ancestor1','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ee','CutSR','Ancestor2','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ee','CutSR','Ancestor3','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ee','CutSR','HFake','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ee','CutSR','EFake','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ee','CutSR','TruePh','WGammajetsElNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ee','CutSR','TruePh','WGammajetsMuNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ee','CutSR','TruePh','WGammajetsTauNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ee','CutSR','HFake','WjetsEl','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ee','CutSR','HFake','WjetsMu','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ee','CutSR','HFake','WjetsTau','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ee','CutSR','EFake','WjetsEl','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ee','CutSR','EFake','WjetsMu','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ee','CutSR','EFake','WjetsTau','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ee','CutSR','TruePh','ZGammajetsElElNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ee','CutSR','TruePh','ZGammajetsMuMuNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ee','CutSR','TruePh','ZGammajetsTauTauNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ee','CutSR','HFake','ZjetsElEl','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ee','CutSR','HFake','ZjetsMuMu','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ee','CutSR','HFake','ZjetsTauTau','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ee','CutSR','EFake','ZjetsElEl','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ee','CutSR','EFake','ZjetsMuMu','Reco','FULL','','',], 
        ['PhMatch', 'CR1','ee','CutSR','EFake','ZjetsTauTau','Reco','FULL','','',], 
	['PhMatch', 'CR1','ee','CutSR','TruePh','Diboson','Reco','FULL','','',], 
	['PhMatch', 'CR1','ee','CutSR','TruePh','STOthers','Reco','FULL','','',], 
	['PhMatch', 'CR1','ee','CutSR','TruePh','STWT','Reco','FULL','','',], 
	['PhMatch', 'CR1','ee','CutSR','HFake','Diboson','Reco','FULL','','',], 
	['PhMatch', 'CR1','ee','CutSR','HFake','STOthers','Reco','FULL','','',], 
	['PhMatch', 'CR1','ee','CutSR','HFake','STWT','Reco','FULL','','',], 
	['PhMatch', 'CR1','ee','CutSR','EFake','Diboson','Reco','FULL','','',], 
	['PhMatch', 'CR1','ee','CutSR','EFake','STOthers','Reco','FULL','','',], 
	['PhMatch', 'CR1','ee','CutSR','EFake','STWT','Reco','FULL','','',], 

	['', 'CR1','mumu','CutSR','','','Data','','','',], 
        ['PhMatch UseKfactor', 'CR1','mumu','CutSR','TruePh','Signal','Reco','FULL','','',], 
        ['PhMatch UseKfactor', 'CR1','mumu','CutSR','Ancestor1','Signal','Reco','FULL','','',], 
        ['PhMatch UseKfactor', 'CR1','mumu','CutSR','Ancestor2','Signal','Reco','FULL','','',], 
        ['PhMatch UseKfactor', 'CR1','mumu','CutSR','Ancestor3','Signal','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mumu','CutSR','TruePh','Signal','Reco','FULL','','Old',], 
        ['PhMatch', 'CR1','mumu','CutSR','TruePh','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mumu','CutSR','Ancestor1','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mumu','CutSR','Ancestor2','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mumu','CutSR','Ancestor3','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mumu','CutSR','HFake','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mumu','CutSR','EFake','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mumu','CutSR','TruePh','WGammajetsElNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mumu','CutSR','TruePh','WGammajetsMuNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mumu','CutSR','TruePh','WGammajetsTauNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mumu','CutSR','HFake','WjetsEl','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mumu','CutSR','HFake','WjetsMu','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mumu','CutSR','HFake','WjetsTau','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mumu','CutSR','EFake','WjetsEl','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mumu','CutSR','EFake','WjetsMu','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mumu','CutSR','EFake','WjetsTau','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mumu','CutSR','TruePh','ZGammajetsElElNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mumu','CutSR','TruePh','ZGammajetsMuMuNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mumu','CutSR','TruePh','ZGammajetsTauTauNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mumu','CutSR','HFake','ZjetsElEl','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mumu','CutSR','HFake','ZjetsMuMu','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mumu','CutSR','HFake','ZjetsTauTau','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mumu','CutSR','EFake','ZjetsElEl','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mumu','CutSR','EFake','ZjetsMuMu','Reco','FULL','','',], 
        ['PhMatch', 'CR1','mumu','CutSR','EFake','ZjetsTauTau','Reco','FULL','','',], 
	['PhMatch', 'CR1','mumu','CutSR','TruePh','Diboson','Reco','FULL','','',], 
	['PhMatch', 'CR1','mumu','CutSR','TruePh','STOthers','Reco','FULL','','',], 
	['PhMatch', 'CR1','mumu','CutSR','TruePh','STWT','Reco','FULL','','',], 
	['PhMatch', 'CR1','mumu','CutSR','HFake','Diboson','Reco','FULL','','',], 
	['PhMatch', 'CR1','mumu','CutSR','HFake','STOthers','Reco','FULL','','',], 
	['PhMatch', 'CR1','mumu','CutSR','HFake','STWT','Reco','FULL','','',], 
	['PhMatch', 'CR1','mumu','CutSR','EFake','Diboson','Reco','FULL','','',], 
	['PhMatch', 'CR1','mumu','CutSR','EFake','STOthers','Reco','FULL','','',], 
	['PhMatch', 'CR1','mumu','CutSR','EFake','STWT','Reco','FULL','','',], 

	['', 'CR1','emu','CutSR','','','Data','','','',], 
        ['PhMatch UseKfactor', 'CR1','emu','CutSR','TruePh','Signal','Reco','FULL','','',], 
        ['PhMatch UseKfactor', 'CR1','emu','CutSR','Ancestor1','Signal','Reco','FULL','','',], 
        ['PhMatch UseKfactor', 'CR1','emu','CutSR','Ancestor2','Signal','Reco','FULL','','',], 
        ['PhMatch UseKfactor', 'CR1','emu','CutSR','Ancestor3','Signal','Reco','FULL','','',], 
        ['PhMatch', 'CR1','emu','CutSR','TruePh','Signal','Reco','FULL','','Old',], 
        ['PhMatch', 'CR1','emu','CutSR','TruePh','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','emu','CutSR','Ancestor1','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','emu','CutSR','Ancestor2','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','emu','CutSR','Ancestor3','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','emu','CutSR','HFake','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','emu','CutSR','EFake','TTBar','Reco','FULL','','',], 
        ['PhMatch', 'CR1','emu','CutSR','TruePh','WGammajetsElNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','emu','CutSR','TruePh','WGammajetsMuNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','emu','CutSR','TruePh','WGammajetsTauNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','emu','CutSR','HFake','WjetsEl','Reco','FULL','','',], 
        ['PhMatch', 'CR1','emu','CutSR','HFake','WjetsMu','Reco','FULL','','',], 
        ['PhMatch', 'CR1','emu','CutSR','HFake','WjetsTau','Reco','FULL','','',], 
        ['PhMatch', 'CR1','emu','CutSR','EFake','WjetsEl','Reco','FULL','','',], 
        ['PhMatch', 'CR1','emu','CutSR','EFake','WjetsMu','Reco','FULL','','',], 
        ['PhMatch', 'CR1','emu','CutSR','EFake','WjetsTau','Reco','FULL','','',], 
        ['PhMatch', 'CR1','emu','CutSR','TruePh','ZGammajetsElElNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','emu','CutSR','TruePh','ZGammajetsMuMuNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','emu','CutSR','TruePh','ZGammajetsTauTauNLO','Reco','FULL','','',], 
        ['PhMatch', 'CR1','emu','CutSR','HFake','ZjetsElEl','Reco','FULL','','',], 
        ['PhMatch', 'CR1','emu','CutSR','HFake','ZjetsMuMu','Reco','FULL','','',], 
        ['PhMatch', 'CR1','emu','CutSR','HFake','ZjetsTauTau','Reco','FULL','','',], 
        ['PhMatch', 'CR1','emu','CutSR','EFake','ZjetsElEl','Reco','FULL','','',], 
        ['PhMatch', 'CR1','emu','CutSR','EFake','ZjetsMuMu','Reco','FULL','','',], 
        ['PhMatch', 'CR1','emu','CutSR','EFake','ZjetsTauTau','Reco','FULL','','',], 
	['PhMatch', 'CR1','emu','CutSR','TruePh','Diboson','Reco','FULL','','',], 
	['PhMatch', 'CR1','emu','CutSR','TruePh','STOthers','Reco','FULL','','',], 
	['PhMatch', 'CR1','emu','CutSR','TruePh','STWT','Reco','FULL','','',], 
	['PhMatch', 'CR1','emu','CutSR','HFake','Diboson','Reco','FULL','','',], 
	['PhMatch', 'CR1','emu','CutSR','HFake','STOthers','Reco','FULL','','',], 
	['PhMatch', 'CR1','emu','CutSR','HFake','STWT','Reco','FULL','','',], 
	['PhMatch', 'CR1','emu','CutSR','EFake','Diboson','Reco','FULL','','',], 
	['PhMatch', 'CR1','emu','CutSR','EFake','STOthers','Reco','FULL','','',], 
	['PhMatch', 'CR1','emu','CutSR','EFake','STWT','Reco','FULL','','',], 

	]

for torun in ToRun:
    option = gen_option
    option += torun[0]
    region = torun[1]
    subregion = torun[2]
    cut = torun[3]
    phmatch = torun[4] 
    process = torun[5]
    Type = torun[6]
    simu = torun[7]
    para = torun[8]
    addtag = torun[9]

    run = "cd /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/config/;"
    run += "cp 13TeV_analysis_sample_proto.cfg 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_OPTION/" + option + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_PHMATCH/" + phmatch + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_REGION/" + region + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_SUBREGION/" + subregion + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_PROCESS/" + process + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_TYPE/" + Type + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_SIMULATION/" + simu + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_CUT/" + cut + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_ADDTAG/" + addtag + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_QCDPARA/" + para + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_SAVE/" + Save + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    #run += "sed -i \"s/#KEY_TEST/" + test + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    #run += "sed -i \"s/#KEY_DEBUG/" + debug + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "cd /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/;"
    run += "../bin/analysissample ../config/13TeV_analysis_sample_tmp.cfg;"
    success = os.system(run)

    if success !=  0:
        break

    run = "rm ../../config/13TeV_analysis_sample_tmp.cfg;"
    os.system(run)
