import os

Save = "V010.01"
gen_option = "DoHist UseWeight LumiWeight"
ToRun = [ 

	['', 'CR1','ejets','CutWGammaVR3','','','Data','','',''], 
	['', 'CR1','ejets','CutWGammaVR3','','','QCD','','eta:mtw',''], 
        ['PhMatch', 'CR1','ejets','CutWGammaVR3','TruePh','Signal','Reco','FULL','',''], 
        ['PhMatch', 'CR1','ejets','CutWGammaVR3','HFake','TTBar','Reco','FULL','',''], 
        ['PhMatch', 'CR1','ejets','CutWGammaVR3','EFake','TTBar','Reco','FULL','',''], 
        ['PhMatch', 'CR1','ejets','CutWGammaVR3','TruePh','WGammajetsElNLO','Reco','FULL','',''], 
        ['PhMatch', 'CR1','ejets','CutWGammaVR3','TruePh','WGammajetsMuNLO','Reco','FULL','',''], 
        ['PhMatch', 'CR1','ejets','CutWGammaVR3','TruePh','WGammajetsTauNLO','Reco','FULL','',''], 
        ['PhMatch', 'CR1','ejets','CutWGammaVR3','HFake','WjetsEl','Reco','FULL','',''], 
        ['PhMatch', 'CR1','ejets','CutWGammaVR3','HFake','WjetsMu','Reco','FULL','',''], 
        ['PhMatch', 'CR1','ejets','CutWGammaVR3','HFake','WjetsTau','Reco','FULL','',''], 
        ['PhMatch', 'CR1','ejets','CutWGammaVR3','TruePh','ZGammajetsElElNLO','Reco','FULL','',''], 
        ['PhMatch', 'CR1','ejets','CutWGammaVR3','TruePh','ZGammajetsMuMuNLO','Reco','FULL','',''], 
        ['PhMatch', 'CR1','ejets','CutWGammaVR3','TruePh','ZGammajetsTauTauNLO','Reco','FULL','',''], 
        ['PhMatch', 'CR1','ejets','CutWGammaVR3','HFake','ZjetsElEl','Reco','FULL','',''], 
        ['PhMatch', 'CR1','ejets','CutWGammaVR3','HFake','ZjetsMuMu','Reco','FULL','',''], 
        ['PhMatch', 'CR1','ejets','CutWGammaVR3','HFake','ZjetsTauTau','Reco','FULL','',''], 
        ['PhMatch', 'CR1','ejets','CutWGammaVR3','EFake','ZjetsElEl','Reco','FULL','',''], 
        ['PhMatch', 'CR1','ejets','CutWGammaVR3','EFake','ZjetsMuMu','Reco','FULL','',''], 
        ['PhMatch', 'CR1','ejets','CutWGammaVR3','EFake','ZjetsTauTau','Reco','FULL','',''], 
	['', 'CR1','ejets','CutWGammaVR3','','Diboson','Reco','FULL','',''], 
	['', 'CR1','ejets','CutWGammaVR3','','STOthers','Reco','FULL','',''], 
	['', 'CR1','ejets','CutWGammaVR3','','STWT','Reco','FULL','',''], 

	['', 'CR1','mujets','CutWGammaVR3','','','Data','','',''], 
	['UsePS', 'CR1','mujets','CutWGammaVR3','','','QCD','','pt:mtw',''], 
        ['PhMatch', 'CR1','mujets','CutWGammaVR3','TruePh','Signal','Reco','FULL','',''], 
        ['PhMatch', 'CR1','mujets','CutWGammaVR3','HFake','TTBar','Reco','FULL','',''], 
        ['PhMatch', 'CR1','mujets','CutWGammaVR3','EFake','TTBar','Reco','FULL','',''], 
        ['PhMatch', 'CR1','mujets','CutWGammaVR3','TruePh','WGammajetsElNLO','Reco','FULL','',''], 
        ['PhMatch', 'CR1','mujets','CutWGammaVR3','TruePh','WGammajetsMuNLO','Reco','FULL','',''], 
        ['PhMatch', 'CR1','mujets','CutWGammaVR3','TruePh','WGammajetsTauNLO','Reco','FULL','',''], 
        ['PhMatch', 'CR1','mujets','CutWGammaVR3','HFake','WjetsEl','Reco','FULL','',''], 
        ['PhMatch', 'CR1','mujets','CutWGammaVR3','HFake','WjetsMu','Reco','FULL','',''], 
        ['PhMatch', 'CR1','mujets','CutWGammaVR3','HFake','WjetsTau','Reco','FULL','',''], 
        ['PhMatch', 'CR1','mujets','CutWGammaVR3','TruePh','ZGammajetsElElNLO','Reco','FULL','',''], 
        ['PhMatch', 'CR1','mujets','CutWGammaVR3','TruePh','ZGammajetsMuMuNLO','Reco','FULL','',''], 
        ['PhMatch', 'CR1','mujets','CutWGammaVR3','TruePh','ZGammajetsTauTauNLO','Reco','FULL','',''], 
        ['PhMatch', 'CR1','mujets','CutWGammaVR3','HFake','ZjetsElEl','Reco','FULL','',''], 
        ['PhMatch', 'CR1','mujets','CutWGammaVR3','HFake','ZjetsMuMu','Reco','FULL','',''], 
        ['PhMatch', 'CR1','mujets','CutWGammaVR3','HFake','ZjetsTauTau','Reco','FULL','',''], 
        ['PhMatch', 'CR1','mujets','CutWGammaVR3','EFake','ZjetsElEl','Reco','FULL','',''], 
        ['PhMatch', 'CR1','mujets','CutWGammaVR3','EFake','ZjetsMuMu','Reco','FULL','',''], 
        ['PhMatch', 'CR1','mujets','CutWGammaVR3','EFake','ZjetsTauTau','Reco','FULL','',''], 
	['', 'CR1','mujets','CutWGammaVR3','','Diboson','Reco','FULL','',''], 
	['', 'CR1','mujets','CutWGammaVR3','','STOthers','Reco','FULL','',''], 
	['', 'CR1','mujets','CutWGammaVR3','','STWT','Reco','FULL','',''], 

	]

for torun in ToRun:
    option = gen_option
    option += torun[0]
    region = torun[1]
    subregion = torun[2]
    cut = torun[3]
    phmatch = torun[4] 
    process = torun[5]
    Type = torun[6]
    simu = torun[7]
    para = torun[8]

    run = "cd /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/config/;"
    run += "cp 13TeV_analysis_sample_proto.cfg 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_OPTION/" + option + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_PHMATCH/" + phmatch + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_REGION/" + region + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_SUBREGION/" + subregion + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_PROCESS/" + process + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_TYPE/" + Type + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_SIMULATION/" + simu + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_CUT/" + cut + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_ADDTAG//g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_QCDPARA/" + para + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_SAVE/" + Save + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    #run += "sed -i \"s/#KEY_TEST/" + test + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    #run += "sed -i \"s/#KEY_DEBUG/" + debug + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "cd /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/;"
    run += "../bin/analysissample ../config/13TeV_analysis_sample_tmp.cfg;"
    success = os.system(run)

    if success !=  0:
        break

    run = "rm ../../config/13TeV_analysis_sample_tmp.cfg;"
    os.system(run)
