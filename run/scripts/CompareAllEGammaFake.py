import os

ToCompare = [ 
#	['DoNorm ErrorBar IsSimu', 
#	 'probe_Pt_[GeV]', 
#	 'LeadPhPtForFR_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhPtForFR_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhPtForFR_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhPtForFR_EF1_zeg_Reco_ZjetsElEl_Nominal SubLepPtForFR_EF1_zee_Reco_ZjetsElEl_Nominal', 
#	 'results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeA_Lumiweighted_V006.10.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeB_Lumiweighted_V006.10.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeC_Lumiweighted_V006.10.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeD_Lumiweighted_V006.10.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zee_CutZee_Lumiweighted_V006.10.root', 
#	 '(a)mis-reco. (b)mis-match (c)prompt_QED (d)non-prompt_QED probe_e', 
#	 'Ze#gamma/Zee_MC', 
#	 'ProbePt_EGammaFake_V006', 
#	 '0.5', '1.5'], 
#
#	['DoNorm ErrorBar IsSimu', 
#	 'probe_#eta', 
#	 'LeadPhEta_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhEta_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhEta_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhEta_EF1_zeg_Reco_ZjetsElEl_Nominal SubLepEta_EF1_zee_Reco_ZjetsElEl_Nominal', 
#	 'results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeA_Lumiweighted_V006.10.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeB_Lumiweighted_V006.10.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeC_Lumiweighted_V006.10.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeD_Lumiweighted_V006.10.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zee_CutZee_Lumiweighted_V006.10.root', 
#	 '(a)mis-reco. (b)mis-match (c)prompt_QED (d)non-prompt_QED probe_e', 
#	 'Ze#gamma/Zee_MC', 
#	 'ProbeEta_EGammaFake_V006', 
#	 '0.5', '1.5'], 
#
#	['DoNorm ErrorBar IsSimu', 
#	 'tag_Pt_[GeV]', 
#	 'LeadLepPtForFR_EF1_zeg_Reco_ZjetsElEl_Nominal LeadLepPtForFR_EF1_zeg_Reco_ZjetsElEl_Nominal LeadLepPtForFR_EF1_zeg_Reco_ZjetsElEl_Nominal LeadLepPtForFR_EF1_zeg_Reco_ZjetsElEl_Nominal LeadLepPtForFR_EF1_zee_Reco_ZjetsElEl_Nominal', 
#	 'results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeA_Lumiweighted_V006.10.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeB_Lumiweighted_V006.10.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeC_Lumiweighted_V006.10.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeD_Lumiweighted_V006.10.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zee_CutZee_Lumiweighted_V006.10.root', 
#	 'tag_for_(a) tag_for_(b) tag_for_(c) tag_for_(d) tag_e', 
#	 'Ze#gamma/Zee_MC', 
#	 'TagPt_EGammaFake_V006', 
#	 '0.5', '1.5'], 
#
#	['DoNorm ErrorBar IsSimu', 
#	 'm(#gamma,e)_[GeV]', 
#	 'MPhLepForZ_EF1_zeg_Reco_ZjetsElEl_Nominal MPhLepForZ_EF1_zeg_Reco_ZjetsElEl_Nominal MPhLepForZ_EF1_zeg_Reco_ZjetsElEl_Nominal MPhLepForZ_EF1_zeg_Reco_ZjetsElEl_Nominal MLepLepForZ_EF1_zee_Reco_ZjetsElEl_Nominal', 
#	 'results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeA_Lumiweighted_V006.10.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeB_Lumiweighted_V006.10.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeC_Lumiweighted_V006.10.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeD_Lumiweighted_V006.10.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zee_CutZee_Lumiweighted_V006.10.root', 
#	 'm(tag,(a)) m(tag,(b)) m(tag,(c)) m(tag,(d)) m(tag,probe)', 
#	 'Ze#gamma/Zee_MC', 
#	 'Mass_EGammaFake_V006', 
#	 '0.5', '1.5'], 
#

##########################################
# egamma fake data-driven SF comparison

# pt/eta SF of different fake types
	['ErrorBar IsSimu', 
	 'probe_Pt_[GeV]', 
	 'FR_Ptbins_EF1_zee_Reco_TypeA_Nominal FR_Ptbins_EF1_zee_Reco_TypeB_Nominal FR_Ptbins_EF1_zee_Reco_TypeC_Nominal FR_Ptbins_EF1_zee_Reco_TypeD_Nominal',
	 'results/EGammaFakeRates_EF1_zee_Pt_Eta_BkgGaus.root results/EGammaFakeRates_EF1_zee_Pt_Eta_BkgGaus.root results/EGammaFakeRates_EF1_zee_Pt_Eta_BkgGaus.root results/EGammaFakeRates_EF1_zee_Pt_Eta_BkgGaus.root',
	 '(a)mis-reco. (b)mis-match (c)prompt_QED (d)non-prompt_QED', 
	 'Fake_Rate', 
	 'FR_Pt_EGammaFake_V009', 
	 '0.5', '1.5'], 

	['ErrorBar IsSimu', 
	 'probe_Eta', 
	 'FR_Etabins_EF1_zee_Reco_TypeA_Nominal FR_Etabins_EF1_zee_Reco_TypeB_Nominal FR_Etabins_EF1_zee_Reco_TypeC_Nominal FR_Etabins_EF1_zee_Reco_TypeD_Nominal',
	 'results/EGammaFakeRates_EF1_zee_Pt_Eta_BkgGaus.root results/EGammaFakeRates_EF1_zee_Pt_Eta_BkgGaus.root results/EGammaFakeRates_EF1_zee_Pt_Eta_BkgGaus.root results/EGammaFakeRates_EF1_zee_Pt_Eta_BkgGaus.root',
	 '(a)mis-reco. (b)mis-match (c)prompt_QED (d)non-prompt_QED', 
	 'Fake_Rate', 
	 'FR_Eta_EGammaFake_V009', 
	 '0.5', '1.5'], 

#	['ErrorBar', 
#	 'probe_Pt_[GeV]', 
#	 'FR_Ptbins_EF1_zee_Data_Nominal FR_Ptbins_EF1_zee_Data_Nominal FR_Ptbins_EF1_zee_Reco_TypeA_Nominal FR_Ptbins_EF1_zee_Reco_TypeA_Nominal',
#	 'results/EGammaFakeRates_EF1_zee_MCTemp_V10.root results/EGammaFakeRates_EF1_zee_Reverse_MCTemp_V10.root results/EGammaFakeRates_EF1_zee_MCTemp_V10.root results/EGammaFakeRates_EF1_zee_Reverse_MCTemp_V10.root',
#	 'data data_(R) type_(a) type_(a)_(R)', 
#	 'Fake_Rate', 
#	 'FR_R_Pt_EGammaFake_V006', 
#	 '0.5', '1.5'], 
#
#	['ErrorBar', 
#	 'probe_Eta', 
#	 'FR_Etabins_EF1_zee_Data_Nominal FR_Etabins_EF1_zee_Data_Nominal FR_Etabins_EF1_zee_Reco_TypeA_Nominal FR_Etabins_EF1_zee_Reco_TypeA_Nominal',
#	 'results/EGammaFakeRates_EF1_zee_MCTemp_V10.root results/EGammaFakeRates_EF1_zee_Reverse_MCTemp_V10.root results/EGammaFakeRates_EF1_zee_MCTemp_V10.root results/EGammaFakeRates_EF1_zee_Reverse_MCTemp_V10.root',
#	 'data data_(R) type_(a) type_(a)_(R)', 
#	 'Fake_Rate', 
#	 'FR_R_Eta_EGammaFake_V006', 
#	 '0.5', '1.5'], 

##########################################
# SF errror decomposition
#	['ErrorBar ShowRatio', 
#	 'probe_Pt_[GeV]', 
#	 'SF_Ptbins_ SF_N_Ptbins_ SF_M_Ptbins_ SF_E_Ptbins_ SF_S_Ptbins_ SF_ZG_Ptbins_',
#	 'results/EGammaSF.root results/EGammaSF.root results/EGammaSF.root results/EGammaSF.root results/EGammaSF.root results/EGammaSF.root',
#	 'Nominal Stat. Signal. RangeUp RangeDown Subtraction', 
#	 'Nominal_Fake_Rate', 
#	 'Nominal_EGamma_FR_SF_Pt', 
#	 '0.5', '1.5'], 
#
#	['ErrorBar ShowRatio', 
#	 'probe_Pt_[GeV]', 
#	 'SF_Ptbins__Reverse SF_N_Ptbins__Reverse SF_M_Ptbins__Reverse SF_E_Ptbins__Reverse SF_S_Ptbins__Reverse SF_ZG_Ptbins__Reverse',
#	 'results/EGammaSF.root results/EGammaSF.root results/EGammaSF.root results/EGammaSF.root results/EGammaSF.root results/EGammaSF.root',
#	 'Nominal Stat. Signal. RangeUp RangeDown Subtraction', 
#	 'Reverse_Fake_Rate', 
#	 'Reverse_EGamma_FR_SF_Pt', 
#	 '0.5', '1.5'], 
#
#	['ErrorBar ShowRatio', 
#	 'probe_Pt_[GeV]', 
#	 'SF_PtMerged_Ptbins_ SF_PtMerged_N_Ptbins_ SF_PtMerged_M_Ptbins_ SF_PtMerged_E_Ptbins_ SF_PtMerged_S_Ptbins_ SF_PtMerged_ZG_Ptbins_',
#	 'results/EGammaSF.root results/EGammaSF.root results/EGammaSF.root results/EGammaSF.root results/EGammaSF.root results/EGammaSF.root',
#	 'Nominal Stat. Signal. RangeUp RangeDown Subtraction', 
#	 'PtMerged_Fake_Rate', 
#	 'PtMerged_EGamma_FR_SF_Pt', 
#	 '0.5', '1.5'], 
#
#	['ErrorBar ShowRatio DoubleYRange', 
#	 'probe_Eta', 
#	 'SF_Etabins_ SF_N_Etabins_ SF_M_Etabins_ SF_E_Etabins_ SF_S_Etabins_ SF_ZG_Etabins_',
#	 'results/EGammaSF.root results/EGammaSF.root results/EGammaSF.root results/EGammaSF.root results/EGammaSF.root results/EGammaSF.root',
#	 'Nominal Stat. Signal. RangeUp RangeDown Subtraction', 
#	 'Nominal_Fake_Rate', 
#	 'Nominal_EGamma_FR_SF_Eta', 
#	 '0.5', '1.5'], 

##########################################
# SF nominal/reverse comparison
#	['ErrorBar DrawRatio', 
#	 'probe_Pt_[GeV]', 
#	 'SF_Ptbins_ SF_Ptbins__Reverse',
#	 'results/EGammaSF.root results/EGammaSF.root',
#	 'Nominal Reverse', 
#	 'Pt_Total_Error', 
#	 'NvsR_EGamma_FR_SF_Pt', 
#	 '0', '2'], 

#	['ErrorBar DrawRatio', 
#	 'probe_Pt_[GeV]', 
#	 'SF_N_Ptbins_ SF_N_Ptbins__Reverse',
#	 'results/EGammaSF.root results/EGammaSF.root',
#	 'Nominal Reverse', 
#	 'Pt_Nominal', 
#	 'NvsR_Nominal_EGamma_FR_SF_Pt', 
#	 '0', '2'], 
#	
#	['ErrorBar DrawRatio', 
#	 'probe_Pt_[GeV]', 
#	 'SF_M_Ptbins_ SF_M_Ptbins__Reverse',
#	 'results/EGammaSF.root results/EGammaSF.root',
#	 'Nominal Reverse', 
#	 'Pt_MCTemp', 
#	 'NvsR_MCTemp_EGamma_FR_SF_Pt', 
#	 '0', '2'], 
#
#	['ErrorBar DrawRatio', 
#	 'probe_Pt_[GeV]', 
#	 'SF_E_Ptbins_ SF_E_Ptbins__Reverse',
#	 'results/EGammaSF.root results/EGammaSF.root',
#	 'Nominal Reverse', 
#	 'Pt_RExpand', 
#	 'NvsR_RExpand_EGamma_FR_SF_Pt', 
#	 '0', '2'], 
#
#	['ErrorBar DrawRatio', 
#	 'probe_Pt_[GeV]', 
#	 'SF_S_Ptbins_ SF_S_Ptbins__Reverse',
#	 'results/EGammaSF.root results/EGammaSF.root',
#	 'Nominal Reverse', 
#	 'Pt_RShrink', 
#	 'NvsR_RShrink_EGamma_FR_SF_Pt', 
#	 '0', '2'], 
#
#	['ErrorBar DrawRatio', 
#	 'probe_Pt_[GeV]', 
#	 'SF_ZG_Ptbins_ SF_ZG_Ptbins__Reverse',
#	 'results/EGammaSF.root results/EGammaSF.root',
#	 'Nominal Reverse', 
#	 'Pt_ZGamma', 
#	 'NvsR_ZGamma_EGamma_FR_SF_Pt', 
#	 '0', '2'], 
#
#	['ErrorBar DrawRatio', 
#	 'probe_Eta', 
#	 'SF_Etabins_ SF_Etabins__Reverse',
#	 'results/EGammaSF.root results/EGammaSF.root',
#	 'Nominal Reverse', 
#	 'Eta_Total_Error', 
#	 'NvsR_EGamma_FR_SF_Eta', 
#	 '0', '2'], 
#	
#	['ErrorBar DrawRatio', 
#	 'probe_Eta', 
#	 'SF_N_Etabins_ SF_N_Etabins__Reverse',
#	 'results/EGammaSF.root results/EGammaSF.root',
#	 'Nominal Reverse', 
#	 'Eta_Nominal', 
#	 'NvsR_Nominal_EGamma_FR_SF_Eta', 
#	 '0', '2'], 
#	
#	['ErrorBar DrawRatio', 
#	 'probe_Eta', 
#	 'SF_M_Etabins_ SF_M_Etabins__Reverse',
#	 'results/EGammaSF.root results/EGammaSF.root',
#	 'Nominal Reverse', 
#	 'Eta_MCTemp', 
#	 'NvsR_MCTemp_EGamma_FR_SF_Eta', 
#	 '0', '2'], 
#
#	['ErrorBar DrawRatio', 
#	 'probe_Eta', 
#	 'SF_E_Etabins_ SF_E_Etabins__Reverse',
#	 'results/EGammaSF.root results/EGammaSF.root',
#	 'Nominal Reverse', 
#	 'Eta_RExpand', 
#	 'NvsR_RExpand_EGamma_FR_SF_Eta', 
#	 '0', '2'], 
#
#	['ErrorBar DrawRatio', 
#	 'probe_Eta', 
#	 'SF_S_Etabins_ SF_S_Etabins__Reverse',
#	 'results/EGammaSF.root results/EGammaSF.root',
#	 'Nominal Reverse', 
#	 'Eta_RShrink', 
#	 'NvsR_RShrink_EGamma_FR_SF_Eta', 
#	 '0', '2'], 
#
#	['ErrorBar DrawRatio', 
#	 'probe_Eta', 
#	 'SF_ZG_Etabins_ SF_ZG_Etabins__Reverse',
#	 'results/EGammaSF.root results/EGammaSF.root',
#	 'Nominal Reverse', 
#	 'Eta_ZGamma', 
#	 'NvsR_ZGamma_EGamma_FR_SF_Eta', 
#	 '0', '2'], 

#	['ErrorBar DrawRatio', 
#	 'probe_Eta', 
#	 'SF_Etabins_ SF_N_Etabins_',
#	 'results/EGammaSF.root results/EGammaSF.root',
#	 'Total Stat.', 
#	 'Eta_FR_SF', 
#	 'Error_EGamma_FR_SF_Eta', 
#	 '0', '2'], 

#############################################
#  Shape comparison between each fake types
#	['DoNorm ErrorBar DrawRatio', 
#	 'PPT', 
#	 'LeadPhPPT_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhPPT_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhPPT_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhPPT_EF1_zeg_Reco_ZjetsElEl_Nominal',
#	 'results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeA_Lumiweighted_EGammaReweighted_V009.03.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeB_Lumiweighted_EGammaReweighted_V009.03.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeC_Lumiweighted_EGammaReweighted_V009.03.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeD_Lumiweighted_EGammaReweighted_V009.03.root',
#	 '(a)mis-reco. (b)mis-match (c)prompt_QED (d)non-prompt_QED', 
#	 'EGamma_Fake', 
#	 'EGamma_Shape_PPT', 
#	 '0.5', '1.5'], 
#
#	['DoNorm ErrorBar DrawRatio', 
#	 'fSide', 
#	 'LeadPhfSide_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhfSide_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhfSide_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhfSide_EF1_zeg_Reco_ZjetsElEl_Nominal',
#	 'results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeA_Lumiweighted_EGammaReweighted_V009.03.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeB_Lumiweighted_EGammaReweighted_V009.03.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeC_Lumiweighted_EGammaReweighted_V009.03.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeD_Lumiweighted_EGammaReweighted_V009.03.root',
#	 '(a)mis-reco. (b)mis-match (c)prompt_QED (d)non-prompt_QED', 
#	 'EGamma_Fake', 
#	 'EGamma_Shape_fSide', 
#	 '0.5', '1.5'], 
#
#	['DoNorm ErrorBar DrawRatio', 
#	 'WEta2', 
#	 'LeadPhWEta2_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhWEta2_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhWEta2_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhWEta2_EF1_zeg_Reco_ZjetsElEl_Nominal',
#	 'results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeA_Lumiweighted_EGammaReweighted_V009.03.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeB_Lumiweighted_EGammaReweighted_V009.03.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeC_Lumiweighted_EGammaReweighted_V009.03.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeD_Lumiweighted_EGammaReweighted_V009.03.root',
#	 '(a)mis-reco. (b)mis-match (c)prompt_QED (d)non-prompt_QED', 
#	 'EGamma_Fake', 
#	 'EGamma_Shape_WEta2', 
#	 '0.5', '1.5'], 
#
#	['DoNorm ErrorBar DrawRatio', 
#	 'WEta', 
#	 'LeadPhWEta_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhWEta_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhWEta_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhWEta_EF1_zeg_Reco_ZjetsElEl_Nominal',
#	 'results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeA_Lumiweighted_EGammaReweighted_V009.03.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeB_Lumiweighted_EGammaReweighted_V009.03.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeC_Lumiweighted_EGammaReweighted_V009.03.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeD_Lumiweighted_EGammaReweighted_V009.03.root',
#	 '(a)mis-reco. (b)mis-match (c)prompt_QED (d)non-prompt_QED', 
#	 'EGamma_Fake', 
#	 'EGamma_Shape_WEta', 
#	 '0.5', '1.5'], 
#
#	['DoNorm ErrorBar DrawRatio', 
#	 'RPhi', 
#	 'LeadPhRPhi_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhRPhi_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhRPhi_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhRPhi_EF1_zeg_Reco_ZjetsElEl_Nominal',
#	 'results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeA_Lumiweighted_EGammaReweighted_V009.03.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeB_Lumiweighted_EGammaReweighted_V009.03.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeC_Lumiweighted_EGammaReweighted_V009.03.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeD_Lumiweighted_EGammaReweighted_V009.03.root',
#	 '(a)mis-reco. (b)mis-match (c)prompt_QED (d)non-prompt_QED', 
#	 'EGamma_Fake', 
#	 'EGamma_Shape_RPhi', 
#	 '0.5', '1.5'], 
#
#	['DoNorm ErrorBar DrawRatio', 
#	 'REta', 
#	 'LeadPhREta_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhREta_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhREta_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhREta_EF1_zeg_Reco_ZjetsElEl_Nominal',
#	 'results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeA_Lumiweighted_EGammaReweighted_V009.03.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeB_Lumiweighted_EGammaReweighted_V009.03.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeC_Lumiweighted_EGammaReweighted_V009.03.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeD_Lumiweighted_EGammaReweighted_V009.03.root',
#	 '(a)mis-reco. (b)mis-match (c)prompt_QED (d)non-prompt_QED', 
#	 'EGamma_Fake', 
#	 'EGamma_Shape_REta', 
#	 '0.5', '1.5'], 
#
#	['DoNorm ErrorBar DrawRatio', 
#	 'RHad1', 
#	 'LeadPhRHad1_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhRHad1_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhRHad1_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhRHad1_EF1_zeg_Reco_ZjetsElEl_Nominal',
#	 'results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeA_Lumiweighted_EGammaReweighted_V009.03.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeB_Lumiweighted_EGammaReweighted_V009.03.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeC_Lumiweighted_EGammaReweighted_V009.03.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeD_Lumiweighted_EGammaReweighted_V009.03.root',
#	 '(a)mis-reco. (b)mis-match (c)prompt_QED (d)non-prompt_QED', 
#	 'EGamma_Fake', 
#	 'EGamma_Shape_RHad1', 
#	 '0.5', '1.5'], 

#	['DoNorm ErrorBar DrawRatio', 
#	 'PtForFR', 
#	 'LeadPhPtForFR_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhPtForFR_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhPtForFR_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhPtForFR_EF1_zeg_Reco_ZjetsElEl_Nominal',
#	 'results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeA_Lumiweighted_EGammaReweighted_V009.03.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeB_Lumiweighted_EGammaReweighted_V009.03.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeC_Lumiweighted_EGammaReweighted_V009.03.root results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeD_Lumiweighted_EGammaReweighted_V009.03.root',
#	 '(a) (b) (c) (d)', 
#	 'EGamma_Fake', 
#	 'EGamma_Shape_PtForFR', 
#	 '0.5', '1.5'], 

#	['DoNorm ErrorBar DrawRatio', 
#	 'dR(true-el,reco-ph)', 
#	 'LeadPhMCElDr_EF1_zeg_Reco_ZjetsElEl_Nominal LeadPhMCElDr_EF1_zeg_Reco_ZGammajetsElElNLO_Nominal',
#	 'results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeC_Lumiweighted_V009.04.root results/Results_ZGammajetsElElNLO_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeC_Lumiweighted_V009.04.root',
#	 'Zjets Zyjets', 
#	 'EGamma_Fake_TypeC', 
#	 'EGamma_Shape_TypeC_MCElDr', 
#	 '0.5', '1.5'], 
#
	]

for tocompare in ToCompare:
    option = "_Options " 
    option += tocompare[0]
    title = "_XTitle "
    title += tocompare[1]
    hists = "_HistList "
    hists += tocompare[2]
    files = "_FileList "
    files += tocompare[3]
    lgs = "_Titles "
    lgs += tocompare[4]
    channel = "_Channel "
    channel += tocompare[5]
    save = "_Save "
    save += tocompare[6]
    ratiolo = "_RatioLo "
    ratiolo += tocompare[7]
    ratiohi = "_RatioHi "
    ratiohi += tocompare[8]
    
    run = "cd ../../config;"
    run += "echo \"" + option + "\" > 13TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + title + "\" >> 13TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + hists + "\" >> 13TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + files + "\" >> 13TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + lgs + "\" >> 13TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + channel + "\" >> 13TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + save + "\" >> 13TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + ratiolo + "\" >> 13TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + ratiohi + "\" >> 13TeV_compare_shape_tmp.cfg;"
    run += "cd ../run; ../bin/compareshapes ../config/13TeV_compare_shape_tmp.cfg;"
    success = os.system(run)

    if success !=  0:
        break

    run = "rm ../../config/13TeV_compare_shape_tmp.cfg;"
    os.system(run)
