import os

Save = "V006.10"
gen_option = "DoHist UseWeight LumiWeight"
ToRun = [ 

	#['DoKLFit DoKLbVeto PhMatch', 'CR1','ejets','CutSR2016','Ancestor1','Signal','Reco','FULL'], 
	#['DoKLFit DoKLbVeto PhMatch', 'CR1','ejets','CutSR2016','Ancestor2','Signal','Reco','FULL'], 
	#['DoKLFit DoKLbVeto PhMatch', 'CR1','ejets','CutSR2016','Ancestor3','Signal','Reco','FULL'], 

	#['DoKLFit DoKLbVeto PhMatch', 'CR1','ejets','CutSR2016Plus','Ancestor1','Signal','Reco','FULL'], 
	#['DoKLFit DoKLbVeto PhMatch', 'CR1','ejets','CutSR2016Plus','Ancestor2','Signal','Reco','FULL'], 
	#['DoKLFit DoKLbVeto PhMatch', 'CR1','ejets','CutSR2016Plus','Ancestor3','Signal','Reco','FULL'], 

	#['DoKLFit DoKLbVeto PhMatch', 'CR1','ejets','CutSR2016Plus2','Ancestor1','Signal','Reco','FULL'], 
	#['DoKLFit DoKLbVeto PhMatch', 'CR1','ejets','CutSR2016Plus2','Ancestor2','Signal','Reco','FULL'], 
	#['DoKLFit DoKLbVeto PhMatch', 'CR1','ejets','CutSR2016Plus2','Ancestor3','Signal','Reco','FULL'], 

	#['DoKLFit DoKLbVeto PhMatch', 'CR1','ejets','CutSR2016Plus3','Ancestor1','Signal','Reco','FULL'], 
	#['DoKLFit DoKLbVeto PhMatch', 'CR1','ejets','CutSR2016Plus3','Ancestor2','Signal','Reco','FULL'], 
	#['DoKLFit DoKLbVeto PhMatch', 'CR1','ejets','CutSR2016Plus3','Ancestor3','Signal','Reco','FULL'], 

#	['DoKLFit DoKLbVeto PhMatch', 'CR1','ejets','CutSR2016Plus4','Ancestor1','Signal','Reco','FULL'], 
#	['DoKLFit DoKLbVeto PhMatch', 'CR1','ejets','CutSR2016Plus4','Ancestor2','Signal','Reco','FULL'], 
#	['DoKLFit DoKLbVeto PhMatch', 'CR1','ejets','CutSR2016Plus4','Ancestor3','Signal','Reco','FULL'], 
        ['DoKLFit DoKLbVeto PhMatch', 'CR1','ejets','CutSR2016Plus4','HorEFake','TTBar','Reco','FULL'], 
#        ['DoKLFit DoKLbVeto', 'CR1','ejets','CutSR2016Plus4','','WjetsEl','Reco','FULL'], 
#        ['DoKLFit DoKLbVeto', 'CR1','ejets','CutSR2016Plus4','','WjetsMu','Reco','FULL'], 
#        ['DoKLFit DoKLbVeto', 'CR1','ejets','CutSR2016Plus4','','WjetsTau','Reco','FULL'], 
#        ['DoKLFit DoKLbVeto', 'CR1','ejets','CutSR2016Plus4','','ZjetsElEl','Reco','FULL'], 
#        ['DoKLFit DoKLbVeto', 'CR1','ejets','CutSR2016Plus4','','ZjetsMuMu','Reco','FULL'], 
#        ['DoKLFit DoKLbVeto', 'CR1','ejets','CutSR2016Plus4','','ZjetsTauTau','Reco','FULL'], 
#	['DoKLFit DoKLbVeto', 'CR1','ejets','CutSR2016Plus4','','Diboson','Reco','FULL'], 
#	['DoKLFit DoKLbVeto', 'CR1','ejets','CutSR2016Plus4','','STOthers','Reco','FULL'], 
#	['DoKLFit DoKLbVeto', 'CR1','ejets','CutSR2016Plus4','','STWT','Reco','FULL'], 

#	['DoKLFit DoKLbVeto PhMatch', 'CR1','mujets','CutSR2016Plus4','Ancestor1','Signal','Reco','FULL'], 
#	['DoKLFit DoKLbVeto PhMatch', 'CR1','mujets','CutSR2016Plus4','Ancestor2','Signal','Reco','FULL'], 
#	['DoKLFit DoKLbVeto PhMatch', 'CR1','mujets','CutSR2016Plus4','Ancestor3','Signal','Reco','FULL'], 
#        ['DoKLFit DoKLbVeto PhMatch', 'CR1','mujets','CutSR2016Plus4','HorEFake','TTBar','Reco','FULL'], 
#        ['DoKLFit DoKLbVeto', 'CR1','mujets','CutSR2016Plus4','','WjetsEl','Reco','FULL'], 
#        ['DoKLFit DoKLbVeto', 'CR1','mujets','CutSR2016Plus4','','WjetsMu','Reco','FULL'], 
#        ['DoKLFit DoKLbVeto', 'CR1','mujets','CutSR2016Plus4','','WjetsTau','Reco','FULL'], 
#        ['DoKLFit DoKLbVeto', 'CR1','mujets','CutSR2016Plus4','','ZjetsElEl','Reco','FULL'], 
#        ['DoKLFit DoKLbVeto', 'CR1','mujets','CutSR2016Plus4','','ZjetsMuMu','Reco','FULL'], 
#        ['DoKLFit DoKLbVeto', 'CR1','mujets','CutSR2016Plus4','','ZjetsTauTau','Reco','FULL'], 
#	['DoKLFit DoKLbVeto', 'CR1','mujets','CutSR2016Plus4','','Diboson','Reco','FULL'], 
#	['DoKLFit DoKLbVeto', 'CR1','mujets','CutSR2016Plus4','','STOthers','Reco','FULL'], 
#	['DoKLFit DoKLbVeto', 'CR1','mujets','CutSR2016Plus4','','STWT','Reco','FULL'], 

	]

for torun in ToRun:
    option = gen_option
    option += torun[0]
    region = torun[1]
    subregion = torun[2]
    cut = torun[3]
    phmatch = torun[4] 
    process = torun[5]
    Type = torun[6]
    simu = torun[7]

    run = "cd /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/config/;"
    run += "cp 13TeV_analysis_sample_proto.cfg 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_OPTION/" + option + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_PHMATCH/" + phmatch + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_REGION/" + region + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_SUBREGION/" + subregion + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_PROCESS/" + process + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_TYPE/" + Type + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_SIMULATION/" + simu + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_CUT/" + cut + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_SAVE/" + Save + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    #run += "sed -i \"s/#KEY_TEST/" + test + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    #run += "sed -i \"s/#KEY_DEBUG/" + debug + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "cd /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/;"
    run += "../bin/analysissample ../config/13TeV_analysis_sample_tmp.cfg;"
    success = os.system(run)

    if success !=  0:
        break

    run = "rm ../../config/13TeV_analysis_sample_tmp.cfg;"
    os.system(run)
