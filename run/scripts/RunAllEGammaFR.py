import os

Save = "V009.06"
gen_option = "DoHist UseWeight LumiWeight"
ToRun = [ 
	['PhMatch TTBarEE', 'CR1','ejets','CutTTeg','EFake2TypeA','TTBar','Reco','FULL'], 
	['PhMatch TTBarEE', 'CR1','ejets','CutTTeg','EFake2TypeB','TTBar','Reco','FULL'], 
	['PhMatch TTBarEE', 'CR1','ejets','CutTTeg','EFake2TypeC','TTBar','Reco','FULL'], 
	['PhMatch TTBarEE', 'CR1','ejets','CutTTeg','EFake2TypeD','TTBar','Reco','FULL'], 
        ['PhMatch TTBarEE', 'CR1','ejets','CutTTeg','EFake2TypeC','Signal','Reco','FULL'], 
#	['PhMatch TTBarEE EFakeDr02', 'CR1','ejets','CutTTeg','EFake2TypeA','TTBar','Reco','FULL'], 
#	['PhMatch TTBarEE EFakeDr02', 'CR1','ejets','CutTTeg','EFake2TypeB','TTBar','Reco','FULL'], 
#	['PhMatch TTBarEE EFakeDr02', 'CR1','ejets','CutTTeg','EFake2TypeC','TTBar','Reco','FULL'], 
#	['PhMatch TTBarEE EFakeDr02', 'CR1','ejets','CutTTeg','EFake2TypeD','TTBar','Reco','FULL'], 
#        ['PhMatch TTBarEE EFakeDr02', 'CR1','ejets','CutTTeg','EFake2TypeC','Signal','Reco','FULL'], 
#	['PhMatch TTBarEE EFakeDr05', 'CR1','ejets','CutTTeg','EFake2TypeA','TTBar','Reco','FULL'], 
#	['PhMatch TTBarEE EFakeDr05', 'CR1','ejets','CutTTeg','EFake2TypeB','TTBar','Reco','FULL'], 
#	['PhMatch TTBarEE EFakeDr05', 'CR1','ejets','CutTTeg','EFake2TypeC','TTBar','Reco','FULL'], 
#	['PhMatch TTBarEE EFakeDr05', 'CR1','ejets','CutTTeg','EFake2TypeD','TTBar','Reco','FULL'], 
#        ['PhMatch TTBarEE EFakeDr05', 'CR1','ejets','CutTTeg','EFake2TypeC','Signal','Reco','FULL'], 
	['TTBarEE', 'EF1','ttel_ee','CutTTee','','TTBar','Reco','FULL'], 

	['PhMatch TTBarEE', 'CR1','ejets','CutTTegReverse','EFake2TypeA','TTBar','Reco','FULL'], 
	['PhMatch TTBarEE', 'CR1','ejets','CutTTegReverse','EFake2TypeB','TTBar','Reco','FULL'], 
	['PhMatch TTBarEE', 'CR1','ejets','CutTTegReverse','EFake2TypeC','TTBar','Reco','FULL'], 
	['PhMatch TTBarEE', 'CR1','ejets','CutTTegReverse','EFake2TypeD','TTBar','Reco','FULL'], 
        ['PhMatch TTBarEE', 'CR1','ejets','CutTTegReverse','EFake2TypeC','Signal','Reco','FULL'], 
	['TTBarEE', 'EF1','ttel_ee','CutTTeeReverse','','TTBar','Reco','FULL'], 

	['PhMatch TTBarEM', 'CR1','mujets','CutTTmug','EFake2TypeA','TTBar','Reco','FULL'], 
	['PhMatch TTBarEM', 'CR1','mujets','CutTTmug','EFake2TypeB','TTBar','Reco','FULL'], 
	['PhMatch TTBarEM', 'CR1','mujets','CutTTmug','EFake2TypeC','TTBar','Reco','FULL'], 
	['PhMatch TTBarEM', 'CR1','mujets','CutTTmug','EFake2TypeD','TTBar','Reco','FULL'], 
        ['PhMatch TTBarEM', 'CR1','mujets','CutTTmug','EFake2TypeC','Signal','Reco','FULL'], 
#	['PhMatch TTBarEM EFakeDr02', 'CR1','mujets','CutTTmug','EFake2TypeA','TTBar','Reco','FULL'], 
#	['PhMatch TTBarEM EFakeDr02', 'CR1','mujets','CutTTmug','EFake2TypeB','TTBar','Reco','FULL'], 
#	['PhMatch TTBarEM EFakeDr02', 'CR1','mujets','CutTTmug','EFake2TypeC','TTBar','Reco','FULL'], 
#	['PhMatch TTBarEM EFakeDr02', 'CR1','mujets','CutTTmug','EFake2TypeD','TTBar','Reco','FULL'], 
#        ['PhMatch TTBarEM EFakeDr02', 'CR1','mujets','CutTTmug','EFake2TypeC','Signal','Reco','FULL'], 
#	['PhMatch TTBarEM EFakeDr05', 'CR1','mujets','CutTTmug','EFake2TypeA','TTBar','Reco','FULL'], 
#	['PhMatch TTBarEM EFakeDr05', 'CR1','mujets','CutTTmug','EFake2TypeB','TTBar','Reco','FULL'], 
#	['PhMatch TTBarEM EFakeDr05', 'CR1','mujets','CutTTmug','EFake2TypeC','TTBar','Reco','FULL'], 
#	['PhMatch TTBarEM EFakeDr05', 'CR1','mujets','CutTTmug','EFake2TypeD','TTBar','Reco','FULL'], 
#        ['PhMatch TTBarEM EFakeDr05', 'CR1','mujets','CutTTmug','EFake2TypeC','Signal','Reco','FULL'], 
	['TTBarEM', 'EF1','ttel_mue','CutTTmue','','TTBar','Reco','FULL'], 

	['PhMatch TTBarEM', 'CR1','mujets','CutTTmugReverse','EFake2TypeA','TTBar','Reco','FULL'], 
	['PhMatch TTBarEM', 'CR1','mujets','CutTTmugReverse','EFake2TypeB','TTBar','Reco','FULL'], 
	['PhMatch TTBarEM', 'CR1','mujets','CutTTmugReverse','EFake2TypeC','TTBar','Reco','FULL'], 
	['PhMatch TTBarEM', 'CR1','mujets','CutTTmugReverse','EFake2TypeD','TTBar','Reco','FULL'], 
        ['PhMatch TTBarEM', 'CR1','mujets','CutTTmugReverse','EFake2TypeC','Signal','Reco','FULL'], 
	['TTBarEM', 'EF1','ttel_mue','CutTTmueReverse','','TTBar','Reco','FULL'], 

	]

for torun in ToRun:
    option = gen_option
    option += torun[0]
    region = torun[1]
    subregion = torun[2]
    cut = torun[3]
    phmatch = torun[4] 
    process = torun[5]
    Type = torun[6]
    simu = torun[7]

    run = "cd /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/config/;"
    run += "cp 13TeV_analysis_sample_proto_dev.cfg 13TeV_analysis_sample_tmp_dev.cfg;"
    run += "sed -i \"s/#KEY_OPTION/" + option + "/g\" 13TeV_analysis_sample_tmp_dev.cfg;"
    run += "sed -i \"s/#KEY_PHMATCH/" + phmatch + "/g\" 13TeV_analysis_sample_tmp_dev.cfg;"
    run += "sed -i \"s/#KEY_REGION/" + region + "/g\" 13TeV_analysis_sample_tmp_dev.cfg;"
    run += "sed -i \"s/#KEY_SUBREGION/" + subregion + "/g\" 13TeV_analysis_sample_tmp_dev.cfg;"
    run += "sed -i \"s/#KEY_PROCESS/" + process + "/g\" 13TeV_analysis_sample_tmp_dev.cfg;"
    run += "sed -i \"s/#KEY_TYPE/" + Type + "/g\" 13TeV_analysis_sample_tmp_dev.cfg;"
    run += "sed -i \"s/#KEY_SIMULATION/" + simu + "/g\" 13TeV_analysis_sample_tmp_dev.cfg;"
    run += "sed -i \"s/#KEY_CUT/" + cut + "/g\" 13TeV_analysis_sample_tmp_dev.cfg;"
    run += "sed -i \"s/#KEY_QCDPARA//g\" 13TeV_analysis_sample_tmp_dev.cfg;"
    run += "sed -i \"s/#KEY_ADDTAG//g\" 13TeV_analysis_sample_tmp_dev.cfg;"
    run += "sed -i \"s/#KEY_SAVE/" + Save + "/g\" 13TeV_analysis_sample_tmp_dev.cfg;"
    #run += "sed -i \"s/#KEY_TEST/" + test + "/g\" 13TeV_analysis_sample_tmp_dev.cfg;"
    #run += "sed -i \"s/#KEY_DEBUG/" + debug + "/g\" 13TeV_analysis_sample_tmp_dev.cfg;"
    run += "cd /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/;"
    run += "../bin/analysissample_dev ../config/13TeV_analysis_sample_tmp_dev.cfg;"
    success = os.system(run)

    if success !=  0:
        break

    run = "rm ../../config/13TeV_analysis_sample_tmp_dev.cfg;"
    os.system(run)
