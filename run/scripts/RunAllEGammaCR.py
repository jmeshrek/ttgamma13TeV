import os

Save = "V009.05"
gen_option = "DoHist UseWeight LumiWeight"
ToRun = [ 

	['', 'EF1','ttel_ee','CutTTeeSR','','','Data','FULL'], 
	['', 'EF1','ttel_ee','CutTTeeSR','','ZjetsElEl','Reco','FULL'], 
	['', 'EF1','ttel_ee','CutTTeeSR','','ZjetsMuMu','Reco','FULL'], 
	['', 'EF1','ttel_ee','CutTTeeSR','','ZjetsTauTau','Reco','FULL'], 
        ['', 'EF1','ttel_ee','CutTTeeSR','','TTBar','Reco','FULL'], 
        ['', 'EF1','ttel_ee','CutTTeeSR','','WjetsEl','Reco','FULL'], 
        ['', 'EF1','ttel_ee','CutTTeeSR','','WjetsMu','Reco','FULL'], 
        ['', 'EF1','ttel_ee','CutTTeeSR','','WjetsTau','Reco','FULL'], 
	['', 'EF1','ttel_ee','CutTTeeSR','','Diboson','Reco','FULL'], 
	['', 'EF1','ttel_ee','CutTTeeSR','','STOthers','Reco','FULL'], 
	['', 'EF1','ttel_ee','CutTTeeSR','','STWT','Reco','FULL'], 

	]

for torun in ToRun:
    option = gen_option
    option += torun[0]
    region = torun[1]
    subregion = torun[2]
    cut = torun[3]
    phmatch = torun[4] 
    process = torun[5]
    Type = torun[6]
    simu = torun[7]

    run = "cd /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/config/;"
    run += "cp 13TeV_analysis_sample_proto_dev.cfg 13TeV_analysis_sample_tmp_dev.cfg;"
    run += "sed -i \"s/#KEY_OPTION/" + option + "/g\" 13TeV_analysis_sample_tmp_dev.cfg;"
    run += "sed -i \"s/#KEY_PHMATCH/" + phmatch + "/g\" 13TeV_analysis_sample_tmp_dev.cfg;"
    run += "sed -i \"s/#KEY_REGION/" + region + "/g\" 13TeV_analysis_sample_tmp_dev.cfg;"
    run += "sed -i \"s/#KEY_SUBREGION/" + subregion + "/g\" 13TeV_analysis_sample_tmp_dev.cfg;"
    run += "sed -i \"s/#KEY_PROCESS/" + process + "/g\" 13TeV_analysis_sample_tmp_dev.cfg;"
    run += "sed -i \"s/#KEY_TYPE/" + Type + "/g\" 13TeV_analysis_sample_tmp_dev.cfg;"
    run += "sed -i \"s/#KEY_SIMULATION/" + simu + "/g\" 13TeV_analysis_sample_tmp_dev.cfg;"
    run += "sed -i \"s/#KEY_CUT/" + cut + "/g\" 13TeV_analysis_sample_tmp_dev.cfg;"
    run += "sed -i \"s/#KEY_QCDPARA//g\" 13TeV_analysis_sample_tmp_dev.cfg;"
    run += "sed -i \"s/#KEY_ADDTAG//g\" 13TeV_analysis_sample_tmp_dev.cfg;"
    run += "sed -i \"s/#KEY_SAVE/" + Save + "/g\" 13TeV_analysis_sample_tmp_dev.cfg;"
    #run += "sed -i \"s/#KEY_TEST/" + test + "/g\" 13TeV_analysis_sample_tmp_dev.cfg;"
    #run += "sed -i \"s/#KEY_DEBUG/" + debug + "/g\" 13TeV_analysis_sample_tmp_dev.cfg;"
    run += "cd /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/;"
    run += "../bin/analysissample_dev ../config/13TeV_analysis_sample_tmp_dev.cfg;"
    success = os.system(run)

    if success !=  0:
        break

    run = "rm ../../config/13TeV_analysis_sample_tmp_dev.cfg;"
    os.system(run)
