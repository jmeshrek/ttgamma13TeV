import os

ToCompare = [ 

	['ErrorBar DrawRatio IsSimu Is8TeV', 
	 'photon_Pt_[GeV]', 
	 'ATLAS_2017_I1604029/photon_pT_el PhPtTest_El_Truth_Signal_Nominal',
	 '/afs/cern.ch/work/y/yili/private/Sample/EventStore/truth/rivet.root /afs/cern.ch/work/y/yili/public/TTG_8TeV/run/results/Results_Signal_Truth_Test_test_2017.root',
	 'Rivet Analysis', 
	 'e+jets', 
	 'Rivet_Validation_PhPt_El', 
	 '0.95', '1.05'], 

	['ErrorBar DrawRatio IsSimu Is8TeV', 
	 'photon_Pt_[GeV]', 
	 'ATLAS_2017_I1604029/photon_pT_mu PhPtTest_Mu_Truth_Signal_Nominal',
	 '/afs/cern.ch/work/y/yili/private/Sample/EventStore/truth/rivet.root /afs/cern.ch/work/y/yili/public/TTG_8TeV/run/results/Results_Signal_Truth_Test_test_2017.root',
	 'Rivet Analysis', 
	 '#mu+jets', 
	 'Rivet_Validation_PhPt_Mu', 
	 '0.95', '1.05'], 

	['ErrorBar DrawRatio IsSimu Is8TeV', 
	 'photon_Eta', 
	 'ATLAS_2017_I1604029/photon_eta_el PhEtaTest_El_Truth_Signal_Nominal',
	 '/afs/cern.ch/work/y/yili/private/Sample/EventStore/truth/rivet.root /afs/cern.ch/work/y/yili/public/TTG_8TeV/run/results/Results_Signal_Truth_Test_test_2017.root',
	 'Rivet Analysis', 
	 'e+jets', 
	 'Rivet_Validation_PhEta_El', 
	 '0.95', '1.05'], 

	['ErrorBar DrawRatio IsSimu Is8TeV', 
	 'photon_Eta', 
	 'ATLAS_2017_I1604029/photon_eta_mu PhEtaTest_Mu_Truth_Signal_Nominal',
	 '/afs/cern.ch/work/y/yili/private/Sample/EventStore/truth/rivet.root /afs/cern.ch/work/y/yili/public/TTG_8TeV/run/results/Results_Signal_Truth_Test_test_2017.root',
	 'Rivet Analysis', 
	 '#mu+jets', 
	 'Rivet_Validation_PhEta_Mu', 
	 '0.95', '1.05'], 

	]

for tocompare in ToCompare:
    option = "_Options " 
    option += tocompare[0]
    title = "_XTitle "
    title += tocompare[1]
    hists = "_HistList "
    hists += tocompare[2]
    files = "_FileList "
    files += tocompare[3]
    lgs = "_Titles "
    lgs += tocompare[4]
    channel = "_Channel "
    channel += tocompare[5]
    save = "_Save "
    save += tocompare[6]
    ratiolo = "_RatioLo "
    ratiolo += tocompare[7]
    ratiohi = "_RatioHi "
    ratiohi += tocompare[8]
    
    run = "cd ../../config;"
    run += "echo \"" + option + "\" > 8TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + title + "\" >> 8TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + hists + "\" >> 8TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + files + "\" >> 8TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + lgs + "\" >> 8TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + channel + "\" >> 8TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + save + "\" >> 8TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + ratiolo + "\" >> 8TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + ratiohi + "\" >> 8TeV_compare_shape_tmp.cfg;"
    run += "cd ../run; ../bin/compareshapes ../config/8TeV_compare_shape_tmp.cfg;"
    success = os.system(run)

    if success !=  0:
        break

    run = "rm ../../config/8TeV_compare_shape_tmp.cfg;"
    os.system(run)
