import os

# v1 nominal

ToRun = [ 

	['QCD5', 'Para1', 'CR1', 'ejets', 'UPS'],
	['QCD5', 'Para1', 'CR1', 'mujets', 'UPS'],
	['QCD5', 'Para1', 'CR1', 'mujets', 'PS'],
	['QCD5', 'Para2', 'CR1', 'ejets', 'UPS'],
	['QCD5', 'Para2', 'CR1', 'mujets', 'UPS'],
	['QCD5', 'Para2', 'CR1', 'mujets', 'PS'],
	['QCD5', 'Para3', 'CR1', 'ejets', 'UPS'],
	['QCD5', 'Para3', 'CR1', 'mujets', 'UPS'],
	['QCD5', 'Para3', 'CR1', 'mujets', 'PS'],
	['QCD5', 'Para4', 'CR1', 'ejets', 'UPS'],
	['QCD5', 'Para4', 'CR1', 'mujets', 'UPS'],
	['QCD5', 'Para4', 'CR1', 'mujets', 'PS'],
	['QCD5', 'Para5', 'CR1', 'ejets', 'UPS'],
	['QCD5', 'Para5', 'CR1', 'mujets', 'UPS'],
	['QCD5', 'Para5', 'CR1', 'mujets', 'PS'],
	['QCD5', 'Para6', 'CR1', 'ejets', 'UPS'],
	['QCD5', 'Para6', 'CR1', 'mujets', 'UPS'],
	['QCD5', 'Para6', 'CR1', 'mujets', 'PS'],
	['QCD5', 'Para7', 'CR1', 'ejets', 'UPS'],
	['QCD5', 'Para7', 'CR1', 'mujets', 'UPS'],
	['QCD5', 'Para7', 'CR1', 'mujets', 'PS'],

	]

for torun in ToRun:
    qcdv = torun[0]
    para = torun[1]
    region = torun[2]
    subregion = torun[3]
    ps = torun[4]

    run = "cd /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/config/;"
    run += "cp 13TeV_draw_stack_qcd_proto.cfg 13TeV_draw_stack_qcd_tmp.cfg;"
    run += "sed -i \"s/#QCDV/" + qcdv + "/g\" 13TeV_draw_stack_qcd_tmp.cfg;"
    run += "sed -i \"s/#PARA/" + para + "/g\" 13TeV_draw_stack_qcd_tmp.cfg;"
    run += "sed -i \"s/#REGION/" + region + "/g\" 13TeV_draw_stack_qcd_tmp.cfg;"
    run += "sed -i \"s/#SUBREGION/" + subregion + "/g\" 13TeV_draw_stack_qcd_tmp.cfg;"
    run += "sed -i \"s/#PS/" + ps + "/g\" 13TeV_draw_stack_qcd_tmp.cfg;"
    run += "cd /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/;"
    run += "../bin/drawstack ../config/13TeV_draw_stack_qcd_tmp.cfg;"
    success = os.system(run)

    if success !=  0:
        break

    run = "rm ../../config/13TeV_draw_stack_qcd_tmp.cfg;"
    #os.system(run)
