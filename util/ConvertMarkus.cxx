#include <time.h>
#include "StringPlayer.h"
#include <TH2.h>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <TKey.h>
#include <TIterator.h>
#include <TH1.h>
#include <TLorentzVector.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TStopwatch.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <TFile.h>

using namespace std;

int main(int argc, char * argv[]) 
{
    string input;
    string output = "output";;
    int maxevt = -1;

    bool debug = false;
    for (int i = 0; i < argc; i++) {
	if (!strcmp(argv[i],"--input")) {
	    input = argv[i+1];
   	}
	if (!strcmp(argv[i],"--output")) {
	    output = argv[i+1];
   	}
	if (!strcmp(argv[i],"--debug")) {
	    debug = true;
   	}
    }

    ifstream ifile;
    ifile.open(input.c_str());

    char tmpline[10000];
    TH1F* h_phpt = new TH1F("phpt","phpt",60,0,300);
    TH1F* h_phpt_NLO = new TH1F("phpt_NLO","phpt_NLO",60,0,300);
    int nb = 0;
    while (ifile.getline(tmpline, 10000, '\n')) {
        string line = tmpline;
        if (line.find("#",0) != string::npos) continue;
        if (line.size() == 0) continue;
	if (debug) cout << line << endl;
        nb++;

        istringstream iss(line);
        vector<string> words;
        for (string word; iss >> word;) {
            words.push_back(word);
        }

	double bin_low = atof(words.at(0).c_str());

        double bin = atof(words.at(1).c_str());
        double bin_e = atof(words.at(2).c_str());
	if (bin_low == 10) {
	    bin = 0;
	    bin_e = 0;
	}
        h_phpt->SetBinContent(h_phpt->FindBin(bin_low), bin);
        h_phpt->SetBinError(h_phpt->FindBin(bin_low), bin_e);

	if (words.size() > 3) {
            double bin_NLO = atof(words.at(3).c_str());
            double bin_e_NLO = atof(words.at(4).c_str());
	    if (bin_low == 10) {
	        bin_NLO = 0;
	        bin_e_NLO = 0;
	    }
	    if (h_phpt->FindBin(bin_low) <= h_phpt_NLO->GetNbinsX()) {
		h_phpt_NLO->SetBinContent(h_phpt->FindBin(bin_low), bin_NLO);
            	h_phpt_NLO->SetBinError(h_phpt->FindBin(bin_low), bin_e_NLO);
	    } else {
		h_phpt_NLO->SetBinContent(h_phpt->GetNbinsX(), bin_NLO);
            	h_phpt_NLO->SetBinError(h_phpt->GetNbinsX(), bin_e_NLO);
	    }
	}
    }

    string savename = output + ".root";

    cout << "Int xsec: " << h_phpt->Integral() << endl;
    if (h_phpt_NLO->Integral() != 0) {
	cout << "NLO Int xsec: " << h_phpt_NLO->Integral() << endl;
    }
    cout << "Save hist to " << savename << endl;

    TFile* fout = new TFile(savename.c_str(),"recreate");
    h_phpt->Write();
    h_phpt_NLO->Write();
    fout->Close();
    delete fout;
}
