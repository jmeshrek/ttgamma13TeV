#include <TFile.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TPad.h>
#include <TLine.h>
#include <string>
#include <TLatex.h>
#include <TMath.h>
#include <vector>
#include <TGraphErrors.h>
#include <map>
#include <TIterator.h>
#include <TKey.h>
#include <TObject.h>
#include <THStack.h>

#include "AtlasStyle.h"
#include "PlotComparor.h"
#include "StringPlayer.h"
#include "ConfigReader.h"
#include "Logger.h"

using namespace std;

int main(int argc, char * argv[]) {
    Logger *lg = new Logger("MAIN");
    lg->Info("s", "Hi, this is the main programm~");

    string fname_13tev = "results/Nominal/Sig_Sys_ScalePdf.root";
    string fname_14tev_reco = "results/Nominal/Sig_Sys_Upgrade.root";
    string fname_14tev_truth = "results/Nominal/Sig_Sys_Upgrade3.root";

    string channel = "ll";
    string var = "ph_pt";
    string hname_reco = "h_"+var+"Nominal_"+channel+"_reco";
    string hname_truth = "h_"+var+"Nominal_"+channel+"_truth";

    TFile* f_13tev = new TFile(fname_13tev.c_str());
    TFile* f_14tev_reco = new TFile(fname_14tev_reco.c_str());
    TFile* f_14tev_truth = new TFile(fname_14tev_truth.c_str());

    TH1F* h_13tev_reco = (TH1F*)f_13tev->Get(hname_reco.c_str());
    TH1F* h_14tev_reco = (TH1F*)f_14tev_reco->Get(hname_reco.c_str());
    TH1F* h_13tev_truth = (TH1F*)f_13tev->Get(hname_truth.c_str());
    TH1F* h_14tev_truth = (TH1F*)f_14tev_truth->Get(hname_truth.c_str());

    PlotComparor* PC = new PlotComparor();
    PC->DrawRatio(true);
    PC->SetSaveDir("plots/Upgrade/");
    PC->NormToUnit(true);
    PC->SetChannel(channel);
    string drawoptions = "hist";
    drawoptions += " _e";
    PC->SetDrawOption(drawoptions.c_str());

    PC->ClearAlterHs();
    PC->SetBaseH(h_13tev_reco);
    PC->SetBaseHName("13TeV reco");
    PC->AddAlterH(h_14tev_reco);
    PC->AddAlterHName("14TeV reco");
    PC->AddAlterH(h_13tev_truth);
    PC->AddAlterHName("13TeV truth");
    PC->AddAlterH(h_14tev_truth);
    PC->AddAlterHName("14TeV truth");
    string savename = "CompareShape1314_"+var+"_"+channel;
    PC->SetSaveName(savename);
    PC->Is14TeV(true);
    PC->SetMaxRatio(1.5);
    PC->SetMinRatio(0.5);
    PC->SetXtitle(var);
    //PC->DataLumi(36.5);
    PC->Compare();
}
    
