
#include<TH1F.h>
#include<TH2F.h>
#include<TString.h>
#include<TGraphErrors.h>
#include<TPaveStats.h>
#include<TFile.h>
#include<TROOT.h>
#include<TStyle.h>
#include<RooRealVar.h>
#include<RooPlot.h>
#include<TF1.h>
#include<TCanvas.h>
#include <RooFit.h>
#include <RooRealVar.h>
#include <RooGaussian.h>
#include <RooCBShape.h>
#include <RooWorkspace.h>
#include <RooDataSet.h>
#include "RooPolynomial.h"
#include <RooRealVar.h>
#include <RooArgSet.h>
#include <RooWorkspace.h>
#include <RooDataHist.h>
#include <RooHist.h>
#include <RooHistPdf.h>
#include <RooProduct.h>
#include <RooCurve.h>
#include <RooAddPdf.h>
#include <RooAbsPdf.h>
#include <RooAddition.h>
#include <RooProdPdf.h>
#include <RooGaussModel.h>
#include <RooBinning.h>
#include <RooGenericPdf.h>
#include <RooExtendPdf.h>
#include <RooPoisson.h>
#include <RooLognormal.h>
#include <RooCategory.h>
#include <RooSimultaneous.h>

#include <RooStats/RooStatsUtils.h>
#include <RooStats/ModelConfig.h>
#include <RooStats/MCMCInterval.h>
#include <RooStats/MCMCIntervalPlot.h>

#include"Logger.h"
#include"PlotComparor.h"
#include "StringPlayer.h"
#include "Logger.h"
#include "ConfigReader.h"

using namespace std;
using namespace RooFit;
using namespace RooStats;

pair<double,double> FitPeak(TH1F*h, bool dbg = false);
void FitLiner(TH1F*h);
void DrawPeak(TH1F*h);

int main(int argc, char * argv[]) {

    TFile* f_typea_in = new TFile("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeA_Lumiweighted_V009.01.root");
    TFile* f_typea_in_reverse = new TFile("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZegReverse_PhMatch_EFakeTypeA_Lumiweighted_V009.01.root");
    TH1F* h_typea_1d = (TH1F*)f_typea_in->Get("LeadPhMMCPt_EF1_zeg_Reco_ZjetsElEl_Nominal"); h_typea_1d->SetName("CaliHist_TypeA_PtbinAll");
    TH2F* h_typea_2d = (TH2F*)f_typea_in->Get("LeadPhMMCPt_LeadPhPt_EF1_zeg_Reco_ZjetsElEl_Nominal");
    TH1F* h_typea_1d_reverse = (TH1F*)f_typea_in_reverse->Get("LeadPhMMCPt_EF1_zeg_Reco_ZjetsElEl_Nominal"); h_typea_1d_reverse->SetName("CaliHist_TypeA_PtbinAll_Reverse");
    TH2F* h_typea_2d_reverse = (TH2F*)f_typea_in_reverse->Get("LeadPhMMCPt_LeadPhPt_EF1_zeg_Reco_ZjetsElEl_Nominal");
    TFile* f_typeb_in = new TFile("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg_PhMatch_EFakeTypeB_Lumiweighted_V009.01.root");
    TFile* f_typeb_in_reverse = new TFile("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZegReverse_PhMatch_EFakeTypeB_Lumiweighted_V009.01.root");
    TH1F* h_typeb_1d = (TH1F*)f_typeb_in->Get("LeadPhMMCElPt_EF1_zeg_Reco_ZjetsElEl_Nominal"); h_typeb_1d->SetName("CaliHist_TypeB_PtbinAll");
    TH2F* h_typeb_2d = (TH2F*)f_typeb_in->Get("LeadPhMMCElPt_LeadPhPt_EF1_zeg_Reco_ZjetsElEl_Nominal");
    TH1F* h_typeb_1d_reverse = (TH1F*)f_typeb_in_reverse->Get("LeadPhMMCElPt_EF1_zeg_Reco_ZjetsElEl_Nominal"); h_typeb_1d_reverse->SetName("CaliHist_TypeB_PtbinAll_Reverse");
    TH2F* h_typeb_2d_reverse = (TH2F*)f_typeb_in_reverse->Get("LeadPhMMCElPt_LeadPhPt_EF1_zeg_Reco_ZjetsElEl_Nominal");

    TH1F* h_typeapb_1d = (TH1F*)h_typea_1d->Clone(); h_typeapb_1d->SetName("CaliHist_TypeApB_PtbinAll"); h_typeapb_1d->Add(h_typeb_1d);
    TH2F* h_typeapb_2d = (TH2F*)h_typea_2d->Clone(); h_typeapb_2d->Add(h_typeb_2d);
    TH1F* h_typeapb_1d_reverse = (TH1F*)h_typea_1d_reverse->Clone(); h_typeapb_1d_reverse->SetName("CaliHist_TypeApB_PtbinAll_Reverse"); h_typeapb_1d_reverse->Add(h_typeb_1d_reverse);
    TH2F* h_typeapb_2d_reverse = (TH2F*)h_typea_2d_reverse->Clone(); h_typeapb_2d_reverse->Add(h_typeb_2d_reverse);

    TH1F* h_typeapb_npr_1d = (TH1F*)h_typeapb_1d->Clone(); h_typeapb_npr_1d->SetName("CaliHist_TypeApB_NpR_PtbinAll"); h_typeapb_npr_1d->Add(h_typeapb_1d_reverse);
    TH2F* h_typeapb_npr_2d = (TH2F*)h_typeapb_2d->Clone(); h_typeapb_npr_2d->Add(h_typeapb_2d_reverse);

    vector<TH1F*> h_typea_1ds;
    vector<TH1F*> h_typea_1ds_reverse;
    vector<TH1F*> h_typeb_1ds;
    vector<TH1F*> h_typeb_1ds_reverse;
    vector<TH1F*> h_typeapb_1ds;
    vector<TH1F*> h_typeapb_1ds_reverse;
    vector<TH1F*> h_typeapb_npr_1ds;
    for (int i = 0; i < h_typea_2d->GetNbinsY(); i++) {
	char tmp[100];
	sprintf(tmp, "CaliHist_TypeA_Ptbin%d", i+1);
	TH1F* h_typea_tmp_1d = new TH1F(tmp,"", 80, -0.4, 0.4);
	sprintf(tmp, "CaliHist_TypeA_Ptbin%d_Reverse", i+1);
	TH1F* h_typea_tmp_1d_reverse = new TH1F(tmp,"", 80, -0.4, 0.4);
	sprintf(tmp, "CaliHist_TypeB_Ptbin%d", i+1);
	TH1F* h_typeb_tmp_1d = new TH1F(tmp,"", 80, -0.4, 0.4);
	sprintf(tmp, "CaliHist_TypeB_Ptbin%d_Reverse", i+1);
	TH1F* h_typeb_tmp_1d_reverse = new TH1F(tmp,"", 80, -0.4, 0.4);
	sprintf(tmp, "CaliHist_TypeApB_Ptbin%d", i+1);
	TH1F* h_typeapb_tmp_1d = new TH1F(tmp,"", 80, -0.4, 0.4);
	sprintf(tmp, "CaliHist_TypeApB_Ptbin%d_Reverse", i+1);
	TH1F* h_typeapb_tmp_1d_reverse = new TH1F(tmp,"", 80, -0.4, 0.4);
	sprintf(tmp, "CaliHist_TypeApB_NpR_Ptbin%d", i+1);
	TH1F* h_typeapb_npr_tmp_1d = new TH1F(tmp,"", 80, -0.4, 0.4);

	for (int j = 0; j < h_typea_2d->GetNbinsX(); j++) {
	    h_typea_tmp_1d->SetBinContent(j+1, h_typea_2d->GetBinContent(j+1, i+1));
	    h_typea_tmp_1d_reverse->SetBinContent(j+1, h_typea_2d_reverse->GetBinContent(j+1, i+1));
	}
	h_typea_1ds.push_back(h_typea_tmp_1d);
	h_typea_1ds_reverse.push_back(h_typea_tmp_1d_reverse);

	for (int j = 0; j < h_typeb_2d->GetNbinsX(); j++) {
	    h_typeb_tmp_1d->SetBinContent(j+1, h_typeb_2d->GetBinContent(j+1, i+1));
	    h_typeb_tmp_1d_reverse->SetBinContent(j+1, h_typeb_2d_reverse->GetBinContent(j+1, i+1));
	}
	h_typeb_1ds.push_back(h_typeb_tmp_1d);
	h_typeb_1ds_reverse.push_back(h_typeb_tmp_1d_reverse);

	for (int j = 0; j < h_typeapb_2d->GetNbinsX(); j++) {
	    h_typeapb_tmp_1d->SetBinContent(j+1, h_typeapb_2d->GetBinContent(j+1, i+1));
	    h_typeapb_tmp_1d_reverse->SetBinContent(j+1, h_typeapb_2d_reverse->GetBinContent(j+1, i+1));
	}
	h_typeapb_1ds.push_back(h_typeapb_tmp_1d);
	h_typeapb_1ds_reverse.push_back(h_typeapb_tmp_1d_reverse);

	for (int j = 0; j < h_typeapb_npr_2d->GetNbinsX(); j++) {
	    h_typeapb_npr_tmp_1d->SetBinContent(j+1, h_typeapb_npr_2d->GetBinContent(j+1, i+1));
	}
	h_typeapb_npr_1ds.push_back(h_typeapb_npr_tmp_1d);
    }

    TFile*fout = new TFile("results/EGammaCalibration.root", "recreate");
    TH1F*h_typea_out = new TH1F("Prefit_CaliHist_TypeA_Ptbin_Alpha","",11,20,75);
    TH1F*h_typea_out_reverse = new TH1F("Prefit_CaliHist_TypeA_Ptbin_Alpha_Reverse","",11,20,75);
    TH1F*h_typeb_out = new TH1F("Prefit_CaliHist_TypeB_Ptbin_Alpha","",11,20,75);
    TH1F*h_typeb_out_reverse = new TH1F("Prefit_CaliHist_TypeB_Ptbin_Alpha_Reverse","",11,20,75);
    TH1F*h_typeapb_out = new TH1F("Prefit_CaliHist_TypeApB_Ptbin_Alpha","",11,20,75);
    TH1F*h_typeapb_out_reverse = new TH1F("Prefit_CaliHist_TypeApB_Ptbin_Alpha_Reverse","",11,20,75);
    TH1F*h_typeapb_npr_out = new TH1F("Prefit_CaliHist_TypeApB_NpR_Ptbin_Alpha","",11,20,75);

    DrawPeak(h_typea_1d); FitPeak(h_typea_1d);
    DrawPeak(h_typea_1d_reverse); FitPeak(h_typea_1d_reverse);
    DrawPeak(h_typeb_1d); FitPeak(h_typeb_1d);
    DrawPeak(h_typeb_1d_reverse); FitPeak(h_typeb_1d_reverse);
    DrawPeak(h_typeapb_1d); FitPeak(h_typeapb_1d);
    DrawPeak(h_typeapb_1d_reverse); FitPeak(h_typeapb_1d_reverse);
    DrawPeak(h_typeapb_npr_1d); FitPeak(h_typeapb_npr_1d);
    for (int i = 0; i < h_typea_1ds.size(); i++) {
	fout->cd();
	h_typea_1ds.at(i)->Write();
	h_typea_1ds_reverse.at(i)->Write();
	DrawPeak(h_typea_1ds.at(i));
	DrawPeak(h_typea_1ds_reverse.at(i));
	pair<double,double> alpha = FitPeak(h_typea_1ds.at(i));
	h_typea_out->SetBinContent(i+1, alpha.first);
	h_typea_out->SetBinError(i+1, alpha.second);
	pair<double,double> alpha_reverse = FitPeak(h_typea_1ds_reverse.at(i));
	h_typea_out_reverse->SetBinContent(i+1, alpha_reverse.first);
	h_typea_out_reverse->SetBinError(i+1, alpha_reverse.second);
    }
    for (int i = 0; i < h_typeb_1ds.size(); i++) {
	fout->cd();
	h_typeb_1ds.at(i)->Write();
	h_typeb_1ds_reverse.at(i)->Write();
	DrawPeak(h_typeb_1ds.at(i));
	DrawPeak(h_typeb_1ds_reverse.at(i));
	pair<double,double> alpha = FitPeak(h_typeb_1ds.at(i));
	h_typeb_out->SetBinContent(i+1, alpha.first);
	h_typeb_out->SetBinError(i+1, alpha.second);
	pair<double,double> alpha_reverse = FitPeak(h_typeb_1ds_reverse.at(i));
	h_typeb_out_reverse->SetBinContent(i+1, alpha_reverse.first);
	h_typeb_out_reverse->SetBinError(i+1, alpha_reverse.second);
    }
    for (int i = 0; i < h_typeapb_1ds.size(); i++) {
	fout->cd();
	h_typeapb_1ds.at(i)->Write();
	h_typeapb_1ds_reverse.at(i)->Write();
	DrawPeak(h_typeapb_1ds.at(i));
	DrawPeak(h_typeapb_1ds_reverse.at(i));
	pair<double,double> alpha = FitPeak(h_typeapb_1ds.at(i));
	h_typeapb_out->SetBinContent(i+1, alpha.first);
	h_typeapb_out->SetBinError(i+1, alpha.second);
	pair<double,double> alpha_reverse = FitPeak(h_typeapb_1ds_reverse.at(i));
	h_typeapb_out_reverse->SetBinContent(i+1, alpha_reverse.first);
	h_typeapb_out_reverse->SetBinError(i+1, alpha_reverse.second);
    }
    for (int i = 0; i < h_typeapb_npr_1ds.size(); i++) {
	fout->cd();
	h_typeapb_npr_1ds.at(i)->Write();
	DrawPeak(h_typeapb_npr_1ds.at(i));
	pair<double,double> alpha = FitPeak(h_typeapb_npr_1ds.at(i));
	h_typeapb_npr_out->SetBinContent(i+1, alpha.first);
	h_typeapb_npr_out->SetBinError(i+1, alpha.second);
    }


    h_typea_out->Write();
    h_typea_out_reverse->Write();
    h_typeb_out->Write();
    h_typeb_out_reverse->Write();
    h_typeapb_out->Write();
    h_typeapb_out_reverse->Write();
    h_typeapb_npr_out->Write();
    FitLiner(h_typea_out);
    FitLiner(h_typea_out_reverse);
    FitLiner(h_typeb_out);
    FitLiner(h_typeb_out_reverse);
    FitLiner(h_typeapb_out);
    FitLiner(h_typeapb_out_reverse);
    FitLiner(h_typeapb_npr_out);
    h_typea_out->Write("Postfit_CaliHist_TypeA_Ptbin_Alpha");
    h_typea_out_reverse->Write("Postfit_CaliHist_TypeA_Ptbin_Alpha_Reverse");
    h_typeb_out->Write("Postfit_CaliHist_TypeB_Ptbin_Alpha");
    h_typeb_out_reverse->Write("Postfit_CaliHist_TypeB_Ptbin_Alpha_Reverse");
    h_typeapb_out->Write("Postfit_CaliHist_TypeApB_Ptbin_Alpha");
    h_typeapb_out_reverse->Write("Postfit_CaliHist_TypeApB_Ptbin_Alpha_Reverse");
    h_typeapb_npr_out->Write("Postfit_CaliHist_TypeApB_NpR_Ptbin_Alpha");

    fout->Close();
    delete fout;

    return 0;
}

void FitLiner(TH1F*h) {

   RooMsgService::instance().setSilentMode(kTRUE);
   RooMsgService::instance().setGlobalKillBelow(WARNING);

    //h = new TH1F("h","",11,20,75);
    //for (int i = 1; i <= 11; i++) {
    //    double val = 1+i*0.5;
    //    h->SetBinContent(i, val);
    //    h->SetName("Test");
    //}

//    RooRealVar x("x","x",20,75) ;
//    x.setBins(11);
//
//    x.setRange("R1",25,40) ;
//    x.setRange("R2",50,70) ;
//    x.setRange("R",25,70) ;
//
//    RooRealVar a0("a0","a0",-0.05,-10,10);
//    RooRealVar a1("a1","a1",0.005,0,10);
//    RooPolynomial p1("p1","p1",x,RooArgSet(a0,a1),0);
//
//    RooDataHist *data = new RooDataHist("data", "data", x, h);
//    p1.fitTo(*data,SumW2Error(kTRUE)) ;
//    //p1.fitTo(*data,Range("R1","R2")) ;
//
//    TCanvas *tmpc = new TCanvas("tmp", "tmp", 800, 600);
//    RooPlot* xframe = x.frame(Title("example")) ;
//    data->plotOn(xframe) ;
//    p1.plotOn(xframe,Range("R")) ;

    int n = 7;
    double x[7],y[7];
    double dx[7],dy[7];
    int idx = 0;
    for (int i = 1; i <= h->GetNbinsX(); i++) {
	if ((i >= 2 && i <= 4) || (i >= 7 && i <= 10)) {
	    x[idx] = h->GetXaxis()->GetBinLowEdge(i) + h->GetXaxis()->GetBinWidth(i)/2;
	    dx[idx] = h->GetXaxis()->GetBinWidth(i)/2;
	    y[idx] = h->GetBinContent(i);
	    dy[idx] = h->GetBinError(i);
	    idx++;
	}
    }
   TGraphErrors* gr = new TGraphErrors(n,x,y,dx,dy);

   TF1 *f1 = new TF1("f1", "[0]*x + [1]", 20., 75.);
   f1->SetParameters(0,0.05);
   f1->SetParameters(1,-0.1);
   gr->Fit("f1");
   TCanvas *tmpc = new TCanvas("tmp", "tmp", 800, 600);
    h->GetXaxis()->SetTitle("Pt");
    h->GetYaxis()->SetTitle("Cali.");
    h->Draw();
    gr->Draw("same");
    f1->Draw("same");

//    xframe->Draw();
//
    TString save = "plots/Postfit_";
    save += h->GetName();
    save += ".png";
    tmpc->SaveAs(save);
    TString save2 = "plots/Postfit_";
    save2 += h->GetName();
    save2 += ".pdf";
    tmpc->SaveAs(save2);

    for (int i = 1; i <= h->GetNbinsX(); i++) {
	double cen = h->GetXaxis()->GetBinLowEdge(i) + h->GetXaxis()->GetBinWidth(i)/2;
	h->SetBinContent(i, f1->Eval(cen));
    }
}

pair<double,double> FitPeak(TH1F*h, bool dbg) {

   RooMsgService::instance().setSilentMode(kTRUE);
   RooMsgService::instance().setGlobalKillBelow(WARNING);

    if (h->Integral() == 0) {
	return pair<double,double> (0,0);
    }

    double x_lo = -0.4;
    double x_hi = 0.4;

    RooRealVar x("x","x",x_lo,x_hi) ;
    x.setBins(80);

    double mean_cen = -0.04;
    double sigma_cen = 0.1;

    if (!dbg) {
	RooRealVar* mean = new RooRealVar("mean","mean of gaussians",mean_cen,x_lo,x_hi) ;
    	RooRealVar* sigma = new RooRealVar("sigma","width of gaussians",sigma_cen,0,1) ;
    	RooGaussian *gaus = new RooGaussian("gaus","Gaussian Function", x, *mean, *sigma);

    	RooDataHist *data = new RooDataHist("data", "data", x, h);
    	gaus->fitTo(*data,SumW2Error(kTRUE)) ;

    	TCanvas *tmpc = new TCanvas("tmp", "tmp", 800, 600);
    	RooPlot* xframe = x.frame(Title("example")) ;
    	data->plotOn(xframe) ;
    	gaus->plotOn(xframe) ;

    	xframe->Draw();

    	TString save = "plots/Postfit_";
    	save += h->GetName();
    	save += ".png";
    	tmpc->SaveAs(save);
    	TString save2 = "plots/Postfit_";
    	save2 += h->GetName();
    	save2 += ".pdf";
    	tmpc->SaveAs(save2);

    	return pair<double,double> (mean->getVal(), mean->getError());
    } else {
	RooRealVar* mean = new RooRealVar("mean","mean of gaussians",mean_cen,x_lo,x_hi) ;
    	RooRealVar* sigma1 = new RooRealVar("sigma1","width of gaussians",sigma_cen,0,1) ;
    	RooGaussian *gaus1 = new RooGaussian("gaus1","Gaussian Function", x, *mean, *sigma1);
    	RooRealVar* sigma2 = new RooRealVar("sigma2","width of gaussians",sigma_cen,0,1) ;
    	RooGaussian *gaus2 = new RooGaussian("gaus2","Gaussian Function", x, *mean, *sigma2);
	RooRealVar* fr = new RooRealVar("fr", "fr", 0.5, 0, 1);
	RooAddPdf* model = new RooAddPdf("model","", *gaus1, *gaus2, *fr);

    	RooDataHist *data = new RooDataHist("data", "data", x, h);
    	model->fitTo(*data,SumW2Error(kTRUE)) ;

    	TCanvas *tmpc = new TCanvas("tmp", "tmp", 800, 600);
    	RooPlot* xframe = x.frame(Title("example")) ;
    	data->plotOn(xframe) ;
    	model->plotOn(xframe) ;
	model->plotOn(xframe,Components(*gaus1),LineStyle(kDashed)) ;
	model->plotOn(xframe,Components(*gaus2),LineStyle(kDotted)) ;

    	xframe->Draw();

    	TString save = "plots/Postfit_DB_";
    	save += h->GetName();
    	save += ".png";
    	tmpc->SaveAs(save);
    	TString save2 = "plots/Postfit_DB_";
    	save2 += h->GetName();
    	save2 += ".pdf";
    	tmpc->SaveAs(save2);

    	return pair<double,double> (mean->getVal(), mean->getError());
    }
}

void DrawPeak(TH1F*h) {

    TCanvas *tmpc = new TCanvas("tmp", "tmp", 800, 600);
    h->Draw("e");
    TString save = "plots/Prefit_";
    save += h->GetName();
    save += ".png";
    tmpc->SaveAs(save);

    return;
}
