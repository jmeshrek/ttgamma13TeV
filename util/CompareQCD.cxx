{
    {
    TString ps = "UPS";
    TString chan = "ejets";
    vector<TString> names;
    names.push_back("pt");
    names.push_back("eta");
    names.push_back("dR");
    names.push_back("jetpt");
    names.push_back("dPhi");
    names.push_back("nbtag");
    names.push_back("mtw");
    cout << setw(20) << " ";
    for (int i = 0; i < names.size(); i++) {
	cout << " &" << setw(20) << names.at(i);
    }
    cout << " &" << setw(20) << "Aver.+Enve.";
    cout << "\\\\" << endl;

    vector<TString> files;
    files.push_back("results/Results_QCD_13TeV_CR1_"+chan+"_CutSR_Para1_"+ps+"_V007.01.root");
    files.push_back("results/Results_QCD_13TeV_CR1_"+chan+"_CutSR_Para2_"+ps+"_V007.01.root");
    files.push_back("results/Results_QCD_13TeV_CR1_"+chan+"_CutSR_Para3_"+ps+"_V007.01.root");
    files.push_back("results/Results_QCD_13TeV_CR1_"+chan+"_CutSR_Para4_"+ps+"_V007.01.root");
    files.push_back("results/Results_QCD_13TeV_CR1_"+chan+"_CutSR_Para5_"+ps+"_V007.01.root");
    files.push_back("results/Results_QCD_13TeV_CR1_"+chan+"_CutSR_Para6_"+ps+"_V007.01.root");
    files.push_back("results/Results_QCD_13TeV_CR1_"+chan+"_CutSR_Para7_"+ps+"_V007.01.root");

    cout << fixed << setprecision(1);
    cout << setw(20) << "ej/UPS";
    double n_ej_UPS_av = 0;
    double n_ej_UPS_max = 0;
    double n_ej_UPS_min = 999999;
    for (int i = 0; i < files.size(); i++) {
	TFile* f = new TFile(files.at(i));
	TH1F* h = (TH1F*)f->Get("Cutflow_CR1_"+chan+"_QCD_Nominal");
	double n = 0;
	double e = 0;
	for (int j = 0; j < h->GetNbinsX(); j++) {
	    //cout << h->GetXaxis()->GetBinLabel(j+1) << " " << h->GetBinContent(j+1) << endl;
	    if (TString(h->GetXaxis()->GetBinLabel(j+1)) == "Cut:DrPhLep1.0") {
		n = h->GetBinContent(j+1);
		e = h->GetBinError(j+1);
	    }
	}
	n_ej_UPS_av += n;
	if (n < n_ej_UPS_min) n_ej_UPS_min = n;
	if (n > n_ej_UPS_max) n_ej_UPS_max = n;
	//cout << files.at(i) << " " << n << " " << e << endl;
	cout << " &" << setw(10) << n << "$\\pm$" << setw(5) << e;
    }
    n_ej_UPS_av /= files.size();
    n_ej_UPS_av = (n_ej_UPS_min + n_ej_UPS_max)/2;
    double e_ej_UPS_err = (n_ej_UPS_max - n_ej_UPS_min)/2;
    cout << " &" << setw(10) << n_ej_UPS_av << "$\\pm$" << setw(5) << e_ej_UPS_err;
    cout << "\\\\" << endl;
    cout << endl;
    }

    double n_mu_av = 0;
    double n_mu_max = 0;
    double n_mu_min = 999999;
    {
    TString ps = "UPS";
    TString chan = "mujets";
    vector<TString> names;
    names.push_back("pt");
    names.push_back("eta");
    names.push_back("dR");
    names.push_back("jetpt");
    names.push_back("dPhi");
    names.push_back("nbtag");
    names.push_back("mtw");
    cout << setw(20) << " ";
    for (int i = 0; i < names.size(); i++) {
	cout << " &" << setw(20) << names.at(i);
    }
    cout << " &" << setw(20) << "Aver.+Enve.";
    cout << "\\\\" << endl;

    vector<TString> files;
    files.push_back("results/Results_QCD_13TeV_CR1_"+chan+"_CutSR_Para1_"+ps+"_V007.01.root");
    files.push_back("results/Results_QCD_13TeV_CR1_"+chan+"_CutSR_Para2_"+ps+"_V007.01.root");
    files.push_back("results/Results_QCD_13TeV_CR1_"+chan+"_CutSR_Para3_"+ps+"_V007.01.root");
    files.push_back("results/Results_QCD_13TeV_CR1_"+chan+"_CutSR_Para4_"+ps+"_V007.01.root");
    files.push_back("results/Results_QCD_13TeV_CR1_"+chan+"_CutSR_Para5_"+ps+"_V007.01.root");
    files.push_back("results/Results_QCD_13TeV_CR1_"+chan+"_CutSR_Para6_"+ps+"_V007.01.root");
    files.push_back("results/Results_QCD_13TeV_CR1_"+chan+"_CutSR_Para7_"+ps+"_V007.01.root");

    cout << fixed << setprecision(1);
    cout << setw(20) << "mj/UPS";
    double n_mu_UPS_av = 0;
    double n_mu_UPS_max = 0;
    double n_mu_UPS_min = 999999;
    for (int i = 0; i < files.size(); i++) {
	TFile* f = new TFile(files.at(i));
	TH1F* h = (TH1F*)f->Get("Cutflow_CR1_"+chan+"_QCD_Nominal");
	double n = 0;
	double e = 0;
	for (int j = 0; j < h->GetNbinsX(); j++) {
	    //cout << h->GetXaxis()->GetBinLabel(j+1) << " " << h->GetBinContent(j+1) << endl;
	    if (TString(h->GetXaxis()->GetBinLabel(j+1)) == "Cut:DrPhLep1.0") {
		n = h->GetBinContent(j+1);
		e = h->GetBinError(j+1);
	    }
	}
	n_mu_UPS_av += n;
	if (n < n_mu_UPS_min) n_mu_UPS_min = n;
	if (n > n_mu_UPS_max) n_mu_UPS_max = n;
	n_mu_av += n;
	if (n < n_mu_min) n_mu_min = n;
	if (n > n_mu_max) n_mu_max = n;
	//cout << files.at(i) << " " << n << " " << e << endl;
	cout << " &" << setw(10) << n << "$\\pm$" << setw(5) << e;
    }
    n_mu_UPS_av /= files.size();
    n_mu_UPS_av = (n_mu_UPS_min + n_mu_UPS_max)/2;
    double e_mu_UPS_err = (n_mu_UPS_max - n_mu_UPS_min)/2;
    cout << " &" << setw(10) << n_mu_UPS_av << "$\\pm$" << setw(5) << e_mu_UPS_err;
    cout << "\\\\" << endl;
    cout << endl;
    }

    {
    TString ps = "PS";
    TString chan = "mujets";
    vector<TString> names;
    names.push_back("pt");
    names.push_back("eta");
    names.push_back("dR");
    names.push_back("jetpt");
    names.push_back("dPhi");
    names.push_back("nbtag");
    names.push_back("mtw");
    cout << setw(20) << " ";
    for (int i = 0; i < names.size(); i++) {
	cout << " &" << setw(20) << names.at(i);
    }
    cout << " &" << setw(20) << "Aver.+Enve.";
    cout << "\\\\" << endl;

    vector<TString> files;
    files.push_back("results/Results_QCD_13TeV_CR1_"+chan+"_CutSR_Para1_"+ps+"_V007.01.root");
    files.push_back("results/Results_QCD_13TeV_CR1_"+chan+"_CutSR_Para2_"+ps+"_V007.01.root");
    files.push_back("results/Results_QCD_13TeV_CR1_"+chan+"_CutSR_Para3_"+ps+"_V007.01.root");
    files.push_back("results/Results_QCD_13TeV_CR1_"+chan+"_CutSR_Para4_"+ps+"_V007.01.root");
    files.push_back("results/Results_QCD_13TeV_CR1_"+chan+"_CutSR_Para5_"+ps+"_V007.01.root");
    files.push_back("results/Results_QCD_13TeV_CR1_"+chan+"_CutSR_Para6_"+ps+"_V007.01.root");
    files.push_back("results/Results_QCD_13TeV_CR1_"+chan+"_CutSR_Para7_"+ps+"_V007.01.root");

    cout << fixed << setprecision(1);
    cout << setw(20) << "mj/PS";
    double n_mu_PS_av = 0;
    double n_mu_PS_max = 0;
    double n_mu_PS_min = 999999;
    for (int i = 0; i < files.size(); i++) {
	TFile* f = new TFile(files.at(i));
	TH1F* h = (TH1F*)f->Get("Cutflow_CR1_"+chan+"_QCD_Nominal");
	double n = 0;
	double e = 0;
	for (int j = 0; j < h->GetNbinsX(); j++) {
	    //cout << h->GetXaxis()->GetBinLabel(j+1) << " " << h->GetBinContent(j+1) << endl;
	    if (TString(h->GetXaxis()->GetBinLabel(j+1)) == "Cut:DrPhLep1.0") {
		n = h->GetBinContent(j+1);
		e = h->GetBinError(j+1);
	    }
	}
	n_mu_PS_av += n;
	if (n < n_mu_PS_min) n_mu_PS_min = n;
	if (n > n_mu_PS_max) n_mu_PS_max = n;
	n_mu_av += n;
	if (n < n_mu_min) n_mu_min = n;
	if (n > n_mu_max) n_mu_max = n;
	//cout << files.at(i) << " " << n << " " << e << endl;
	cout << " &" << setw(10) << n << "$\\pm$" << setw(5) << e;
    }
    n_mu_PS_av /= files.size();
    n_mu_PS_av = (n_mu_PS_min + n_mu_PS_max)/2;
    double e_mu_PS_err = (n_mu_PS_max - n_mu_PS_min)/2;
    cout << " &" << setw(10) << n_mu_PS_av << "$\\pm$" << setw(5) << e_mu_PS_err;
    cout << "\\\\" << endl;
    cout << endl;

    n_mu_av /= (2*files.size());
    double e_mu_err = (n_mu_max - n_mu_min)/2;
    cout << n_mu_av << " " << e_mu_err << endl;
    }
}
