#include <TFile.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TH2F.h>
#include <TH1F.h>

#include <RooChi2Var.h>
#include <RooMinimizer.h>
#include <RooPlot.h>
#include <RooFitResult.h>
#include <RooArgList.h>
#include <RooFormulaVar.h>
#include <RooFit.h>
#include <RooRealVar.h>
#include <RooGaussian.h>
#include <RooWorkspace.h>
#include <RooDataSet.h>
#include <RooRealVar.h>
#include <RooArgSet.h>
#include <RooWorkspace.h>
#include <RooDataHist.h>
#include <RooHist.h>
#include <RooHistPdf.h>
#include <RooProduct.h>
#include <RooAddPdf.h>
#include <RooAbsPdf.h>
#include <RooAddition.h>
#include <RooProdPdf.h>
#include <RooGaussModel.h>
#include <RooBinning.h>
#include <RooGenericPdf.h>
#include <RooExtendPdf.h>
#include <RooPoisson.h>
#include <RooLognormal.h>
#include <RooCategory.h>
#include <RooSimultaneous.h>

#include <RooStats/RooStatsUtils.h>
#include <RooStats/ModelConfig.h>
#include <RooStats/MCMCInterval.h>
#include <RooStats/MCMCIntervalPlot.h>

#include <iostream>
#include <sstream>
#include <string>
#include <iostream>
#include <iomanip>
#include <string>
#include <libgen.h>
#include <stdio.h>      
#include <stdlib.h> 

#include "PlotComparor.h"

using namespace RooFit;
using namespace RooStats;
using namespace std;

TH1F* h_fr_prod;

bool Compare(TString channel, TString var, TString opt, double &n_C, double &e_C, int nrebin = -1);
RooArgSet set_data_comb_ctw;
RooArgSet set_data_comb_ctg;
RooArgSet set_data_comb_ctb;
RooArgSet set_pdf_comb_ctw; 
RooArgSet set_pdf_comb_ctg; 
RooArgSet set_pdf_comb_ctb; 
bool eftall = false; // consider same amount of eft effects for radiative decay

double lumiscale = 1;

int main(int argc, char * argv[]) {

    for (int i = 0; i < argc; i++) {
	if (!strcmp(argv[i],"--eftall")) {
	    eftall = true;
   	}
	if (!strcmp(argv[i],"--lumiscale")) {
	    lumiscale = atof(string(argv[i+1]).c_str());
   	}
    }

    // 1) assign dummy sys
    // 2) eftall true/false
    // 3) add MC stat sys
    // 4) channel combined fit
    // 5) plots

    // the fraction of radiative production
    h_fr_prod = new TH1F("fr_prod", "fr_prod", 8, 0, 200);
    h_fr_prod->SetBinContent(1,0.3);
    h_fr_prod->SetBinContent(2,0.35);
    h_fr_prod->SetBinContent(3,0.5);
    h_fr_prod->SetBinContent(4,0.65);
    h_fr_prod->SetBinContent(5,0.76);
    h_fr_prod->SetBinContent(6,0.84);
    h_fr_prod->SetBinContent(7,0.89);
    h_fr_prod->SetBinContent(8,0.93);

    RooMsgService::instance().setSilentMode(kTRUE);
    RooMsgService::instance().setGlobalKillBelow(WARNING);
    
    double n_C_var_para_sl[2][3] = {};
    double e_C_var_para_sl[2][3] = {}; 
    double n_C_var_para_ll[2][3] = {};
    double e_C_var_para_ll[2][3] = {}; 
    {
    Compare("sinlepton", "ph_pt", "ctw", n_C_var_para_sl[0][0], e_C_var_para_sl[0][0], 27);
    Compare("sinlepton", "ph_pt", "ctg", n_C_var_para_sl[0][1], e_C_var_para_sl[0][1], 27);
    Compare("sinlepton", "ph_pt", "ctb", n_C_var_para_sl[0][2], e_C_var_para_sl[0][2], 27);
    }
    {
    Compare("dilepton", "ph_pt", "ctw", n_C_var_para_ll[0][0], e_C_var_para_ll[0][0], 27);
    Compare("dilepton", "ph_pt", "ctg", n_C_var_para_ll[0][1], e_C_var_para_ll[0][1], 27);
    Compare("dilepton", "ph_pt", "ctb", n_C_var_para_ll[0][2], e_C_var_para_ll[0][2], 27);
    }
    cout << setprecision(2) << endl;
    cout << setw(15) << "ph_pt" << " & " << setw(30) << "ctw" << " & " << setw(30) << "ctg" << " & " << setw(30) << "ctb" << " \\\\" << endl;
    cout << setw(15) << "sinlepton" << " & " << setw(15) << n_C_var_para_sl[0][0] << " +/- " << setw(10) << e_C_var_para_sl[0][0]
                                     << " & " << setw(15) << n_C_var_para_sl[0][1] << " +/- " << setw(10) << e_C_var_para_sl[0][1]
        			     << " & " << setw(15) << n_C_var_para_sl[0][2] << " +/- " << setw(10) << e_C_var_para_sl[0][2]
        			     << " \\\\" << endl;
    cout << setw(15) << "dilepton" << " & " << setw(15) << n_C_var_para_ll[0][0] << " +/- " << setw(10) << e_C_var_para_ll[0][0]
                                     << " & " << setw(15) << n_C_var_para_ll[0][1] << " +/- " << setw(10) << e_C_var_para_ll[0][1]
        			     << " & " << setw(15) << n_C_var_para_ll[0][2] << " +/- " << setw(10) << e_C_var_para_ll[0][2]
        			     << " \\\\" << endl;

    //{
    //double n_C_var_para[2][3] = {};
    //double e_C_var_para[2][3] = {}; 
    //Compare("dilepton", "ph_pt", "ctw", n_C_var_para[0][0], e_C_var_para[0][0], 12);
    //Compare("dilepton", "ph_pt", "ctg", n_C_var_para[0][1], e_C_var_para[0][1], 12);
    //Compare("dilepton", "ph_pt", "ctb", n_C_var_para[0][2], e_C_var_para[0][2], 12);
    //cout << setw(15) << "ph_pt" << " & " << setw(30) << "ctw" << " & " << setw(30) << "ctg" << " & " << setw(30) << "ctb" << " \\\\" << endl;
    //cout << setw(15) << "dilepton" << " & " << setw(15) << n_C_var_para[0][0] << " +/- " << setw(10) << e_C_var_para[0][0]
    //                                 << " & " << setw(15) << n_C_var_para[0][1] << " +/- " << setw(10) << e_C_var_para[0][1]
    //    			     << " & " << setw(15) << n_C_var_para[0][2] << " +/- " << setw(10) << e_C_var_para[0][2]
    //    			     << " \\\\" << endl;
    //}
    
    //for (int i = 1; i <= 50; i++) {
    //    double n_C_var_para[2][3] = {};
    //	double e_C_var_para[2][3] = {};

    //	if (!Compare("sinlepton", "ph_pt", "ctw", n_C_var_para[0][0], e_C_var_para[0][0], i)) break;
    //	Compare("sinlepton", "ph_pt", "ctg", n_C_var_para[0][1], e_C_var_para[0][1], i);
    //	Compare("sinlepton", "ph_pt", "ctb", n_C_var_para[0][2], e_C_var_para[0][2], i);
    //	cout << "rebin " << i << ": " << setw(15) << "ph_pt" << " & " << setw(15) << "ctw" << " & " << setw(15) << "ctg" << " & " << setw(15) << "ctb" << " \\\\" << endl;
    //	cout << "rebin " << i << ": " << setw(15) << "singlepton" << " & " << setw(15) << e_C_var_para[0][0]
    //	                                 << " & " << setw(15) << e_C_var_para[0][1]
    //	    			     << " & " << setw(15) << e_C_var_para[0][2]
    //	    			     << " \\\\" << endl;
    //}

//    for (int i = 1; i <= 50; i++) {
//	double n_C_var_para[2][3] = {};
//    	double e_C_var_para[2][3] = {};
//
//    	if (!Compare("dilepton", "ph_pt", "ctw", n_C_var_para[0][0], e_C_var_para[0][0], i)) break;
//    	Compare("dilepton", "ph_pt", "ctg", n_C_var_para[0][1], e_C_var_para[0][1], i);
//    	Compare("dilepton", "ph_pt", "ctb", n_C_var_para[0][2], e_C_var_para[0][2], i);
//    	cout << "rebin " << i << ": " << setw(15) << "ph_pt" << " & " << setw(15) << "ctw" << " & " << setw(15) << "ctg" << " & " << setw(15) << "ctb" << " \\\\" << endl;
//    	cout << "rebin " << i << ": " << setw(15) << "dilepton" << " & " << setw(15)   << e_C_var_para[0][0]
//    	                                 << " & " << setw(15) << e_C_var_para[0][1]
//    	    			     << " & " << setw(15) << e_C_var_para[0][2]
//    	    			     << " \\\\" << endl;
//    }

    	//RooProdPdf *model_comb_ctw = new RooProdPdf("model_comb_ctw", "model_comb_ctw", set_pdf_comb_ctw);
    	//RooProdPdf *model_comb_ctg = new RooProdPdf("model_comb_ctg", "model_comb_ctg", set_pdf_comb_ctg);
    	//RooProdPdf *model_comb_ctb = new RooProdPdf("model_comb_ctb", "model_comb_ctb", set_pdf_comb_ctb);
    	//RooDataSet *datainc_comb_ctw = new RooDataSet("datainc_comb_ctw", "datainc_comb_ctw", set_data_comb_ctw);
    	//RooDataSet *datainc_comb_ctg = new RooDataSet("datainc_comb_ctg", "datainc_comb_ctg", set_data_comb_ctg);
    	//RooDataSet *datainc_comb_ctb = new RooDataSet("datainc_comb_ctb", "datainc_comb_ctb", set_data_comb_ctb);
    	//datainc_comb_ctw->add(set_data_comb_ctw);
    	//datainc_comb_ctg->add(set_data_comb_ctg);
    	//datainc_comb_ctb->add(set_data_comb_ctb);
    	//RooFitResult *resultsinc_comb_ctw = model_comb_ctw->fitTo(*datainc_comb_ctw, RooFit::Save());
    	//RooFitResult *resultsinc_comb_ctg = model_comb_ctg->fitTo(*datainc_comb_ctg, RooFit::Save());
    	//RooFitResult *resultsinc_comb_ctb = model_comb_ctb->fitTo(*datainc_comb_ctb, RooFit::Save());
    	//RooArgList paras_comb_ctw = resultsinc_comb_ctw->floatParsFinal();
    	//RooArgList paras_comb_ctg = resultsinc_comb_ctg->floatParsFinal();
    	//RooArgList paras_comb_ctb = resultsinc_comb_ctb->floatParsFinal();

    	//e_C_var_para_chan[0][0][2] = ((RooRealVar*)paras_comb_ctw.at(0))->getError();
    	//e_C_var_para_chan[0][1][2] = ((RooRealVar*)paras_comb_ctg.at(0))->getError();
    	//e_C_var_para_chan[0][2][2] = ((RooRealVar*)paras_comb_ctb.at(0))->getError();

    	//Compare("singlepton", "mLeadJet_ph", "ctw", n_C_var_para_chan[1][0][0], e_C_var_para_chan[1][0][0]);
    	//Compare("singlepton", "mLeadJet_ph", "ctg", n_C_var_para_chan[1][1][0], e_C_var_para_chan[1][1][0]);
    	//Compare("singlepton", "mLeadJet_ph", "ctb", n_C_var_para_chan[1][2][0], e_C_var_para_chan[1][2][0]);
    	//Compare("dilepton", "mLeadJet_ph", "ctw", n_C_var_para_chan[1][0][1], e_C_var_para_chan[1][0][1]);
    	//Compare("dilepton", "mLeadJet_ph", "ctg", n_C_var_para_chan[1][1][1], e_C_var_para_chan[1][1][1]);
    	//Compare("dilepton", "mLeadJet_ph", "ctb", n_C_var_para_chan[1][2][1], e_C_var_para_chan[1][2][1]);

//    	cout << "rebin " << i << ": " << setw(15) << "ph_pt" << " & " << setw(15) << "ctw" << " & " << setw(15) << "ctg" << " & " << setw(15) << "ctb" << " \\\\" << endl;
//    	cout << "rebin " << i << ": " << setw(15) << "singlepton" << " & " << setw(15) << e_C_var_para_chan[0][0][0] 
//    	                                 << " & " << setw(15) << e_C_var_para_chan[0][1][0]
//    	    			     << " & " << setw(15) << e_C_var_para_chan[0][2][0] 
//    	    			     << " \\\\" << endl;
//    	cout << "rebin " << i << ": " << setw(15) << "dilepton" << " & " << setw(15)   << e_C_var_para_chan[0][0][1] 
//    	                                 << " & " << setw(15) << e_C_var_para_chan[0][1][1]
//    	    			     << " & " << setw(15) << e_C_var_para_chan[0][2][1] 
//    	    			     << " \\\\" << endl;
//    	cout << "rebin " << i << ": " << setw(15) << "combined" << " & " << setw(15)   << e_C_var_para_chan[0][0][2] 
//    	                                 << " & " << setw(15) << e_C_var_para_chan[0][1][2]
//    	    			     << " & " << setw(15) << e_C_var_para_chan[0][2][2] 
//    	    			     << " \\\\" << endl;
//  	  cout << setw(15) << "mLeadJet_ph" << " & " << setw(15) << "ctw" << " & " << setw(15) << "ctg" << " & " << setw(15) << "ctb" << " \\\\" << endl;
//  	  cout << setw(15) << "singlepton" << " & " << setw(15) << e_C_var_para_chan[1][0][0] 
//  	                                   << " & " << setw(15) << e_C_var_para_chan[1][1][0]
//  	    			     << " & " << setw(15) << e_C_var_para_chan[1][2][0] 
//  	    			     << " \\\\" << endl;
//  	  cout << setw(15) << "mLeadJet_ph" << " & " << setw(15) << "ctw" << " & " << setw(15) << "ctg" << " & " << setw(15) << "ctb" << " \\\\" << endl;
//  	  cout << setw(15) << "dilepton" << " & " << setw(15)   << e_C_var_para_chan[1][0][1] 
//  	                                   << " & " << setw(15) << e_C_var_para_chan[1][1][1]
//  	    			     << " & " << setw(15) << e_C_var_para_chan[1][2][1] 
//  	    			     << " \\\\" << endl;
}

bool Compare(TString channel, TString var, TString opt, double &n_C, double &e_C, int nrebin) {
    double Copt = 1;

    TFile* f1 = new TFile("../data/ttgamma_unfolding_pseudoAsimov_" + channel + ".root");
    TH1F* h_truth = (TH1F*)f1->Get("truth_weighted_" + var);
    TH1F* h_unfolded_tmp = (TH1F*)f1->Get("unfolded_pseudo_" + var);
    double unfolded_tot_err = 0;
    for (int i = 1; i <= h_unfolded_tmp->GetNbinsX(); i++) {
	unfolded_tot_err += pow(h_unfolded_tmp->GetBinError(i),2);
    }
    unfolded_tot_err = sqrt(unfolded_tot_err);

    if (!h_truth) f1->ls();
    if (!h_unfolded_tmp) f1->ls();

    TH1F* h_unfolded = (TH1F*)h_truth->Clone();
    h_unfolded->SetName(h_unfolded_tmp->GetName());
    for (int i = 1; i <= h_unfolded_tmp->GetNbinsX(); i++) {
	h_unfolded->SetBinError(i, h_unfolded->GetBinContent(i)*h_unfolded_tmp->GetBinError(i)/h_unfolded_tmp->GetBinContent(i)/sqrt(lumiscale));
    }
    
    TFile* f2 = new TFile("../data/ttgamma_EFT_fullStat_" + channel + ".root");
    TH1F* h_SM = (TH1F*)f2->Get("sigma_sm_" + opt + "_" + var); 
    TH1F* h_sigma1 = (TH1F*)f2->Get("sigma_1_" + opt + "_" + var); 
    TH1F* h_sigma2 = (TH1F*)f2->Get("sigma_2_" + opt + "_" + var); 

    if (!h_SM) f2->ls();
    if (!h_sigma1) f2->ls();
    if (!h_sigma2) f2->ls();

    int nb = h_truth->GetNbinsX();
    double x_lo = h_truth->GetBinLowEdge(1);
    double x_hi = h_truth->GetBinLowEdge(nb) + h_truth->GetBinWidth(nb);

    // rebinning
    char tmpnbin[50];
    if (nrebin != -1) {
	sprintf(tmpnbin, "Bin%d", nrebin);
	if (nrebin <= 0) {
	    return false;
	}
	if (nrebin >= nb) {
	    nrebin = nb;
	}
	h_truth->Rebin(nrebin);
	h_unfolded->Rebin(nrebin);
	h_SM->Rebin(nrebin);
	h_sigma1->Rebin(nrebin);
	h_sigma2->Rebin(nrebin);
    } else {
	sprintf(tmpnbin, "AllBin");
    }

    TH1F* h_SM_sigma1 = (TH1F*)h_SM->Clone();
    h_SM_sigma1->Add(h_sigma1);
    TH1F* h_SM_sigma1_sigma2 = (TH1F*)h_SM->Clone();
    h_SM_sigma1_sigma2->Add(h_sigma1);
    h_SM_sigma1_sigma2->Add(h_sigma2);
    
    // plot EFT operators
    PlotComparor* PC = new PlotComparor();
    PC->SetSaveDir("plots/");
    PC->Is13TeV(true);
    PC->IsSimulation(true);

    PC->SquareCanvas(true);
    PC->ClearAlterHs();
    PC->SetBaseH(h_SM);
    PC->SetBaseHName("SM");
    PC->AddAlterH(h_SM_sigma1);
    PC->AddAlterHName("SM+sigma1");
    PC->AddAlterH(h_SM_sigma1_sigma2);
    PC->AddAlterHName("SM+sigma1+sigma2");
    PC->SetChannel(channel.Data());
    TString savename = "Compare_Unfolding_ISR_" + var + "_" + channel + "_" + opt + "_" + tmpnbin;
    PC->SetSaveName(savename.Data());
    PC->DrawRatio(true);
    PC->SetMaxRatio(1.8);
    PC->SetMinRatio(0.6);
    PC->Compare();   

    PC->SquareCanvas(true);
    PC->ClearAlterHs();
    PC->SetBaseH(h_truth);
    PC->SetBaseHName("Truth");
    PC->AddAlterH(h_unfolded);
    PC->AddAlterHName("Unfolded");
    PC->SetChannel(channel.Data());
    savename = "Compare_Unfolding_Inputs_" + var + "_" + channel + "_" + opt + "_" + tmpnbin;
    PC->SetSaveName(savename.Data());
    PC->DrawRatio(true);
    PC->SetMaxRatio(1.8);
    PC->SetMinRatio(0.6);
    PC->Compare();   

    // add overflow
    //h_SM->SetBinContent(h_SM->GetNbinsX(), h_SM->GetBinContent(h_SM->GetNbinsX()+1)+h_SM->GetBinContent(h_SM->GetNbinsX()));
    //h_SM->SetBinError(h_SM->GetNbinsX(), sqrt(pow(h_SM->GetBinError(h_SM->GetNbinsX()+1),2)+pow(h_SM->GetBinError(h_SM->GetNbinsX()),2)));
    //h_sigma1->SetBinContent(h_sigma1->GetNbinsX(), h_sigma1->GetBinContent(h_sigma1->GetNbinsX()+1)+h_sigma1->GetBinContent(h_sigma1->GetNbinsX()));
    //h_sigma1->SetBinError(h_sigma1->GetNbinsX(), sqrt(pow(h_sigma1->GetBinError(h_sigma1->GetNbinsX()+1),2)+pow(h_sigma1->GetBinError(h_sigma1->GetNbinsX()),2)));
    //h_sigma2->SetBinContent(h_sigma2->GetNbinsX(), h_sigma2->GetBinContent(h_sigma2->GetNbinsX()+1)+h_sigma2->GetBinContent(h_sigma2->GetNbinsX()));
    //h_sigma2->SetBinError(h_sigma2->GetNbinsX(), sqrt(pow(h_sigma2->GetBinError(h_sigma2->GetNbinsX()+1),2)+pow(h_sigma2->GetBinError(h_sigma2->GetNbinsX()),2)));
    
    // EFT contribution
    TH1F* h_sigma1_forfit = (TH1F*)h_truth->Clone();
    for (int i = 1; i <= h_sigma1_forfit->GetNbinsX(); i++) {
	double n_eftfr = h_sigma1->GetBinContent(i)/h_SM->GetBinContent(i);
	double e_eftfr = n_eftfr * sqrt(pow(h_sigma1->GetBinError(i)/h_sigma1->GetBinContent(i),2) + pow(h_SM->GetBinError(i)/h_SM->GetBinContent(i),2));
	cout << "in: " << n_eftfr << endl;

	double bincenter = h_sigma1_forfit->GetBinCenter(i);
	int jbin = h_fr_prod->FindBin(bincenter);
	if (jbin > h_fr_prod->GetNbinsX()) jbin = h_fr_prod->GetNbinsX();
	double fr_prod = h_fr_prod->GetBinContent(jbin);

	if (eftall) fr_prod = 1;

	double n_prod = h_truth->GetBinContent(i)*fr_prod;
	double e_prod = h_truth->GetBinError(i)*fr_prod;

	double n_new_prod = n_prod * n_eftfr;
	double e_new_prod = n_new_prod * sqrt(pow(e_prod/n_prod,2) + pow(e_eftfr/n_eftfr,2));

	h_sigma1_forfit->SetBinContent(i, n_new_prod);
	h_sigma1_forfit->SetBinError(i, e_new_prod);

	cout << "out: " << h_sigma1_forfit->GetBinContent(i)/h_truth->GetBinContent(i) << endl;
    }
    TH1F* h_sigma2_forfit = (TH1F*)h_truth->Clone();
    for (int i = 1; i <= h_sigma2_forfit->GetNbinsX(); i++) {
	double n_eftfr = h_sigma2->GetBinContent(i)/h_SM->GetBinContent(i);
	double e_eftfr = n_eftfr * sqrt(pow(h_sigma2->GetBinError(i)/h_sigma2->GetBinContent(i),2) + pow(h_SM->GetBinError(i)/h_SM->GetBinContent(i),2));
	cout << "in: " << n_eftfr << endl;

	double bincenter = h_sigma2_forfit->GetBinCenter(i);
	int jbin = h_fr_prod->FindBin(bincenter);
	if (jbin > h_fr_prod->GetNbinsX()) jbin = h_fr_prod->GetNbinsX();
	double fr_prod = h_fr_prod->GetBinContent(jbin);

	if (eftall) fr_prod = 1;

	double n_prod = h_truth->GetBinContent(i)*fr_prod;
	double e_prod = h_truth->GetBinError(i)*fr_prod;

	double n_new_prod = n_prod * n_eftfr;
	double e_new_prod = n_new_prod * sqrt(pow(e_prod/n_prod,2) + pow(e_eftfr/n_eftfr,2));

	h_sigma2_forfit->SetBinContent(i, n_new_prod);
	h_sigma2_forfit->SetBinError(i, e_new_prod);
	cout << "out: " << h_sigma2_forfit->GetBinContent(i)/h_truth->GetBinContent(i) << endl;
    }

    // plot EFT
    TH1F* h_truth_sigma1_ = (TH1F*)h_truth->Clone();
    h_truth_sigma1_->Add(h_sigma1_forfit);
    TH1F* h_truth_sigma2_ = (TH1F*)h_truth->Clone();
    h_truth_sigma2_->Add(h_sigma2_forfit);
    PC->SquareCanvas(true);
    PC->ClearAlterHs();
    PC->SetBaseH(h_truth);
    PC->SetBaseHName("SM");
    PC->AddAlterH(h_truth_sigma1_);
    PC->AddAlterHName("SM+sigma1");
    PC->AddAlterH(h_truth_sigma2_);
    PC->AddAlterHName("SM+sigma1+sigma2");
    PC->SetChannel(channel.Data());
    savename = "Compare_Unfolding_IFSR_" + var + "_" + channel + "_" + opt + "_" + tmpnbin;
    if (eftall) savename += "_EFTAll";
    PC->SetSaveName(savename.Data());
    PC->DrawRatio(true);
    PC->SetMaxRatio(1.8);
    PC->SetMinRatio(0.6);
    PC->Compare();   
    
    RooMsgService::instance().setSilentMode(kTRUE);
    RooMsgService::instance().setGlobalKillBelow(WARNING);

    RooRealVar *C = new RooRealVar("C"+opt, "C"+opt, 0, -2, 2);
    C->setVal(0);

    RooRealVar x("x","x",x_lo,x_hi) ;
    x.setBins(nb);
    RooDataHist * data = new RooDataHist("data", "data", x, h_unfolded);
    RooDataHist * hist_sm = new RooDataHist("sm", "sm", x, h_truth);
    RooDataHist * hist_sigma1 = new RooDataHist("sigma1", "sigma1", x, h_sigma1_forfit);
    RooDataHist * hist_sigma2 = new RooDataHist("sigma2", "sigma2", x, h_sigma2_forfit);
    RooHistPdf * pdf_sm = new RooHistPdf("pdf_sm", "pdf_sm", x, *hist_sm);
    RooHistPdf * pdf_sigma1 = new RooHistPdf("pdf_sigma1", "pdf_sigma1", x, *hist_sigma1);
    RooHistPdf * pdf_sigma2 = new RooHistPdf("pdf_sigma2", "pdf_sigma2", x, *hist_sigma2);
    
    double n_truth = h_truth->Integral();
    double n_sigma1 = h_sigma1_forfit->Integral();
    double n_sigma2 = h_sigma2_forfit->Integral();
    RooRealVar* N = new RooRealVar("N","N",1000, 0,100000);
    N->setVal(n_truth);
    N->setConstant(true);
    RooFormulaVar *C1 = new RooFormulaVar("C1", "C1", "@0*@1", RooArgSet(*C, RooConst(n_sigma1)));
    RooFormulaVar *C2 = new RooFormulaVar("C2", "C2", "@0*@0*@1", RooArgSet(*C, RooConst(n_sigma2)));
    
    RooAddPdf *model = new RooAddPdf("model", "model", RooArgList(*pdf_sm,*pdf_sigma1,*pdf_sigma2), RooArgList(*N, *C1, *C2));
    
    //RooDataSet *testdata = (RooDataSet*)model->generate(RooArgSet(x,),
    RooFitResult *results = model->fitTo(*data, RooFit::Save(), SumW2Error(kTRUE));
    //results->Print();
    
    //RooChi2Var chi2("chi2","chi2",*model,*data,DataError(RooAbsData::SumW2));
    //RooMinimizer m(chi2);
    //m.migrad();
    //m.hesse();    
    //RooFitResult* results = m.save();
    //results->Print();
    
    RooPlot* xframe = x.frame(Title("frame")) ;
    //data->plotOn(xframe,DataError(RooAbsData::SumW2));
    TH1* t = data->createHistogram("t",x);
    RooHist *h_sumw2Err_uncorrected = new RooHist(*t,0,1,RooAbsData::SumW2,1.,true);
    h_sumw2Err_uncorrected->SetMarkerColor(kGreen);h_sumw2Err_uncorrected->SetLineColor(kBlack);
    xframe->addPlotable(h_sumw2Err_uncorrected,"P");
    model->paramOn(xframe,Layout(0.55)) ;
    model->plotOn(xframe);
    model->plotOn(xframe,Components(*pdf_sm),LineStyle(kDashed),LineColor(kBlack),Normalization(1.0,RooAbsReal::RelativeExpected));
    model->plotOn(xframe,Components(*pdf_sigma1),LineStyle(kDashed),LineColor(kBlue),Normalization(1.0,RooAbsReal::RelativeExpected));
    model->plotOn(xframe,Components(*pdf_sigma2),LineStyle(kDashed),LineColor(kRed),Normalization(1.0,RooAbsReal::RelativeExpected));
    data->statOn(xframe,Layout(0.55,0.99,0.8)) ;
    TCanvas* c = new TCanvas("test","test",800,600) ;
    xframe->Draw() ;
    savename = "plots/Compare_Unfolding_PostFit_" + var + "_" + channel + "_" + opt + "_" + tmpnbin;
    if (eftall) savename += "_EFTAll";
    c->SaveAs(savename+".png");
    delete c;
   
    n_C = C->getVal();
    e_C = C->getError();

    return true;
}
