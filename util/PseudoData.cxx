#include <TFile.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TH2F.h>
#include <TH1F.h>

#include <RooFit.h>
#include <RooRealVar.h>
#include <RooGaussian.h>
#include <RooWorkspace.h>
#include <RooDataSet.h>
#include <RooRealVar.h>
#include <RooArgSet.h>
#include <RooWorkspace.h>
#include <RooDataHist.h>
#include <RooHist.h>
#include <RooHistPdf.h>
#include <RooProduct.h>
#include <RooAddPdf.h>
#include <RooAbsPdf.h>
#include <RooAddition.h>
#include <RooProdPdf.h>
#include <RooGaussModel.h>
#include <RooBinning.h>
#include <RooGenericPdf.h>
#include <RooExtendPdf.h>
#include <RooPoisson.h>
#include <RooPlot.h>
#include <RooLognormal.h>
#include <RooCategory.h>
#include <RooSimultaneous.h>

#include <RooStats/RooStatsUtils.h>
#include <RooStats/ModelConfig.h>
#include <RooStats/MCMCInterval.h>
#include <RooStats/MCMCIntervalPlot.h>
#include <iostream>
#include <sstream>
#include <string>
#include <iostream>
#include <iomanip>
#include <string>
#include <libgen.h>
#include <stdio.h>      
#include <stdlib.h> 

using namespace RooFit;
using namespace RooStats;
using namespace std;

int main () {

    // mother histogram
    TH1F* h = new TH1F("test","test",3,0,3);
    h->SetBinContent(1,100);
    h->SetBinContent(2,400);
    h->SetBinContent(3,100);

    // parameters
    TString outrootname = "Test.root";
    TString outpngname = "test.png";
    double h2_min = 0;
    double h2_max = 1.3*h->GetMaximum();
    int h2_nbin = (h2_max - h2_min)/10;
    int n_pseudo = 1000;

    // don't touch
    RooRealVar x("x","x",h->GetBinLowEdge(1),h->GetBinLowEdge(h->GetNbinsX())+h->GetBinWidth(h->GetNbinsX()),"Unit");
    x.setBins(h->GetNbinsX());
    RooDataHist* rdh = new RooDataHist("rdh","rdh",RooArgList(x),h);
    RooHistPdf *rhp = new RooHistPdf("rhp","rhp",x,*rdh);

    TFile*fout = new TFile(outrootname,"recreate");
    TCanvas* c = new TCanvas("c","c",800,600);
    h->SetLineColor(1);
    h->SetLineWidth(2);
    h->GetYaxis()->SetRangeUser(0, h2_max);
    h->Draw();
    TH1F *h_int = new TH1F("int","int",100, h->Integral() - 5*sqrt(h->Integral()), h->Integral() + 5*sqrt(h->Integral()));
    TH2F *h2 = new TH2F("h2","h2",h->GetNbinsX(),h->GetBinLowEdge(1),h->GetBinLowEdge(h->GetNbinsX())+h->GetBinWidth(h->GetNbinsX()),h2_nbin,h2_min,h2_max);
    for (int i = 0; i < n_pseudo; i++) {
	char tmpname[100];
	sprintf(tmpname, "pseudo_%i", i);
	RooDataSet* data = rhp->generate(RooArgSet(x),h->Integral(),Extended(),AutoBinned(true));
	TH1F* hout = (TH1F*)data->createHistogram("data",x);
	hout->SetName(tmpname);
	hout->SetLineStyle(2);
	hout->SetLineWidth(1);
	hout->SetLineColor(2);
	for (int j = 0; j < hout->GetNbinsX(); j++) {
	    hout->SetBinError(j+1,0);
	    double bincen = hout->GetBinCenter(j+1);
	    h2->Fill(bincen,hout->GetBinContent(j+1));
	}
	hout->Draw("same");
	hout->Write();
	h_int->Fill(hout->Integral());
    }
    c->SaveAs(outpngname);

    cout << "Output saved to -> " << outrootname << endl;
    h_int->Write();
    h2->Write();
    fout->Close();
    delete fout;
}
