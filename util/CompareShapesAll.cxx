#include <TFile.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TPad.h>
#include <TLine.h>
#include <string>
#include <TLatex.h>
#include <TMath.h>
#include <vector>
#include <TGraphErrors.h>
#include <map>
#include <TIterator.h>
#include <TKey.h>
#include <TObject.h>
#include <THStack.h>

#include "AtlasStyle.h"
#include "PlotComparor.h"
#include "StringPlayer.h"
#include "ConfigReader.h"
#include "Logger.h"

using namespace std;

int main(int argc, char * argv[]) {
    Logger *lg = new Logger("MAIN");
    lg->Info("s", "Hi, this is the main programm~");

    string file1 = argv[1];
    string file2 = argv[2];

    PlotComparor* PC = new PlotComparor();
    PC->DrawRatio(true);
    PC->SetSaveDir("plots/");
    PC->NormToUnit(true);
    string drawoptions = "hist";
    drawoptions += " _e";
    PC->SetDrawOption(drawoptions.c_str());

    TFile *f1 = new TFile(files1.c_str());
    TFile *f2 = new TFile(files2.c_str());

    TIter next(f1->GetListOfKeys());
    TKey* key;
    int ivar = 0;
    while ((key = (TKey*)next())) {
        TObject* obj = (TObject*)key->ReadObj();
        if (obj->InheritsFrom(TH1F::Class())) {
	    ivar++;
            TH1F*h1 = (TH1F*)key->ReadObj();
            TString hname1 = h1->GetName();
	    TString hname2 = hname1;
            TH1F*h2 = (TH1F*)f2->Get(hname2);
            if (!h2) {
        	cout << "Can't find histogram in f2 -> " << hname2 << endl;
        	f2->ls();
        	exit(-1);
            }
	    PC->ClearAlterHs();
	    vector<TH1F*> hs;
	    hs.push_back(h2);
	    PC->SetBaseH(h1);
    	    PC->SetBaseHName("File1");
    	    PC->AddAlterH(h2);
    	    PC->AddAlterHName("File2");
	    string savename = "CompareShape_";
	    char tmp[10];
	    sprintf(tmp, "Var%d", ivar);
	    savename += tmp;
	    PC->SetSaveName(savename);
	    PC->Is13TeV(true);
	    PC->DataLumi(36.5);
	    PC->Compare();
	}
    }
    
    return 0;
}

