#include <TH1F.h>
#include <algorithm>
#include <PlotComparor.h>
#include <TFile.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "StringPlayer.h"
#include <TH2.h>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <TKey.h>
#include <TIterator.h>
#include <TH1.h>
#include <TLorentzVector.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TStopwatch.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <TFile.h>

using namespace std;

void GetHistFromYoda(string input, TH1F* h_phpt, TH1F* h_pheta, TH1F* h_leppt, TH1F* h_lepeta, TH1F* h_jet1pt, TH1F* h_jet1eta, TH1F* h_jet2pt, 
		    TH1F* h_jet2eta, TH1F* h_jet3pt, TH1F* h_jet3eta, TH1F* h_jet4pt, TH1F* h_jet4eta);

int main()
{
    Double_t bins_phpt[] = {20,35,50,65,80,95,110,140,180,300};
    int binnum_phpt = sizeof(bins_phpt)/sizeof(Double_t) - 1;
    Double_t bins_pheta[] = {-2.37, -1.7, -1.2, -1.0, -0.8, -0.6, -0.4, -0.2, 0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.7, 2.37};
    int binnum_pheta = sizeof(bins_pheta)/sizeof(Double_t) - 1;
    TH1F* h_phpt_LO_MG5_dyn = new TH1F("phpt_LO_MG5_dyn","phpt_LO_MG5_dyn",binnum_phpt, bins_phpt);
    TH1F* h_phpt_LO_MG5_dyn_jet03 = new TH1F("phpt_LO_MG5_dyn_jet03","phpt_LO_MG5_dyn_jet03",binnum_phpt, bins_phpt);
    TH1F* h_phpt_LO_MG5_dyn_jet05 = new TH1F("phpt_LO_MG5_dyn_jet05","phpt_LO_MG5_dyn_jet05",binnum_phpt, bins_phpt);
    TH1F* h_phpt_LO_MG5_dyn_noHD = new TH1F("phpt_LO_MG5_dyn_noHD","phpt_LO_MG5_dyn_noHD",binnum_phpt, bins_phpt);
    TH1F* h_phpt_LO_MG5_dyn_noUE = new TH1F("phpt_LO_MG5_dyn_noUE","phpt_LO_MG5_dyn_noUE",binnum_phpt, bins_phpt);
    TH1F* h_phpt_LO_MG5_dyn_bare = new TH1F("phpt_LO_MG5_dyn_bare","phpt_LO_MG5_dyn_bare",binnum_phpt, bins_phpt);
    TH1F* h_pheta_LO_MG5_dyn = new TH1F("pheta_LO_MG5_dyn","pheta_LO_MG5_dyn",binnum_pheta, bins_pheta);
    TH1F* h_pheta_LO_MG5_dyn_jet03 = new TH1F("pheta_LO_MG5_dyn_jet03","pheta_LO_MG5_dyn_jet03",binnum_pheta, bins_pheta);
    TH1F* h_pheta_LO_MG5_dyn_jet05 = new TH1F("pheta_LO_MG5_dyn_jet05","pheta_LO_MG5_dyn_jet05",binnum_pheta, bins_pheta);
    TH1F* h_pheta_LO_MG5_dyn_noHD = new TH1F("pheta_LO_MG5_dyn_noHD","pheta_LO_MG5_dyn_noHD",binnum_pheta, bins_pheta);
    TH1F* h_pheta_LO_MG5_dyn_noUE = new TH1F("pheta_LO_MG5_dyn_noUE","pheta_LO_MG5_dyn_noUE",binnum_pheta, bins_pheta);
    TH1F* h_pheta_LO_MG5_dyn_bare = new TH1F("pheta_LO_MG5_dyn_bare","pheta_LO_MG5_dyn_bare",binnum_pheta, bins_pheta);
    TH1F* h_leppt_LO_MG5_dyn = new TH1F("leppt_LO_MG5_dyn","leppt_LO_MG5_dyn",binnum_phpt, bins_phpt);
    TH1F* h_leppt_LO_MG5_dyn_jet03 = new TH1F("leppt_LO_MG5_dyn_jet03","leppt_LO_MG5_dyn_jet03",binnum_phpt, bins_phpt);
    TH1F* h_leppt_LO_MG5_dyn_jet05 = new TH1F("leppt_LO_MG5_dyn_jet05","leppt_LO_MG5_dyn_jet05",binnum_phpt, bins_phpt);
    TH1F* h_leppt_LO_MG5_dyn_noHD = new TH1F("leppt_LO_MG5_dyn_noHD","leppt_LO_MG5_dyn_noHD",binnum_phpt, bins_phpt);
    TH1F* h_leppt_LO_MG5_dyn_noUE = new TH1F("leppt_LO_MG5_dyn_noUE","leppt_LO_MG5_dyn_noUE",binnum_phpt, bins_phpt);
    TH1F* h_leppt_LO_MG5_dyn_bare = new TH1F("leppt_LO_MG5_dyn_bare","leppt_LO_MG5_dyn_bare",binnum_phpt, bins_phpt);
    TH1F* h_lepeta_LO_MG5_dyn = new TH1F("lepeta_LO_MG5_dyn","lepeta_LO_MG5_dyn",binnum_pheta, bins_pheta);
    TH1F* h_lepeta_LO_MG5_dyn_jet03 = new TH1F("lepeta_LO_MG5_dyn_jet03","lepeta_LO_MG5_dyn_jet03",binnum_pheta, bins_pheta);
    TH1F* h_lepeta_LO_MG5_dyn_jet05 = new TH1F("lepeta_LO_MG5_dyn_jet05","lepeta_LO_MG5_dyn_jet05",binnum_pheta, bins_pheta);
    TH1F* h_lepeta_LO_MG5_dyn_noHD = new TH1F("lepeta_LO_MG5_dyn_noHD","lepeta_LO_MG5_dyn_noHD",binnum_pheta, bins_pheta);
    TH1F* h_lepeta_LO_MG5_dyn_noUE = new TH1F("lepeta_LO_MG5_dyn_noUE","lepeta_LO_MG5_dyn_noUE",binnum_pheta, bins_pheta);
    TH1F* h_lepeta_LO_MG5_dyn_bare = new TH1F("lepeta_LO_MG5_dyn_bare","lepeta_LO_MG5_dyn_bare",binnum_pheta, bins_pheta);
    TH1F* h_jet1pt_LO_MG5_dyn = new TH1F("jet1pt_LO_MG5_dyn","jet1pt_LO_MG5_dyn",binnum_phpt, bins_phpt);
    TH1F* h_jet1pt_LO_MG5_dyn_jet03 = new TH1F("jet1pt_LO_MG5_dyn_jet03","jet1pt_LO_MG5_dyn_jet03",binnum_phpt, bins_phpt);
    TH1F* h_jet1pt_LO_MG5_dyn_jet05 = new TH1F("jet1pt_LO_MG5_dyn_jet05","jet1pt_LO_MG5_dyn_jet05",binnum_phpt, bins_phpt);
    TH1F* h_jet1pt_LO_MG5_dyn_noHD = new TH1F("jet1pt_LO_MG5_dyn_noHD","jet1pt_LO_MG5_dyn_noHD",binnum_phpt, bins_phpt);
    TH1F* h_jet1pt_LO_MG5_dyn_noUE = new TH1F("jet1pt_LO_MG5_dyn_noUE","jet1pt_LO_MG5_dyn_noUE",binnum_phpt, bins_phpt);
    TH1F* h_jet1pt_LO_MG5_dyn_bare = new TH1F("jet1pt_LO_MG5_dyn_bare","jet1pt_LO_MG5_dyn_bare",binnum_phpt, bins_phpt);
    TH1F* h_jet1eta_LO_MG5_dyn = new TH1F("jet1eta_LO_MG5_dyn","jet1eta_LO_MG5_dyn",binnum_pheta, bins_pheta);
    TH1F* h_jet1eta_LO_MG5_dyn_jet03 = new TH1F("jet1eta_LO_MG5_dyn_jet03","jet1eta_LO_MG5_dyn_jet03",binnum_pheta, bins_pheta);
    TH1F* h_jet1eta_LO_MG5_dyn_jet05 = new TH1F("jet1eta_LO_MG5_dyn_jet05","jet1eta_LO_MG5_dyn_jet05",binnum_pheta, bins_pheta);
    TH1F* h_jet1eta_LO_MG5_dyn_noHD = new TH1F("jet1eta_LO_MG5_dyn_noHD","jet1eta_LO_MG5_dyn_noHD",binnum_pheta, bins_pheta);
    TH1F* h_jet1eta_LO_MG5_dyn_noUE = new TH1F("jet1eta_LO_MG5_dyn_noUE","jet1eta_LO_MG5_dyn_noUE",binnum_pheta, bins_pheta);
    TH1F* h_jet1eta_LO_MG5_dyn_bare = new TH1F("jet1eta_LO_MG5_dyn_bare","jet1eta_LO_MG5_dyn_bare",binnum_pheta, bins_pheta);
    TH1F* h_jet2pt_LO_MG5_dyn = new TH1F("jet2pt_LO_MG5_dyn","jet2pt_LO_MG5_dyn",binnum_phpt, bins_phpt);
    TH1F* h_jet2pt_LO_MG5_dyn_jet03 = new TH1F("jet2pt_LO_MG5_dyn_jet03","jet2pt_LO_MG5_dyn_jet03",binnum_phpt, bins_phpt);
    TH1F* h_jet2pt_LO_MG5_dyn_jet05 = new TH1F("jet2pt_LO_MG5_dyn_jet05","jet2pt_LO_MG5_dyn_jet05",binnum_phpt, bins_phpt);
    TH1F* h_jet2pt_LO_MG5_dyn_noHD = new TH1F("jet2pt_LO_MG5_dyn_noHD","jet2pt_LO_MG5_dyn_noHD",binnum_phpt, bins_phpt);
    TH1F* h_jet2pt_LO_MG5_dyn_noUE = new TH1F("jet2pt_LO_MG5_dyn_noUE","jet2pt_LO_MG5_dyn_noUE",binnum_phpt, bins_phpt);
    TH1F* h_jet2pt_LO_MG5_dyn_bare = new TH1F("jet2pt_LO_MG5_dyn_bare","jet2pt_LO_MG5_dyn_bare",binnum_phpt, bins_phpt);
    TH1F* h_jet2eta_LO_MG5_dyn = new TH1F("jet2eta_LO_MG5_dyn","jet2eta_LO_MG5_dyn",binnum_pheta, bins_pheta);
    TH1F* h_jet2eta_LO_MG5_dyn_jet03 = new TH1F("jet2eta_LO_MG5_dyn_jet03","jet2eta_LO_MG5_dyn_jet03",binnum_pheta, bins_pheta);
    TH1F* h_jet2eta_LO_MG5_dyn_jet05 = new TH1F("jet2eta_LO_MG5_dyn_jet05","jet2eta_LO_MG5_dyn_jet05",binnum_pheta, bins_pheta);
    TH1F* h_jet2eta_LO_MG5_dyn_noHD = new TH1F("jet2eta_LO_MG5_dyn_noHD","jet2eta_LO_MG5_dyn_noHD",binnum_pheta, bins_pheta);
    TH1F* h_jet2eta_LO_MG5_dyn_noUE = new TH1F("jet2eta_LO_MG5_dyn_noUE","jet2eta_LO_MG5_dyn_noUE",binnum_pheta, bins_pheta);
    TH1F* h_jet2eta_LO_MG5_dyn_bare = new TH1F("jet2eta_LO_MG5_dyn_bare","jet2eta_LO_MG5_dyn_bare",binnum_pheta, bins_pheta);
    TH1F* h_jet3pt_LO_MG5_dyn = new TH1F("jet3pt_LO_MG5_dyn","jet3pt_LO_MG5_dyn",binnum_phpt, bins_phpt);
    TH1F* h_jet3pt_LO_MG5_dyn_jet03 = new TH1F("jet3pt_LO_MG5_dyn_jet03","jet3pt_LO_MG5_dyn_jet03",binnum_phpt, bins_phpt);
    TH1F* h_jet3pt_LO_MG5_dyn_jet05 = new TH1F("jet3pt_LO_MG5_dyn_jet05","jet3pt_LO_MG5_dyn_jet05",binnum_phpt, bins_phpt);
    TH1F* h_jet3pt_LO_MG5_dyn_noHD = new TH1F("jet3pt_LO_MG5_dyn_noHD","jet3pt_LO_MG5_dyn_noHD",binnum_phpt, bins_phpt);
    TH1F* h_jet3pt_LO_MG5_dyn_noUE = new TH1F("jet3pt_LO_MG5_dyn_noUE","jet3pt_LO_MG5_dyn_noUE",binnum_phpt, bins_phpt);
    TH1F* h_jet3pt_LO_MG5_dyn_bare = new TH1F("jet3pt_LO_MG5_dyn_bare","jet3pt_LO_MG5_dyn_bare",binnum_phpt, bins_phpt);
    TH1F* h_jet3eta_LO_MG5_dyn = new TH1F("jet3eta_LO_MG5_dyn","jet3eta_LO_MG5_dyn",binnum_pheta, bins_pheta);
    TH1F* h_jet3eta_LO_MG5_dyn_jet03 = new TH1F("jet3eta_LO_MG5_dyn_jet03","jet3eta_LO_MG5_dyn_jet03",binnum_pheta, bins_pheta);
    TH1F* h_jet3eta_LO_MG5_dyn_jet05 = new TH1F("jet3eta_LO_MG5_dyn_jet05","jet3eta_LO_MG5_dyn_jet05",binnum_pheta, bins_pheta);
    TH1F* h_jet3eta_LO_MG5_dyn_noHD = new TH1F("jet3eta_LO_MG5_dyn_noHD","jet3eta_LO_MG5_dyn_noHD",binnum_pheta, bins_pheta);
    TH1F* h_jet3eta_LO_MG5_dyn_noUE = new TH1F("jet3eta_LO_MG5_dyn_noUE","jet3eta_LO_MG5_dyn_noUE",binnum_pheta, bins_pheta);
    TH1F* h_jet3eta_LO_MG5_dyn_bare = new TH1F("jet3eta_LO_MG5_dyn_bare","jet3eta_LO_MG5_dyn_bare",binnum_pheta, bins_pheta);
    TH1F* h_jet4pt_LO_MG5_dyn = new TH1F("jet4pt_LO_MG5_dyn","jet4pt_LO_MG5_dyn",binnum_phpt, bins_phpt);
    TH1F* h_jet4pt_LO_MG5_dyn_jet03 = new TH1F("jet4pt_LO_MG5_dyn_jet03","jet4pt_LO_MG5_dyn_jet03",binnum_phpt, bins_phpt);
    TH1F* h_jet4pt_LO_MG5_dyn_jet05 = new TH1F("jet4pt_LO_MG5_dyn_jet05","jet4pt_LO_MG5_dyn_jet05",binnum_phpt, bins_phpt);
    TH1F* h_jet4pt_LO_MG5_dyn_noHD = new TH1F("jet4pt_LO_MG5_dyn_noHD","jet4pt_LO_MG5_dyn_noHD",binnum_phpt, bins_phpt);
    TH1F* h_jet4pt_LO_MG5_dyn_noUE = new TH1F("jet4pt_LO_MG5_dyn_noUE","jet4pt_LO_MG5_dyn_noUE",binnum_phpt, bins_phpt);
    TH1F* h_jet4pt_LO_MG5_dyn_bare = new TH1F("jet4pt_LO_MG5_dyn_bare","jet4pt_LO_MG5_dyn_bare",binnum_phpt, bins_phpt);
    TH1F* h_jet4eta_LO_MG5_dyn = new TH1F("jet4eta_LO_MG5_dyn","jet4eta_LO_MG5_dyn",binnum_pheta, bins_pheta);
    TH1F* h_jet4eta_LO_MG5_dyn_jet03 = new TH1F("jet4eta_LO_MG5_dyn_jet03","jet4eta_LO_MG5_dyn_jet03",binnum_pheta, bins_pheta);
    TH1F* h_jet4eta_LO_MG5_dyn_jet05 = new TH1F("jet4eta_LO_MG5_dyn_jet05","jet4eta_LO_MG5_dyn_jet05",binnum_pheta, bins_pheta);
    TH1F* h_jet4eta_LO_MG5_dyn_noHD = new TH1F("jet4eta_LO_MG5_dyn_noHD","jet4eta_LO_MG5_dyn_noHD",binnum_pheta, bins_pheta);
    TH1F* h_jet4eta_LO_MG5_dyn_noUE = new TH1F("jet4eta_LO_MG5_dyn_noUE","jet4eta_LO_MG5_dyn_noUE",binnum_pheta, bins_pheta);
    TH1F* h_jet4eta_LO_MG5_dyn_bare = new TH1F("jet4eta_LO_MG5_dyn_bare","jet4eta_LO_MG5_dyn_bare",binnum_pheta, bins_pheta);

    GetHistFromYoda("/afs/cern.ch/work/y/yili/private/Analysis/Rivet/ttgamma8TeV/kfactor_sl_10k/Rivet.yoda",
    h_phpt_LO_MG5_dyn, h_pheta_LO_MG5_dyn,
    h_leppt_LO_MG5_dyn, h_lepeta_LO_MG5_dyn,
    h_jet1pt_LO_MG5_dyn, h_jet1eta_LO_MG5_dyn,
    h_jet2pt_LO_MG5_dyn, h_jet2eta_LO_MG5_dyn,
    h_jet3pt_LO_MG5_dyn, h_jet3eta_LO_MG5_dyn,
    h_jet4pt_LO_MG5_dyn, h_jet4eta_LO_MG5_dyn
    );

    GetHistFromYoda("/afs/cern.ch/work/y/yili/private/Analysis/Rivet/ttgamma8TeV/kfactor_sl_10k_jet03/Rivet.yoda",
    h_phpt_LO_MG5_dyn_jet03, h_pheta_LO_MG5_dyn_jet03,
    h_leppt_LO_MG5_dyn_jet03, h_lepeta_LO_MG5_dyn_jet03,
    h_jet1pt_LO_MG5_dyn_jet03, h_jet1eta_LO_MG5_dyn_jet03,
    h_jet2pt_LO_MG5_dyn_jet03, h_jet2eta_LO_MG5_dyn_jet03,
    h_jet3pt_LO_MG5_dyn_jet03, h_jet3eta_LO_MG5_dyn_jet03,
    h_jet4pt_LO_MG5_dyn_jet03, h_jet4eta_LO_MG5_dyn_jet03
    );

    GetHistFromYoda("/afs/cern.ch/work/y/yili/private/Analysis/Rivet/ttgamma8TeV/kfactor_sl_10k_jet05/Rivet.yoda",
    h_phpt_LO_MG5_dyn_jet05, h_pheta_LO_MG5_dyn_jet05,
    h_leppt_LO_MG5_dyn_jet05, h_lepeta_LO_MG5_dyn_jet05,
    h_jet1pt_LO_MG5_dyn_jet05, h_jet1eta_LO_MG5_dyn_jet05,
    h_jet2pt_LO_MG5_dyn_jet05, h_jet2eta_LO_MG5_dyn_jet05,
    h_jet3pt_LO_MG5_dyn_jet05, h_jet3eta_LO_MG5_dyn_jet05,
    h_jet4pt_LO_MG5_dyn_jet05, h_jet4eta_LO_MG5_dyn_jet05
    );

    GetHistFromYoda("/afs/cern.ch/work/y/yili/private/Analysis/Rivet/ttgamma8TeV/kfactor_sl_10k_noHD/Rivet.yoda",
    h_phpt_LO_MG5_dyn_noHD, h_pheta_LO_MG5_dyn_noHD,
    h_leppt_LO_MG5_dyn_noHD, h_lepeta_LO_MG5_dyn_noHD,
    h_jet1pt_LO_MG5_dyn_noHD, h_jet1eta_LO_MG5_dyn_noHD,
    h_jet2pt_LO_MG5_dyn_noHD, h_jet2eta_LO_MG5_dyn_noHD,
    h_jet3pt_LO_MG5_dyn_noHD, h_jet3eta_LO_MG5_dyn_noHD,
    h_jet4pt_LO_MG5_dyn_noHD, h_jet4eta_LO_MG5_dyn_noHD
    );

    GetHistFromYoda("/afs/cern.ch/work/y/yili/private/Analysis/Rivet/ttgamma8TeV/kfactor_sl_10k_noUE/Rivet.yoda",
    h_phpt_LO_MG5_dyn_noUE, h_pheta_LO_MG5_dyn_noUE,
    h_leppt_LO_MG5_dyn_noUE, h_lepeta_LO_MG5_dyn_noUE,
    h_jet1pt_LO_MG5_dyn_noUE, h_jet1eta_LO_MG5_dyn_noUE,
    h_jet2pt_LO_MG5_dyn_noUE, h_jet2eta_LO_MG5_dyn_noUE,
    h_jet3pt_LO_MG5_dyn_noUE, h_jet3eta_LO_MG5_dyn_noUE,
    h_jet4pt_LO_MG5_dyn_noUE, h_jet4eta_LO_MG5_dyn_noUE
    );

    {
    TFile* f_LO_MG5_dyn = new TFile("/afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/output_sl.root");
    TH1F* h_phpt_LO_MG5_dyn_raw = (TH1F*)f_LO_MG5_dyn->Get("sl_phpt");
    TH1F* h_pheta_LO_MG5_dyn_raw = (TH1F*)f_LO_MG5_dyn->Get("sl_pheta");
    TH1F* h_leppt_LO_MG5_dyn_raw = (TH1F*)f_LO_MG5_dyn->Get("sl_leppt");
    TH1F* h_lepeta_LO_MG5_dyn_raw = (TH1F*)f_LO_MG5_dyn->Get("sl_lepeta");
    TH1F* h_jet1pt_LO_MG5_dyn_raw = (TH1F*)f_LO_MG5_dyn->Get("sl_jet1pt");
    TH1F* h_jet1eta_LO_MG5_dyn_raw = (TH1F*)f_LO_MG5_dyn->Get("sl_jet1eta");
    TH1F* h_jet2pt_LO_MG5_dyn_raw = (TH1F*)f_LO_MG5_dyn->Get("sl_jet2pt");
    TH1F* h_jet2eta_LO_MG5_dyn_raw = (TH1F*)f_LO_MG5_dyn->Get("sl_jet2eta");
    TH1F* h_jet3pt_LO_MG5_dyn_raw = (TH1F*)f_LO_MG5_dyn->Get("sl_jet3pt");
    TH1F* h_jet3eta_LO_MG5_dyn_raw = (TH1F*)f_LO_MG5_dyn->Get("sl_jet3eta");
    TH1F* h_jet4pt_LO_MG5_dyn_raw = (TH1F*)f_LO_MG5_dyn->Get("sl_jet4pt");
    TH1F* h_jet4eta_LO_MG5_dyn_raw = (TH1F*)f_LO_MG5_dyn->Get("sl_jet4eta");
    for (int i = 1; i < h_phpt_LO_MG5_dyn_raw->GetNbinsX(); i++) {
	double bin_low = h_phpt_LO_MG5_dyn_raw->GetXaxis()->GetBinLowEdge(i);
	double n_bin = h_phpt_LO_MG5_dyn_raw->GetBinContent(i);
	double e_bin = h_phpt_LO_MG5_dyn_raw->GetBinError(i);
	int ibin = h_phpt_LO_MG5_dyn_bare->FindBin(bin_low);
	if (ibin > h_phpt_LO_MG5_dyn_bare->GetNbinsX()) ibin = h_phpt_LO_MG5_dyn_bare->GetNbinsX();
	h_phpt_LO_MG5_dyn_bare->SetBinContent(ibin, h_phpt_LO_MG5_dyn_bare->GetBinContent(ibin)+n_bin);
	h_phpt_LO_MG5_dyn_bare->SetBinError(ibin, sqrt(pow(h_phpt_LO_MG5_dyn_bare->GetBinError(ibin),2)+pow(e_bin,2)));
    }
    for (int i = 1; i < h_pheta_LO_MG5_dyn_raw->GetNbinsX(); i++) {
	double bin_low = h_pheta_LO_MG5_dyn_raw->GetXaxis()->GetBinLowEdge(i);
	double n_bin = h_pheta_LO_MG5_dyn_raw->GetBinContent(i);
	double e_bin = h_pheta_LO_MG5_dyn_raw->GetBinError(i);
	int ibin = h_pheta_LO_MG5_dyn_bare->FindBin(bin_low);
	h_pheta_LO_MG5_dyn_bare->SetBinContent(ibin, h_pheta_LO_MG5_dyn_bare->GetBinContent(ibin)+n_bin);
	h_pheta_LO_MG5_dyn_bare->SetBinError(ibin, sqrt(pow(h_pheta_LO_MG5_dyn_bare->GetBinError(ibin),2)+pow(e_bin,2)));
    }
    for (int i = 1; i < h_leppt_LO_MG5_dyn_raw->GetNbinsX(); i++) {
	double bin_low = h_leppt_LO_MG5_dyn_raw->GetXaxis()->GetBinLowEdge(i);
	double n_bin = h_leppt_LO_MG5_dyn_raw->GetBinContent(i);
	double e_bin = h_leppt_LO_MG5_dyn_raw->GetBinError(i);
	int ibin = h_leppt_LO_MG5_dyn_bare->FindBin(bin_low);
	if (ibin > h_leppt_LO_MG5_dyn_bare->GetNbinsX()) ibin = h_leppt_LO_MG5_dyn_bare->GetNbinsX();
	h_leppt_LO_MG5_dyn_bare->SetBinContent(ibin, h_leppt_LO_MG5_dyn_bare->GetBinContent(ibin)+n_bin);
	h_leppt_LO_MG5_dyn_bare->SetBinError(ibin, sqrt(pow(h_leppt_LO_MG5_dyn_bare->GetBinError(ibin),2)+pow(e_bin,2)));
    }
    for (int i = 1; i < h_lepeta_LO_MG5_dyn_raw->GetNbinsX(); i++) {
	double bin_low = h_lepeta_LO_MG5_dyn_raw->GetXaxis()->GetBinLowEdge(i);
	double n_bin = h_lepeta_LO_MG5_dyn_raw->GetBinContent(i);
	double e_bin = h_lepeta_LO_MG5_dyn_raw->GetBinError(i);
	int ibin = h_lepeta_LO_MG5_dyn_bare->FindBin(bin_low);
	h_lepeta_LO_MG5_dyn_bare->SetBinContent(ibin, h_lepeta_LO_MG5_dyn_bare->GetBinContent(ibin)+n_bin);
	h_lepeta_LO_MG5_dyn_bare->SetBinError(ibin, sqrt(pow(h_lepeta_LO_MG5_dyn_bare->GetBinError(ibin),2)+pow(e_bin,2)));
    }
    for (int i = 1; i < h_jet1pt_LO_MG5_dyn_raw->GetNbinsX(); i++) {
	double bin_low = h_jet1pt_LO_MG5_dyn_raw->GetXaxis()->GetBinLowEdge(i);
	double n_bin = h_jet1pt_LO_MG5_dyn_raw->GetBinContent(i);
	double e_bin = h_jet1pt_LO_MG5_dyn_raw->GetBinError(i);
	int ibin = h_jet1pt_LO_MG5_dyn_bare->FindBin(bin_low);
	if (ibin > h_jet1pt_LO_MG5_dyn_bare->GetNbinsX()) ibin = h_jet1pt_LO_MG5_dyn_bare->GetNbinsX();
	h_jet1pt_LO_MG5_dyn_bare->SetBinContent(ibin, h_jet1pt_LO_MG5_dyn_bare->GetBinContent(ibin)+n_bin);
	h_jet1pt_LO_MG5_dyn_bare->SetBinError(ibin, sqrt(pow(h_jet1pt_LO_MG5_dyn_bare->GetBinError(ibin),2)+pow(e_bin,2)));
    }
    for (int i = 1; i < h_jet1eta_LO_MG5_dyn_raw->GetNbinsX(); i++) {
	double bin_low = h_jet1eta_LO_MG5_dyn_raw->GetXaxis()->GetBinLowEdge(i);
	double n_bin = h_jet1eta_LO_MG5_dyn_raw->GetBinContent(i);
	double e_bin = h_jet1eta_LO_MG5_dyn_raw->GetBinError(i);
	int ibin = h_jet1eta_LO_MG5_dyn_bare->FindBin(bin_low);
	h_jet1eta_LO_MG5_dyn_bare->SetBinContent(ibin, h_jet1eta_LO_MG5_dyn_bare->GetBinContent(ibin)+n_bin);
	h_jet1eta_LO_MG5_dyn_bare->SetBinError(ibin, sqrt(pow(h_jet1eta_LO_MG5_dyn_bare->GetBinError(ibin),2)+pow(e_bin,2)));
    }
    for (int i = 1; i < h_jet2pt_LO_MG5_dyn_raw->GetNbinsX(); i++) {
	double bin_low = h_jet2pt_LO_MG5_dyn_raw->GetXaxis()->GetBinLowEdge(i);
	double n_bin = h_jet2pt_LO_MG5_dyn_raw->GetBinContent(i);
	double e_bin = h_jet2pt_LO_MG5_dyn_raw->GetBinError(i);
	int ibin = h_jet2pt_LO_MG5_dyn_bare->FindBin(bin_low);
	if (ibin > h_jet2pt_LO_MG5_dyn_bare->GetNbinsX()) ibin = h_jet2pt_LO_MG5_dyn_bare->GetNbinsX();
	h_jet2pt_LO_MG5_dyn_bare->SetBinContent(ibin, h_jet2pt_LO_MG5_dyn_bare->GetBinContent(ibin)+n_bin);
	h_jet2pt_LO_MG5_dyn_bare->SetBinError(ibin, sqrt(pow(h_jet2pt_LO_MG5_dyn_bare->GetBinError(ibin),2)+pow(e_bin,2)));
    }
    for (int i = 1; i < h_jet2eta_LO_MG5_dyn_raw->GetNbinsX(); i++) {
	double bin_low = h_jet2eta_LO_MG5_dyn_raw->GetXaxis()->GetBinLowEdge(i);
	double n_bin = h_jet2eta_LO_MG5_dyn_raw->GetBinContent(i);
	double e_bin = h_jet2eta_LO_MG5_dyn_raw->GetBinError(i);
	int ibin = h_jet2eta_LO_MG5_dyn_bare->FindBin(bin_low);
	h_jet2eta_LO_MG5_dyn_bare->SetBinContent(ibin, h_jet2eta_LO_MG5_dyn_bare->GetBinContent(ibin)+n_bin);
	h_jet2eta_LO_MG5_dyn_bare->SetBinError(ibin, sqrt(pow(h_jet2eta_LO_MG5_dyn_bare->GetBinError(ibin),2)+pow(e_bin,2)));
    }
    for (int i = 1; i < h_jet3pt_LO_MG5_dyn_raw->GetNbinsX(); i++) {
	double bin_low = h_jet3pt_LO_MG5_dyn_raw->GetXaxis()->GetBinLowEdge(i);
	double n_bin = h_jet3pt_LO_MG5_dyn_raw->GetBinContent(i);
	double e_bin = h_jet3pt_LO_MG5_dyn_raw->GetBinError(i);
	int ibin = h_jet3pt_LO_MG5_dyn_bare->FindBin(bin_low);
	if (ibin > h_jet3pt_LO_MG5_dyn_bare->GetNbinsX()) ibin = h_jet3pt_LO_MG5_dyn_bare->GetNbinsX();
	h_jet3pt_LO_MG5_dyn_bare->SetBinContent(ibin, h_jet3pt_LO_MG5_dyn_bare->GetBinContent(ibin)+n_bin);
	h_jet3pt_LO_MG5_dyn_bare->SetBinError(ibin, sqrt(pow(h_jet3pt_LO_MG5_dyn_bare->GetBinError(ibin),2)+pow(e_bin,2)));
    }
    for (int i = 1; i < h_jet3eta_LO_MG5_dyn_raw->GetNbinsX(); i++) {
	double bin_low = h_jet3eta_LO_MG5_dyn_raw->GetXaxis()->GetBinLowEdge(i);
	double n_bin = h_jet3eta_LO_MG5_dyn_raw->GetBinContent(i);
	double e_bin = h_jet3eta_LO_MG5_dyn_raw->GetBinError(i);
	int ibin = h_jet3eta_LO_MG5_dyn_bare->FindBin(bin_low);
	h_jet3eta_LO_MG5_dyn_bare->SetBinContent(ibin, h_jet3eta_LO_MG5_dyn_bare->GetBinContent(ibin)+n_bin);
	h_jet3eta_LO_MG5_dyn_bare->SetBinError(ibin, sqrt(pow(h_jet3eta_LO_MG5_dyn_bare->GetBinError(ibin),2)+pow(e_bin,2)));
    }
    for (int i = 1; i < h_jet4pt_LO_MG5_dyn_raw->GetNbinsX(); i++) {
	double bin_low = h_jet4pt_LO_MG5_dyn_raw->GetXaxis()->GetBinLowEdge(i);
	double n_bin = h_jet4pt_LO_MG5_dyn_raw->GetBinContent(i);
	double e_bin = h_jet4pt_LO_MG5_dyn_raw->GetBinError(i);
	int ibin = h_jet4pt_LO_MG5_dyn_bare->FindBin(bin_low);
	if (ibin > h_jet4pt_LO_MG5_dyn_bare->GetNbinsX()) ibin = h_jet4pt_LO_MG5_dyn_bare->GetNbinsX();
	h_jet4pt_LO_MG5_dyn_bare->SetBinContent(ibin, h_jet4pt_LO_MG5_dyn_bare->GetBinContent(ibin)+n_bin);
	h_jet4pt_LO_MG5_dyn_bare->SetBinError(ibin, sqrt(pow(h_jet4pt_LO_MG5_dyn_bare->GetBinError(ibin),2)+pow(e_bin,2)));
    }
    for (int i = 1; i < h_jet4eta_LO_MG5_dyn_raw->GetNbinsX(); i++) {
	double bin_low = h_jet4eta_LO_MG5_dyn_raw->GetXaxis()->GetBinLowEdge(i);
	double n_bin = h_jet4eta_LO_MG5_dyn_raw->GetBinContent(i);
	double e_bin = h_jet4eta_LO_MG5_dyn_raw->GetBinError(i);
	int ibin = h_jet4eta_LO_MG5_dyn_bare->FindBin(bin_low);
	h_jet4eta_LO_MG5_dyn_bare->SetBinContent(ibin, h_jet4eta_LO_MG5_dyn_bare->GetBinContent(ibin)+n_bin);
	h_jet4eta_LO_MG5_dyn_bare->SetBinError(ibin, sqrt(pow(h_jet4eta_LO_MG5_dyn_bare->GetBinError(ibin),2)+pow(e_bin,2)));
    }
    }

    h_phpt_LO_MG5_dyn_bare->Scale(10./11.);    h_pheta_LO_MG5_dyn_bare->Scale(10./11.);
    h_leppt_LO_MG5_dyn_bare->Scale(10./11.);    h_lepeta_LO_MG5_dyn_bare->Scale(10./11.);
    h_jet1pt_LO_MG5_dyn_bare->Scale(10./11.);    h_jet1eta_LO_MG5_dyn_bare->Scale(10./11.);
    h_jet2pt_LO_MG5_dyn_bare->Scale(10./11.);    h_jet2eta_LO_MG5_dyn_bare->Scale(10./11.);
    h_jet3pt_LO_MG5_dyn_bare->Scale(10./11.);    h_jet3eta_LO_MG5_dyn_bare->Scale(10./11.);
    h_jet4pt_LO_MG5_dyn_bare->Scale(10./11.);    h_jet4eta_LO_MG5_dyn_bare->Scale(10./11.);
    h_phpt_LO_MG5_dyn_bare->SetLineWidth(2); h_pheta_LO_MG5_dyn_bare->SetLineWidth(2);
    h_leppt_LO_MG5_dyn_bare->SetLineWidth(2); h_lepeta_LO_MG5_dyn_bare->SetLineWidth(2);
    h_jet1pt_LO_MG5_dyn_bare->SetLineWidth(2); h_jet1eta_LO_MG5_dyn_bare->SetLineWidth(2);
    h_jet2pt_LO_MG5_dyn_bare->SetLineWidth(2); h_jet2eta_LO_MG5_dyn_bare->SetLineWidth(2);
    h_jet3pt_LO_MG5_dyn_bare->SetLineWidth(2); h_jet3eta_LO_MG5_dyn_bare->SetLineWidth(2);
    h_jet4pt_LO_MG5_dyn_bare->SetLineWidth(2); h_jet4eta_LO_MG5_dyn_bare->SetLineWidth(2);

    PlotComparor* PC = new PlotComparor();
    PC->DrawRatio(true);
    PC->SetSaveDir("plots/");
    PC->SetChannel("tt->ej");
    PC->SetRatioYtitle("Ratio");    
    PC->ShowOverflow(false);
    PC->NormToUnit(true);

    PC->ClearAlterHs();
    PC->SetBaseH(h_phpt_LO_MG5_dyn_bare);
    PC->SetBaseHName("ME");
    PC->AddAlterH(h_phpt_LO_MG5_dyn);
    PC->AddAlterHName("MEPS");
    PC->AddAlterH(h_phpt_LO_MG5_dyn_jet03);
    PC->AddAlterHName("MEPS j03");
    PC->AddAlterH(h_phpt_LO_MG5_dyn_jet05);
    PC->AddAlterHName("MEPS j05");
    PC->AddAlterH(h_phpt_LO_MG5_dyn_noHD);
    PC->AddAlterHName("MEPS no Hard.");
    PC->AddAlterH(h_phpt_LO_MG5_dyn_noUE);
    PC->AddAlterHName("MEPS low UE");
    PC->SetXtitle("phpt");
    PC->SetYtitle("Arbi.");
    PC->SetMinRatio(0.4);
    PC->SetMaxRatio(1.6);
    PC->UseLogy(true);
    PC->LogyMin(0.01);
    PC->SetSaveName("Kfactor_sl_phpt");
    PC->SetLogo("tty Kfactor");
    PC->ShiftLegendX(-0.2);
    PC->SetLegendNC(2);
    PC->Compare();

    PC->ClearAlterHs();
    PC->SetBaseH(h_pheta_LO_MG5_dyn_bare);
    PC->SetBaseHName("ME");
    PC->AddAlterH(h_pheta_LO_MG5_dyn);
    PC->AddAlterHName("MEPS");
    PC->AddAlterH(h_pheta_LO_MG5_dyn_jet03);
    PC->AddAlterHName("MEPS j03");
    PC->AddAlterH(h_pheta_LO_MG5_dyn_jet05);
    PC->AddAlterHName("MEPS j05");
    PC->AddAlterH(h_pheta_LO_MG5_dyn_noHD);
    PC->AddAlterHName("MEPS no Hard.");
    PC->AddAlterH(h_pheta_LO_MG5_dyn_noUE);
    PC->AddAlterHName("MEPS low UE");
    PC->SetXtitle("pheta");
    PC->SetYtitle("Arbi.");
    PC->SetMinRatio(0.4);
    PC->SetMaxRatio(1.6);
    PC->UseLogy(true);
    PC->LogyMin(0.01);
    PC->SetSaveName("Kfactor_sl_pheta");
    PC->SetLogo("tty Kfactor");
    PC->ShiftLegendX(-0.2);
    PC->SetLegendNC(2);
    PC->Compare();

    PC->ClearAlterHs();
    PC->SetBaseH(h_leppt_LO_MG5_dyn_bare);
    PC->SetBaseHName("ME");
    PC->AddAlterH(h_leppt_LO_MG5_dyn);
    PC->AddAlterHName("MEPS");
    PC->AddAlterH(h_leppt_LO_MG5_dyn_jet03);
    PC->AddAlterHName("MEPS j03");
    PC->AddAlterH(h_leppt_LO_MG5_dyn_jet05);
    PC->AddAlterHName("MEPS j05");
    PC->AddAlterH(h_leppt_LO_MG5_dyn_noHD);
    PC->AddAlterHName("MEPS no Hard.");
    PC->AddAlterH(h_leppt_LO_MG5_dyn_noUE);
    PC->AddAlterHName("MEPS low UE");
    PC->SetXtitle("leppt");
    PC->SetYtitle("Arbi.");
    PC->SetMinRatio(0.4);
    PC->SetMaxRatio(1.6);
    PC->UseLogy(true);
    PC->LogyMin(0.01);
    PC->SetSaveName("Kfactor_sl_leppt");
    PC->SetLogo("tty Kfactor");
    PC->ShiftLegendX(-0.2);
    PC->SetLegendNC(2);
    PC->Compare();

    PC->ClearAlterHs();
    PC->SetBaseH(h_lepeta_LO_MG5_dyn_bare);
    PC->SetBaseHName("ME");
    PC->AddAlterH(h_lepeta_LO_MG5_dyn);
    PC->AddAlterHName("MEPS");
    PC->AddAlterH(h_lepeta_LO_MG5_dyn_jet03);
    PC->AddAlterHName("MEPS j03");
    PC->AddAlterH(h_lepeta_LO_MG5_dyn_jet05);
    PC->AddAlterHName("MEPS j05");
    PC->AddAlterH(h_lepeta_LO_MG5_dyn_noHD);
    PC->AddAlterHName("MEPS no Hard.");
    PC->AddAlterH(h_lepeta_LO_MG5_dyn_noUE);
    PC->AddAlterHName("MEPS low UE");
    PC->SetXtitle("lepeta");
    PC->SetYtitle("Arbi.");
    PC->SetMinRatio(0.4);
    PC->SetMaxRatio(1.6);
    PC->UseLogy(true);
    PC->LogyMin(0.01);
    PC->SetSaveName("Kfactor_sl_lepeta");
    PC->SetLogo("tty Kfactor");
    PC->ShiftLegendX(-0.2);
    PC->SetLegendNC(2);
    PC->Compare();

    PC->ClearAlterHs();
    PC->SetBaseH(h_jet1pt_LO_MG5_dyn_bare);
    PC->SetBaseHName("ME");
    PC->AddAlterH(h_jet1pt_LO_MG5_dyn);
    PC->AddAlterHName("MEPS");
    PC->AddAlterH(h_jet1pt_LO_MG5_dyn_jet03);
    PC->AddAlterHName("MEPS j03");
    PC->AddAlterH(h_jet1pt_LO_MG5_dyn_jet05);
    PC->AddAlterHName("MEPS j05");
    PC->AddAlterH(h_jet1pt_LO_MG5_dyn_noHD);
    PC->AddAlterHName("MEPS no Hard.");
    PC->AddAlterH(h_jet1pt_LO_MG5_dyn_noUE);
    PC->AddAlterHName("MEPS low UE");
    PC->SetXtitle("jet1pt");
    PC->SetYtitle("Arbi.");
    PC->SetMinRatio(0.4);
    PC->SetMaxRatio(1.6);
    PC->UseLogy(true);
    PC->LogyMin(0.01);
    PC->SetSaveName("Kfactor_sl_jet1pt");
    PC->SetLogo("tty Kfactor");
    PC->ShiftLegendX2(0.1);
    PC->ShiftLogoX(0.15);
    PC->SetLegendNC(2);
    PC->Compare();

    PC->ClearAlterHs();
    PC->SetBaseH(h_jet1eta_LO_MG5_dyn_bare);
    PC->SetBaseHName("ME");
    PC->AddAlterH(h_jet1eta_LO_MG5_dyn);
    PC->AddAlterHName("MEPS");
    PC->AddAlterH(h_jet1eta_LO_MG5_dyn_jet03);
    PC->AddAlterHName("MEPS j03");
    PC->AddAlterH(h_jet1eta_LO_MG5_dyn_jet05);
    PC->AddAlterHName("MEPS j05");
    PC->AddAlterH(h_jet1eta_LO_MG5_dyn_noHD);
    PC->AddAlterHName("MEPS no Hard.");
    PC->AddAlterH(h_jet1eta_LO_MG5_dyn_noUE);
    PC->AddAlterHName("MEPS low UE");
    PC->SetXtitle("jet1eta");
    PC->SetYtitle("Arbi.");
    PC->SetMinRatio(0.4);
    PC->SetMaxRatio(1.6);
    PC->UseLogy(true);
    PC->LogyMin(0.01);
    PC->SetSaveName("Kfactor_sl_jet1eta");
    PC->SetLogo("tty Kfactor");
    PC->ShiftLegendX(-0.2);
    PC->SetLegendNC(2);
    PC->Compare();

    PC->ClearAlterHs();
    PC->SetBaseH(h_jet2pt_LO_MG5_dyn_bare);
    PC->SetBaseHName("ME");
    PC->AddAlterH(h_jet2pt_LO_MG5_dyn);
    PC->AddAlterHName("MEPS");
    PC->AddAlterH(h_jet2pt_LO_MG5_dyn_jet03);
    PC->AddAlterHName("MEPS j03");
    PC->AddAlterH(h_jet2pt_LO_MG5_dyn_jet05);
    PC->AddAlterHName("MEPS j05");
    PC->AddAlterH(h_jet2pt_LO_MG5_dyn_noHD);
    PC->AddAlterHName("MEPS no Hard.");
    PC->AddAlterH(h_jet2pt_LO_MG5_dyn_noUE);
    PC->AddAlterHName("MEPS low UE");
    PC->SetXtitle("jet2pt");
    PC->SetYtitle("Arbi.");
    PC->SetMinRatio(0.4);
    PC->SetMaxRatio(1.6);
    PC->UseLogy(true);
    PC->LogyMin(0.01);
    PC->SetSaveName("Kfactor_sl_jet2pt");
    PC->SetLogo("tty Kfactor");
    PC->ShiftLegendX(-0.2);
    PC->SetLegendNC(2);
    PC->Compare();

    PC->ClearAlterHs();
    PC->SetBaseH(h_jet2eta_LO_MG5_dyn_bare);
    PC->SetBaseHName("ME");
    PC->AddAlterH(h_jet2eta_LO_MG5_dyn);
    PC->AddAlterHName("MEPS");
    PC->AddAlterH(h_jet2eta_LO_MG5_dyn_jet03);
    PC->AddAlterHName("MEPS j03");
    PC->AddAlterH(h_jet2eta_LO_MG5_dyn_jet05);
    PC->AddAlterHName("MEPS j05");
    PC->AddAlterH(h_jet2eta_LO_MG5_dyn_noHD);
    PC->AddAlterHName("MEPS no Hard.");
    PC->AddAlterH(h_jet2eta_LO_MG5_dyn_noUE);
    PC->AddAlterHName("MEPS low UE");
    PC->SetXtitle("jet2eta");
    PC->SetYtitle("Arbi.");
    PC->SetMinRatio(0.4);
    PC->SetMaxRatio(1.6);
    PC->UseLogy(true);
    PC->LogyMin(0.01);
    PC->SetSaveName("Kfactor_sl_jet2eta");
    PC->SetLogo("tty Kfactor");
    PC->ShiftLegendX(-0.2);
    PC->SetLegendNC(2);
    PC->Compare();

    PC->ClearAlterHs();
    PC->SetBaseH(h_jet3pt_LO_MG5_dyn_bare);
    PC->SetBaseHName("ME");
    PC->AddAlterH(h_jet3pt_LO_MG5_dyn);
    PC->AddAlterHName("MEPS");
    PC->AddAlterH(h_jet3pt_LO_MG5_dyn_jet03);
    PC->AddAlterHName("MEPS j03");
    PC->AddAlterH(h_jet3pt_LO_MG5_dyn_jet05);
    PC->AddAlterHName("MEPS j05");
    PC->AddAlterH(h_jet3pt_LO_MG5_dyn_noHD);
    PC->AddAlterHName("MEPS no Hard.");
    PC->AddAlterH(h_jet3pt_LO_MG5_dyn_noUE);
    PC->AddAlterHName("MEPS low UE");
    PC->SetXtitle("jet3pt");
    PC->SetYtitle("Arbi.");
    PC->SetMinRatio(0.5);
    PC->SetMaxRatio(2.5);
    PC->UseLogy(true);
    PC->LogyMin(0.001);
    PC->SetSaveName("Kfactor_sl_jet3pt");
    PC->SetLogo("tty Kfactor");
    PC->ShiftLegendX(-0.2);
    PC->SetLegendNC(2);
    PC->Compare();

    PC->ClearAlterHs();
    PC->SetBaseH(h_jet3eta_LO_MG5_dyn_bare);
    PC->SetBaseHName("ME");
    PC->AddAlterH(h_jet3eta_LO_MG5_dyn);
    PC->AddAlterHName("MEPS");
    PC->AddAlterH(h_jet3eta_LO_MG5_dyn_jet03);
    PC->AddAlterHName("MEPS j03");
    PC->AddAlterH(h_jet3eta_LO_MG5_dyn_jet05);
    PC->AddAlterHName("MEPS j05");
    PC->AddAlterH(h_jet3eta_LO_MG5_dyn_noHD);
    PC->AddAlterHName("MEPS no Hard.");
    PC->AddAlterH(h_jet3eta_LO_MG5_dyn_noUE);
    PC->AddAlterHName("MEPS low UE");
    PC->SetXtitle("jet3eta");
    PC->SetYtitle("Arbi.");
    PC->SetMinRatio(0.5);
    PC->SetMaxRatio(2.0);
    PC->UseLogy(true);
    PC->LogyMin(0.01);
    PC->SetSaveName("Kfactor_sl_jet3eta");
    PC->SetLogo("tty Kfactor");
    PC->ShiftLegendX(-0.2);
    PC->SetLegendNC(2);
    PC->Compare();

    PC->ClearAlterHs();
    PC->SetBaseH(h_jet4pt_LO_MG5_dyn_bare);
    PC->SetBaseHName("ME");
    PC->AddAlterH(h_jet4pt_LO_MG5_dyn);
    PC->AddAlterHName("MEPS");
    PC->AddAlterH(h_jet4pt_LO_MG5_dyn_jet03);
    PC->AddAlterHName("MEPS j03");
    PC->AddAlterH(h_jet4pt_LO_MG5_dyn_jet05);
    PC->AddAlterHName("MEPS j05");
    PC->AddAlterH(h_jet4pt_LO_MG5_dyn_noHD);
    PC->AddAlterHName("MEPS no Hard.");
    PC->AddAlterH(h_jet4pt_LO_MG5_dyn_noUE);
    PC->AddAlterHName("MEPS low UE");
    PC->SetXtitle("jet4pt");
    PC->SetYtitle("Arbi.");
    PC->SetMinRatio(0.4);
    PC->SetMaxRatio(4.4);
    PC->UseLogy(true);
    PC->LogyMin(0.001);
    PC->SetSaveName("Kfactor_sl_jet4pt");
    PC->SetLogo("tty Kfactor");
    PC->ShiftLegendX(-0.2);
    PC->SetLegendNC(2);
    PC->Compare();

    PC->ClearAlterHs();
    PC->SetBaseH(h_jet4eta_LO_MG5_dyn_bare);
    PC->SetBaseHName("ME");
    PC->AddAlterH(h_jet4eta_LO_MG5_dyn);
    PC->AddAlterHName("MEPS");
    PC->AddAlterH(h_jet4eta_LO_MG5_dyn_jet03);
    PC->AddAlterHName("MEPS j03");
    PC->AddAlterH(h_jet4eta_LO_MG5_dyn_jet05);
    PC->AddAlterHName("MEPS j05");
    PC->AddAlterH(h_jet4eta_LO_MG5_dyn_noHD);
    PC->AddAlterHName("MEPS no Hard.");
    PC->AddAlterH(h_jet4eta_LO_MG5_dyn_noUE);
    PC->AddAlterHName("MEPS low UE");
    PC->SetXtitle("jet4eta");
    PC->SetYtitle("Arbi.");
    PC->SetMinRatio(0.4);
    PC->SetMaxRatio(1.6);
    PC->UseLogy(true);
    PC->LogyMin(0.01);
    PC->SetSaveName("Kfactor_sl_jet4eta");
    PC->SetLogo("tty Kfactor");
    PC->ShiftLegendX(-0.2);
    PC->SetLegendNC(2);
    PC->Compare();

    PC->Print();

}

void GetHistFromYoda(string input, TH1F* h_phpt, TH1F* h_pheta, TH1F* h_leppt, TH1F* h_lepeta, TH1F* h_jet1pt, TH1F* h_jet1eta, TH1F* h_jet2pt, 
		    TH1F* h_jet2eta, TH1F* h_jet3pt, TH1F* h_jet3eta, TH1F* h_jet4pt, TH1F* h_jet4eta) {
    ifstream ifile; ifile.open(input.c_str());
    //"/afs/cern.ch/work/y/yili/private/Analysis/Rivet/ttgamma8TeV/kfactor_ll_grid/Merged.yoda");
    h_phpt->SetLineWidth(2); h_pheta->SetLineWidth(2);
    h_leppt->SetLineWidth(2); h_lepeta->SetLineWidth(2);
    h_jet1pt->SetLineWidth(2); h_jet1eta->SetLineWidth(2);
    h_jet2pt->SetLineWidth(2); h_jet2eta->SetLineWidth(2);
    h_jet3pt->SetLineWidth(2); h_jet3eta->SetLineWidth(2);
    h_jet4pt->SetLineWidth(2); h_jet4eta->SetLineWidth(2);
    string line;
    bool isphpt = false;
    bool ispheta = false;
    bool isleppt = false;
    bool islepeta = false;
    bool isjet1pt = false;
    bool isjet1eta = false;
    bool isjet2pt = false;
    bool isjet2eta = false;
    bool isjet3pt = false;
    bool isjet3eta = false;
    bool isjet4pt = false;
    bool isjet4eta = false;
    while (getline(ifile,line)) {
	if (line.size() == 0) continue;

	if (line.find("BEGIN",0) != string::npos) {
	    if (line.find("photon_pT",0) != string::npos) isphpt = true;
	    else isphpt = false;
	    if (line.find("photon_eta",0) != string::npos) ispheta = true;
	    else ispheta = false;
	    if (line.find("lepton_pT",0) != string::npos) isleppt = true;
	    else isleppt = false;
	    if (line.find("lepton_eta",0) != string::npos) islepeta = true;
	    else islepeta = false;
	    if (line.find("jet1_pT",0) != string::npos) isjet1pt = true;
	    else isjet1pt = false;
	    if (line.find("jet1_eta",0) != string::npos) isjet1eta = true;
	    else isjet1eta = false;
	    if (line.find("jet2_pT",0) != string::npos) isjet2pt = true;
	    else isjet2pt = false;
	    if (line.find("jet2_eta",0) != string::npos) isjet2eta = true;
	    else isjet2eta = false;
	    if (line.find("jet3_pT",0) != string::npos) isjet3pt = true;
	    else isjet3pt = false;
	    if (line.find("jet3_eta",0) != string::npos) isjet3eta = true;
	    else isjet3eta = false;
	    if (line.find("jet4_pT",0) != string::npos) isjet4pt = true;
	    else isjet4pt = false;
	    if (line.find("jet4_eta",0) != string::npos) isjet4eta = true;
	    else isjet4eta = false;
	}

	if (line.find("BEGIN",0) != string::npos) continue;
	if (line.find("Path",0) != string::npos) continue;
	if (line.find("Title",0) != string::npos) continue;
	if (line.find("Type",0) != string::npos) continue;
	if (line.find("XLabel",0) != string::npos) continue;
	if (line.find("YLabel",0) != string::npos) continue;
	if (line.find("Total",0) != string::npos) continue;
	if (line.find("Underflow",0) != string::npos) continue;
	if (line.find("Overflow",0) != string::npos) continue;
	if (line.find("END",0) != string::npos) continue;
	if (line.find("#",0) != string::npos) continue;

        istringstream iss(line);
        double bin_low = -1;
        double n_bin = -1;
        int idx = 0;
        for (string word; iss >> word;) {
            if (idx == 0) bin_low = atof(word.c_str());
            if (idx == 6) n_bin = atof(word.c_str());
            idx++;
        }
	if (isphpt) {
	    int ibin = h_phpt->FindBin(bin_low);
	    if (ibin > h_phpt->GetNbinsX()) ibin = h_phpt->GetNbinsX();
	    h_phpt->SetBinContent(ibin, h_phpt->GetBinContent(ibin)+n_bin);
	    h_phpt->SetBinError(ibin, sqrt(pow(h_phpt->GetBinError(ibin),2)+n_bin));
	}
	if (ispheta) {
	    int ibin = h_pheta->FindBin(bin_low);
	    h_pheta->SetBinContent(ibin, h_pheta->GetBinContent(ibin)+n_bin);
	    h_pheta->SetBinError(ibin, sqrt(pow(h_pheta->GetBinError(ibin),2)+n_bin));
	}
	if (isleppt) {
	    int ibin = h_leppt->FindBin(bin_low);
	    if (ibin > h_leppt->GetNbinsX()) ibin = h_leppt->GetNbinsX();
	    h_leppt->SetBinContent(ibin, h_leppt->GetBinContent(ibin)+n_bin);
	    h_leppt->SetBinError(ibin, sqrt(pow(h_leppt->GetBinError(ibin),2)+n_bin));
	}
	if (islepeta) {
	    int ibin = h_lepeta->FindBin(bin_low);
	    h_lepeta->SetBinContent(ibin, h_lepeta->GetBinContent(ibin)+n_bin);
	    h_lepeta->SetBinError(ibin, sqrt(pow(h_lepeta->GetBinError(ibin),2)+n_bin));
	}
	if (isjet1pt) {
	    int ibin = h_jet1pt->FindBin(bin_low);
	    if (ibin > h_jet1pt->GetNbinsX()) ibin = h_jet1pt->GetNbinsX();
	    h_jet1pt->SetBinContent(ibin, h_jet1pt->GetBinContent(ibin)+n_bin);
	    h_jet1pt->SetBinError(ibin, sqrt(pow(h_jet1pt->GetBinError(ibin),2)+n_bin));
	}
	if (isjet1eta) {
	    int ibin = h_jet1eta->FindBin(bin_low);
	    h_jet1eta->SetBinContent(ibin, h_jet1eta->GetBinContent(ibin)+n_bin);
	    h_jet1eta->SetBinError(ibin, sqrt(pow(h_jet1eta->GetBinError(ibin),2)+n_bin));
	}
	if (isjet2pt) {
	    int ibin = h_jet2pt->FindBin(bin_low);
	    if (ibin > h_jet2pt->GetNbinsX()) ibin = h_jet2pt->GetNbinsX();
	    h_jet2pt->SetBinContent(ibin, h_jet2pt->GetBinContent(ibin)+n_bin);
	    h_jet2pt->SetBinError(ibin, sqrt(pow(h_jet2pt->GetBinError(ibin),2)+n_bin));
	}
	if (isjet2eta) {
	    int ibin = h_jet2eta->FindBin(bin_low);
	    h_jet2eta->SetBinContent(ibin, h_jet2eta->GetBinContent(ibin)+n_bin);
	    h_jet2eta->SetBinError(ibin, sqrt(pow(h_jet2eta->GetBinError(ibin),2)+n_bin));
	}
	if (isjet3pt) {
	    int ibin = h_jet3pt->FindBin(bin_low);
	    if (ibin > h_jet3pt->GetNbinsX()) ibin = h_jet3pt->GetNbinsX();
	    h_jet3pt->SetBinContent(ibin, h_jet3pt->GetBinContent(ibin)+n_bin);
	    h_jet3pt->SetBinError(ibin, sqrt(pow(h_jet3pt->GetBinError(ibin),2)+n_bin));
	}
	if (isjet3eta) {
	    int ibin = h_jet3eta->FindBin(bin_low);
	    h_jet3eta->SetBinContent(ibin, h_jet3eta->GetBinContent(ibin)+n_bin);
	    h_jet3eta->SetBinError(ibin, sqrt(pow(h_jet3eta->GetBinError(ibin),2)+n_bin));
	}
	if (isjet4pt) {
	    int ibin = h_jet4pt->FindBin(bin_low);
	    if (ibin > h_jet4pt->GetNbinsX()) ibin = h_jet4pt->GetNbinsX();
	    h_jet4pt->SetBinContent(ibin, h_jet4pt->GetBinContent(ibin)+n_bin);
	    h_jet4pt->SetBinError(ibin, sqrt(pow(h_jet4pt->GetBinError(ibin),2)+n_bin));
	}
	if (isjet4eta) {
	    int ibin = h_jet4eta->FindBin(bin_low);
	    h_jet4eta->SetBinContent(ibin, h_jet4eta->GetBinContent(ibin)+n_bin);
	    h_jet4eta->SetBinError(ibin, sqrt(pow(h_jet4eta->GetBinError(ibin),2)+n_bin));
	}
    }
}
