#include <iostream>
#include <TLatex.h>
#include <TCanvas.h>
#include <iomanip>
#include <TFile.h>
#include <TH1F.h>
#include <vector>
#include "StringPlayer.h"
#include "EDM.h"
#include "PlotComparor.h"
#include <TLine.h>

using namespace std;

void DoComparison(TString ps, TString chan, TString tag = "", bool dopeak = false);

int main()
{
    TString var_y = "LeadPhMinDrPhJetCoarse";
    TString var_e = "SubLepMinDrPhJetCoarse";
    TString var_save = "DR";

    //TString var_y = "Njet";
    //TString var_e = "Njet";
    //TString var_save = "Njet";

    TFile* f_zeg_TypeA = new TFile("results/Nominal/13TeV_CutZeg_PhMatch_EFakeTypeA_ZjetsElEl_Reco_EF1_zeg_Nominal_Final04.root");
    TFile* f_zeg_TypeB = new TFile("results/Nominal/13TeV_CutZeg_PhMatch_EFakeTypeB_ZjetsElEl_Reco_EF1_zeg_Nominal_Final04.root");
    TFile* f_zee = new TFile("results/Nominal/13TeV_CutZee_ZjetsElEl_Reco_EF1_zee_Nominal_Final03.root");

    TFile* f_tteg_TypeA = new TFile("results/Nominal/13TeV_CutTTeg_PhMatch_EFakeTypeA_TTBar_Reco_CR1_ejets_Nominal_Final04.root");
    TFile* f_tteg_TypeB = new TFile("results/Nominal/13TeV_CutTTeg_PhMatch_EFakeTypeB_TTBar_Reco_CR1_ejets_Nominal_Final04.root");
    TFile* f_ttee = new TFile("results/Nominal/13TeV_CutTTee_TTBar_Reco_EF1_ttel_ee_Nominal_Final04.root");

    TFile* f_ttmug_TypeA = new TFile("results/Nominal/13TeV_CutTTmug_PhMatch_EFakeTypeA_TTBar_Reco_CR1_mujets_Nominal_Final04.root");
    TFile* f_ttmug_TypeB = new TFile("results/Nominal/13TeV_CutTTmug_PhMatch_EFakeTypeB_TTBar_Reco_CR1_mujets_Nominal_Final04.root");
    TFile* f_ttmue = new TFile("results/Nominal/13TeV_CutTTmue_TTBar_Reco_EF1_ttel_mue_Nominal_Final04.root");

    TH1F* h_zeg_TypeA = (TH1F*)f_zeg_TypeA->Get(var_y+"_13TeV_CutZeg_PhMatch_EFakeTypeA_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TH1F* h_zeg_TypeB = (TH1F*)f_zeg_TypeB->Get(var_y+"_13TeV_CutZeg_PhMatch_EFakeTypeB_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TH1F* h_zee = (TH1F*)f_zee->Get(var_e+"_13TeV_CutZee_ZjetsElEl_Reco_EF1_zee_Nominal");

    TH1F* h_tteg_TypeA = (TH1F*)f_tteg_TypeA->Get(var_y+"_13TeV_CutTTeg_PhMatch_EFakeTypeA_TTBar_Reco_CR1_ejets_Nominal");
    TH1F* h_tteg_TypeB = (TH1F*)f_tteg_TypeB->Get(var_y+"_13TeV_CutTTeg_PhMatch_EFakeTypeB_TTBar_Reco_CR1_ejets_Nominal");
    TH1F* h_ttee = (TH1F*)f_ttee->Get(var_e+"_13TeV_CutTTee_TTBar_Reco_EF1_ttel_ee_Nominal");

    TH1F* h_ttmug_TypeA = (TH1F*)f_ttmug_TypeA->Get(var_y+"_13TeV_CutTTmug_PhMatch_EFakeTypeA_TTBar_Reco_CR1_mujets_Nominal");
    TH1F* h_ttmug_TypeB = (TH1F*)f_ttmug_TypeB->Get(var_y+"_13TeV_CutTTmug_PhMatch_EFakeTypeB_TTBar_Reco_CR1_mujets_Nominal");
    TH1F* h_ttmue = (TH1F*)f_ttmue->Get(var_e+"_13TeV_CutTTmue_TTBar_Reco_EF1_ttel_mue_Nominal");

    TH1F* fake_z_TypeA = (TH1F*)h_zeg_TypeA->Clone(); fake_z_TypeA->Divide(h_zee);
    TH1F* fake_z_TypeB = (TH1F*)h_zeg_TypeB->Clone(); fake_z_TypeB->Divide(h_zee);
    TH1F* fake_ttee_TypeA = (TH1F*)h_tteg_TypeA->Clone(); fake_ttee_TypeA->Divide(h_ttee);
    TH1F* fake_ttee_TypeB = (TH1F*)h_tteg_TypeB->Clone(); fake_ttee_TypeB->Divide(h_ttee);
    TH1F* fake_ttmue_TypeA = (TH1F*)h_ttmug_TypeA->Clone(); fake_ttmue_TypeA->Divide(h_ttmue);
    TH1F* fake_ttmue_TypeB = (TH1F*)h_ttmug_TypeB->Clone(); fake_ttmue_TypeB->Divide(h_ttmue);

    fake_z_TypeA->SetLineWidth(2);
    fake_z_TypeB->SetLineWidth(2);
    fake_ttee_TypeA->SetLineWidth(2);
    fake_ttee_TypeB->SetLineWidth(2);
    fake_ttmue_TypeA->SetLineWidth(2);
    fake_ttmue_TypeB->SetLineWidth(2);

    PlotComparor* PC = new PlotComparor();
    PC->SetSaveDir("plots/");
    PC->Is13TeV(true);
    PC->IsSimulation(true);
    PC->DataLumi(36.1);
    //PC->HasData(true);
    PC->ClearAlterHs();
    PC->SetYtitle("F.R.");
    PC->SetBaseH(fake_z_TypeA);
    PC->SetBaseHName("Z->ee");
    PC->AddAlterH(fake_ttee_TypeA);
    PC->AddAlterHName("tt->ee");
    PC->AddAlterH(fake_ttmue_TypeA);
    PC->AddAlterHName("tt->me");
    PC->SetChannel("TypeA");
    PC->SetSaveName(("EFakeClosure_TypeA_"+var_save).Data());
    PC->Compare();

    PC->ClearAlterHs();
    PC->SetYtitle("F.R.");
    PC->SetBaseH(fake_z_TypeB);
    PC->SetBaseHName("Z->ee");
    PC->AddAlterH(fake_ttee_TypeB);
    PC->AddAlterHName("tt->ee");
    PC->AddAlterH(fake_ttmue_TypeB);
    PC->AddAlterHName("tt->me");
    PC->SetChannel("TypeB");
    PC->SetSaveName(("EFakeClosure_TypeB_"+var_save).Data());
    PC->Compare();
}
