#include <TFile.h>
#include <iostream>
#include <TCanvas.h>
#include <TH2F.h>
#include <TStyle.h>

using namespace std;

void Correlation(TString type, bool reverse);

int main() {
    Correlation("TypeA", false);
    Correlation("TypeA", true);
    Correlation("TypeB", false);
    Correlation("TypeB", true);
}

void Correlation(TString type, bool reverse)
{
    gStyle->SetOptStat(0);
    TString rtag = "";
    if (reverse) rtag = "Reverse";

    TFile *f_zeg = new TFile("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg"+rtag+"_PhMatch_EFake"+type+"_Lumiweighted_V009.01.root");
    TH2F* h_zeg = (TH2F*)f_zeg->Get("LeadPhPtForFR8TeV_LeadPhAbsEtaForFR8TeV_EF1_zeg_Reco_ZjetsElEl_Nominal");
    TFile *f_zee = new TFile("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zee_CutZee"+rtag+"_Lumiweighted_V009.01.root");
    TH2F* h_zee;
    if (!reverse) h_zee = (TH2F*)f_zee->Get("SubLepPtForFR8TeV_SubLepAbsEtaForFR8TeV_EF1_zee_Reco_ZjetsElEl_Nominal");
    else h_zee = (TH2F*)f_zee->Get("LeadLepPtForFR8TeV_LeadLepAbsEtaForFR8TeV_EF1_zee_Reco_ZjetsElEl_Nominal");

    h_zeg->Divide(h_zee);

    TCanvas* c = new TCanvas("","",800,600);
    h_zeg->GetXaxis()->SetTitle("pt [GeV]");
    h_zeg->GetYaxis()->SetTitle("|#eta|");
    if (type == "TypeA") h_zeg->GetZaxis()->SetRangeUser(0, 0.05);
    else if (type == "TypeB") h_zeg->GetZaxis()->SetRangeUser(0, 0.03);
    h_zeg->Draw("Colz");
    cout << h_zeg->GetCovariance() << endl;
    c->SaveAs("plots/EGammaFake_MC_Correlation_"+type+"_"+rtag+".png");
    c->SaveAs("plots/EGammaFake_MC_Correlation_"+type+"_"+rtag+".pdf");
}
