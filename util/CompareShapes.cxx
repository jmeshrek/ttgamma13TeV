#include <TFile.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TPad.h>
#include <TLine.h>
#include <string>
#include <TLatex.h>
#include <TMath.h>
#include <vector>
#include <TGraphErrors.h>
#include <map>
#include <THStack.h>

#include "AtlasStyle.h"
#include "PlotComparor.h"
#include "StringPlayer.h"
#include "ConfigReader.h"
#include "Logger.h"

using namespace std;

int main(int argc, char * argv[]) {
    Logger *lg = new Logger("MAIN");
    lg->Info("s", "Hi, this is the main programm~");

    string filename;
    if (argc < 2) {
	//lg->Err("s", "Missing input parameter [config file]");
	//return 0;
	filename = "../config/8TeV_compare_shape.cfg";
    } else {
	filename = argv[1];
    }

    ConfigReader* rd = new ConfigReader(filename, '_', false);
    rd->Init();

    bool DoNorm, ErrorBar, DrawRatio, Debug, Overflow, Cutbins, IsSimu, Is8TeV, DoubleYRange;
    rd->FillValueB("_Options", "DoNorm", DoNorm);
    rd->FillValueB("_Options", "ErrorBar", ErrorBar);
    rd->FillValueB("_Options", "DrawRatio", DrawRatio);
    rd->FillValueB("_Options", "Debug", Debug);
    rd->FillValueB("_Options", "Overflow", Overflow);
    rd->FillValueB("_Options", "Cutbins", Cutbins);
    rd->FillValueB("_Options", "IsSimu", IsSimu);
    rd->FillValueB("_Options", "Is8TeV", Is8TeV);
    rd->FillValueB("_Options", "DoubleYRange", DoubleYRange);
    string ratiolo = rd->GetValue("_RatioLo");
    string ratiohi = rd->GetValue("_RatioHi");

    string Var = rd->GetValue("_Var");
    string CMS = rd->GetValue("_CMS");
    string Variation = rd->GetValue("_Variation");
    string Cut = rd->GetValue("_Cut");
    string Region = rd->GetValue("_Region");
    string SubRegion = rd->GetValue("_SubRegion");
    string Tag = rd->GetValue("_Tag");
    string inputdir = rd->GetValue("_InputDir");

    vector<string> filelist = rd->GetValueAll("_FileList");
    vector<string> titles = rd->GetValueAll("_Titles");
    AutoComplete(filelist);
    AutoComplete(titles);

    for (int i = 0; i < argc; i++) {
	if (!strcmp(argv[i],"--Cut")) {
	    Cut = argv[i+1];
   	}
	if (!strcmp(argv[i],"--Region")) {
	    Region = argv[i+1];
   	}
	if (!strcmp(argv[i],"--SubRegion")) {
	    SubRegion = argv[i+1];
   	}
    }

    PlotComparor* PC = new PlotComparor();
    PC->DrawRatio(DrawRatio);
    if (ratiolo != "") PC->SetMinRatio(atof(ratiolo.c_str()));
    if (ratiohi != "") PC->SetMaxRatio(atof(ratiohi.c_str()));
    PC->SetSaveDir("plots/");
    PC->NormToUnit(DoNorm);
    string drawoptions = "hist";
    if (ErrorBar) {
	drawoptions += " _e";
    }
    PC->SetDrawOption(drawoptions.c_str());

    PC->ClearAlterHs();
    
    vector<TH1F*> hs;
    for (int i = 0; i < filelist.size(); i++) {
	string fname = inputdir + CMS + "_" + Cut + "_";
	fname += filelist.at(i);
	fname += "_Reco_" + Region + "_" + SubRegion + "_" + Variation + "_" + Tag + ".root";
        TFile*f = new TFile(fname.c_str());
	if (!f) {
	    cout << "Can't open -> " << fname << endl;
	    exit(-1);
	}
	
	int n1 = fname.find("_Nominal", 0);
	string namekey = fname.substr(inputdir.size(), n1-inputdir.size()+1);
	string hname = Var + "_"; hname += namekey; hname += "Nominal";
	TH1F*h = (TH1F*)f->Get(hname.c_str());
	if (!h) {
	    cout << "Can't get -> " << hname << endl;
	    f->ls();
	    exit(-1);
	}
        hs.push_back(h);
    }
    
    PC->SetBaseH(hs.at(0));
    PC->SetBaseHName(replaceChar(titles.at(0), '_', ' '));
    for (int k = 1; k < hs.size(); k++) {
        PC->AddAlterH(hs.at(k));
        PC->AddAlterHName(replaceChar(titles.at(k), '_', ' '));
    }
    
    string savename = CMS + "_Shape_" + Var + "_" + Cut + "_" + Region + "_" + SubRegion;
    if (DoNorm) savename += "_Normed";
    PC->SetSaveName(savename);
    PC->SetChannel(SubRegion);
    PC->SetRatioYtitle("Ratio");    
    PC->Is13TeV(!Is8TeV);
    PC->DataLumi(36.1);
    PC->ShowOverflow(Overflow);
    if (Cutbins) PC->DoCutbins(24., 50.);
    if (IsSimu) PC->HasData(false);
    if (DoubleYRange) PC->DoubleYRange(true);
    PC->Compare();
    
    return 0;
}

