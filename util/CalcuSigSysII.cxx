#include <TFile.h>
#include <TH2F.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TH1F.h>
#include <iostream>
#include <iomanip>
#include "SumWeightTree.h"
#include <map>

using namespace std;

string GetName(int i);

int main()
{
    double tot_xsec = 5370; //fb
    double tot_xsec_err = tot_xsec * 0.14;

    string filename = "/eos/user/c/caudron2/TtGamma_PL/v010/410389.ttgamma_noallhad.p3152.PL5.001.root";
    TFile* f_sw = new TFile(filename.c_str());
    TTree* t = (TTree*)f_sw->Get("sumWeights");
    SumWeightTree sumWeight(t);
    int totentry = sumWeight.GetTotalEntry();

    string total_name[110];
    double total_val[110];
    for (int ientry = 0; ientry < totentry; ientry++) {
	sumWeight.GetEntry(ientry);
	for (int i = 0; i < 110; i++) {
	    total_name[i] = GetName(i);
	    total_val[i] += sumWeight.totalEventsWeighted_mc_generator_weights->at(i);
	}
    }
    map<string, double> total;
    for (int i = 0; i < 110; i++) {
	total.insert(pair<string,double>(total_name[i],total_val[i]));
    }

    TFile* f = new TFile("results/Nominal/Sig_Sys_NominalNew.root");

    string scales[6] = {"R1F2", "R1F05", "R2F1", "R05F1", "R2F2", "R05F05"};
    string pdfs[100];
    for (int i = 1; i <= 100; i++) {
	char tmp[10];
	sprintf(tmp, "%d", i);
	string name = "PDF"; name += tmp;
	pdfs[i-1] = name;
    }
    int n_ch = 7;
    string channels[7] = {"ejets", "mujets", "ee", "emu", "mumu", "ljets", "ll"};
    string channel_titles[7] = {"\\chejets", "\\chmujets", "\\chee", "\\chemu", "\\chmumu", "\\chljets", "\\chll"};
    string levels[2] = {"reco", "truth"};

    // nominal
    double nominal_total = total["Nominal"];
    double nominal[7][2] = {};
    double nominal_err[7][2] = {};
    for (int j = 0; j < n_ch; j++) {
        for (int k = 0; k < 2; k++) {
	   string hname = "N_Nominal_" + channels[j] + "_" + levels[k];
	   TH1F* h = (TH1F*)f->Get(hname.c_str());
	   if (!h) {
	       cout << "can't find " << hname << endl;
	       f->ls();
	       exit(-1);
	    }
	   nominal[j][k] = h->GetBinContent(1);
	   nominal_err[j][k] = h->GetBinError(1);
        }
    }
    double C_nominal[7] = {};
    double C_nominal_err[7] = {};
    for (int j = 0; j < n_ch; j++) {
	double C = nominal[j][0]/nominal[j][1];
	double C_err = C*(nominal_err[j][0]/nominal[j][0]);
	cout << "C: " << nominal_err[j][0] << " " << nominal[j][0] << endl;
	C_nominal[j] = C;
	C_nominal_err[j] = C_err;
    }
    double A_nominal[7] = {};
    double A_nominal_err[7] = {};
    for (int i = 0; i < n_ch; i++) {
	double A = nominal[i][1]/nominal_total;
	double A_err = A*(nominal_err[i][1]/nominal[i][1]);
	cout << "A: " << nominal_err[i][1] << " " << nominal[i][1] << endl;
	A_nominal[i] = A;
	A_nominal_err[i] = A_err;
    }

    // scale
    double scale_vars_total[6] = {};
    for (int i = 0; i < 6; i++) {
	scale_vars_total[i] = total[scales[i].c_str()];
    }
    double scale_vars[6][7][2] = {};
    for (int i = 0; i < 6; i++) {
	for (int j = 0; j < n_ch; j++) {
	    for (int k = 0; k < 2; k++) {
		string hname = "N_" + scales[i] + "_" + channels[j] + "_" + levels[k];
		TH1F* h = (TH1F*)f->Get(hname.c_str());
	    	if (!h) {
	    	    cout << "can't find " << hname << endl;
	    	    f->ls();
	    	    exit(-1);
	    	 }
		scale_vars[i][j][k] = h->GetBinContent(1);
	    }
	}
    }
    double C_scale_vars[6][7] = {};
    for (int i = 0; i < 6; i++) {
	for (int j = 0; j < n_ch; j++) {
	    double C = scale_vars[i][j][0]/scale_vars[i][j][1];
	    C_scale_vars[i][j] = C;
	}
    }
    double A_scale_vars[6][7] = {};
    for (int i = 0; i < 6; i++) {
	for (int j = 0; j < n_ch; j++) {
	    double A = scale_vars[i][j][1]/scale_vars_total[i];
	    A_scale_vars[i][j] = A;
	}
    }

    double C_scale_sys[6][7] = {};
    for (int i = 0; i < 6; i++) {
	for (int j = 0; j < n_ch; j++) {
	    double sys = (C_scale_vars[i][j]-C_nominal[j])/C_nominal[j];
	    C_scale_sys[i][j] = sys;
	}
    }
    double A_scale_sys[6][7] = {};
    for (int i = 0; i < 6; i++) {
	for (int j = 0; j < n_ch; j++) {
	    double sys = (A_scale_vars[i][j]-A_nominal[j])/A_nominal[j];
	    A_scale_sys[i][j] = sys;
	}
    }

    double C_scale_sys_combined[7] = {};
    for (int i = 0; i < n_ch; i++) {
	double sum = 0;
	for (int j = 0; j < 3; j++) {
	    double up = C_scale_sys[2*j][i];
	    double dn = C_scale_sys[2*j+1][i];
	    double max = fabs(up) > fabs(dn) ? up : dn;
	    sum += pow(max,2);
	}
	sum = sqrt(sum);
	C_scale_sys_combined[i] = sum;
    }
    double A_scale_sys_combined[7] = {};
    for (int i = 0; i < n_ch; i++) {
	double sum = 0;
	for (int j = 0; j < 3; j++) {
	    double up = A_scale_sys[2*j][i];
	    double dn = A_scale_sys[2*j+1][i];
	    double max = fabs(up) > fabs(dn) ? up : dn;
	    sum += pow(max,2);
	}
	sum = sqrt(sum);
	A_scale_sys_combined[i] = sum;
    }

    // pdf
    double pdf_vars_total[100] = {};
    for (int i = 0; i < 100; i++) {
	pdf_vars_total[i] = total[pdfs[i].c_str()];
    }
    double pdf_vars[100][7][2] = {};
    for (int i = 0; i < 100; i++) {
	for (int j = 0; j < n_ch; j++) {
	    for (int k = 0; k < 2; k++) {
		string hname = "N_" + pdfs[i] + "_" + channels[j] + "_" + levels[k];
		TH1F* h = (TH1F*)f->Get(hname.c_str());
	    	if (!h) {
	    	    cout << "can't find " << hname << endl;
	    	    f->ls();
	    	    exit(-1);
	    	 }
		pdf_vars[i][j][k] = h->GetBinContent(1);
	    }
	}
    }
    double C_pdf_vars[100][7] = {};
    for (int i = 0; i < 100; i++) {
	for (int j = 0; j < n_ch; j++) {
	    double C = pdf_vars[i][j][0]/pdf_vars[i][j][1];
	    C_pdf_vars[i][j] = C;
	}
    }
    double A_pdf_vars[100][7] = {};
    for (int i = 0; i < 100; i++) {
	for (int j = 0; j < n_ch; j++) {
	    double A = pdf_vars[i][j][1]/pdf_vars_total[i];
	    A_pdf_vars[i][j] = A;
	}
    }

    double C_pdf_mean[7] = {};
    for (int i = 0; i < n_ch; i++) {
	C_pdf_mean[i] = 0;
	for (int j = 0; j < 100; j++) {
    	    C_pdf_mean[i] += C_pdf_vars[j][i];
    	}
	C_pdf_mean[i] /= 100.;
    }
    double C_pdf_sigma[7] = {};
    for (int i = 0; i < n_ch; i++) {
	double d2 = 0;
	for (int j = 0; j < 100; j++) {
	    d2 += pow(C_pdf_vars[j][i] - C_pdf_mean[i],2);
    	}
	d2 /= 100.;
	C_pdf_sigma[i] = sqrt(d2);
    }

    double A_pdf_mean[7] = {};
    for (int i = 0; i < n_ch; i++) {
	A_pdf_mean[i] = 0;
	for (int j = 0; j < 100; j++) {
    	    A_pdf_mean[i] += A_pdf_vars[j][i];
    	}
	A_pdf_mean[i] /= 100.;
    }
    double A_pdf_sigma[7] = {};
    for (int i = 0; i < n_ch; i++) {
	double d2 = 0;
	for (int j = 0; j < 100; j++) {
	    d2 += pow(A_pdf_vars[j][i] - A_pdf_mean[i],2);
    	}
	d2 /= 100.;
	A_pdf_sigma[i] = sqrt(d2);
    }

    // ISRFSRUp
    string filename_ISRFSRUp = "/eos/user/c/caudron2/TtGamma_PL/v010/410404.MadGraphPythia8EvtGen_A14NNPDF23Var3cUp_ttgamma_nonallhad.p3152.PL5.001.root";
    TFile* f_sw_ISRFSRUp = new TFile(filename_ISRFSRUp.c_str());
    TTree* t_ISRFSRUp = (TTree*)f_sw_ISRFSRUp->Get("sumWeights");
    SumWeightTree sumWeight_ISRFSRUp(t_ISRFSRUp);
    int totentry_ISRFSRUp = sumWeight_ISRFSRUp.GetTotalEntry();

    string total_ISRFSRUp_name[110];
    double total_ISRFSRUp_val[110];
    for (int ientry = 0; ientry < totentry_ISRFSRUp; ientry++) {
	sumWeight_ISRFSRUp.GetEntry(ientry);
	for (int i = 0; i < 110; i++) {
	    total_ISRFSRUp_name[i] = GetName(i);
	    total_ISRFSRUp_val[i] += sumWeight_ISRFSRUp.totalEventsWeighted_mc_generator_weights->at(i);
	}
    }
    map<string, double> total_ISRFSRUp;
    for (int i = 0; i < 110; i++) {
	total_ISRFSRUp.insert(pair<string,double>(total_ISRFSRUp_name[i],total_ISRFSRUp_val[i]));
    }

    TFile* f_ISRFSRUp = new TFile("results/Nominal/Sig_Sys_ISRFSRUp.root");

    double nominal_total_ISRFSRUp = total_ISRFSRUp["Nominal"];
    double nominal_ISRFSRUp[7][2] = {};
    double nominal_ISRFSRUp_err[7][2] = {};
    for (int j = 0; j < n_ch; j++) {
        for (int k = 0; k < 2; k++) {
	   string hname = "N_Nominal_" + channels[j] + "_" + levels[k];
	   TH1F* h = (TH1F*)f_ISRFSRUp->Get(hname.c_str());
	   if (!h) {
	       cout << "can't find " << hname << endl;
	       f->ls();
	       exit(-1);
	    }
	   nominal_ISRFSRUp[j][k] = h->GetBinContent(1);
	   nominal_ISRFSRUp_err[j][k] = h->GetBinError(1);
        }
    }
    double C_nominal_ISRFSRUp[7] = {};
    double C_nominal_ISRFSRUp_err[7] = {};
    for (int j = 0; j < n_ch; j++) {
	double C = nominal_ISRFSRUp[j][0]/nominal_ISRFSRUp[j][1];
	double C_err = C*(nominal_ISRFSRUp_err[j][0]/nominal_ISRFSRUp[j][0]);
	C_nominal_ISRFSRUp[j] = C;
	C_nominal_ISRFSRUp_err[j] = C_err;
    }
    double A_nominal_ISRFSRUp[7] = {};
    double A_nominal_ISRFSRUp_err[7] = {};
    for (int i = 0; i < n_ch; i++) {
	double A = nominal_ISRFSRUp[i][1]/nominal_total_ISRFSRUp;
	double A_err = A*(nominal_ISRFSRUp_err[i][1]/nominal_ISRFSRUp[i][1]);
	A_nominal_ISRFSRUp[i] = A;
	A_nominal_ISRFSRUp_err[i] = A_err;
    }

    double C_ISRFSRUp_sys[7] = {};
    double C_ISRFSRUp_sys_err[7] = {};
    for (int i = 0; i < n_ch; i++) {
	double sys = (C_nominal_ISRFSRUp[i]-C_nominal[i])/C_nominal[i];	
	double sys_err = (sys+1)*sqrt(pow(C_nominal_ISRFSRUp_err[i]/C_nominal_ISRFSRUp[i],2) + pow(C_nominal_err[i]/C_nominal[i],2));
	C_ISRFSRUp_sys[i] = sys;
	C_ISRFSRUp_sys_err[i] = sys_err;
    }
    double A_ISRFSRUp_sys[7] = {};
    double A_ISRFSRUp_sys_err[7] = {};
    for (int i = 0; i < n_ch; i++) {
	double sys = (A_nominal_ISRFSRUp[i]-A_nominal[i])/A_nominal[i];	
	double sys_err = (sys+1)*sqrt(pow(A_nominal_ISRFSRUp_err[i]/A_nominal_ISRFSRUp[i],2) + pow(A_nominal_err[i]/A_nominal[i],2));
	A_ISRFSRUp_sys[i] = sys;
	A_ISRFSRUp_sys_err[i] = sys_err;
    }

    // ISRFSRDn
    string filename_ISRFSRDn = "/eos/user/c/caudron2/TtGamma_PL/v010/410405.MadGraphPythia8EvtGen_A14NNPDF23Var3cDown_ttgamma_nonallhad.p3152.PL5.001.root";
    TFile* f_sw_ISRFSRDn = new TFile(filename_ISRFSRDn.c_str());
    TTree* t_ISRFSRDn = (TTree*)f_sw_ISRFSRDn->Get("sumWeights");
    SumWeightTree sumWeight_ISRFSRDn(t_ISRFSRDn);
    int totentry_ISRFSRDn = sumWeight_ISRFSRDn.GetTotalEntry();

    string total_ISRFSRDn_name[110];
    double total_ISRFSRDn_val[110];
    for (int ientry = 0; ientry < totentry_ISRFSRDn; ientry++) {
	sumWeight_ISRFSRDn.GetEntry(ientry);
	for (int i = 0; i < 110; i++) {
	    total_ISRFSRDn_name[i] = GetName(i);
	    total_ISRFSRDn_val[i] += sumWeight_ISRFSRDn.totalEventsWeighted_mc_generator_weights->at(i);
	}
    }
    map<string, double> total_ISRFSRDn;
    for (int i = 0; i < 110; i++) {
	total_ISRFSRDn.insert(pair<string,double>(total_ISRFSRDn_name[i],total_ISRFSRDn_val[i]));
    }

    TFile* f_ISRFSRDn = new TFile("results/Nominal/Sig_Sys_ISRFSRDn.root");

    double nominal_total_ISRFSRDn = total_ISRFSRDn["Nominal"];
    double nominal_ISRFSRDn[7][2] = {};
    double nominal_ISRFSRDn_err[7][2] = {};
    for (int j = 0; j < n_ch; j++) {
        for (int k = 0; k < 2; k++) {
	   string hname = "N_Nominal_" + channels[j] + "_" + levels[k];
	   TH1F* h = (TH1F*)f_ISRFSRDn->Get(hname.c_str());
	   if (!h) {
	       cout << "can't find " << hname << endl;
	       f->ls();
	       exit(-1);
	    }
	   nominal_ISRFSRDn[j][k] = h->GetBinContent(1);
	   nominal_ISRFSRDn_err[j][k] = h->GetBinError(1);
        }
    }
    double C_nominal_ISRFSRDn[7] = {};
    double C_nominal_ISRFSRDn_err[7] = {};
    for (int j = 0; j < n_ch; j++) {
	double C = nominal_ISRFSRDn[j][0]/nominal_ISRFSRDn[j][1];
	double C_err = C*(nominal_ISRFSRDn_err[j][0]/nominal_ISRFSRDn[j][0]);
	C_nominal_ISRFSRDn[j] = C;
	C_nominal_ISRFSRDn_err[j] = C_err;
    }
    double A_nominal_ISRFSRDn[7] = {};
    double A_nominal_ISRFSRDn_err[7] = {};
    for (int i = 0; i < n_ch; i++) {
	double A = nominal_ISRFSRDn[i][1]/nominal_total_ISRFSRDn;
	double A_err = A*(nominal_ISRFSRDn_err[i][1]/nominal_ISRFSRDn[i][1]);
	A_nominal_ISRFSRDn[i] = A;
	A_nominal_ISRFSRDn_err[i] = A_err;
    }

    double C_ISRFSRDn_sys[7] = {};
    double C_ISRFSRDn_sys_err[7] = {};
    for (int i = 0; i < n_ch; i++) {
	double sys = (C_nominal_ISRFSRDn[i]-C_nominal[i])/C_nominal[i];	
	double sys_err = (sys+1)*sqrt(pow(C_nominal_ISRFSRDn_err[i]/C_nominal_ISRFSRDn[i],2) + pow(C_nominal_err[i]/C_nominal[i],2));
	C_ISRFSRDn_sys[i] = sys;
	C_ISRFSRDn_sys_err[i] = sys_err;
    }
    double A_ISRFSRDn_sys[7] = {};
    double A_ISRFSRDn_sys_err[7] = {};
    for (int i = 0; i < n_ch; i++) {
	double sys = (A_nominal_ISRFSRDn[i]-A_nominal[i])/A_nominal[i];	
	double sys_err = (sys+1)*sqrt(pow(A_nominal_ISRFSRDn_err[i]/A_nominal_ISRFSRDn[i],2) + pow(A_nominal_err[i]/A_nominal[i],2));
	A_ISRFSRDn_sys[i] = sys;
	A_ISRFSRDn_sys_err[i] = sys_err;
    }
    
    // PS
    double PSweight = 4144.78/22210643.50;
    string filename_PS = "/eos/user/c/caudron2/TtGamma_PL/v010/410395.ttgamma_noallhad_MGH7EG_H7UE.p3152.PL5.001.root";
    TFile* f_sw_PS = new TFile(filename_PS.c_str());
    TTree* t_PS = (TTree*)f_sw_PS->Get("sumWeights");
    SumWeightTree sumWeight_PS(t_PS);
    int totentry_PS = sumWeight_PS.GetTotalEntry();

    string total_PS_name[110];
    double total_PS_val[110];
    for (int ientry = 0; ientry < totentry_PS; ientry++) {
	sumWeight_PS.GetEntry(ientry);
	for (int i = 0; i < 110; i++) {
	    total_PS_name[i] = GetName(i);
	    total_PS_val[i] += sumWeight_PS.totalEventsWeighted_mc_generator_weights->at(0);
	}
    }
    map<string, double> total_PS;
    for (int i = 0; i < 110; i++) {
	total_PS.insert(pair<string,double>(total_PS_name[i],total_PS_val[i]));
    }

    TFile* f_PS = new TFile("results/Nominal/Sig_Sys_PSNew.root");

    double nominal_total_PS = total_PS["Nominal"];
    nominal_total_PS *= PSweight;
    double nominal_PS[7][2] = {};
    double nominal_PS_err[7][2] = {};
    for (int j = 0; j < n_ch; j++) {
        for (int k = 0; k < 2; k++) {
	   string hname = "N_Nominal_" + channels[j] + "_" + levels[k];
	   TH1F* h = (TH1F*)f_PS->Get(hname.c_str());
	   if (!h) {
	       cout << "can't find " << hname << endl;
	       f->ls();
	       exit(-1);
	    }
	   nominal_PS[j][k] = h->GetBinContent(1)*PSweight;
	   nominal_PS_err[j][k] = h->GetBinError(1)*PSweight;
        }
    }
    double C_nominal_PS[7] = {};
    double C_nominal_PS_err[7] = {};
    for (int j = 0; j < n_ch; j++) {
	double C = nominal_PS[j][0]/nominal_PS[j][1];
	double C_err = C*(nominal_PS_err[j][0]/nominal_PS[j][0]);
	C_nominal_PS[j] = C;
	C_nominal_PS_err[j] = C_err;
    }
    double A_nominal_PS[7] = {};
    double A_nominal_PS_err[7] = {};
    for (int i = 0; i < n_ch; i++) {
	double A = nominal_PS[i][1]/nominal_total_PS;
	double A_err = A*(nominal_PS_err[i][1]/nominal_PS[i][1]);
	A_nominal_PS[i] = A;
	A_nominal_PS_err[i] = A_err;
    }

    double C_PS_sys[7] = {};
    double C_PS_sys_err[7] = {};
    for (int i = 0; i < n_ch; i++) {
	double sys = (C_nominal_PS[i]-C_nominal[i])/C_nominal[i];	
	double sys_err = (sys+1)*sqrt(pow(C_nominal_PS_err[i]/C_nominal_PS[i],2) + pow(C_nominal_err[i]/C_nominal[i],2));
	C_PS_sys[i] = sys;
	C_PS_sys_err[i] = sys_err;
    }
    double A_PS_sys[7] = {};
    double A_PS_sys_err[7] = {};
    for (int i = 0; i < n_ch; i++) {
	double sys = (A_nominal_PS[i]-A_nominal[i])/A_nominal[i];	
	double sys_err = (sys+1)*sqrt(pow(A_nominal_PS_err[i]/A_nominal_PS[i],2) + pow(A_nominal_err[i]/A_nominal[i],2));
	A_PS_sys[i] = sys;
	A_PS_sys_err[i] = sys_err;
    }

    // print
    cout << fixed << setprecision(2);

    cout << "\\scalebox{0.8}{" << endl;
    cout << "\\begin{tabular}{c|c|c|c|c|c}" << endl;
    cout << "\\hline" << endl;
    cout << "\\hline" << endl;
    cout << setw(20) << "" << "&";
    for (int i = 0; i < n_ch; i++) {
	cout << setw(20) << channel_titles[i];
	if (i != 6) cout << "&";
	else cout << "\\\\";
    }
    cout << "\\hline" << endl;
    cout << setw(20) << "$C$ val. (\\%)" << "&";
    for (int i = 0; i < n_ch; i++) {
	cout << setw(7) << C_nominal[i]*100 << " $\\pm$ " << setw(6) << C_nominal_err[i]*100;
	if (i != 6) cout << "&";
	else cout << "\\\\";
    }
    cout << "\\hline" << endl;
    cout << setw(20) << "$A$ val. (\\%)" << "&";
    for (int i = 0; i < n_ch; i++) {
	cout << setw(7) << A_nominal[i]*100 << " $\\pm$ " << setw(6) << A_nominal_err[i]*100;
	if (i != 6) cout << "&";
	else cout << "\\\\";
    }
    cout << "\\hline" << endl;
    cout << "\\hline" << endl;
    cout << "\\end{tabular}}" << endl;
    cout << endl;

    cout << "\\scalebox{0.8}{" << endl;
    cout << "\\begin{tabular}{c|c|c|c|c|c}" << endl;
    cout << "\\hline" << endl;
    cout << "\\hline" << endl;
    cout << setw(20) << "Scale Sys" << "&";
    for (int i = 0; i < n_ch; i++) {
	cout << setw(20) << channel_titles[i];
	if (i != 6) cout << "&";
	else cout << "\\\\";
    }
    cout << "\\hline" << endl;
    cout << setw(20) << "$C$ sys (\\%)" << "&";
    for (int i = 0; i < n_ch; i++) {
	cout << setw(10) << C_scale_sys_combined[i]*100;
	if (i != 6) cout << "&";
	else cout << "\\\\";
    }
    cout << "\\hline" << endl;
    cout << setw(20) << "$A$ sys (\\%)" << "&";
    for (int i = 0; i < n_ch; i++) {
	cout << setw(10) << A_scale_sys_combined[i]*100;
	if (i != 6) cout << "&";
	else cout << "\\\\";
    }
    cout << "\\hline" << endl;
    cout << "\\hline" << endl;
    cout << "\\end{tabular}}" << endl;
    cout << endl;

    cout << "\\scalebox{0.8}{" << endl;
    cout << "\\begin{tabular}{c|c|c|c|c|c}" << endl;
    cout << "\\hline" << endl;
    cout << "\\hline" << endl;
    cout << setw(20) << "Pdf Sys" << "&";
    for (int i = 0; i < n_ch; i++) {
	cout << setw(20) << channel_titles[i];
	if (i != 6) cout << "&";
	else cout << "\\\\";
    }
    cout << "\\hline" << endl;
    cout << setw(20) << "$C$ sys (\\%)" << "&";
    for (int i = 0; i < n_ch; i++) {
	cout << setw(20) << C_pdf_sigma[i]/C_pdf_mean[i]*100;
	if (i != 6) cout << "&";
	else cout << "\\\\";
    }
    cout << "\\hline" << endl;
    cout << setw(20) << "$A$ sys (\\%)" << "&";
    for (int i = 0; i < n_ch; i++) {
	cout << setw(20) << A_pdf_sigma[i]/A_pdf_mean[i]*100;
	if (i != 6) cout << "&";
	else cout << "\\\\";
    }
    cout << "\\hline" << endl;
    cout << "\\hline" << endl;
    cout << "\\end{tabular}}" << endl;
    cout << endl;

    cout << "\\scalebox{0.8}{" << endl;
    cout << "\\begin{tabular}{c|c|c|c|c|c}" << endl;
    cout << "\\hline" << endl;
    cout << "\\hline" << endl;
    cout << setw(20) << "ISRFSRUp Sys" << "&";
    for (int i = 0; i < n_ch; i++) {
	cout << setw(20) << channel_titles[i];
	if (i != 6) cout << "&";
	else cout << "\\\\";
    }
    cout << "\\hline" << endl;
    cout << setw(20) << "$C$ sys (\\%)" << "&";
    for (int i = 0; i < n_ch; i++) {
	cout << setw(7) << C_ISRFSRUp_sys[i]*100 << " $\\pm$ " << setw(6) << C_ISRFSRUp_sys_err[i]*100;
	if (i != 6) cout << "&";
	else cout << "\\\\";
    }
    cout << "\\hline" << endl;
    cout << setw(20) << "$A$ sys (\\%)" << "&";
    for (int i = 0; i < n_ch; i++) {
	cout << setw(7) << A_ISRFSRUp_sys[i]*100 << " $\\pm$ " << setw(6) << A_ISRFSRUp_sys_err[i]*100;
	if (i != 6) cout << "&";
	else cout << "\\\\";
    }
    cout << "\\hline" << endl;
    cout << "\\hline" << endl;
    cout << "\\end{tabular}}" << endl;
    cout << endl;

    cout << "\\scalebox{0.8}{" << endl;
    cout << "\\begin{tabular}{c|c|c|c|c|c}" << endl;
    cout << "\\hline" << endl;
    cout << "\\hline" << endl;
    cout << setw(20) << "ISRFSRDn Sys" << "&";
    for (int i = 0; i < n_ch; i++) {
	cout << setw(20) << channel_titles[i];
	if (i != 6) cout << "&";
	else cout << "\\\\";
    }
    cout << "\\hline" << endl;
    cout << setw(20) << "$C$ sys (\\%)" << "&";
    for (int i = 0; i < n_ch; i++) {
	cout << setw(7) << C_ISRFSRDn_sys[i]*100 << " $\\pm$ " << setw(6) << C_ISRFSRDn_sys_err[i]*100;
	if (i != 6) cout << "&";
	else cout << "\\\\";
    }
    cout << "\\hline" << endl;
    cout << setw(20) << "$A$ sys (\\%)" << "&";
    for (int i = 0; i < n_ch; i++) {
	cout << setw(7) << A_ISRFSRDn_sys[i]*100 << " $\\pm$ " << setw(6) << A_ISRFSRDn_sys_err[i]*100;
	if (i != 6) cout << "&";
	else cout << "\\\\";
    }
    cout << "\\hline" << endl;
    cout << "\\hline" << endl;
    cout << "\\end{tabular}}" << endl;
    cout << endl;

    cout << "\\scalebox{0.8}{" << endl;
    cout << "\\begin{tabular}{c|c|c|c|c|c}" << endl;
    cout << "\\hline" << endl;
    cout << "\\hline" << endl;
    cout << setw(20) << "PS Sys" << "&";
    for (int i = 0; i < n_ch; i++) {
	cout << setw(20) << channel_titles[i];
	if (i != 6) cout << "&";
	else cout << "\\\\";
    }
    cout << "\\hline" << endl;
    cout << setw(20) << "PS sys" << "&";
    for (int i = 0; i < n_ch; i++) {
	cout << setw(7) << C_PS_sys[i]*100 << " $\\pm$ " << setw(6) << C_PS_sys_err[i]*100 << "\%";
	if (i != 6) cout << "&";
	else cout << "\\\\";
    }
    cout << "\\hline" << endl;
    cout << setw(20) <<  "PS sys" << "&";
    for (int i = 0; i < n_ch; i++) {
	cout << setw(7) << A_PS_sys[i]*100 << " $\\pm$ " << setw(6) << A_PS_sys_err[i]*100 << "\%";
	if (i != 6) cout << "&";
	else cout << "\\\\";
    }
    cout << "\\hline" << endl;
    cout << "\\hline" << endl;
    cout << "\\end{tabular}}" << endl;
    cout << endl;

    cout << "Reco level" << endl;
    for (int j = 0; j < n_ch; j++) {
	cout << "Channel: " << channels[j] << endl;
	cout << setw(10) << nominal[j][0] << " & "
	     << setw(10) << nominal_ISRFSRUp[j][0] << " & "
	     << setw(10) << nominal_ISRFSRDn[j][0] << " & "
	     << setw(10) << nominal_PS[j][0] << endl;
    }
    cout << endl;

    cout << "Truth level" << endl;
    for (int j = 0; j < n_ch; j++) {
	cout << "Channel: " << channels[j] << endl;
	cout << setw(10) << nominal[j][1] << " & "
	     << setw(10) << nominal_ISRFSRUp[j][1] << " & "
	     << setw(10) << nominal_ISRFSRDn[j][1] << " & "
	     << setw(10) << nominal_PS[j][1] << endl;
    }
    cout << endl;

    cout << "Truth level total" << endl;
    cout << setw(10) << nominal_total << " & "
         << setw(10) << nominal_total_ISRFSRUp << " & "
         << setw(10) << nominal_total_ISRFSRDn << " & "
         << setw(10) << nominal_total_PS << endl;
    cout << endl;

    // quadratic sum
    double C_sys_combined[7] = {};
    for (int i = 0; i < n_ch; i++) {
	double sys_combined = 0;
	sys_combined += pow(C_scale_sys_combined[i],2);
	sys_combined += pow(C_pdf_sigma[i]/C_pdf_mean[i],2);
	double isrfsrsysup = fabs(C_ISRFSRUp_sys[i]) > fabs(C_ISRFSRUp_sys_err[i]) ? C_ISRFSRUp_sys[i] : C_ISRFSRUp_sys_err[i];
	double isrfsrsysdn = fabs(C_ISRFSRDn_sys[i]) > fabs(C_ISRFSRDn_sys_err[i]) ? C_ISRFSRDn_sys[i] : C_ISRFSRDn_sys_err[i];
	double isrfsrsys = fabs(isrfsrsysup) > fabs(isrfsrsysdn) ? isrfsrsysup : isrfsrsysdn;
	double pssys = fabs(C_PS_sys[i]) > fabs(C_PS_sys_err[i]) ? C_PS_sys[i] : C_PS_sys_err[i];
	sys_combined += pow(isrfsrsys,2);
	sys_combined += pow(pssys,2);
	C_sys_combined[i] = sqrt(sys_combined);
    }
    double A_sys_combined[7] = {};
    for (int i = 0; i < n_ch; i++) {
	double sys_combined = 0;
	sys_combined += pow(A_scale_sys_combined[i],2);
	sys_combined += pow(A_pdf_sigma[i]/A_pdf_mean[i],2);
	double isrfsrsysup = fabs(A_ISRFSRUp_sys[i]) > fabs(A_ISRFSRUp_sys_err[i]) ? A_ISRFSRUp_sys[i] : A_ISRFSRUp_sys_err[i];
	double isrfsrsysdn = fabs(A_ISRFSRDn_sys[i]) > fabs(A_ISRFSRDn_sys_err[i]) ? A_ISRFSRDn_sys[i] : A_ISRFSRDn_sys_err[i];
	double isrfsrsys = fabs(isrfsrsysup) > fabs(isrfsrsysdn) ? isrfsrsysup : isrfsrsysdn;
	double pssys = fabs(A_PS_sys[i]) > fabs(A_PS_sys_err[i]) ? A_PS_sys[i] : A_PS_sys_err[i];
	sys_combined += pow(isrfsrsys,2);
	sys_combined += pow(pssys,2);
	A_sys_combined[i] = sqrt(sys_combined);
    }

    double fid_xsec[7] = {};
    double fid_xsec_err[7] = {};
    for (int i = 0; i < n_ch; i++) {
	double xsec = tot_xsec * A_nominal[i];
	double xsec_err = xsec*sqrt(pow(tot_xsec_err/tot_xsec,2) + pow(A_sys_combined[i],2));
	fid_xsec[i] = xsec;
	fid_xsec_err[i] = xsec_err;
    }

    cout << "\\scalebox{0.8}{" << endl;
    cout << "\\begin{tabular}{c|c|c|c|c|c}" << endl;
    cout << "\\hline" << endl;
    cout << "\\hline" << endl;
    cout << setw(33) << "" << "&";
    for (int i = 0; i < n_ch; i++) {
	cout << setw(33) << channel_titles[i];
	if (i != 6) cout << "&";
	else cout << "\\\\";
    }
    cout << "\\hline" << endl;
    cout << setw(33) << "Fid. Xsec." << "&";
    for (int i = 0; i < n_ch; i++) {
	cout << setw(7) << fid_xsec[i] << " $\\pm$ " << setw(7) << fid_xsec_err[i] << " (" << setw(7) << fid_xsec_err[i]/fid_xsec[i]*100 << "\\%)";
	if (i != 6) cout << "&";
	else cout << "\\\\";
    }
    cout << "\\hline" << endl;
    cout << "\\hline" << endl;
    cout << "\\end{tabular}}" << endl;
    cout << endl;

    // Xtalk
    TH2F* h_xtalk = (TH2F*)f->Get("xtalk");
    TCanvas *c = new TCanvas("c","c",800,600);
    c->SetTopMargin(0.02);
    c->SetLeftMargin(0.12);
    c->SetBottomMargin(0.12);

    for (int i = 1; i <= 5; i++) {
	double total = 0;
	for (int j = 1; j <= 6; j++) {
	    double cell = h_xtalk->GetBinContent(i,j);
	    total += cell;
	}
	for (int j = 1; j <= 6; j++) {
	    double cell = h_xtalk->GetBinContent(i,j);
	    double cell_nom = cell/total;
	    h_xtalk->SetBinContent(i,j,cell_nom*100);
	}
    }

    h_xtalk->SetTitle("");

    h_xtalk->GetXaxis()->SetBinLabel(1, "ejets");
    h_xtalk->GetXaxis()->SetBinLabel(2, "mujets");
    h_xtalk->GetXaxis()->SetBinLabel(3, "ee");
    h_xtalk->GetXaxis()->SetBinLabel(4, "emu");
    h_xtalk->GetXaxis()->SetBinLabel(5, "mumu");
    h_xtalk->GetYaxis()->SetBinLabel(1, "ejets");
    h_xtalk->GetYaxis()->SetBinLabel(2, "mujets");
    h_xtalk->GetYaxis()->SetBinLabel(3, "ee");
    h_xtalk->GetYaxis()->SetBinLabel(4, "emu");
    h_xtalk->GetYaxis()->SetBinLabel(5, "mumu");
    h_xtalk->GetYaxis()->SetBinLabel(6, "other");

    h_xtalk->GetYaxis()->SetTitle("Truth Channel");
    h_xtalk->GetXaxis()->SetTitle("Reco Channel");

    gStyle->SetPaintTextFormat(".0f");
    gStyle->SetOptStat(0);
    h_xtalk->SetMarkerSize(2);
    h_xtalk->Draw("colz text");

    TString savename = "plots/Unfolding/Map_Xtalk";

    c->SaveAs(savename + ".pdf");
    c->SaveAs(savename + ".png");

}

string GetName(int i) {
    string namekey;
    if (i == 0) namekey = "Nominal";
    else if (i == 77) namekey = "R1F2";
    else if (i == 33) namekey = "R2F1";
    else if (i == 66) namekey = "R2F05";
    else if (i == 88) namekey = "R05F2";
    else if (i == 22) namekey = "R05F1";
    else if (i == 55) namekey = "R05F05";
    else if (i == 44) namekey = "R1F05";
    else if (i == 99) namekey = "R2F2";
    else if (i == 39) namekey = "PDF25";
    else if (i == 38) namekey = "PDF24";
    else if (i == 41) namekey = "PDF27";
    else if (i == 40) namekey = "PDF26";
    else if (i == 35) namekey = "PDF21";
    else if (i == 34) namekey = "PDF20";
    else if (i == 37) namekey = "PDF23";
    else if (i == 36) namekey = "PDF22";
    else if (i == 43) namekey = "PDF29";
    else if (i == 42) namekey = "PDF28";
    else if (i == 51) namekey = "PDF36";
    else if (i == 49) namekey = "PDF34";
    else if (i == 50) namekey = "PDF35";
    else if (i == 47) namekey = "PDF32";
    else if (i == 48) namekey = "PDF33";
    else if (i == 45) namekey = "PDF30";
    else if (i == 46) namekey = "PDF31";
    else if (i == 53) namekey = "PDF38";
    else if (i == 54) namekey = "PDF39";
    else if (i == 103) namekey = "PDF83";
    else if (i == 102) namekey = "PDF82";
    else if (i == 101) namekey = "PDF81";
    else if (i == 100) namekey = "PDF80";
    else if (i == 107) namekey = "PDF87";
    else if (i == 13) namekey = "PDF100";
    else if (i == 105) namekey = "PDF85";
    else if (i == 104) namekey = "PDF84";
    else if (i == 109) namekey = "PDF89";
    else if (i == 108) namekey = "PDF88";
    else if (i == 87) namekey = "PDF69";
    else if (i == 86) namekey = "PDF68";
    else if (i == 79) namekey = "PDF61";
    else if (i == 78) namekey = "PDF60";
    else if (i == 81) namekey = "PDF63";
    else if (i == 80) namekey = "PDF62";
    else if (i == 83) namekey = "PDF65";
    else if (i == 82) namekey = "PDF64";
    else if (i == 85) namekey = "PDF67";
    else if (i == 84) namekey = "PDF66";
    else if (i == 20) namekey = "PDF8";
    else if (i == 21) namekey = "PDF9";
    else if (i == 18) namekey = "PDF6";
    else if (i == 19) namekey = "PDF7";
    else if (i == 16) namekey = "PDF4";
    else if (i == 17) namekey = "PDF5";
    else if (i == 14) namekey = "PDF2";
    else if (i == 15) namekey = "PDF3";
    else if (i == 12) namekey = "PDF1";
    else if (i == 6) namekey = "PDF94";
    else if (i == 7) namekey = "PDF95";
    else if (i == 8) namekey = "PDF96";
    else if (i == 9) namekey = "PDF97";
    else if (i == 2) namekey = "PDF90";
    else if (i == 3) namekey = "PDF91";
    else if (i == 4) namekey = "PDF92";
    else if (i == 5) namekey = "PDF93";
    else if (i == 10) namekey = "PDF98";
    else if (i == 11) namekey = "PDF99";
    else if (i == 31) namekey = "PDF18";
    else if (i == 32) namekey = "PDF19";
    else if (i == 27) namekey = "PDF14";
    else if (i == 28) namekey = "PDF15";
    else if (i == 29) namekey = "PDF16";
    else if (i == 30) namekey = "PDF17";
    else if (i == 23) namekey = "PDF10";
    else if (i == 24) namekey = "PDF11";
    else if (i == 25) namekey = "PDF12";
    else if (i == 26) namekey = "PDF13";
    else if (i == 97) namekey = "PDF78";
    else if (i == 98) namekey = "PDF79";
    else if (i == 91) namekey = "PDF72";
    else if (i == 92) namekey = "PDF73";
    else if (i == 89) namekey = "PDF70";
    else if (i == 90) namekey = "PDF71";
    else if (i == 95) namekey = "PDF76";
    else if (i == 96) namekey = "PDF77";
    else if (i == 93) namekey = "PDF74";
    else if (i == 94) namekey = "PDF75";
    else if (i == 106) namekey = "PDF86";
    else if (i == 52) namekey = "PDF37";
    else if (i == 65) namekey = "PDF49";
    else if (i == 64) namekey = "PDF48";
    else if (i == 63) namekey = "PDF47";
    else if (i == 62) namekey = "PDF46";
    else if (i == 61) namekey = "PDF45";
    else if (i == 60) namekey = "PDF44";
    else if (i == 59) namekey = "PDF43";
    else if (i == 58) namekey = "PDF42";
    else if (i == 57) namekey = "PDF41";
    else if (i == 56) namekey = "PDF40";
    else if (i == 67) namekey = "PDF50";
    else if (i == 68) namekey = "PDF51";
    else if (i == 69) namekey = "PDF52";
    else if (i == 70) namekey = "PDF53";
    else if (i == 71) namekey = "PDF54";
    else if (i == 72) namekey = "PDF55";
    else if (i == 73) namekey = "PDF56";
    else if (i == 74) namekey = "PDF57";
    else if (i == 75) namekey = "PDF58";
    else if (i == 76) namekey = "PDF59";
    else namekey = "Unknown";

    return namekey;
}
