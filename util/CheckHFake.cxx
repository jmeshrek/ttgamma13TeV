#include <TDirectoryFile.h>
#include <TFile.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TPad.h>
#include <TLine.h>
#include <string>
#include <TLatex.h>
#include <TMath.h>
#include <vector>
#include <TGraphErrors.h>
#include <map>
#include <TIterator.h>
#include <TKey.h>
#include <TObject.h>
#include <THStack.h>

#include "AtlasStyle.h"
#include "PlotComparor.h"
#include "StringPlayer.h"
#include "ConfigReader.h"
#include "Logger.h"

using namespace std;

int main(int argc, char * argv[])

{
    vector<string> files;
    files.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v5/user.finelli.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v5_output_root/user.finelli.14799720._000001.output.root");
    files.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v5/user.finelli.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v5_output_root/user.finelli.14799720._000002.output.root");
    files.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v5/user.finelli.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v5_output_root/user.finelli.14799720._000003.output.root.2");
    files.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v5/user.finelli.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v5_output_root/user.finelli.14799720._000004.output.root");
    files.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v5/user.finelli.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v5_output_root/user.finelli.14799720._000005.output.root");
    files.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v5/user.finelli.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v5_output_root/user.finelli.14799720._000006.output.root");
    files.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v5/user.finelli.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v5_output_root/user.finelli.14799720._000007.output.root");
    files.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v5/user.finelli.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v5_output_root/user.finelli.14799720._000008.output.root");
    files.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v5/user.finelli.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v5_output_root/user.finelli.14799720._000009.output.root");
    files.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v5/user.finelli.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v5_output_root/user.finelli.14799720._000010.output.root");

    TH1F* h_cf = NULL;
    for (int i = 0; i < files.size(); i++) {
	string finname = files.at(i);
	TFile* fin = new TFile(finname.c_str());
	TDirectoryFile *dr = (TDirectoryFile*)fin->Get("ejets_gamma_basic");
	TH1F* hin = (TH1F*)dr->Get("cutflow_upgrade_level");
	if (i == 0) h_cf = (TH1F*)hin->Clone();
	else h_cf->Add(hin);
    }

    h_cf->Scale(870.94769);
    h_cf->Scale(3000*535094.2/17276744688.0);
    h_cf->Print("all");

    //TFile*fin_14 = new TFile("results/Upgrade/14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_TTBar_Upgrade_UR_ejets_Nominal_U05.root");
    //TH1F* hphpt_14 = (TH1F*)fin_14->Get("LeadPhPt_14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_TTBar_Upgrade_UR_ejets_Nominal");
    TFile*fin_14 = new TFile("results/Upgrade/All.root");
    fin_14->ls();
    TH1F* hphpt_14 = (TH1F*)fin_14->Get("LeadPhPt_HFake_ljets");
    TFile*fin_13 = new TFile("results/Nominal/13TeV_CutSR_PhMatch_HFake_TTBar_Reco_CR1_ejets_Nominal_Final02.root");
    TH1F* hphpt_13 = (TH1F*)fin_13->Get("LeadPhPt_13TeV_CutSR_PhMatch_HFake_TTBar_Reco_CR1_ejets_Nominal");

    PlotComparor* PC = new PlotComparor();
    PC->DrawRatio(true);
    PC->SetSaveDir("plots/Upgrade/");
    PC->NormToUnit(true);
    PC->SetChannel("ejets");
    string drawoptions = "hist";
    drawoptions += " _e";
    PC->SetDrawOption(drawoptions.c_str());

    PC->ClearAlterHs();
    PC->SetBaseH(hphpt_13);
    PC->SetBaseHName("13TeV hfake");
    PC->AddAlterH(hphpt_14);
    PC->AddAlterHName("14TeV hfake");
    string savename = "CompareShape1314_HFake_PhPt_ejets";
    PC->SetSaveName(savename);
    PC->Is14TeV(true);
    PC->SetMaxRatio(1.5);
    PC->SetMinRatio(0.5);
    PC->SetXtitle("Photon pT");
    PC->Compare();
}
