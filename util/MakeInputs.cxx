#include <TFile.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TH2F.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TH1F.h>
#include <iostream>
#include <iomanip>
#include "AtlasStyle.h"
#include "SumWeightTree.h"
#include <map>

using namespace std;

string dir_13tev = "results/Nominal/";
string dir_14tev = "results/Upgrade/";
string run2label = "Run-2 (36 \\ifb)";
string HLlabel = "HL-LHC";

bool debug = false;
bool emuonly = false;
bool pt1000 = false;
string hfakereweight = "HFakeReweighted_";

void FindLastBin(TH1F* h, double &n, double &e, bool debug = false);
void RecordCutflow(TH1F* h, vector<pair<string,double> > &cf, string channel, string cme);

int main(int argc, char * argv[]) 
{
    for (int i = 0; i < argc; i++) {
	if (!strcmp(argv[i],"--debug")) {
	    debug = true;
   	}
	if (!strcmp(argv[i],"--emu")) {
	    emuonly = true;
   	}
	if (!strcmp(argv[i],"--pt1000")) {
	    pt1000 = true;
   	}
	if (!strcmp(argv[i],"--NoHFweight")) {
	    hfakereweight = "";
   	}
	if (!strcmp(argv[i],"--HFweight025")) {
	    hfakereweight = "HFakeReweighted025_";
   	}
	if (!strcmp(argv[i],"--HFweight05")) {
	    hfakereweight = "HFakeReweighted05_";
   	}
	if (!strcmp(argv[i],"--HFweight075")) {
	    hfakereweight = "HFakeReweighted075_";
   	}
    }

    cout << fixed << setprecision(0) << endl;
    
    vector<string> channels;
    channels.push_back("ejets");
    channels.push_back("mujets");
    channels.push_back("ee");
    channels.push_back("emu");
    channels.push_back("mumu");

    vector<string> channel_titles;
    channel_titles.push_back("\\chejets");
    channel_titles.push_back("\\chmujets");
    channel_titles.push_back("\\chee");
    channel_titles.push_back("\\chemu");
    channel_titles.push_back("\\chmumu");

    vector<string> processes;
    processes.push_back("Signal");
    processes.push_back("HFake");
    processes.push_back("EFake");
    processes.push_back("WGamma");
    processes.push_back("ZGamma");
    processes.push_back("Diboson");
    processes.push_back("ST");
    processes.push_back("FakeLep");

    vector<string> processe_titles;
    processe_titles.push_back("t#bar{t}#gamma");
    processe_titles.push_back("Had-fake");
    processe_titles.push_back("e-fake");
    processe_titles.push_back("W#gamma");
    processe_titles.push_back("Z#gamma");
    processe_titles.push_back("Diboson#gamma");
    processe_titles.push_back("ST#gamma");
    processe_titles.push_back("Fake lepton");

    vector<vector<TH1F*> > hss_leadphpt_14tev;
    vector<vector<TH1F*> > hss_leadpheta_14tev;
    vector<vector<TH1F*> > hss_drphlep_14tev;
    vector<vector<TH1F*> > hss_detall_14tev;
    vector<vector<TH1F*> > hss_dphill_14tev;
    vector<TH1F* > hs_leadphpt_14tev_ljets; vector<TH1F* > hs_leadphpt_14tev_ll;
    vector<TH1F* > hs_leadpheta_14tev_ljets; vector<TH1F* > hs_leadpheta_14tev_ll;
    vector<TH1F* > hs_drphlep_14tev_ljets; vector<TH1F* > hs_drphlep_14tev_ll;
    vector<TH1F* > hs_detall_14tev_ljets; vector<TH1F* > hs_detall_14tev_ll;
    vector<TH1F* > hs_dphill_14tev_ljets; vector<TH1F* > hs_dphill_14tev_ll;

    vector<TH1F*> h_ratio_procs;
    vector<TH1F*> h_ratio_signals;

    for (int ich = 0; ich < channels.size(); ich++) {
	string channel = channels.at(ich);

    	vector<vector<string> > fnamess_14tev;
    	vector<vector<string> > hnamess_14tev;

    	vector<string> fnames_Signal_14tev; 
    	vector<string> hnames_Signal_14tev; 
    	fnames_Signal_14tev.push_back("14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_Signal_Upgrade_UR_"+channel+"_Nominal_U07.root"); 
    	hnames_Signal_14tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_Signal_Upgrade_UR_"+channel+"_Nominal"); 
    	fnamess_14tev.push_back(fnames_Signal_14tev);
    	hnamess_14tev.push_back(hnames_Signal_14tev);

    	vector<string> fnames_HFake_14tev; 
    	vector<string> hnames_HFake_14tev; 
    	fnames_HFake_14tev.push_back("14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_TTBar_Upgrade_UR_"+channel+"_"+hfakereweight+"Nominal_U07.root");
    	if (channel != "emu") fnames_HFake_14tev.push_back("14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_Zjets_Upgrade_UR_"+channel+"_"+hfakereweight+"Nominal_U07.root");
    	fnames_HFake_14tev.push_back("14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_ST_Upgrade_UR_"+channel+"_"+hfakereweight+"Nominal_U07.root");
    	fnames_HFake_14tev.push_back("14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_Diboson_Upgrade_UR_"+channel+"_"+hfakereweight+"Nominal_U07.root");
    	if (channel == "ejets" || channel == "mujets") fnames_HFake_14tev.push_back("14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_Wjets_Upgrade_UR_"+channel+"_"+hfakereweight+"Nominal_U07.root");
    	hnames_HFake_14tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_TTBar_Upgrade_UR_"+channel+"_"+hfakereweight+"Nominal");
    	if (channel != "emu") hnames_HFake_14tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_Zjets_Upgrade_UR_"+channel+"_"+hfakereweight+"Nominal");
    	hnames_HFake_14tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_ST_Upgrade_UR_"+channel+"_"+hfakereweight+"Nominal");
    	hnames_HFake_14tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_Diboson_Upgrade_UR_"+channel+"_"+hfakereweight+"Nominal");
    	if (channel == "ejets" || channel == "mujets") hnames_HFake_14tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_Wjets_Upgrade_UR_"+channel+"_"+hfakereweight+"Nominal");
    	fnamess_14tev.push_back(fnames_HFake_14tev);
    	hnamess_14tev.push_back(hnames_HFake_14tev);

    	vector<string> fnames_EFake_14tev; 
    	vector<string> hnames_EFake_14tev; 
    	if (channel == "ejets") {
    	    fnames_EFake_14tev.push_back("14TeV_CutSRUpgrade_PhMatch_EFake_LpMatch_TrueLp_TTBar_Upgrade_UR_"+channel+"_Nominal_U07.root");
    		fnames_EFake_14tev.push_back("14TeV_CutSRUpgrade_PhMatch_EFake_LpMatch_TrueLp_Zjets_Upgrade_UR_"+channel+"_Nominal_U07.root");
    		hnames_EFake_14tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_EFake_LpMatch_TrueLp_TTBar_Upgrade_UR_"+channel+"_Nominal");
    		hnames_EFake_14tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_EFake_LpMatch_TrueLp_Zjets_Upgrade_UR_"+channel+"_Nominal");
    	} else if (channel == "mujets") {
    	    fnames_EFake_14tev.push_back("14TeV_CutSRUpgrade_PhMatch_EFake_LpMatch_TrueLp_TTBar_Upgrade_UR_"+channel+"_Nominal_U07.root");
    		hnames_EFake_14tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_EFake_LpMatch_TrueLp_TTBar_Upgrade_UR_"+channel+"_Nominal");
    	}
    	fnamess_14tev.push_back(fnames_EFake_14tev);
    	hnamess_14tev.push_back(hnames_EFake_14tev);

    	vector<string> fnames_WGamma_14tev; 
    	vector<string> hnames_WGamma_14tev; 
	if (channel == "ejets" || channel == "mujets") fnames_WGamma_14tev.push_back("14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_Wjets_Upgrade_UR_"+channel+"_Nominal_U07.root");
	if (channel == "ejets" || channel == "mujets") hnames_WGamma_14tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_Wjets_Upgrade_UR_"+channel+"_Nominal");
    	fnamess_14tev.push_back(fnames_WGamma_14tev);
    	hnamess_14tev.push_back(hnames_WGamma_14tev);

    	vector<string> fnames_ZGamma_14tev; 
    	vector<string> hnames_ZGamma_14tev; 
	if (channel == "ejets") {
	    fnames_ZGamma_14tev.push_back("13TeV_CutSR_PhMatch_TruePh_ZGammajetsElElNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal_Final06.root");
	    hnames_ZGamma_14tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_ZGammajetsElElNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal");
	    fnames_ZGamma_14tev.push_back("13TeV_CutSR_PhMatch_TruePh_ZGammajetsTauTauNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal_Final06.root");
	    hnames_ZGamma_14tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_ZGammajetsTauTauNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal");
	}
	if (channel == "mujets") {
	    fnames_ZGamma_14tev.push_back("13TeV_CutSR_PhMatch_TruePh_ZGammajetsMuMuNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal_Final06.root");
	    hnames_ZGamma_14tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_ZGammajetsMuMuNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal");
	    fnames_ZGamma_14tev.push_back("13TeV_CutSR_PhMatch_TruePh_ZGammajetsTauTauNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal_Final06.root");
	    hnames_ZGamma_14tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_ZGammajetsTauTauNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal");
	}
	if (channel == "ee") {
	    fnames_ZGamma_14tev.push_back("13TeV_CutSR_PhMatch_TruePh_ZGammajetsElElNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal_Final06.root");
	    hnames_ZGamma_14tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_ZGammajetsElElNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal");
	    fnames_ZGamma_14tev.push_back("13TeV_CutSR_PhMatch_TruePh_ZGammajetsTauTauNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal_Final06.root");
	    hnames_ZGamma_14tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_ZGammajetsTauTauNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal");
	}
	if (channel == "mumu") {
	    fnames_ZGamma_14tev.push_back("13TeV_CutSR_PhMatch_TruePh_ZGammajetsMuMuNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal_Final06.root");
	    hnames_ZGamma_14tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_ZGammajetsMuMuNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal");
	    fnames_ZGamma_14tev.push_back("13TeV_CutSR_PhMatch_TruePh_ZGammajetsTauTauNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal_Final06.root");
	    hnames_ZGamma_14tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_ZGammajetsTauTauNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal");
	}
	if (channel == "emu") {
	    fnames_ZGamma_14tev.push_back("13TeV_CutSR_PhMatch_TruePh_ZGammajetsTauTauNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal_Final06.root");
	    hnames_ZGamma_14tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_ZGammajetsTauTauNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal");
	}
    	fnamess_14tev.push_back(fnames_ZGamma_14tev);
    	hnamess_14tev.push_back(hnames_ZGamma_14tev);

    	vector<string> fnames_Diboson_14tev; 
    	vector<string> hnames_Diboson_14tev; 
	if (channel != "emu") fnames_Diboson_14tev.push_back("14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_Diboson_Upgrade_UR_"+channel+"_Nominal_U07.root");
	if (channel != "emu") hnames_Diboson_14tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_Diboson_Upgrade_UR_"+channel+"_Nominal");
    	fnamess_14tev.push_back(fnames_Diboson_14tev);
    	hnamess_14tev.push_back(hnames_Diboson_14tev);

    	vector<string> fnames_ST_14tev; 
    	vector<string> hnames_ST_14tev; 
	fnames_ST_14tev.push_back("14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_ST_Upgrade_UR_"+channel+"_Nominal_U07.root");
	hnames_ST_14tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_ST_Upgrade_UR_"+channel+"_Nominal");
    	fnamess_14tev.push_back(fnames_ST_14tev);
    	hnamess_14tev.push_back(hnames_ST_14tev);

    	vector<string> fnames_FakeLep_14tev; 
    	vector<string> hnames_FakeLep_14tev; 
    	if (channel == "ejets") {
    		fnames_FakeLep_14tev.push_back("13TeV_CutSR_QCD_CR1_"+channel+"_UPS_eta_mtw_ZGammaReweighted_Nominal_Final06.root");
    		hnames_FakeLep_14tev.push_back("Cutflow_13TeV_CutSR_QCD_CR1_"+channel+"_UPS_eta_mtw_ZGammaReweighted_Nominal");
    	} else if (channel == "mujets") {
    		fnames_FakeLep_14tev.push_back("13TeV_CutSR_QCD_CR1_"+channel+"_PS_pt_mtw_ZGammaReweighted_Nominal_Final06.root");
    		hnames_FakeLep_14tev.push_back("Cutflow_13TeV_CutSR_QCD_CR1_"+channel+"_PS_pt_mtw_ZGammaReweighted_Nominal");
    	}
    	fnamess_14tev.push_back(fnames_FakeLep_14tev);
    	hnamess_14tev.push_back(hnames_FakeLep_14tev);

    	vector<double> proc_ns_14tev;
    	vector<double> proc_es_14tev;

	vector<pair<string,double> > Signal_Cutflow_14tev;
	vector<TH1F*> hs_leadphpt_14tev;
	vector<TH1F*> hs_leadpheta_14tev;
	vector<TH1F*> hs_drphlep_14tev;
	vector<TH1F*> hs_detall_14tev;
	vector<TH1F*> hs_dphill_14tev;

    	for (int ipro = 0; ipro < processes.size(); ipro++) {
	    vector<string> fnames_14tev = fnamess_14tev.at(ipro);
	    vector<string> hnames_14tev = hnamess_14tev.at(ipro);
	    double ns_14tev = 0;
	    double es_14tev = 0;
	    TH1F* h_leadphpt_14tev = NULL;
	    TH1F* h_leadpheta_14tev = NULL;
	    TH1F* h_drphlep_14tev = NULL;
	    TH1F* h_detall_14tev = NULL;
	    TH1F* h_dphill_14tev = NULL;
	    if (debug) cout << "tag01" << endl;
	    for (int i = 0; i < fnames_14tev.size(); i++) {
	        string fname_14tev;
		if (fname_14tev.find("13TeV",0) == string::npos) fname_14tev = dir_14tev + fnames_14tev.at(i);
		else fname_14tev = dir_13tev + fnames_14tev.at(i);
	        string hname_14tev = hnames_14tev.at(i);
	        TFile* f_14tev = new TFile(fname_14tev.c_str());
		if (debug) cout << "tag1" << endl;
		string hname_14tev_leadphpt = hname_14tev; 
		if (pt1000) {
		    hname_14tev_leadphpt.replace(0, 7, "LeadPhPtUnfold2");
		} else {
		    hname_14tev_leadphpt.replace(0, 7, "LeadPhPtUnfold");
		}
		TH1F* h_tmp_leadphpt_14tev = (TH1F*)f_14tev->Get(hname_14tev_leadphpt.c_str());
		string hname_14tev_leadpheta = hname_14tev; 
		hname_14tev_leadpheta.replace(0, 7, "LeadPhAbsEtaUnfold");
		TH1F* h_tmp_leadpheta_14tev = (TH1F*)f_14tev->Get(hname_14tev_leadpheta.c_str());
		string hname_14tev_drphlep = hname_14tev; 
		hname_14tev_drphlep.replace(0, 7, "LeadPhMinDrPhLepUnfold");
		TH1F* h_tmp_drphlep_14tev = (TH1F*)f_14tev->Get(hname_14tev_drphlep.c_str());
		string hname_14tev_detall = hname_14tev; 
		hname_14tev_detall.replace(0, 7, "DEtaLepLepUnfold");
		TH1F* h_tmp_detall_14tev = (TH1F*)f_14tev->Get(hname_14tev_detall.c_str());
		string hname_14tev_dphill = hname_14tev; 
		hname_14tev_dphill.replace(0, 7, "DPhiLepLepUnfold");
		TH1F* h_tmp_dphill_14tev = (TH1F*)f_14tev->Get(hname_14tev_dphill.c_str());

		double scale = 1;
		if (fname_14tev.find("ZGammajets",0) != string::npos) scale = 83*1.08;
		if (fname_14tev.find("QCD",0) != string::npos) scale = 83;

		if (channel == "emu" && fname_14tev.find("ZGammajets",0) != string::npos) {
		    //cout << fname_14tev << endl;
		    //cout << hname_14tev_leadphpt << endl;
		    //cout << h_tmp_leadphpt_14tev->Integral() << endl;
		}

		if (h_tmp_leadphpt_14tev != NULL) h_tmp_leadphpt_14tev->Scale(scale);
		if (h_tmp_leadpheta_14tev != NULL) h_tmp_leadpheta_14tev->Scale(scale);
		if (h_tmp_drphlep_14tev != NULL) h_tmp_drphlep_14tev->Scale(scale);
		if (h_tmp_detall_14tev != NULL) h_tmp_detall_14tev->Scale(scale);
		if (h_tmp_dphill_14tev != NULL) h_tmp_dphill_14tev->Scale(scale);

		if (h_leadphpt_14tev == NULL) h_leadphpt_14tev = h_tmp_leadphpt_14tev;
		else h_leadphpt_14tev->Add(h_tmp_leadphpt_14tev);
		if (h_leadpheta_14tev == NULL) h_leadpheta_14tev = h_tmp_leadpheta_14tev;
		else h_leadpheta_14tev->Add(h_tmp_leadpheta_14tev);
		if (h_drphlep_14tev == NULL) h_drphlep_14tev = h_tmp_drphlep_14tev;
		else h_drphlep_14tev->Add(h_tmp_drphlep_14tev);
		if (h_detall_14tev == NULL) h_detall_14tev = h_tmp_detall_14tev;
		else h_detall_14tev->Add(h_tmp_detall_14tev);
		if (h_dphill_14tev == NULL) h_dphill_14tev = h_tmp_dphill_14tev;
		else h_dphill_14tev->Add(h_tmp_dphill_14tev);
	    }
	    if (debug) cout << "tag13" << endl;
    	    //if (processes.at(ipro) == "HFake") {
	    //    if (channel == "ejets") scale = 0.36158;
	    //    if (channel == "mujets") scale = 0.35294;
	    //    if (channel == "ee") scale = 0.16889;
	    //    if (channel == "emu") scale = 0.11048;
	    //    if (channel == "mumu") scale = 0.087362;
    	    //}
	    if (h_leadphpt_14tev != NULL) for (int ib = 1; ib <= h_leadphpt_14tev->GetNbinsX(); ib++) h_leadphpt_14tev->SetBinError(ib, 0);
	    if (h_leadpheta_14tev != NULL) for (int ib = 1; ib <= h_leadpheta_14tev->GetNbinsX(); ib++) h_leadpheta_14tev->SetBinError(ib, 0);
	    if (h_drphlep_14tev != NULL) for (int ib = 1; ib <= h_drphlep_14tev->GetNbinsX(); ib++) h_drphlep_14tev->SetBinError(ib, 0);
	    if (h_detall_14tev != NULL) for (int ib = 1; ib <= h_detall_14tev->GetNbinsX(); ib++) h_detall_14tev->SetBinError(ib, 0);
	    if (h_dphill_14tev != NULL) for (int ib = 1; ib <= h_dphill_14tev->GetNbinsX(); ib++) h_dphill_14tev->SetBinError(ib, 0);
	    if (debug) cout << "tag14" << endl;
    	    proc_ns_14tev.push_back(ns_14tev);
    	    proc_es_14tev.push_back(es_14tev);
	    hs_leadphpt_14tev.push_back(h_leadphpt_14tev);
	    hs_leadpheta_14tev.push_back(h_leadpheta_14tev);
	    hs_drphlep_14tev.push_back(h_drphlep_14tev);
	    hs_detall_14tev.push_back(h_detall_14tev);
	    hs_dphill_14tev.push_back(h_dphill_14tev);
	    if (debug) cout << "tag15" << endl;
    	}
	if (debug) cout << "tag2" << endl;

	if (ich == 0) {
	    for (int itmp = 0; itmp < hs_leadphpt_14tev.size(); itmp++) {
		if (hs_leadphpt_14tev.at(itmp) != NULL) 
		    hs_leadphpt_14tev_ljets.push_back((TH1F*)hs_leadphpt_14tev.at(itmp)->Clone());
		else hs_leadphpt_14tev_ljets.push_back(NULL);
	    }
	} else if (ich == 1) {
	    for (int i = 0; i < hs_leadphpt_14tev_ljets.size(); i++) {
		if (hs_leadphpt_14tev_ljets.at(i) == NULL && hs_leadphpt_14tev.at(i) != NULL) hs_leadphpt_14tev_ljets.at(i) = hs_leadphpt_14tev.at(i);
		else if (hs_leadphpt_14tev_ljets.at(i) != NULL && hs_leadphpt_14tev.at(i) != NULL) hs_leadphpt_14tev_ljets.at(i)->Add(hs_leadphpt_14tev.at(i));
	    }
	} else if (ich == 2) {
	    if (!emuonly) {
		for (int itmp = 0; itmp < hs_leadphpt_14tev.size(); itmp++) {
		    if (hs_leadphpt_14tev.at(itmp) != NULL) 
		        hs_leadphpt_14tev_ll.push_back((TH1F*)hs_leadphpt_14tev.at(itmp)->Clone());
		    else hs_leadphpt_14tev_ll.push_back(NULL);
	    	}
	    }
	} else if (ich == 3) {
	    if (!emuonly) {
		for (int i = 0; i < hs_leadphpt_14tev_ll.size(); i++) {
	    	    if (hs_leadphpt_14tev_ll.at(i) == NULL && hs_leadphpt_14tev.at(i) != NULL) hs_leadphpt_14tev_ll.at(i) = hs_leadphpt_14tev.at(i);
	    	    else if (hs_leadphpt_14tev_ll.at(i) != NULL && hs_leadphpt_14tev.at(i) != NULL) hs_leadphpt_14tev_ll.at(i)->Add(hs_leadphpt_14tev.at(i));
	    	}
	    } else {
		hs_leadphpt_14tev_ll = hs_leadphpt_14tev;
	    }
	} else if (ich == 4) {
	    if (!emuonly) {
		for (int i = 0; i < hs_leadphpt_14tev_ll.size(); i++) {
	    	    if (hs_leadphpt_14tev_ll.at(i) == NULL && hs_leadphpt_14tev.at(i) != NULL) hs_leadphpt_14tev_ll.at(i) = hs_leadphpt_14tev.at(i);
	    	    else if (hs_leadphpt_14tev_ll.at(i) != NULL && hs_leadphpt_14tev.at(i) != NULL) hs_leadphpt_14tev_ll.at(i)->Add(hs_leadphpt_14tev.at(i));
	    	}
	    }
	}

	if (ich == 0) {
	    for (int itmp = 0; itmp < hs_leadpheta_14tev.size(); itmp++) {
		if (hs_leadpheta_14tev.at(itmp) != NULL) 
		    hs_leadpheta_14tev_ljets.push_back((TH1F*)hs_leadpheta_14tev.at(itmp)->Clone());
		else hs_leadpheta_14tev_ljets.push_back(NULL);
	    }
	} else if (ich == 1) {
	    for (int i = 0; i < hs_leadpheta_14tev_ljets.size(); i++) {
		if (hs_leadpheta_14tev_ljets.at(i) == NULL && hs_leadpheta_14tev.at(i) != NULL) hs_leadpheta_14tev_ljets.at(i) = hs_leadpheta_14tev.at(i);
		else if (hs_leadpheta_14tev_ljets.at(i) != NULL && hs_leadpheta_14tev.at(i) != NULL) hs_leadpheta_14tev_ljets.at(i)->Add(hs_leadpheta_14tev.at(i));
	    }
	} else if (ich == 2) {
	    if (!emuonly) {
		for (int itmp = 0; itmp < hs_leadpheta_14tev.size(); itmp++) {
		    if (hs_leadpheta_14tev.at(itmp) != NULL) 
		        hs_leadpheta_14tev_ll.push_back((TH1F*)hs_leadpheta_14tev.at(itmp)->Clone());
		    else hs_leadpheta_14tev_ll.push_back(NULL);
	    	}
	    }
	} else if (ich == 3) {
	    if (!emuonly) {
		for (int i = 0; i < hs_leadpheta_14tev_ll.size(); i++) {
	    	    if (hs_leadpheta_14tev_ll.at(i) == NULL && hs_leadpheta_14tev.at(i) != NULL) hs_leadpheta_14tev_ll.at(i) = hs_leadpheta_14tev.at(i);
	    	    else if (hs_leadpheta_14tev_ll.at(i) != NULL && hs_leadpheta_14tev.at(i) != NULL) hs_leadpheta_14tev_ll.at(i)->Add(hs_leadpheta_14tev.at(i));
	    	}
	    } else {
		hs_leadpheta_14tev_ll = hs_leadpheta_14tev;
	    }
	} else if (ich == 4) {
	    if (!emuonly) {
		for (int i = 0; i < hs_leadpheta_14tev_ll.size(); i++) {
	    	    if (hs_leadpheta_14tev_ll.at(i) == NULL && hs_leadpheta_14tev.at(i) != NULL) hs_leadpheta_14tev_ll.at(i) = hs_leadpheta_14tev.at(i);
	    	    else if (hs_leadpheta_14tev_ll.at(i) != NULL && hs_leadpheta_14tev.at(i) != NULL) hs_leadpheta_14tev_ll.at(i)->Add(hs_leadpheta_14tev.at(i));
	    	}
	    }
	}

	if (ich == 0) {
	    for (int itmp = 0; itmp < hs_drphlep_14tev.size(); itmp++) {
		if (hs_drphlep_14tev.at(itmp) != NULL) 
		    hs_drphlep_14tev_ljets.push_back((TH1F*)hs_drphlep_14tev.at(itmp)->Clone());
		else hs_drphlep_14tev_ljets.push_back(NULL);
	    }
	} else if (ich == 1) {
	    for (int i = 0; i < hs_drphlep_14tev_ljets.size(); i++) {
		if (hs_drphlep_14tev_ljets.at(i) == NULL && hs_drphlep_14tev.at(i) != NULL) hs_drphlep_14tev_ljets.at(i) = hs_drphlep_14tev.at(i);
		else if (hs_drphlep_14tev_ljets.at(i) != NULL && hs_drphlep_14tev.at(i) != NULL) hs_drphlep_14tev_ljets.at(i)->Add(hs_drphlep_14tev.at(i));
	    }
	} else if (ich == 2) {
	    if (!emuonly) {
		for (int itmp = 0; itmp < hs_drphlep_14tev.size(); itmp++) {
		    if (hs_drphlep_14tev.at(itmp) != NULL) 
		        hs_drphlep_14tev_ll.push_back((TH1F*)hs_drphlep_14tev.at(itmp)->Clone());
		    else hs_drphlep_14tev_ll.push_back(NULL);
	    	}
	    }
	} else if (ich == 3) {
	    if (!emuonly) {
		for (int i = 0; i < hs_drphlep_14tev_ll.size(); i++) {
	    	    if (hs_drphlep_14tev_ll.at(i) == NULL && hs_drphlep_14tev.at(i) != NULL) hs_drphlep_14tev_ll.at(i) = hs_drphlep_14tev.at(i);
	    	    else if (hs_drphlep_14tev_ll.at(i) != NULL && hs_drphlep_14tev.at(i) != NULL) hs_drphlep_14tev_ll.at(i)->Add(hs_drphlep_14tev.at(i));
	    	}
	    } else {
		hs_drphlep_14tev_ll = hs_drphlep_14tev;
	    }
	} else if (ich == 4) {
	    if (!emuonly) {
		for (int i = 0; i < hs_drphlep_14tev_ll.size(); i++) {
	    	    if (hs_drphlep_14tev_ll.at(i) == NULL && hs_drphlep_14tev.at(i) != NULL) hs_drphlep_14tev_ll.at(i) = hs_drphlep_14tev.at(i);
	    	    else if (hs_drphlep_14tev_ll.at(i) != NULL && hs_drphlep_14tev.at(i) != NULL) hs_drphlep_14tev_ll.at(i)->Add(hs_drphlep_14tev.at(i));
	    	}
	    }
	}

	if (ich == 0) {
	    for (int itmp = 0; itmp < hs_detall_14tev.size(); itmp++) {
		if (hs_detall_14tev.at(itmp) != NULL) 
		    hs_detall_14tev_ljets.push_back((TH1F*)hs_detall_14tev.at(itmp)->Clone());
		else hs_detall_14tev_ljets.push_back(NULL);
	    }
	} else if (ich == 1) {
	    for (int i = 0; i < hs_detall_14tev_ljets.size(); i++) {
		if (hs_detall_14tev_ljets.at(i) == NULL && hs_detall_14tev.at(i) != NULL) hs_detall_14tev_ljets.at(i) = hs_detall_14tev.at(i);
		else if (hs_detall_14tev_ljets.at(i) != NULL && hs_detall_14tev.at(i) != NULL) hs_detall_14tev_ljets.at(i)->Add(hs_detall_14tev.at(i));
	    }
	} else if (ich == 2) {
	    if (!emuonly) {
		for (int itmp = 0; itmp < hs_detall_14tev.size(); itmp++) {
		    if (hs_detall_14tev.at(itmp) != NULL) 
		        hs_detall_14tev_ll.push_back((TH1F*)hs_detall_14tev.at(itmp)->Clone());
		    else hs_detall_14tev_ll.push_back(NULL);
	    	}
	    }
	} else if (ich == 3) {
	    if (!emuonly) {
		for (int i = 0; i < hs_detall_14tev_ll.size(); i++) {
	    	    if (hs_detall_14tev_ll.at(i) == NULL && hs_detall_14tev.at(i) != NULL) hs_detall_14tev_ll.at(i) = hs_detall_14tev.at(i);
	    	    else if (hs_detall_14tev_ll.at(i) != NULL && hs_detall_14tev.at(i) != NULL) hs_detall_14tev_ll.at(i)->Add(hs_detall_14tev.at(i));
	    	}
	    } else {
		hs_detall_14tev_ll = hs_detall_14tev;
	    }
	} else if (ich == 4) {
	    if (!emuonly) {
		for (int i = 0; i < hs_detall_14tev_ll.size(); i++) {
	    	    if (hs_detall_14tev_ll.at(i) == NULL && hs_detall_14tev.at(i) != NULL) hs_detall_14tev_ll.at(i) = hs_detall_14tev.at(i);
	    	    else if (hs_detall_14tev_ll.at(i) != NULL && hs_detall_14tev.at(i) != NULL) hs_detall_14tev_ll.at(i)->Add(hs_detall_14tev.at(i));
	    	}
	    }
	}

	if (ich == 0) {
	    for (int itmp = 0; itmp < hs_dphill_14tev.size(); itmp++) {
		if (hs_dphill_14tev.at(itmp) != NULL) 
		    hs_dphill_14tev_ljets.push_back((TH1F*)hs_dphill_14tev.at(itmp)->Clone());
		else hs_dphill_14tev_ljets.push_back(NULL);
	    }
	} else if (ich == 1) {
	    for (int i = 0; i < hs_dphill_14tev_ljets.size(); i++) {
		if (hs_dphill_14tev_ljets.at(i) == NULL && hs_dphill_14tev.at(i) != NULL) hs_dphill_14tev_ljets.at(i) = hs_dphill_14tev.at(i);
		else if (hs_dphill_14tev_ljets.at(i) != NULL && hs_dphill_14tev.at(i) != NULL) hs_dphill_14tev_ljets.at(i)->Add(hs_dphill_14tev.at(i));
	    }
	} else if (ich == 2) {
	    if (!emuonly) {
		for (int itmp = 0; itmp < hs_dphill_14tev.size(); itmp++) {
		    if (hs_dphill_14tev.at(itmp) != NULL) 
		        hs_dphill_14tev_ll.push_back((TH1F*)hs_dphill_14tev.at(itmp)->Clone());
		    else hs_dphill_14tev_ll.push_back(NULL);
	    	}
	    }
	} else if (ich == 3) {
	    if (!emuonly) {
		for (int i = 0; i < hs_dphill_14tev_ll.size(); i++) {
	    	    if (hs_dphill_14tev_ll.at(i) == NULL && hs_dphill_14tev.at(i) != NULL) hs_dphill_14tev_ll.at(i) = hs_dphill_14tev.at(i);
	    	    else if (hs_dphill_14tev_ll.at(i) != NULL && hs_dphill_14tev.at(i) != NULL) hs_dphill_14tev_ll.at(i)->Add(hs_dphill_14tev.at(i));
	    	}
	    } else {
		hs_dphill_14tev_ll = hs_dphill_14tev;
	    }
	} else if (ich == 4) {
	    if (!emuonly) {
		for (int i = 0; i < hs_dphill_14tev_ll.size(); i++) {
	    	    if (hs_dphill_14tev_ll.at(i) == NULL && hs_dphill_14tev.at(i) != NULL) hs_dphill_14tev_ll.at(i) = hs_dphill_14tev.at(i);
	    	    else if (hs_dphill_14tev_ll.at(i) != NULL && hs_dphill_14tev.at(i) != NULL) hs_dphill_14tev_ll.at(i)->Add(hs_dphill_14tev.at(i));
	    	}
	    }
	}

	hss_leadphpt_14tev.push_back(hs_leadphpt_14tev);
	hss_leadpheta_14tev.push_back(hs_leadpheta_14tev);
	hss_drphlep_14tev.push_back(hs_drphlep_14tev);
	hss_detall_14tev.push_back(hs_detall_14tev);
	hss_dphill_14tev.push_back(hs_dphill_14tev);

	if (debug) cout << "tag21" << endl;
    }
    
    if (debug) cout << "tag3" << endl;

    string savename = "results/Upgrade/All_new";
    if (emuonly) savename += "_emuonly";
    if (pt1000) savename += "_pt1000";
    savename += ".root";
    TFile* fout = new TFile(savename.c_str(), "recreate");
    fout->cd();
    cout << "file save to -> " << savename << endl;
    
    channels.push_back("ljets");
    channels.push_back("ll");
    hss_leadphpt_14tev.push_back(hs_leadphpt_14tev_ljets); hss_leadphpt_14tev.push_back(hs_leadphpt_14tev_ll);
    hss_leadpheta_14tev.push_back(hs_leadpheta_14tev_ljets); hss_leadpheta_14tev.push_back(hs_leadpheta_14tev_ll);
    hss_drphlep_14tev.push_back(hs_drphlep_14tev_ljets); hss_drphlep_14tev.push_back(hs_drphlep_14tev_ll);
    hss_detall_14tev.push_back(hs_detall_14tev_ljets); hss_detall_14tev.push_back(hs_detall_14tev_ll);
    hss_dphill_14tev.push_back(hs_dphill_14tev_ljets); hss_dphill_14tev.push_back(hs_dphill_14tev_ll);

    for (int ich = 0; ich < channels.size(); ich++) {
	for (int ipro = 0; ipro < processes.size(); ipro++) {
	    TH1F* h_tmp_leadphpt = hss_leadphpt_14tev.at(ich).at(ipro);
	    if (h_tmp_leadphpt != NULL) {
		string hname_leadphpt = "LeadPhPt_" + processes.at(ipro) + "_" + channels.at(ich);
		h_tmp_leadphpt->Write(hname_leadphpt.c_str());
	    }
	    TH1F* h_tmp_leadpheta = hss_leadpheta_14tev.at(ich).at(ipro);
	    if (h_tmp_leadpheta != NULL) {
		string hname_leadpheta = "LeadPhAbsEta_" + processes.at(ipro) + "_" + channels.at(ich);
		h_tmp_leadpheta->Write(hname_leadpheta.c_str());
	    }
	    TH1F* h_tmp_drphlep = hss_drphlep_14tev.at(ich).at(ipro);
	    if (h_tmp_drphlep != NULL) {
		string hname_drphlep = "LeadPhMinDrPhLep_" + processes.at(ipro) + "_" + channels.at(ich);
		h_tmp_drphlep->Write(hname_drphlep.c_str());
	    }
	    TH1F* h_tmp_detall = hss_detall_14tev.at(ich).at(ipro);
	    if (h_tmp_detall != NULL) {
		string hname_detall = "DEtaLepLep_" + processes.at(ipro) + "_" + channels.at(ich);
		h_tmp_detall->Write(hname_detall.c_str());
	    }
	    TH1F* h_tmp_dphill = hss_dphill_14tev.at(ich).at(ipro);
	    if (h_tmp_dphill != NULL) {
		string hname_dphill = "DPhiLepLep_" + processes.at(ipro) + "_" + channels.at(ich);
		h_tmp_dphill->Write(hname_dphill.c_str());
	    }
	}
    }

    fout->Close();
    delete fout;
}

void FindLastBin(TH1F* h, double &n, double &e, bool debug) {
    bool found = false;
    for (int i = 1; i <= h->GetNbinsX(); i++) {
	if (debug) cout << h->GetXaxis()->GetBinLabel(i) << " " << h->GetBinContent(i) << endl;
	if (string(h->GetXaxis()->GetBinLabel(i)).find("DrPhLep1.0",0) != string::npos) {
	    n = h->GetBinContent(i);
	    e = h->GetBinError(i);
	    found = true;
	    break;
	}
    }
    if (!found) {
	n = 0;
	e = 0;
    }
}

void RecordCutflow(TH1F* h, vector<pair<string,double> > &cf, string channel, string cme) {
    for (int i = 1; i <= h->GetNbinsX(); i++) {
	string cutname = h->GetXaxis()->GetBinLabel(i);
	double cutval = h->GetBinContent(i);
	if (channel == "ejets" || channel == "mujets") {
	    if (cme == "13tev") cutval *= 1.30/1.16;
	}
	if (channel == "ee" || channel == "mumu" || channel == "emu") {
	    if (cme == "13tev") cutval *= 1.44/1.16;
	}
	if (cutname == "Cut:Region") continue;
	if (cutname == "Cut:MuPt27.5") continue;
	if (cutname == "Cut:OneGoodPh") continue;
	if (cutname == "Cut:PhIsoFCT") continue;
	if (channel != "ejets" && cutname == "Cut:ZVeto") cutval = 0;
	if (channel != "ee" && channel != "mumu" && (cutname == "Cut:MllZveto" || cutname == "Cut:MllyZveto" || cutname == "Cut:MET>30")) cutval = 0;
	if (cutname == "Cut:PhMatch") cutname = "Cut:n(#gamma)";
	if (cutname == "Cut:SubRegion") cutname = "Cut:n(l)";
	if (cutname == "Cut:NjetGeq4") cutname = "Cut:n(j)";
	if (cutname == "Cut:NbjetGeq1") cutname = "Cut:n(bj)";
	if (cutname == "Cut:ZVeto") cutname = "Cut:m(e,#gamma) veto";
	if (cutname == "Cut:MllZveto") cutname = "Cut:m(l,l) veto";
	if (cutname == "Cut:MllyZveto") cutname = "Cut:m(l,l,#gamma) veto";
	if (cutname == "Cut:MET>30") cutname = "Cut:MET";
	if (cutname == "Cut:DrPhLep1.0") cutname = "Cut:#DeltaR(#gamma,l)";
	cutname = cutname.substr(4, cutname.size()-4);
	cf.push_back(pair<string,double>(cutname, cutval));
	if (string(h->GetXaxis()->GetBinLabel(i+1)).find("STOP",0) != string::npos) break;
    }
}
