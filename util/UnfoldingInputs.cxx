#include "RecoLevel.h"
#include <TRandom3.h>
#include "ParticleLevel.h"
#include "PartonLevel.h"
#include <TStopwatch.h>
#include <TLorentzVector.h>
#include <iostream>
#include <TH1F.h>
#include <TH2F.h>
#include <TFile.h>
#include <map>

using namespace std;

std::pair<bool, bool> isPromptElectron(int type, int origin, int egMotherType, int egMotherOrigin, int egMotherPdgId, int RecoCharge){
    // 43 is "diboson" origin, but is needed due to buggy origin flags in Sherpa ttbar
    bool isprompt = (type == 2 || 
	(type == 4 && origin == 5 && fabs(egMotherPdgId) == 11) ||
	// bkg electrons from ElMagDecay with origin top, W or Z, higgs, diBoson
	(type == 4 && origin == 7 && egMotherType == 2 && (egMotherOrigin == 10 || egMotherOrigin == 12 || egMotherOrigin == 13 || egMotherOrigin == 14 || egMotherOrigin == 43) && fabs(egMotherPdgId) == 11) ||
	// unknown electrons from multi-boson (sherpa 222, di-boson)
	(type == 1 && egMotherType == 2 && egMotherOrigin == 47 && fabs(egMotherPdgId) == 11) );

    bool isChargeFl = false;
    if (egMotherPdgId*RecoCharge>0) isChargeFl = true;
    if (isprompt) return std::make_pair(true,isChargeFl); //charge flipped electrons are also considered prompt

    // bkg photons from photon conv from FSR (must check!!)
    if (type == 4 && origin == 5 && egMotherOrigin == 40) return std::make_pair(true,false);  
    // non-iso photons from FSR for the moment but we must check!! (must check!!)
    if (type == 15 && origin == 40) return std::make_pair(true,false);  
    // mainly in Sherpa Zee, but some also in Zmumu
    if (type == 4 && origin == 7 && egMotherType == 15 && egMotherOrigin == 40) return std::make_pair(true,false); 

    return std::make_pair(false,false);
}

//void AddOverflow(TH1F*h);
TH2F *Norm2DHRow(TH2F* h_2d);
TH2F *Norm2DHColumn(TH2F* h_2d);
string GetName(int i);

bool debug = false;
bool ISRFSRUp = false;
bool ISRFSRDn = false;
bool PS = false;
bool PSNew = false;
bool NominalNew = false;
bool TTBar = false;
bool Upgrade = false;
bool Upgrade2 = false;
bool Upgrade3 = false;
int NEvt = -1;
bool emuonly = false;
bool pt500 = false;
bool pt1000 = false;
bool pt2000 = false;
bool ISRPhoton = false;

bool IsLepton(int pdg);

int main(int argc, char * argv[]) {

    TRandom *m_random = new TRandom3();
    TStopwatch t; t.Start();

    for (int i = 0; i < argc; i++) {
	if (!strcmp(argv[i],"--debug")) {
	    debug = true;
   	}
	if (!strcmp(argv[i],"--ISRFSRUp")) {
	    ISRFSRUp = true;
   	}
	if (!strcmp(argv[i],"--ISRFSRDn")) {
	    ISRFSRDn = true;
   	}
	if (!strcmp(argv[i],"--PS")) {
	    PS = true;
   	}
	if (!strcmp(argv[i],"--PSNew")) {
	    PSNew = true;
   	}
	if (!strcmp(argv[i],"--NominalNew")) {
	    NominalNew = true;
   	}
	if (!strcmp(argv[i],"--TTBar")) {
	    TTBar = true;
   	}
	if (!strcmp(argv[i],"--Upgrade")) {
	    Upgrade = true;
   	}
	if (!strcmp(argv[i],"--Upgrade2")) {
	    Upgrade2 = true;
   	}
	if (!strcmp(argv[i],"--Upgrade3")) {
	    Upgrade3 = true;
   	}
	if (!strcmp(argv[i],"--emu")) {
	    emuonly = true;
   	}
	if (!strcmp(argv[i],"--pt500")) {
	    pt500 = true;
   	}
	if (!strcmp(argv[i],"--pt1000")) {
	    pt1000 = true;
   	}
	if (!strcmp(argv[i],"--pt2000")) {
	    pt2000 = true;
   	}
	if (!strcmp(argv[i],"--ISRPhoton")) {
	    ISRPhoton = true;
   	}
	if (!strcmp(argv[i],"--NEvt")) {
	    NEvt = atoi(string(argv[i+1]).c_str());
   	}
    }

    if (Upgrade) {
	cout << "Running Upgrade mode" << endl;
	if (Upgrade2) {
	    cout << "Running in addition Upgrade2 mode" << endl;
	}
	if (Upgrade3) {
	    cout << "Running in addition Upgrade3 mode" << endl;
	}
    }
    if (emuonly) cout  << " Only emu~"<<endl;
    if (pt500) cout  << " pt up to 500~"<<endl;
    if (pt1000) cout  << " pt up to 1000~"<<endl;
    if (pt2000) cout  << " pt up to 2000~"<<endl;

    vector<string> filenames;
    if (ISRFSRUp) {
	filenames.push_back("/eos/user/c/caudron2/TtGamma_PL/v010/410404.MadGraphPythia8EvtGen_A14NNPDF23Var3cUp_ttgamma_nonallhad.p3152.PL5.001.root");
    } else if (ISRFSRDn) {
	filenames.push_back("/eos/user/c/caudron2/TtGamma_PL/v010/410405.MadGraphPythia8EvtGen_A14NNPDF23Var3cDown_ttgamma_nonallhad.p3152.PL5.001.root");
    } else if (PS) {
	filenames.push_back("eos/user/c/caudron2/TtGamma_PL/v010/410395.ttgamma_noallhad_MGH7EG_H7UE.p3152.PL5.001.root");
    } else if (PSNew) {
	filenames.push_back("/eos/user/c/caudron2/TtGamma_PL/v010/410395.ttgamma_noallhad_MGH7EG_H7UE.p3152.PL6.001.root");
    } else if (NominalNew) {
	//filename = "/eos/user/c/caudron2/TtGamma_PL/v010/410389.ttgamma_noallhad.p3152.PL6.001.root";
	filenames.push_back("/tmp/yili/user.caudron.410389.MadGraphPythia8EvtGen.DAOD_TOPQ1.e6155_s2726_r7772_r7676_p3152.mntPL6_010a_output.root/output.root");
    } else if (TTBar) {
	filenames.push_back("/eos/user/c/caudron2/TtGamma_PL/v009/410501.ttbar_nonallhad_P8.p3138.PL4.001.root");
    } else if (Upgrade) {
	    filenames.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v6/user.adurglis.410389.MadGraphPythia8EvtGe.DAOD_TRUTH1.e6540_p3401.v06_output_root/user.adurglis.15814602._000001.output.root");
	    filenames.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v6/user.adurglis.410389.MadGraphPythia8EvtGe.DAOD_TRUTH1.e6540_p3401.v06_output_root/user.adurglis.15814602._000002.output.root");
	    filenames.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v6/user.adurglis.410389.MadGraphPythia8EvtGe.DAOD_TRUTH1.e6540_p3401.v06_output_root/user.adurglis.15814602._000003.output.root");
	    filenames.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v6/user.adurglis.410389.MadGraphPythia8EvtGe.DAOD_TRUTH1.e6540_p3401.v06_output_root/user.adurglis.15814602._000004.output.root");
	    filenames.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v6/user.adurglis.410389.MadGraphPythia8EvtGe.DAOD_TRUTH1.e6540_p3401.v06_output_root/user.adurglis.15814602._000005.output.root");
	    filenames.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v6/user.adurglis.410389.MadGraphPythia8EvtGe.DAOD_TRUTH1.e6540_p3401.v06_output_root/user.adurglis.15814602._000006.output.root");
	    filenames.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v6/user.adurglis.410389.MadGraphPythia8EvtGe.DAOD_TRUTH1.e6540_p3401.v06_output_root/user.adurglis.15814602._000007.output.root");
    } else {
	filenames.push_back("/eos/user/c/caudron2/TtGamma_PL/v010/410389.ttgamma_noallhad.p3152.PL5.001.root");
    }

    // for nominal study
    string name_cutflow_reco_ej[20] = {"all","ejets","njet>=4","nph=1","iso","origin","nbjet>=1","zveto","dr"};
    string name_cutflow_truth_ej[20] = {"all","ejets_pl","njet>=4","nbjet>=1","nph=1","drphj0.4","drphl1.0"};
    string name_cutflow_reco_mj[20] = {"all","mujets","njet>=4","nph=1","iso","origin","nbjet>=1","dr"};
    string name_cutflow_truth_mj[20] = {"all","mujets_pl","njet>=4","nbjet>=1","nph=1","drphj0.4","drphl1.0"};
    string name_cutflow_reco_ee[20] = {"all","ee","njet>=2","nph=1","iso","origin","nbjet>=1","mll","mlly","MET","dr"};
    string name_cutflow_truth_ee[20] = {"all","ee_pl","njet>=2","nbjet>=1","nph=1","drphj0.4","drphl1.0"};
    string name_cutflow_reco_emu[20] = {"all","emu","njet>=2","nph=1","iso","origin","nbjet>=1","dr"};
    string name_cutflow_truth_emu[20] = {"all","emu_pl","njet>=2","nbjet>=1","nph=1","drphj0.4","drphl1.0"};
    string name_cutflow_reco_mumu[20] = {"all","mumu","njet>=2","nph=1","iso","origin","nbjet>=1","mll","mlly","MET","dr"};
    string name_cutflow_truth_mumu[20] = {"all","mummu_pl","njet>=2","nbjet>=1","nph=1","drphj0.4","drphl1.0"};
    double cutflow[110][7][2][20] = {};
    double cutflow_err[110][7][2][20] = {};
    int end_idx[7][2] = {{8,6}, {8,6}, {10,6}, {10,6}, {10,6}, {8,6}, {10,6}};
    TH1F* h_ph_pt[110][2][5] = {};
    TH1F* h_njet[2] = {};
    TH1F* h_ph_abseta[110][2][5] = {};
    TH1F* h_dR_ph_lep[110][2][5] = {};
    TH1F* h_dphi_ll[110][2][5] = {};
    TH1F* h_deta_ll[110][2][5] = {};
    TH2F* h2_ph_pt[110][2] = {};
    TH2F* h2_llp_pt[110][2] = {};
    TH2F* h2_ph_abseta[110][2] = {};
    TH2F* h2_dR_ph_lep[110][2] = {};
    TH2F* h2_dphi_ll[110][2] = {};
    TH2F* h2_deta_ll[110][2] = {};

    TH1F* h_dR_truth_reco[2][2] = {};
    TH2F* h2_ph_pt_nomatch[2] = {};
    TH2F* h2_ph_abseta_nomatch[2] = {};
    TH2F* h2_dR_ph_lep_nomatch[2] = {};
    TH2F* h2_dphi_ll_nomatch[2] = {};
    TH2F* h2_deta_ll_nomatch[2] = {};

    double m_efake_dr = 0.05;
    double event_norm = 0.0008098;
    if (PS || PSNew) event_norm = 1.511e-07;
    if (Upgrade) {
	event_norm = 5433.8 / 12416.2;
    }
    double lumi = 36097.56;
    if (Upgrade) lumi = 3000.;
    double m_Z = 91187.6;

    {
	h_dR_truth_reco[0][0] = new TH1F("dR_truth_reco_photon_ljets","dR_truth_reco_photon_ljets",50,0,1); h_dR_truth_reco[0][0]->Sumw2();
	h_dR_truth_reco[1][0] = new TH1F("dR_truth_reco_lepton_ljets","dR_truth_reco_lepton_ljets",50,0,1); h_dR_truth_reco[1][0]->Sumw2();
	h_dR_truth_reco[0][1] = new TH1F("dR_truth_reco_photon_ll","dR_truth_reco_photon_ll",50,0,1); h_dR_truth_reco[0][1]->Sumw2();
	h_dR_truth_reco[1][1] = new TH1F("dR_truth_reco_lepton_ll","dR_truth_reco_lepton_ll",50,0,1); h_dR_truth_reco[1][1]->Sumw2();
    }

    // ph pt
    double limit_ph_pt = 300; double newval_ph_pt = 200;
    for (int i = 0; i < 110; i++) {
	if (pt500) {
	    limit_ph_pt = 500; newval_ph_pt = 400;
	    Double_t bins[] = {20,35,50,65,80, 95 ,110,140, 180,300,500};
	    int binnum = sizeof(bins)/sizeof(Double_t) - 1;
	    char tmpch[100];
	    sprintf(tmpch, "reco_ph_pt_sinlepton_weight%d", i); h_ph_pt[i][0][0] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][0][0]->Sumw2();
	    sprintf(tmpch, "true_ph_pt_sinlepton_weight%d", i); h_ph_pt[i][0][1] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][0][1]->Sumw2();
	    sprintf(tmpch, "reco_fid_ph_pt_sinlepton_weight%d", i); h_ph_pt[i][0][2] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][0][2]->Sumw2();
	    sprintf(tmpch, "reco_fid_match_ph_pt_sinlepton_weight%d", i); h_ph_pt[i][0][3] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][0][3]->Sumw2();
	    sprintf(tmpch, "reco_failfid_match_ph_pt_sinlepton_weight%d", i); h_ph_pt[i][0][4] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][0][4]->Sumw2();
	    sprintf(tmpch, "reco_ph_pt_dilepton_weight%d", i); h_ph_pt[i][1][0] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][1][0]->Sumw2();
	    sprintf(tmpch, "true_ph_pt_dilepton_weight%d", i); h_ph_pt[i][1][1] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][1][1]->Sumw2();
	    sprintf(tmpch, "reco_fid_ph_pt_dilepton_weight%d", i); h_ph_pt[i][1][2] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][1][2]->Sumw2();
	    sprintf(tmpch, "reco_fid_match_ph_pt_dilepton_weight%d", i); h_ph_pt[i][1][3] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][1][3]->Sumw2();
	    sprintf(tmpch, "reco_failfid_match_ph_pt_dilepton_weight%d", i); h_ph_pt[i][1][4] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][1][4]->Sumw2();
	    sprintf(tmpch, "mig_ph_pt_sinlepton_weight%d", i); h2_ph_pt[i][0] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_ph_pt[i][0]->Sumw2();
	    sprintf(tmpch, "mig_ph_pt_dilepton_weight%d", i); h2_ph_pt[i][1] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_ph_pt[i][1]->Sumw2();
	    if (i == 0) {
	    sprintf(tmpch, "mig_ph_pt_sinlepton_nomatch"); h2_ph_pt_nomatch[0] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_ph_pt_nomatch[0]->Sumw2();
	    sprintf(tmpch, "mig_ph_pt_dilepton_nomatch"); h2_ph_pt_nomatch[1] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_ph_pt_nomatch[1]->Sumw2();
	    }
	    sprintf(tmpch, "mig_llp_pt_sinlepton_weight%d", i); h2_llp_pt[i][0] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_llp_pt[i][0]->Sumw2();
	    sprintf(tmpch, "mig_llp_pt_dilepton_weight%d", i); h2_llp_pt[i][1] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_llp_pt[i][1]->Sumw2();
	} else if (pt1000) {
	    limit_ph_pt = 1000; newval_ph_pt = 800;
	    Double_t bins[] = {20,35,50,65,80, 95 ,110,140, 180,300,500, 1000};
	    int binnum = sizeof(bins)/sizeof(Double_t) - 1;
	    char tmpch[100];
	    sprintf(tmpch, "reco_ph_pt_sinlepton_weight%d", i); h_ph_pt[i][0][0] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][0][0]->Sumw2();
	    sprintf(tmpch, "true_ph_pt_sinlepton_weight%d", i); h_ph_pt[i][0][1] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][0][1]->Sumw2();
	    sprintf(tmpch, "reco_fid_ph_pt_sinlepton_weight%d", i); h_ph_pt[i][0][2] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][0][2]->Sumw2();
	    sprintf(tmpch, "reco_fid_match_ph_pt_sinlepton_weight%d", i); h_ph_pt[i][0][3] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][0][3]->Sumw2();
	    sprintf(tmpch, "reco_failfid_match_ph_pt_sinlepton_weight%d", i); h_ph_pt[i][0][4] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][0][4]->Sumw2();
	    sprintf(tmpch, "reco_ph_pt_dilepton_weight%d", i); h_ph_pt[i][1][0] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][1][0]->Sumw2();
	    sprintf(tmpch, "true_ph_pt_dilepton_weight%d", i); h_ph_pt[i][1][1] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][1][1]->Sumw2();
	    sprintf(tmpch, "reco_fid_ph_pt_dilepton_weight%d", i); h_ph_pt[i][1][2] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][1][2]->Sumw2();
	    sprintf(tmpch, "reco_fid_match_ph_pt_dilepton_weight%d", i); h_ph_pt[i][1][3] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][1][3]->Sumw2();
	    sprintf(tmpch, "reco_failfid_match_ph_pt_dilepton_weight%d", i); h_ph_pt[i][1][4] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][1][4]->Sumw2();
	    sprintf(tmpch, "mig_ph_pt_sinlepton_weight%d", i); h2_ph_pt[i][0] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_ph_pt[i][0]->Sumw2();
	    sprintf(tmpch, "mig_ph_pt_dilepton_weight%d", i); h2_ph_pt[i][1] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_ph_pt[i][1]->Sumw2();
	    if (i == 0) {
	    sprintf(tmpch, "mig_ph_pt_sinlepton_nomatch"); h2_ph_pt_nomatch[0] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_ph_pt_nomatch[0]->Sumw2();
	    sprintf(tmpch, "mig_ph_pt_dilepton_nomatch"); h2_ph_pt_nomatch[1] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_ph_pt_nomatch[1]->Sumw2();
	    }
	    sprintf(tmpch, "mig_llp_pt_sinlepton_weight%d", i); h2_llp_pt[i][0] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_llp_pt[i][0]->Sumw2();
	    sprintf(tmpch, "mig_llp_pt_dilepton_weight%d", i); h2_llp_pt[i][1] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_llp_pt[i][1]->Sumw2();
	} else if (pt2000) {
	    limit_ph_pt = 2000; newval_ph_pt = 1500;
	    Double_t bins[] = {20,35,50,65,80, 95 ,110,140, 180,300,500, 1000, 2000};
	    int binnum = sizeof(bins)/sizeof(Double_t) - 1;
	    char tmpch[100];
	    sprintf(tmpch, "reco_ph_pt_sinlepton_weight%d", i); h_ph_pt[i][0][0] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][0][0]->Sumw2();
	    sprintf(tmpch, "true_ph_pt_sinlepton_weight%d", i); h_ph_pt[i][0][1] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][0][1]->Sumw2();
	    sprintf(tmpch, "reco_fid_ph_pt_sinlepton_weight%d", i); h_ph_pt[i][0][2] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][0][2]->Sumw2();
	    sprintf(tmpch, "reco_fid_match_ph_pt_sinlepton_weight%d", i); h_ph_pt[i][0][3] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][0][3]->Sumw2();
	    sprintf(tmpch, "reco_failfid_match_ph_pt_sinlepton_weight%d", i); h_ph_pt[i][0][4] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][0][4]->Sumw2();
	    sprintf(tmpch, "reco_ph_pt_dilepton_weight%d", i); h_ph_pt[i][1][0] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][1][0]->Sumw2();
	    sprintf(tmpch, "true_ph_pt_dilepton_weight%d", i); h_ph_pt[i][1][1] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][1][1]->Sumw2();
	    sprintf(tmpch, "reco_fid_ph_pt_dilepton_weight%d", i); h_ph_pt[i][1][2] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][1][2]->Sumw2();
	    sprintf(tmpch, "reco_fid_match_ph_pt_dilepton_weight%d", i); h_ph_pt[i][1][3] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][1][3]->Sumw2();
	    sprintf(tmpch, "reco_failfid_match_ph_pt_dilepton_weight%d", i); h_ph_pt[i][1][4] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][1][4]->Sumw2();
	    sprintf(tmpch, "mig_ph_pt_sinlepton_weight%d", i); h2_ph_pt[i][0] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_ph_pt[i][0]->Sumw2();
	    sprintf(tmpch, "mig_ph_pt_dilepton_weight%d", i); h2_ph_pt[i][1] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_ph_pt[i][1]->Sumw2();
	    if (i == 0) {
	    sprintf(tmpch, "mig_ph_pt_sinlepton_nomatch"); h2_ph_pt_nomatch[0] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_ph_pt_nomatch[0]->Sumw2();
	    sprintf(tmpch, "mig_ph_pt_dilepton_nomatch"); h2_ph_pt_nomatch[1] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_ph_pt_nomatch[1]->Sumw2();
	    }
	    sprintf(tmpch, "mig_llp_pt_sinlepton_weight%d", i); h2_llp_pt[i][0] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_llp_pt[i][0]->Sumw2();
	    sprintf(tmpch, "mig_llp_pt_dilepton_weight%d", i); h2_llp_pt[i][1] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_llp_pt[i][1]->Sumw2();
	} else {
	    Double_t bins[] = {20,35,50,65,80, 95 ,110,140, 180,300};
	    int binnum = sizeof(bins)/sizeof(Double_t) - 1;
	    char tmpch[100];
	    sprintf(tmpch, "reco_ph_pt_sinlepton_weight%d", i); h_ph_pt[i][0][0] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][0][0]->Sumw2();
	    sprintf(tmpch, "true_ph_pt_sinlepton_weight%d", i); h_ph_pt[i][0][1] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][0][1]->Sumw2();
	    sprintf(tmpch, "reco_fid_ph_pt_sinlepton_weight%d", i); h_ph_pt[i][0][2] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][0][2]->Sumw2();
	    sprintf(tmpch, "reco_fid_match_ph_pt_sinlepton_weight%d", i); h_ph_pt[i][0][3] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][0][3]->Sumw2();
	    sprintf(tmpch, "reco_failfid_match_ph_pt_sinlepton_weight%d", i); h_ph_pt[i][0][4] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][0][4]->Sumw2();
	    sprintf(tmpch, "reco_ph_pt_dilepton_weight%d", i); h_ph_pt[i][1][0] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][1][0]->Sumw2();
	    sprintf(tmpch, "true_ph_pt_dilepton_weight%d", i); h_ph_pt[i][1][1] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][1][1]->Sumw2();
	    sprintf(tmpch, "reco_fid_ph_pt_dilepton_weight%d", i); h_ph_pt[i][1][2] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][1][2]->Sumw2();
	    sprintf(tmpch, "reco_fid_match_ph_pt_dilepton_weight%d", i); h_ph_pt[i][1][3] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][1][3]->Sumw2();
	    sprintf(tmpch, "reco_failfid_match_ph_pt_dilepton_weight%d", i); h_ph_pt[i][1][4] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_pt[i][1][4]->Sumw2();
	    sprintf(tmpch, "mig_ph_pt_sinlepton_weight%d", i); h2_ph_pt[i][0] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_ph_pt[i][0]->Sumw2();
	    sprintf(tmpch, "mig_ph_pt_dilepton_weight%d", i); h2_ph_pt[i][1] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_ph_pt[i][1]->Sumw2();
	    if (i == 0) {
	    sprintf(tmpch, "mig_ph_pt_sinlepton_nomatch"); h2_ph_pt_nomatch[0] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_ph_pt_nomatch[0]->Sumw2();
	    sprintf(tmpch, "mig_ph_pt_dilepton_nomatch"); h2_ph_pt_nomatch[1] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_ph_pt_nomatch[1]->Sumw2();
	    }
	    sprintf(tmpch, "mig_llp_pt_sinlepton_weight%d", i); h2_llp_pt[i][0] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_llp_pt[i][0]->Sumw2();
	    sprintf(tmpch, "mig_llp_pt_dilepton_weight%d", i); h2_llp_pt[i][1] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_llp_pt[i][1]->Sumw2();
	}
    }
    {
	Double_t bins[] = {0,1,2,3,4,5,6,7,8,9};
	int binnum = sizeof(bins)/sizeof(Double_t) - 1;
	char tmpch[100];
	sprintf(tmpch, "njet_sinlepton_weight%d", 0); h_njet[0] = new TH1F(tmpch, tmpch, binnum, bins); h_njet[0]->Sumw2();
	sprintf(tmpch, "njet_dilepton_weight%d", 0); h_njet[1] = new TH1F(tmpch, tmpch, binnum, bins); h_njet[1]->Sumw2();
    }
    // ph abseta
    double limit_ph_abseta = 2.37; double newval_ph_abseta = 2.0;
    for (int i = 0; i < 110; i++) {
	Double_t bins[] = {0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.7, 2.37};
	int binnum = sizeof(bins)/sizeof(Double_t) - 1;
	char tmpch[100];
	sprintf(tmpch, "reco_ph_abseta_sinlepton_weight%d", i); h_ph_abseta[i][0][0] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_abseta[i][0][0]->Sumw2();
	sprintf(tmpch, "true_ph_abseta_sinlepton_weight%d", i); h_ph_abseta[i][0][1] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_abseta[i][0][1]->Sumw2();
	sprintf(tmpch, "reco_fid_ph_abseta_sinlepton_weight%d", i); h_ph_abseta[i][0][2] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_abseta[i][0][2]->Sumw2();
	sprintf(tmpch, "reco_fid_match_ph_abseta_sinlepton_weight%d", i); h_ph_abseta[i][0][3] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_abseta[i][0][3]->Sumw2();
	sprintf(tmpch, "reco_failfid_match_ph_abseta_sinlepton_weight%d", i); h_ph_abseta[i][0][4] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_abseta[i][0][4]->Sumw2();
	sprintf(tmpch, "reco_ph_abseta_dilepton_weight%d", i); h_ph_abseta[i][1][0] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_abseta[i][1][0]->Sumw2();
	sprintf(tmpch, "true_ph_abseta_dilepton_weight%d", i); h_ph_abseta[i][1][1] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_abseta[i][1][1]->Sumw2();
	sprintf(tmpch, "reco_fid_ph_abseta_dilepton_weight%d", i); h_ph_abseta[i][1][2] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_abseta[i][1][2]->Sumw2();
	sprintf(tmpch, "reco_fid_match_ph_abseta_dilepton_weight%d", i); h_ph_abseta[i][1][3] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_abseta[i][1][3]->Sumw2();
	sprintf(tmpch, "reco_failfid_match_ph_abseta_dilepton_weight%d", i); h_ph_abseta[i][1][4] = new TH1F(tmpch, tmpch, binnum, bins); h_ph_abseta[i][1][4]->Sumw2();
	sprintf(tmpch, "mig_ph_abseta_sinlepton_weight%d", i); h2_ph_abseta[i][0] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_ph_abseta[i][0]->Sumw2();
	sprintf(tmpch, "mig_ph_abseta_dilepton_weight%d", i); h2_ph_abseta[i][1] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_ph_abseta[i][1]->Sumw2();
	if (i == 0) {
	sprintf(tmpch, "mig_ph_abseta_sinlepton_nomatch"); h2_ph_abseta_nomatch[0] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_ph_abseta_nomatch[0]->Sumw2();
	sprintf(tmpch, "mig_ph_abseta_dilepton_nomatch"); h2_ph_abseta_nomatch[1] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_ph_abseta_nomatch[1]->Sumw2();
	}
    }
    // dr(ph,lep)
    double limit_dR_ph_lep = 3.5; double newval_dR_ph_lep = 3.0;
    for (int i = 0; i < 110; i++) {
	Double_t bins[] = {1.0,1.2,1.4,1.6,1.8,2.0,2.2,2.4,2.6,3.5};
	int binnum = sizeof(bins)/sizeof(Double_t) - 1;
	char tmpch[100];
	sprintf(tmpch, "reco_dR_ph_lep_sinlepton_weight%d", i); h_dR_ph_lep[i][0][0] = new TH1F(tmpch, tmpch, binnum, bins); h_dR_ph_lep[i][0][0]->Sumw2();
	sprintf(tmpch, "true_dR_ph_lep_sinlepton_weight%d", i); h_dR_ph_lep[i][0][1] = new TH1F(tmpch, tmpch, binnum, bins); h_dR_ph_lep[i][0][1]->Sumw2();
	sprintf(tmpch, "reco_fid_dR_ph_lep_sinlepton_weight%d", i); h_dR_ph_lep[i][0][2] = new TH1F(tmpch, tmpch, binnum, bins); h_dR_ph_lep[i][0][2]->Sumw2();
	sprintf(tmpch, "reco_fid_match_dR_ph_lep_sinlepton_weight%d", i); h_dR_ph_lep[i][0][3] = new TH1F(tmpch, tmpch, binnum, bins); h_dR_ph_lep[i][0][3]->Sumw2();
	sprintf(tmpch, "reco_failfid_match_dR_ph_lep_sinlepton_weight%d", i); h_dR_ph_lep[i][0][4] = new TH1F(tmpch, tmpch, binnum, bins); h_dR_ph_lep[i][0][4]->Sumw2();
	sprintf(tmpch, "reco_dR_ph_lep_dilepton_weight%d", i); h_dR_ph_lep[i][1][0] = new TH1F(tmpch, tmpch, binnum, bins); h_dR_ph_lep[i][1][0]->Sumw2();
	sprintf(tmpch, "true_dR_ph_lep_dilepton_weight%d", i); h_dR_ph_lep[i][1][1] = new TH1F(tmpch, tmpch, binnum, bins); h_dR_ph_lep[i][1][1]->Sumw2();
	sprintf(tmpch, "reco_fid_dR_ph_lep_dilepton_weight%d", i); h_dR_ph_lep[i][1][2] = new TH1F(tmpch, tmpch, binnum, bins); h_dR_ph_lep[i][1][2]->Sumw2();
	sprintf(tmpch, "reco_fid_match_dR_ph_lep_dilepton_weight%d", i); h_dR_ph_lep[i][1][3] = new TH1F(tmpch, tmpch, binnum, bins); h_dR_ph_lep[i][1][3]->Sumw2();
	sprintf(tmpch, "reco_failfid_match_dR_ph_lep_dilepton_weight%d", i); h_dR_ph_lep[i][1][4] = new TH1F(tmpch, tmpch, binnum, bins); h_dR_ph_lep[i][1][4]->Sumw2();
	sprintf(tmpch, "mig_dR_ph_lep_sinlepton_weight%d", i); h2_dR_ph_lep[i][0] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_dR_ph_lep[i][0]->Sumw2();
	sprintf(tmpch, "mig_dR_ph_lep_dilepton_weight%d", i); h2_dR_ph_lep[i][1] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_dR_ph_lep[i][1]->Sumw2();
	if (i == 0) {
	sprintf(tmpch, "mig_dR_ph_lep_sinlepton_nomatch"); h2_dR_ph_lep_nomatch[0] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_dR_ph_lep_nomatch[0]->Sumw2();
	sprintf(tmpch, "mig_dR_ph_lep_dilepton_nomatch"); h2_dR_ph_lep_nomatch[1] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_dR_ph_lep_nomatch[1]->Sumw2();
	}
    }
    // dphi(lep,lep)
    double limit_dphi_ll = 3.14; double newval_dphi_ll = 3.0;
    for (int i = 0; i < 110; i++) {
	Double_t bins[] = {0, 0.35, 0.70, 1.05, 1.40, 1.75, 2.10, 2.45, 2.8, 3.14};
	int binnum = sizeof(bins)/sizeof(Double_t) - 1;
	char tmpch[100];
	sprintf(tmpch, "reco_dphi_ll_sinlepton_weight%d", i); h_dphi_ll[i][0][0] = new TH1F(tmpch, tmpch, binnum, bins); h_dphi_ll[i][0][0]->Sumw2();
	sprintf(tmpch, "true_dphi_ll_sinlepton_weight%d", i); h_dphi_ll[i][0][1] = new TH1F(tmpch, tmpch, binnum, bins); h_dphi_ll[i][0][1]->Sumw2();
	sprintf(tmpch, "reco_fid_dphi_ll_sinlepton_weight%d", i); h_dphi_ll[i][0][2] = new TH1F(tmpch, tmpch, binnum, bins); h_dphi_ll[i][0][2]->Sumw2();
	sprintf(tmpch, "reco_fid_match_dphi_ll_sinlepton_weight%d", i); h_dphi_ll[i][0][3] = new TH1F(tmpch, tmpch, binnum, bins); h_dphi_ll[i][0][3]->Sumw2();
	sprintf(tmpch, "reco_failfid_match_dphi_ll_sinlepton_weight%d", i); h_dphi_ll[i][0][4] = new TH1F(tmpch, tmpch, binnum, bins); h_dphi_ll[i][0][4]->Sumw2();
	sprintf(tmpch, "reco_dphi_ll_dilepton_weight%d", i); h_dphi_ll[i][1][0] = new TH1F(tmpch, tmpch, binnum, bins); h_dphi_ll[i][1][0]->Sumw2();
	sprintf(tmpch, "true_dphi_ll_dilepton_weight%d", i); h_dphi_ll[i][1][1] = new TH1F(tmpch, tmpch, binnum, bins); h_dphi_ll[i][1][1]->Sumw2();
	sprintf(tmpch, "reco_fid_dphi_ll_dilepton_weight%d", i); h_dphi_ll[i][1][2] = new TH1F(tmpch, tmpch, binnum, bins); h_dphi_ll[i][1][2]->Sumw2();
	sprintf(tmpch, "reco_fid_match_dphi_ll_dilepton_weight%d", i); h_dphi_ll[i][1][3] = new TH1F(tmpch, tmpch, binnum, bins); h_dphi_ll[i][1][3]->Sumw2();
	sprintf(tmpch, "reco_failfid_match_dphi_ll_dilepton_weight%d", i); h_dphi_ll[i][1][4] = new TH1F(tmpch, tmpch, binnum, bins); h_dphi_ll[i][1][4]->Sumw2();
	sprintf(tmpch, "mig_dphi_ll_sinlepton_weight%d", i); h2_dphi_ll[i][0] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_dphi_ll[i][0]->Sumw2();
	sprintf(tmpch, "mig_dphi_ll_dilepton_weight%d", i); h2_dphi_ll[i][1] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_dphi_ll[i][1]->Sumw2();
	if (i == 0) {
	sprintf(tmpch, "mig_dphi_ll_sinlepton_nomatch"); h2_dphi_ll_nomatch[0] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_dphi_ll_nomatch[0]->Sumw2();
	sprintf(tmpch, "mig_dphi_ll_dilepton_nomatch"); h2_dphi_ll_nomatch[1] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_dphi_ll_nomatch[1]->Sumw2();
	}
    }
    // deta(lep,lep)
    double limit_deta_ll = 2.5; double newval_deta_ll = 2.0;
    for (int i = 0; i < 110; i++) {
	Double_t bins[] = {0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.7, 2.5};
	int binnum = sizeof(bins)/sizeof(Double_t) - 1;
	char tmpch[100];
	sprintf(tmpch, "reco_deta_ll_sinlepton_weight%d", i); h_deta_ll[i][0][0] = new TH1F(tmpch, tmpch, binnum, bins); h_deta_ll[i][0][0]->Sumw2();
	sprintf(tmpch, "true_deta_ll_sinlepton_weight%d", i); h_deta_ll[i][0][1] = new TH1F(tmpch, tmpch, binnum, bins); h_deta_ll[i][0][1]->Sumw2();
	sprintf(tmpch, "reco_fid_deta_ll_sinlepton_weight%d", i); h_deta_ll[i][0][2] = new TH1F(tmpch, tmpch, binnum, bins); h_deta_ll[i][0][2]->Sumw2();
	sprintf(tmpch, "reco_fid_match_deta_ll_sinlepton_weight%d", i); h_deta_ll[i][0][3] = new TH1F(tmpch, tmpch, binnum, bins); h_deta_ll[i][0][3]->Sumw2();
	sprintf(tmpch, "reco_failfid_match_deta_ll_sinlepton_weight%d", i); h_deta_ll[i][0][4] = new TH1F(tmpch, tmpch, binnum, bins); h_deta_ll[i][0][4]->Sumw2();
	sprintf(tmpch, "reco_deta_ll_dilepton_weight%d", i); h_deta_ll[i][1][0] = new TH1F(tmpch, tmpch, binnum, bins); h_deta_ll[i][1][0]->Sumw2();
	sprintf(tmpch, "true_deta_ll_dilepton_weight%d", i); h_deta_ll[i][1][1] = new TH1F(tmpch, tmpch, binnum, bins); h_deta_ll[i][1][1]->Sumw2();
	sprintf(tmpch, "reco_fid_deta_ll_dilepton_weight%d", i); h_deta_ll[i][1][2] = new TH1F(tmpch, tmpch, binnum, bins); h_deta_ll[i][1][2]->Sumw2();
	sprintf(tmpch, "reco_fid_match_deta_ll_dilepton_weight%d", i); h_deta_ll[i][1][3] = new TH1F(tmpch, tmpch, binnum, bins); h_deta_ll[i][1][3]->Sumw2();
	sprintf(tmpch, "reco_failfid_match_deta_ll_dilepton_weight%d", i); h_deta_ll[i][1][4] = new TH1F(tmpch, tmpch, binnum, bins); h_deta_ll[i][1][4]->Sumw2();
	sprintf(tmpch, "mig_deta_ll_sinlepton_weight%d", i); h2_deta_ll[i][0] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_deta_ll[i][0]->Sumw2();
	sprintf(tmpch, "mig_deta_ll_dilepton_weight%d", i); h2_deta_ll[i][1] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_deta_ll[i][1]->Sumw2();
	if (i == 0) {
	sprintf(tmpch, "mig_deta_ll_sinlepton_nomatch"); h2_deta_ll_nomatch[0] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_deta_ll_nomatch[0]->Sumw2();
	sprintf(tmpch, "mig_deta_ll_dilepton_nomatch"); h2_deta_ll_nomatch[1] = new TH2F(tmpch, tmpch, binnum, bins, binnum, bins); h2_deta_ll_nomatch[1]->Sumw2();
	}
    }

    // for xtalk
    double xtalk[5][6] = {};
    double xtalk_err[5][6] = {};


    for (int ifile = 0; ifile < filenames.size(); ifile++) {
	TFile* f = new TFile(filenames.at(ifile).c_str());
    	//TTree* t_parton = (TTree*)f->Get("truth");
    	TTree* t_part = (TTree*)f->Get("particleLevel");
    	TTree* t_reco;
    	if (!Upgrade) {
    	    t_reco = (TTree*)f->Get("nominal");
    	} else {
    	    t_reco = (TTree*)f->Get("upgrade");
    	}

    	if (t_part->BuildIndex("runNumber", "eventNumber") < 0){
    	    std::cerr << "Could not build particle level index!" << std::endl;
    	    std::abort();
    	}

    	//cout << "Total event at parton/particle/reco level: " << t_parton->GetEntriesFast() << " " << t_part->GetEntriesFast() << " " << t_reco->GetEntriesFast() << endl;
    	//int n_total = t_parton->GetEntriesFast();

    	RecoLevel RecoTree(t_reco, Upgrade);
    	int totentry = RecoTree.GetTotalEntry();
    	ParticleLevel TruthTree(t_part, Upgrade);
	if (Upgrade3) {
	    totentry = TruthTree.GetTotalEntry();
	}

    	if (NEvt != -1) totentry = NEvt;

    	for (int ientry = 0; ientry < totentry; ientry++) {
    	    if (ientry%10000 == 0) cout << "this is the " << ientry << " entry" << endl;
    	    RecoTree.GetEntry(ientry);
	    if (!Upgrade2) {
		TruthTree.GetEntry(ientry);
	    } else {
		Int_t nBytesRead = t_part->GetEntryWithIndex(RecoTree.runNumber, RecoTree.eventNumber);
		if ( nBytesRead < 0 ) {
    	    	    continue;
    	    	}
	    }

    	    if (debug) cout << "getting the 110 reco weights" << endl;
    	    //if (debug) cout << "total number of weight: " << RecoTree.mc_generator_weights->size() << endl;

    	    double weight_reco[110];
    	    if (Upgrade) {
    	        for (int i = 0; i < 110; i++) {
    	    	weight_reco[i] = RecoTree.weight_mc;
    	        }
    	    } else {
    	        if (RecoTree.selph_index1 < 0) {
    	            for (int i = 0; i < 110; i++) {
    	        	if (!PS && !TTBar &&!PSNew) {
    	        	    weight_reco[i] = RecoTree.mc_generator_weights->at(i) * RecoTree.weight_pileup * RecoTree.weight_leptonSF * RecoTree.weight_jvt * RecoTree.weight_bTagSF_Continuous;
    	        	} else {
    	        	    weight_reco[i] = RecoTree.weight_mc * RecoTree.weight_pileup * RecoTree.weight_leptonSF * RecoTree.weight_jvt * RecoTree.weight_bTagSF_Continuous;
    	        	}
    	            }
    	        } else {
    	            for (int i = 0; i < 110; i++) {
    	        	if (!PS && !TTBar && !PSNew) {
    	        	    weight_reco[i] = RecoTree.mc_generator_weights->at(i) * RecoTree.weight_pileup * RecoTree.weight_leptonSF * RecoTree.ph_SF_eff->at(RecoTree.selph_index1) * RecoTree.ph_SF_iso->at(RecoTree.selph_index1) * RecoTree.weight_jvt * RecoTree.weight_bTagSF_Continuous;
    	        	} else {
    	        	    weight_reco[i] = RecoTree.weight_mc * RecoTree.weight_pileup * RecoTree.weight_leptonSF * RecoTree.ph_SF_eff->at(RecoTree.selph_index1) * RecoTree.ph_SF_iso->at(RecoTree.selph_index1) * RecoTree.weight_jvt * RecoTree.weight_bTagSF_Continuous;
    	        	}
    	            }
    	        }
    	    }
    	        
    	    double weight_reco_nominal = weight_reco[0];

    	    if (debug) cout << "getting the 110 truth weights" << endl;

    	    double weight_truth[110];
    	    for (int i = 0; i < 110; i++) {
    	        if (Upgrade) {
    	    	weight_truth[i] = RecoTree.weight_mc;
    	        } else {
    	    	if (!PS && !TTBar && !PSNew) {
    	    	    weight_truth[i] = RecoTree.mc_generator_weights->at(i);
    	    	} else {
    	    	    weight_truth[i] = RecoTree.weight_mc;
    	    	}
    	        }
		if (Upgrade3) {
		    weight_truth[i] = TruthTree.weight_mc;
		}
    	    }

    	    double weight_truth_nominal = weight_truth[0];

    	    vector<TLorentzVector> for_upgrade_or;
    	    if (debug) cout << "lep prompt" << endl;
    	    vector<int> n_el_good_index_reco;
    	    vector<int> n_mu_good_index_reco;
    	    if (Upgrade) {
    	        if (debug) cout << "el prompt" << endl;
    	        for (int i = 0; i < RecoTree.el_pt->size(); i++) {
    	    	if (RecoTree.el_faketype->at(i) != 0) continue;
    	            bool isprompt = isPromptElectron(RecoTree.el_true_type->at(i),RecoTree.el_true_origin->at(i),RecoTree.el_true_type->at(i),RecoTree.el_true_origin->at(i),11,RecoTree.el_charge->at(i)).first;
    	            if (RecoTree.el_pt->at(i) >= 25000. && fabs(RecoTree.el_eta->at(i)) < 2.47 && (fabs(RecoTree.el_eta->at(i)) < 1.37 || fabs(RecoTree.el_eta->at(i)) > 1.52) && isprompt) {
    	                n_el_good_index_reco.push_back(i);
    			TLorentzVector tmp; tmp.SetPtEtaPhiE(RecoTree.el_pt->at(i),RecoTree.el_eta->at(i),RecoTree.el_phi->at(i),RecoTree.el_e->at(i));
			for_upgrade_or.push_back(tmp);			
    	            }
    	        }
    	        if (debug) cout << "mu prompt" << endl;
    	        for (int i = 0; i < RecoTree.mu_pt->size(); i++) {
    	            if (RecoTree.mu_pt->at(i) >= 25000. && fabs(RecoTree.mu_eta->at(i)) < 2.5 && RecoTree.mu_true_isPrompt->at(i)) {
    	                n_mu_good_index_reco.push_back(i);
			TLorentzVector tmp; tmp.SetPtEtaPhiE(RecoTree.mu_pt->at(i),RecoTree.mu_eta->at(i),RecoTree.mu_phi->at(i),RecoTree.mu_e->at(i));
			for_upgrade_or.push_back(tmp);			
    	            }
    	        }
    	    }
    	    int n_el_reco = n_el_good_index_reco.size(); 
    	    int n_mu_reco = n_mu_good_index_reco.size(); 


    	    if (debug) cout << "is ph_prompt" << endl;

    	    if (Upgrade) {
    	        RecoTree.selph_index1 = -1;
    	        RecoTree.event_ngoodphotons = 0;
    	        if (debug) cout << "ph loop" << endl;
    	        if (debug) cout << RecoTree.ph_pt->size() << endl;
    	        for (int i = 0; i < RecoTree.ph_pt->size(); i++) {
		    if (debug) cout << "ph faketype" << endl;
    	    	    if (RecoTree.ph_faketype->at(i) != 0) continue;
    	    	    if (debug) cout << "ph type/origin" << endl;
	    	    if ((RecoTree.ph_mc_Origin->at(i)==23 || RecoTree.ph_mc_Origin->at(i)==24 || RecoTree.ph_mc_Origin->at(i)==25 || RecoTree.ph_mc_Origin->at(i)==26 || RecoTree.ph_mc_Origin->at(i)==27 || RecoTree.ph_mc_Origin->at(i)==28 || RecoTree.ph_mc_Origin->at(i)==29 || RecoTree.ph_mc_Origin->at(i)==30 || RecoTree.ph_mc_Origin->at(i)==31 || RecoTree.ph_mc_Origin->at(i)==32 || RecoTree.ph_mc_Origin->at(i)==33 || RecoTree.ph_mc_Origin->at(i)==34 || RecoTree.ph_mc_Origin->at(i)==35 || RecoTree.ph_mc_Origin->at(i)==42) && RecoTree.ph_mc_Type->at(i) == 16) continue;
    	            if (!(RecoTree.ph_pt->at(i) >= 20000. && fabs(RecoTree.ph_eta->at(i)) < 2.37 && (fabs(RecoTree.ph_eta->at(i)) < 1.37 || fabs(RecoTree.ph_eta->at(i)) > 1.52))) continue;
    	    	    if (RecoTree.selph_index1 == -1) RecoTree.selph_index1 = i;
		    RecoTree.event_ngoodphotons++;
		    TLorentzVector tmp; tmp.SetPtEtaPhiE(RecoTree.ph_pt->at(i),RecoTree.ph_eta->at(i),RecoTree.ph_phi->at(i),RecoTree.ph_e->at(i));
		    for_upgrade_or.push_back(tmp);			
    	        }
    	    }


    	    if (debug) cout << "is ph_prompt2" << endl;
    	    bool ph_prompt = false;
    	    if (Upgrade) {
    	        if (RecoTree.selph_index1 >= 0) {
    	    	ph_prompt = !((RecoTree.ph_mc_Origin->at(RecoTree.selph_index1)==23 || RecoTree.ph_mc_Origin->at(RecoTree.selph_index1)==24 || RecoTree.ph_mc_Origin->at(RecoTree.selph_index1)==25 || RecoTree.ph_mc_Origin->at(RecoTree.selph_index1)==26 || RecoTree.ph_mc_Origin->at(RecoTree.selph_index1)==27 || RecoTree.ph_mc_Origin->at(RecoTree.selph_index1)==28 || RecoTree.ph_mc_Origin->at(RecoTree.selph_index1)==29 || RecoTree.ph_mc_Origin->at(RecoTree.selph_index1)==30 || RecoTree.ph_mc_Origin->at(RecoTree.selph_index1)==31 || RecoTree.ph_mc_Origin->at(RecoTree.selph_index1)==32 || RecoTree.ph_mc_Origin->at(RecoTree.selph_index1)==33 || RecoTree.ph_mc_Origin->at(RecoTree.selph_index1)==34 || RecoTree.ph_mc_Origin->at(RecoTree.selph_index1)==35 || RecoTree.ph_mc_Origin->at(RecoTree.selph_index1)==42) && RecoTree.ph_mc_Type->at(RecoTree.selph_index1) == 16);
    	        }
    	    } else {
    	        if (RecoTree.selph_index1 >= 0) {
    	            ph_prompt = !((RecoTree.ph_truthOrigin->at(RecoTree.selph_index1)==23 || RecoTree.ph_truthOrigin->at(RecoTree.selph_index1)==24 || RecoTree.ph_truthOrigin->at(RecoTree.selph_index1)==25 || RecoTree.ph_truthOrigin->at(RecoTree.selph_index1)==26 || RecoTree.ph_truthOrigin->at(RecoTree.selph_index1)==27 || RecoTree.ph_truthOrigin->at(RecoTree.selph_index1)==28 || RecoTree.ph_truthOrigin->at(RecoTree.selph_index1)==29 || RecoTree.ph_truthOrigin->at(RecoTree.selph_index1)==30 || RecoTree.ph_truthOrigin->at(RecoTree.selph_index1)==31 || RecoTree.ph_truthOrigin->at(RecoTree.selph_index1)==32 || RecoTree.ph_truthOrigin->at(RecoTree.selph_index1)==33 || RecoTree.ph_truthOrigin->at(RecoTree.selph_index1)==34 || RecoTree.ph_truthOrigin->at(RecoTree.selph_index1)==35 || RecoTree.ph_truthOrigin->at(RecoTree.selph_index1)==42) && RecoTree.ph_truthType->at(RecoTree.selph_index1) == 16 ) && !( abs(RecoTree.ph_mc_pid->at(RecoTree.selph_index1))==11 || ( RecoTree.ph_mcel_dr->at(RecoTree.selph_index1)< m_efake_dr && RecoTree.ph_mcel_dr->at(RecoTree.selph_index1)>=0 ));
    	        }
    	    }

    	    if (debug) cout << "reco lep/ph tlvs" << endl;
    	    TLorentzVector tlv_reco_ph, tlv_reco_el, tlv_reco_el2, tlv_reco_mu, tlv_reco_mu2;
    	    vector<TLorentzVector> for_upgrade_or2;
    	    double upgrade_mphlep = 0;
    	    double upgrade_drphlep = 999;
    	    double upgrade_mll = 0;
    	    double upgrade_mlly = 0;
    	    if (RecoTree.selph_index1 >= 0) {tlv_reco_ph.SetPtEtaPhiE(RecoTree.ph_pt->at(RecoTree.selph_index1),RecoTree.ph_eta->at(RecoTree.selph_index1),RecoTree.ph_phi->at(RecoTree.selph_index1),RecoTree.ph_e->at(RecoTree.selph_index1)); for_upgrade_or2.push_back(tlv_reco_ph);}
    	    if (Upgrade) {
    	        if (n_el_good_index_reco.size() >= 1) {tlv_reco_el.SetPtEtaPhiE(RecoTree.el_pt->at(n_el_good_index_reco.at(0)),RecoTree.el_eta->at(n_el_good_index_reco.at(0)),RecoTree.el_phi->at(n_el_good_index_reco.at(0)),RecoTree.el_e->at(n_el_good_index_reco.at(0))); for_upgrade_or2.push_back(tlv_reco_el); upgrade_mphlep = (tlv_reco_ph + tlv_reco_el).M(); if (upgrade_drphlep > tlv_reco_el.DeltaR(tlv_reco_ph)) upgrade_drphlep = tlv_reco_el.DeltaR(tlv_reco_ph);}
    	        if (n_el_good_index_reco.size() >= 2) {tlv_reco_el2.SetPtEtaPhiE(RecoTree.el_pt->at(n_el_good_index_reco.at(1)),RecoTree.el_eta->at(n_el_good_index_reco.at(1)),RecoTree.el_phi->at(n_el_good_index_reco.at(1)),RecoTree.el_e->at(n_el_good_index_reco.at(1))); for_upgrade_or2.push_back(tlv_reco_el2); if (upgrade_drphlep > tlv_reco_el2.DeltaR(tlv_reco_ph)) upgrade_drphlep = tlv_reco_el2.DeltaR(tlv_reco_ph); upgrade_mll = (tlv_reco_el+tlv_reco_el2).M(); upgrade_mlly = (tlv_reco_el+tlv_reco_el2+tlv_reco_ph).M();}
    	        if (n_mu_good_index_reco.size() >= 1) {tlv_reco_mu.SetPtEtaPhiE(RecoTree.mu_pt->at(n_mu_good_index_reco.at(0)),RecoTree.mu_eta->at(n_mu_good_index_reco.at(0)),RecoTree.mu_phi->at(n_mu_good_index_reco.at(0)),RecoTree.mu_e->at(n_mu_good_index_reco.at(0))); for_upgrade_or2.push_back(tlv_reco_mu); if (upgrade_drphlep > tlv_reco_mu.DeltaR(tlv_reco_ph)) upgrade_drphlep = tlv_reco_mu.DeltaR(tlv_reco_ph);}
    	        if (n_mu_good_index_reco.size() >= 2) {tlv_reco_mu2.SetPtEtaPhiE(RecoTree.mu_pt->at(n_mu_good_index_reco.at(1)),RecoTree.mu_eta->at(n_mu_good_index_reco.at(1)),RecoTree.mu_phi->at(n_mu_good_index_reco.at(1)),RecoTree.mu_e->at(n_mu_good_index_reco.at(1))); for_upgrade_or2.push_back(tlv_reco_mu2); if (upgrade_drphlep > tlv_reco_mu2.DeltaR(tlv_reco_ph)) upgrade_drphlep = tlv_reco_mu2.DeltaR(tlv_reco_ph); upgrade_mll = (tlv_reco_mu+tlv_reco_mu2).M(); upgrade_mlly = (tlv_reco_mu+tlv_reco_mu2+tlv_reco_ph).M();}
    	    } else {
    	        if (RecoTree.el_pt->size() >= 1) {tlv_reco_el.SetPtEtaPhiE(RecoTree.el_pt->at(0),RecoTree.el_eta->at(0),RecoTree.el_phi->at(0),RecoTree.el_e->at(0));}
    	        if (RecoTree.el_pt->size() >= 2) {tlv_reco_el2.SetPtEtaPhiE(RecoTree.el_pt->at(1),RecoTree.el_eta->at(1),RecoTree.el_phi->at(1),RecoTree.el_e->at(1)); }
    	        if (RecoTree.mu_pt->size() >= 1) {tlv_reco_mu.SetPtEtaPhiE(RecoTree.mu_pt->at(0),RecoTree.mu_eta->at(0),RecoTree.mu_phi->at(0),RecoTree.mu_e->at(0));}
    	        if (RecoTree.mu_pt->size() >= 2) {tlv_reco_mu2.SetPtEtaPhiE(RecoTree.mu_pt->at(1),RecoTree.mu_eta->at(1),RecoTree.mu_phi->at(1),RecoTree.mu_e->at(1));}
    	    }

    	    if (debug) cout << "reco ejets" << endl;
    	    int upgrade_njet = 0;
    	    int upgrade_nbjet = 0;
    	    if (Upgrade) {
    	        for (int i = 0; i < RecoTree.jet_pt->size(); i++) {
    	            if (!(RecoTree.jet_pt->at(i) > 25000 && fabs(RecoTree.jet_eta->at(i)) < 2.5)) continue;
    	        	TLorentzVector tmp_jet;
    	        	tmp_jet.SetPtEtaPhiE(RecoTree.jet_pt->at(i), RecoTree.jet_eta->at(i), RecoTree.jet_phi->at(i), RecoTree.jet_e->at(i));

    	        	bool overlaped = false;
    	        	for (int j = 0; j < for_upgrade_or.size(); j++) {
    	        	    if (tmp_jet.DeltaR(for_upgrade_or.at(j)) < 0.4) {
    	        		overlaped = true;
    	        		break;
    	        	    }
    	        	}
    	        	if (overlaped) continue;

    	        	upgrade_njet++;
    	        	if (RecoTree.jet_mv1eff->at(i) > m_random->Rndm()) {
    	        	    upgrade_nbjet++;
    	        	}
    	        }
    	    }

    	    if (debug) cout << "reco selection" << endl;

    	    int icf_reco = 0;
    	    for (int i = 0; i < 110; i++) {cutflow[i][0][0][icf_reco] += weight_reco[i];cutflow_err[i][0][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    if (Upgrade ? RecoTree.ejets_gamma_basic && n_el_reco == 1 && n_mu_reco == 0: (RecoTree.ejets_2015 || RecoTree.ejets_2016)) {
    	        for (int i = 0; i < 110; i++) {cutflow[i][0][0][icf_reco] += weight_reco[i];cutflow_err[i][0][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    	if (RecoTree.event_ngoodphotons == 1) {
    	    	    for (int i = 0; i < 110; i++) {cutflow[i][0][0][icf_reco] += weight_reco[i];cutflow_err[i][0][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    	    if (Upgrade ? 1 : RecoTree.ph_isoFCT->at(RecoTree.selph_index1)) {
    	    		for (int i = 0; i < 110; i++) {cutflow[i][0][0][icf_reco] += weight_reco[i];cutflow_err[i][0][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    		if (ph_prompt) {
    	    		    for (int i = 0; i < 110; i++) {cutflow[i][0][0][icf_reco] += weight_reco[i];cutflow_err[i][0][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	        if (Upgrade ? upgrade_njet >=4 : RecoTree.jet_pt->size() >= 4) {
    	    	for (int i = 0; i < 110; i++) {cutflow[i][0][0][icf_reco] += weight_reco[i];cutflow_err[i][0][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    		    if (Upgrade ? upgrade_nbjet >= 1 : RecoTree.event_nbjets77 >= 1) {
    	    			for (int i = 0; i < 110; i++) {cutflow[i][0][0][icf_reco] += weight_reco[i];cutflow_err[i][0][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    			if (Upgrade ? fabs(upgrade_mphlep-m_Z) > 5000. : fabs(RecoTree.ph_mgammalept->at(RecoTree.selph_index1) - m_Z) > 5000.) {
    	    			    for (int i = 0; i < 110; i++) {cutflow[i][0][0][icf_reco] += weight_reco[i];cutflow_err[i][0][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    			    if (Upgrade ? upgrade_drphlep > 1.0 : RecoTree.ph_drlept->at(RecoTree.selph_index1) > 1.0) {
    	    				for (int i = 0; i < 110; i++) {cutflow[i][0][0][icf_reco] += weight_reco[i];cutflow_err[i][0][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    			    }
    	    			}
    	    		    }
    	    		}
    	    	    }
    	    	}
    	        }
    	    }

    	    if (debug) cout << "reco mujets" << endl;

    	    icf_reco = 0;
    	    for (int i = 0; i < 110; i++) {cutflow[i][1][0][icf_reco] += weight_reco[i];cutflow_err[i][1][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    if (Upgrade ? RecoTree.mujets_gamma_basic && n_el_reco == 0 && n_mu_reco == 1: ((RecoTree.mujets_2015 || RecoTree.mujets_2016) && RecoTree.mu_pt->at(0) > 27500.)) {
    	        for (int i = 0; i < 110; i++) {cutflow[i][1][0][icf_reco] += weight_reco[i];cutflow_err[i][1][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	        if (Upgrade ? upgrade_njet >= 4 : RecoTree.jet_pt->size() >= 4) {
    	    	for (int i = 0; i < 110; i++) {cutflow[i][1][0][icf_reco] += weight_reco[i];cutflow_err[i][1][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    	if (RecoTree.event_ngoodphotons == 1) {
    	    	    for (int i = 0; i < 110; i++) {cutflow[i][1][0][icf_reco] += weight_reco[i];cutflow_err[i][1][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    	    if (Upgrade ? 1 : RecoTree.ph_isoFCT->at(RecoTree.selph_index1)) {
    	    		for (int i = 0; i < 110; i++) {cutflow[i][1][0][icf_reco] += weight_reco[i];cutflow_err[i][1][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    		if (ph_prompt) {
    	    		    for (int i = 0; i < 110; i++) {cutflow[i][1][0][icf_reco] += weight_reco[i];cutflow_err[i][1][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    		    if (Upgrade ? upgrade_nbjet >= 1 : RecoTree.event_nbjets77 >= 1) {
    	    			for (int i = 0; i < 110; i++) {cutflow[i][1][0][icf_reco] += weight_reco[i];cutflow_err[i][1][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    			for (int i = 0; i < 110; i++) {cutflow[i][1][0][icf_reco] += weight_reco[i];cutflow_err[i][1][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    			if (Upgrade ? upgrade_drphlep > 1.0 : RecoTree.ph_drlept->at(RecoTree.selph_index1) > 1.0) {
    	    			    for (int i = 0; i < 110; i++) {cutflow[i][1][0][icf_reco] += weight_reco[i];cutflow_err[i][1][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    			}
    	    		    }
    	    		}
    	    	    }
    	    	}
    	        }
    	    }

    	    if (debug) cout << "reco ee" << endl;

    	    icf_reco = 0;
    	    for (int i = 0; i < 110; i++) {cutflow[i][2][0][icf_reco] += weight_reco[i];cutflow_err[i][2][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    if (Upgrade ? RecoTree.ee_gamma_basic && n_el_reco == 2 && n_mu_reco == 0: (RecoTree.ee_2015 || RecoTree.ee_2016)) {
    	        for (int i = 0; i < 110; i++) {cutflow[i][2][0][icf_reco] += weight_reco[i];cutflow_err[i][2][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	        if (Upgrade ? upgrade_njet >= 2 : RecoTree.jet_pt->size() >= 2) {
    	    	for (int i = 0; i < 110; i++) {cutflow[i][2][0][icf_reco] += weight_reco[i];cutflow_err[i][2][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    	if (RecoTree.event_ngoodphotons == 1) {
    	    	    for (int i = 0; i < 110; i++) {cutflow[i][2][0][icf_reco] += weight_reco[i];cutflow_err[i][2][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    	    if (Upgrade ? 1 : RecoTree.ph_isoFCT->at(RecoTree.selph_index1)) {
    	    		for (int i = 0; i < 110; i++) {cutflow[i][2][0][icf_reco] += weight_reco[i];cutflow_err[i][2][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    		if (ph_prompt) {
    	    		    for (int i = 0; i < 110; i++) {cutflow[i][2][0][icf_reco] += weight_reco[i];cutflow_err[i][2][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    		    if (Upgrade ? upgrade_nbjet >= 1: RecoTree.event_nbjets77 >= 1) {
    	    			for (int i = 0; i < 110; i++) {cutflow[i][2][0][icf_reco] += weight_reco[i];cutflow_err[i][2][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    			if (RecoTree.met_met >= 30000.) {
    	    			    for (int i = 0; i < 110; i++) {cutflow[i][2][0][icf_reco] += weight_reco[i];cutflow_err[i][2][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    			    if (Upgrade ? (upgrade_mll>95000.||upgrade_mll<85000.) : (RecoTree.event_mll>95000. || RecoTree.event_mll < 85000.)) {
    	    				for (int i = 0; i < 110; i++) {cutflow[i][2][0][icf_reco] += weight_reco[i];cutflow_err[i][2][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    				if (Upgrade ? (upgrade_mlly>95000. || upgrade_mlly<85000.) : (RecoTree.ph_mgammalept->at(RecoTree.selph_index1) > 95000. || RecoTree.ph_mgammalept->at(RecoTree.selph_index1) < 85000.)) {
    	    				    for (int i = 0; i < 110; i++) {cutflow[i][2][0][icf_reco] += weight_reco[i];cutflow_err[i][2][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    		    	    	    if (Upgrade ? upgrade_drphlep > 1.0 : RecoTree.ph_drlept->at(RecoTree.selph_index1) > 1.0) {
    	    					for (int i = 0; i < 110; i++) {cutflow[i][2][0][icf_reco] += weight_reco[i];cutflow_err[i][2][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    		    	    	    }
    	    		    	    	}
    	    			    }
    	    			}
    	    		    }
    	    		}
    	    	    }
    	    	}
    	        }
    	    }

    	    if (debug) cout << "reco emu" << endl;

    	    icf_reco = 0;
    	    for (int i = 0; i < 110; i++) {cutflow[i][3][0][icf_reco] += weight_reco[i];cutflow_err[i][3][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    if (Upgrade ? RecoTree.emu_gamma_basic && n_el_reco == 1 && n_mu_reco == 1: ((RecoTree.emu_2015 || RecoTree.emu_2016) && ((RecoTree.el_pt->at(0) < RecoTree.mu_pt->at(0) && RecoTree.mu_pt->at(0) > 27500) || (RecoTree.el_pt->at(0) > RecoTree.mu_pt->at(0))))) {
    	        for (int i = 0; i < 110; i++) {cutflow[i][3][0][icf_reco] += weight_reco[i];cutflow_err[i][3][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	        if (Upgrade ? upgrade_njet >= 2 : RecoTree.jet_pt->size() >= 2) {
    	    	for (int i = 0; i < 110; i++) {cutflow[i][3][0][icf_reco] += weight_reco[i];cutflow_err[i][3][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    	if (RecoTree.event_ngoodphotons == 1) {
    	    	    for (int i = 0; i < 110; i++) {cutflow[i][3][0][icf_reco] += weight_reco[i];cutflow_err[i][3][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    	    if (Upgrade ? 1 : RecoTree.ph_isoFCT->at(RecoTree.selph_index1)) {
    	    		for (int i = 0; i < 110; i++) {cutflow[i][3][0][icf_reco] += weight_reco[i];cutflow_err[i][3][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    		if (ph_prompt) {
    	    		    for (int i = 0; i < 110; i++) {cutflow[i][3][0][icf_reco] += weight_reco[i];cutflow_err[i][3][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    		    if (Upgrade ? upgrade_nbjet >= 1 : RecoTree.event_nbjets77 >= 1) {
    	    			for (int i = 0; i < 110; i++) {cutflow[i][3][0][icf_reco] += weight_reco[i];cutflow_err[i][3][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    			for (int i = 0; i < 110; i++) {cutflow[i][3][0][icf_reco] += weight_reco[i];cutflow_err[i][3][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    			for (int i = 0; i < 110; i++) {cutflow[i][3][0][icf_reco] += weight_reco[i];cutflow_err[i][3][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    			for (int i = 0; i < 110; i++) {cutflow[i][3][0][icf_reco] += weight_reco[i];cutflow_err[i][3][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    		    	if (Upgrade ? upgrade_drphlep > 1.0 : RecoTree.ph_drlept->at(RecoTree.selph_index1) > 1.0) {
    	    			    for (int i = 0; i < 110; i++) {cutflow[i][3][0][icf_reco] += weight_reco[i];cutflow_err[i][3][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    		    	}
    	    		    }
    	    		}
    	    	    }
    	    	}
    	        }
    	    }

    	    if (debug) cout << "reco mumu" << endl;

    	    icf_reco = 0;
    	    for (int i = 0; i < 110; i++) {cutflow[i][4][0][icf_reco] += weight_reco[i];cutflow_err[i][4][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    if (Upgrade ? RecoTree.mumu_gamma_basic && n_mu_reco == 2 && n_el_reco == 0: ((RecoTree.mumu_2015 || RecoTree.mumu_2016) && (RecoTree.mu_pt->at(0) > 27500 && RecoTree.mu_pt->at(1) > 27500))) {
    	    for (int i = 0; i < 110; i++) {cutflow[i][4][0][icf_reco] += weight_reco[i];cutflow_err[i][4][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	        if (Upgrade ? upgrade_njet >= 2 : RecoTree.jet_pt->size() >= 2) {
    	        for (int i = 0; i < 110; i++) {cutflow[i][4][0][icf_reco] += weight_reco[i];cutflow_err[i][4][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    	if (RecoTree.event_ngoodphotons == 1) {
    	    	for (int i = 0; i < 110; i++) {cutflow[i][4][0][icf_reco] += weight_reco[i];cutflow_err[i][4][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    	    if (Upgrade ? 1 : RecoTree.ph_isoFCT->at(RecoTree.selph_index1)) {
    	    		for (int i = 0; i < 110; i++) {cutflow[i][4][0][icf_reco] += weight_reco[i];cutflow_err[i][4][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    		if (ph_prompt) {
    	    		    for (int i = 0; i < 110; i++) {cutflow[i][4][0][icf_reco] += weight_reco[i];cutflow_err[i][4][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    		    if (Upgrade ? upgrade_nbjet >= 1 : RecoTree.event_nbjets77 >= 1) {
    	    			for (int i = 0; i < 110; i++) {cutflow[i][4][0][icf_reco] += weight_reco[i];cutflow_err[i][4][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    			if (RecoTree.met_met >= 30000.) {
    	    			    for (int i = 0; i < 110; i++) {cutflow[i][4][0][icf_reco] += weight_reco[i];cutflow_err[i][4][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    			    if (Upgrade ? (upgrade_mll>95000.||upgrade_mll<85000.) : (RecoTree.event_mll>95000. || RecoTree.event_mll < 85000.)) {
    	    				for (int i = 0; i < 110; i++) {cutflow[i][4][0][icf_reco] += weight_reco[i];cutflow_err[i][4][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    				if (Upgrade ? (upgrade_mlly>95000. || upgrade_mlly<85000.) : (RecoTree.ph_mgammalept->at(RecoTree.selph_index1) > 95000. || RecoTree.ph_mgammalept->at(RecoTree.selph_index1) < 85000.)) {
    	    				    for (int i = 0; i < 110; i++) {cutflow[i][4][0][icf_reco] += weight_reco[i];cutflow_err[i][4][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    		    	    	    if (Upgrade ? upgrade_drphlep > 1.0 : RecoTree.ph_drlept->at(RecoTree.selph_index1) > 1.0) {
    	    					for (int i = 0; i < 110; i++) {cutflow[i][4][0][icf_reco] += weight_reco[i];cutflow_err[i][4][0][icf_reco] += pow(weight_reco[i],2);} icf_reco ++;
    	    		    	    	    }
    	    		    	    	}
    	    			    }
    	    			}
    	    		    }
    	    		}
    	    	    }
    	    	}
    	        }
    	    }

    	    if (debug) cout << "reco flags" << endl;

    	    bool passreco_ej, passreco_mj, passreco_ee, passreco_mumu, passreco_emu;
    	    if (!Upgrade) {
    	        passreco_ej = (RecoTree.ejets_2015 || RecoTree.ejets_2016) && (RecoTree.event_njets >= 4) && (RecoTree.event_ngoodphotons == 1) && (RecoTree.ph_isoFCT->at(RecoTree.selph_index1)) && ph_prompt && (RecoTree.event_nbjets77 >= 1) && (fabs(RecoTree.ph_mgammalept->at(RecoTree.selph_index1) - m_Z) > 5000.) && (RecoTree.ph_drlept->at(RecoTree.selph_index1) > 1.0);
    	        passreco_mj = ((RecoTree.mujets_2015 || RecoTree.mujets_2016) && RecoTree.mu_pt->at(0) > 27500.) && (RecoTree.event_njets >= 4) && (RecoTree.event_ngoodphotons == 1) && (RecoTree.ph_isoFCT->at(RecoTree.selph_index1)) && ph_prompt && (RecoTree.event_nbjets77 >= 1) && (RecoTree.ph_drlept->at(RecoTree.selph_index1) > 1.0);
    	        passreco_ee = (RecoTree.ee_2015 || RecoTree.ee_2016) && (RecoTree.event_njets >= 2) && (RecoTree.event_ngoodphotons == 1) && (RecoTree.ph_isoFCT->at(RecoTree.selph_index1)) && ph_prompt && (RecoTree.event_nbjets77 >= 1) && (RecoTree.met_met >= 30000.) && (RecoTree.event_mll > 95000. || RecoTree.event_mll < 85000.) && (RecoTree.ph_mgammalept->at(RecoTree.selph_index1) > 95000. || RecoTree.ph_mgammalept->at(RecoTree.selph_index1) < 85000.) && (RecoTree.ph_drlept->at(RecoTree.selph_index1) > 1.0);
    	        passreco_mumu = ((RecoTree.mumu_2015 || RecoTree.mumu_2016) && (RecoTree.mu_pt->at(0) > 27500 && RecoTree.mu_pt->at(1) > 27500)) && (RecoTree.event_njets >= 2) && (RecoTree.event_ngoodphotons == 1) && (RecoTree.ph_isoFCT->at(RecoTree.selph_index1)) && ph_prompt && (RecoTree.event_nbjets77 >= 1) && (RecoTree.met_met >= 30000.) && (RecoTree.event_mll > 95000. || RecoTree.event_mll < 85000.) && (RecoTree.ph_mgammalept->at(RecoTree.selph_index1) > 95000. || RecoTree.ph_mgammalept->at(RecoTree.selph_index1) < 85000.) && (RecoTree.ph_drlept->at(RecoTree.selph_index1) > 1.0);
    	        passreco_emu = ((RecoTree.emu_2015 || RecoTree.emu_2016) && ((RecoTree.el_pt->at(0) < RecoTree.mu_pt->at(0) && RecoTree.mu_pt->at(0) > 27500) || (RecoTree.el_pt->at(0) > RecoTree.mu_pt->at(0)))) && (RecoTree.event_njets >= 2) && (RecoTree.event_ngoodphotons == 1) && (RecoTree.ph_isoFCT->at(RecoTree.selph_index1)) && ph_prompt && (RecoTree.event_nbjets77 >= 1) && (RecoTree.ph_drlept->at(RecoTree.selph_index1) > 1.0);
    	    } else {
    	        passreco_ej = RecoTree.ejets_gamma_basic && n_el_reco == 1 && n_mu_reco == 0 && upgrade_njet >= 4 && RecoTree.event_ngoodphotons == 1 && ph_prompt && upgrade_nbjet >= 1 && fabs(upgrade_mphlep - m_Z) > 5000. && upgrade_drphlep > 1.0;
    	        passreco_mj = RecoTree.mujets_gamma_basic && n_el_reco == 0 && n_mu_reco == 1 && upgrade_njet >= 4 && RecoTree.event_ngoodphotons == 1 && ph_prompt && upgrade_nbjet >= 1 && upgrade_drphlep > 1.0;
    	        passreco_ee = RecoTree.ee_gamma_basic && n_el_reco == 2 && n_mu_reco == 0 && upgrade_njet >= 2 && RecoTree.event_ngoodphotons == 1 && ph_prompt && upgrade_nbjet >= 1 && RecoTree.met_met >= 30000. && (upgrade_mll > 95000. || upgrade_mll < 85000.) && (upgrade_mlly > 95000. || upgrade_mlly < 85000.) && upgrade_drphlep > 1.0;
    	        passreco_mumu = RecoTree.mumu_gamma_basic && n_el_reco == 0 && n_mu_reco == 2 && upgrade_njet >= 2 && RecoTree.event_ngoodphotons == 1 && ph_prompt && upgrade_nbjet >= 1 && (RecoTree.met_met >= 30000.) && (upgrade_mll > 95000. || upgrade_mll < 85000.) && (upgrade_mlly > 95000. || upgrade_mlly < 85000.) && upgrade_drphlep > 1.0;
    	        passreco_emu = RecoTree.emu_gamma_basic && n_el_reco == 1 && n_mu_reco == 1 && upgrade_njet >= 2 && RecoTree.event_ngoodphotons == 1 && ph_prompt && upgrade_nbjet >= 1 && upgrade_drphlep > 1.0;
    	    }

    	    if (debug) cout << "truth photon selection" << endl;

    	    int n_ph = 0;
    	    int ph1_idx = -1;
    	    for (int i = 0; i < TruthTree.ph_pt->size(); i++) {
    	        if (!(TruthTree.ph_pt->at(i) > 20000.)) continue;
    	        if (!(fabs(TruthTree.ph_eta->at(i)) < 2.37)) continue;
    	        n_ph++;
    	        if (ph1_idx == -1) ph1_idx = i;
    	    }

    	    vector<int> n_el_good_index_true;
    	    vector<int> n_mu_good_index_true;
    	    if (Upgrade) {
    	        for (int i = 0; i < TruthTree.el_pt->size(); i++) {
    	            if (TruthTree.el_pt->at(i) >= 25000. && fabs(TruthTree.el_eta->at(i)) < 2.5) { 
    	                n_el_good_index_true.push_back(i);
    	            }
    	        }
    	        for (int i = 0; i < TruthTree.mu_pt->size(); i++) {
    	            if (TruthTree.mu_pt->at(i) >= 25000. && fabs(TruthTree.mu_eta->at(i)) < 2.5) {
    	                n_mu_good_index_true.push_back(i);
    	            }
    	        }
    	    }
    	    int n_el_true = n_el_good_index_true.size(); 
    	    int n_mu_true = n_mu_good_index_true.size(); 

    	    if (debug) cout << "dR variables" << endl;

    	    TLorentzVector tlv_truth_ph, tlv_truth_el, tlv_truth_el2, tlv_truth_mu, tlv_truth_mu2;
    	    if (ph1_idx != -1) tlv_truth_ph.SetPtEtaPhiE(TruthTree.ph_pt->at(ph1_idx),TruthTree.ph_eta->at(ph1_idx),TruthTree.ph_phi->at(ph1_idx),TruthTree.ph_e->at(ph1_idx));
    	    if (Upgrade) {
    	        if (n_el_true >= 1) tlv_truth_el.SetPtEtaPhiE(TruthTree.el_pt->at(n_el_good_index_true.at(0)),TruthTree.el_eta->at(n_el_good_index_true.at(0)),TruthTree.el_phi->at(n_el_good_index_true.at(0)),TruthTree.el_e->at(n_el_good_index_true.at(0)));
    	        if (n_el_true >= 2) tlv_truth_el2.SetPtEtaPhiE(TruthTree.el_pt->at(n_el_good_index_true.at(1)),TruthTree.el_eta->at(n_el_good_index_true.at(1)),TruthTree.el_phi->at(n_el_good_index_true.at(1)),TruthTree.el_e->at(n_el_good_index_true.at(1)));
    	        if (n_mu_true >= 1) tlv_truth_mu.SetPtEtaPhiE(TruthTree.mu_pt->at(n_mu_good_index_true.at(0)),TruthTree.mu_eta->at(n_mu_good_index_true.at(0)),TruthTree.mu_phi->at(n_mu_good_index_true.at(0)),TruthTree.mu_e->at(n_mu_good_index_true.at(0)));
    	        if (n_mu_true >= 2) tlv_truth_mu2.SetPtEtaPhiE(TruthTree.mu_pt->at(n_mu_good_index_true.at(1)),TruthTree.mu_eta->at(n_mu_good_index_true.at(1)),TruthTree.mu_phi->at(n_mu_good_index_true.at(1)),TruthTree.mu_e->at(n_mu_good_index_true.at(1)));
    	    } else {
    	        if (TruthTree.el_pt->size() >= 1) tlv_truth_el.SetPtEtaPhiE(TruthTree.el_pt->at(0),TruthTree.el_eta->at(0),TruthTree.el_phi->at(0),TruthTree.el_e->at(0));
    	        if (TruthTree.el_pt->size() >= 2) tlv_truth_el2.SetPtEtaPhiE(TruthTree.el_pt->at(1),TruthTree.el_eta->at(1),TruthTree.el_phi->at(1),TruthTree.el_e->at(1));
    	        if (TruthTree.mu_pt->size() >= 1) tlv_truth_mu.SetPtEtaPhiE(TruthTree.mu_pt->at(0),TruthTree.mu_eta->at(0),TruthTree.mu_phi->at(0),TruthTree.mu_e->at(0));
    	        if (TruthTree.mu_pt->size() >= 2) tlv_truth_mu2.SetPtEtaPhiE(TruthTree.mu_pt->at(1),TruthTree.mu_eta->at(1),TruthTree.mu_phi->at(1),TruthTree.mu_e->at(1));
    	    }
    	    double dr_truth_phel = tlv_truth_ph.DeltaR(tlv_truth_el);
    	    double dr_truth_phel2 = tlv_truth_ph.DeltaR(tlv_truth_el2);
    	    double dr_truth_phmu = tlv_truth_ph.DeltaR(tlv_truth_mu);
    	    double dr_truth_phmu2 = tlv_truth_ph.DeltaR(tlv_truth_mu2);

    	    if (debug) {
    	        if (ph1_idx != -1 && RecoTree.selph_index1 != -1) {
    	            cout << "reco ph: " << tlv_reco_ph.Pt() << " " << tlv_reco_ph.Eta() << " " << tlv_reco_ph.Phi() << endl;
    	            cout << "truth ph: " << tlv_truth_ph.Pt() << " " << tlv_truth_ph.Eta() << " " << tlv_truth_ph.Phi() << endl;
    	            cout << "dR: " << tlv_reco_ph.DeltaR(tlv_truth_ph) << endl;
    	        }
    	    }

    	    if (debug) cout << "truth jet selection" << endl;

    	    int n_jet = 0;
    	    int n_bjet = 0;
    	    for (int i = 0; i < TruthTree.jet_pt->size(); i++) {
    	        if (!(TruthTree.jet_pt->at(i) > 25000.)) continue;
    	        if (!(fabs(TruthTree.jet_eta->at(i)) < 2.5)) continue;
    	        n_jet++;
    	        if (TruthTree.jet_nGhosts_bHadron->at(i)) {
    	    	n_bjet++;
    	        }
    	    }

    	    if (debug) cout << "truth ejets" << endl;

    	    int icf_truth = 0;
    	    for (int i = 0; i < 110; i++) {cutflow[i][0][1][icf_truth] += weight_truth[i];cutflow_err[i][0][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	    if (Upgrade ? TruthTree.ejets_gamma_basic && n_el_true == 1 && n_mu_true == 0: (TruthTree.ejets_2015_pl || TruthTree.ejets_2016_pl)) {
    	        for (int i = 0; i < 110; i++) {cutflow[i][0][1][icf_truth] += weight_truth[i];cutflow_err[i][0][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	        if (n_jet >= 4) {
    	    	for (int i = 0; i < 110; i++) {cutflow[i][0][1][icf_truth] += weight_truth[i];cutflow_err[i][0][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	            if (n_bjet >= 1) {
    	    	    for (int i = 0; i < 110; i++) {cutflow[i][0][1][icf_truth] += weight_truth[i];cutflow_err[i][0][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	    	   if (n_ph == 1) {
    	    		for (int i = 0; i < 110; i++) {cutflow[i][0][1][icf_truth] += weight_truth[i];cutflow_err[i][0][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	        	       if (1) {
    	    		    for (int i = 0; i < 110; i++) {cutflow[i][0][1][icf_truth] += weight_truth[i];cutflow_err[i][0][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	    		    if (dr_truth_phel > 1.0) {
    	    			for (int i = 0; i < 110; i++) {cutflow[i][0][1][icf_truth] += weight_truth[i];cutflow_err[i][0][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	    		    }   
    	        	       }
    	        	   }
    	    	}
    	        }
    	    }
    	    
    	    if (debug) cout << "truth mujets" << endl;

    	    icf_truth = 0;
    	    for (int i = 0; i < 110; i++) {cutflow[i][1][1][icf_truth] += weight_truth[i];cutflow_err[i][1][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	    if (Upgrade ? TruthTree.mujets_gamma_basic && n_el_true == 0 && n_mu_true == 1:  (TruthTree.mujets_2015_pl || TruthTree.mujets_2016_pl)) {
    	        for (int i = 0; i < 110; i++) {cutflow[i][1][1][icf_truth] += weight_truth[i];cutflow_err[i][1][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	        if (n_jet >= 4) {
    	    	for (int i = 0; i < 110; i++) {cutflow[i][1][1][icf_truth] += weight_truth[i];cutflow_err[i][1][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	            if (n_bjet >= 1) {
    	    	    for (int i = 0; i < 110; i++) {cutflow[i][1][1][icf_truth] += weight_truth[i];cutflow_err[i][1][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	        	   if (n_ph == 1) {
    	    		for (int i = 0; i < 110; i++) {cutflow[i][1][1][icf_truth] += weight_truth[i];cutflow_err[i][1][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	    		if (1) {
    	    		    for (int i = 0; i < 110; i++) {cutflow[i][1][1][icf_truth] += weight_truth[i];cutflow_err[i][1][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	    		    if (dr_truth_phmu > 1.0) {
    	    			for (int i = 0; i < 110; i++) {cutflow[i][1][1][icf_truth] += weight_truth[i];cutflow_err[i][1][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	    		    }
    	    		}
    	        	   }
    	            }
    	        }
    	    }

    	    if (debug) cout << "truth ee" << endl;

    	    icf_truth = 0;
    	    for (int i = 0; i < 110; i++) {cutflow[i][2][1][icf_truth] += weight_truth[i];cutflow_err[i][2][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	    if (Upgrade ? TruthTree.ee_gamma_basic && n_el_true == 2 && n_mu_true == 0: (TruthTree.ee_2015_pl || TruthTree.ee_2016_pl)) {
    	        for (int i = 0; i < 110; i++) {cutflow[i][2][1][icf_truth] += weight_truth[i];cutflow_err[i][2][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	        if (n_jet >= 2) {
    	    	for (int i = 0; i < 110; i++) {cutflow[i][2][1][icf_truth] += weight_truth[i];cutflow_err[i][2][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	            if (n_bjet >= 1) {
    	    	    for (int i = 0; i < 110; i++) {cutflow[i][2][1][icf_truth] += weight_truth[i];cutflow_err[i][2][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	    	    if (n_ph == 1) {
    	    		for (int i = 0; i < 110; i++) {cutflow[i][2][1][icf_truth] += weight_truth[i];cutflow_err[i][2][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	    		if (1) {
    	    		    for (int i = 0; i < 110; i++) {cutflow[i][2][1][icf_truth] += weight_truth[i];cutflow_err[i][2][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	    		    if (dr_truth_phel > 1.0 && dr_truth_phel2 > 1.0) {
    	    			for (int i = 0; i < 110; i++) {cutflow[i][2][1][icf_truth] += weight_truth[i];cutflow_err[i][2][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	    		    }
    	    		}
    	    	    }
    	        	}
    	        }
    	    }

    	    if (debug) cout << "truth emu" << endl;

    	    icf_truth = 0;
    	    for (int i = 0; i < 110; i++) {cutflow[i][3][1][icf_truth] += weight_truth[i];cutflow_err[i][3][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	    if (Upgrade ? TruthTree.emu_gamma_basic && n_el_true == 1 && n_mu_true == 1 : (TruthTree.emu_2015_pl || TruthTree.emu_2016_pl)) {
    	        for (int i = 0; i < 110; i++) {cutflow[i][3][1][icf_truth] += weight_truth[i];cutflow_err[i][3][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	        if (n_jet >= 2) {
    	    	for (int i = 0; i < 110; i++) {cutflow[i][3][1][icf_truth] += weight_truth[i];cutflow_err[i][3][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	            if (n_bjet >= 1) {
    	    	    for (int i = 0; i < 110; i++) {cutflow[i][3][1][icf_truth] += weight_truth[i];cutflow_err[i][3][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	    	    if (n_ph == 1) {
    	    		for (int i = 0; i < 110; i++) {cutflow[i][3][1][icf_truth] += weight_truth[i];cutflow_err[i][3][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	    		if (1) {
    	    		    for (int i = 0; i < 110; i++) {cutflow[i][3][1][icf_truth] += weight_truth[i];cutflow_err[i][3][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	    		    if (dr_truth_phel > 1.0 && dr_truth_phmu > 1.0) {
    	    			for (int i = 0; i < 110; i++) {cutflow[i][3][1][icf_truth] += weight_truth[i];cutflow_err[i][3][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	    		    }
    	        	       	}
    	        	   }
    	            }
    	        }
    	    }

    	    if (debug) cout << "truth mumu" << endl;

    	    icf_truth = 0;
    	    for (int i = 0; i < 110; i++) {cutflow[i][4][1][icf_truth] += weight_truth[i];cutflow_err[i][4][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	    if (Upgrade ? TruthTree.mumu_gamma_basic && n_mu_true == 2 && n_el_true == 0: (TruthTree.mumu_2015_pl || TruthTree.mumu_2016_pl)) {
    	        for (int i = 0; i < 110; i++) {cutflow[i][4][1][icf_truth] += weight_truth[i];cutflow_err[i][4][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	        if (n_jet >= 2) {
    	    	for (int i = 0; i < 110; i++) {cutflow[i][4][1][icf_truth] += weight_truth[i];cutflow_err[i][4][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	            if (n_bjet >= 1) {
    	    	    for (int i = 0; i < 110; i++) {cutflow[i][4][1][icf_truth] += weight_truth[i];cutflow_err[i][4][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	    	   if (n_ph == 1) {
    	    		for (int i = 0; i < 110; i++) {cutflow[i][4][1][icf_truth] += weight_truth[i];cutflow_err[i][4][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	    		if (1) {
    	    		    for (int i = 0; i < 110; i++) {cutflow[i][4][1][icf_truth] += weight_truth[i];cutflow_err[i][4][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	    		    if (dr_truth_phmu > 1.0 && dr_truth_phmu2 > 1.0) {
    	    			for (int i = 0; i < 110; i++) {cutflow[i][4][1][icf_truth] += weight_truth[i];cutflow_err[i][4][1][icf_reco] += pow(weight_truth[i],2);} icf_truth ++;
    	    		    }
    	    	       }
    	        	   }
    	            }
    	        }
    	    }

    	    if (debug) cout << "truth flags" << endl;

    	    bool passtruth_ej, passtruth_mj, passtruth_ee, passtruth_mumu, passtruth_emu;
    	    if (!Upgrade) {
    	        passtruth_ej = (TruthTree.ejets_2015_pl || TruthTree.ejets_2016_pl) && (n_jet >= 4) && (n_bjet >= 1) && (n_ph == 1) && (dr_truth_phel > 1.0);
    	        passtruth_mj = (TruthTree.mujets_2015_pl || TruthTree.mujets_2015_pl) && (n_jet >= 4) && (n_bjet >= 1) && (n_ph == 1) && (dr_truth_phmu > 1.0);
    	        passtruth_ee = (TruthTree.ee_2015_pl || TruthTree.ee_2015_pl) && (n_jet >= 2) && (n_bjet >= 1) && (n_ph == 1) && (dr_truth_phel > 1.0) && (dr_truth_phel2 > 1.0);
    	        passtruth_mumu = (TruthTree.mumu_2015_pl || TruthTree.mumu_2015_pl) && (n_jet >= 2) && (n_bjet >= 1) && (n_ph == 1) && (dr_truth_phmu > 1.0) && (dr_truth_phmu2 > 1.0);
    	        passtruth_emu = (TruthTree.emu_2015_pl || TruthTree.emu_2015_pl) && (n_jet >= 2) && (n_bjet >= 1) && (n_ph == 1) && (dr_truth_phel > 1.0) && (dr_truth_phmu > 1.0);
    	    } else {
    	        passtruth_ej = TruthTree.ejets_gamma_basic && n_el_true == 1 && n_mu_true == 0 && (n_jet >= 4) && (n_bjet >= 1) && (n_ph == 1) && (dr_truth_phel > 1.0);
    	        passtruth_mj = TruthTree.mujets_gamma_basic && n_el_true == 0 && n_mu_true == 1 && (n_jet >= 4) && (n_bjet >= 1) && (n_ph == 1) && (dr_truth_phmu > 1.0);
    	        passtruth_ee = TruthTree.ee_gamma_basic && n_el_true == 2 && n_mu_true == 0 && (n_jet >= 2) && (n_bjet >= 1) && (n_ph == 1) && (dr_truth_phel > 1.0) && (dr_truth_phel2 > 1.0);
    	        passtruth_mumu = TruthTree.mumu_gamma_basic && n_el_true == 0 && n_mu_true == 2 && (n_jet >= 2) && (n_bjet >= 1) && (n_ph == 1) && (dr_truth_phmu > 1.0) && (dr_truth_phmu2 > 1.0);
    	        passtruth_emu = TruthTree.emu_gamma_basic && n_el_true == 1 && n_mu_true == 1 && (n_jet >= 2) && (n_bjet >= 1) && (n_ph == 1) && (dr_truth_phel > 1.0) && (dr_truth_phmu > 1.0);
    	    }

    	    if (debug) cout << "variables for unfolding" << endl;
    	    double reco_ph_pt = 0;
    	    double reco_njet = 0;
    	    if (Upgrade) reco_njet = upgrade_njet;
    	    else reco_njet = RecoTree.jet_pt->size();
    	    double reco_ph_abseta = 0;
    	    double reco_dR_ph_lep = -1;
    	    double reco_dphi_ll = -1;
    	    double reco_deta_ll = -1;
    	    double truth_ph_pt = 0;
    	    double truth_njet = n_jet;
    	    double truth_ph_abseta = 0;
    	    double truth_dR_ph_lep = -1;
    	    double truth_dphi_ll = -1;
    	    double truth_deta_ll = -1;
    	    if (passreco_ej) {
    	        reco_ph_pt = tlv_reco_ph.Pt()/1000.;
    	        reco_ph_abseta = fabs(tlv_reco_ph.Eta());
    	        reco_dR_ph_lep = tlv_reco_ph.DeltaR(tlv_reco_el);
    	    }
    	    if (passreco_mj) {
    	        reco_ph_pt = tlv_reco_ph.Pt()/1000.;
    	        reco_ph_abseta = fabs(tlv_reco_ph.Eta());
    	        reco_dR_ph_lep = tlv_reco_ph.DeltaR(tlv_reco_mu);
    	    }
    	    if (passreco_ee) {
    	        reco_ph_pt = tlv_reco_ph.Pt()/1000.;
    	        reco_ph_abseta = fabs(tlv_reco_ph.Eta());
    	        reco_dR_ph_lep = min(tlv_reco_ph.DeltaR(tlv_reco_el),tlv_reco_ph.DeltaR(tlv_reco_el2));
    	        reco_dphi_ll = fabs(tlv_reco_el.DeltaPhi(tlv_reco_el2));
    	        reco_deta_ll = fabs(tlv_reco_el.Eta() - tlv_reco_el2.Eta());
    	    }
    	    if (passreco_emu) {
    	        reco_ph_pt = tlv_reco_ph.Pt()/1000.;
    	        reco_ph_abseta = fabs(tlv_reco_ph.Eta());
    	        reco_dR_ph_lep = min(tlv_reco_ph.DeltaR(tlv_reco_el),tlv_reco_ph.DeltaR(tlv_reco_mu));
    	        reco_dphi_ll = fabs(tlv_reco_el.DeltaPhi(tlv_reco_mu));
    	        reco_deta_ll = fabs(tlv_reco_el.Eta() - tlv_reco_mu.Eta());
    	    }
    	    if (passreco_mumu) {
    	        reco_ph_pt = tlv_reco_ph.Pt()/1000.;
    	        reco_ph_abseta = fabs(tlv_reco_ph.Eta());
    	        reco_dR_ph_lep = min(tlv_reco_ph.DeltaR(tlv_reco_mu),tlv_reco_ph.DeltaR(tlv_reco_mu2));
    	        reco_dphi_ll = fabs(tlv_reco_mu.DeltaPhi(tlv_reco_mu2));
    	        reco_deta_ll = fabs(tlv_reco_mu.Eta() - tlv_reco_mu2.Eta());
    	    }
    	    if (passtruth_ej) {
    	        truth_ph_pt = tlv_truth_ph.Pt()/1000.;
    	        truth_ph_abseta = fabs(tlv_truth_ph.Eta());
    	        truth_dR_ph_lep = tlv_truth_ph.DeltaR(tlv_truth_el);
    	    }
    	    if (passtruth_mj) {
    	        truth_ph_pt = tlv_truth_ph.Pt()/1000.;
    	        truth_ph_abseta = fabs(tlv_truth_ph.Eta());
    	        truth_dR_ph_lep = tlv_truth_ph.DeltaR(tlv_truth_mu);
    	    }
    	    if (passtruth_ee) {
    	        truth_ph_pt = tlv_truth_ph.Pt()/1000.;
    	        truth_ph_abseta = fabs(tlv_truth_ph.Eta());
    	        truth_dR_ph_lep = min(tlv_truth_ph.DeltaR(tlv_truth_el),tlv_truth_ph.DeltaR(tlv_truth_el2));
    	        truth_dphi_ll = fabs(tlv_truth_el.DeltaPhi(tlv_truth_el2));
    	        truth_deta_ll = fabs(tlv_truth_el.Eta() - tlv_truth_el2.Eta());
    	    }
    	    if (passtruth_emu) {
    	        truth_ph_pt = tlv_truth_ph.Pt()/1000.;
    	        truth_ph_abseta = fabs(tlv_truth_ph.Eta());
    	        truth_dR_ph_lep = min(tlv_truth_ph.DeltaR(tlv_truth_el),tlv_truth_ph.DeltaR(tlv_truth_mu));
    	        truth_dphi_ll = fabs(tlv_truth_el.DeltaPhi(tlv_truth_mu));
    	        truth_deta_ll = fabs(tlv_truth_el.Eta() - tlv_truth_mu.Eta());
    	    }
    	    if (passtruth_mumu) {
    	        truth_ph_pt = tlv_truth_ph.Pt()/1000.;
    	        truth_ph_abseta = fabs(tlv_truth_ph.Eta());
    	        truth_dR_ph_lep = min(tlv_truth_ph.DeltaR(tlv_truth_mu),tlv_truth_ph.DeltaR(tlv_truth_mu2));
    	        truth_dphi_ll = fabs(tlv_truth_mu.DeltaPhi(tlv_truth_mu2));
    	        truth_deta_ll = fabs(tlv_truth_mu.Eta() - tlv_truth_mu2.Eta());
    	    }
    	    if (reco_ph_pt > limit_ph_pt) reco_ph_pt = newval_ph_pt;
    	    if (reco_ph_abseta > limit_ph_abseta) reco_ph_abseta = newval_ph_abseta;
    	    if (reco_dR_ph_lep > limit_dR_ph_lep) reco_dR_ph_lep = newval_dR_ph_lep;
    	    if (reco_dphi_ll > limit_dphi_ll) reco_dphi_ll = newval_dphi_ll;
    	    if (reco_deta_ll > limit_deta_ll) reco_deta_ll = newval_deta_ll;
    	    if (truth_ph_pt > limit_ph_pt) truth_ph_pt = newval_ph_pt;
    	    if (truth_ph_abseta > limit_ph_abseta) truth_ph_abseta = newval_ph_abseta;
    	    if (truth_dR_ph_lep > limit_dR_ph_lep) truth_dR_ph_lep = newval_dR_ph_lep;
    	    if (truth_dphi_ll > limit_dphi_ll) truth_dphi_ll = newval_dphi_ll;
    	    if (truth_deta_ll > limit_deta_ll) truth_deta_ll = newval_deta_ll;

	    if (emuonly) {
		passreco_ej = false;
	    	passreco_mj = false;
	    	passreco_ee = false;
	    	passreco_mumu = false;
	    	passtruth_ej = false;
	    	passtruth_mj = false;
	    	passtruth_ee = false;
	    	passtruth_mumu = false;
	    }

    	    if (debug) cout << "fill the variables" << endl;
    	    if (passreco_ej || passreco_mj) {
    	        for (int i = 0; i < 110; i++) {
    	    	h_ph_pt[i][0][0]->Fill(reco_ph_pt, weight_reco[i]);
    	    	h_ph_abseta[i][0][0]->Fill(reco_ph_abseta, weight_reco[i]);
    	    	h_dR_ph_lep[i][0][0]->Fill(reco_dR_ph_lep, weight_reco[i]);
    	        }
    	    }
    	    if (passreco_ee || passreco_emu || passreco_mumu) {
    	        for (int i = 0; i < 110; i++) {
    	    	h_ph_pt[i][1][0]->Fill(reco_ph_pt, weight_reco[i]);
    	    	h_ph_abseta[i][1][0]->Fill(reco_ph_abseta, weight_reco[i]);
    	    	h_dR_ph_lep[i][1][0]->Fill(reco_dR_ph_lep, weight_reco[i]);
    	    	h_dphi_ll[i][1][0]->Fill(reco_dphi_ll, weight_reco[i]);
    	    	h_deta_ll[i][1][0]->Fill(reco_deta_ll, weight_reco[i]);
    	        }
    	    }
    	    if (passtruth_ej || passtruth_mj) {
    	        for (int i = 0; i < 110; i++) {
    	    	h_ph_pt[i][0][1]->Fill(truth_ph_pt, weight_truth[i]);
    	    	h_ph_abseta[i][0][1]->Fill(truth_ph_abseta, weight_truth[i]);
    	    	h_dR_ph_lep[i][0][1]->Fill(truth_dR_ph_lep, weight_truth[i]);
    	        }
    	    }
    	    if (passtruth_ee || passtruth_emu || passtruth_mumu) {
    	        for (int i = 0; i < 110; i++) {
    	    	h_ph_pt[i][1][1]->Fill(truth_ph_pt, weight_truth[i]);
    	    	h_ph_abseta[i][1][1]->Fill(truth_ph_abseta, weight_truth[i]);
    	    	h_dR_ph_lep[i][1][1]->Fill(truth_dR_ph_lep, weight_truth[i]);
    	    	h_dphi_ll[i][1][1]->Fill(truth_dphi_ll, weight_truth[i]);
    	    	h_deta_ll[i][1][1]->Fill(truth_deta_ll, weight_truth[i]);
    	        }
    	    }
    	    if (passreco_ej && !passtruth_ej) {
    	        if (ph1_idx != -1 && tlv_truth_ph.DeltaR(tlv_reco_ph) < 0.1) {
    	    	h_ph_pt[0][0][4]->Fill(reco_ph_pt, weight_reco[0]);
    	        	h_ph_abseta[0][0][4]->Fill(reco_ph_abseta, weight_reco[0]);
    	    	if (TruthTree.el_pt->size() >= 1 && tlv_truth_el.DeltaR(tlv_reco_el) < 0.1) {
    	    	   h_dR_ph_lep[0][0][4]->Fill(reco_dR_ph_lep, weight_reco[0]);
    	    	}
    	        }
    	    }
    	    if (passtruth_ej && passreco_ej) {
    	        h_ph_pt[0][0][2]->Fill(reco_ph_pt, weight_reco[0]);
    	        h_ph_abseta[0][0][2]->Fill(reco_ph_abseta, weight_reco[0]);
    	        h_dR_ph_lep[0][0][2]->Fill(reco_dR_ph_lep, weight_reco[0]);
    	        if (tlv_truth_ph.DeltaR(tlv_reco_ph) < 0.1) {
    	    	h_ph_pt[0][0][3]->Fill(reco_ph_pt, weight_reco[0]);
    	        	h_ph_abseta[0][0][3]->Fill(reco_ph_abseta, weight_reco[0]);
    	    	if (tlv_truth_el.DeltaR(tlv_reco_el) < 0.1) {
    	    	   h_dR_ph_lep[0][0][3]->Fill(reco_dR_ph_lep, weight_reco[0]);
    	    	}
    	        }
    	        h_dR_truth_reco[0][0]->Fill(tlv_truth_ph.DeltaR(tlv_reco_ph), weight_reco[0]);
    	        h_dR_truth_reco[1][0]->Fill(tlv_truth_el.DeltaR(tlv_reco_el), weight_reco[0]);
    	        h2_ph_pt_nomatch[0]->Fill(reco_ph_pt, truth_ph_pt, weight_reco[0]);
    	        h2_ph_abseta_nomatch[0]->Fill(reco_ph_abseta, truth_ph_abseta, weight_reco[0]);
    	        h2_dR_ph_lep_nomatch[0]->Fill(reco_dR_ph_lep, truth_dR_ph_lep, weight_reco[0]);
    	        if (tlv_truth_ph.DeltaR(tlv_reco_ph) < 0.1) {
    	    	for (int i = 0; i < 110; i++) {
    	    	    h2_ph_pt[i][0]->Fill(reco_ph_pt, truth_ph_pt, weight_reco[i]);
    	    	    h2_ph_abseta[i][0]->Fill(reco_ph_abseta, truth_ph_abseta, weight_reco[i]);
    	    	    if (tlv_truth_el.DeltaR(tlv_reco_el) < 0.1) {
    	    		h2_dR_ph_lep[i][0]->Fill(reco_dR_ph_lep, truth_dR_ph_lep, weight_reco[i]);
    	    	    }
    	    	}
    	        }
    	        if (tlv_truth_el.DeltaR(tlv_reco_el) < 0.1) {
    	    	for (int i = 0; i < 110; i++) {
    	    	    h2_llp_pt[i][0]->Fill(tlv_reco_el.Pt(), tlv_truth_el.Pt(), weight_reco[i]);
    	    	}
    	        }
    	    }
    	    if (passreco_ej && !passtruth_ej) h_njet[0]->Fill(truth_njet, weight_reco[0]);
    	    if (passreco_mj && !passtruth_mj) {
    	        if (ph1_idx != -1 && tlv_truth_ph.DeltaR(tlv_reco_ph) < 0.1) {
    	    	h_ph_pt[0][0][4]->Fill(reco_ph_pt, weight_reco[0]);
    	        	h_ph_abseta[0][0][4]->Fill(reco_ph_abseta, weight_reco[0]);
    	    	if (TruthTree.mu_pt->size() >= 1 && tlv_truth_mu.DeltaR(tlv_reco_mu) < 0.1) {
    	    	   h_dR_ph_lep[0][0][4]->Fill(reco_dR_ph_lep, weight_reco[0]);
    	    	}
    	        }
    	    }
    	    if (passtruth_mj && passreco_mj) {
    	        h_ph_pt[0][0][2]->Fill(reco_ph_pt, weight_reco[0]);
    	        h_ph_abseta[0][0][2]->Fill(reco_ph_abseta, weight_reco[0]);
    	        h_dR_ph_lep[0][0][2]->Fill(reco_dR_ph_lep, weight_reco[0]);
    	        if (tlv_truth_ph.DeltaR(tlv_reco_ph) < 0.1) {
    	    	h_ph_pt[0][0][3]->Fill(reco_ph_pt, weight_reco[0]);
    	        	h_ph_abseta[0][0][3]->Fill(reco_ph_abseta, weight_reco[0]);
    	    	if (tlv_truth_mu.DeltaR(tlv_reco_mu) < 0.1) {
    	    	   h_dR_ph_lep[0][0][3]->Fill(reco_dR_ph_lep, weight_reco[0]);
    	    	}
    	        }
    	        h_dR_truth_reco[0][0]->Fill(tlv_truth_ph.DeltaR(tlv_reco_ph), weight_reco[0]);
    	        h_dR_truth_reco[1][0]->Fill(tlv_truth_mu.DeltaR(tlv_reco_mu), weight_reco[0]);
    	        h2_ph_pt_nomatch[0]->Fill(reco_ph_pt, truth_ph_pt, weight_reco[0]);
    	        h2_ph_abseta_nomatch[0]->Fill(reco_ph_abseta, truth_ph_abseta, weight_reco[0]);
    	        h2_dR_ph_lep_nomatch[0]->Fill(reco_dR_ph_lep, truth_dR_ph_lep, weight_reco[0]);
    	        if (tlv_truth_ph.DeltaR(tlv_reco_ph) < 0.1) {
    	    	for (int i = 0; i < 110; i++) {
    	    	    h2_ph_pt[i][0]->Fill(reco_ph_pt, truth_ph_pt, weight_reco[i]);
    	    	    h2_ph_abseta[i][0]->Fill(reco_ph_abseta, truth_ph_abseta, weight_reco[i]);
    	    	    if (tlv_truth_mu.DeltaR(tlv_reco_mu) < 0.1) {
    	    		h2_dR_ph_lep[i][0]->Fill(reco_dR_ph_lep, truth_dR_ph_lep, weight_reco[i]);
    	    	    }
    	    	}
    	        }
    	        if (tlv_truth_mu.DeltaR(tlv_reco_mu) < 0.1) {
    	    	for (int i = 0; i < 110; i++) {
    	    	    h2_llp_pt[i][0]->Fill(tlv_reco_mu.Pt(), tlv_truth_mu.Pt(), weight_reco[i]);
    	    	}
    	        }
    	    }
    	    if (passreco_mj && !passtruth_mj) h_njet[0]->Fill(truth_njet, weight_reco[0]);
    	    if (passreco_ee && !passtruth_ee) {
    	        if (ph1_idx != -1 && tlv_truth_ph.DeltaR(tlv_reco_ph) < 0.1) {
    	    	h_ph_pt[0][1][4]->Fill(reco_ph_pt, weight_reco[0]);
    	        	h_ph_abseta[0][1][4]->Fill(reco_ph_abseta, weight_reco[0]);
    	    	if (TruthTree.el_pt->size() >= 2 && tlv_truth_el.DeltaR(tlv_reco_el) < 0.1 && tlv_truth_el2.DeltaR(tlv_reco_el2) < 0.1) {
    	    	   h_dR_ph_lep[0][1][4]->Fill(reco_dR_ph_lep, weight_reco[0]);
    	    	   h_dphi_ll[0][1][4]->Fill(reco_dphi_ll, weight_reco[0]);
    	    	   h_deta_ll[0][1][4]->Fill(reco_deta_ll, weight_reco[0]);
    	    	}
    	        }
    	    }
    	    if (passtruth_ee && passreco_ee) {
    	        h_ph_pt[0][1][2]->Fill(reco_ph_pt, weight_reco[0]);
    	        h_ph_abseta[0][1][2]->Fill(reco_ph_abseta, weight_reco[0]);
    	        h_dR_ph_lep[0][1][2]->Fill(reco_dR_ph_lep, weight_reco[0]);
    	        h_dphi_ll[0][1][2]->Fill(reco_dphi_ll, weight_reco[0]);
    	        h_deta_ll[0][1][2]->Fill(reco_deta_ll, weight_reco[0]);
    	        if (tlv_truth_ph.DeltaR(tlv_reco_ph) < 0.1) {
    	    	h_ph_pt[0][1][3]->Fill(reco_ph_pt, weight_reco[0]);
    	        	h_ph_abseta[0][1][3]->Fill(reco_ph_abseta, weight_reco[0]);
    	    	if (tlv_truth_el.DeltaR(tlv_reco_el) < 0.1 && tlv_truth_el2.DeltaR(tlv_reco_el2) < 0.1) {
    	    	   h_dR_ph_lep[0][1][3]->Fill(reco_dR_ph_lep, weight_reco[0]);
    	    	   h_dphi_ll[0][1][3]->Fill(reco_dphi_ll, weight_reco[0]);
    	    	   h_deta_ll[0][1][3]->Fill(reco_deta_ll, weight_reco[0]);
    	    	}
    	        }
    	        h_dR_truth_reco[0][1]->Fill(tlv_truth_ph.DeltaR(tlv_reco_ph), weight_reco[0]);
    	        h_dR_truth_reco[1][1]->Fill(tlv_truth_el.DeltaR(tlv_reco_el), weight_reco[0]);
    	        h_dR_truth_reco[1][1]->Fill(tlv_truth_el2.DeltaR(tlv_reco_el2), weight_reco[0]);
    	        h2_ph_pt_nomatch[1]->Fill(reco_ph_pt, truth_ph_pt, weight_reco[0]);
    	        h2_ph_abseta_nomatch[1]->Fill(reco_ph_abseta, truth_ph_abseta, weight_reco[0]);
    	        h2_dR_ph_lep_nomatch[1]->Fill(reco_dR_ph_lep, truth_dR_ph_lep, weight_reco[0]);
    	        h2_dphi_ll_nomatch[1]->Fill(reco_dphi_ll, truth_dphi_ll, weight_reco[0]);
    	        h2_deta_ll_nomatch[1]->Fill(reco_deta_ll, truth_deta_ll, weight_reco[0]);
    	        if (tlv_truth_ph.DeltaR(tlv_reco_ph) < 0.1) {
    	    	for (int i = 0; i < 110; i++) {
    	    	    h2_ph_pt[i][1]->Fill(reco_ph_pt, truth_ph_pt, weight_reco[i]);
    	    	    h2_ph_abseta[i][1]->Fill(reco_ph_abseta, truth_ph_abseta, weight_reco[i]);
    	    	    if (tlv_truth_el.DeltaR(tlv_reco_el) < 0.1 && tlv_truth_el2.DeltaR(tlv_reco_el2) < 0.1) {
    	    		h2_dR_ph_lep[i][1]->Fill(reco_dR_ph_lep, truth_dR_ph_lep, weight_reco[i]);
    	    		h2_dphi_ll[i][1]->Fill(reco_dphi_ll, truth_dphi_ll, weight_reco[i]);
    	    		h2_deta_ll[i][1]->Fill(reco_deta_ll, truth_deta_ll, weight_reco[i]);
    	    	    }
    	    	}
    	        }
    	    }
    	    if (passreco_ee && !passtruth_ee) h_njet[1]->Fill(truth_njet, weight_reco[0]);
    	    if (passreco_emu && !passtruth_emu) {
    	        if (ph1_idx != -1 && tlv_truth_ph.DeltaR(tlv_reco_ph) < 0.1) {
    	    	h_ph_pt[0][1][4]->Fill(reco_ph_pt, weight_reco[0]);
    	        	h_ph_abseta[0][1][4]->Fill(reco_ph_abseta, weight_reco[0]);
    	    	if (TruthTree.el_pt->size() >= 1 && TruthTree.mu_pt->size() >= 1 && tlv_truth_el.DeltaR(tlv_reco_el) < 0.1 && tlv_truth_mu.DeltaR(tlv_reco_mu) < 0.1) {
    	    	   h_dR_ph_lep[0][1][4]->Fill(reco_dR_ph_lep, weight_reco[0]);
    	    	   h_dphi_ll[0][1][4]->Fill(reco_dphi_ll, weight_reco[0]);
    	    	   h_deta_ll[0][1][4]->Fill(reco_deta_ll, weight_reco[0]);
    	    	}
    	        }
    	    }
    	    if (passtruth_emu && passreco_emu) {
    	        h_ph_pt[0][1][2]->Fill(reco_ph_pt, weight_reco[0]);
    	        h_ph_abseta[0][1][2]->Fill(reco_ph_abseta, weight_reco[0]);
    	        h_dR_ph_lep[0][1][2]->Fill(reco_dR_ph_lep, weight_reco[0]);
    	        h_dphi_ll[0][1][2]->Fill(reco_dphi_ll, weight_reco[0]);
    	        h_deta_ll[0][1][2]->Fill(reco_deta_ll, weight_reco[0]);
    	        if (tlv_truth_ph.DeltaR(tlv_reco_ph) < 0.1) {
    	    	h_ph_pt[0][1][3]->Fill(reco_ph_pt, weight_reco[0]);
    	        	h_ph_abseta[0][1][3]->Fill(reco_ph_abseta, weight_reco[0]);
    	    	if (tlv_truth_el.DeltaR(tlv_reco_el) < 0.1 && tlv_truth_mu.DeltaR(tlv_reco_mu) < 0.1) {
    	    	   h_dR_ph_lep[0][1][3]->Fill(reco_dR_ph_lep, weight_reco[0]);
    	    	   h_dphi_ll[0][1][3]->Fill(reco_dphi_ll, weight_reco[0]);
    	    	   h_deta_ll[0][1][3]->Fill(reco_deta_ll, weight_reco[0]);
    	    	}
    	        }
    	        h_dR_truth_reco[0][1]->Fill(tlv_truth_ph.DeltaR(tlv_reco_ph), weight_reco[0]);
    	        h_dR_truth_reco[1][1]->Fill(tlv_truth_el.DeltaR(tlv_reco_el), weight_reco[0]);
    	        h_dR_truth_reco[1][1]->Fill(tlv_truth_mu.DeltaR(tlv_reco_mu), weight_reco[0]);
    	        h2_ph_pt_nomatch[1]->Fill(reco_ph_pt, truth_ph_pt, weight_reco[0]);
    	        h2_ph_abseta_nomatch[1]->Fill(reco_ph_abseta, truth_ph_abseta, weight_reco[0]);
    	        h2_dR_ph_lep_nomatch[1]->Fill(reco_dR_ph_lep, truth_dR_ph_lep, weight_reco[0]);
    	        h2_dphi_ll_nomatch[1]->Fill(reco_dphi_ll, truth_dphi_ll, weight_reco[0]);
    	        h2_deta_ll_nomatch[1]->Fill(reco_deta_ll, truth_deta_ll, weight_reco[0]);
    	        if (tlv_truth_ph.DeltaR(tlv_reco_ph) < 0.1) {
    	    	for (int i = 0; i < 110; i++) {
    	    	    h2_ph_pt[i][1]->Fill(reco_ph_pt, truth_ph_pt, weight_reco[i]);
    	    	    h2_ph_abseta[i][1]->Fill(reco_ph_abseta, truth_ph_abseta, weight_reco[i]);
    	    	    if (tlv_truth_el.DeltaR(tlv_reco_el) < 0.1 && tlv_truth_mu.DeltaR(tlv_reco_mu) < 0.1) {
    	    		h2_dR_ph_lep[i][1]->Fill(reco_dR_ph_lep, truth_dR_ph_lep, weight_reco[i]);
    	    		h2_dphi_ll[i][1]->Fill(reco_dphi_ll, truth_dphi_ll, weight_reco[i]);
    	    		h2_deta_ll[i][1]->Fill(reco_deta_ll, truth_deta_ll, weight_reco[i]);
    	    	    }
    	    	}
    	        }
    	    }
    	    if (passreco_emu && !passtruth_emu) h_njet[1]->Fill(truth_njet, weight_reco[0]);
    	    if (passreco_mumu && !passtruth_mumu) {
    	        if (ph1_idx != -1 && tlv_truth_ph.DeltaR(tlv_reco_ph) < 0.1) {
    	    	h_ph_pt[0][1][4]->Fill(reco_ph_pt, weight_reco[0]);
    	        	h_ph_abseta[0][1][4]->Fill(reco_ph_abseta, weight_reco[0]);
    	    	if (TruthTree.mu_pt->size() >= 2 && tlv_truth_mu.DeltaR(tlv_reco_mu) < 0.1 && tlv_truth_mu2.DeltaR(tlv_reco_mu2) < 0.1) {
    	    	   h_dR_ph_lep[0][1][4]->Fill(reco_dR_ph_lep, weight_reco[0]);
    	    	   h_dphi_ll[0][1][4]->Fill(reco_dphi_ll, weight_reco[0]);
    	    	   h_deta_ll[0][1][4]->Fill(reco_deta_ll, weight_reco[0]);
    	    	}
    	        }
    	    }
    	    if (passtruth_mumu && passreco_mumu) {
    	        h_ph_pt[0][1][2]->Fill(reco_ph_pt, weight_reco[0]);
    	        h_ph_abseta[0][1][2]->Fill(reco_ph_abseta, weight_reco[0]);
    	        h_dR_ph_lep[0][1][2]->Fill(reco_dR_ph_lep, weight_reco[0]);
    	        h_dphi_ll[0][1][2]->Fill(reco_dphi_ll, weight_reco[0]);
    	        h_deta_ll[0][1][2]->Fill(reco_deta_ll, weight_reco[0]);
    	        if (tlv_truth_ph.DeltaR(tlv_reco_ph) < 0.1) {
    	    	h_ph_pt[0][1][3]->Fill(reco_ph_pt, weight_reco[0]);
    	        	h_ph_abseta[0][1][3]->Fill(reco_ph_abseta, weight_reco[0]);
    	    	if (tlv_truth_mu.DeltaR(tlv_reco_mu) < 0.1 && tlv_truth_mu2.DeltaR(tlv_reco_mu2) < 0.1) {
    	    	   h_dR_ph_lep[0][1][3]->Fill(reco_dR_ph_lep, weight_reco[0]);
    	    	   h_dphi_ll[0][1][3]->Fill(reco_dphi_ll, weight_reco[0]);
    	    	   h_deta_ll[0][1][3]->Fill(reco_deta_ll, weight_reco[0]);
    	    	}
    	        }
    	        h_dR_truth_reco[0][1]->Fill(tlv_truth_ph.DeltaR(tlv_reco_ph), weight_reco[0]);
    	        h_dR_truth_reco[1][1]->Fill(tlv_truth_mu.DeltaR(tlv_reco_mu), weight_reco[0]);
    	        h_dR_truth_reco[1][1]->Fill(tlv_truth_mu2.DeltaR(tlv_reco_mu2), weight_reco[0]);
    	        h2_ph_pt_nomatch[1]->Fill(reco_ph_pt, truth_ph_pt, weight_reco[0]);
    	        h2_ph_abseta_nomatch[1]->Fill(reco_ph_abseta, truth_ph_abseta, weight_reco[0]);
    	        h2_dR_ph_lep_nomatch[1]->Fill(reco_dR_ph_lep, truth_dR_ph_lep, weight_reco[0]);
    	        h2_dphi_ll_nomatch[1]->Fill(reco_dphi_ll, truth_dphi_ll, weight_reco[0]);
    	        h2_deta_ll_nomatch[1]->Fill(reco_deta_ll, truth_deta_ll, weight_reco[0]);
    	        if (tlv_truth_ph.DeltaR(tlv_reco_ph) < 0.1) {
    	    	for (int i = 0; i < 110; i++) {
    	    	    h2_ph_pt[i][1]->Fill(reco_ph_pt, truth_ph_pt, weight_reco[i]);
    	    	    h2_ph_abseta[i][1]->Fill(reco_ph_abseta, truth_ph_abseta, weight_reco[i]);
    	    	    if (tlv_truth_mu.DeltaR(tlv_reco_mu) < 0.1 && tlv_truth_mu2.DeltaR(tlv_reco_mu2) < 0.1) {
    	    		h2_dR_ph_lep[i][1]->Fill(reco_dR_ph_lep, truth_dR_ph_lep, weight_reco[i]);
    	    		h2_dphi_ll[i][1]->Fill(reco_dphi_ll, truth_dphi_ll, weight_reco[i]);
    	    		h2_deta_ll[i][1]->Fill(reco_deta_ll, truth_deta_ll, weight_reco[i]);
    	    	    }
    	    	}
    	        }
    	    }
    	    if (passreco_mumu && !passtruth_mumu) h_njet[1]->Fill(truth_njet, weight_reco[0]);
    	    if (debug) cout << "variable filling finished" << endl;

    	    if (passreco_ej) {
    	        if (passtruth_ej) {xtalk[0][0] += weight_reco_nominal;xtalk_err[0][0] += pow(weight_reco_nominal,2);}
    	        else if (passtruth_mj) {xtalk[0][1] += weight_reco_nominal;xtalk_err[0][1] += pow(weight_reco_nominal,2);}
    	        else if (passtruth_ee) {xtalk[0][2] += weight_reco_nominal;xtalk_err[0][2] += pow(weight_reco_nominal,2);}
    	        else if (passtruth_emu) {xtalk[0][3] += weight_reco_nominal;xtalk_err[0][3] += pow(weight_reco_nominal,2);}
    	        else if (passtruth_mumu) {xtalk[0][4] += weight_reco_nominal;xtalk_err[0][4] += pow(weight_reco_nominal,2);}
    	        else {xtalk[0][5] += weight_reco_nominal;xtalk_err[0][5] += pow(weight_reco_nominal,2);}
    	    }
    	    if (passreco_mj) {
    	        if (passtruth_ej) {xtalk[1][0] += weight_reco_nominal;xtalk_err[1][0] += pow(weight_reco_nominal,2);}
    	        else if (passtruth_mj) {xtalk[1][1] += weight_reco_nominal;xtalk_err[1][1] += pow(weight_reco_nominal,2);}
    	        else if (passtruth_ee) {xtalk[1][2] += weight_reco_nominal;xtalk_err[1][2] += pow(weight_reco_nominal,2);}
    	        else if (passtruth_emu) {xtalk[1][3] += weight_reco_nominal;xtalk_err[1][3] += pow(weight_reco_nominal,2);}
    	        else if (passtruth_mumu) {xtalk[1][4] += weight_reco_nominal;xtalk_err[1][4] += pow(weight_reco_nominal,2);}
    	        else {xtalk[1][5] += weight_reco_nominal;xtalk_err[1][5] += pow(weight_reco_nominal,2);}
    	    }
    	    if (passreco_ee) {
    	        if (passtruth_ej) {xtalk[2][0] += weight_reco_nominal;xtalk_err[2][0] += pow(weight_reco_nominal,2);}
    	        else if (passtruth_mj) {xtalk[2][1] += weight_reco_nominal;xtalk_err[2][1] += pow(weight_reco_nominal,2);}
    	        else if (passtruth_ee) {xtalk[2][2] += weight_reco_nominal;xtalk_err[2][2] += pow(weight_reco_nominal,2);}
    	        else if (passtruth_emu) {xtalk[2][3] += weight_reco_nominal;xtalk_err[2][3] += pow(weight_reco_nominal,2);}
    	        else if (passtruth_mumu) {xtalk[2][4] += weight_reco_nominal;xtalk_err[2][4] += pow(weight_reco_nominal,2);}
    	        else {xtalk[2][5] += weight_reco_nominal;xtalk_err[2][5] += pow(weight_reco_nominal,2);}
    	    }
    	    if (passreco_emu) {
    	        if (passtruth_ej) {xtalk[3][0] += weight_reco_nominal;xtalk_err[3][0] += pow(weight_reco_nominal,2);}
    	        else if (passtruth_mj) {xtalk[3][1] += weight_reco_nominal;xtalk_err[3][1] += pow(weight_reco_nominal,2);}
    	        else if (passtruth_ee) {xtalk[3][2] += weight_reco_nominal;xtalk_err[3][2] += pow(weight_reco_nominal,2);}
    	        else if (passtruth_emu) {xtalk[3][3] += weight_reco_nominal;xtalk_err[3][3] += pow(weight_reco_nominal,2);}
    	        else if (passtruth_mumu) {xtalk[3][4] += weight_reco_nominal;xtalk_err[3][4] += pow(weight_reco_nominal,2);}
    	        else {xtalk[3][5] += weight_reco_nominal;xtalk_err[3][5] += pow(weight_reco_nominal,2);}
    	    }
    	    if (passreco_mumu) {
    	        if (passtruth_ej) {xtalk[4][0] += weight_reco_nominal;xtalk_err[4][0] += pow(weight_reco_nominal,2);}
    	        else if (passtruth_mj) {xtalk[4][1] += weight_reco_nominal;xtalk_err[4][1] += pow(weight_reco_nominal,2);}
    	        else if (passtruth_ee) {xtalk[4][2] += weight_reco_nominal;xtalk_err[4][2] += pow(weight_reco_nominal,2);}
    	        else if (passtruth_emu) {xtalk[4][3] += weight_reco_nominal;xtalk_err[4][3] += pow(weight_reco_nominal,2);}
    	        else if (passtruth_mumu) {xtalk[4][4] += weight_reco_nominal;xtalk_err[4][4] += pow(weight_reco_nominal,2);}
    	        else {xtalk[4][5] += weight_reco_nominal;xtalk_err[4][5] += pow(weight_reco_nominal,2);}
    	    }
    	}
    }

    for (int i = 0; i < 110; i++) {
	for (int j = 0; j < 2; j++) {
	    for (int k = 0; k < 2; k++) {
		for (int l = 0; l < 20; l++) {
		    cutflow[i][5][k][l] += cutflow[i][j][k][l];
		    cutflow_err[i][5][k][l] += cutflow_err[i][j][k][l];
		}
	    }
	}
    }
    for (int i = 0; i < 110; i++) {
	for (int j = 2; j < 5; j++) {
	    for (int k = 0; k < 2; k++) {
		for (int l = 0; l < 20; l++) {
		    cutflow[i][6][k][l] += cutflow[i][j][k][l];
		    cutflow_err[i][6][k][l] += cutflow_err[i][j][k][l];
		}
	    }
	}
    }

    string savename;
    if (ISRFSRUp) {
	savename = "results/Nominal/Sig_Sys_ISRFSRUp.root";
    } else if (ISRFSRDn) {
	savename = "results/Nominal/Sig_Sys_ISRFSRDn.root";
    } else if (PS) {
	savename = "results/Nominal/Sig_Sys_PS.root";
    } else if (PSNew) {
	savename = "results/Nominal/Sig_Sys_PSNew.root";
    } else if (NominalNew) {
	savename = "results/Nominal/Sig_Sys_NominalNew.root";
    } else if (TTBar) {
	savename = "results/Nominal/Sig_Sys_TTBar.root";
    } else if (Upgrade) {
	savename = "results/Nominal/Sig_Sys_Upgrade";
	if (Upgrade2) savename += "2";
	if (Upgrade3) savename += "3";
	if (emuonly) savename += "_emuonly";
	if (pt500) savename += "_pt500";
	if (pt1000) savename += "_pt1000";
	if (pt2000) savename += "_pt2000";
	savename += ".root";
    } else {
	savename = "results/Nominal/Sig_Sys_ScalePdf.root";
    }
    TFile *fout = new TFile(savename.c_str(), "recreate");
    fout->cd();
    fout->ls();

    string channels[7] = {"ejets", "mujets", "ee", "emu", "mumu", "ljets", "ll"};
    string levels[5] = {"reco", "truth", "reco_fid", "reco_fid_match", "reco_failfid_match"};
    for (int i = 0; i < 110; i++) {
        string varkey = GetName(i);
        for (int j = 0; j < 7; j++) {
            string channel = channels[j];
            for (int k = 0; k < 2; k++) {
        	double kfactor;
        	if (j == 0 || j == 1 || j == 5) {
        	    if (!Upgrade) kfactor = 1.80;
        	    else kfactor = 1.30;
        	}
        	if (j == 2 || j == 3 || j == 4 || j == 6) {
        	    if (!Upgrade) kfactor = 1.97;
        	    else kfactor = 1.44;
        	}
        	string level = levels[k];
        	string hname = "N_" + varkey + "_" + channel + "_" + level;
        	TH1F* h = new TH1F(hname.c_str(), hname.c_str(), 1, 0, 1);
        	h->SetBinContent(1, cutflow[i][j][k][end_idx[j][k]]);
        	h->SetBinError(1, sqrt(cutflow_err[i][j][k][end_idx[j][k]]));
        	h->Write();
        	if (i == 0) {
        	    cout << hname << ": " << h->GetBinContent(1) << " " << h->GetBinContent(1)*event_norm*lumi*kfactor << endl;
        	    //for (int itmp = 0; itmp < 20; itmp++) {
        	    //    cout << cutflow[i][j][k][itmp]*event_norm*lumi*kfactor << endl;
        	    //}
        	}
            }
        }
    }

    cout << "save xtalk" << endl;
    TH2F* h_xtalk = new TH2F("xtalk", "xtakl", 5, 0, 5, 6, 0, 6);
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 6; j++) {
            h_xtalk->SetBinContent(i+1, j+1, xtalk[i][j]);
            h_xtalk->SetBinError(i+1, j+1, sqrt(xtalk_err[i][j]));
        }
    }
    h_xtalk->Write();

    cout << "save 110 copies" << endl;
    for (int i = 0; i < 110; i++) {
        string varkey = GetName(i);
        for (int j = 0; j < 2; j++) {
            string channel = channels[j+5];
            string hname;
            TH2F* tmph=NULL;
            double kfactor;
            if (j == 0) {
        	if (!Upgrade) kfactor = 1.80;
        	else kfactor = 1.30;
            }
            if (j == 1) {
        	if (!Upgrade) kfactor = 1.97;
        	else kfactor = 1.44;
            }

            hname = "h2_ph_pt" + varkey + "_" + channel;
            h2_ph_pt[i][j]->SetName(hname.c_str()); h2_ph_pt[i][j]->Write();
            hname = "h2_lumi_ph_pt" + varkey + "_" + channel;
            tmph = (TH2F*)h2_ph_pt[i][j]->Clone(hname.c_str());
            tmph->Scale(event_norm * lumi * kfactor); tmph->Write();
            hname = "h2_llp_pt" + varkey + "_" + channel;
            h2_llp_pt[i][j]->SetName(hname.c_str()); h2_llp_pt[i][j]->Write();
            hname = "h2_lumi_llp_pt" + varkey + "_" + channel;
            tmph = (TH2F*)h2_llp_pt[i][j]->Clone(hname.c_str());
            tmph->Scale(event_norm * lumi * kfactor); tmph->Write();
            hname = "h2_ph_abseta" + varkey + "_" + channel;
            h2_ph_abseta[i][j]->SetName(hname.c_str()); h2_ph_abseta[i][j]->Write();
            hname = "h2_lumi_ph_abseta" + varkey + "_" + channel;
            tmph = (TH2F*)h2_ph_abseta[i][j]->Clone(hname.c_str());
            tmph->Scale(event_norm * lumi * kfactor); tmph->Write();
            hname = "h2_dR_ph_lep" + varkey + "_" + channel;
            h2_dR_ph_lep[i][j]->SetName(hname.c_str()); h2_dR_ph_lep[i][j]->Write();
            hname = "h2_lumi_dR_ph_lep" + varkey + "_" + channel;
            tmph = (TH2F*)h2_dR_ph_lep[i][j]->Clone(hname.c_str());
            tmph->Scale(event_norm * lumi * kfactor); tmph->Write();
            hname = "h2_dphi_ll" + varkey + "_" + channel;
            h2_dphi_ll[i][j]->SetName(hname.c_str()); h2_dphi_ll[i][j]->Write();
            hname = "h2_lumi_dphi_ll" + varkey + "_" + channel;
            tmph = (TH2F*)h2_dphi_ll[i][j]->Clone(hname.c_str());
            tmph->Scale(event_norm * lumi * kfactor); tmph->Write();
            hname = "h2_deta_ll" + varkey + "_" + channel;
            h2_deta_ll[i][j]->SetName(hname.c_str()); h2_deta_ll[i][j]->Write();
            hname = "h2_lumi_deta_ll" + varkey + "_" + channel;
            tmph = (TH2F*)h2_deta_ll[i][j]->Clone(hname.c_str());
            tmph->Scale(event_norm * lumi * kfactor); tmph->Write();
            for (int k = 0; k < 5; k++) {
        	string level = levels[k];
        	string hname;
        	TH1F* tmph = NULL;
        	hname = "h_ph_pt" + varkey + "_" + channel + "_" + level;
        	h_ph_pt[i][j][k]->SetName(hname.c_str()); h_ph_pt[i][j][k]->Write();
        	hname = "h_lumi_ph_pt" + varkey + "_" + channel + "_" + level;
        	tmph = (TH1F*)h_ph_pt[i][j][k]->Clone(hname.c_str());
        	tmph->Scale(event_norm * lumi * kfactor); tmph->Write();
        	//if (i==0) cout << hname << ": " << tmph->Integral() << endl;
        	hname = "h_ph_abseta" + varkey + "_" + channel + "_" + level;
        	h_ph_abseta[i][j][k]->SetName(hname.c_str()); h_ph_abseta[i][j][k]->Write();
        	hname = "h_lumi_ph_abseta" + varkey + "_" + channel + "_" + level;
        	tmph = (TH1F*)h_ph_abseta[i][j][k]->Clone(hname.c_str());
        	tmph->Scale(event_norm * lumi * kfactor); tmph->Write();
        	hname = "h_dR_ph_lep" + varkey + "_" + channel + "_" + level;
        	h_dR_ph_lep[i][j][k]->SetName(hname.c_str()); h_dR_ph_lep[i][j][k]->Write();
        	hname = "h_lumi_dR_ph_lep" + varkey + "_" + channel + "_" + level;
        	tmph = (TH1F*)h_dR_ph_lep[i][j][k]->Clone(hname.c_str());
        	tmph->Scale(event_norm * lumi * kfactor); tmph->Write();
        	hname = "h_dphi_ll" + varkey + "_" + channel + "_" + level;
        	h_dphi_ll[i][j][k]->SetName(hname.c_str()); h_dphi_ll[i][j][k]->Write();
        	hname = "h_lumi_dphi_ll" + varkey + "_" + channel + "_" + level;
        	tmph = (TH1F*)h_dphi_ll[i][j][k]->Clone(hname.c_str());
        	tmph->Scale(event_norm * lumi * kfactor); tmph->Write();
        	hname = "h_deta_ll" + varkey + "_" + channel + "_" + level;
        	h_deta_ll[i][j][k]->SetName(hname.c_str()); h_deta_ll[i][j][k]->Write();
        	hname = "h_lumi_deta_ll" + varkey + "_" + channel + "_" + level;
        	tmph = (TH1F*)h_deta_ll[i][j][k]->Clone(hname.c_str());
        	tmph->Scale(event_norm * lumi * kfactor); tmph->Write();
            }
        }
    }
    h_njet[0]->Write();
    h_njet[1]->Write();

    cout << "save dR" << endl;
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            h_dR_truth_reco[i][j]->Write();
        }
    }
    cout << "save nomatch" << endl;
    for (int i = 0; i < 2; i++) {
        h2_ph_pt_nomatch[i]->Write();
        h2_ph_abseta_nomatch[i]->Write();
        h2_dR_ph_lep_nomatch[i]->Write();
        h2_dphi_ll_nomatch[i]->Write();
        h2_deta_ll_nomatch[i]->Write();
    }

    //h_reco_ej_dr_truth_ph_parton_b1->Write();
    //h_reco_ej_dr_truth_ph_parton_b2->Write();
    //h_reco_ej_dr_truth_ph_parton_el1->Write();
    //h_reco_ej_dr_truth_ph_parton_q1->Write();
    //h_reco_ej_dr_truth_ph_parton_q2->Write();
    //h_reco_ej_dr_truth_ph_qcd_recoil->Write();
    //h_reco_emu_dr_truth_ph_parton_b1->Write();
    //h_reco_emu_dr_truth_ph_parton_b2->Write();
    //h_reco_emu_dr_truth_ph_parton_el1->Write();
    //h_reco_emu_dr_truth_ph_parton_mu1->Write();
    //h_reco_emu_dr_truth_ph_qcd_recoil->Write();
    //h_parton_channel->Write();
    //h_dl_cf->Write();

    cout << "Results save to: " << savename << endl;
    fout->Close();
    delete fout;
}

TH2F *Norm2DHRow(TH2F* h_2d) {
    TH2F* h_out = (TH2F*)h_2d->Clone();
    for (int i = 0; i < h_2d->GetNbinsY(); i++) {
        double sum_row = 0;
        for (int j = 0; j < h_2d->GetNbinsX(); j++) {
            sum_row += h_2d->GetBinContent(j+1,i+1);
        }
        for (int j = 0; j < h_2d->GetNbinsX(); j++) {
            h_out->SetBinContent(j+1,i+1, h_2d->GetBinContent(j+1,i+1)/sum_row);
            h_out->SetBinError(j+1,i+1, h_2d->GetBinError(j+1,i+1)/sum_row); // to be improved
        }
    }
    return h_out;
}

TH2F *Norm2DHColumn(TH2F* h_2d) {
    TH2F* h_out = (TH2F*)h_2d->Clone();
    for (int i = 0; i < h_2d->GetNbinsX(); i++) {
        double sum_column = 0;
        for (int j = 0; j < h_2d->GetNbinsY(); j++) {
            sum_column += h_2d->GetBinContent(i+1,j+1);
        }
        for (int j = 0; j < h_2d->GetNbinsY(); j++) {
            h_out->SetBinContent(i+1,j+1, h_2d->GetBinContent(i+1,j+1)/sum_column);
            h_out->SetBinError(i+1,j+1, h_2d->GetBinError(i+1,j+1)/sum_column); // to be improved
        }
    }
    return h_out;
}

string GetName(int i) {
    string namekey;
    if (i == 0) namekey = "Nominal";
    else if (i == 77) namekey = "R1F2";
    else if (i == 33) namekey = "R2F1";
    else if (i == 66) namekey = "R2F05";
    else if (i == 88) namekey = "R05F2";
    else if (i == 22) namekey = "R05F1";
    else if (i == 55) namekey = "R05F05";
    else if (i == 44) namekey = "R1F05";
    else if (i == 99) namekey = "R2F2";
    else if (i == 39) namekey = "PDF25";
    else if (i == 38) namekey = "PDF24";
    else if (i == 41) namekey = "PDF27";
    else if (i == 40) namekey = "PDF26";
    else if (i == 35) namekey = "PDF21";
    else if (i == 34) namekey = "PDF20";
    else if (i == 37) namekey = "PDF23";
    else if (i == 36) namekey = "PDF22";
    else if (i == 43) namekey = "PDF29";
    else if (i == 42) namekey = "PDF28";
    else if (i == 51) namekey = "PDF36";
    else if (i == 49) namekey = "PDF34";
    else if (i == 50) namekey = "PDF35";
    else if (i == 47) namekey = "PDF32";
    else if (i == 48) namekey = "PDF33";
    else if (i == 45) namekey = "PDF30";
    else if (i == 46) namekey = "PDF31";
    else if (i == 53) namekey = "PDF38";
    else if (i == 54) namekey = "PDF39";
    else if (i == 103) namekey = "PDF83";
    else if (i == 102) namekey = "PDF82";
    else if (i == 101) namekey = "PDF81";
    else if (i == 100) namekey = "PDF80";
    else if (i == 107) namekey = "PDF87";
    else if (i == 13) namekey = "PDF100";
    else if (i == 105) namekey = "PDF85";
    else if (i == 104) namekey = "PDF84";
    else if (i == 109) namekey = "PDF89";
    else if (i == 108) namekey = "PDF88";
    else if (i == 87) namekey = "PDF69";
    else if (i == 86) namekey = "PDF68";
    else if (i == 79) namekey = "PDF61";
    else if (i == 78) namekey = "PDF60";
    else if (i == 81) namekey = "PDF63";
    else if (i == 80) namekey = "PDF62";
    else if (i == 83) namekey = "PDF65";
    else if (i == 82) namekey = "PDF64";
    else if (i == 85) namekey = "PDF67";
    else if (i == 84) namekey = "PDF66";
    else if (i == 20) namekey = "PDF8";
    else if (i == 21) namekey = "PDF9";
    else if (i == 18) namekey = "PDF6";
    else if (i == 19) namekey = "PDF7";
    else if (i == 16) namekey = "PDF4";
    else if (i == 17) namekey = "PDF5";
    else if (i == 14) namekey = "PDF2";
    else if (i == 15) namekey = "PDF3";
    else if (i == 12) namekey = "PDF1";
    else if (i == 6) namekey = "PDF94";
    else if (i == 7) namekey = "PDF95";
    else if (i == 8) namekey = "PDF96";
    else if (i == 9) namekey = "PDF97";
    else if (i == 2) namekey = "PDF90";
    else if (i == 3) namekey = "PDF91";
    else if (i == 4) namekey = "PDF92";
    else if (i == 5) namekey = "PDF93";
    else if (i == 10) namekey = "PDF98";
    else if (i == 11) namekey = "PDF99";
    else if (i == 31) namekey = "PDF18";
    else if (i == 32) namekey = "PDF19";
    else if (i == 27) namekey = "PDF14";
    else if (i == 28) namekey = "PDF15";
    else if (i == 29) namekey = "PDF16";
    else if (i == 30) namekey = "PDF17";
    else if (i == 23) namekey = "PDF10";
    else if (i == 24) namekey = "PDF11";
    else if (i == 25) namekey = "PDF12";
    else if (i == 26) namekey = "PDF13";
    else if (i == 97) namekey = "PDF78";
    else if (i == 98) namekey = "PDF79";
    else if (i == 91) namekey = "PDF72";
    else if (i == 92) namekey = "PDF73";
    else if (i == 89) namekey = "PDF70";
    else if (i == 90) namekey = "PDF71";
    else if (i == 95) namekey = "PDF76";
    else if (i == 96) namekey = "PDF77";
    else if (i == 93) namekey = "PDF74";
    else if (i == 94) namekey = "PDF75";
    else if (i == 106) namekey = "PDF86";
    else if (i == 52) namekey = "PDF37";
    else if (i == 65) namekey = "PDF49";
    else if (i == 64) namekey = "PDF48";
    else if (i == 63) namekey = "PDF47";
    else if (i == 62) namekey = "PDF46";
    else if (i == 61) namekey = "PDF45";
    else if (i == 60) namekey = "PDF44";
    else if (i == 59) namekey = "PDF43";
    else if (i == 58) namekey = "PDF42";
    else if (i == 57) namekey = "PDF41";
    else if (i == 56) namekey = "PDF40";
    else if (i == 67) namekey = "PDF50";
    else if (i == 68) namekey = "PDF51";
    else if (i == 69) namekey = "PDF52";
    else if (i == 70) namekey = "PDF53";
    else if (i == 71) namekey = "PDF54";
    else if (i == 72) namekey = "PDF55";
    else if (i == 73) namekey = "PDF56";
    else if (i == 74) namekey = "PDF57";
    else if (i == 75) namekey = "PDF58";
    else if (i == 76) namekey = "PDF59";
    else namekey = "Unknown";

    return namekey;
}

bool IsLepton(int pdg) {
    if (fabs(pdg) == 11 || fabs(pdg) == 12 || fabs(pdg) == 13 || fabs(pdg) == 14 || fabs(pdg) == 15 || fabs(pdg) == 16) {
	return true;
    } else {
	return false;
    }
}
