{
    TFile*f = new TFile("results/TTGOR/Histograms.root");
    TH1F* h_Event_Category = (TH1F*)f->Get("Event_Category");
    TH1F* h_cat1_t_diff1_M = (TH1F*)f->Get("cat1_t_diff1_M");
    TH1F* h_cat2_t_diff1_M = (TH1F*)f->Get("cat2_t_diff1_M");
    TH1F* h_cat3_t_diff2_M = (TH1F*)f->Get("cat3_t_diff2_M");
    TH1F* h_cat4_tbar_diff2_M = (TH1F*)f->Get("cat4_tbar_diff2_M");
    TH1F* h_cat5_t_diff2_M = (TH1F*)f->Get("cat5_t_diff2_M");
    TH1F* h_cat6_tbar_diff2_M = (TH1F*)f->Get("cat6_tbar_diff2_M");
    
    TCanvas* c = new TCanvas("c","",800,600);
    c->Divide(3,3);

    c->cd(1);
    h_Event_Category->Draw();

    c->cd(2);
    h_cat1_t_diff1_M->Draw();

    c->cd(3);
    h_cat2_t_diff1_M->Draw();

    c->cd(4);
    h_cat3_t_diff2_M->Draw();

    c->cd(5);
    h_cat4_tbar_diff2_M->Draw();

    c->cd(6);
    h_cat5_t_diff2_M->Draw();

    c->cd(7);
    h_cat6_tbar_diff2_M->Draw();

    c->SaveAs("plots/TTGOR/cat1_t_diff1_M.png");
}
