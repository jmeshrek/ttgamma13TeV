#include <TFile.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TPad.h>
#include <TLine.h>
#include <string>
#include <TLatex.h>
#include <TMath.h>
#include <vector>
#include <TGraphErrors.h>
#include <map>
#include <TIterator.h>
#include <TKey.h>
#include <TObject.h>
#include <THStack.h>

#include "AtlasStyle.h"
#include "PlotComparor.h"
#include "StringPlayer.h"
#include "ConfigReader.h"
#include "Logger.h"

using namespace std;

int main(int argc, char * argv[]) {
    Logger *lg = new Logger("MAIN");
    lg->Info("s", "Hi, this is the main programm~");

    string fname_13tev = "results/Nominal/Sig_Sys_ScalePdf.root";
    string fname_14tev = "results/Nominal/Sig_Sys_Upgrade.root";

    string channel = "ljets";
    string hname_photon = "dR_truth_reco_photon_" + channel;
    string hname_lepton = "dR_truth_reco_lepton_" + channel;

    TFile* f_13tev = new TFile(fname_13tev.c_str());
    TFile* f_14tev = new TFile(fname_14tev.c_str());

    TH1F* h_13tev_photon = (TH1F*)f_13tev->Get(hname_photon.c_str());
    TH1F* h_14tev_photon = (TH1F*)f_14tev->Get(hname_photon.c_str());
    TH1F* h_13tev_lepton = (TH1F*)f_13tev->Get(hname_lepton.c_str());
    TH1F* h_14tev_lepton = (TH1F*)f_14tev->Get(hname_lepton.c_str());

    PlotComparor* PC = new PlotComparor();
    PC->DrawRatio(true);
    PC->SetSaveDir("plots/Upgrade/");
    PC->NormToUnit(true);
    PC->SetChannel(channel);
    string drawoptions = "hist";
    drawoptions += " _e";
    PC->SetDrawOption(drawoptions.c_str());

    PC->ClearAlterHs();
    PC->SetBaseH(h_13tev_photon);
    PC->SetBaseHName("13TeV photon");
    PC->AddAlterH(h_14tev_photon);
    PC->AddAlterHName("14TeV photon");
    PC->AddAlterH(h_13tev_lepton);
    PC->AddAlterHName("13TeV lepton");
    PC->AddAlterH(h_14tev_lepton);
    PC->AddAlterHName("14TeV lepton");
    string savename = "CompareShape13142_"+channel;
    PC->SetSaveName(savename);
    PC->Is14TeV(true);
    PC->SetMaxRatio(1.5);
    PC->SetMinRatio(0.5);
    PC->SetXtitle("dR(reco,truth)");
    //PC->DataLumi(36.5);
    PC->Compare();
}
    
