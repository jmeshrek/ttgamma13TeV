#include "TLegend.h"
#include "TFile.h"
#include "TApplication.h"
#include "TSystem.h"
#include "TROOT.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>

#include "Logger.h"
#include "ConfigReader.h"
#include "StringPlayer.h"
#include "WorkspaceMaker.h"

using namespace std;

ConfigReader *rd = NULL;
void AutoComplete(vector<string> &input);

int main(int argc, char* argv[]) {

    // LOGGER
    Logger *lg = new Logger("MAIN");
    lg->Info("s", "Hi, this is the main program~");
    lg->NewLine();

    if (argc < 2) {
	lg->Err("s", "Missing input parameter [config file]");
	return 0;
    }

    // READ config
    string filename = argv[1];
    lg->Info("s", "Reading configuration...");
    rd = new ConfigReader(filename, '_', false);
    rd->Init();

    bool Silent, Debug, ShutUp, NoShapeSys;
    rd->FillValueB("_Options", "Silent", Silent);
    rd->FillValueB("_Options", "ShutUp", ShutUp);
    rd->FillValueB("_Options", "Debug", Debug);
    rd->FillValueB("_Options", "NoShapeSys", NoShapeSys);
    vector<string> ChannelFilter = rd->GetValueAll("_ChannelFilter");
    string BinToUse = rd->GetValue("_BinToUse");
    int bintouse = 999;
    if (BinToUse != "") bintouse = atoi(BinToUse.c_str());

    for (int i = 0; i < argc; i++) {
	if (!strcmp(argv[i],"--shutup")) {
	    ShutUp = true;
   	}
	if (!strcmp(argv[i],"--debug")) {
	    Debug = true;
   	}
    }

    if (ShutUp) lg->ShutUp();

    if (Debug) lg->Info("s", "Reading save configuration");
    string SaveDir = rd->GetValue("_SaveDir");
    vector<string> SaveKeyWords = rd->GetValueAll("_SaveKeyWords");

    if (Debug) lg->Info("s", "Reading observable");
    vector<string> Observable = rd->GetValueAll("_Observable");

    if (Debug) lg->Info("s", "Reading channel");
    vector<string> ChannelNames = rd->GetValueAll("_ChannelNames");
    vector<string> ChannelTitles = rd->GetValueAll("_ChannelTitles");

    if (Debug) lg->Info("s", "Reading data");
    vector<string> DataFiles = rd->GetValueAll("_DataFiles");
    vector<string> DataHists = rd->GetValueAll("_DataHists");

    if (Debug) lg->Info("s", "Reading process");
    vector<vector<string> > Processes = rd->GetAmbiValueAll("_Proc");

    if (Debug) lg->Info("s", "Reading parameters");
    vector<vector<string> > Paras = rd->GetAmbiValueAll("_Para");

    if (Debug) lg->Info("s", "Reading systematics");
    int responsetype = rd->GetValueI("_ResponseType");
    float response_lowthreshold_norm = rd->GetValueF("_NormResponseLowThreshold");
    float response_lowthreshold_shape = rd->GetValueF("_ShapeResponseLowThreshold");
    float response_highthreshold_norm = rd->GetValueF("_NormResponseHighThreshold");
    float response_highthreshold_shape = rd->GetValueF("_ShapeResponseHighThreshold");
    vector<vector<string> > NormSystematics = rd->GetAmbiValueAll("_NormSys");
    vector<vector<string> > ShapeSystematics = rd->GetAmbiValueAll("_ShapeSys");
    //vector<vector<string> > CorrTemps = rd->GetAmbiValueAll("_CorrTemp");
    // ---------------------------------------------------------


    // DIGEST config
    if (Debug) lg->Info("s", "Digesting configuration...");
    string savename = SaveDir; savename += "Workspace";
    for (int i = 0; i < SaveKeyWords.size(); i++) { savename += "_"; savename += SaveKeyWords.at(i);} 
    //char char_resp[10];
    //sprintf(char_resp, "_Resp%d", responsetype);
    //savename += char_resp;
    savename += ".root";

    if (Debug) lg->Info("s", "Digesting observable");
    string ObsTitle = Observable.at(0);
    string ObsUnit = Observable.at(1);
    float ObsLow = atof(Observable.at(2).c_str());
    float ObsHigh = atof(Observable.at(3).c_str());
    int ObsNBin = atoi(Observable.at(4).c_str());
    
    int n_ch = ChannelNames.size();
    
    if (Debug) lg->Info("s", "Digesting data");
    AutoComplete(DataFiles);
    AutoComplete(DataHists);

    if (Debug) lg->Info("s", "Digesting process");
    for (int i = 0; i < Processes.size(); i++) AutoComplete(Processes.at(i));
    int n_proc = Processes.size();
    vector<string> ProcNames;
    vector<string> ProcTitles;
    vector<vector<string > > ProcInChans;
    vector<vector<string > > ProcNorms;
    vector<vector<string > > ProcTempFiles;
    vector<vector<string > > ProcTempHists;
    vector<bool> ProcisSigs;
    for (int i = 0; i < n_proc; i++) {
	if (Debug) lg->Info("ss", "Digesting process -->", Processes.at(i).at(0).c_str());
	if (Debug) lg->Info("s", "Digesting process names");
        ProcNames.push_back(Processes.at(i).at(0));
	if (Debug) lg->Info("s", "Digesting process titles");
        ProcTitles.push_back(replaceChar(Processes.at(i).at(1), '_', ' '));
	if (Debug) lg->Info("s", "Digesting process flags");
        ProcisSigs.push_back(atoi(Processes.at(i).at(2).c_str()));
	if (Debug) lg->Info("s", "Digesting process channels");
	ProcInChans.push_back(SplitToVector(Processes.at(i).at(3)));
	if (Debug) lg->Info("s", "Digesting process normalizations");
	ProcNorms.push_back(SplitToVector(Processes.at(i).at(4)));
	if (Debug) lg->Info("s", "Digesting process files");
	ProcTempFiles.push_back(SplitToVector(Processes.at(i).at(5)));
	if (Debug) lg->Info("s", "Digesting process histograms");
	ProcTempHists.push_back(SplitToVector(Processes.at(i).at(6)));
	AutoComplete(ProcNorms.back());
	AutoComplete(ProcTempFiles.back());
	AutoComplete(ProcTempHists.back());
    }

    if (Debug) lg->Info("s", "Digesting parameters");
    int n_para = Paras.size();
    vector<string> ParaNames;
    vector<string> ParaTitles;
    vector<float> ParaValues;
    vector<float> ParaLows;
    vector<float> ParaHighs;
    vector<int> ParaFixed;
    vector<int> ParaisPoi;
    vector<vector<string> > ParaOnProcs;
    vector<vector<string> > ParaOnChans;
    for (int i = 0; i < n_para; i++) {
	ParaNames.push_back(Paras.at(i).at(0));
	ParaTitles.push_back(replaceChar(Paras.at(i).at(1), '_', ' '));
	ParaValues.push_back(atof(Paras.at(i).at(2).c_str()));
	ParaLows.push_back(atof(Paras.at(i).at(3).c_str()));
	ParaHighs.push_back(atof(Paras.at(i).at(4).c_str()));
	ParaFixed.push_back(atoi(Paras.at(i).at(5).c_str()));
	ParaisPoi.push_back(atoi(Paras.at(i).at(6).c_str()));
	ParaOnProcs.push_back(SplitToVector(Paras.at(i).at(7)));
	ParaOnChans.push_back(SplitToVector(Paras.at(i).at(8)));
    }

    if (Debug) lg->Info("s", "Digesting norm systematics");
    vector<string> NormSysNames;
    vector<string> NormSysTitles;
    vector<vector<string> > NormSysOnProcs;
    vector<vector<string> > NormSysOnChans;
    vector<vector<string> > NormSysResponses;
    int n_norm_sys = NormSystematics.size();
    for (int i = 0; i < n_norm_sys; i++) {
        NormSysNames.push_back(NormSystematics.at(i).at(0));
        NormSysTitles.push_back(replaceChar(NormSystematics.at(i).at(1), '_', ' '));
        NormSysOnProcs.push_back(SplitToVector(NormSystematics.at(i).at(2)));
        NormSysOnChans.push_back(SplitToVector(NormSystematics.at(i).at(3)));
        NormSysResponses.push_back(SplitToVector(NormSystematics.at(i).at(4)));
	AutoComplete(NormSysResponses.back());
    }

    if (Debug) lg->Info("s", "Digesting shape systematics");
    vector<string> ShapeSysNames;
    vector<string> ShapeSysTitles;
    vector<vector<string> > ShapeSysOnProcs;
    vector<vector<string> > ShapeSysOnChans;
    vector<vector<string> > ShapeSysResponses;
    int n_shape_sys = ShapeSystematics.size();
    for (int i = 0; i < n_shape_sys; i++) {
        ShapeSysNames.push_back(ShapeSystematics.at(i).at(0));
        ShapeSysTitles.push_back(replaceChar(ShapeSystematics.at(i).at(1), '_', ' '));
        ShapeSysOnProcs.push_back(SplitToVector(ShapeSystematics.at(i).at(2)));
        ShapeSysOnChans.push_back(SplitToVector(ShapeSystematics.at(i).at(3)));
        ShapeSysResponses.push_back(SplitToVector(ShapeSystematics.at(i).at(4)));
	AutoComplete(ShapeSysResponses.back());
    }
    
    lg->Info("s", "Configuration read.");
    lg->NewLine(); 
    // ---------------------------------------------------------
    
    
    // MAKE Workspace
    if (Debug) lg->Info("s", "Initialize worksapce maker");
    WorkspaceMaker *WM = new WorkspaceMaker();
    if (Silent) WM->Silent();
    if (ShutUp) WM->ShutUp();
    WM->DebugMode(Debug);
    WM->UseNbins(bintouse);

    // observable
    if (Debug) lg->Info("s", "Define observable");
    WM->DefineObs(ObsTitle, ObsUnit, ObsLow, ObsHigh, ObsNBin);
    lg->NewLine();

    // channel
    if (Debug) lg->Info("s", "Add channel");
    for (int i = 0; i < n_ch; i++) {
	if (ChannelFilter.size() != 0) {
	    bool skip = false;
	    for (int j = 0; j < ChannelFilter.size(); j++) {
		if (ChannelNames.at(i) == ChannelFilter.at(j)) {
		    skip = true;
		    break;
		}
	    }
	    if (skip) continue;
	}
	WM->AddChannel(ChannelNames.at(i), replaceChar(ChannelTitles.at(i), '_', ' '));
    }
    WM->FreezeChannels();
    lg->NewLine();

    // data
    if (Debug) lg->Info("s", "Add data");
    for (int i = 0; i < n_ch; i++) {
	TFile* tmpf = new TFile(DataFiles.at(i).c_str());
	if (!tmpf) {
	    lg->Err("s", "Cannot open file -->", DataFiles.at(i).c_str());
	    exit(-1);
	}
	TH1F* tmph = (TH1F*)tmpf->Get(DataHists.at(i).c_str())->Clone();
	if (!tmph) {
	    lg->Err("s", "Missing histogram -->", DataHists.at(i).c_str());
	    exit(-1);
	}
	if (ChannelFilter.size() != 0) {
	    bool skip = false;
	    for (int j = 0; j < ChannelFilter.size(); j++) {
		if (ChannelNames.at(i) == ChannelFilter.at(j)) {
		    skip = true;
		    break;
		}
	    }
	    if (skip) continue;
	}
	WM->SetData(tmph, ChannelNames.at(i));
    }
    lg->NewLine();
    
    // process
    if (Debug) lg->Info("s", "Add process");
    for (int i = 0; i < n_proc; i++) for (int j = 0; j < ProcInChans.at(i).size(); j++) {
	if (ChannelFilter.size() != 0) {
	    bool skip = false;
	    for (int ift = 0; ift < ChannelFilter.size(); ift++) {
		if (ProcInChans.at(i).at(j) == ChannelFilter.at(ift)) {
		    skip = true;
		    break;
		}
	    }
	    if (skip) continue;
	}
	WM->AddProcess(ProcInChans.at(i).at(j), ProcNames.at(i), ProcTitles.at(i), ProcisSigs.at(i));
    }
    WM->FreezeProcesses();
    lg->NewLine();

    if (Debug) lg->Info("s", "Add process parameter and template");
    for (int i = 0; i < n_proc; i++) for (int j = 0; j < ProcInChans.at(i).size(); j++) {
	if (ChannelFilter.size() != 0) {
	    bool skip = false;
	    for (int ift = 0; ift < ChannelFilter.size(); ift++) {
		if (ProcInChans.at(i).at(j) == ChannelFilter.at(ift)) {
		    skip = true;
		    break;
		}
	    }
	    if (skip) continue;
	}

	int index = -1;
	for (int k = 0; k < ChannelNames.size(); k++) {
	    if (ChannelNames.at(k) == ProcInChans.at(i).at(j)) {
		index = k;
		break;
	    }
	}

	if (atof(ProcNorms.at(i).at(j).c_str()) >= 0) 
	    WM->AddParameter(ChannelNames.at(index), ProcNames.at(i), 
	    	     string("Num")+ProcNames.at(i)+ChannelNames.at(index), 
	    	     string("Number of ")+ProcTitles.at(i)+string(" in ")+ChannelTitles.at(index),
	    	     atof(ProcNorms.at(i).at(j).c_str()), 0, 2*atof(ProcNorms.at(i).at(j).c_str()), true, false);
	
	TFile* tmpf = new TFile(ProcTempFiles.at(i).at(j).c_str());
	TH1F* tmph = (TH1F*)tmpf->Get(ProcTempHists.at(i).at(j).c_str())->Clone();
	WM->SetTemplate(tmph, ChannelNames.at(index), ProcNames.at(i));
    }

    // additional parameters
    for (int i = 0; i < n_para; i++) {
	for (int j = 0; j < ParaOnChans.at(i).size(); j++) { 
	    if (ChannelFilter.size() != 0) {
	        bool skip = false;
	        for (int ift = 0; ift < ChannelFilter.size(); ift++) {
		   if (ParaOnChans.at(i).at(j) == ChannelFilter.at(ift)) {
	    	       skip = true;
	    	       break;
	    	   }
	        }
	        if (skip) continue;
	    }
	    for (int k = 0; k < ParaOnProcs.at(i).size(); k++) {
		WM->AddParameter(ParaOnChans.at(i).at(j), ParaOnProcs.at(i).at(k), ParaNames.at(i), ParaTitles.at(i), ParaValues.at(i), ParaLows.at(i), ParaHighs.at(i), ParaFixed.at(i), ParaisPoi.at(i));
	    }
	}
    }
    lg->NewLine();

    // systematics
    WM->SetResponseType(responsetype);
    for (int i = 0; i < n_norm_sys; i++) {
	for (int j = 0; j < NormSysOnChans.at(i).size(); j++) {
	    if (ChannelFilter.size() != 0) {
	        bool skip = false;
	        for (int ift = 0; ift < ChannelFilter.size(); ift++) {
		   if (NormSysOnChans.at(i).at(j) == ChannelFilter.at(ift)) {
	    	       skip = true;
	    	       break;
	    	   }
	        }
	        if (skip) continue;
	    }
            for (int k = 0; k < NormSysOnProcs.at(i).size(); k++) { 
		float response =  atof(NormSysResponses.at(i).at(j*NormSysOnProcs.at(i).size() + k).c_str());
		//if (!(NormSysOnProcs.at(i).at(k) == "QCDGamma" && NormSysNames.at(i).find("SysStat", 0) != string::npos)) {
	    	    if (fabs(response) <= response_lowthreshold_norm) continue;
		    else if (fabs(response) >= response_highthreshold_norm) {
			lg->Err("s", "blowing up norm systematics!");
			lg->Err("sfss", "details ->", NormSysNames.at(i).c_str(), response, NormSysOnChans.at(i).at(j).c_str(), NormSysOnProcs.at(i).at(k).c_str());
			exit(-1);
		    //} else {
		    //    string paraname = "Num"; paraname += NormSysOnProcs.at(i).at(k); paraname += NormSysOnChans.at(i).at(j);
		    //    float paraval = WM->GetParameterVal(NormSysOnChans.at(i).at(j), NormSysOnProcs.at(i).at(k), paraname); 
		    //    if (paraval <= 0) {
		    //        lg->Err("s", "non-positive central value!");
		    //        lg->Err("ssfsf", "para name ->", paraname.c_str(), paraval, NormSysNames.at(i).c_str(), response);
		    //        exit(-1);
		    //    }
		    //    WM->AddSystematics(NormSysNames.at(i), NormSysTitles.at(i)); // Source 
		    //	WM->AddNormResponse2(NormSysNames.at(i), NormSysOnChans.at(i).at(j), NormSysOnProcs.at(i).at(k), response*paraval);
	    	    } else {
	    	    //if (SysNames.at(i).find("Stat",0) != string::npos) WM->AddSystematicsStat(SysNames.at(i), SysTitles.at(i), atof(SysResponses.at(i).at(0).c_str())); // Source 
            	    //else WM->AddSystematics(SysNames.at(i), SysTitles.at(i));
			WM->AddSystematics(NormSysNames.at(i), NormSysTitles.at(i)); // Source 
			WM->AddNormResponse(NormSysNames.at(i), NormSysOnChans.at(i).at(j), NormSysOnProcs.at(i).at(k), response);
		    }
		//} else {
	    	//    WM->AddSystematics(NormSysNames.at(i), NormSysTitles.at(i)); // Source 
		//    WM->AddNormResponse2(NormSysNames.at(i), NormSysOnChans.at(i).at(j), NormSysOnProcs.at(i).at(k), response);
		//}
	    }
	}
    }

    for (int i = 0; i < n_shape_sys; i++) {
	for (int j = 0; j < ShapeSysOnChans.at(i).size(); j++) {
	    if (ChannelFilter.size() != 0) {
	        bool skip = false;
	        for (int ift = 0; ift < ChannelFilter.size(); ift++) {
		   if (ShapeSysOnChans.at(i).at(j) == ChannelFilter.at(ift)) {
	    	       skip = true;
	    	       break;
	    	   }
	        }
	        if (skip) continue;
	    }
	    for (int k = 0; k < ShapeSysOnProcs.at(i).size(); k++) {
		vector<float> response;
	    	float largest = -1;
		string str_response;
	    	for (int l = 0; l < ObsNBin; l++) {
	    	    response.push_back(atof(ShapeSysResponses.at(i).at(j*(ShapeSysOnProcs.at(i).size()*ObsNBin) + k*ObsNBin + l).c_str()));
	    	    if (fabs(response.back()) > largest) largest = fabs(response.back());
		    char tmp[100];
		    sprintf(tmp, "%f,", response.back());
		    str_response += tmp;
	    	}
		//cout << "shape sys of " << ShapeSysOnProcs.at(i).at(k) << " to " << ShapeSysNames.at(i) << " in " << ShapeSysOnChans.at(i).at(j) << endl;
		//for (int l = 0; l < ObsNBin; l++) cout << response.at(l) << ",";
		//cout << endl;
		//if (ShapeSysNames.at(i).find("ShapeSysStat",0) == string::npos) {
	    	    if (fabs(largest) < response_lowthreshold_shape) continue;
	    	    else if (fabs(largest) >= response_highthreshold_shape) {
			lg->Err("s", "blowing up shape systematics!");
			lg->Err("ssss", "details ->", ShapeSysNames.at(i).c_str(), str_response.c_str(), ShapeSysOnChans.at(i).at(j).c_str(), ShapeSysOnProcs.at(i).at(k).c_str());
			exit(-1);
	    	    } else {
			WM->AddSystematics(ShapeSysNames.at(i), ShapeSysTitles.at(i));
			//if (ShapeSysNames.at(i).find("HadronFake",0) != string::npos) WM->AddShapeResponse(ShapeSysNames.at(i), ShapeSysOnChans.at(i).at(j), ShapeSysOnProcs.at(i).at(k), response);
			//else WM->AddShapeResponse(ShapeSysNames.at(i), ShapeSysOnChans.at(i).at(j), ShapeSysOnProcs.at(i).at(k), response);
			WM->AddShapeResponse(ShapeSysNames.at(i), ShapeSysOnChans.at(i).at(j), ShapeSysOnProcs.at(i).at(k), response);
		    }
		//} else {
		//    for (int l = 0; l < ObsNBin; l++) {
		//	char tmpname[200];
		//	sprintf(tmpname, "%sBin%d", ShapeSysNames.at(i).c_str(), l+1);
		//	char tmptitle[200];
		//	sprintf(tmptitle, "%s Bin %d", ShapeSysTitles.at(i).c_str(), l+1);
		//	WM->AddSystematics(tmpname, tmptitle);

		//	vector<float> response2;
		//	for (int m = 0; m < ObsNBin; m++) {
		//	    if (m != l) response2.push_back(0);
		//	    else response2.push_back(response.at(l));
		//	}
		//       	WM->AddShapeResponse(tmpname, ShapeSysOnChans.at(i).at(j), ShapeSysOnProcs.at(i).at(k), response2);
		//    }
		//}
	    }
	}
    }

    lg->NewLine();
    WM->Check();
    
    WM->SetSaveName(savename);
    if (NoShapeSys) WM->ShapeSysOff();
    WM->MakeWorkspace();

    return 0;
}
