#include <TFile.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TPad.h>
#include <TLine.h>
#include <string>
#include <TLatex.h>
#include <TMath.h>
#include <vector>
#include <TGraphErrors.h>
#include <map>
#include <TIterator.h>
#include <TKey.h>
#include <TObject.h>
#include <THStack.h>

#include "AtlasStyle.h"
#include "PlotComparor.h"
#include "StringPlayer.h"
#include "ConfigReader.h"
#include "Logger.h"


int main(int argc, char * argv[]) 
{
    SetAtlasStyle();
    gStyle->SetErrorX(0.0001);

    TFile* f = new TFile("/afs/cern.ch/work/y/yili/private/Analysis/Unfolding_Upgrade/Results_Step1/ph_pt_data_sinlepton_debug.root");
    f->ls();
    TH1F* h1 = (TH1F*)f->Get("LeadPhPtUnfold_14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_TTBar_Upgrade_UR_ejets_Nominalhf");
    TH1F* h2 = (TH1F*)f->Get("LeadPhPtUnfold_14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_TTBar_Upgrade_UR_ejets_Nominalhf_ttbar_ISRFSR_Up");
    TH1F* h3 = (TH1F*)f->Get("LeadPhPtUnfold_14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_TTBar_Upgrade_UR_ejets_Nominalhf_ttbar_ISRFSR_Down");

    PlotComparor* PC = new PlotComparor();
    PC->DrawRatio(true);
    PC->SetSaveDir("plots/Upgrade/");
    //PC->NormToUnit(true);
    string drawoptions = "hist";
    drawoptions += " _e";
    PC->SetDrawOption(drawoptions.c_str());

    {
    	PC->SetChannel("ejets");

    	PC->ClearAlterHs();
    	PC->SetBaseH(h1);
    	PC->SetBaseHName("Nominal");
    	PC->AddAlterH(h2);
    	PC->AddAlterHName("ISR/FSR Up");
    	PC->AddAlterH(h3);
    	PC->AddAlterHName("ISR/FSR Down");
    	string savename = "Upgrade_HFake_ISRFSR_Sys_ejets";
    	PC->SetSaveName(savename);
	PC->IsSimulation(true);
	PC->SetLHCInfo("");
    	PC->Is14TeV(true);
    	PC->SetMaxRatio(1.8);
    	PC->SetMinRatio(0.2);
	PC->SquareCanvas(true);
    	PC->Compare();
    }
}
