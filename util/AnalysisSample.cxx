#include "Logger.h"
#include "SampleAnalyzor.h"
#include "ConfigReader.h"
#include "StringPlayer.h"

#include <time.h>
#include <TH2.h>
#include <TKey.h>
#include <TIterator.h>
#include <TH1.h>
#include <TLorentzVector.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TStopwatch.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

int main(int argc, char * argv[]) {

    TStopwatch t; t.Start();

    Logger *lg = new Logger("MAIN");
    lg->Info("s", "Hi, this is the main programm~");
    lg->NewLine();

    // Fixed tags
    string CME = "13TeV";

    // Data base
    string SampleList = "/afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/config/Samples.txt";
    string VariationList = "/afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/config/Variations.txt";

    // ------------------ READ CONFIG -------------------
    bool Debug = false;
    int NEvt = -1; // event to run over each time
    int NFile = -1; // file to run over each time
    bool DoEGammaHist = false; // fill histograms related to egamma fake study
    bool DoEGammaTF1 = false; // fill histograms related to egamma fake study
    bool DoEGammaReweight = false; // remove non-Z bkg via mass peak ratio weights
    bool DoHFakeReweight = false; // remove non-Z bkg via mass peak ratio weights
    bool DoHFakeReweight025 = false; // remove non-Z bkg via mass peak ratio weights
    bool DoHFakeReweight05 = false; // remove non-Z bkg via mass peak ratio weights
    bool DoHFakeReweight075 = false; // remove non-Z bkg via mass peak ratio weights
    bool DoZGammaReweight = false; // remove non-Z bkg via mass peak ratio weights
    bool AllSys = false; // run over all sys 
    bool UsePS = false; // use PS trigger for QCD
    bool PhMatch = false; // if doing photon truth matching
    bool LpMatch = false; // if doing photon truth matching
    bool DoHist = false; // if filling histograms
    bool DoSysHist = false; // if filling histograms when doing sys run
    bool UseWeight = false; // if applying MC weight
    bool LumiWeight = false; // if applying lumi weight
    bool TTNoTau = false; // remove tau decay channel
    bool NoKfactor = false; // not to use signal kfactor
    bool DoBootStrap = false; // do bootstrap
    bool NoPhJOR = false;
    bool LargeEta = false;
    string Region = ""; // region to run over
    string SubRegion = ""; // subregion to run over
    string Type = ""; // sample type to run over
    string Process = ""; // sample process, only relavent for MC;
    string AddTag = ""; // additional tag on sample name
    string Selection = ""; // selection
    string QCDPara = ""; // parametrisation of QCD
    string PhMatchType = ""; // type of photon truth matching
    string LpMatchType = ""; // type of photon truth matching
    string SaveTag = ""; // additional tag for saving results
    string SaveDir = "/tmp/yili/";
    string Variation = "Nominal"; // variation to run

    // Quick access from command line
    for (int i = 0; i < argc; i++) {
	if (!strcmp(argv[i],"--NEvt")) {
	    NEvt = atoi(string(argv[i+1]).c_str());
   	}
	if (!strcmp(argv[i],"--NFile")) {
	    NFile = atoi(string(argv[i+1]).c_str());
   	}
	if (!strcmp(argv[i],"--EGammaHist")) {
	    DoEGammaHist = true;
   	}
	if (!strcmp(argv[i],"--EGammaTF1")) {
	    DoEGammaTF1 = true;
   	}
	if (!strcmp(argv[i],"--LargeEta")) {
	    LargeEta = true;
   	}
	if (!strcmp(argv[i],"--CME")) {
	    CME = argv[i+1];
   	}
	if (!strcmp(argv[i],"--EGammaReweight")) {
	    DoEGammaReweight = true;
   	}
	if (!strcmp(argv[i],"--HFakeReweight")) {
	    DoHFakeReweight = true;
   	}
	if (!strcmp(argv[i],"--HFakeReweight025")) {
	    DoHFakeReweight025 = true;
   	}
	if (!strcmp(argv[i],"--HFakeReweight05")) {
	    DoHFakeReweight05 = true;
   	}
	if (!strcmp(argv[i],"--HFakeReweight075")) {
	    DoHFakeReweight075 = true;
   	}
	if (!strcmp(argv[i],"--ZGammaReweight")) {
	    DoZGammaReweight = true;
   	}
	if (!strcmp(argv[i],"--AllSys")) {
	    AllSys = true;
   	}
	if (!strcmp(argv[i],"--Region")) {
	    Region = argv[i+1];
   	}
	if (!strcmp(argv[i],"--SubRegion")) {
	    SubRegion = argv[i+1];
   	}
	if (!strcmp(argv[i],"--Type")) {
	    Type = argv[i+1];
   	}
	if (!strcmp(argv[i],"--Process")) {
	    Process = argv[i+1];
   	}
	if (!strcmp(argv[i],"--AddTag")) {
	    AddTag = argv[i+1];
   	}
	if (!strcmp(argv[i],"--Selection")) {
	    Selection = argv[i+1];
   	}
	if (!strcmp(argv[i],"--UsePS")) {
	    UsePS = true;
   	}
	if (!strcmp(argv[i],"--NoPhJOR")) {
	    NoPhJOR = true;
   	}
	if (!strcmp(argv[i],"--NoKfactor")) {
	    NoKfactor = true;
   	}
	if (!strcmp(argv[i],"--QCDPara")) {
	    QCDPara = argv[i+1];
   	}
	if (!strcmp(argv[i],"--PhMatch")) {
	    PhMatch = true;
	    PhMatchType = argv[i+1];
   	}
	if (!strcmp(argv[i],"--LpMatch")) {
	    LpMatch = true;
	    LpMatchType = argv[i+1];
   	}
	if (!strcmp(argv[i],"--DoHist")) {
	    DoHist = true;
   	}
	if (!strcmp(argv[i],"--DoSysHist")) {
	    DoSysHist = true;
   	}
	if (!strcmp(argv[i],"--UseWeight")) {
	    UseWeight = true;
   	}
	if (!strcmp(argv[i],"--LumiWeight")) {
	    LumiWeight = true;
   	}
	if (!strcmp(argv[i],"--TTNoTau")) {
	    TTNoTau = true;
   	}
	if (!strcmp(argv[i],"--SaveTag")) {
	    SaveTag = argv[i+1];
   	}
	if (!strcmp(argv[i],"--SaveDir")) {
	    SaveDir = argv[i+1];
   	}
	if (!strcmp(argv[i],"--Variation")) {
	    Variation = argv[i+1];
   	}
	if (!strcmp(argv[i],"--Debug")) {
	    Debug = true;
   	}
	if (!strcmp(argv[i],"--Debug")) {
	    Debug = true;
   	}
	if (!strcmp(argv[i],"--DoBootStrap")) {
	    DoBootStrap = true;
   	}
    }
    // ---------------------------------------------------------

    // ------------------ CONFIG SUMMARY -------------------
    SampleAnalyzor *SA = new SampleAnalyzor();
    SA->Debug(Debug);
    lg->NewLine();
    lg->Info("s", "============= Config Summary ==============");

    // General
    lg->NewLine(); lg->Info("s", "General");
    SA->RunNEvt(NEvt);
    if (NEvt != -1) lg->Info("sd", "Number of event to run over ->", NEvt);
    SA->RunNFile(NFile);
    if (NFile != -1) lg->Info("sd", "Number of file to run over ->", NFile);

    // Hist
    lg->NewLine(); lg->Info("s", "Histogram Related");
    SA->DoHist(DoHist, DoSysHist);
    if (DoHist) lg->Info("s", "Do histograms!");
    if (DoSysHist) lg->Info("s", "Do histograms even it's sys run!");
    SA->DoEGammaHist(DoEGammaHist);
    if (DoEGammaHist) lg->Info("s", "Do egamma fake histogram");
    SA->DoEGammaTF1(DoEGammaTF1);
    if (DoEGammaTF1) lg->Info("s", "Do egamma fake tf1 with TKDE");

    // Weight
    lg->NewLine(); lg->Info("s", "Weight Related");
    SA->UseEvtWeight(UseWeight);
    SA->UseLumiWeight(LumiWeight);
    if (Type != "Data" && Type != "QCD") {
	if (UseWeight) {
	    lg->Info("s", "Use event weight!");
	    if (LumiWeight) {
		lg->Info("s", "Use luminosity weight!");
	    }
	}
    }
    SA->ReweightEGamma(DoEGammaReweight);
    SA->ReweightHFake(DoHFakeReweight);
    SA->ReweightHFake025(DoHFakeReweight025);
    SA->ReweightHFake05(DoHFakeReweight05);
    SA->ReweightHFake075(DoHFakeReweight075);
    SA->ReweightZGamma(DoZGammaReweight);
    SA->LargeEta();
    if (DoEGammaReweight) lg->Info("s", "Do egamma fake invariant mass reweighting!");
    if (DoHFakeReweight) lg->Info("s", "Do hadronic fake pt reweighting!");
    if (DoHFakeReweight025) lg->Info("s", "Do hadronic fake pt reweighting*0.25!");
    if (DoHFakeReweight05) lg->Info("s", "Do hadronic fake pt reweighting*0.5!");
    if (DoHFakeReweight075) lg->Info("s", "Do hadronic fake pt reweighting*0.75!");
    if (DoZGammaReweight) lg->Info("s", "Do zgamma photon pt reweighting!");
    if (LargeEta) lg->Info("s", "Use 4.5 eta cut!");
    if (NoKfactor) {
	SA->UseKfactor(false);
	lg->Info("s", "Will not use signal kfactor!");
    }
    if (NoPhJOR) {
	SA->NoPhJOR();
	lg->Info("s", "Will not do ph/j overlap removal!");
    }
    if (DoBootStrap) {
	SA->DoBootStrap(true);
	lg->Info("s", "Will use bootstrap!");
    }

    // Channel
    lg->NewLine(); lg->Info("s", "Channel Related");
    if (Region == "" || SubRegion == "") {
	lg->Err("s", "Please check your Region/SubRegion configuration!");
	exit(-1);
    }
    SA->SetRegion(Region);
    SA->SetSubRegion(SubRegion);
    lg->Info("ssss", "Run over channel ->", Region.c_str(), "/", SubRegion.c_str());

    // Sample
    lg->NewLine(); lg->Info("s", "Sample Related");
    if (Type == "") {
	lg->Err("s", "Please check your Type configuration!");
	exit(-1);
    }
    SA->SetType(Type);
    lg->Info("ss", "Sample type ->", Type.c_str());
    if (Type != "Data" && Type != "QCD") {
	if (Process == "") {
    	    lg->Err("s", "Please check your Process configuration!");
    	    exit(-1);
    	}
	SA->SetProcess(Process);
	lg->Info("ss", "Sample process ->", Process.c_str());
    }
    if (Type == "QCD") {
	SA->UsePSTrigger(UsePS);
	if (QCDPara != "") {
	    SA->SetQCDPara(QCDPara);
	}
	lg->Info("ss", "QCD trigger type ->", UsePS ? "PS":"UPS");
	lg->Info("ss", "QCD parametrization ->", QCDPara == "" ? "Try all" : QCDPara.c_str());
    }
    if (AddTag != "") lg->Info("ss", "Additional tag for sample ->", AddTag.c_str());
    vector<string> FilePath;
    string FileTag = "_"; if (Type != "Data" && Type != "QCD") {FileTag += Process; FileTag += "_";} FileTag += Type; FileTag += "_"; FileTag += Region; FileTag += "_"; FileTag += SubRegion; FileTag += "_"; if (AddTag != "") {FileTag += AddTag; FileTag += "_";}
    ifstream ifile_samples;
    ifile_samples.open(SampleList.c_str());
    string line;
    while (getline(ifile_samples, line)) {
        if (line.find("#", 0) != string::npos) continue;
        if (line.size() == 0) continue;
        if (line.substr(0,1) != "_") continue;

	istringstream iss(line);
	vector<string> words;
	for (string word; iss >> word;) {
	    words.push_back(word);
	}
        string tag = words.at(0);
        string file = words.at(1);
    
        if (tag == FileTag) {
	    FilePath.push_back(file);
        }
    }
    if (FilePath.size() == 0) {
        lg->Err("ssss", "Can't find file ->", FileTag.c_str(), "in", SampleList.c_str());
	exit(-1);
    }
    vector<string> files;
    for (int i = 0; i < FilePath.size(); i++) {
	if (FilePath.at(i).find("*",0) == string::npos) {
	    if (NFile != -1) {
		if (files.size() < NFile) {
		    lg->Info("ss", "Samples used -->", FilePath.at(i).c_str());
		    files.push_back(FilePath.at(i));
		}
	    } else {
		lg->Info("ss", "Samples used -->", FilePath.at(i).c_str());
		files.push_back(FilePath.at(i));
	    }
    	} else {
    	    lg->Info("s", "Samples used:");
    	    time_t seconds = time (NULL);
    	    char tmplist[1000];
    	    sprintf(tmplist, "/tmp/yili/log.%ld.tmp", seconds);
    	    char tmprun[1000];
    	    sprintf(tmprun, "ls %s > %s", FilePath.at(i).c_str(), tmplist);
    	    int rtcode = -1;
    	    int n_tried = 0;
    	    int max_try = 100;
    	    while (rtcode != 0 || !std::ifstream(tmplist)) {
    	        rtcode = system(tmprun);
    	        usleep(10000000);
    	        n_tried ++;
    	        if (n_tried > max_try) {
    	    	cout << "Initialization of input list failed!" << endl;
    	    	exit(-1);
    	        }
    	    }
    	    ifstream ifile;
    	    ifile.open(tmplist);
    	    string line;
    	    while (getline(ifile,line)) {
    	       //lg->Info("s", line.c_str());
		if (NFile != -1) {
	    	    if (files.size() < NFile) {
			lg->Info("ss", "Samples used -->", line.c_str());
	    	        files.push_back(line);
	    	    }
	    	} else {
		    lg->Info("ss", "Samples used -->", line.c_str());
		    files.push_back(line);
		}
    	    }
    	    sprintf(tmprun, "rm %s", tmplist);
    	    system(tmprun);
    	}
    }
    SA->AddFile(files);

    // Selection
    lg->NewLine(); lg->Info("s", "Selection Related");
    if (Selection == "") {
	lg->Err("s", "Please check your Selection configuration!");
	exit(-1);
    }
    SA->Selection(Selection);
    lg->Info("ss", "Selection ->", Selection.c_str());
    if (PhMatch) {
	SA->DoPhMatch(true, PhMatchType);
	lg->Info("ss",  "Do photon truth matching ->", PhMatchType.c_str());
    }
    if (LpMatch) {
	SA->DoLpMatch(true, LpMatchType);
	lg->Info("ss",  "Do lepton truth matching ->", LpMatchType.c_str());
    }
    if (TTNoTau) {
	SA->SelectTTBarNoTau(true);
	lg->Info("s",  "Remove ttbar tau decay channel!");
    }

    // Sys
    lg->NewLine(); lg->Info("s", "Systematics Related");
    vector<string> TreeNames;
    vector<string> Variations;
    ifstream ifile_variations;
    ifile_variations.open(VariationList.c_str());
    while (getline(ifile_variations, line)) {
        if (line.size() == 0) continue;
    
        int n1 = line.find_first_of(" ", 0);
        int n2 = line.size();
    
        string treename = line.substr(0, n1);
        string variation = line.substr(n1+1, n2-n1-1);
    
	if (AllSys) {
	    Variations.push_back(variation);
	    if (Type == "Upgrade" && treename == "nominal") treename = "upgrade";
            TreeNames.push_back(treename);
	} else {
	    if (variation == Variation) {
		Variations.push_back(variation);
		if (Type == "Upgrade" && treename == "nominal") treename = "upgrade";
            	TreeNames.push_back(treename);
		break;
	    }
	}
    }
    for (unsigned int i = 0; i < TreeNames.size(); i++) {
        SA->AddTreeName(TreeNames.at(i));
        SA->AddVariation(Variations.at(i));
    }
    if (AllSys) lg->Info("s", "Will run over all systematics from list ->", VariationList.c_str());
    else lg->Info("ss", "Will run over only ->", Variation.c_str());

    // Save
    lg->NewLine(); lg->Info("s", "Save Related");
    string SaveKey;
    SaveKey += CME; SaveKey += "_"; 
    SaveKey += Selection; SaveKey += "_"; 
    if (Type == "Reco" || Type == "Upgrade") {
	if (PhMatch) {
	    SaveKey += "PhMatch_"; SaveKey += PhMatchType; SaveKey += "_";
	}	
	if (LpMatch) {
	    SaveKey += "LpMatch_"; SaveKey += LpMatchType; SaveKey += "_";
	}	
	if (!UseWeight && !LumiWeight) {
    	    SaveKey += "Raw_";
    	} else if (UseWeight && !LumiWeight) {
    	    SaveKey += "NoLumi_";
    	}
    }
    SaveKey += FileTag.substr(1, FileTag.size());
    if (Type == "QCD") {
	if (UsePS) SaveKey += "PS_";
	else SaveKey += "UPS_";
	if (QCDPara != "") {
	    string tmpstr = replaceChar(QCDPara, ':', '_');
	    SaveKey += tmpstr;
	    SaveKey += "_";
	}
    }
    if (DoEGammaReweight) SaveKey += "MlyReweighted_";
    if (DoHFakeReweight) SaveKey += "HFakeReweighted_";
    if (DoHFakeReweight025) SaveKey += "HFakeReweighted025_";
    if (DoHFakeReweight05) SaveKey += "HFakeReweighted05_";
    if (DoHFakeReweight075) SaveKey += "HFakeReweighted075_";
    if (DoZGammaReweight) SaveKey += "ZGammaReweighted_";
    if (TTNoTau) SaveKey += "TTNoTau_";
    if (NoKfactor) SaveKey += "NoKfactor_";
    if (NoPhJOR) SaveKey += "NoPhJOR_";
    if (LargeEta) SaveKey += "LargeEta_";
    string SaveTo = SaveDir; SaveTo += "/"; SaveTo += SaveKey.substr(0, SaveKey.size()); 
    if (AllSys) SaveTo += "AllSys_";
    else {SaveTo += Variation; SaveTo += "_";}
    if (SaveTag != "") {
	SaveTo += SaveTag;
    }
    if (AddTag != "") {
	SaveTo += "_"; SaveTo += AddTag;
    }
    SaveTo += ".root";
    SA->SaveKey(SaveKey);
    SA->SaveTo(SaveTo);
    lg->Info("ss", "Save results to ->", SaveTo.c_str());
    lg->Info("s", "-------------------------------------------");
    lg->NewLine();
    // ---------------------------------------------------------

    // ------------------ RUN -------------------
    SA->Check();
    if (Type == "Upgrade") SA->Loop(true);
    SA->Loop();
    SA->Clear();

    delete SA;

    t.Stop();
    float timeused = t.RealTime();
    float cpuused = t.CpuTime();
    lg->NewLine();
    lg->Info("s", "Finished with time comsuption ==>");
    lg->Info("sfsfsfs", "Real time -->", timeused, "seconds /", timeused/60, "minutes /", timeused/3600, "hours /");
    lg->Info("sfsfsfs", "Cpu time -->", cpuused, "seconds /", cpuused/60, "minutes /", cpuused/3600, "hours /");

    return 1;
}
