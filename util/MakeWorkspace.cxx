#include <TCanvas.h>
#include <TMath.h>
#include <TFile.h>
#include <TH1.h>
#include <TIterator.h>
#include <TKey.h>
#include <iostream>
#include <map>
#include <string>
#include <TString.h>

#include "ConfigReader.h"
#include "StringPlayer.h"
#include "Logger.h"
#include "PlotComparor.h"
#include "AtlasStyle.h"
#include "WorkspaceMaker.h"

using namespace std;

struct Proc{
    TString name;
    double n_norm;
    double e_norm_MCstat;
    TH1F* temp;
};

void Convert(TString name, TH1F* hist, Proc& proc);

int main(int argc, char * argv[]) {

    TString inputfile = "/afs/cern.ch/work/j/jwsmith/public/yichen/SR1_ejets_2017-05-05/ttgamma_SR1_ejets_2017-05-05/Histograms//ttgamma_SR1_ejets_2017-05-05_ph_ptcone20_ejets_histos.root";

    TString vartofit = "ph_ptcone20";
    TString channel = "ejets";
    vector<TString> processes;
    processes.push_back("ttphoton");
    processes.push_back("hadronfakes");
    processes.push_back("electronfakes");
    processes.push_back("Wphoton");
    processes.push_back("Wjets");
    processes.push_back("Other");
    processes.push_back("QCD");
    TString signal = "ttphoton";
    vector<TString> floated;
    floated.push_back("ttphoton");
    floated.push_back("hadronfakes");

    TString hname_Data = vartofit + "_" + channel + "_Data"; 
    TH1F* h_Data;
    {
	TFile* f = new TFile(inputfile);
	h_Data = (TH1F*)f->Get(hname_Data)->Clone();
    }
    vector<TH1F*> h_processes;
    for (int i = 0; i < processes.size(); i++) {
	TFile* f = new TFile(inputfile);
	TString hname = vartofit + "_" + channel + "_" + processes.at(i);
	TH1F* h_tmp = (TH1F*)f->Get(hname)->Clone();
	h_processes.push_back(h_tmp);
    }
    cout << h_Data->Integral() << endl;

    Proc Data; Convert("Data", h_Data, Data);
    vector<Proc> Processes;
    for (int i = 0; i < processes.size(); i++) {
	Proc Process; Convert(processes.at(i), h_processes.at(i), Process);
	Processes.push_back(Process);
	cout << Processes.back().name << " " << Processes.back().n_norm << " +/- " << Processes.back().e_norm_MCstat << endl;
    }

    int binnum = h_Data->GetNbinsX();
    double binlow = h_Data->GetBinLowEdge(1);
    double binhigh = h_Data->GetBinLowEdge(binnum) + h_Data->GetBinWidth(binnum);

    WorkspaceMaker *WM = new WorkspaceMaker();
    WM->Silent();
    WM->ShutUp();
    WM->DefineObs(vartofit.Data(), "GeV", binlow, binhigh, binnum);
    WM->AddChannel(channel.Data(), channel.Data());
    WM->FreezeChannels();
    WM->SetData(Data.temp, channel.Data());
    for (int i = 0; i < processes.size(); i++) {
	TString name = Processes.at(i).name;
	bool issignal = (name == signal);
	WM->AddProcess(channel.Data(), name.Data(), name.Data(), issignal);
    }
    WM->FreezeProcesses();
    for (int i = 0; i < processes.size(); i++) {
	TString name = Processes.at(i).name;
	bool issignal = (name == signal);
	double n_norm = Processes.at(i).n_norm;
	bool isfixed = true;
	for (int j = 0; j < floated.size(); j++) {
	    if (floated.at(j) == name) {
		isfixed = false;
		break;
	    }
	}
	WM->AddParameter(channel.Data(), name.Data(), ("Num"+name+channel).Data(), ("Num"+name+channel).Data(), n_norm, 0, 2*n_norm, isfixed, issignal);
	WM->SetTemplate(Processes.at(i).temp, channel.Data(), name.Data());
    }

    WM->Check();
    TString savename = "workspaces/13TeV_Workspace_" + channel + ".root";
    WM->SetSaveName(savename.Data());
    WM->MakeWorkspace();

}

void Convert(TString name, TH1F* hist, Proc& proc) {
    proc.name = name;
    proc.temp = (TH1F*)hist->Clone();
    double n_overflow = proc.temp->GetBinContent(proc.temp->GetNbinsX()+1);
    double e_overflow = proc.temp->GetBinError(proc.temp->GetNbinsX()+1);
    double n_underflow = proc.temp->GetBinContent(0);
    double e_underflow = proc.temp->GetBinError(0);
    proc.temp->SetBinContent(1, proc.temp->GetBinContent(1) + n_underflow);
    proc.temp->SetBinError(1, sqrt(pow(proc.temp->GetBinError(1),2) + pow(e_underflow,2)));
    proc.temp->SetBinContent(proc.temp->GetNbinsX(), proc.temp->GetBinContent(proc.temp->GetNbinsX()) + n_overflow);
    proc.temp->SetBinError(proc.temp->GetNbinsX(), sqrt(pow(proc.temp->GetBinError(proc.temp->GetNbinsX()),2) + pow(e_overflow,2)));
    proc.n_norm = 0;
    proc.e_norm_MCstat = 0;
    for (int i = 1; i <= hist->GetNbinsX(); i++) {
	proc.n_norm += hist->GetBinContent(i);
	proc.e_norm_MCstat = sqrt(pow(proc.e_norm_MCstat,2) + pow(hist->GetBinError(i),2));
    }
    if (!name.Contains("Data")) proc.temp->Scale(1/proc.temp->Integral());
    proc.temp->SetName(name);
}
