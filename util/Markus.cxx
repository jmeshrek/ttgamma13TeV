#include <TH1F.h>
#include <algorithm>
#include <PlotComparor.h>
#include <TFile.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "StringPlayer.h"
#include <TH2.h>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <TKey.h>
#include <TIterator.h>
#include <TH1.h>
#include <TLorentzVector.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TStopwatch.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <TFile.h>

using namespace std;

int main()
{
    Double_t bins_phpt[] = {20,35,50,65,80,95,110,140,180,300};
    int binnum_phpt = sizeof(bins_phpt)/sizeof(Double_t) - 1;
    Double_t bins_pheta[] = {0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.7, 2.37};
    int binnum_pheta = sizeof(bins_pheta)/sizeof(Double_t) - 1;
    TH1F* h_sl_phpt_LO_Markus_mt = new TH1F("sl_phpt_LO_Markus_mt","sl_phpt_LO_Markus_mt",binnum_phpt, bins_phpt);
    TH1F* h_sl_phpt_LO_Markus_hm = new TH1F("sl_phpt_LO_Markus_hm","sl_phpt_LO_Markus_hm",binnum_phpt, bins_phpt);
    TH1F* h_sl_phpt_LO_Markus_tm = new TH1F("sl_phpt_LO_Markus_tm","sl_phpt_LO_Markus_tm",binnum_phpt, bins_phpt);
    TH1F* h_dl_phpt_LO_Markus_mt = new TH1F("dl_phpt_LO_Markus_mt","dl_phpt_LO_Markus_mt",binnum_phpt, bins_phpt);
    TH1F* h_dl_phpt_LO_Markus_hm = new TH1F("dl_phpt_LO_Markus_hm","dl_phpt_LO_Markus_hm",binnum_phpt, bins_phpt);
    TH1F* h_dl_phpt_LO_Markus_tm = new TH1F("dl_phpt_LO_Markus_tm","dl_phpt_LO_Markus_tm",binnum_phpt, bins_phpt);

    {
    ifstream ifile; ifile.open("../data/ttbP_13TeV_dilept_NN_mt_pTga.dat");
    string line;
    while (getline(ifile,line)) {
	if (line.find("#",0) != string::npos) continue;
	istringstream iss(line);
	double bin_low = -1;
	double n_bin = -1;
	double e_bin = -1;
	int idx = 0;
	int ihist = 0;
	for (string word; iss >> word;) {
	    if (idx == 0) bin_low = atof(word.substr(0,word.size()-1).c_str());
	    if (idx == 1) n_bin = atof(word.substr(0,word.size()-1).c_str());
	    if (idx == 2) e_bin = atof(word.substr(0,word.size()-1).c_str());
	    idx++;
	}
	int ibin = h_sl_phpt_LO_Markus_mt->FindBin(bin_low);
	if (ibin > h_sl_phpt_LO_Markus_mt->GetNbinsX()) ibin = h_sl_phpt_LO_Markus_mt->GetNbinsX();
	h_sl_phpt_LO_Markus_mt->SetBinContent(ibin, h_sl_phpt_LO_Markus_mt->GetBinContent(ibin)+n_bin);
	h_sl_phpt_LO_Markus_mt->SetBinError(ibin, sqrt(pow(h_sl_phpt_LO_Markus_mt->GetBinError(ibin),2)+pow(e_bin,2)));
    }
    }
    {
    ifstream ifile; ifile.open("../data/ttbP_13TeV_dilept_NN_hm_pTga.dat");
    string line;
    while (getline(ifile,line)) {
	if (line.find("#",0) != string::npos) continue;
	istringstream iss(line);
	double bin_low = -1;
	double n_bin = -1;
	double e_bin = -1;
	int idx = 0;
	int ihist = 0;
	for (string word; iss >> word;) {
	    if (idx == 0) bin_low = atof(word.substr(0,word.size()-1).c_str());
	    if (idx == 1) n_bin = atof(word.substr(0,word.size()-1).c_str());
	    if (idx == 2) e_bin = atof(word.substr(0,word.size()-1).c_str());
	    idx++;
	}
	int ibin = h_sl_phpt_LO_Markus_hm->FindBin(bin_low);
	if (ibin > h_sl_phpt_LO_Markus_hm->GetNbinsX()) ibin = h_sl_phpt_LO_Markus_hm->GetNbinsX();
	h_sl_phpt_LO_Markus_hm->SetBinContent(ibin, h_sl_phpt_LO_Markus_hm->GetBinContent(ibin)+n_bin);
	h_sl_phpt_LO_Markus_hm->SetBinError(ibin, sqrt(pow(h_sl_phpt_LO_Markus_hm->GetBinError(ibin),2)+pow(e_bin,2)));
    }
    }
    {
    ifstream ifile; ifile.open("../data/ttbP_13TeV_dilept_NN_tm_pTga.dat");
    string line;
    while (getline(ifile,line)) {
	if (line.find("#",0) != string::npos) continue;
	istringstream iss(line);
	double bin_low = -1;
	double n_bin = -1;
	double e_bin = -1;
	int idx = 0;
	int ihist = 0;
	for (string word; iss >> word;) {
	    if (idx == 0) bin_low = atof(word.substr(0,word.size()-1).c_str());
	    if (idx == 1) n_bin = atof(word.substr(0,word.size()-1).c_str());
	    if (idx == 2) e_bin = atof(word.substr(0,word.size()-1).c_str());
	    idx++;
	}
	int ibin = h_sl_phpt_LO_Markus_tm->FindBin(bin_low);
	if (ibin > h_sl_phpt_LO_Markus_tm->GetNbinsX()) ibin = h_sl_phpt_LO_Markus_tm->GetNbinsX();
	h_sl_phpt_LO_Markus_tm->SetBinContent(ibin, h_sl_phpt_LO_Markus_tm->GetBinContent(ibin)+n_bin);
	h_sl_phpt_LO_Markus_tm->SetBinError(ibin, sqrt(pow(h_sl_phpt_LO_Markus_tm->GetBinError(ibin),2)+pow(e_bin,2)));
    }
    }
    {
    ifstream ifile; ifile.open("../data/ttbP_13TeV_sinlept_NN_mt_pTga.dat");
    string line;
    while (getline(ifile,line)) {
	if (line.find("#",0) != string::npos) continue;
	istringstream iss(line);
	double bin_low = -1;
	double n_bin = -1;
	double e_bin = -1;
	int idx = 0;
	int ihist = 0;
	for (string word; iss >> word;) {
	    if (idx == 0) bin_low = atof(word.substr(0,word.size()-1).c_str());
	    if (idx == 1) n_bin = atof(word.substr(0,word.size()-1).c_str());
	    if (idx == 2) e_bin = atof(word.substr(0,word.size()-1).c_str());
	    idx++;
	}
	int ibin = h_dl_phpt_LO_Markus_mt->FindBin(bin_low);
	if (ibin > h_dl_phpt_LO_Markus_mt->GetNbinsX()) ibin = h_dl_phpt_LO_Markus_mt->GetNbinsX();
	h_dl_phpt_LO_Markus_mt->SetBinContent(ibin, h_dl_phpt_LO_Markus_mt->GetBinContent(ibin)+n_bin);
	h_dl_phpt_LO_Markus_mt->SetBinError(ibin, sqrt(pow(h_dl_phpt_LO_Markus_mt->GetBinError(ibin),2)+pow(e_bin,2)));
    }
    }
    {
    ifstream ifile; ifile.open("../data/ttbP_13TeV_sinlept_NN_hm_pTga.dat");
    string line;
    while (getline(ifile,line)) {
	if (line.find("#",0) != string::npos) continue;
	istringstream iss(line);
	double bin_low = -1;
	double n_bin = -1;
	double e_bin = -1;
	int idx = 0;
	int ihist = 0;
	for (string word; iss >> word;) {
	    if (idx == 0) bin_low = atof(word.substr(0,word.size()-1).c_str());
	    if (idx == 1) n_bin = atof(word.substr(0,word.size()-1).c_str());
	    if (idx == 2) e_bin = atof(word.substr(0,word.size()-1).c_str());
	    idx++;
	}
	int ibin = h_dl_phpt_LO_Markus_hm->FindBin(bin_low);
	if (ibin > h_dl_phpt_LO_Markus_hm->GetNbinsX()) ibin = h_dl_phpt_LO_Markus_hm->GetNbinsX();
	h_dl_phpt_LO_Markus_hm->SetBinContent(ibin, h_dl_phpt_LO_Markus_hm->GetBinContent(ibin)+n_bin);
	h_dl_phpt_LO_Markus_hm->SetBinError(ibin, sqrt(pow(h_dl_phpt_LO_Markus_hm->GetBinError(ibin),2)+pow(e_bin,2)));
    }
    }
    {
    ifstream ifile; ifile.open("../data/ttbP_13TeV_sinlept_NN_tm_pTga.dat");
    string line;
    while (getline(ifile,line)) {
	if (line.find("#",0) != string::npos) continue;
	istringstream iss(line);
	double bin_low = -1;
	double n_bin = -1;
	double e_bin = -1;
	int idx = 0;
	int ihist = 0;
	for (string word; iss >> word;) {
	    if (idx == 0) bin_low = atof(word.substr(0,word.size()-1).c_str());
	    if (idx == 1) n_bin = atof(word.substr(0,word.size()-1).c_str());
	    if (idx == 2) e_bin = atof(word.substr(0,word.size()-1).c_str());
	    idx++;
	}
	int ibin = h_dl_phpt_LO_Markus_tm->FindBin(bin_low);
	if (ibin > h_dl_phpt_LO_Markus_tm->GetNbinsX()) ibin = h_dl_phpt_LO_Markus_tm->GetNbinsX();
	h_dl_phpt_LO_Markus_tm->SetBinContent(ibin, h_dl_phpt_LO_Markus_tm->GetBinContent(ibin)+n_bin);
	h_dl_phpt_LO_Markus_tm->SetBinError(ibin, sqrt(pow(h_dl_phpt_LO_Markus_tm->GetBinError(ibin),2)+pow(e_bin,2)));
    }
    }

    cout << "Markus LO Integral sl phpt hm: " << h_sl_phpt_LO_Markus_hm->Integral() << endl;
    cout << "Markus LO Integral sl phpt mt: " << h_sl_phpt_LO_Markus_mt->Integral() << endl;
    cout << "Markus LO Integral sl phpt tm: " << h_sl_phpt_LO_Markus_tm->Integral() << endl;
    cout << "uncert tm: " << h_sl_phpt_LO_Markus_tm->Integral()/h_sl_phpt_LO_Markus_mt->Integral() - 1 << endl;
    cout << "uncert hm: " << h_sl_phpt_LO_Markus_hm->Integral()/h_sl_phpt_LO_Markus_mt->Integral() - 1 << endl;

    cout << endl;
    cout << "Markus LO Integral dl phpt hm: " << h_dl_phpt_LO_Markus_hm->Integral() << endl;
    cout << "Markus LO Integral dl phpt mt: " << h_dl_phpt_LO_Markus_mt->Integral() << endl;
    cout << "Markus LO Integral dl phpt tm: " << h_dl_phpt_LO_Markus_tm->Integral() << endl;
    cout << "uncert tm: " << h_dl_phpt_LO_Markus_tm->Integral()/h_dl_phpt_LO_Markus_mt->Integral() - 1 << endl;
    cout << "uncert hm: " << h_dl_phpt_LO_Markus_hm->Integral()/h_dl_phpt_LO_Markus_mt->Integral() - 1 << endl;

    //h_phpt_LO_Markus_mt->Scale(16.2/3.24274);
    //h_pheta_LO_Markus_mt->Scale(16.2/162.145);

    //PlotComparor* PC = new PlotComparor();
    //PC->DrawRatio(true);
    //PC->SetSaveDir("plots/");
    //PC->SetChannel("tt->e+mu-");
    //PC->SetRatioYtitle("Ratio");    
    //PC->ShowOverflow(false);

    //PC->ClearAlterHs();
    //h_phpt_LO_Markus_mt->SetLineWidth(3);
    //h_phpt_NLO_Markus_mt->SetLineWidth(3);
    //h_phpt_LO_MG5_dyn->SetLineWidth(3);
    //h_phpt_LO_MG5_dyn_bare->SetLineWidth(3);
    //PC->SetBaseH(h_phpt_LO_MG5_dyn);
    //PC->SetBaseHName("MG5+PS LO dyn");
    //PC->AddAlterH(h_phpt_LO_Markus_mt);
    //PC->AddAlterHName("TOPAZ LO mt");
    //PC->AddAlterH(h_phpt_LO_MG5_dyn_bare);
    //PC->AddAlterHName("MG5 LO dyn");
    //PC->AddAlterH(h_phpt_NLO_Markus_mt);
    //PC->AddAlterHName("TOPAZ NLO mt");
    //PC->SetXtitle("photon pt [GeV]");
    //PC->SetYtitle("xsection [fb]");
    //PC->SetMinRatio(1.6);
    //PC->SetMaxRatio(2.2);
    //PC->UseLogy(true);
    //PC->LogyMin(0.01);
    //PC->SetSaveName("Kfactor_emu_phpt");
    //PC->SetLogo("tty Kfactor");
    //PC->Compare();

    //PC->ClearAlterHs();
    //h_pheta_LO_Markus_mt->SetLineWidth(3);
    //h_pheta_NLO_Markus_mt->SetLineWidth(3);
    //h_pheta_LO_MG5_dyn->SetLineWidth(3);
    //h_pheta_LO_MG5_dyn_bare->SetLineWidth(3);
    //PC->SetBaseH(h_pheta_LO_MG5_dyn);
    //PC->SetBaseHName("MG5+PS LO dyn");
    //PC->AddAlterH(h_pheta_LO_Markus_mt);
    //PC->AddAlterHName("TOPAZ LO mt");
    //PC->AddAlterH(h_pheta_LO_MG5_dyn_bare);
    //PC->AddAlterHName("MG5 LO dyn");
    //PC->AddAlterH(h_pheta_NLO_Markus_mt);
    //PC->AddAlterHName("TOPAZ NLO mt");
    //PC->SetXtitle("photon eta");
    //PC->SetYtitle("xsection [fb]");
    //PC->SetMinRatio(1.6);
    //PC->SetMaxRatio(2.2);
    //PC->UseLogy(true);
    //PC->LogyMin(0.1);
    //PC->SetSaveName("Kfactor_emu_pheta");
    //PC->SetLogo("tty Kfactor");
    //PC->Compare();

    //TFile* fout = new TFile("Kfactor_new.root","recreate");

    //TH1F* h_kfac_phpt = (TH1F*)h_phpt_NLO_Markus_mt->Clone("ll_phpt");
    //h_kfac_phpt->Sumw2();
    //h_kfac_phpt->Divide(h_phpt_LO_MG5_dyn);
    //TH1F* h_kfac_pheta = (TH1F*)h_pheta_NLO_Markus_mt->Clone("ll_pheta");
    //h_kfac_pheta->Sumw2();
    //h_kfac_pheta->Divide(h_pheta_LO_MG5_dyn);

    //h_kfac_phpt->Write();
    //h_kfac_pheta->Write();
    //fout->Close();
    //delete fout;
}
