#include <TFile.h>
#include <TH1.h>
#include <fstream>
#include <iostream>
#include <iomanip>

using namespace std;

void CheckFile ()
{
    string filter1 = "_Signal_";
    string filter2 = "_CR1_";
    bool truth = false;

    cout << "Checking sample list" << endl;
    cout << fixed << setprecision(3);

    ifstream ifile;
    if (truth) ifile.open("../config/Sample_totest_pl.txt");
    else ifile.open("../config/Sample_totest.txt");
    vector<string> tags;
    vector<string> files;
    vector<string> chans;
    string line;
    while (getline(ifile,line)) {
	if (line.find("#", 0) != string::npos) continue;
	if (line.size() == 0) continue;
	if (line.substr(0,1) != "_") continue;
	int n1 = line.find_first_of(" ", 0);
	int n2 = line.find_first_not_of(" ", n1+1);
	int n3 = line.find_first_of(" ", n2+1);

	string tag = line.substr(0, n1);
	string file = line.substr(n2, n3-n2);

	if (tag.find("Data",0) != string::npos) continue;
	if (tag.find("data",0) != string::npos) continue;

	string chan;
	if (tag.find("_ejets_",0) != string::npos) chan = "ejets";
	if (tag.find("_mujets_",0) != string::npos) chan = "mujets";
	if (tag.find("_ee_",0) != string::npos) chan = "ee";
	if (tag.find("_emu_",0) != string::npos) chan = "emu";
	if (tag.find("_mumu_",0) != string::npos) chan = "mumu";

	//if (tag.find("Signal",0) == string::npos) continue;

	if (tag.find("Data",0)!=string::npos) continue;
	if (tag.find("Zeg",0)!=string::npos) continue;
	
	tags.push_back(tag);
	files.push_back(file);
	chans.push_back(chan);
    }

    vector<TH1F*> h_new_signals;
    for (int i = 0; i < files.size(); i++) {
	if (tags.at(i).find("New",0) == string::npos) continue;
	if (filter1 != "") {
	    if (tags.at(i).find(filter1.c_str(),0) == string::npos) continue;
	}
	if (filter2 != "") {
	    if (tags.at(i).find(filter2.c_str(),0) == string::npos) continue;
	}
	//cout << tags.at(i) << " " << filter1 << endl;

	string hname;
	if (!truth) {
	    if (chans.at(i) == "ejets") hname = "ejets_2016/d410389_cutflow_mc_pu_zvtx";
		if (chans.at(i) == "mujets") hname = "mujets_2016/d410389_cutflow_mc_pu_zvtx";
		if (chans.at(i) == "ee") hname = "ee_2016/d410389_cutflow_mc_pu_zvtx";
		if (chans.at(i) == "emu") hname = "emu_2016/d410389_cutflow_mc_pu_zvtx";
		if (chans.at(i) == "mumu") hname = "mumu_2016/d410389_cutflow_mc_pu_zvtx";
	} else {
	    if (chans.at(i) == "ejets") hname = "ejets_2016_pl/cutflow_mc_pu_zvtx";
	    if (chans.at(i) == "mujets") hname = "mujets_2016_pl/cutflow_mc_pu_zvtx";
	    if (chans.at(i) == "ee") hname = "ee_2016_pl/cutflow_mc_pu_zvtx";
	    if (chans.at(i) == "emu") hname = "emu_2016_pl/cutflow_mc_pu_zvtx";
	    if (chans.at(i) == "mumu") hname = "mumu_2016_pl/cutflow_mc_pu_zvtx";
	}

	string file = files.at(i);
	TH1F*h_cf=NULL;

	cout << tags.at(i) << " " << file << " " << hname << endl;

	if (file.find("*",0) == string::npos) {
	   TFile*f=new TFile(file.c_str());
	   //string drname = chans.at(i); drname += "_2016_pl"; TDirectoryFile*dr=(TDirectoryFile*)f->Get(drname.c_str()); dr->ls();
	   h_cf = (TH1F*)f->Get(hname.c_str());
	   if (!h_cf) {
	       cout << "Warn: cannot find " << hname << endl;
	    }
	} else {
	    char run[1000];
	    sprintf(run, "ls %s > log.tmp", file.c_str());
	    system(run);
	    ifstream ifile;
	    ifile.open("log.tmp");
	    string line;
	    while (getline(ifile,line)) {
	        TFile* f = new TFile(line.c_str());
	        if (!f) {
	            cout << "Error: cannot open " << line << endl;
	            exit(-1);
	        }
		//string drname = chans.at(i); drname += "_2016"; TDirectoryFile*dr=(TDirectoryFile*)f->Get(drname.c_str()); dr->ls();
	        TH1F* tmp = (TH1F*)f->Get(hname.c_str());
	        if (!tmp) {
	            cout << "Warn: cannot find " << hname << endl;
		    exit(-1);
	        }
		if (!h_cf) {
		    h_cf = (TH1F*)tmp->Clone();
		} else {
		    h_cf->Add(tmp);
		}
	    }
	    sprintf(run, "rm log.tmp");
	    system(run);
	}
	h_new_signals.push_back(h_cf);
	//for (int i = 0; i < h_cf->GetNbinsX(); i++) {
	//    if (h_cf->GetBinContent(i+1) != 0) {
	//         cout << setw(40) << h_cf->GetXaxis()->GetBinLabel(i+1) << " " << setw(20) << h_cf->GetBinContent(i+1) << endl;
	//    }
	//}
    }
    vector<TH1F*> h_old_signals;
    for (int i = 0; i < files.size(); i++) {
	if (tags.at(i).find("Old",0) == string::npos) continue;
	if (filter1 != "") {
	    if (tags.at(i).find(filter1.c_str(),0) == string::npos) continue;
	}
	if (filter2 != "") {
	    if (tags.at(i).find(filter2.c_str(),0) == string::npos) continue;
	}
	//cout << tags.at(i) << " " << filter1 << endl;

	string hname;
	if (!truth) {
	    if (chans.at(i) == "ejets") hname = "ejets_2016/d410082_cutflow_mc_pu_zvtx";
		if (chans.at(i) == "mujets") hname = "mujets_2016/d410082_cutflow_mc_pu_zvtx";
		if (chans.at(i) == "ee") hname = "ee_2016/d410082_cutflow_mc_pu_zvtx";
		if (chans.at(i) == "emu") hname = "emu_2016/d410082_cutflow_mc_pu_zvtx";
		if (chans.at(i) == "mumu") hname = "mumu_2016/d410082_cutflow_mc_pu_zvtx";
	} else {
	    if (chans.at(i) == "ejets") hname = "ejets_2016_pl/cutflow_mc_pu_zvtx";
	    if (chans.at(i) == "mujets") hname = "mujets_2016_pl/cutflow_mc_pu_zvtx";
	    if (chans.at(i) == "ee") hname = "ee_2016_pl/cutflow_mc_pu_zvtx";
	    if (chans.at(i) == "emu") hname = "emu_2016_pl/cutflow_mc_pu_zvtx";
	    if (chans.at(i) == "mumu") hname = "mumu_2016_pl/cutflow_mc_pu_zvtx";
	}

	string file = files.at(i);
	TH1F*h_cf=NULL;

	cout << tags.at(i) << " " << file << " " << hname << endl;

	if (file.find("*",0) == string::npos) {
	   TFile*f=new TFile(file.c_str());
	   h_cf = (TH1F*)f->Get(hname.c_str());
	   if (!h_cf) {
	       cout << "Warn: cannot find " << hname << endl;
	    }
	} else {
	    char run[1000];
	    sprintf(run, "ls %s > log.tmp", file.c_str());
	    system(run);
	    ifstream ifile;
	    ifile.open("log.tmp");
	    string line;
	    while (getline(ifile,line)) {
	        TFile* f = new TFile(line.c_str());
	        if (!f) {
	            cout << "Error: cannot open " << line << endl;
	            exit(-1);
	        }
	        TH1F* tmp = (TH1F*)f->Get(hname.c_str());
	        if (!tmp) {
	            cout << "Warn: cannot find " << hname << endl;
		    exit(-1);
	        }
		if (!h_cf) {
		    h_cf = (TH1F*)tmp->Clone();
		} else {
		    h_cf->Add(tmp);
		}
	    }
	    sprintf(run, "rm log.tmp");
	    system(run);
	}
	h_old_signals.push_back(h_cf);
	//for (int i = 0; i < h_cf->GetNbinsX(); i++) {
	//    if (h_cf->GetBinContent(i+1) != 0) {
	//         cout << setw(40) << h_cf->GetXaxis()->GetBinLabel(i+1) << " " << setw(20) << h_cf->GetBinContent(i+1) << endl;
	//    }
	//}
    }

    for (int i = 0; i < h_old_signals.size(); i++) {
	double old_norm;
	double new_norm;
	for (int j = 0; j < h_old_signals.at(i)->GetNbinsX(); j++) {
	    if (j == 0) old_norm = h_old_signals.at(i)->GetBinContent(j+1);
	    if (j == 0) new_norm = h_new_signals.at(i)->GetBinContent(j+1);
	    cout << setw(40) << h_old_signals.at(i)->GetXaxis()->GetBinLabel(j+1) << " " << setw(20) << h_old_signals.at(i)->GetBinContent(j+1) << " " << setw(20) << h_new_signals.at(i)->GetBinContent(j+1) << " " << setw(10) << h_old_signals.at(i)->GetBinContent(j+1)/old_norm*100 << " " << setw(10) << h_new_signals.at(i)->GetBinContent(j+1)/new_norm*100 << endl;
	    //cout << setw(40) << h_old_signals.at(i)->GetXaxis()->GetBinLabel(j+1) << " " << setw(20) << h_old_signals.at(i)->GetBinContent(j+1) << " " << setw(20) << h_new_signals.at(i)->GetBinContent(j+1) << " " << setw(10) << h_old_signals.at(i)->GetBinContent(j+1)/old_norm*100 << " " << setw(10) << h_new_signals.at(i)->GetBinContent(j+1)/new_norm*100 << endl;
	}
	cout << endl;
    }
}
