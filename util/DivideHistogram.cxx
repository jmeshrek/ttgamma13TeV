#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TIterator.h>
#include <TKey.h>
#include <TObject.h>
#include <iostream>

using namespace std;

int main ()
{
    vector<string> files1;
    files1.push_back("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF2_zeg_CutZeg_PhMatch18_Lumiweighted_Test5.root");
    files1.push_back("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF2_zeg_CutZegCvt_PhMatch18_Lumiweighted_Test5.root");
    files1.push_back("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF2_zeg_CutZegUnCvt_PhMatch18_Lumiweighted_Test5.root");
    files1.push_back("results/Results_TTBar_Reco_FULL_13TeV_VR1_ejets_CutTTeg_TTBarEE_PhMatch21_Lumiweighted_Test5.root");
    vector<string> files2;
    files2.push_back("results/SubtractedHistograms.root");
    files2.push_back("results/SubtractedHistograms_Cvt.root");
    files2.push_back("results/SubtractedHistograms_UnCvt.root");
    files2.push_back("results/SubtractedHistograms_TTBar.root");
    vector<string> files3;
    files3.push_back("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF2_zeg_CutZeg_PhMatch20_Lumiweighted_Test5.root");
    files3.push_back("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF2_zeg_CutZegCvt_PhMatch20_Lumiweighted_Test5.root");
    files3.push_back("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF2_zeg_CutZegUnCvt_PhMatch20_Lumiweighted_Test5.root");
    files3.push_back("results/Results_TTBar_Reco_FULL_13TeV_VR1_ejets_CutTTeg_TTBarEE_PhMatch23_Lumiweighted_Test5.root");
    vector<string> files4;
    files4.push_back("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF2_zee_CutZee_Lumiweighted_Test5.root");
    files4.push_back("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF2_zee_CutZee_Lumiweighted_Test5.root");
    files4.push_back("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF2_zee_CutZee_Lumiweighted_Test5.root");
    files4.push_back("results/Results_TTBar_Reco_FULL_13TeV_EF2_ttel_ee_CutTTee_TTBarEE_Lumiweighted_Test5.root");

    vector<string> savenames;
    savenames.push_back("results/DividedHistograms.root");
    savenames.push_back("results/DividedHistograms_Cvt.root");
    savenames.push_back("results/DividedHistograms_UnCvt.root");
    savenames.push_back("results/DividedHistograms_TTBar.root");

    for (int i = 0; i < files1.size(); i++) {
	TFile* f1 = new TFile(files1.at(i).c_str());
	TFile* f2 = new TFile(files2.at(i).c_str());
	TFile* f3 = new TFile(files3.at(i).c_str());
	TFile* f4 = new TFile(files4.at(i).c_str());
	string savename = savenames.at(i);

        TIter next(f4->GetListOfKeys());
        TKey* key;
        bool newfile = true;
        while ((key = (TKey*)next())) {
    	TObject* obj = (TObject*)key->ReadObj();
    	if (obj->InheritsFrom(TH1F::Class())) {
    	    TH1F*h4 = (TH1F*)key->ReadObj();
    	    TString hname4 = h4->GetName();
    	    if (hname4.Contains("LeadPh")) continue;
    	    if (hname4.Contains("MPhLep")) continue;
    	    if (hname4.Contains("LeadLep") && hname4.Contains("SubLep")) continue;
    	    TString hname1 = hname4;
    	    TString hname2 = hname4;
    	    TString hname3 = hname4;
    	    hname1.ReplaceAll("zee", "zeg");
    	    hname2.ReplaceAll("zee", "zeg");
    	    hname3.ReplaceAll("zee", "zeg");
    	    hname1.ReplaceAll("EF2_ttel_ee", "VR1_ejets");
    	    hname2.ReplaceAll("EF2_ttel_ee", "VR1_ejets");
    	    hname3.ReplaceAll("EF2_ttel_ee", "VR1_ejets");
    	    hname1.ReplaceAll("SubLep", "LeadPh");
    	    hname2.ReplaceAll("SubLep", "LeadPh");
    	    hname3.ReplaceAll("SubLep", "LeadPh");
    	    hname2.ReplaceAll("ZjetsElEl", "ZjetsElEl_Minus_ZGammajetsElEl");
    	    hname2.ReplaceAll("TTBar", "TTBar_Minus_Signal");
    	    hname1.ReplaceAll("LeadLepLeadPh", "LeadPhLeadLep");
    	    hname2.ReplaceAll("LeadLepLeadPh", "LeadPhLeadLep");
    	    hname3.ReplaceAll("LeadLepLeadPh", "LeadPhLeadLep");
    	    hname1.ReplaceAll("MLepLep", "MPhLep");
    	    hname2.ReplaceAll("MLepLep", "MPhLep");
    	    hname3.ReplaceAll("MLepLep", "MPhLep");
    	    TH1F*h1 = (TH1F*)f1->Get(hname1);
    	    if (!h1) {
    		cout << "Can't find histogram in f1 -> " << hname1 << endl;
    		f1->ls();
    		exit(-1);
    	    }
    	    TH1F*h2 = (TH1F*)f2->Get(hname2);
    	    if (!h2) {
    		cout << "Can't find histogram in f2 -> " << hname2 << endl;
    		f2->ls();
    		exit(-1);
    	    }
    	    TH1F*h3 = (TH1F*)f3->Get(hname3);
    	    if (!h3) {
    		cout << "Can't find histogram in f3 -> " << hname3 << endl;
    		f3->ls();
    		exit(-1);
    	    }
    
    	    h1->Divide(h4);
    	    h2->Divide(h4);
    	    h3->Divide(h4);
    
    	    string option;
    	    if (newfile) option = "recreate";
    	    else option = "update";
    	    TFile*f5= new TFile(savename.c_str(), option.c_str());
    	    newfile = false;
    	    f5->cd();
    	
    	    TString hname5 = "TypeA_" + hname1;
    	    TString hname6 = "TypeDB_" + hname1;
    	    TString hname7 = "TypeC_" + hname1;
    	    h1->Write(hname5);
    	    h2->Write(hname6);
    	    h3->Write(hname7);
    
    	    f5->Close();
    	    delete f5;
    	} else {
    	    TH2F*hh4 = (TH2F*)key->ReadObj();
    	    TString hname4 = hh4->GetName();
    	    if (hname4.Contains("LeadPh")) continue;
    	    if (hname4.Contains("MPhLep")) continue;
    	    if (hname4.Contains("LeadLep") && hname4.Contains("SubLep")) continue;
    	    TString hname1 = hname4;
    	    TString hname2 = hname4;
    	    TString hname3 = hname4;
    	    hname1.ReplaceAll("zee", "zeg");
    	    hname2.ReplaceAll("zee", "zeg");
    	    hname3.ReplaceAll("zee", "zeg");
    	    hname1.ReplaceAll("EF2_ttel_ee", "VR1_ejets");
    	    hname2.ReplaceAll("EF2_ttel_ee", "VR1_ejets");
    	    hname3.ReplaceAll("EF2_ttel_ee", "VR1_ejets");
    	    hname1.ReplaceAll("SubLep", "LeadPh");
    	    hname2.ReplaceAll("SubLep", "LeadPh");
    	    hname3.ReplaceAll("SubLep", "LeadPh");
    	    hname2.ReplaceAll("ZjetsElEl", "ZjetsElEl_Minus_ZGammajetsElEl");
    	    hname2.ReplaceAll("TTBar", "TTBar_Minus_Signal");
    	    hname1.ReplaceAll("LeadLepLeadPh", "LeadPhLeadLep");
    	    hname2.ReplaceAll("LeadLepLeadPh", "LeadPhLeadLep");
    	    hname3.ReplaceAll("LeadLepLeadPh", "LeadPhLeadLep");
    	    hname1.ReplaceAll("MLepLep", "MPhLep");
    	    hname2.ReplaceAll("MLepLep", "MPhLep");
    	    hname3.ReplaceAll("MLepLep", "MPhLep");
    	    TH2F*hh1 = (TH2F*)f1->Get(hname1);
    	    if (!hh1) {
    		cout << "Can't find histogram in f1 -> " << hname1 << endl;
    		f1->ls();
    		exit(-1);
    	    }
    	    TH2F*hh2 = (TH2F*)f2->Get(hname2);
    	    if (!hh2) {
    		cout << "Can't find histogram in f2 -> " << hname2 << endl;
    		f2->ls();
    		exit(-1);
    	    }
    	    TH2F*hh3 = (TH2F*)f3->Get(hname3);
    	    if (!hh3) {
    		cout << "Can't find histogram in f3 -> " << hname3 << endl;
    		f3->ls();
    		exit(-1);
    	    }
    
    	    hh1->Divide(hh4);
    	    hh2->Divide(hh4);
    	    hh3->Divide(hh4);
    
    	    string option;
    	    if (newfile) option = "recreate";
    	    else option = "update";
    	    TFile*f5= new TFile(savename.c_str(), option.c_str());
    	    newfile = false;
    	    f5->cd();
    	
    	    TString hname5 = "TypeA_" + hname1;
    	    TString hname6 = "TypeDB_" + hname1;
    	    TString hname7 = "TypeC_" + hname1;
    	    hh1->Write(hname5);
    	    hh2->Write(hname6);
    	    hh3->Write(hname7);
    	    
    	    f5->Close();
    	    delete f5;
    	}
        }
    }

    return 0;
}
