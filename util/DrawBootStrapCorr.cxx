#include <iostream>
#include <TColor.h>
#include <TLine.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TPad.h>
#include <TMathText.h>
#include "AtlasStyle.h"
#include <TFile.h>
#include <TH1.h>
#include <iomanip>
#include <TH2.h>
#include <string>
#include <sstream>
#include <algorithm>
#include <TCanvas.h>
#include <iterator>
#include <fstream>
#include <vector>

using namespace std;
string channel = "sinlepton";

void set_plot_style()
{
    const Int_t NRGBs = 5;
    const Int_t NCont = 10;

    Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    Double_t green[NRGBs] = { 0.00, 0.00, 0.00, 0.00, 0.00 };
    //Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
}

int main(int argc, char * argv[]) {

    for (int i = 0; i < argc; i++) {
	if (!strcmp(argv[i],"--Channel")) {
	        channel = argv[i+1];
        }
    }

    ifstream ifile;
    string ifilename = "/afs/cern.ch/work/y/yili/private/Analysis/Unfolding/Results_Step6/bootstrap_statcorr_";
    ifilename += channel;
    ifilename += "_normalised.tex";
    ifile.open(ifilename.c_str());
    string line;
    bool start = false;
    vector<vector<double> > corrs;
    while(getline(ifile,line)){
	if (line.find("hline",0) != string::npos) {
	    start = true;
	}
	if (line.find("bottomrule",0) != string::npos) {
	    start = false;
	}

	if (start) {
	    vector<double> corr;
	    istringstream iss(line);
	    vector<string> tokens;
	    copy(istream_iterator<string>(iss),
	         istream_iterator<string>(),
	         back_inserter(tokens));
	    bool firstand = false;
	    for (int i = 0; i < tokens.size(); i++) {
		string token = tokens.at(i);
		if (!firstand && token == "&") {
		    firstand = true;
		    continue;
		}
		
		if (firstand) {
		    if (token == "&") continue;
		    if (token.find("\\",0) != string::npos) {
			token = token.substr(0, token.size()-2);
		    }
		    corr.push_back(atof(token.c_str()));
		}
	    }

	    corrs.push_back(corr);
	}
    }

    vector<string> obs;
    obs.push_back("ph_pt");
    obs.push_back("ph_abseta");
    obs.push_back("dR_ph_lep");
    vector<string> obsname;
    obsname.push_back("p_{T}(#gamma)");
    obsname.push_back("|#eta|(#gamma)");
    if (channel == "sinlepton") {
	obsname.push_back("#DeltaR(#gamma,#it{l})");
    } else {
	obsname.push_back("#DeltaR(#gamma,#it{l})_{min}");
    }
    string chan_name = "ljets";
    if (channel == "dilepton") {
	obs.push_back("dphi_ll");
	obs.push_back("deta_ll");
	obsname.push_back("#Delta#phi(#it{l},#it{l})");
	obsname.push_back("#Delta#eta(#it{l},#it{l})");
	chan_name = "ll";
    }

    TFile* finput = new TFile("/afs/cern.ch/work/y/yili/private/Analysis/Unfolding/external/PL5_nominal.root");
    vector<TH1F*> hists;
    int dimension = 0;
    vector<int> binnum_lo;
    vector<int> binnum_hi;
    for (int i = 0; i < obs.size(); i++) {
	string histname = "h_";
	histname += obs.at(i);
	histname += "Nominal_";
	histname += chan_name;
	histname += "_truth";
	
	TH1F* hist = (TH1F*)finput->Get(histname.c_str());
	if (hist==NULL){
	    cout << histname << endl;
	}
	hists.push_back(hist);
	
	binnum_lo.push_back(dimension+1);
	dimension += hist->GetNbinsX();
	binnum_hi.push_back(dimension);
    }

    TH2F*h2 = new TH2F("corr", "corr", corrs.size(), 0, corrs.size(), corrs.size(), 0, corrs.size());
    int obsindex = 0;
    for (int i = 0; i< corrs.size(); i++){
	if (!((i+1 <= binnum_hi[obsindex]) and (i+1 >= binnum_lo[obsindex]))) {
	    obsindex += 1;
	}
	int binid = i+1 - binnum_lo[obsindex] + 1;

	char binlab[50];
	sprintf(binlab, "%s-%d", obsname.at(obsindex).c_str(), binid);
	h2->GetXaxis()->SetBinLabel(i+1, binlab);
	h2->GetYaxis()->SetBinLabel(i+1, binlab);
	for (int j = 0; j < corrs.at(i).size(); j++){
	    h2->SetBinContent(i+1,j+1,corrs.at(i).at(j));
	}
    }

    SetAtlasStyle();
    TCanvas* c = new TCanvas("c","c",800, 800);
    c->SetRightMargin(0.1);
    c->SetTopMargin(0.18);
    if (channel == "sinlepton") {
	c->SetLeftMargin(0.14);
	c->SetBottomMargin(0.12);
    }
    if (channel == "dilepton") {
	c->SetLeftMargin(0.10);
	c->SetBottomMargin(0.10);
    }
    
    
    //set_plot_style();
    Int_t palette[22];
    palette[0] = kBlue;
    palette[1] = kBlue;
    palette[2] = kBlue;
    palette[3] = kBlue;
    palette[4] = kBlue-4;
    palette[5] = kBlue-4;
    palette[6] = kBlue-4;
    palette[7] = kBlue-4;
    palette[8] = kBlue-7;
    palette[9] = kBlue-7;
    palette[10] = kBlue-7;
    palette[11] = kBlue-7;
    palette[12] = kBlue-9;
    palette[13] = kBlue-9;
    palette[14] = kBlue-9;
    palette[15] = kBlue-9;
    palette[16] = kBlue-10;
    palette[17] = kBlue-10;
    palette[18] = kBlue-10;
    palette[19] = kBlue-10;
    palette[20] = kWhite;
    palette[21] = kWhite;
    palette[22] = kRed-10;
    palette[23] = kRed-10;
    palette[24] = kRed-10;
    palette[25] = kRed-10;
    palette[26] = kRed-9;
    palette[27] = kRed-9;
    palette[28] = kRed-9;
    palette[29] = kRed-9;
    palette[30] = kRed-7;
    palette[31] = kRed-7;
    palette[32] = kRed-7;
    palette[33] = kRed-7;
    palette[34] = kRed-4;
    palette[35] = kRed-4;
    palette[36] = kRed-4;
    palette[37] = kRed-4;
    palette[38] = kRed;
    palette[39] = kRed;
    palette[40] = kRed;
    palette[41] = kRed;
    gStyle->SetPalette(41,palette);
    gStyle->SetNumberContours(40);

    h2->GetZaxis()->SetRangeUser(-1,1);
    h2->GetZaxis()->SetNdivisions(202);
    if (channel == "dilepton") {
	h2->GetXaxis()->SetLabelSize(0.7*h2->GetXaxis()->GetLabelSize());
	h2->GetYaxis()->SetLabelSize(0.7*h2->GetYaxis()->GetLabelSize());
    }
    h2->Draw("colz");
    h2->GetXaxis()->LabelsOption("v");

    TLatex lt; 
    lt.SetNDC();
    lt.SetTextFont(72);
    lt.SetTextSize(0.05);
    lt.DrawLatex(0.05, 0.95, "ATLAS");
    lt.SetTextFont(42);
    lt.DrawLatex(0.21, 0.95, "#sqrt{s} = 13 TeV, 36.1 fb^{-1}");
    lt.SetTextSize(0.04);
    lt.DrawLatex(0.05, 0.90, "Fiducial phase-space statistical correlations");
    if (channel == "sinlepton") {
	lt.DrawLatex(0.05, 0.85, "Normalized cross-sections of single-lepton channel");
    } else {
	lt.DrawLatex(0.05, 0.85, "Normalized cross-sections of dilepton channel");
    }

    //for (int i = 0; i < obs.size()-1; i++) {
//  //    TLine tline(h2->GetXaxis()->GetBinLowEdge(binnum_hi[i])+h2->GetXaxis()->GetBinWidth(binnum_hi[i]), 0, h2->GetXaxis()->GetBinLowEdge(binnum_hi[i])+h2->GetXaxis()->GetBinWidth(binnum_hi[i]), 1);
    //    TLine tline(c->GetUxmin(), 0.5, c->GetUxmax(), 0.5);
    //    tline.SetLineColor(kBlack);
    //    tline.SetLineWidth(3);
    //    tline.Draw("same");
    //}

    string savename = "BSCorr_";
    savename += channel;
    savename += ".eps";
    c->SaveAs(savename.c_str());

}
