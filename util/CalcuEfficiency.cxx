#include <TFile.h>
#include <fstream>
#include <TH2F.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TH1F.h>
#include <iostream>
#include <iomanip>
#include "SumWeightTree.h"
#include <map>

using namespace std;

string run2label = "Run-2 (36 \\ifb)";
string HLlabel = "HL-LHC";

int main(int argc, char * argv[]) 
{
    vector<string> types;
    types.push_back("ScalePdf");
    types.push_back("Upgrade");

    vector<double> cor_13tev;
    vector<double> cor_14tev;
    vector<double> acc_13tev;
    vector<double> acc_14tev;
    vector<double> err_cor_13tev;
    vector<double> err_cor_14tev;
    vector<double> err_acc_13tev;
    vector<double> err_acc_14tev;

    int n_ch = 5;
    string channel_titles[5] = {"\\chejets", "\\chmujets", "\\chee", "\\chemu", "\\chmumu"};
    string channels[7] = {"ejets", "mujets", "ee", "emu", "mumu"};

    for (int it  = 0; it < types.size(); it++) {
	string type = types.at(it);
    
	cout << fixed << setprecision(2);

    	vector<string> filenames;
    	if (type == "Upgrade") {
	    filenames.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v6/user.adurglis.410389.MadGraphPythia8EvtGe.DAOD_TRUTH1.e6540_p3401.v06_output_root/user.adurglis.15814602._000001.output.root");
	    filenames.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v6/user.adurglis.410389.MadGraphPythia8EvtGe.DAOD_TRUTH1.e6540_p3401.v06_output_root/user.adurglis.15814602._000002.output.root");
	    filenames.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v6/user.adurglis.410389.MadGraphPythia8EvtGe.DAOD_TRUTH1.e6540_p3401.v06_output_root/user.adurglis.15814602._000003.output.root");
	    filenames.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v6/user.adurglis.410389.MadGraphPythia8EvtGe.DAOD_TRUTH1.e6540_p3401.v06_output_root/user.adurglis.15814602._000004.output.root");
	    filenames.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v6/user.adurglis.410389.MadGraphPythia8EvtGe.DAOD_TRUTH1.e6540_p3401.v06_output_root/user.adurglis.15814602._000005.output.root");
	    filenames.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v6/user.adurglis.410389.MadGraphPythia8EvtGe.DAOD_TRUTH1.e6540_p3401.v06_output_root/user.adurglis.15814602._000006.output.root");
	    filenames.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v6/user.adurglis.410389.MadGraphPythia8EvtGe.DAOD_TRUTH1.e6540_p3401.v06_output_root/user.adurglis.15814602._000007.output.root");
    	} else {
    	    filenames.push_back("/eos/user/c/caudron2/TtGamma_PL/v010/410389.ttgamma_noallhad.p3152.PL5.001.root");
    	}

    	double nominal_total = 0;
    	for (int i = 0; i < filenames.size(); i++) {
    	    string filename = filenames.at(i);
    	    TFile* f_sw = new TFile(filename.c_str());
    	    TTree* t = (TTree*)f_sw->Get("sumWeights");
    	    SumWeightTree sumWeight(t);
    	    int totentry = sumWeight.GetTotalEntry();
    	    for (int ientry = 0; ientry < totentry; ientry++) {
    	        sumWeight.GetEntry(ientry);
    	        nominal_total += sumWeight.totalEventsWeighted_mc_generator_weights->at(0);
    	    }
    	}

    	string fname = "results/Nominal/Sig_Sys_" + type + ".root";
    	string fname2 = "results/Nominal/Sig_Sys_" + type + "3.root";
    	cout << "File used: " << fname << endl;
    	TFile* f = new TFile(fname.c_str());
    	TFile* f2 = NULL;
	if (type == "Upgrade") {
	   f2 = new TFile(fname2.c_str());
	}

    	// nominal
    	double nominal[7][2] = {};
    	double nominal_err[7][2] = {};
    	string levels[2] = {"reco", "truth"};
    	for (int j = 0; j < n_ch; j++) {
    	    for (int k = 0; k < 2; k++) {
    	       string hname = "N_Nominal_" + channels[j] + "_" + levels[k];
    	       TH1F* h = NULL;
	       if (type != "Upgrade") {
		   h = (TH1F*)f->Get(hname.c_str());
	       } else {
		   if (k == 0) {
			h = (TH1F*)f->Get(hname.c_str());
		    }
		   if (k == 1) {
			h = (TH1F*)f2->Get(hname.c_str());
		    }
	       }
    	       if (!h) {
    	           cout << "can't find " << hname << endl;
    	           f->ls();
    	           exit(-1);
    	        }
    	       nominal[j][k] = h->GetBinContent(1);
    	       nominal_err[j][k] = h->GetBinError(1);
	       cout << hname << " " << h->GetBinContent(1) << endl;
    	    }
    	}
    	double C_nominal[7] = {};
    	double C_nominal_err[7] = {};
    	for (int j = 0; j < n_ch; j++) {
    	    double C = nominal[j][0]/nominal[j][1];
    	    double C_err = C*(nominal_err[j][0]/nominal[j][0]);
    	    //cout << "C: " << nominal_err[j][0] << " " << nominal[j][0] << endl;
    	    C_nominal[j] = C;
    	    C_nominal_err[j] = C_err;
	    C*=100;
	    C_err*=100;
	    if (type == "ScalePdf" && j < 5) {
		cor_13tev.push_back(C);
		err_cor_13tev.push_back(C_err);
	    }
	    if (type == "Upgrade" && j < 5) {
		cor_14tev.push_back(C);
		err_cor_14tev.push_back(C_err);
	    }
    	}
    	double A_nominal[7] = {};
    	double A_nominal_err[7] = {};
    	for (int i = 0; i < n_ch; i++) {
	    double scale = 3000./(nominal_total/(5.4338E-03*10e6));
	    cout << nominal[i][1]*scale << " " << nominal_total*scale << endl;
    	    double A = nominal[i][1]/nominal_total;
    	    double A_err = A*(nominal_err[i][1]/nominal[i][1]);
    	    //cout << "A: " << nominal_err[i][1] << " " << nominal[i][1] << endl;
    	    A_nominal[i] = A;
    	    A_nominal_err[i] = A_err;
	    A*=100;
	    A_err*=100;
	    if (type == "ScalePdf" && i < 5) {
		acc_13tev.push_back(A);
		err_acc_13tev.push_back(A_err);
	    }
	    if (type == "Upgrade" && i < 5) {
		acc_14tev.push_back(A);
		err_acc_14tev.push_back(A_err);
	    }
    	}
    }

    	// print

    	cout << "\\scalebox{0.8}{" << endl;
    	cout << "\\begin{tabular}{c|c|c|c|c|c|c}" << endl;
    	cout << "\\hline" << endl;
    	cout << "\\hline" << endl;
    	cout << setw(20) << "\\multicolumn{2}{c|}{Channel}" << "&" << endl;;
    	for (int i = 0; i < n_ch; i++) {
    	    cout << setw(20) << channel_titles[i];
    	    if (i != n_ch-1) cout << "&";
    	    else cout << "\\\\";
    	}
	cout << endl;
    	cout << "\\hline" << endl;
	cout << "\\multirow{3}{*}{Corr.(\\%)}" << endl;
    	cout << setw(20) << "&" << HLlabel << "&" << endl;
    	for (int i = 0; i < n_ch; i++) {
    	    cout << setw(7) << cor_14tev.at(i) << " $\\pm$ " << setw(6) << err_cor_14tev.at(i);
    	    if (i != n_ch-1) cout << "&";
    	    else cout << "\\\\";
    	}
	cout << endl;
    	cout << setw(20) << "& " << run2label << "&" << endl;
    	for (int i = 0; i < n_ch; i++) {
    	    cout << setw(7) << cor_13tev.at(i) << " $\\pm$ " << setw(6) << err_cor_13tev.at(i);
    	    if (i != n_ch-1) cout << "&";
    	    else cout << "\\\\";
    	}
	cout << endl;
    	cout << setw(20) << "& Ratio &" << endl;
    	for (int i = 0; i < n_ch; i++) {
    	    cout << setw(7) << cor_14tev.at(i)/cor_13tev.at(i); 
    	    if (i != n_ch-1) cout << "&";
    	    else cout << "\\\\";
    	}
	cout << endl;
    	cout << "\\hline" << endl;
	cout << "\\multirow{3}{*}{Acc.(\\%)}" << endl;
    	cout << setw(20) << "&" << HLlabel << "&" << endl;
    	for (int i = 0; i < n_ch; i++) {
    	    cout << setw(7) << acc_14tev.at(i) << " $\\pm$ " << setw(6) << err_acc_14tev.at(i);
    	    if (i != n_ch-1) cout << "&";
    	    else cout << "\\\\";
    	}
	cout << endl;
    	cout << setw(20) << "& " << run2label << "&" << endl;
    	for (int i = 0; i < n_ch; i++) {
    	    cout << setw(7) << acc_13tev.at(i) << " $\\pm$ " << setw(6) << err_acc_13tev.at(i);
    	    if (i != n_ch-1) cout << "&";
    	    else cout << "\\\\";
    	}
	cout << endl;
    	cout << setw(20) << "& Ratio &" << endl;
    	for (int i = 0; i < n_ch; i++) {
    	    cout << setw(7) << acc_14tev.at(i)/acc_13tev.at(i); 
    	    if (i != n_ch-1) cout << "&";
    	    else cout << "\\\\";
    	}
	cout << endl;
    	cout << "\\hline" << endl;
    	cout << "\\hline" << endl;
    	cout << "\\end{tabular}}" << endl;
    	cout << endl;

	ofstream ofile;
	ofile.open("log_eff");
	ofile << fixed << setprecision(2);
    	ofile << "\\scalebox{0.8}{" << endl;
    	ofile << "\\begin{tabular}{c|c|c|c|c|c|c}" << endl;
    	ofile << "\\hline" << endl;
    	ofile << "\\hline" << endl;
    	ofile << setw(20) << "\\multicolumn{2}{c|}{Channel}" << "&" << endl;;
    	for (int i = 0; i < n_ch; i++) {
    	    ofile << setw(20) << channel_titles[i];
    	    if (i != n_ch-1) ofile << "&";
    	    else ofile << "\\\\";
    	}
	ofile << endl;
    	ofile << "\\hline" << endl;
	ofile << "\\multirow{3}{*}{Corr.(\\%)}" << endl;
    	ofile << setw(20) << "&" << HLlabel << "&" << endl;
    	for (int i = 0; i < n_ch; i++) {
    	    ofile << setw(7) << cor_14tev.at(i) << " $\\pm$ " << setw(6) << err_cor_14tev.at(i);
    	    if (i != n_ch-1) ofile << "&";
    	    else ofile << "\\\\";
    	}
	ofile << endl;
    	ofile << setw(20) << "& " << run2label << "&" << endl;
    	for (int i = 0; i < n_ch; i++) {
    	    ofile << setw(7) << cor_13tev.at(i) << " $\\pm$ " << setw(6) << err_cor_13tev.at(i);
    	    if (i != n_ch-1) ofile << "&";
    	    else ofile << "\\\\";
    	}
	ofile << endl;
    	ofile << setw(20) << "& Ratio &" << endl;
    	for (int i = 0; i < n_ch; i++) {
    	    ofile << setw(7) << cor_14tev.at(i)/cor_13tev.at(i); 
    	    if (i != n_ch-1) ofile << "&";
    	    else ofile << "\\\\";
    	}
	ofile << endl;
    	ofile << "\\hline" << endl;
	ofile << "\\multirow{3}{*}{Acc.(\\%)}" << endl;
    	ofile << setw(20) << "&" << HLlabel << "&" << endl;
    	for (int i = 0; i < n_ch; i++) {
    	    ofile << setw(7) << acc_14tev.at(i) << " $\\pm$ " << setw(6) << err_acc_14tev.at(i);
    	    if (i != n_ch-1) ofile << "&";
    	    else ofile << "\\\\";
    	}
	ofile << endl;
    	ofile << setw(20) << "& " << run2label << "&" << endl;
    	for (int i = 0; i < n_ch; i++) {
    	    ofile << setw(7) << acc_13tev.at(i) << " $\\pm$ " << setw(6) << err_acc_13tev.at(i);
    	    if (i != n_ch-1) ofile << "&";
    	    else ofile << "\\\\";
    	}
	ofile << endl;
    	ofile << setw(20) << "& Ratio &" << endl;
    	for (int i = 0; i < n_ch; i++) {
    	    ofile << setw(7) << acc_14tev.at(i)/acc_13tev.at(i); 
    	    if (i != n_ch-1) ofile << "&";
    	    else ofile << "\\\\";
    	}
	ofile << endl;
    	ofile << "\\hline" << endl;
    	ofile << "\\hline" << endl;
    	ofile << "\\end{tabular}}" << endl;
    	ofile << endl;
	ofile.close();

    return 0;
}

string GetName(int i) {
    string namekey;
    if (i == 0) namekey = "Nominal";
    else if (i == 77) namekey = "R1F2";
    else if (i == 33) namekey = "R2F1";
    else if (i == 66) namekey = "R2F05";
    else if (i == 88) namekey = "R05F2";
    else if (i == 22) namekey = "R05F1";
    else if (i == 55) namekey = "R05F05";
    else if (i == 44) namekey = "R1F05";
    else if (i == 99) namekey = "R2F2";
    else if (i == 39) namekey = "PDF25";
    else if (i == 38) namekey = "PDF24";
    else if (i == 41) namekey = "PDF27";
    else if (i == 40) namekey = "PDF26";
    else if (i == 35) namekey = "PDF21";
    else if (i == 34) namekey = "PDF20";
    else if (i == 37) namekey = "PDF23";
    else if (i == 36) namekey = "PDF22";
    else if (i == 43) namekey = "PDF29";
    else if (i == 42) namekey = "PDF28";
    else if (i == 51) namekey = "PDF36";
    else if (i == 49) namekey = "PDF34";
    else if (i == 50) namekey = "PDF35";
    else if (i == 47) namekey = "PDF32";
    else if (i == 48) namekey = "PDF33";
    else if (i == 45) namekey = "PDF30";
    else if (i == 46) namekey = "PDF31";
    else if (i == 53) namekey = "PDF38";
    else if (i == 54) namekey = "PDF39";
    else if (i == 103) namekey = "PDF83";
    else if (i == 102) namekey = "PDF82";
    else if (i == 101) namekey = "PDF81";
    else if (i == 100) namekey = "PDF80";
    else if (i == 107) namekey = "PDF87";
    else if (i == 13) namekey = "PDF100";
    else if (i == 105) namekey = "PDF85";
    else if (i == 104) namekey = "PDF84";
    else if (i == 109) namekey = "PDF89";
    else if (i == 108) namekey = "PDF88";
    else if (i == 87) namekey = "PDF69";
    else if (i == 86) namekey = "PDF68";
    else if (i == 79) namekey = "PDF61";
    else if (i == 78) namekey = "PDF60";
    else if (i == 81) namekey = "PDF63";
    else if (i == 80) namekey = "PDF62";
    else if (i == 83) namekey = "PDF65";
    else if (i == 82) namekey = "PDF64";
    else if (i == 85) namekey = "PDF67";
    else if (i == 84) namekey = "PDF66";
    else if (i == 20) namekey = "PDF8";
    else if (i == 21) namekey = "PDF9";
    else if (i == 18) namekey = "PDF6";
    else if (i == 19) namekey = "PDF7";
    else if (i == 16) namekey = "PDF4";
    else if (i == 17) namekey = "PDF5";
    else if (i == 14) namekey = "PDF2";
    else if (i == 15) namekey = "PDF3";
    else if (i == 12) namekey = "PDF1";
    else if (i == 6) namekey = "PDF94";
    else if (i == 7) namekey = "PDF95";
    else if (i == 8) namekey = "PDF96";
    else if (i == 9) namekey = "PDF97";
    else if (i == 2) namekey = "PDF90";
    else if (i == 3) namekey = "PDF91";
    else if (i == 4) namekey = "PDF92";
    else if (i == 5) namekey = "PDF93";
    else if (i == 10) namekey = "PDF98";
    else if (i == 11) namekey = "PDF99";
    else if (i == 31) namekey = "PDF18";
    else if (i == 32) namekey = "PDF19";
    else if (i == 27) namekey = "PDF14";
    else if (i == 28) namekey = "PDF15";
    else if (i == 29) namekey = "PDF16";
    else if (i == 30) namekey = "PDF17";
    else if (i == 23) namekey = "PDF10";
    else if (i == 24) namekey = "PDF11";
    else if (i == 25) namekey = "PDF12";
    else if (i == 26) namekey = "PDF13";
    else if (i == 97) namekey = "PDF78";
    else if (i == 98) namekey = "PDF79";
    else if (i == 91) namekey = "PDF72";
    else if (i == 92) namekey = "PDF73";
    else if (i == 89) namekey = "PDF70";
    else if (i == 90) namekey = "PDF71";
    else if (i == 95) namekey = "PDF76";
    else if (i == 96) namekey = "PDF77";
    else if (i == 93) namekey = "PDF74";
    else if (i == 94) namekey = "PDF75";
    else if (i == 106) namekey = "PDF86";
    else if (i == 52) namekey = "PDF37";
    else if (i == 65) namekey = "PDF49";
    else if (i == 64) namekey = "PDF48";
    else if (i == 63) namekey = "PDF47";
    else if (i == 62) namekey = "PDF46";
    else if (i == 61) namekey = "PDF45";
    else if (i == 60) namekey = "PDF44";
    else if (i == 59) namekey = "PDF43";
    else if (i == 58) namekey = "PDF42";
    else if (i == 57) namekey = "PDF41";
    else if (i == 56) namekey = "PDF40";
    else if (i == 67) namekey = "PDF50";
    else if (i == 68) namekey = "PDF51";
    else if (i == 69) namekey = "PDF52";
    else if (i == 70) namekey = "PDF53";
    else if (i == 71) namekey = "PDF54";
    else if (i == 72) namekey = "PDF55";
    else if (i == 73) namekey = "PDF56";
    else if (i == 74) namekey = "PDF57";
    else if (i == 75) namekey = "PDF58";
    else if (i == 76) namekey = "PDF59";
    else namekey = "Unknown";

    return namekey;
}
