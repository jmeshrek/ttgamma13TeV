#include "PlotComparor.h"
#include <TFile.h>
#include <TH1F.h>
#include <TString.h>

using namespace std;

int main()
{
    //TCanvas*c = new TCanvas("c","c",800,600);
    //c->SetLogy(true);
    //c->Divide(3,2);
    //int subc = 0;

    PlotComparor* PC = new PlotComparor();
    PC->DrawRatio(true);
    PC->SetMinRatio(0.6);
    PC->SetMaxRatio(1.4);
    PC->SetSaveDir("plots/");
    PC->SetDrawOption("hist_e");
    TString channel;
    
    vector<TString> channels;
    channels.push_back("ejets");
    channels.push_back("mujets");
    channels.push_back("ee");
    channels.push_back("emu");
    channels.push_back("mumu");
    for (int i = 0; i < channels.size(); i++)
    {
	TString channel = channels.at(i);
	TFile* f_new = new TFile("results/Nominal//13TeV_CutSR_PhMatch_TruePh_Signal_Reco_CR1_" + channel + "_Nominal_Final02.root");
    	TFile* f_old = new TFile("results/Nominal//13TeV_CutSR_PhMatch_TruePh_OldSignal_Reco_CR1_" + channel + "_Nominal_Final02.root");
	TString name_new = "Cutflow_13TeV_CutSR_PhMatch_TruePh_Signal_Reco_CR1_" + channel + "_Nominal";
	TString name_old = "Cutflow_13TeV_CutSR_PhMatch_TruePh_OldSignal_Reco_CR1_" + channel + "_Nominal";
    	TH1F*h_new = (TH1F*)f_new->Get(name_new);
    	TH1F*h_old = (TH1F*)f_old->Get(name_old);
	PC->ClearAlterHs();
	PC->SetBaseH(h_old);
	PC->SetBaseHName("Old");
        PC->AddAlterH(h_new);
	PC->AddAlterHName("New");
	PC->SetSaveName(("Compare_CF_Signal_" + channel).Data());
	PC->SetChannel(channel.Data());
	PC->DoCutbins(0.5, 15.5);
	PC->NormToUnit(false);
	PC->VerticleXLabel(true);
	PC->Compare();
    }
}
