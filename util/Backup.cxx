#include <TFile.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TPad.h>
#include <TLine.h>
#include <string>
#include <TLatex.h>
#include <TMath.h>
#include <vector>
#include <TGraphErrors.h>
#include <map>
#include <THStack.h>

#include "AtlasStyle.h"
#include "PlotComparor.h"
#include "ConfigReader.h"
#include "StringPlayer.h"
#include "Logger.h"

using namespace std;

int main(int argc, char * argv[]) {
    Logger *lg = new Logger("MAIN");
    lg->Info("s", "Hi, this is the main programm~");

    string filename;
    if (argc < 2) {
	lg->Err("s", "Missing input parameter [config file]");
	lg->Info("s", "Will use the default ../config/13TeV_compare_shape.cfg");
	filename = "../config/13TeV_compare_shape.cfg";
    } else {
	filename = argv[1];
    }

    ConfigReader* rd = new ConfigReader(filename, '_', false);
    rd->Init();

    bool DoNorm, ErrorBar, DrawRatio, Debug;
    rd->FillValueB("_Options", "DoNorm", DoNorm);
    rd->FillValueB("_Options", "ErrorBar", ErrorBar);
    rd->FillValueB("_Options", "DrawRatio", DrawRatio);
    rd->FillValueB("_Options", "Debug", Debug);
    string channel = rd->GetValue("_Channel");
    string save = rd->GetValue("_SaveName");
    float ratiolo = rd->GetValueF("_RatioLo");
    float ratiohi = rd->GetValueF("_RatioHi");

    string xtitle = rd->GetValue("_XTitle");
    vector<string> filelist = rd->GetValueAll("_FileList");
    vector<string> histlist = rd->GetValueAll("_HistList");
    vector<string> legend = rd->GetValueAll("_Legend");
    AutoComplete(filelist);
    AutoComplete(histlist);
    AutoComplete(legend);

    for (int i = 0; i < argc; i++) {
	if (!strcmp(argv[i],"--norm")) {
	    DoNorm = true;
   	}
	if (!strcmp(argv[i],"--ratio")) {
	    DrawRatio = true;
   	}
    }

    PlotComparor* PC = new PlotComparor();
    PC->DrawRatio(DrawRatio);
    PC->SetMinRatio(ratiolo);
    PC->SetMaxRatio(ratiohi);
    PC->SetSaveDir("plots/");
    PC->NormToUnit(DoNorm);
    string drawoptions = "hist";
    if (ErrorBar) {
	drawoptions += " _e";
    }
    PC->SetDrawOption(drawoptions.c_str());

    PC->ClearAlterHs();
    
    vector<TH1F*> hs;
    for (int i = 0; i < filelist.size(); i++) {
        TFile*f = new TFile(filelist.at(i).c_str());
	if (!f) {
	    cout << "Err: cannot open file -> " << filelist.at(i) << endl;
	    exit(-1);
	}
	if (Debug) f->ls();
        TH1F* h = (TH1F*)f->Get(histlist.at(i).c_str());
        if (!h) {
            cout << "Err: cannot find histogram --> " << histlist.at(i) << " in " << filelist.at(i) << endl;
            exit(-1);
        }
        hs.push_back(h);
    }
    
    PC->SetBaseH(hs.at(0));
    PC->SetBaseHName(replaceChar(legend.at(0), '_', ' '));
    for (int k = 1; k < hs.size(); k++) {
        PC->AddAlterH(hs.at(k));
        PC->AddAlterHName(replaceChar(legend.at(k), '_', ' '));
    }
    
    string savename = "CompareShape_";
    savename += save;
    if (DoNorm) {
	savename += "_Norm";
    }

    PC->SetSaveName(savename);
    if (channel != "") PC->SetChannel(replaceChar(channel, '_', ' '));
    PC->SetXtitle(replaceChar(xtitle, '_', ' '));
    PC->SetRatioYtitle("Ratio");    
    PC->Is13TeV(true);
    PC->DataLumi(36.5);
    PC->Compare();
    
    return 0;
}

