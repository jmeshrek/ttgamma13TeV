#include<TH1F.h>
#include <RooFitResult.h>
#include<TLatex.h>
#include<TLegend.h>
#include<TH2F.h>
#include<TString.h>
#include<TPaveStats.h>
#include<TFile.h>
#include<TROOT.h>
#include<TStyle.h>
#include<RooRealVar.h>
#include<RooPlot.h>
#include<TF1.h>
#include<TCanvas.h>
#include <RooFit.h>
#include <RooRealVar.h>
#include <RooGaussian.h>
#include <RooCBShape.h>
#include <RooWorkspace.h>
#include <RooDataSet.h>
#include "RooPolynomial.h"
#include <RooRealVar.h>
#include <RooArgSet.h>
#include <RooWorkspace.h>
#include <RooDataHist.h>
#include <RooHist.h>
#include <RooHistPdf.h>
#include <RooProduct.h>
#include <RooCurve.h>
#include <RooAddPdf.h>
#include <RooAbsPdf.h>
#include <RooAddition.h>
#include <RooProdPdf.h>
#include <RooGaussModel.h>
#include <RooBinning.h>
#include <RooGenericPdf.h>
#include <RooExtendPdf.h>
#include <RooPoisson.h>
#include <RooLognormal.h>
#include <RooCategory.h>
#include <RooSimultaneous.h>

#include <RooStats/RooStatsUtils.h>
#include <RooStats/ModelConfig.h>
#include <RooStats/MCMCInterval.h>
#include <RooStats/MCMCIntervalPlot.h>

#include"Logger.h"
#include"PlotComparor.h"
#include "StringPlayer.h"
#include "Logger.h"
#include "ConfigReader.h"

using namespace std;
using namespace RooFit;
using namespace RooStats;

double GetTotalUncert(TH1F*h);
void FitData(double&cen, double&err, TH1F*h, string region, string tag = "");
TH1F* Convert2Dto1D(TH2F* h2, string var, int ib, string region, string subregion, string cut);

struct EGammaResults {
    TH1F* h_mass_zeg;
    TH1F* h_mass_zee;
    double n_zeg;
    double e_zeg;
    double n_zee;
    double e_zee;
    double n_fr;
    double e_fr;
};

EGammaResults GetEGammaResults (TFile* f_mass_zeg, TFile* f_mass_zee, string ptbin, string etabin, bool isdata, bool reverse, TH1F* h_sig_zeg = NULL, TH1F* h_sig_zee = NULL, bool cplus = false);
EGammaResults GetEGammaResultsTTBar(TFile* f_zeg, TFile* f_zee, string ptbin, string etabin, bool reverse, bool isttee, bool cplus = false);
EGammaResults SumEGammaResults (vector<EGammaResults>);
EGammaResults SubtractEGammaResults (EGammaResults A, EGammaResults B);
EGammaResults DivideEGammaResults (EGammaResults n1, EGammaResults n2);
void PrintEGammaResults(EGammaResults r);
void PrintEGammaResults(EGammaResults r, ofstream &ofile);
void FitMassPeak(double &n, double &e, TH1F* h, string ptbin, string etabin, bool reverse, TH1F* h_sig = NULL, bool isdata = false);
void FitMassPeakMC(double &n_z, double &e_z, TH1F* h, string ptbin, string etabin, bool reverse);
void CalcuRatio(double &nr, double &er, double n1, double e1, double n2, double e2);

// Use MC template instead of CB for signal
bool MCTemp = false;
// Whether to use same mean for CB (signal) and Gaussian (bkg)
bool DiffMean = false;
// Expand fit range
bool RExpand = false;
// Shrink fit range
bool RShrink = false;
// Reverse tag/probe
bool DoReverse = false;
// Use Gaussian as bkg ? Otherwise polynomial
bool BkgGaus = true;
// Use ZGamma MC to simulate type C? Otherwise use Zjets MC
bool ZGamma = false;
string version;
string option_zeg = "recreate";
string option_zee = "recreate";

vector<double> m_egammaptbins_lo;
vector<double> m_egammaptbins_hi;
vector<double> m_egammaetabins_lo;
vector<double> m_egammaetabins_hi;
vector<double> m_egammapt2dbins_lo;
vector<double> m_egammapt2dbins_hi;
vector<double> m_egammaeta2dbins_lo;
vector<double> m_egammaeta2dbins_hi;

void EGammaStudy(TH1F* h_zeg_MC_true_fake, TH1F* h_zeg_MC_fake_fake, TH1F* h_zeg_data, TH1F* h_zee_MC, TH1F* h_zee_data);

int main(int argc, char * argv[]) {

    m_egammaptbins_lo.push_back(20.);
    m_egammaptbins_lo.push_back(25.);
    m_egammaptbins_lo.push_back(30.);
    m_egammaptbins_lo.push_back(35.);
    m_egammaptbins_lo.push_back(40.);
    m_egammaptbins_lo.push_back(45.);
    m_egammaptbins_lo.push_back(50.);
    m_egammaptbins_lo.push_back(55.);
    m_egammaptbins_lo.push_back(60.);
    m_egammaptbins_lo.push_back(65.);
    m_egammaptbins_lo.push_back(70.);
    m_egammaptbins_hi.push_back(25.);
    m_egammaptbins_hi.push_back(30.);
    m_egammaptbins_hi.push_back(35.);
    m_egammaptbins_hi.push_back(40.);
    m_egammaptbins_hi.push_back(45.);
    m_egammaptbins_hi.push_back(50.);
    m_egammaptbins_hi.push_back(55.);
    m_egammaptbins_hi.push_back(60.);
    m_egammaptbins_hi.push_back(65.);
    m_egammaptbins_hi.push_back(70.);
    m_egammaptbins_hi.push_back(99999.);
    
    m_egammaetabins_lo.push_back(0.);
    m_egammaetabins_lo.push_back(0.2);
    m_egammaetabins_lo.push_back(0.4);
    m_egammaetabins_lo.push_back(0.6);
    m_egammaetabins_lo.push_back(0.8);
    m_egammaetabins_lo.push_back(1.0);
    m_egammaetabins_lo.push_back(1.2);
    m_egammaetabins_lo.push_back(1.37);
    m_egammaetabins_lo.push_back(1.52);
    m_egammaetabins_lo.push_back(1.7);
    m_egammaetabins_lo.push_back(1.9);
    m_egammaetabins_lo.push_back(2.1);
    m_egammaetabins_hi.push_back(0.2);
    m_egammaetabins_hi.push_back(0.4);
    m_egammaetabins_hi.push_back(0.6);
    m_egammaetabins_hi.push_back(0.8);
    m_egammaetabins_hi.push_back(1.0);
    m_egammaetabins_hi.push_back(1.2);
    m_egammaetabins_hi.push_back(1.37);
    m_egammaetabins_hi.push_back(1.52);
    m_egammaetabins_hi.push_back(1.7);
    m_egammaetabins_hi.push_back(1.9);
    m_egammaetabins_hi.push_back(2.1);
    m_egammaetabins_hi.push_back(2.37);

    m_egammapt2dbins_lo.push_back(20.);
    m_egammapt2dbins_lo.push_back(30.);
    m_egammapt2dbins_lo.push_back(40.);
    m_egammapt2dbins_lo.push_back(50.);
    m_egammapt2dbins_lo.push_back(60.);
    m_egammapt2dbins_lo.push_back(70.);
    m_egammapt2dbins_hi.push_back(30.);
    m_egammapt2dbins_hi.push_back(40.);
    m_egammapt2dbins_hi.push_back(50.);
    m_egammapt2dbins_hi.push_back(60.);
    m_egammapt2dbins_hi.push_back(70.);
    m_egammapt2dbins_hi.push_back(99999.);

    m_egammaeta2dbins_lo.push_back(0.);
    m_egammaeta2dbins_lo.push_back(0.4);
    m_egammaeta2dbins_lo.push_back(0.8);
    m_egammaeta2dbins_lo.push_back(1.37);
    m_egammaeta2dbins_lo.push_back(1.52);
    m_egammaeta2dbins_lo.push_back(1.9);
    m_egammaeta2dbins_hi.push_back(0.4);
    m_egammaeta2dbins_hi.push_back(0.8);
    m_egammaeta2dbins_hi.push_back(1.37);
    m_egammaeta2dbins_hi.push_back(1.52);
    m_egammaeta2dbins_hi.push_back(1.9);
    m_egammaeta2dbins_hi.push_back(2.37);

    Logger *lg = new Logger("MAIN");
    lg->Info("s", "Hi, this is the main programm~");
    lg->NewLine();

    string filename;
    for (int i = 0; i < argc; i++) {
	if (!strcmp(argv[i],"--config")) {
	    filename = argv[i+1];
   	}
    }
    if (filename == "") {
	lg->Info("s", "Will use default config file --> ../config/13TeV_egammafake_study.cfg");
	filename = "../config/13TeV_egammafake_study.cfg" ;
    }

    ConfigReader* rd = new ConfigReader(filename, '_', false);
    rd->Init();

    bool DoPt, DoEta, DoPtEta, DoEGammaCali1, DoEGammaCali2, DoEGammaCali3, DoEGammaCali4, DoData, DoMC, ZGamma;
    rd->FillValueB("_Options", "MCTemp", MCTemp);
    rd->FillValueB("_Options", "DiffMean", DiffMean);
    rd->FillValueB("_Options", "Reverse", DoReverse);
    rd->FillValueB("_Options", "DoMC", DoMC);
    rd->FillValueB("_Options", "DoData", DoData);
    rd->FillValueB("_Options", "DoPt", DoPt);
    rd->FillValueB("_Options", "DoEta", DoEta);
    rd->FillValueB("_Options", "DoPtEta", DoPtEta);
    rd->FillValueB("_Options", "RShrink", RShrink);
    rd->FillValueB("_Options", "RExpand", RExpand);
    rd->FillValueB("_Options", "BkgGaus", BkgGaus);
    rd->FillValueB("_Options", "ZGamma", ZGamma);
    rd->FillValueB("_Options", "DoEGammaCali1", DoEGammaCali1);
    rd->FillValueB("_Options", "DoEGammaCali2", DoEGammaCali2);
    rd->FillValueB("_Options", "DoEGammaCali3", DoEGammaCali3);
    rd->FillValueB("_Options", "DoEGammaCali4", DoEGammaCali4);
    version = rd->GetValue("_Version");
    int precision = rd->GetValueI("_Precision");

    for (int i = 0; i < argc; i++) {
	if (!strcmp(argv[i],"--pt")) {
	    DoPt = true;
   	}
	if (!strcmp(argv[i],"--eta")) {
	    DoEta = true;
   	}
	if (!strcmp(argv[i],"--pteta")) {
	    DoPtEta = true;
   	}
	if (!strcmp(argv[i],"--gaus")) {
	    BkgGaus = true;
   	}
	if (!strcmp(argv[i],"--zgamma")) {
	    ZGamma = true;
   	}
	if (!strcmp(argv[i],"--expand")) {
	    RExpand = true;
   	}
	if (!strcmp(argv[i],"--shrink")) {
	    RShrink = true;
   	}
	if (!strcmp(argv[i],"--reverse")) {
	    DoReverse = true;
   	}
	if (!strcmp(argv[i],"--mctemp")) {
	    MCTemp = true;
   	}
	if (!strcmp(argv[i],"--diffmean")) {
	    DiffMean = true;
   	}
    }

    // build a module: input histogram -> out put FR + intermediate results
    TString vtag = version;
    TString calitag = "";
    if (DoEGammaCali1) calitag = "_EGammaCali1";
    if (DoEGammaCali2) calitag = "_EGammaCali2";
    if (DoEGammaCali3) calitag = "_EGammaCali3";
    if (DoEGammaCali4) calitag = "_EGammaCali4";
    TString rtag = "";
    if (DoReverse) rtag = "Reverse";

    TFile* f_zeg_data= NULL;
    TFile* f_zee_data= NULL;
    TFile* f_zeg_MC_TypeA= NULL;
    TFile* f_zeg_MC_TypeB= NULL;
    TFile* f_zeg_MC_TypeC= NULL;
    TFile* f_zeg_MC_TypeCp= NULL;
    TFile* f_zeg_MC_TypeD= NULL;
    TFile* f_tteg_MC_TypeA= NULL;
    TFile* f_tteg_MC_TypeB= NULL;
    TFile* f_tteg_MC_TypeC= NULL;
    TFile* f_tteg_MC_TypeCp= NULL;
    TFile* f_tteg_MC_TypeD= NULL;
    TFile* f_ttmg_MC_TypeA= NULL;
    TFile* f_ttmg_MC_TypeB= NULL;
    TFile* f_ttmg_MC_TypeC= NULL;
    TFile* f_ttmg_MC_TypeCp= NULL;
    TFile* f_ttmg_MC_TypeD= NULL;
    TFile* f_zee_MC= NULL;
    TFile* f_ttee_MC= NULL;
    TFile* f_ttme_MC= NULL;

    f_zeg_data = TFile::Open("results/Results_Data_13TeV_EF1_zeg_CutZeg" + rtag + calitag + "_" + vtag + ".root");
    f_zeg_MC_TypeA = TFile::Open("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg" + rtag + "_PhMatch_EFakeTypeA_Lumiweighted" + calitag + "_" + vtag + ".root");
    f_zeg_MC_TypeB = TFile::Open("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg" + rtag + "_PhMatch_EFakeTypeB_Lumiweighted" + calitag + "_" + vtag + ".root");
    f_zeg_MC_TypeC = TFile::Open("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg" + rtag + "_PhMatch_EFakeTypeC_Lumiweighted" + calitag + "_" + vtag + ".root");
    f_zeg_MC_TypeCp = TFile::Open("results/Results_ZGammajetsElElNLO_Reco_FULL_13TeV_EF1_zeg_CutZeg" + rtag + "_PhMatch_EFakeTypeC_Lumiweighted" + calitag + "_" + vtag + ".root");
    f_zeg_MC_TypeD = TFile::Open("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg" + rtag + "_PhMatch_EFakeTypeD_Lumiweighted" + calitag + "_" + vtag + ".root");
    f_tteg_MC_TypeA = TFile::Open("results/Results_TTBar_Reco_FULL_13TeV_CR1_ejets_CutTTeg" + rtag + "_TTBarEE_PhMatch_EFake2TypeA_Lumiweighted" + calitag + "_" + vtag + ".root");
    f_tteg_MC_TypeB = TFile::Open("results/Results_TTBar_Reco_FULL_13TeV_CR1_ejets_CutTTeg" + rtag + "_TTBarEE_PhMatch_EFake2TypeB_Lumiweighted" + calitag + "_" + vtag + ".root");
    f_tteg_MC_TypeC = TFile::Open("results/Results_TTBar_Reco_FULL_13TeV_CR1_ejets_CutTTeg" + rtag + "_TTBarEE_PhMatch_EFake2TypeC_Lumiweighted" + calitag + "_" + vtag + ".root");
    f_tteg_MC_TypeCp = TFile::Open("results/Results_Signal_Reco_FULL_13TeV_CR1_ejets_CutTTeg" + rtag + "_TTBarEE_PhMatch_EFake2TypeC_Lumiweighted" + calitag + "_" + vtag + ".root");
    f_tteg_MC_TypeD = TFile::Open("results/Results_TTBar_Reco_FULL_13TeV_CR1_ejets_CutTTeg" + rtag + "_TTBarEE_PhMatch_EFake2TypeD_Lumiweighted" + calitag + "_" + vtag + ".root");
    f_ttmg_MC_TypeA = TFile::Open("results/Results_TTBar_Reco_FULL_13TeV_CR1_mujets_CutTTmug" + rtag + "_TTBarEM_PhMatch_EFake2TypeA_Lumiweighted" + calitag + "_" + vtag + ".root");
    f_ttmg_MC_TypeB = TFile::Open("results/Results_TTBar_Reco_FULL_13TeV_CR1_mujets_CutTTmug" + rtag + "_TTBarEM_PhMatch_EFake2TypeB_Lumiweighted" + calitag + "_" + vtag + ".root");
    f_ttmg_MC_TypeC = TFile::Open("results/Results_TTBar_Reco_FULL_13TeV_CR1_mujets_CutTTmug" + rtag + "_TTBarEM_PhMatch_EFake2TypeC_Lumiweighted" + calitag + "_" + vtag + ".root");
    f_ttmg_MC_TypeCp = TFile::Open("results/Results_Signal_Reco_FULL_13TeV_CR1_mujets_CutTTmug" + rtag + "_TTBarEM_PhMatch_EFake2TypeC_Lumiweighted" + calitag + "_" + vtag + ".root");
    f_ttmg_MC_TypeD = TFile::Open("results/Results_TTBar_Reco_FULL_13TeV_CR1_mujets_CutTTmug" + rtag + "_TTBarEM_PhMatch_EFake2TypeD_Lumiweighted" + calitag + "_" + vtag + ".root");

    f_zee_data = TFile::Open("results/Results_Data_13TeV_EF1_zee_CutZee" + rtag + "_" + vtag + ".root");
    f_zee_MC = TFile::Open("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zee_CutZee" + rtag + "_Lumiweighted_" + vtag + ".root");
    f_ttee_MC = TFile::Open("results/Results_TTBar_Reco_FULL_13TeV_EF1_ttel_ee_CutTTee" + rtag + "_TTBarEE_Lumiweighted_" + vtag + ".root");
    f_ttme_MC = TFile::Open("results/Results_TTBar_Reco_FULL_13TeV_EF1_ttel_mue_CutTTmue" + rtag + "_TTBarEM_Lumiweighted_" + vtag + ".root");

    EGammaResults r_MC_TypeA = GetEGammaResults(f_zeg_MC_TypeA, f_zee_MC, "-1", "-1", false ,DoReverse);
    EGammaResults r_MC_TypeB = GetEGammaResults(f_zeg_MC_TypeB, f_zee_MC, "-1", "-1", false ,DoReverse);
    EGammaResults r_MC_TypeC = GetEGammaResults(f_zeg_MC_TypeC, f_zee_MC, "-1", "-1", false ,DoReverse);
    EGammaResults r_MC_TypeCp = GetEGammaResults(f_zeg_MC_TypeCp, f_zee_MC, "-1", "-1", false ,DoReverse, NULL, NULL, true);
    EGammaResults r_MC_TypeD = GetEGammaResults(f_zeg_MC_TypeD, f_zee_MC, "-1", "-1", false ,DoReverse);
    EGammaResults r_MC_tteg_TypeA = GetEGammaResultsTTBar(f_tteg_MC_TypeA, f_ttee_MC, "-1", "-1", DoReverse, true);
    EGammaResults r_MC_tteg_TypeB = GetEGammaResultsTTBar(f_tteg_MC_TypeB, f_ttee_MC, "-1", "-1", DoReverse, true);
    EGammaResults r_MC_tteg_TypeC = GetEGammaResultsTTBar(f_tteg_MC_TypeC, f_ttee_MC, "-1", "-1", DoReverse, true);
    EGammaResults r_MC_tteg_TypeCp = GetEGammaResultsTTBar(f_tteg_MC_TypeCp, f_ttee_MC, "-1", "-1", DoReverse, true, true);
    EGammaResults r_MC_tteg_TypeD = GetEGammaResultsTTBar(f_tteg_MC_TypeD, f_ttee_MC, "-1", "-1", DoReverse, true);
    EGammaResults r_MC_ttmg_TypeA = GetEGammaResultsTTBar(f_ttmg_MC_TypeA, f_ttme_MC, "-1", "-1", DoReverse, false);
    EGammaResults r_MC_ttmg_TypeB = GetEGammaResultsTTBar(f_ttmg_MC_TypeB, f_ttme_MC, "-1", "-1", DoReverse, false);
    EGammaResults r_MC_ttmg_TypeC = GetEGammaResultsTTBar(f_ttmg_MC_TypeC, f_ttme_MC, "-1", "-1", DoReverse, false);
    EGammaResults r_MC_ttmg_TypeCp = GetEGammaResultsTTBar(f_ttmg_MC_TypeCp, f_ttme_MC, "-1", "-1", DoReverse, false, true);
    EGammaResults r_MC_ttmg_TypeD = GetEGammaResultsTTBar(f_ttmg_MC_TypeD, f_ttme_MC, "-1", "-1", DoReverse, false);
    vector<EGammaResults> vr_MCs; vr_MCs.push_back(r_MC_TypeA); vr_MCs.push_back(r_MC_TypeB); vr_MCs.push_back(r_MC_TypeC); vr_MCs.push_back(r_MC_TypeD);
    EGammaResults r_MC = SumEGammaResults(vr_MCs);
    vector<EGammaResults> vr_MCs_tteg; vr_MCs_tteg.push_back(r_MC_tteg_TypeA); vr_MCs_tteg.push_back(r_MC_tteg_TypeB); vr_MCs_tteg.push_back(r_MC_tteg_TypeC); vr_MCs_tteg.push_back(r_MC_tteg_TypeD);
    EGammaResults r_MC_tteg = SumEGammaResults(vr_MCs_tteg);
    vector<EGammaResults> vr_MCs_ttmg; vr_MCs_ttmg.push_back(r_MC_ttmg_TypeA); vr_MCs_ttmg.push_back(r_MC_ttmg_TypeB); vr_MCs_ttmg.push_back(r_MC_ttmg_TypeC); vr_MCs_ttmg.push_back(r_MC_ttmg_TypeD);
    EGammaResults r_MC_ttmg = SumEGammaResults(vr_MCs_ttmg);
    EGammaResults r_data = GetEGammaResults(f_zeg_data, f_zee_data, "-1", "-1", true ,DoReverse, r_MC.h_mass_zeg, r_MC.h_mass_zee);
    EGammaResults r_data_MC = DivideEGammaResults(r_data, r_MC);
    EGammaResults r_data_MMC;
    if (!ZGamma) r_data_MMC = SubtractEGammaResults(r_data, r_MC_TypeC);
    else r_data_MMC = SubtractEGammaResults(r_data, r_MC_TypeCp);
    vr_MCs.clear(); vr_MCs.push_back(r_MC_TypeA); vr_MCs.push_back(r_MC_TypeB); vr_MCs.push_back(r_MC_TypeD);
    EGammaResults r_MC2 = SumEGammaResults(vr_MCs);
    EGammaResults r_data_MC2 = DivideEGammaResults(r_data_MMC, r_MC2);

    TH1F* h_SF = new TH1F("FR_SF_EF1_zee_Nominal","",1,0,1);

    vector<EGammaResults> r_MC_TypeA_ptbins;
    vector<EGammaResults> r_MC_TypeB_ptbins;
    vector<EGammaResults> r_MC_TypeC_ptbins;
    vector<EGammaResults> r_MC_TypeCp_ptbins;
    vector<EGammaResults> r_MC_TypeD_ptbins;
    vector<EGammaResults> r_MC_ptbins;
    vector<EGammaResults> r_data_ptbins;
    vector<EGammaResults> r_data_MC_ptbins;
    vector<EGammaResults> r_data_MMC_ptbins;
    vector<EGammaResults> r_MC2_ptbins;
    vector<EGammaResults> r_data_MC2_ptbins;
    TH1F* h_data_ptbins = new TH1F("FR_Ptbins_EF1_zee_Data_Nominal","",m_egammaptbins_lo.size(),0,m_egammaptbins_lo.size());
    TH1F* h_MC_TypeA_ptbins = new TH1F("FR_Ptbins_EF1_zee_Reco_TypeA_Nominal","",m_egammaptbins_lo.size(),0,m_egammaptbins_lo.size());
    TH1F* h_MC_TypeB_ptbins = new TH1F("FR_Ptbins_EF1_zee_Reco_TypeB_Nominal","",m_egammaptbins_lo.size(),0,m_egammaptbins_lo.size());
    TH1F* h_MC_TypeC_ptbins = new TH1F("FR_Ptbins_EF1_zee_Reco_TypeC_Nominal","",m_egammaptbins_lo.size(),0,m_egammaptbins_lo.size());
    TH1F* h_MC_TypeCp_ptbins = new TH1F("FR_Ptbins_EF1_zee_Reco_TypeCp_Nominal","",m_egammaptbins_lo.size(),0,m_egammaptbins_lo.size());
    TH1F* h_MC_TypeD_ptbins = new TH1F("FR_Ptbins_EF1_zee_Reco_TypeD_Nominal","",m_egammaptbins_lo.size(),0,m_egammaptbins_lo.size());
    TH1F* h_SF_ptbins = new TH1F("FR_SF_Ptbins_EF1_zee_Nominal","",m_egammaptbins_lo.size(),0,m_egammaptbins_lo.size());

    if (DoPt) {
	for (int i = 1; i <= m_egammaptbins_lo.size(); i++) {
    	    char tmp[100];
    	    sprintf(tmp, "%d", i);
    	    EGammaResults tmpr_MC_TypeA = GetEGammaResults(f_zeg_MC_TypeA, f_zee_MC, tmp, "-1", false, DoReverse); r_MC_TypeA_ptbins.push_back(tmpr_MC_TypeA);
    	    EGammaResults tmpr_MC_TypeB = GetEGammaResults(f_zeg_MC_TypeB, f_zee_MC, tmp, "-1", false, DoReverse); r_MC_TypeB_ptbins.push_back(tmpr_MC_TypeB);
    	    EGammaResults tmpr_MC_TypeC = GetEGammaResults(f_zeg_MC_TypeC, f_zee_MC, tmp, "-1", false, DoReverse); r_MC_TypeC_ptbins.push_back(tmpr_MC_TypeC);
    	    EGammaResults tmpr_MC_TypeCp = GetEGammaResults(f_zeg_MC_TypeCp, f_zee_MC, tmp, "-1", false, DoReverse, NULL, NULL, true); r_MC_TypeCp_ptbins.push_back(tmpr_MC_TypeCp);
    	    EGammaResults tmpr_MC_TypeD = GetEGammaResults(f_zeg_MC_TypeD, f_zee_MC, tmp, "-1", false, DoReverse); r_MC_TypeD_ptbins.push_back(tmpr_MC_TypeD);
    	    vector<EGammaResults> tmp_vr_MCs; tmp_vr_MCs.push_back(tmpr_MC_TypeA); tmp_vr_MCs.push_back(tmpr_MC_TypeB); tmp_vr_MCs.push_back(tmpr_MC_TypeC); tmp_vr_MCs.push_back(tmpr_MC_TypeD); EGammaResults tmpr_MC = SumEGammaResults(tmp_vr_MCs);
    	    r_MC_ptbins.push_back(tmpr_MC);
    	    EGammaResults tmpr_data = GetEGammaResults(f_zeg_data, f_zee_data, tmp, "-1", true ,DoReverse, tmpr_MC.h_mass_zeg, tmpr_MC.h_mass_zee); r_data_ptbins.push_back(tmpr_data); EGammaResults tmpr_data_MC = DivideEGammaResults(tmpr_data, tmpr_MC);
    	    r_data_MC_ptbins.push_back(tmpr_data_MC);
    	    EGammaResults tmpr_data_MMC;
    	    if (!ZGamma) tmpr_data_MMC = SubtractEGammaResults(tmpr_data, tmpr_MC_TypeC);
    	    else tmpr_data_MMC = SubtractEGammaResults(tmpr_data, tmpr_MC_TypeCp);
    	    r_data_MMC_ptbins.push_back(tmpr_data_MMC);
    	    tmp_vr_MCs.clear(); tmp_vr_MCs.push_back(tmpr_MC_TypeA); tmp_vr_MCs.push_back(tmpr_MC_TypeB); tmp_vr_MCs.push_back(tmpr_MC_TypeD);
    	    EGammaResults tmpr_MC2 = SumEGammaResults(tmp_vr_MCs);
    	    r_MC2_ptbins.push_back(tmpr_MC2);
    	    EGammaResults tmpr_data_MC2 = DivideEGammaResults(tmpr_data_MMC, tmpr_MC2);
    	    r_data_MC2_ptbins.push_back(tmpr_data_MC2);
    	}

    	for (int i = 0; i < m_egammaptbins_lo.size(); i++) {
    	    char tmp[100];
    	    sprintf(tmp, "[%.0f,%.0f]", m_egammaptbins_lo.at(i), m_egammaptbins_hi.at(i));
    	    h_data_ptbins->GetXaxis()->SetBinLabel(i+1, tmp); h_data_ptbins->SetBinContent(i+1, r_data_ptbins.at(i).n_fr); h_data_ptbins->SetBinError(i+1, r_data_ptbins.at(i).e_fr);
    	    h_MC_TypeA_ptbins->GetXaxis()->SetBinLabel(i+1, tmp); h_MC_TypeA_ptbins->SetBinContent(i+1, r_MC_TypeA_ptbins.at(i).n_fr); h_MC_TypeA_ptbins->SetBinError(i+1, r_MC_TypeA_ptbins.at(i).e_fr);
    	    h_MC_TypeB_ptbins->GetXaxis()->SetBinLabel(i+1, tmp); h_MC_TypeB_ptbins->SetBinContent(i+1, r_MC_TypeB_ptbins.at(i).n_fr); h_MC_TypeB_ptbins->SetBinError(i+1, r_MC_TypeB_ptbins.at(i).e_fr);
    	    h_MC_TypeC_ptbins->GetXaxis()->SetBinLabel(i+1, tmp); h_MC_TypeC_ptbins->SetBinContent(i+1, r_MC_TypeC_ptbins.at(i).n_fr); h_MC_TypeC_ptbins->SetBinError(i+1, r_MC_TypeC_ptbins.at(i).e_fr);
    	    h_MC_TypeCp_ptbins->GetXaxis()->SetBinLabel(i+1, tmp); h_MC_TypeCp_ptbins->SetBinContent(i+1, r_MC_TypeCp_ptbins.at(i).n_fr); h_MC_TypeCp_ptbins->SetBinError(i+1, r_MC_TypeCp_ptbins.at(i).e_fr);
    	    h_MC_TypeD_ptbins->GetXaxis()->SetBinLabel(i+1, tmp); h_MC_TypeD_ptbins->SetBinContent(i+1, r_MC_TypeD_ptbins.at(i).n_fr); h_MC_TypeD_ptbins->SetBinError(i+1, r_MC_TypeD_ptbins.at(i).e_fr);
    	}
    }

    vector<EGammaResults> r_data_etabins;
    vector<EGammaResults> r_MC_TypeA_etabins;
    vector<EGammaResults> r_MC_TypeB_etabins;
    vector<EGammaResults> r_MC_TypeC_etabins;
    vector<EGammaResults> r_MC_TypeCp_etabins;
    vector<EGammaResults> r_MC_TypeD_etabins;
    vector<EGammaResults> r_MC_etabins;
    vector<EGammaResults> r_data_MC_etabins;
    vector<EGammaResults> r_data_MMC_etabins;
    vector<EGammaResults> r_MC2_etabins;
    vector<EGammaResults> r_data_MC2_etabins;
    TH1F* h_data_etabins = new TH1F("FR_Etabins_EF1_zee_Data_Nominal","",m_egammaetabins_lo.size(),0,m_egammaetabins_lo.size());
    TH1F* h_MC_TypeA_etabins = new TH1F("FR_Etabins_EF1_zee_Reco_TypeA_Nominal","",m_egammaetabins_lo.size(),0,m_egammaetabins_lo.size());
    TH1F* h_MC_TypeB_etabins = new TH1F("FR_Etabins_EF1_zee_Reco_TypeB_Nominal","",m_egammaetabins_lo.size(),0,m_egammaetabins_lo.size());
    TH1F* h_MC_TypeC_etabins = new TH1F("FR_Etabins_EF1_zee_Reco_TypeC_Nominal","",m_egammaetabins_lo.size(),0,m_egammaetabins_lo.size());
    TH1F* h_MC_TypeCp_etabins = new TH1F("FR_Etabins_EF1_zee_Reco_TypeCp_Nominal","",m_egammaetabins_lo.size(),0,m_egammaetabins_lo.size());
    TH1F* h_MC_TypeD_etabins = new TH1F("FR_Etabins_EF1_zee_Reco_TypeD_Nominal","",m_egammaetabins_lo.size(),0,m_egammaetabins_lo.size());
    TH1F* h_SF_etabins = new TH1F("FR_SF_Etabins_EF1_zee_Nominal","",m_egammaetabins_lo.size(),0,m_egammaetabins_lo.size());
    if (DoEta) {
	for (int i = 1; i <= m_egammaetabins_lo.size(); i++) {
    	    char tmp[100];
    	    sprintf(tmp, "%d", i);
    	    EGammaResults tmpr_MC_TypeA = GetEGammaResults(f_zeg_MC_TypeA, f_zee_MC, "-1", tmp, false, DoReverse); r_MC_TypeA_etabins.push_back(tmpr_MC_TypeA);
    	    EGammaResults tmpr_MC_TypeB = GetEGammaResults(f_zeg_MC_TypeB, f_zee_MC, "-1", tmp, false, DoReverse); r_MC_TypeB_etabins.push_back(tmpr_MC_TypeB);
    	    EGammaResults tmpr_MC_TypeC = GetEGammaResults(f_zeg_MC_TypeC, f_zee_MC, "-1", tmp, false, DoReverse); r_MC_TypeC_etabins.push_back(tmpr_MC_TypeC);
    	    EGammaResults tmpr_MC_TypeCp = GetEGammaResults(f_zeg_MC_TypeCp, f_zee_MC, "-1", tmp, false, DoReverse, NULL, NULL, true); r_MC_TypeCp_etabins.push_back(tmpr_MC_TypeCp);
    	    EGammaResults tmpr_MC_TypeD = GetEGammaResults(f_zeg_MC_TypeD, f_zee_MC, "-1", tmp, false, DoReverse); r_MC_TypeD_etabins.push_back(tmpr_MC_TypeD);
    	    vector<EGammaResults> tmp_vr_MCs; tmp_vr_MCs.push_back(tmpr_MC_TypeA); tmp_vr_MCs.push_back(tmpr_MC_TypeB); tmp_vr_MCs.push_back(tmpr_MC_TypeC); tmp_vr_MCs.push_back(tmpr_MC_TypeD); EGammaResults tmpr_MC = SumEGammaResults(tmp_vr_MCs); 
    	    r_MC_etabins.push_back(tmpr_MC);
    	    EGammaResults tmpr_data = GetEGammaResults(f_zeg_data, f_zee_data, "-1", tmp, true, DoReverse, tmpr_MC.h_mass_zeg, tmpr_MC.h_mass_zee); r_data_etabins.push_back(tmpr_data); EGammaResults tmpr_data_MC = DivideEGammaResults(tmpr_data, tmpr_MC);
    	    r_data_MC_etabins.push_back(tmpr_data_MC);
    	    EGammaResults tmpr_data_MMC;
    	    if (!ZGamma) tmpr_data_MMC = SubtractEGammaResults(tmpr_data, tmpr_MC_TypeC);
    	    else tmpr_data_MMC = SubtractEGammaResults(tmpr_data, tmpr_MC_TypeCp);
    	    r_data_MMC_etabins.push_back(tmpr_data_MMC);
    	    tmp_vr_MCs.clear(); tmp_vr_MCs.push_back(tmpr_MC_TypeA); tmp_vr_MCs.push_back(tmpr_MC_TypeB); tmp_vr_MCs.push_back(tmpr_MC_TypeD);
    	    EGammaResults tmpr_MC2 = SumEGammaResults(tmp_vr_MCs);
    	    r_MC2_etabins.push_back(tmpr_MC2);
    	    EGammaResults tmpr_data_MC2 = DivideEGammaResults(tmpr_data_MMC, tmpr_MC2);
    	    r_data_MC2_etabins.push_back(tmpr_data_MC2);
    	}

    	for (int i = 0; i < m_egammaetabins_lo.size(); i++) {
    	    char tmp[100];
    	    sprintf(tmp, "[%.1f,%.1f]", m_egammaetabins_lo.at(i), m_egammaetabins_hi.at(i));
    	    h_data_etabins->GetXaxis()->SetBinLabel(i+1, tmp); h_data_etabins->SetBinContent(i+1, r_data_etabins.at(i).n_fr); h_data_etabins->SetBinError(i+1, r_data_etabins.at(i).e_fr);
    	    h_MC_TypeA_etabins->GetXaxis()->SetBinLabel(i+1, tmp); h_MC_TypeA_etabins->SetBinContent(i+1, r_MC_TypeA_etabins.at(i).n_fr); h_MC_TypeA_etabins->SetBinError(i+1, r_MC_TypeA_etabins.at(i).e_fr);
    	    h_MC_TypeB_etabins->GetXaxis()->SetBinLabel(i+1, tmp); h_MC_TypeB_etabins->SetBinContent(i+1, r_MC_TypeB_etabins.at(i).n_fr); h_MC_TypeB_etabins->SetBinError(i+1, r_MC_TypeB_etabins.at(i).e_fr);
    	    h_MC_TypeC_etabins->GetXaxis()->SetBinLabel(i+1, tmp); h_MC_TypeC_etabins->SetBinContent(i+1, r_MC_TypeC_etabins.at(i).n_fr); h_MC_TypeC_etabins->SetBinError(i+1, r_MC_TypeC_etabins.at(i).e_fr);
    	    h_MC_TypeCp_etabins->GetXaxis()->SetBinLabel(i+1, tmp); h_MC_TypeCp_etabins->SetBinContent(i+1, r_MC_TypeCp_etabins.at(i).n_fr); h_MC_TypeCp_etabins->SetBinError(i+1, r_MC_TypeCp_etabins.at(i).e_fr);
    	    h_MC_TypeD_etabins->GetXaxis()->SetBinLabel(i+1, tmp); h_MC_TypeD_etabins->SetBinContent(i+1, r_MC_TypeD_etabins.at(i).n_fr); h_MC_TypeD_etabins->SetBinError(i+1, r_MC_TypeD_etabins.at(i).e_fr);
    	}
    }

    vector<vector<EGammaResults> > r_MC_TypeA_ptetabins;
    vector<vector<EGammaResults> > r_MC_TypeB_ptetabins;
    vector<vector<EGammaResults> > r_MC_TypeC_ptetabins;
    vector<vector<EGammaResults> > r_MC_TypeCp_ptetabins;
    vector<vector<EGammaResults> > r_MC_TypeD_ptetabins;
    vector<vector<EGammaResults> > r_MC_ptetabins;
    vector<vector<EGammaResults> > r_data_ptetabins;
    vector<vector<EGammaResults> > r_data_MC_ptetabins;
    vector<vector<EGammaResults> > r_data_MMC_ptetabins;
    vector<vector<EGammaResults> > r_MC2_ptetabins;
    vector<vector<EGammaResults> > r_data_MC2_ptetabins;
    TH2F* h_data_ptetabins = new TH2F("FR_PtEtabins_EF1_zee_Data_Nominal","",m_egammapt2dbins_lo.size(),0,m_egammapt2dbins_lo.size(),m_egammaeta2dbins_lo.size(),0,m_egammaeta2dbins_lo.size());
    TH2F* h_MC_TypeA_ptetabins = new TH2F("FR_PtEtabins_EF1_zee_Reco_TypeA_Nominal","",m_egammapt2dbins_lo.size(),0,m_egammapt2dbins_lo.size(),m_egammaeta2dbins_lo.size(),0,m_egammaeta2dbins_lo.size());
    TH2F* h_MC_TypeB_ptetabins = new TH2F("FR_PtEtabins_EF1_zee_Reco_TypeB_Nominal","",m_egammapt2dbins_lo.size(),0,m_egammapt2dbins_lo.size(),m_egammaeta2dbins_lo.size(),0,m_egammaeta2dbins_lo.size());
    TH2F* h_MC_TypeC_ptetabins = new TH2F("FR_PtEtabins_EF1_zee_Reco_TypeC_Nominal","",m_egammapt2dbins_lo.size(),0,m_egammapt2dbins_lo.size(),m_egammaeta2dbins_lo.size(),0,m_egammaeta2dbins_lo.size());
    TH2F* h_MC_TypeCp_ptetabins = new TH2F("FR_PtEtabins_EF1_zee_Reco_TypeCp_Nominal","",m_egammapt2dbins_lo.size(),0,m_egammapt2dbins_lo.size(),m_egammaeta2dbins_lo.size(),0,m_egammaeta2dbins_lo.size());
    TH2F* h_MC_TypeD_ptetabins = new TH2F("FR_PtEtabins_EF1_zee_Reco_TypeD_Nominal","",m_egammapt2dbins_lo.size(),0,m_egammapt2dbins_lo.size(),m_egammaeta2dbins_lo.size(),0,m_egammaeta2dbins_lo.size());
    TH2F* h_SF_ptetabins = new TH2F("FR_SF_PtEtabins_EF1_zee_Nominal","",m_egammapt2dbins_lo.size(),0,m_egammapt2dbins_lo.size(),m_egammaeta2dbins_lo.size(),0,m_egammaeta2dbins_lo.size());

    if (DoPtEta) {
	for (int i = 1; i <= m_egammapt2dbins_lo.size(); i++) {
	    char tmpptbin[100];
    	    sprintf(tmpptbin, "%d", i);
	    vector<EGammaResults> r_tmp_MC_TypeA_ptetabins;
	    vector<EGammaResults> r_tmp_MC_TypeB_ptetabins;
	    vector<EGammaResults> r_tmp_MC_TypeC_ptetabins;
	    vector<EGammaResults> r_tmp_MC_TypeCp_ptetabins;
	    vector<EGammaResults> r_tmp_MC_TypeD_ptetabins;
	    vector<EGammaResults> r_tmp_MC_ptetabins;
	    vector<EGammaResults> r_tmp_data_ptetabins;
	    vector<EGammaResults> r_tmp_data_MC_ptetabins;
	    vector<EGammaResults> r_tmp_data_MMC_ptetabins;
	    vector<EGammaResults> r_tmp_MC2_ptetabins;
	    vector<EGammaResults> r_tmp_data_MC2_ptetabins;
	    for (int j = 1; j <= m_egammaeta2dbins_lo.size(); i++) {
		char tmpetabin[100];
		sprintf(tmpetabin, "%d", j);
    	    	EGammaResults tmpr_MC_TypeA = GetEGammaResults(f_zeg_MC_TypeA, f_zee_MC, tmpptbin, tmpetabin, false, DoReverse); r_tmp_MC_TypeA_ptetabins.push_back(tmpr_MC_TypeA);
    	    	EGammaResults tmpr_MC_TypeB = GetEGammaResults(f_zeg_MC_TypeB, f_zee_MC, tmpptbin, tmpetabin, false, DoReverse); r_tmp_MC_TypeB_ptetabins.push_back(tmpr_MC_TypeB);
    	    	EGammaResults tmpr_MC_TypeC = GetEGammaResults(f_zeg_MC_TypeC, f_zee_MC, tmpptbin, tmpetabin, false, DoReverse); r_tmp_MC_TypeC_ptetabins.push_back(tmpr_MC_TypeC);
    	    	EGammaResults tmpr_MC_TypeCp = GetEGammaResults(f_zeg_MC_TypeCp, f_zee_MC, tmpptbin, tmpetabin, false, DoReverse, NULL, NULL, true); r_tmp_MC_TypeCp_ptetabins.push_back(tmpr_MC_TypeCp);
    	    	EGammaResults tmpr_MC_TypeD = GetEGammaResults(f_zeg_MC_TypeD, f_zee_MC, tmpptbin, tmpetabin, false, DoReverse); r_tmp_MC_TypeD_ptetabins.push_back(tmpr_MC_TypeD);
    	    	vector<EGammaResults> tmp_vr_MCs; tmp_vr_MCs.push_back(tmpr_MC_TypeA); tmp_vr_MCs.push_back(tmpr_MC_TypeB); tmp_vr_MCs.push_back(tmpr_MC_TypeC); tmp_vr_MCs.push_back(tmpr_MC_TypeD); EGammaResults tmpr_MC = SumEGammaResults(tmp_vr_MCs);
    	    	r_tmp_MC_ptetabins.push_back(tmpr_MC);
    	    	EGammaResults tmpr_data = GetEGammaResults(f_zeg_data, f_zee_data, tmpptbin, tmpetabin, true ,DoReverse, tmpr_MC.h_mass_zeg, tmpr_MC.h_mass_zee); r_tmp_data_ptetabins.push_back(tmpr_data); EGammaResults tmpr_data_MC = DivideEGammaResults(tmpr_data, tmpr_MC);
    	    	r_tmp_data_MC_ptetabins.push_back(tmpr_data_MC);
    	    	EGammaResults tmpr_data_MMC;
    	    	if (!ZGamma) tmpr_data_MMC = SubtractEGammaResults(tmpr_data, tmpr_MC_TypeC);
    	    	else tmpr_data_MMC = SubtractEGammaResults(tmpr_data, tmpr_MC_TypeCp);
    	    	r_tmp_data_MMC_ptetabins.push_back(tmpr_data_MMC);
    	    	tmp_vr_MCs.clear(); tmp_vr_MCs.push_back(tmpr_MC_TypeA); tmp_vr_MCs.push_back(tmpr_MC_TypeB); tmp_vr_MCs.push_back(tmpr_MC_TypeD);
    	    	EGammaResults tmpr_MC2 = SumEGammaResults(tmp_vr_MCs);
    	    	r_tmp_MC2_ptetabins.push_back(tmpr_MC2);
    	    	EGammaResults tmpr_data_MC2 = DivideEGammaResults(tmpr_data_MMC, tmpr_MC2);
    	    	r_tmp_data_MC2_ptetabins.push_back(tmpr_data_MC2);
	    }
	    r_MC_TypeA_ptetabins.push_back(r_tmp_MC_TypeA_ptetabins);
	    r_MC_TypeB_ptetabins.push_back(r_tmp_MC_TypeB_ptetabins);
	    r_MC_TypeC_ptetabins.push_back(r_tmp_MC_TypeC_ptetabins);
	    r_MC_TypeCp_ptetabins.push_back(r_tmp_MC_TypeCp_ptetabins);
	    r_MC_TypeD_ptetabins.push_back(r_tmp_MC_TypeD_ptetabins);
	    r_MC_ptetabins.push_back(r_tmp_MC_ptetabins);
	    r_data_ptetabins.push_back(r_tmp_data_ptetabins);
	    r_data_MC_ptetabins.push_back(r_tmp_data_MC_ptetabins);
	    r_data_MMC_ptetabins.push_back(r_tmp_data_MMC_ptetabins);
	    r_MC2_ptetabins.push_back(r_tmp_MC2_ptetabins);
	    r_data_MC2_ptetabins.push_back(r_tmp_data_MC2_ptetabins);
    	}

    	for (int i = 0; i < m_egammapt2dbins_lo.size(); i++) {
    	    char tmpptbin[100];
    	    sprintf(tmpptbin, "[%.0f,%.0f]", m_egammapt2dbins_lo.at(i), m_egammapt2dbins_hi.at(i));
	    for (int j = 0; j < m_egammaeta2dbins_lo.size(); i++) {
		char tmpetabin[100];
		sprintf(tmpetabin, "[%.1f,%.1f]", m_egammapt2dbins_lo.at(i), m_egammapt2dbins_hi.at(i));
		h_data_ptetabins->GetXaxis()->SetBinLabel(i+1, tmpptbin); 
		h_data_ptetabins->GetYaxis()->SetBinLabel(j+1, tmpetabin); 
		h_data_ptetabins->SetBinContent(i+1,j+1, r_data_ptetabins.at(i).at(j).n_fr); 
		h_data_ptetabins->SetBinError(i+1,j+1, r_data_ptetabins.at(i).at(j).e_fr);
		h_MC_TypeA_ptetabins->GetXaxis()->SetBinLabel(i+1, tmpptbin); 
		h_MC_TypeA_ptetabins->GetYaxis()->SetBinLabel(j+1, tmpetabin); 
		h_MC_TypeA_ptetabins->SetBinContent(i+1,j+1, r_MC_TypeA_ptetabins.at(i).at(j).n_fr); 
		h_MC_TypeA_ptetabins->SetBinError(i+1,j+1, r_MC_TypeA_ptetabins.at(i).at(j).e_fr);
		h_MC_TypeB_ptetabins->GetXaxis()->SetBinLabel(i+1, tmpptbin); 
		h_MC_TypeB_ptetabins->GetYaxis()->SetBinLabel(j+1, tmpetabin); 
		h_MC_TypeB_ptetabins->SetBinContent(i+1,j+1, r_MC_TypeB_ptetabins.at(i).at(j).n_fr); 
		h_MC_TypeB_ptetabins->SetBinError(i+1,j+1, r_MC_TypeB_ptetabins.at(i).at(j).e_fr);
		h_MC_TypeC_ptetabins->GetXaxis()->SetBinLabel(i+1, tmpptbin); 
		h_MC_TypeC_ptetabins->GetYaxis()->SetBinLabel(j+1, tmpetabin); 
		h_MC_TypeC_ptetabins->SetBinContent(i+1,j+1, r_MC_TypeC_ptetabins.at(i).at(j).n_fr); 
		h_MC_TypeC_ptetabins->SetBinError(i+1,j+1, r_MC_TypeC_ptetabins.at(i).at(j).e_fr);
		h_MC_TypeCp_ptetabins->GetXaxis()->SetBinLabel(i+1, tmpptbin); 
		h_MC_TypeCp_ptetabins->GetYaxis()->SetBinLabel(j+1, tmpetabin); 
		h_MC_TypeCp_ptetabins->SetBinContent(i+1,j+1, r_MC_TypeCp_ptetabins.at(i).at(j).n_fr); 
		h_MC_TypeCp_ptetabins->SetBinError(i+1,j+1, r_MC_TypeCp_ptetabins.at(i).at(j).e_fr);
		h_MC_TypeD_ptetabins->GetXaxis()->SetBinLabel(i+1, tmpptbin); 
		h_MC_TypeD_ptetabins->GetYaxis()->SetBinLabel(j+1, tmpetabin); 
		h_MC_TypeD_ptetabins->SetBinContent(i+1,j+1, r_MC_TypeD_ptetabins.at(i).at(j).n_fr); 
		h_MC_TypeD_ptetabins->SetBinError(i+1,j+1, r_MC_TypeD_ptetabins.at(i).at(j).e_fr);
	    }
    	}
    }

    cout << fixed << setprecision(precision);
    cout << "\\scalebox{0.5}{" << endl;
    cout << "\\begin{tabular}{c|c|c|c}" << endl;
    cout << "\\hline" << endl;
    cout << "\\hline" << endl;
    cout << "& N(fake) & N(probe) & FR \\\\ " << endl;
    cout << "\\hline" << endl << " Data &";
    PrintEGammaResults(r_data);
    cout << "\\hline" << endl << " A &";
    PrintEGammaResults(r_MC_TypeA);
    cout << "\\hline" << endl << " B &";
    PrintEGammaResults(r_MC_TypeB);
    cout << "\\hline" << endl << " C &";
    if (!ZGamma) PrintEGammaResults(r_MC_TypeC);
    else PrintEGammaResults(r_MC_TypeCp);
    cout << "\\hline" << endl << " D &";
    PrintEGammaResults(r_MC_TypeD);
    cout << "\\hline" << endl << " All &";
    PrintEGammaResults(r_MC);
    cout << "\\hline" << endl << " Data/All &";
    PrintEGammaResults(r_data_MC);
    cout << "\\hline" << endl << " Data-C&";
    PrintEGammaResults(r_data_MMC);
    cout << "\\hline" << endl << " (Data-C)/(A+B+D)&";
    PrintEGammaResults(r_data_MC2);
    h_SF->SetBinContent(1, r_data_MC2.n_fr);
    h_SF->SetBinError(1, r_data_MC2.e_fr);
    cout << "\\hline" << endl;
    cout << "\\hline" << endl;
    cout << "\\end{tabular}}" << endl;

    ofstream ofile_table_efake_1("results/forefake/table_efake_1.txt");
    ofile_table_efake_1 << fixed << setprecision(precision);
    ofile_table_efake_1 << "\\hline" << endl << " Type (a) &";
    PrintEGammaResults(r_MC_TypeA, ofile_table_efake_1);
    ofile_table_efake_1 << "\\hline" << endl << " Type (b) &";
    PrintEGammaResults(r_MC_TypeB, ofile_table_efake_1);
    ofile_table_efake_1 << "\\hline" << endl << " Type (c) &";
    if (!ZGamma) PrintEGammaResults(r_MC_TypeC, ofile_table_efake_1);
    else PrintEGammaResults(r_MC_TypeCp, ofile_table_efake_1);
    ofile_table_efake_1 << "\\hline" << endl << " Type (d) &";
    PrintEGammaResults(r_MC_TypeD, ofile_table_efake_1);
    ofile_table_efake_1 << "\\hline" << endl << " All &";
    PrintEGammaResults(r_MC, ofile_table_efake_1);

    ofstream ofile_table_efake_2("results/forefake/table_efake_2.txt");
    ofile_table_efake_2 << fixed << setprecision(precision);
    ofile_table_efake_2 << "\\hline" << endl << " Data &";
    PrintEGammaResults(r_data, ofile_table_efake_2);
    ofile_table_efake_2 << "\\hline" << endl << " Data/MC(all) &";
    PrintEGammaResults(r_data_MC, ofile_table_efake_2);
    ofile_table_efake_2 << "\\hline" << endl << " Data - MC(c) &";
    PrintEGammaResults(r_data_MMC, ofile_table_efake_2);
    ofile_table_efake_2 << "\\hline" << endl << " (Data-MC(c)/MC(a+b+d) &";
    PrintEGammaResults(r_data_MC2, ofile_table_efake_2);

    string ofilename = "results/forefake/overallsf";
    if (DoReverse) ofilename += "_Reverse";
    if (MCTemp) ofilename += "_MCTemp";
    if (RShrink) ofilename += "_RShrink";
    if (RExpand) ofilename += "_RExpand";
    if (BkgGaus) ofilename += "_BkgGaus";
    else ofilename += "_BkgPoly";
    if (ZGamma) ofilename += "_ZGamma";
    ofilename += ".txt";
    ofstream ofile_overallsf(ofilename.c_str());
    ofile_overallsf << fixed << setprecision(precision);
    ofile_overallsf << r_data_MC2.n_fr << " $\\pm$ " << r_data_MC2.e_fr << endl;

    for (int i = 0; i < r_data_MC_ptbins.size(); i++) {
	cout << "------------------------" << endl;
	cout << "ptbin: " << i << endl;
	cout << "\\scalebox{0.5}{" << endl;
    	cout << "\\begin{tabular}{c|c|c|c}" << endl;
    	cout << "\\hline" << endl;
    	cout << "\\hline" << endl;
	cout << "& N(fake) & N(probe) & FR \\\\ " << endl;
	cout << "\\hline" << endl << " Data &";
	PrintEGammaResults(r_data_ptbins.at(i));
	cout << "\\hline" << endl << " A & ";
    	PrintEGammaResults(r_MC_TypeA_ptbins.at(i));
	cout << "\\hline" << endl << " B & ";
    	PrintEGammaResults(r_MC_TypeB_ptbins.at(i));
	cout << "\\hline" << endl << " C & ";
    	if (!ZGamma) PrintEGammaResults(r_MC_TypeC_ptbins.at(i));
    	else PrintEGammaResults(r_MC_TypeCp_ptbins.at(i));
	cout << "\\hline" << endl << " D & ";
    	PrintEGammaResults(r_MC_TypeD_ptbins.at(i));
	cout << "\\hline" << endl << " All & ";
    	PrintEGammaResults(r_MC_ptbins.at(i));
	cout << "\\hline" << endl << " Data/All & ";
    	PrintEGammaResults(r_data_MC_ptbins.at(i));
	cout << "\\hline" << endl;
	cout << "\\hline" << endl;
	cout << "\\end{tabular}}" << endl;
    }

    cout << "\\scalebox{0.5}{" << endl;
    cout << "\\begin{tabular}{c|c|c|c|c|c|c}" << endl;
    cout << "\\hline" << endl;
    cout << "\\hline" << endl;
    cout << setw(30) << " " << "&" 
         << setw(30) << "FR(Zee)" << "&"
         << setw(30) << "FR(ttee) " << "&"
         << setw(30) << "FR(ttme)" << "\\\\"
	 << endl;
    cout << "\\hline" << endl;
    cout << setw(30) << "A" << "&" 
         << setw(12) << r_MC_TypeA.n_fr << " $\\pm$ " << setw(11) << r_MC_TypeA.e_fr << "&"
         << setw(12) << r_MC_tteg_TypeA.n_fr << " $\\pm$ " << setw(11) << r_MC_tteg_TypeA.e_fr << "&"
         << setw(12) << r_MC_ttmg_TypeA.n_fr << " $\\pm$ " << setw(11) << r_MC_ttmg_TypeA.e_fr << "\\\\"
	 << endl;
    cout << "\\hline" << endl;
    cout << setw(30) << "B" << "&" 
         << setw(12) << r_MC_TypeB.n_fr << " $\\pm$ " << setw(11) << r_MC_TypeB.e_fr << "&"
         << setw(12) << r_MC_tteg_TypeB.n_fr << " $\\pm$ " << setw(11) << r_MC_tteg_TypeB.e_fr << "&"
         << setw(12) << r_MC_ttmg_TypeB.n_fr << " $\\pm$ " << setw(11) << r_MC_ttmg_TypeB.e_fr << "\\\\"
	 << endl;
    cout << "\\hline" << endl;
    if (!ZGamma) {
    cout << setw(30) << "C" << "&" 
         << setw(12) << r_MC_TypeC.n_fr << " $\\pm$ " << setw(11) << r_MC_TypeC.e_fr << "&"
         << setw(12) << r_MC_tteg_TypeC.n_fr << " $\\pm$ " << setw(11) << r_MC_tteg_TypeC.e_fr << "&"
         << setw(12) << r_MC_ttmg_TypeC.n_fr << " $\\pm$ " << setw(11) << r_MC_ttmg_TypeC.e_fr << "\\\\"
	 << endl;
    } else {
    cout << setw(30) << "C" << "&" 
         << setw(12) << r_MC_TypeCp.n_fr << " $\\pm$ " << setw(11) << r_MC_TypeCp.e_fr << "&"
         << setw(12) << r_MC_tteg_TypeCp.n_fr << " $\\pm$ " << setw(11) << r_MC_tteg_TypeCp.e_fr << "&"
         << setw(12) << r_MC_ttmg_TypeCp.n_fr << " $\\pm$ " << setw(11) << r_MC_ttmg_TypeCp.e_fr << "\\\\"
	 << endl;
    }
    cout << "\\hline" << endl;
    cout << setw(30) << "D" << "&" 
         << setw(12) << r_MC_TypeD.n_fr << " $\\pm$ " << setw(11) << r_MC_TypeD.e_fr << "&"
         << setw(12) << r_MC_tteg_TypeD.n_fr << " $\\pm$ " << setw(11) << r_MC_tteg_TypeD.e_fr << "&"
         << setw(12) << r_MC_ttmg_TypeD.n_fr << " $\\pm$ " << setw(11) << r_MC_ttmg_TypeD.e_fr << "\\\\"
	 << endl;
    //cout << " C &" 
    //     << r_MC_TypeC.n_fr << "  $\\pm$  " << r_MC_TypeC.e_fr << " & "
    //     << r_MC_TypeCp.n_fr << "  $\\pm$  " << r_MC_TypeCp.e_fr << " & "
    //     << r_MC_tteg_TypeC.n_fr << "  $\\pm$  " << r_MC_tteg_TypeC.e_fr << " & "
    //     << r_MC_tteg_TypeCp.n_fr << "  $\\pm$  " << r_MC_tteg_TypeCp.e_fr << " & "
    //     << r_MC_ttmg_TypeC.n_fr << "  $\\pm$  " << r_MC_ttmg_TypeC.e_fr << " & "
    //     << r_MC_ttmg_TypeCp.n_fr << "  $\\pm$  " << r_MC_ttmg_TypeCp.e_fr << " \\\\ " << endl;
    cout << setw(30) << "All" << "&" 
         << setw(12) << r_MC_ttmg.n_fr << " $\\pm$ " << setw(11) << r_MC.e_fr << "&"
         << setw(12) << r_MC_tteg.n_fr << " $\\pm$ " << setw(11) << r_MC_tteg.e_fr << "&"
         << setw(12) << r_MC_ttmg.n_fr << " $\\pm$ " << setw(11) << r_MC_ttmg.e_fr << "\\\\"
	 << endl;
    cout << "\\hline" << endl;
    cout << "\\hline" << endl;
    cout << "\\end{tabular}}" << endl;

    if (DoPt) {
    for (int i = 0; i < r_data_MC_ptbins.size(); i++) {
	cout << "------------------------" << endl;
	cout << "ptbin: " << i << endl;
	cout << "\\scalebox{0.5}{" << endl;
    	cout << "\\begin{tabular}{c|c|c|c}" << endl;
    	cout << "\\hline" << endl;
    	cout << "\\hline" << endl;
	cout << "& N(fake) & N(probe) & FR \\\\ " << endl;
	cout << "\\hline" << endl << " Data &";
	PrintEGammaResults(r_data_ptbins.at(i));
	cout << "\\hline" << endl << " A & ";
    	PrintEGammaResults(r_MC_TypeA_ptbins.at(i));
	cout << "\\hline" << endl << " B & ";
    	PrintEGammaResults(r_MC_TypeB_ptbins.at(i));
	cout << "\\hline" << endl << " C & ";
	if (!ZGamma) PrintEGammaResults(r_MC_TypeC_ptbins.at(i));
	else PrintEGammaResults(r_MC_TypeCp_ptbins.at(i));

	cout << "\\hline" << endl << " D & ";
    	PrintEGammaResults(r_MC_TypeD_ptbins.at(i));
	cout << "\\hline" << endl << " All & ";
    	PrintEGammaResults(r_MC_ptbins.at(i));
	cout << "\\hline" << endl << " Data/All & ";
    	PrintEGammaResults(r_data_MC_ptbins.at(i));
	cout << "\\hline" << endl;
	cout << "\\hline" << endl;
	cout << "\\end{tabular}}" << endl;
    }

    cout << endl;
    cout << "\\scalebox{0.45}{" << endl;
    cout << "\\begin{tabular}{c|";
    for (int i = 1; i < r_data_MC_ptbins.size(); i++) {
	cout << "c";
	if (i != r_data_MC_ptbins.size() - 1) cout << "|";
	else cout << "}" << endl;
    }
	cout << "\\hline" << endl;
	cout << "\\hline" << endl;
	cout << " &";
    for (int i = 1; i < r_data_MC_ptbins.size(); i++) {
	cout << "Ptbin " << i+1;
	if (i != r_data_MC_ptbins.size() - 1) cout << "&";
	else cout << "\\\\" << endl;
    }
	cout << "\\hline" << endl;
	cout << "SF(eg) &";
    for (int i = 1; i < r_data_MC_ptbins.size(); i++) {
    	cout << r_data_MC_ptbins.at(i).n_zeg << " $\\pm$ " << r_data_MC_ptbins.at(i).e_zeg ;
	if (i != r_data_MC_ptbins.size()-1) cout << " & ";
	else cout << "\\\\" << endl;
    }
	cout << "\\hline" << endl;
	cout << "SF(ee) &"; 
    for (int i = 1; i < r_data_MC_ptbins.size(); i++) {
    	cout << r_data_MC_ptbins.at(i).n_zee << " $\\pm$ "<<  r_data_MC_ptbins.at(i).e_zee ;
	if (i != r_data_MC_ptbins.size()-1) cout << " & ";
	else cout << "\\\\"<< endl;
    }
	cout << "\\hline" << endl;
	cout << "SF(FR) &";
    for (int i = 1; i < r_data_MC_ptbins.size(); i++) {
    	cout << r_data_MC_ptbins.at(i).n_fr << " $\\pm$ "<<  r_data_MC_ptbins.at(i).e_fr ;
	if (i != r_data_MC_ptbins.size()-1) cout << " & ";
	else cout << "\\\\"<< endl;
    }
	cout << "\\hline" << endl;
	cout << "SF2(FR) &";
    for (int i = 1; i < r_data_MC2_ptbins.size(); i++) {
    	cout << r_data_MC2_ptbins.at(i).n_fr << " $\\pm$ "<<  r_data_MC2_ptbins.at(i).e_fr ;
	if (i != r_data_MC2_ptbins.size()-1) cout << " & ";
	else cout << "\\\\"<< endl;
	h_SF_ptbins->SetBinContent(i+1, r_data_MC2_ptbins.at(i).n_fr);
	h_SF_ptbins->SetBinError(i+1, r_data_MC2_ptbins.at(i).e_fr);
    }
//	cout << "\\hline" << endl;
//	cout << "Sys(FR) &";
//    for (int i = 1; i < r_data_MC2_ptbins.size(); i++) {
//	EGammaResults tmpresult = SubtractEGammaResults(r_data_MC2_ptbins.at(i), r_data_MC_ptbins.at(i));
//	EGammaResults tmpresult2 = DivideEGammaResults(tmpresult, r_data_MC_ptbins.at(i));
//    	cout << tmpresult2.n_fr;
//	if (i != r_data_MC2_ptbins.size()-1) cout << " & ";
//	else cout << "\\\\"<< endl;
//    }
	cout << "\\hline" << endl;
	cout << "\\hline" << endl;
	cout << "\\end{tabular}}" << endl;
    }

    if (DoEta) {
    cout << endl;
    cout << "\\scalebox{0.45}{" << endl;
    cout << "\\begin{tabular}{c|";
    for (int i = 0; i < r_data_MC_etabins.size(); i++) {
	cout << "c";
	if (i != r_data_MC_etabins.size() - 1) cout << "|";
	else cout << "}" << endl;
    }
	cout << "\\hline" << endl;
	cout << "\\hline" << endl;
	cout << " &";
    for (int i = 0; i < r_data_MC_etabins.size(); i++) {
	cout << "Etabin " << i+1;
	if (i != r_data_MC_etabins.size() - 1) cout << "&";
	else cout << "\\\\" << endl;
    }
	cout << "\\hline" << endl;
	cout << "SF(eg) &";
    for (int i = 0; i < r_data_MC_etabins.size(); i++) {
    	cout << r_data_MC_etabins.at(i).n_zeg << " $\\pm$ " << r_data_MC_etabins.at(i).e_zeg ;
	if (i != r_data_MC_etabins.size()-1) cout << " & ";
	else cout << "\\\\" << endl;
    }
	cout << "\\hline" << endl;
	cout << "SF(ee) &"; 
    for (int i = 0; i < r_data_MC_etabins.size(); i++) {
    	cout << r_data_MC_etabins.at(i).n_zee << " $\\pm$ "<<  r_data_MC_etabins.at(i).e_zee ;
	if (i != r_data_MC_etabins.size()-1) cout << " & ";
	else cout << "\\\\"<< endl;
    }
	cout << "\\hline" << endl;
	cout << "SF(FR) &";
    for (int i = 0; i < r_data_MC_etabins.size(); i++) {
    	cout << r_data_MC_etabins.at(i).n_fr << " $\\pm$ "<<  r_data_MC_etabins.at(i).e_fr ;
	if (i != r_data_MC_etabins.size()-1) cout << " & ";
	else cout << "\\\\"<< endl;
    }
	cout << "\\hline" << endl;
	cout << "SF2(FR) &";
    for (int i = 0; i < r_data_MC2_etabins.size(); i++) {
    	cout << r_data_MC2_etabins.at(i).n_fr << " $\\pm$ "<<  r_data_MC2_etabins.at(i).e_fr ;
	if (i != r_data_MC2_etabins.size()-1) cout << " & ";
	else cout << "\\\\"<< endl;
	h_SF_etabins->SetBinContent(i+1, r_data_MC2_etabins.at(i).n_fr);
	h_SF_etabins->SetBinError(i+1, r_data_MC2_etabins.at(i).e_fr);
    }
	cout << "\\hline" << endl;
	cout << "\\hline" << endl;
	cout << "\\end{tabular}}" << endl;
    }

    string savename = "results/EGammaFakeRates_EF1_zee";
    if (DoPt) savename += "_Pt";
    if (DoEta) savename += "_Eta";
    if (DoReverse) savename += "_Reverse";
    if (MCTemp) savename += "_MCTemp";
    if (RShrink) savename += "_RShrink";
    if (RExpand) savename += "_RExpand";
    if (BkgGaus) savename += "_BkgGaus";
    else savename += "_BkgPoly";
    if (ZGamma) savename += "_ZGamma";
    if (DiffMean) savename += "_DiffMean";
    if (DoEGammaCali1) savename += "_EGammaCali1";
    if (DoEGammaCali2) savename += "_EGammaCali2";
    if (DoEGammaCali3) savename += "_EGammaCali3";
    if (DoEGammaCali4) savename += "_EGammaCali4";
    savename += ".root";
    cout << endl;
    cout << "SAVE results to -> " << savename << endl;
    cout << endl;
    TFile* f_output = new TFile(savename.c_str(), "recreate");
    h_SF->Write();
    if (DoPt) {
    h_data_ptbins->Write();
    h_MC_TypeA_ptbins->Write();
    h_MC_TypeB_ptbins->Write();
    h_MC_TypeC_ptbins->Write();
    h_MC_TypeCp_ptbins->Write();
    h_MC_TypeD_ptbins->Write();
    h_SF_ptbins->Write();
    }
    if (DoEta) {
    h_data_etabins->Write();
    h_MC_TypeA_etabins->Write();
    h_MC_TypeB_etabins->Write();
    h_MC_TypeC_etabins->Write();
    h_MC_TypeCp_etabins->Write();
    h_MC_TypeD_etabins->Write();
    h_SF_etabins->Write();
    }
    //h_data_ptetabins->Write();
    //h_MC_TypeA_ptetabins->Write();
    //h_MC_TypeB_ptetabins->Write();
    //h_MC_TypeC_ptetabins->Write();
    //h_MC_TypeD_ptetabins->Write();

    f_output->Close();
    delete f_output;


}

EGammaResults GetEGammaResultsTTBar(TFile* f_zeg, TFile* f_zee, string ptbin, string etabin, bool reverse, bool isttee, bool cplus) {

    EGammaResults r;

    if (f_zeg==NULL || f_zee == NULL) {
	r.h_mass_zeg = NULL;
	r.h_mass_zee = NULL;
	return r;
    }

	r.h_mass_zeg = NULL;
	r.h_mass_zee = NULL;

	double n_zeg, n_zee;
	double e_zeg, e_zee;
	if (etabin == "-1" && ptbin != "-1") { // pt binned
	    TH1F* h_zeg;
	    if (isttee) h_zeg = (TH1F*)f_zeg->Get("LeadPhPtForFR13TeV_CR1_ejets_Reco_TTBar_Nominal");
	    else h_zeg = (TH1F*)f_zeg->Get("LeadPhPtForFR13TeV_CR1_mujets_Reco_TTBar_Nominal");
	    TH1F* h_zee;
	    if (isttee) {
		if (!reverse) h_zee = (TH1F*)f_zee->Get("SubLepPtForFR13TeV_EF1_ttel_ee_Reco_TTBar_Nominal");
		else h_zee = (TH1F*)f_zee->Get("LeadLepPtForFR13TeV_EF1_ttel_ee_Reco_TTBar_Nominal");
	    } else {
		if (!reverse) h_zee = (TH1F*)f_zee->Get("SubLepPtForFR13TeV_EF1_ttle_mue_Reco_TTBar_Nominal");
		else h_zee = (TH1F*)f_zee->Get("LeadLepPtForFR13TeV_EF1_ttel_mue_Reco_TTBar_Nominal");
	    }
	    if (!h_zeg) {
		cout << "Error! can't find zeg histogram in the file" << endl;
		f_zeg->ls();
		exit(-1);
	    }
	    if (!h_zee) {
		cout << "Error! can't find zee histogram in the file" << endl;
		f_zee->ls();
		exit(-1);
	    }
	    n_zeg = h_zeg->GetBinContent(atoi(ptbin.c_str()));
	    n_zee = h_zee->GetBinContent(atoi(ptbin.c_str()));
	    e_zeg = h_zeg->GetBinError(atoi(ptbin.c_str()));
	    e_zee = h_zee->GetBinError(atoi(ptbin.c_str()));
	} else if (ptbin == "-1" && etabin != "-1") { // eta binned
	    TString Proc;
	    if (cplus) Proc = "Signal";
	    else Proc = "TTBar";
	    TH1F* h_zeg;
	    if (isttee) h_zeg = (TH1F*)f_zeg->Get("LeadPhAbsEtaForFR13TeV_CR1_ejets_Reco_TTBar_Nominal");
	    else h_zeg = (TH1F*)f_zeg->Get("LeadPhAbsEtaForFR13TeV_CR1_mujets_Reco_TTBar_Nominal");
	    TH1F* h_zee;
	    if (isttee) {
		if (!reverse) h_zee = (TH1F*)f_zee->Get("SubLepAbsEtaForFR13TeV_EF1_ttel_ee_Reco_TTBar_Nominal");
		else h_zee = (TH1F*)f_zee->Get("LeadLepAbsEtaForFR13TeV_EF1_ttel_ee_Reco_TTBar_Nominal");
	    } else {
		if (!reverse) h_zee = (TH1F*)f_zee->Get("SubLepAbsEtaForFR13TeV_EF1_ttle_mue_Reco_TTBar_Nominal");
		else h_zee = (TH1F*)f_zee->Get("LeadLepAbsEtaForFR13TeV_EF1_ttel_mue_Reco_TTBar_Nominal");
	    }
	    if (!h_zeg) {
		cout << "Error! can't find zeg histogram in the file" << endl;
		f_zeg->ls();
		exit(-1);
	    }
	    if (!h_zee) {
		cout << "Error! can't find zee histogram in the file" << endl;
		f_zee->ls();
		exit(-1);
	    }
	    n_zeg = h_zeg->GetBinContent(atoi(etabin.c_str()));
	    n_zee = h_zee->GetBinContent(atoi(etabin.c_str()));
	    e_zeg = h_zeg->GetBinError(atoi(etabin.c_str()));
	    e_zee = h_zee->GetBinError(atoi(etabin.c_str()));
	} else if (ptbin != "-1" && etabin != "-1") { // pt-eta binned
	    TString Proc;
	    if (cplus) Proc = "Signal";
	    else Proc = "TTBar";
	    TH2F* h_zeg;
	    h_zeg = (TH2F*)f_zeg->Get("LeadPhPtForFR13TeV_LeadPhAbsEtaForFR13TeV_EF1_zeg_Reco_" + Proc + "_Nominal");
	    TH2F* h_zee;
	    if (!reverse) h_zee = (TH2F*)f_zee->Get("SubLepPtForFR13TeV_SubLepAbsEtaForFR13TeV_EF1_zee_Reco_TTBar_Nominal");
	    else h_zee = (TH2F*)f_zee->Get("LeadLepPtForFR13TeV_LeadLepAbsEtaForFR13TeV_EF1_zee_Reco_TTBar_Nominal");
	    if (!h_zeg) {
		cout << "Error! can't find zeg histogram in the file" << endl;
		f_zeg->ls();
		exit(-1);
	    }
	    if (!h_zee) {
		cout << "Error! can't find zee histogram in the file" << endl;
		f_zee->ls();
		exit(-1);
	    }
	    n_zeg = h_zeg->GetBinContent(atoi(ptbin.c_str()), atoi(etabin.c_str()));
	    n_zee = h_zee->GetBinContent(atoi(ptbin.c_str()), atoi(etabin.c_str()));
	    e_zeg = h_zeg->GetBinError(atoi(ptbin.c_str()), atoi(etabin.c_str()));
	    e_zee = h_zee->GetBinError(atoi(ptbin.c_str()), atoi(etabin.c_str()));
	} else if (ptbin == "-1" && etabin == "-1") { // overall
	    TString Proc;
	    if (cplus) Proc = "Signal";
	    else Proc = "TTBar";
	    TH1F* h_zeg;
	    if (isttee) h_zeg = (TH1F*)f_zeg->Get("LeadPhPtForFR13TeV_CR1_ejets_Reco_"+ Proc + "_Nominal");
	    else h_zeg = (TH1F*)f_zeg->Get("LeadPhPtForFR13TeV_CR1_mujets_Reco_"+ Proc + "_Nominal");
	    TH1F* h_zee;
	    if (isttee) {
		if (!reverse) h_zee = (TH1F*)f_zee->Get("SubLepPtForFR13TeV_EF1_ttel_ee_Reco_TTBar_Nominal");
		else h_zee = (TH1F*)f_zee->Get("LeadLepPtForFR13TeV_EF1_ttel_ee_Reco_TTBar_Nominal");
	    } else {
		if (!reverse) h_zee = (TH1F*)f_zee->Get("SubLepPtForFR13TeV_EF1_ttel_mue_Reco_TTBar_Nominal");
		else h_zee = (TH1F*)f_zee->Get("LeadLepPtForFR13TeV_EF1_ttel_mue_Reco_TTBar_Nominal");
	    }
	    if (!h_zeg) {
		cout << "Error! can't find zeg histogram in the file" << endl;
		f_zeg->ls();
		exit(-1);
	    }
	    if (!h_zee) {
		cout << "Error! can't find zee histogram in the file" << endl;
		f_zee->ls();
		exit(-1);
	    }
	    n_zeg = 0; n_zee = 0; e_zeg = 0; e_zee = 0;
	    for (int i = 0; i <= h_zeg->GetNbinsX() + 1; i++) {
		n_zeg += h_zeg->GetBinContent(i);
		e_zeg += pow(h_zeg->GetBinError(i),2);
		n_zee += h_zee->GetBinContent(i);
		e_zee += pow(h_zee->GetBinError(i),2);
	    }
	    e_zeg = sqrt(e_zeg);
	    e_zee = sqrt(e_zee);
	}
	r.n_zeg = n_zeg;
	r.n_zee = n_zee;
	r.e_zeg = e_zeg;
	r.e_zee = e_zee;
	CalcuRatio(r.n_fr, r.e_fr, r.n_zeg, r.e_zeg, r.n_zee, r.e_zee);

    return r;
}

EGammaResults GetEGammaResults(TFile* f_zeg, TFile* f_zee, string ptbin, string etabin, bool isdata, bool reverse, TH1F* h_sig_zeg, TH1F* h_sig_zee, bool cplus) {

    EGammaResults r;

    if (f_zeg==NULL || f_zee == NULL) {
	r.h_mass_zeg = NULL;
	r.h_mass_zee = NULL;
	r.n_zeg = 0;
	r.e_zeg = 0;
	r.n_zee = 0;
	r.e_zee = 0;
	r.n_fr = 0;
	r.e_fr = 0;
	return r;
    }

    string type; 
    if (isdata) type = "Data";
    else type = "Reco";

    string proc;
    if (cplus) proc = "ZGammajetsElElNLO";
    else proc = "ZjetsElEl";

    string name_zeg,name_zee;
    if (etabin == "-1" && ptbin != "-1") { // pt binned
        name_zeg = "InvariantMass_PtBin"; name_zeg += ptbin; 
        name_zee = "InvariantMass_PtBin"; name_zee += ptbin; 
    } else if (ptbin == "-1" && etabin != "-1") { // eta binned
        name_zeg = "InvariantMass_EtaBin"; name_zeg += etabin; 
        name_zee = "InvariantMass_EtaBin"; name_zee += etabin; 
    } else if (ptbin != "-1" && etabin != "-1") { // pt-eta binned
        name_zeg = "InvariantMass_PtBin"; name_zeg += ptbin; name_zeg += "_EtaBin"; name_zeg += etabin; 
        name_zee = "InvariantMass_PtBin"; name_zee += ptbin; name_zee += "_EtaBin"; name_zee += etabin; 
    } else if (ptbin == "-1" && etabin == "-1") { // overall
        name_zeg = "MPhLepForZ";
        name_zee = "MLepLepForZ";
    }

    string hname_zeg = name_zeg; hname_zeg += "_EF1_zeg_"; hname_zeg += type; if (type == "Reco") {hname_zeg += "_"; hname_zeg += proc;} hname_zeg += "_Nominal";
    string hname_zee = name_zee; hname_zee += "_EF1_zee_"; hname_zee += type; if (type == "Reco") {hname_zee += "_"; hname_zee += proc;} hname_zee += "_Nominal";

    TH1F* h_zeg = (TH1F*)f_zeg->Get(hname_zeg.c_str());
    TH1F* h_zee = (TH1F*)f_zee->Get(hname_zee.c_str());
    if (!h_zeg) {
        cout << "Error! can't find zeg histogram " << hname_zeg << " in the file" << endl;
        f_zeg->ls();
        exit(-1);
    }
    if (!h_zee) {
        cout << "Error! can't find zee histogram " << hname_zee << " in the file" << endl;
        f_zee->ls();
        exit(-1);
    }
    r.h_mass_zeg = h_zeg;
    r.h_mass_zee = h_zee;

    FitMassPeak(r.n_zeg, r.e_zeg, h_zeg, ptbin, etabin, reverse, h_sig_zeg, isdata);
    FitMassPeak(r.n_zee, r.e_zee, h_zee, ptbin, etabin, reverse, h_sig_zee, isdata);
    CalcuRatio(r.n_fr, r.e_fr, r.n_zeg, r.e_zeg, r.n_zee, r.e_zee);

    return r;
}

EGammaResults SubtractEGammaResults (EGammaResults A, EGammaResults B) {
    EGammaResults C;
//    if (A.h_mass_zeg != NULL && B.h_mass_zeg != NULL) {
//	cout << A.h_mass_zeg << endl;
//	C.h_mass_zeg = (TH1F*)A.h_mass_zeg->Clone();
//	cout << B.h_mass_zeg << endl;
//    	TH1F* htmp_zeg = (TH1F*)B.h_mass_zeg->Clone();
//    cout << "tag1" << endl;
//    	for (int i = 0; i < htmp_zeg->GetNbinsX(); i++) {
//    	    C.h_mass_zeg->SetBinContent(i+1, A.h_mass_zeg->GetBinContent(i+1) - B.h_mass_zeg->GetBinContent(i+1));
//    	    C.h_mass_zeg->SetBinError(i+1, sqrt(pow(A.h_mass_zeg->GetBinError(i+1),2) + pow(B.h_mass_zeg->GetBinError(i+1),2)));
//    	}
//    cout << "tag2" << endl;
//    }
//    if (A.h_mass_zee != NULL && B.h_mass_zee != NULL) {
//	C.h_mass_zee = (TH1F*)A.h_mass_zee->Clone();
//    	TH1F* htmp_zee = (TH1F*)B.h_mass_zee->Clone();
//    cout << "tag3" << endl;
//    	for (int i = 0; i < htmp_zee->GetNbinsX(); i++) {
//    	    C.h_mass_zee->SetBinContent(i+1, A.h_mass_zee->GetBinContent(i+1) - B.h_mass_zee->GetBinContent(i+1));
//    	    C.h_mass_zee->SetBinError(i+1, sqrt(pow(A.h_mass_zee->GetBinError(i+1),2) + pow(B.h_mass_zee->GetBinError(i+1),2)));
//    	}
//    cout << "tag4" << endl;
//    }
    C.n_zeg = A.n_zeg - B.n_zeg;
    C.n_zee = A.n_zee; //- B.n_zee;
    C.e_zeg = sqrt(pow(A.e_zeg,2) + pow(B.e_zeg,2));
    //C.e_zee = sqrt(pow(A.e_zee,2) + pow(B.e_zee,2));
    C.e_zee = A.e_zee; //- B.n_zee;
    CalcuRatio(C.n_fr, C.e_fr, C.n_zeg, C.e_zeg, C.n_zee, C.e_zee);
    return C;
}

EGammaResults SumEGammaResults (vector<EGammaResults> vr) {
    EGammaResults rs;
    for (int i = 0; i < vr.size(); i++) {
	if (i == 0) {
	    if (vr.at(i).h_mass_zeg) rs.h_mass_zeg = (TH1F*)vr.at(i).h_mass_zeg->Clone();
	    else rs.h_mass_zeg = NULL;
	    if (vr.at(i).h_mass_zee) rs.h_mass_zee = (TH1F*)vr.at(i).h_mass_zee->Clone();
	    else rs.h_mass_zee = NULL;
	    rs.n_zeg = vr.at(i).n_zeg;
	    rs.n_zee = vr.at(i).n_zee;
	    rs.e_zeg = vr.at(i).e_zeg;
	    rs.e_zee = vr.at(i).e_zee;
	} else {
	    if (rs.h_mass_zeg) rs.h_mass_zeg->Add(vr.at(i).h_mass_zeg);
//	    rs.h_mass_zee->Add(vr.at(i).h_mass_zee);
	    rs.n_zeg += vr.at(i).n_zeg;
//	    rs.n_zee += vr.at(i).n_zee;
	    rs.e_zeg = sqrt(pow(rs.e_zeg,2) + pow(vr.at(i).e_zeg,2));
////	    rs.e_zee = sqrt(pow(rs.e_zee,2) + pow(vr.at(i).e_zee,2));
	}
    }
    CalcuRatio(rs.n_fr, rs.e_fr, rs.n_zeg, rs.e_zeg, rs.n_zee, rs.e_zee);
    return rs;
}

EGammaResults DivideEGammaResults (EGammaResults n1, EGammaResults n2) {
    EGammaResults r;
    //r.h_mass_zeg = (TH1F*)n1.h_mass_zeg->Clone();
    //r.h_mass_zeg->Divide(n2.h_mass_zeg);
    //r.h_mass_zee = (TH1F*)n1.h_mass_zee->Clone();
    //r.h_mass_zee->Divide(n2.h_mass_zee);
    CalcuRatio(r.n_zeg, r.e_zeg, n1.n_zeg, n1.e_zeg, n2.n_zeg, n2.e_zeg);
    CalcuRatio(r.n_zee, r.e_zee, n1.n_zee, n1.e_zee, n2.n_zee, n2.e_zee);
    CalcuRatio(r.n_fr, r.e_fr, r.n_zeg, r.e_zeg, r.n_zee, r.e_zee);
    return r;
}

void CalcuRatio(double &nr, double &er, double n1, double e1, double n2, double e2) {
    if (n2 != 0) {
	nr = n1/n2;
	double rel_er = sqrt(pow(e1/n1,2) + pow(e2/n2,2));
	er = nr * rel_er;
    } else {
	nr = 0;
	er = 0;
    }
}

void FitMassPeakMC(double &n_z, double &e_z, TH1F* h, string ptbin, string etabin, bool reverse) {

    if (h->Integral() == 0) {
	n_z = 0;
	e_z = 0;
	return;
    }

    bool iszeg = false;
    if (string(h->GetName()).find("_zeg_", 0) != string::npos) iszeg = true;

    // fit range
    double x_lo = 70;
    double x_hi = 110;
    if (!reverse) {
	if (ptbin == "6") {
	    x_lo = 80;
	    x_hi = 120;
	} else if (ptbin == "7") {
	    x_lo = 90;
	    x_hi = 130;
	} else if (ptbin == "8" || ptbin == "9" || ptbin == "10") {
	    x_lo = 100;
	    x_hi = 140;
	}
    }
    if (RShrink) {
	x_lo += 10;
	x_hi -= 10;
    } else if (RExpand) {
	x_lo -= 10;
	x_hi += 10;
    }

    int bin_lo = h->FindBin(x_lo);
    int bin_hi = h->FindBin(x_hi);

    n_z = 0;
    e_z = 0;
    for (int i = 1; i <= h->GetNbinsX(); i++) {
	if (i < bin_lo) continue;
	if (i >= bin_hi) continue;
	n_z += h->GetBinContent(i);
	e_z += pow(h->GetBinError(i),2);
    }
    e_z = sqrt(e_z);
}

void FitMassPeak(double &n_z, double &e_z, TH1F* h, string ptbin, string etabin, bool reverse, TH1F* h_sig, bool isdata) {

    if (h->Integral() == 0) {
	n_z = 0;
	e_z = 0;
	return;
    }

    bool iszeg = false;
    if (string(h->GetName()).find("_zeg_", 0) != string::npos) iszeg = true;

    RooMsgService::instance().setSilentMode(kTRUE);
    RooMsgService::instance().setGlobalKillBelow(WARNING);
    gStyle->SetOptFit(1111);

    // fit range
    double x_lo = 70;
    double x_hi = 110;
    if (!reverse) {
	if (ptbin == "6") {
	    x_lo = 80;
	    x_hi = 120;
	} else if (ptbin == "7") {
	    x_lo = 90;
	    x_hi = 130;
	} else if (ptbin == "8" || ptbin == "9" || ptbin == "10") {
	    x_lo = 100;
	    x_hi = 140;
	}
    }
    if (RShrink) {
	x_lo += 10;
	x_hi -= 10;
    } else if (RExpand) {
	x_lo -= 10;
	x_hi += 10;
    }

    if (!isdata) {
	int bin_lo = h->FindBin(x_lo);
    	int bin_hi = h->FindBin(x_hi);

    	n_z = 0;
    	e_z = 0;
    	for (int i = 1; i <= h->GetNbinsX(); i++) {
    	    if (i < bin_lo) continue;
    	    if (i >= bin_hi) continue;
    	    n_z += h->GetBinContent(i);
    	    e_z += pow(h->GetBinError(i),2);
    	}
    	e_z = sqrt(e_z);
    } else {
	RooRealVar x("x","x",x_lo,x_hi) ;
    	x.setBins(x_hi-x_lo);
    	if (!h) {
    	    cout << "ERROR! empty data histogram!" << endl;
    	}

    	// data
    	RooDataHist *data = new RooDataHist("data", "data", x, h);

    	// signal template
    	RooDataHist* rdh_sig = NULL;
    	RooHistPdf *rhp_sig = NULL;
    	RooRealVar *m  = NULL;
    	RooRealVar *s  = NULL;
    	RooRealVar *a  = NULL;
    	RooRealVar *n  = NULL;
    	RooCBShape *cb = NULL;
    	RooRealVar *nsig = new RooRealVar("nsig", "nsig", h->Integral()*0.9, 0, 2*h->Integral());
    	RooExtendPdf *sig = NULL;
    	int nFloatParam = 0;
    	if (h_sig == NULL || !MCTemp) {
    	    nFloatParam += 5;
    	    m = new RooRealVar("m","m",(x_hi+x_lo)/2.,x_lo,x_hi) ;
    	    s = new RooRealVar("s","s",3,0,20) ;
    	    if (!reverse && (ptbin == "5" || ptbin == "6" || ptbin == "7"|| ptbin == "8"|| ptbin == "9"|| ptbin == "10")) {
    	        a = new RooRealVar("a","a",-1.,-20.,0.);
    	    } else if (reverse && (ptbin == "7" || ptbin == "8"|| ptbin == "9"|| ptbin == "10" || ptbin == "11")) {
    	        a = new RooRealVar("a","a",-1.,-20.,0.);
    	    } else  {
    	        a = new RooRealVar("a","a",1.,0.01,20.);
    	    }
    	    n = new RooRealVar("n","n",2,0.01,100) ;
    	    
    	    cb = new RooCBShape("cb", "Crystal Ball", x, *m, *s, *a ,*n);
    	    sig = new RooExtendPdf("sig", "sig", *cb, *nsig);
    	} else {
    	    nFloatParam += 1;
    	    rdh_sig = new RooDataHist("rdhsig", "rdhsig", x, h_sig);
    	    rhp_sig = new RooHistPdf("rhpsig", "rhpsig", x, *rdh_sig);
    	    sig = new RooExtendPdf("sig", "sig", *rhp_sig, *nsig);
    	}

    	// bkg template
    	double mean_cen = 90;
    	double sigma_cen = 20;
    	double sigma_hi = 40;
    	double sigma_lo = 5;
    	if (!iszeg) sigma_cen = 30;
    	RooRealVar *mean = NULL;
    	RooRealVar *sigma = NULL;
    	RooGaussian *bkg_gaus = NULL;
    	RooRealVar *a0 = NULL;
    	RooRealVar *a1 = NULL;
    	RooRealVar *a2 = NULL;
    	RooPolynomial *bkg_poly2 = NULL;
    	RooRealVar *nbkg = new RooRealVar("nbkg", "nbkg", h->Integral()*0.1, 0, h->Integral());
    	RooExtendPdf *bkg = NULL;
    	if (BkgGaus) {
    	    if (MCTemp || DiffMean) {
    	        nFloatParam += 3;
    	        if (ptbin == "6" && !reverse) {
    	    	mean = new RooRealVar("mean","mean of gaussians",mean_cen,80,110) ;
    	        } else {
    	    	mean = new RooRealVar("mean","mean of gaussians",mean_cen,80,100) ;
    	        }
    	    } else nFloatParam += 2;
    	    sigma = new RooRealVar("sigma","width of gaussians",sigma_cen,sigma_lo,sigma_hi) ;

    		if (MCTemp || DiffMean) {
    	        bkg_gaus = new RooGaussian("bkg_gaus","Gaussian Function", x, *mean, *sigma);
    		} else {
    	        bkg_gaus = new RooGaussian("bkg_gaus","Gaussian Function", x, *m, *sigma);
    	    }
    	    bkg = new RooExtendPdf("bkg", "bkg", *bkg_gaus, *nbkg);
    	} else {
    	    nFloatParam += 4;
    	    a0 = new RooRealVar("a0", "a0", 0.01, -10, 10);
    	    a1 = new RooRealVar("a1", "a1", 0.01, -10, 10);
    	    a2 = new RooRealVar("a2", "a2", 0.01, -10, 10);
    	    bkg_poly2 = new RooPolynomial("p2", "p2", x, RooArgList(*a0,*a1,*a2),0);
    	    bkg = new RooExtendPdf("bkg", "bkg", *bkg_poly2, *nbkg);
    	}
    	
    	// fit
    	RooAddPdf *model = new RooAddPdf("model", "model", RooArgList(*sig, *bkg));
    	model->fitTo(*data) ;
    	RooFitResult *results = model->fitTo(*data, RooFit::Save());
    	//results->Print();

    	n_z = nsig->getVal();
    	e_z = nsig->getError();

    	// save post-fit results
    	double norm_sig = nsig->getVal();
    	double norm_bkg = nbkg->getVal();
    	RooAbsReal* int_sig = sig->createIntegral(x,NormSet(x),Range("cut"));

    	int bin_lo = h->FindBin(x_lo);
    	int bin_hi = h->FindBin(x_hi);
    	int nbin = bin_hi - bin_lo;
    	TH1F* h_postfit_sig = new TH1F("Postfit_sig","Postfit_sig",nbin,x_lo,x_hi);
    	TH1F* h_postfit_bkg = new TH1F("Postfit_bkg","Postfit_bkg",nbin,x_lo,x_hi);
    	for (int i = 1; i <= nbin; i++) {
    	    x.setRange("tmprange",h_postfit_sig->GetBinLowEdge(i),h_postfit_sig->GetBinLowEdge(i)+h_postfit_sig->GetBinWidth(i));
    	    RooAbsReal* int_sig = sig->createIntegral(x,NormSet(x),Range("tmprange"));
    	    RooAbsReal* int_bkg = bkg->createIntegral(x,NormSet(x),Range("tmprange"));
    	    double bin_cnt_sig = norm_sig * int_sig->getVal();
    	    double bin_cnt_bkg = norm_bkg * int_bkg->getVal();
    	    h_postfit_sig->SetBinContent(i, bin_cnt_sig);
    	    h_postfit_bkg->SetBinContent(i, bin_cnt_bkg);
    	}

    	// draw
    	RooPlot* xframe = x.frame(Title("example")) ;
    	data->plotOn(xframe) ;
    	model->plotOn(xframe) ;
    	if (BkgGaus) {
    	    model->plotOn(xframe,Components(*bkg),LineStyle(kDashed)) ;
    	    model->plotOn(xframe,Components(RooArgSet(*bkg,*sig)),LineStyle(kDotted)) ;
    	} else {
    	    model->plotOn(xframe,Components(*bkg),LineStyle(kDashed)) ;
    	    model->plotOn(xframe,Components(RooArgSet(*bkg,*sig)),LineStyle(kDotted)) ;
    	}

    	string region;
    	if (iszeg) region = "e#gamma CR";
    	else region = "ee CR";
    	if (ptbin != "-1") {
    	    region += " Ptbin "; 
    	    int n_b = atoi(ptbin.c_str());
    	    char tmpchar[100];
    	    sprintf(tmpchar, "[%.0f,%.0f] GeV", m_egammaptbins_lo.at(n_b-1), m_egammaptbins_hi.at(n_b-1));
    	    region += tmpchar;
    	}
    	if (etabin != "-1") {
    	    region += " Etabin "; 
    	    int n_b = atoi(etabin.c_str());
    	    char tmpchar[100];
    	    sprintf(tmpchar, "[%.0f,%.0f]", m_egammaetabins_lo.at(n_b-1), m_egammaetabins_hi.at(n_b-1));
    	    region += tmpchar;
    	}
    	xframe->SetTitle(region.c_str());
    	xframe->GetXaxis()->SetTitle("Invariant mass");

    	TCanvas *tmpc = new TCanvas("tmp", "tmp", 800, 700);
    	xframe->Draw();
    	RooHist* rhdata = (RooHist*)tmpc->GetPrimitive("h_data");
    	RooCurve* rcbkg = (RooCurve*)tmpc->GetPrimitive("model_Norm[x]_Comp[bkg]");
    	RooCurve* rcsigbkg = (RooCurve*)tmpc->GetPrimitive("model_Norm[x]");
    	TLegend *lg = new TLegend(0.65,0.65,0.85,0.85);
    	lg->SetBorderSize(0);
    	lg->SetFillColor(0);
    	lg->AddEntry(rhdata, "Data", "P");
    	lg->AddEntry(rcbkg, "Bkg", "l");
    	lg->AddEntry(rcsigbkg, "Sig+Bkg", "l");
    	lg->Draw("same");

    	Double_t chi2 = xframe->chiSquare(nFloatParam);
    	cout << "Fit chi2/dof = " << chi2 << endl;
    	char tmp_ch[100];
    	sprintf(tmp_ch, "chi2/dof=%.1f", chi2);
    	TLatex lt;
    	lt.SetNDC();
    	lt.SetTextFont(42);
    	lt.DrawLatex(0.66, 0.59, tmp_ch);

    	TString variation;
    	if (!reverse) {
    	    variation = "Nominal";
    	    if (MCTemp) variation = "MCTemplate";
    		else if (RExpand) variation = "[60,120]";
    		else if (RShrink) variation = "[80,100]";
    	} else {
    	    variation = "Nominal[R]";
    	    if (MCTemp) variation = "MCTemplate[R]";
    		else if (RExpand) variation = "[60,120][R]";
    		else if (RShrink) variation = "[80,100][R]";
    	}

    	double m_lt_x = 0.15;
    	double lt_y = 0.8;
    	lt.SetTextFont(72);
    	lt.DrawLatex(m_lt_x, lt_y, "ATLAS");
    	lt.SetTextFont(42);
    	double delx = 0.115*696*tmpc->GetWh()/(472*tmpc->GetWw());
    	lt.DrawLatex(m_lt_x+delx, lt_y, "Internal");
    	double ystep = -0.08;
    	lt_y += ystep;
    	lt.DrawLatex(m_lt_x, lt_y, "#sqrt{s}=13TeV");
    	lt_y += ystep;
    	lt.DrawLatex(m_lt_x, lt_y, "#intL dt = 36.1 fb^{-1}");
    	lt_y += ystep;
    	lt.DrawLatex(m_lt_x, lt_y, variation.Data());
    	lt_y += ystep;
    	
    	string savename = "plots/Postfit_Zpeak"; 
    	if (etabin == "-1" && ptbin != "-1") { // pt binned
    	    savename += "_Ptbin"; savename += ptbin;
    	} else if (ptbin == "-1" && etabin != "-1") { // eta binned
    	    savename += "_Etabin"; savename += etabin;
    	} else if (ptbin != "-1" && etabin != "-1") { // pt-eta binned
    	    savename += "_Ptbin"; savename += ptbin;
    	    savename += "_Etabin"; savename += etabin;
    	}
    	if (iszeg) savename += "_EF1_zeg";
    	else savename += "_EF1_zee";
    	if (reverse) savename += "_Reverse";
    	if (MCTemp) savename += "_MCTemp";
    	if (RExpand) savename += "_RExpand";
    	if (RShrink) savename += "_RShrink";
    	if (BkgGaus) savename += "_BkgGaus";
    	else savename += "_BkgPoly";
    	if (DiffMean) savename += "_DiffMean";
    	tmpc->SaveAs(string(savename + string(".pdf")).c_str());
    	tmpc->SaveAs(string(savename + string(".png")).c_str());

    	// save weight histogram (for data)
    	if (iszeg && ptbin == "-1" && etabin == "-1" && !reverse && !RExpand && !RShrink && BkgGaus && !DiffMean) {
    	    string savename = "results/InvariantMass_Weight_EGammaFake";
    	    if (MCTemp) savename += "_MCTemp";
    	    savename += ".root";
    	    cout << "save weight to file ->" << savename << endl; 
    	    TFile*fout = new TFile(savename.c_str(), "recreate");
    		TH1F* h_ratio = (TH1F*)h_postfit_sig->Clone();
    	    for (int i = 1; i <= h_postfit_sig->GetNbinsX(); i++) {
    	        double i_data = h->GetBinContent(h->FindBin(h_postfit_sig->GetBinCenter(i)));
    	        double i_sig = h_postfit_sig->GetBinContent(i);
    	        h_ratio->SetBinContent(i, i_sig/i_data);
    	        //cout << i_data << " " << i_sig << " " << h_postfit_bkg->GetBinContent(i) << " " << i_sig/i_data << endl;
    	    }
    		//if (MCTemp) {
    		//    if (iszeg) {
    		//	    fout = new TFile("results/Results_Others_Reco_FULL_13TeV_EF1_zeg_CutZeg_MCTemp_Lumiweighted_" + TString(version.c_str()) + ".root",option_zeg.c_str());
    		//	} else {
    		//	    fout = new TFile("results/Results_Others_Reco_FULL_13TeV_EF1_zee_CutZee_MCTemp_Lumiweighted_" + TString(version.c_str()) + ".root",option_zee.c_str());
    		//	}
    		//} else {
    		//    if (iszeg) {
    		//	    fout = new TFile("results/Results_Others_Reco_FULL_13TeV_EF1_zeg_CutZeg_Lumiweighted_" + TString(version.c_str()) + ".root",option_zeg.c_str());
    		//	} else {
    		//	    fout = new TFile("results/Results_Others_Reco_FULL_13TeV_EF1_zee_CutZee_Lumiweighted_" + TString(version.c_str()) + ".root",option_zee.c_str());
    		//	}
    		//}
    		//if (option_zeg == "recreate") option_zeg = "update";
    		//name.ReplaceAll("Reco_ZjetsElEl", "Others");
    	    //if (iszeg) {
    		//    h_bkg->Write("MPhLepForZ_EF1_zeg_Reco_Others_Nominal");
    		//} else {
    		//    h_bkg->Write("MLepLepForZ_EF1_zee_Reco_Others_Nominal");
    		//}
    		//h_bkg->Write(name);
    	    h_ratio->Write("Weight");
    	    fout->Close();
    	    delete fout;
    	}

    	//model.Print("t") ;

    	// Draw the frame on the canvas



    	delete tmpc;
    	delete data;
    	delete m;
    	delete s;
    	delete a;
    	delete n;
    	delete sig;
    	if (BkgGaus) {
    	    //delete mean;
    		delete sigma;
    		delete bkg_gaus;
    	} else {
    	    delete a0;
    	    delete a1;
    	    delete a2;
    	    delete bkg_poly2;
    	}
    	//if (iszeg && ptbin == "2") {
    	//    delete mean2;
    	//	delete sigma2;
    	//	delete bkg2;
    	//	delete sigfrac2;
    	//}
    	//cout << "fit finished" << endl;
    }
}

void PrintEGammaResults(EGammaResults r) {
    cout << setw(10) << r.n_zeg << " $\\pm$ " << setw(10) << r.e_zeg << " & " << setw(10) << r.n_zee << " $\\pm$ " << setw(10) << r.e_zee << " & " << setw(10) << r.n_fr << " $\\pm$ " << r.e_fr << " \\\\" << endl;
}

void PrintEGammaResults(EGammaResults r, ofstream &ofile) {
    ofile << setw(10) << r.n_zeg << " $\\pm$ " << setw(10) << r.e_zeg << " & " << setw(10) << r.n_zee << " $\\pm$ " << setw(10) << r.e_zee << " & " << setw(10) << r.n_fr << " $\\pm$ " << r.e_fr << " \\\\" << endl;
}
