#include "WorkspaceAnalyzor.h"
#include "StringPlayer.h"
#include <TMarker.h>
#include "AtlasStyle.h"

using namespace RooFit;
using namespace RooStats;
using namespace std;

WorkspaceAnalyzor::WorkspaceAnalyzor() {
    m_lg = new Logger("WS Analyzor");
    m_lg->Info("s", "Hi, this is a Workspace Analyzor~");

    // flag
    m_inverse_mode = false;
    m_ref_poi = 1;
    m_debug = false;

    // config
    m_filename = "";
    m_wsname = "";
    m_mcname = "";
    m_dataname = "";
    m_use_asimov = false;

    // initialize
    m_infile = NULL;
    m_obss = new RooArgSet();
    m_model = NULL;
    m_dataset = NULL;
    m_workspace = NULL;
    m_modelconfig = NULL;

    m_obs_title = "";
    m_obs_unit = "";
    m_obs_low = -999;
    m_obs_high = -999;
    m_obs_nbin = -999;
    m_obs = NULL;

    m_chan_names = new vector<string>();
    m_chan_titles = new vector<string>();
    m_chans = new RooCategory("Channels","Channels");
    m_chants = new RooCategory("ChannelTitles","ChannelTitles");
    m_chan_n = 0;

    m_proc_n = new vector<int>();
    m_proc_names = new vector<vector<string>* >();
    m_proc_titles = new vector<vector<string>* >();
    m_procs = new vector<RooCategory*>();
    m_procts = new vector<RooCategory*>();
    h_proc_issigs = NULL;
    h_proc_isfrees = NULL;

    m_firstpoi = NULL;
    m_secondpoi = NULL;
    m_multipoi = false;
    m_pois = new RooArgSet();
    m_nuiss = new RooArgSet();
    m_nonsysnuiss = new RooArgSet();
    m_sysnuiss = new RooArgSet();
    m_sys_names = new vector<string>();
    m_sys_titles = new vector<string>();
    m_sys_n = 0;
    m_paras = new RooArgSet();
    m_functions = new RooArgSet();
    m_functions_new = new RooArgSet();

    m_proc_temps = new vector<vector<TH1F*>*>();
    m_datas = new vector<TH1F*>();
    m_data_nums = new vector<float>();

    // fit
    m_fit_results = NULL;
    m_PLC = NULL;
    m_LI = NULL;
    m_firstpoi_cen = -999;
    m_firstpoi_low = -999;
    m_firstpoi_high = -999;
    m_secondpoi_cen = -999;
    m_secondpoi_low = -999;
    m_secondpoi_high = -999;

    // save
    m_savedir = "results/";
    m_savename = "Results_Default.root";
    m_saveoption = "recreate";
    m_plotdir = "plots/";
    h_fit_results_plc = new TH1F("ResultsPLC", "ResultsPLC", 20, 0, 20);
    h_fit_results_poi = new TH1F("ResultsPOI", "ResultsPOI", 20, 0, 20);

    m_pull_order = "";

    //m_savename = "";
    //m_appendname = "_Nominal";

    //m_NLL = NULL;
    //h_pull_axis = NULL;
    //g_pull = NULL;

    //m_toy_ntoy = 100;
    //m_toy_fitrange = 5;
    //m_toy_fitbinning = 50;
    //m_toy_cnt = 0;
    //m_toy_nevt = 1;
    //h_err_decomp = new TH1F("err_decomp", "err_decomp", 500, 0, 500);

    //m_fit_stored = false;
    //m_doplc_done = false;
    //m_dohypo_done = false;
}

void WorkspaceAnalyzor::ShutUp() {
    m_lg->ShutUp();
}

void WorkspaceAnalyzor::UsePullOrder(string order) {
    m_pull_order = order;
}

void WorkspaceAnalyzor::SetReferencePOI(int ref) {
    m_ref_poi = ref;
}

void WorkspaceAnalyzor::SetFileName(string name) {
    m_filename = name;
}

void WorkspaceAnalyzor::SetWSName(string name) {
    m_wsname = name;
}

void WorkspaceAnalyzor::SetMCName(string name) {
    m_mcname = name;
}

void WorkspaceAnalyzor::Debug() {
    m_debug = true;
}

void WorkspaceAnalyzor::SetDataName(string name) {
    m_dataname = name;
}

void WorkspaceAnalyzor::SetInverseMode(bool inverse) {
    m_inverse_mode = inverse;
}

void WorkspaceAnalyzor::SetSilentMode(bool silent) {
    m_silent_mode = silent;
}

void WorkspaceAnalyzor::SetFuncFilter(vector<string> filt) {
    m_funcfilter = filt;
}

void WorkspaceAnalyzor::SetPlotDir(string plotdir) {
    m_plotdir = plotdir;
}

vector<string>* WorkspaceAnalyzor::GetSysNames() {
    return m_sys_names;
}

void WorkspaceAnalyzor::PrintSysNames() {
    m_lg->Info("s", "Print sys names");
    for (int i = 0; i < m_sys_names->size(); i++) {
	cout << m_sys_names->at(i) << endl;
    }
}

void WorkspaceAnalyzor::Init() {

    m_lg->Info("s", "Initialize workspace analyzor.");

    m_infile = new TFile(m_filename.c_str());
    if (!m_infile) { 
	m_lg->Err("ss", "Cannot open file -->", m_filename.c_str()); 
	exit(-1); 
    }

    m_workspace = (RooWorkspace*)m_infile->Get(m_wsname.c_str());
    if (!m_workspace) { 
	m_lg->Err("ss", "Cannot find workspace -->", m_wsname.c_str()); 
	exit(-1); 
    }

    m_modelconfig = (ModelConfig*)m_workspace->obj(m_mcname.c_str());
    if (!m_modelconfig) { 
	m_lg->Err("ss", "Cannot find model config -->", m_mcname.c_str()); 
	exit(-1); 
    }

    m_dataset = (RooAbsData*)m_workspace->data(m_dataname.c_str());
    if (!m_dataset) { 
	m_lg->Err("ss", "Cannot find data set -->", m_dataname.c_str()); 
	exit(-1); 
    }

    m_model = (RooAbsPdf*)m_modelconfig->GetPdf();
    if (!m_model) {	
	m_lg->Err("s", "Cannot find model!"); 
	exit(-1); 
    }
    
    if (m_debug) m_lg->Info("s", "Get observable");
    m_obs = (RooRealVar*)m_workspace->var("Observable");
    m_obs_title = m_obs->getTitle();
    m_obs_unit = m_obs->getUnit();
    m_obs_low = m_obs->getMin();
    m_obs_high = m_obs->getMax();
    m_obs_nbin = m_obs->numBins();
    {
        TIterator* it = m_modelconfig->GetObservables()->createIterator();
        RooRealVar *tmpvar;
    	while ((tmpvar = (RooRealVar*)it->Next())) {
            if (!tmpvar) continue;
            m_obss->add(*tmpvar);
        }
        delete tmpvar;
    }

    if (m_debug) m_lg->Info("s", "Get channel");
    m_chans = (RooCategory*)m_workspace->cat("Channels");
    m_chants = (RooCategory*)m_workspace->cat("ChannelTitles");
    {
        TIterator* iter = m_chans->typeIterator();
        RooCatType* tt = NULL;
        while (tt = (RooCatType*)iter->Next()) {
            if (!tt) continue;
            m_chan_names->push_back(tt->GetName());
            m_chan_n++;
	}
    }
    {
        TIterator* iter = m_chants->typeIterator();
        RooCatType* tt = NULL;
        while (tt = (RooCatType*)iter->Next()) {
            if (!tt) continue;
            m_chan_titles->push_back(tt->GetName());
	}
    }

    if (m_debug) m_lg->Info("s", "Get process");
    m_procs->resize(m_chan_n);
    m_procts->resize(m_chan_n);
    m_proc_n->resize(m_chan_n);
    m_proc_names->resize(m_chan_n);
    m_proc_titles->resize(m_chan_n);
    h_proc_issigs = (TH2F*)m_workspace->obj("ProcIsSig");
    h_proc_isfrees = (TH2F*)m_workspace->obj("ProcIsFree");
    for (int i = 0; i < m_chan_n; i++) {
	m_procs->at(i) = (RooCategory*)m_workspace->cat((string("Proc")+m_chan_names->at(i)).c_str());
	m_procts->at(i) = (RooCategory*)m_workspace->cat((string("ProcTit")+m_chan_names->at(i)).c_str());
	m_proc_names->at(i) = new vector<string>();
	m_proc_titles->at(i) = new vector<string>();
	{
    	    TIterator* iter = m_procs->at(i)->typeIterator();
    	    RooCatType* tt = NULL;
    	    while (tt = (RooCatType*)iter->Next()) {
    	        if (!tt) continue;
    	        m_proc_names->at(i)->push_back(tt->GetName());
    	    }
    	}
	{
    	    TIterator* iter = m_procts->at(i)->typeIterator();
    	    RooCatType* tt = NULL;
    	    while (tt = (RooCatType*)iter->Next()) {
    	        if (!tt) continue;
    	        m_proc_titles->at(i)->push_back(tt->GetName());
    	    }
    	}
	m_proc_n->at(i) = m_proc_names->at(i)->size();
    }
    
    if (m_debug) m_lg->Info("s", "Get poi");
    m_pois = (RooArgSet*)m_modelconfig->GetParametersOfInterest();
    if (m_pois->getSize() > 1) {
	m_multipoi = true;
	TIterator* it = m_pois->createIterator();
	RooRealVar *tmpvar;
	int i = 0;
	while ((tmpvar = (RooRealVar*)it->Next())) {
	    if (!tmpvar) continue;
	    i++;
	    if (i == 1) continue;
	    if (i == 2) {
		m_secondpoi = tmpvar;
		break;
	    }
	}
    }

    if (m_debug) m_lg->Info("s", "Get firstpoi");
    m_firstpoi = (RooRealVar*)m_modelconfig->GetParametersOfInterest()->first();
    m_firstpoi_name = m_firstpoi->GetName();
    m_firstpoi_title = m_firstpoi->GetTitle();
    m_nuiss = (RooArgSet*)m_modelconfig->GetNuisanceParameters();
    m_paras->add(*m_pois); m_paras->add(*m_nuiss);
    m_sys_order.clear();
    {
	TIterator* it = m_nuiss->createIterator();
	RooRealVar *tmpvar;
	int i = 0;
	while ((tmpvar = (RooRealVar*)it->Next())) {
	    if (!tmpvar) continue;
	    if (string(tmpvar->GetName()).find("Sys",0) != string::npos) {
		m_sysnuiss->add(*tmpvar);
		m_sys_order.push_back(tmpvar->GetName());
	      	m_sys_names->push_back(tmpvar->GetName());
	        m_sys_titles->push_back(tmpvar->getTitle().Data());
	    } else m_nonsysnuiss->add(*tmpvar);
	}
	m_sys_n = m_sys_names->size();
    }
    {
	TIterator* it = m_paras->createIterator();
	RooRealVar *tmpvar;
	int i = 0;
	while ((tmpvar = (RooRealVar*)it->Next())) {
	    if (!tmpvar) continue;
	    m_prefit_val.insert(pair<string,float> (tmpvar->GetName(), tmpvar->getVal()));
	    m_prefit_err.insert(pair<string,float> (tmpvar->GetName(), tmpvar->getError()));
	}
    }

    if (m_debug) m_lg->Info("s", "template");
    m_proc_temps->resize(m_chan_n);
    for (int i = 0; i < m_chan_n; i++) {
	m_proc_temps->at(i) = new vector<TH1F*>();
	m_proc_temps->at(i)->resize(m_proc_n->at(i));
	for (int j = 0; j < m_proc_n->at(i); j++) {
	    m_proc_temps->at(i)->at(j) = (TH1F*)m_workspace->obj((string("Temp")+m_proc_names->at(i)->at(j)+m_chan_names->at(i)).c_str());
	}
    }

    if (m_debug) m_lg->Info("s", "Get data");
    m_datas->resize(m_chan_n);
    for (int i = 0; i < m_chan_n; i++) {
	m_datas->at(i) = (TH1F*)m_workspace->obj((string("Data")+m_chan_names->at(i)).c_str());
	m_data_nums->push_back(m_datas->at(i)->Integral());
    }
    
    {
	vector<vector<vector<RooArgSet> > > chan_proc_bin;
	chan_proc_bin.resize(m_chan_n);
	for (int i = 0; i < m_chan_n; i++) {
	    chan_proc_bin.at(i).resize(m_proc_n->at(i));
	    for (int j = 0; j < m_proc_n->at(i); j++) {
		chan_proc_bin.at(i).at(j).resize(m_obs_nbin);
	    }
	}

	if (m_debug) m_lg->Info("s", "Get functions");
	RooArgSet *allfunctions = new RooArgSet(m_workspace->allFunctions());
        TIterator* it = allfunctions->createIterator();
        RooFormulaVar *tmpvar;
    	while ((tmpvar = (RooFormulaVar*)it->Next())) {
            if (!tmpvar) continue;
	    if (string(tmpvar->GetName()).find("Tmp",0) != string::npos) continue;
            bool pass = true;
            for (int i = 0; i < m_funcfilter.size(); i++) {
        	if (string(tmpvar->GetName()).find(m_funcfilter.at(i).c_str(), 0) != string::npos) {
        	    pass = false;
        	    break;
        	}
	    }
	    if (!pass) continue;
	    m_functions->add(*tmpvar);

	    int id_bin = -1; int id_chan = -1; int id_proc = -1;
	    DetermineBinChanProc(tmpvar, id_bin, id_chan, id_proc);
	    if (id_bin != -1 && id_chan != -1 && id_proc != -1) chan_proc_bin.at(id_chan).at(id_proc).at(id_bin-1).add(*tmpvar);
	}

	if (m_debug) m_lg->Info("s", "Make new functions");
	for (int i = 0; i < m_chan_n; i++) {
	    RooArgSet chan_sig;
	    RooArgSet chan_bkg;
	    RooArgSet chan_fixed;
	    RooArgSet chan_total;
	    for (int j = 0; j < m_obs_nbin; j++) {
		RooArgSet chan_bin_sig;
		RooArgSet chan_bin_bkg;
	    	RooArgSet chan_bin_fixed;
	    	RooArgSet chan_bin_total;
		for (int k = 0; k < m_proc_n->at(i); k++) {
		    chan_bin_total.add(chan_proc_bin.at(i).at(k).at(j));
		    if (h_proc_issigs->GetBinContent(i+1, k+1) == 0) {
		        chan_bin_bkg.add(chan_proc_bin.at(i).at(k).at(j));
		    } else {
		        chan_bin_sig.add(chan_proc_bin.at(i).at(k).at(j));	
		    }
		    if (h_proc_isfrees->GetBinContent(i+1, k+1) == 0) {
		        chan_bin_fixed.add(chan_proc_bin.at(i).at(k).at(j));
		    }
		}
		char tmp1[100];
		char tmp2[100];
		{
		    sprintf(tmp1, "FinNumTotSigBin%d%s", j+1, m_chan_names->at(i).c_str());
		    sprintf(tmp2, "Final Number of Total Signal in Bin %d of %s", j+1, m_chan_titles->at(i).c_str());
		    RooAddition *tmpvar = new RooAddition(tmp1, tmp2, chan_bin_sig);
		    m_functions_new->add(*tmpvar);
		}
		{
		    sprintf(tmp1, "FinNumTotBkgBin%d%s", j+1, m_chan_names->at(i).c_str());
		    sprintf(tmp2, "Final Number of Total Background in Bin %d of %s", j+1, m_chan_titles->at(i).c_str());
		    RooAddition *tmpvar = new RooAddition(tmp1, tmp2, chan_bin_bkg);
		    m_functions_new->add(*tmpvar);
		}
		{
		    sprintf(tmp1, "FinNumTotFixedBin%d%s", j+1, m_chan_names->at(i).c_str());
		    sprintf(tmp2, "Final Number of Total Fixed in Bin %d of %s", j+1, m_chan_titles->at(i).c_str());
		    RooAddition *tmpvar = new RooAddition(tmp1, tmp2, chan_bin_fixed);
		    m_functions_new->add(*tmpvar);
		}
		{
		    sprintf(tmp1, "FinNumTotEvtBin%d%s", j+1, m_chan_names->at(i).c_str());
		    sprintf(tmp2, "Final Number of Total Event in Bin %d of %s", j+1, m_chan_titles->at(i).c_str());
		    RooAddition *tmpvar = new RooAddition(tmp1, tmp2, chan_bin_total);
		    m_functions_new->add(*tmpvar);
		}
		chan_sig.add(chan_bin_sig);
		chan_bkg.add(chan_bin_bkg);
		chan_fixed.add(chan_bin_fixed);
		chan_total.add(chan_bin_total);
	    }
	    {
		RooAddition *tmpvar = new RooAddition((string("FinNumTotSig")+m_chan_names->at(i)).c_str(),
					          (string("Final Number of Total Signal in ")+m_chan_titles->at(i)).c_str(),
					          chan_sig);
		m_functions_new->add(*tmpvar);
	    }
	    {
		RooAddition *tmpvar = new RooAddition((string("FinNumTotBkg")+m_chan_names->at(i)).c_str(),
					          (string("Final Number of Total Background in ")+m_chan_titles->at(i)).c_str(),
					          chan_bkg);
		m_functions_new->add(*tmpvar);
	    }
	    {
		RooAddition *tmpvar = new RooAddition((string("FinNumTotFix")+m_chan_names->at(i)).c_str(),
					          (string("Final Number of Total Fixed in ")+m_chan_titles->at(i)).c_str(),
					          chan_fixed);
		m_functions_new->add(*tmpvar);
	    }
	    {
		RooAddition *tmpvar = new RooAddition((string("FinNumTotEvt")+m_chan_names->at(i)).c_str(),
	    	    			          (string("Final Number of Total Event in ")+m_chan_titles->at(i)).c_str(),
	    	    			          chan_total);
	    	m_functions_new->add(*tmpvar);
	    }

	    for (int j = 0; j < m_proc_n->at(i); j++) {
		RooArgSet chan_proc;
		for (int k = 0; k < m_obs_nbin; k++) {
		    chan_proc.add(chan_proc_bin.at(i).at(j).at(k));
		}
		RooAddition *tmpvar = new RooAddition((string("FinNum")+m_proc_names->at(i)->at(j)+m_chan_names->at(i)).c_str(),
		    			      (string("Final Number of ")+m_proc_titles->at(i)->at(j)+string(" in ")+m_chan_titles->at(i)).c_str(),
		    			      chan_proc);
		m_functions_new->add(*tmpvar);
	    }
	}

	delete allfunctions;
    }
    
    m_workspace->saveSnapshot("PreFit", *m_paras);

    if (!m_silent_mode) {
	m_lg->Info("s", "============= Initialization Summary ==============");
    	m_lg->Info("s", "------- OBSERVABLE --------");
    	m_lg->Info("ss", "Observable -->", m_obs_title.c_str());
    	m_lg->Info("sfsfss", "Range --> [", m_obs_low, ",", m_obs_high, "] ", m_obs_unit.c_str());
    	m_lg->Info("sd", "Bin number -->", m_obs_nbin);
    	m_lg->Info("s", "---------------------------");

    	m_lg->Info("s", "------- CHANNEL --------");
    	m_lg->Info("sd", "Channel number -->", m_chan_n);
    	for (int i = 0; i < m_chan_n; i++) {
    	    m_lg->Info("sdsssss", "(", (i+1), ")", m_chan_names->at(i).c_str(), "(", m_chan_titles->at(i).c_str(), ")");
    	}
    	m_lg->Info("s", "------------------------");

    	m_lg->Info("s", "--------------- PROCESS ----------------");
    	for (int i = 0; i < m_chan_n; i++) {
    	    m_lg->Info("ss", "In", m_chan_titles->at(i).c_str());
    	    for (int j = 0; j < m_proc_n->at(i); j++) {
    	        m_lg->Info("sdssssss", "(", (j+1), ")", m_proc_names->at(i)->at(j).c_str(), "(", m_proc_titles->at(i)->at(j).c_str(), ")", h_proc_issigs->GetBinContent(i+1, j+1) != 0 ? "*" : "");
    	    }
    	}
    	m_lg->Info("s", "-------------------------------------------");

    	m_lg->Info("s", "--------------- POIs ----------------");
    	{
    	    TIterator* it = m_pois->createIterator();
    	    RooRealVar *tmpvar;
    	    int i = 0;
    	    while ((tmpvar = (RooRealVar*)it->Next())) {
    	        if (!tmpvar) continue;
    	        i++;
    	        m_lg->Info("sdsssss", "(", i, ")", tmpvar->GetName(), "(", tmpvar->getTitle().Data(), ")");
    	    }
    	}
    	m_lg->Info("s", "-------------------------------------------");

    	m_lg->Info("s", "--------------- NUISs ----------------");
    	m_lg->Info("s", "Non systematics nuis -->");
    	{
    	    TIterator* it = m_nonsysnuiss->createIterator();
    	    RooRealVar *tmpvar;
    	    int i = 0;
    	    while ((tmpvar = (RooRealVar*)it->Next())) {
    	        if (!tmpvar) continue;
    	        i++;
    	        m_lg->Info("sdsssss", "(", i, ")", tmpvar->GetName(), "(", tmpvar->getTitle().Data(), ")");
    	    }
    	}
    	m_lg->Info("s", "Systematics nuis -->");
    	{
    	    TIterator* it = m_sysnuiss->createIterator();
    	    RooRealVar *tmpvar;
    	    int i = 0;
    	    while ((tmpvar = (RooRealVar*)it->Next())) {
    	        if (!tmpvar) continue;
    	        i++;
    	        m_lg->Info("sdsssss", "(", i, ")", tmpvar->GetName(), "(", tmpvar->getTitle().Data(), ")");
    	    }
    	}
    	m_lg->Info("s", "-------------------------------------------");
    }

    //m_globals = m_modelconfig->GetGlobalObservables();
    //ErrDecomp.insert(pair<string, float>("Stat", -1));
    //h_err_decomp->GetXaxis()->SetBinLabel(1, "Stat");
    //for (int i = 0; i < m_sys_names->size(); i++) {
    //    ErrDecomp.insert(pair<string, float>(m_sys_names->at(i), -1));
    //    h_err_decomp->GetXaxis()->SetBinLabel(i+2, m_sys_names->at(i).c_str());
    //}
    //ErrDecomp.insert(pair<string, float>("Total", -1));
    //h_err_decomp->GetXaxis()->SetBinLabel(m_sys_n+2, "Total");

    m_lg->Info("s", "Workspace analyzor initialized.");
}

void WorkspaceAnalyzor::DetermineBinChanProc(RooFormulaVar *var, int &bin, int &chan, int &proc) {
    string name = var->GetName();

    for (int i = 1; i <= m_obs_nbin; i++) {
	char tmp[100];
	sprintf(tmp, "Bin%d", i);
	if (name.find(tmp, 0) != string::npos) {
	    bin = i;
	}
    }

    for (int i = 0; i < m_chan_n; i++) {
	if (name.find(m_chan_names->at(i).c_str(), 0) != string::npos) {
	    chan = i;
	    for (int j = 0; j < m_proc_n->at(i); j++) {
		if (name.find(m_proc_names->at(i)->at(j).c_str(), 0) != string::npos) {
		    proc = j;
		}
	    }
	}
    }

    return;
}

void WorkspaceAnalyzor::PrintDatas() {
    for (int i = 0; i < m_chan_n; i++) {
	string tmpstr;
	char tmp[100];
	sprintf(tmp, "%10s  %10.0f", m_chan_names->at(i).c_str(), m_datas->at(i)->Integral());
	tmpstr += tmp;
	for (int j = 0; j < m_obs_nbin; j++) {
	    sprintf(tmp, "  %10.0f", m_datas->at(i)->GetBinContent(j+1));
	    tmpstr += tmp;
	}
	m_lg->Info("s", tmpstr.c_str());
    }
}

void WorkspaceAnalyzor::PrintParas(RooArgSet *set) {
    m_lg->NewLine();
    m_lg->Info("s", "PRINT parameters");
    m_lg->InfoFixW("dsdsdsdsds", 5, "Nr.", 30, "Varable", 80, "Title", 15, "Value", 15, "Error");
    {
	TIterator* it = set->createIterator();
	RooRealVar *tmpvar;
	int i = 0;
	while ((tmpvar = (RooRealVar*)it->Next())) {
	    if (!tmpvar) continue;
	    i++;
	    m_lg->SetPrecision(3);
	    m_lg->InfoFixW("dddsdsdfdf", 5, i, 30, tmpvar->GetName(), 80, tmpvar->getTitle().Data(), 15, tmpvar->getVal(), 15, tmpvar->getError());
	    m_lg->SetPrecision(1);
	}
    }
}

void WorkspaceAnalyzor::PrintFuncs(RooArgSet *set) {
    m_lg->NewLine();
    m_lg->Info("s", "PRINT functions");
    if (!m_fit_results) {
	m_lg->InfoFixW("dsdsdsds", 5, "Nr.", 30, "Varable", 80, "Title", 15, "Value");
    	{
    	    TIterator* it = set->createIterator();
    	    RooFormulaVar *tmpvar;
    	    int i = 0;
    	    while ((tmpvar = (RooFormulaVar*)it->Next())) {
    	        if (!tmpvar) continue;
    	        i++;
    	        m_lg->InfoFixW("dddsdsdf", 5, i, 30, tmpvar->GetName(), 80, tmpvar->getTitle().Data(), 15, tmpvar->getVal());
    	    }
    	}
    } else {
	m_lg->InfoFixW("dsdsdsdsds", 5, "Nr.", 30, "Varable", 80, "Title", 15, "Value", 15, "Error");
    	{
    	    TIterator* it = set->createIterator();
    	    RooFormulaVar *tmpvar;
    	    int i = 0;
    	    while ((tmpvar = (RooFormulaVar*)it->Next())) {
    	        if (!tmpvar) continue;
    	        i++;
    	        m_lg->InfoFixW("dddsdsdfdf", 5, i, 30, tmpvar->GetName(), 80, tmpvar->getTitle().Data(), 15, tmpvar->getVal(), 15, tmpvar->getPropagatedError(*m_fit_results));
    	    }
    	}
    }
}

void WorkspaceAnalyzor::Print() {
    m_lg->Info("s", "PRINT workspace.");
    m_lg->NewLine();
    
    PrintDatas();
    PrintParas(m_paras);
    PrintFuncs(m_functions); 
    PrintFuncs(m_functions_new); 
}

void WorkspaceAnalyzor::Fit(string option, bool verbose, bool save) {
    RooMsgService::instance().setSilentMode(kTRUE);
    RooMsgService::instance().setGlobalKillBelow(WARNING);

    // PLAIN Fit
    if (option == "Plain") {
	if (verbose) m_lg->Info("s", "Plain fit");

	if (m_debug) m_firstpoi->Print("v");

	if (save) {
	   if (m_fit_results) delete m_fit_results;
	   m_fit_results = m_model->fitTo(*m_dataset, RooFit::Save());
	} else {
	   m_model->fitTo(*m_dataset, RooFit::Save());
	}

	if (save) {
	    m_firstpoi_cen = m_firstpoi->getVal();
	    m_firstpoi_low = m_firstpoi_cen - m_firstpoi->getError();
	    m_firstpoi_high = m_firstpoi_cen + m_firstpoi->getError();
	    if (m_multipoi) {
		m_secondpoi_cen = m_secondpoi->getVal();
	    	m_secondpoi_low = m_secondpoi_cen - m_secondpoi->getError();
	    	m_secondpoi_high = m_secondpoi_cen + m_secondpoi->getError();
	    }
    	    {
    	        TIterator* it = m_paras->createIterator();
    	        RooRealVar *tmpvar;
    	        while ((tmpvar = (RooRealVar*)it->Next())) {
    	            if (!tmpvar) continue;
    	            m_postfit_err.insert(pair<string,float> (tmpvar->GetName(), tmpvar->getError()));
    	        }
    	    }
    	    {
    	        TIterator* it = m_pois->createIterator();
    	        RooRealVar *tmpvar;
    	        int cnt = 0;
    	        while ((tmpvar = (RooRealVar*)it->Next())) {
    	            if (!tmpvar) continue;
		    cnt++;
		    h_fit_results_poi->GetXaxis()->SetBinLabel(cnt, tmpvar->GetName());
		    h_fit_results_poi->SetBinContent(cnt, tmpvar->getVal());
		    h_fit_results_poi->SetBinError(cnt, tmpvar->getError());
    	        }
    	    }
	}

    	if (verbose) {
	    cout << fixed << setprecision(1);
	    Print();
	    {
		int size = m_pois->getSize();
    	        TIterator* it = m_pois->createIterator();
    	        RooRealVar *tmpvar;
		string tmpstr;
		int cnt = 0;
    	        while ((tmpvar = (RooRealVar*)it->Next())) {
    	            if (!tmpvar) continue;
		    cnt++;
		    char tmp[100];
		    //sprintf(tmp, "%10.1f $\\pm$ %5.1f (%.1f%) fb &", tmpvar->getVal(), tmpvar->getError(), tmpvar->getError()/tmpvar->getVal()*100.);
		    if (cnt != size) {
			sprintf(tmp, "%10.1f $\\pm$ %5.1f &", tmpvar->getVal(), tmpvar->getError());
		    } else {
			sprintf(tmp, "%10.1f $\\pm$ %5.1f \\\\", tmpvar->getVal(), tmpvar->getError());
		    }
		    tmpstr += tmp;
    	        }
		m_lg->NewLine();
		m_lg->Info("ss", "Parameter of Interests:", tmpstr.c_str());

		//char tmp[100];
		//sprintf(tmp, "%10.1f $\\pm$ %5.1f (%.1f%) fb &", 0.139*m_firstpoi->getVal(), 0.139*m_firstpoi->getError(), m_firstpoi->getError()/m_firstpoi->getVal()*100.);
		//m_lg->Info("s", tmp);
		//sprintf(tmp, "%10.1f $\\pm$ %5.1f (%.1f%) fb &", 0.132*m_firstpoi->getVal(), 0.132*m_firstpoi->getError(), m_firstpoi->getError()/m_firstpoi->getVal()*100.);
		//m_lg->Info("s", tmp);
    	    }
	}
    } else if (option == "PLC") {
	if (verbose) m_lg->Info("s", "PLC fit");

    	m_model->fitTo(*m_dataset, RooFit::Save());

	float cen = m_firstpoi->getVal();
	float cen2;
	if (m_multipoi) cen2 = m_secondpoi->getVal();

        if (m_PLC) delete m_PLC;
    	m_PLC = new ProfileLikelihoodCalculator(*m_dataset, *m_modelconfig);
    	m_PLC->SetConfidenceLevel(0.683);
    	if (m_LI) delete m_LI;
    	m_LI = m_PLC->GetInterval();
	
	float low = m_LI->LowerLimit(*m_firstpoi);
	float high = m_LI->UpperLimit(*m_firstpoi);
	float err_low = cen - low;
	float err_high = high - cen;
	float low2, high2, err_low2, err_high2;
	if (m_multipoi) {
	    low2 = m_LI->LowerLimit( *m_secondpoi);
	    high2 = m_LI->UpperLimit(*m_secondpoi);
	    err_low2 = cen2 - low2;
	    err_high2 = high2 - cen2;
	}

    	if (save) {
	    m_firstpoi_cen = cen;
	    m_firstpoi_low = low;
	    m_firstpoi_high = high;
	    h_fit_results_plc->SetBinContent(1, cen);
	    h_fit_results_plc->SetBinContent(2, err_low);
	    h_fit_results_plc->SetBinContent(3, err_high);
	    if (m_multipoi) {
		m_secondpoi_cen = cen2;
		m_secondpoi_low = low2;
	    	m_secondpoi_high = high2;
		h_fit_results_plc->SetBinContent(4, cen2);
	    	h_fit_results_plc->SetBinContent(5, err_low2);
	    	h_fit_results_plc->SetBinContent(6, err_high2);
	    }
	}

	if (verbose) {
	    cout << fixed << setprecision(1);
	    m_lg->Info("s", "PLC calculation -->");
            m_lg->Info("sssfsfsfs", "POI = \"", m_firstpoi_name.c_str(), "\" = $", cen, "\b_{-", err_low, "\b}^{+", err_high, "\b} $");
            m_lg->Info("sfsfs", "POI range = [", low, ",", high, "]");
            m_lg->Info("sf", "EERRO Average =", (high - low)/2);
	    if (m_multipoi) {
		m_lg->NewLine();
            	m_lg->Info("sssfsfsfs", "second POI = \"", m_secondpoi->GetName(), "\" = $", cen2, "\b_{-", err_low2, "\b}^{+", err_high2, "\b} $");
            	m_lg->Info("sfsfs", "second POI range = [", low2, ",", high2, "]");
            	m_lg->Info("sf", "EERRO Average =", (high2 - low2)/2);
	    }
	}
    } else if (option == "EvtNumTable") {
	if (verbose) m_lg->Info("s", "Print event number table");

	if (m_fit_results) delete m_fit_results;
    	m_fit_results = m_model->fitTo(*m_dataset, RooFit::Save());

	m_lg->SetModeSimple(true);
	m_lg->NewLine();
	for (int i = 0; i < m_chan_n; i++) {
	    char tmp[100];

	    sprintf(tmp, "%-20s", " ");
	    string tmpline1 = tmp;
	    tmpline1 += " & ";

	    sprintf(tmp, "Post-fit %-30s", m_chan_titles->at(i).c_str());
	    string tmpline2 = tmp;
	    tmpline2 += " & ";

	    // each process
	    vector<double> nevts;
	    vector<string> names;
	    double the_rest = 0;
	    for (int j = 0; j < m_proc_n->at(i); j++) {
		sprintf(tmp, "%-22s", m_proc_titles->at(i)->at(j).c_str());
		tmpline1 += tmp;
		tmpline1 += " & ";

		sprintf(tmp, "FinNum%s%s", m_proc_names->at(i)->at(j).c_str(), m_chan_names->at(i).c_str());
		RooFormulaVar *tmpvar = (RooFormulaVar*)m_functions_new->find(tmp);
		if (!tmpvar) {
		    m_lg->Err("ss", "CANNOT find formulavar -->", tmp);
		    exit(-1);
		}

		sprintf(tmp, "%-8.1f $\\pm$ %-7.1f", tmpvar->getVal(), tmpvar->getPropagatedError(*m_fit_results));
		tmpline2 += tmp;
		tmpline2 += " & ";
    
		if (m_proc_names->at(i)->at(j) != "Diboson" && m_proc_names->at(i)->at(j) != "QCDGamma" && m_proc_names->at(i)->at(j) != "STGamma") {
		    nevts.push_back(tmpvar->getVal());
		    if (m_proc_names->at(i)->at(j) == "Signal") names.push_back("$t\\bar{t}\\gamma$");
		    if (m_proc_names->at(i)->at(j) == "HadronFake") names.push_back("Hadron Fake");
		    if (m_proc_names->at(i)->at(j) == "EGammaFake") names.push_back("$e\\to\\gamma$ Fake");
		    if (m_proc_names->at(i)->at(j) == "WGammaJets") names.push_back("$W\\gamma$+jets");
		    if (m_proc_names->at(i)->at(j) == "ZGammaJets") names.push_back("$Z\\gamma$+jets");
		} else {
		    the_rest += tmpvar->getVal();
		}
	    }
	    nevts.push_back(the_rest);
	    names.push_back("All the rest");

	    // total bkg
	    //sprintf(tmp, "%-22s", "TotBkg");
	    //tmpline1 += tmp;
	    //tmpline1 += " & ";
	    //
	    //{
	    //    sprintf(tmp, "FinNumTotBkg%s", m_chan_names->at(i).c_str());
	    //    RooFormulaVar *tmpvar = (RooFormulaVar*)m_functions_new->find(tmp);
	    //    if (!tmpvar) {
	    //        m_lg->Err("ss", "CANNOT find formulavar -->", tmp);
	    //        exit(-1);
	    //    }
	    //    sprintf(tmp, "%-8.1f $\\pm$ %-7.1f", tmpvar->getVal(), tmpvar->getPropagatedError(*m_fit_results));
	    //}
	    //tmpline2 += tmp;
	    //tmpline2 += " & ";

	    // total bkg + sig
	    sprintf(tmp, "%-22s", "TotBkg+Sig");
	    tmpline1 += tmp;
	    tmpline1 += " & ";

	    double tot_evts;
	    {
		sprintf(tmp, "FinNumTotEvt%s", m_chan_names->at(i).c_str());
		RooFormulaVar *tmpvar = (RooFormulaVar*)m_functions_new->find(tmp);
		if (!tmpvar) {
		    m_lg->Err("ss", "CANNOT find formulavar -->", tmp);
		    exit(-1);
		}
		sprintf(tmp, "%-8.1f $\\pm$ %-7.1f", tmpvar->getVal(), tmpvar->getPropagatedError(*m_fit_results));
		tot_evts = tmpvar->getVal();
	    }
	    tmpline2 += tmp;
	    tmpline2 += " & ";

	    // data
	    sprintf(tmp, "%-22s", "Data");
	    tmpline1 += tmp;
	    tmpline1 += "\\\\";

	    sprintf(tmp, "%-8.1f $\\pm$ %-7.1f", m_data_nums->at(i), sqrt(m_data_nums->at(i)));
	    tmpline2 += tmp;
	    tmpline2 += "\\\\";

	    //if (i == 0) m_lg->Info("s", tmpline1.c_str());
	    m_lg->Info("s", tmpline2.c_str());

	    m_lg->NewLine();
	    string str_pie = "Pie-chart:   ";
	    for (int j = 0; j < nevts.size(); j++) {
		char char_pie[100];
		sprintf(char_pie, "%.1f\/%s", 100.*nevts.at(j)/tot_evts, names.at(j).c_str());
		str_pie += char_pie;
		if (j != nevts.size()-1) str_pie += ", ";
	    }
	    cout << "     " << str_pie << endl;
	    m_lg->NewLine();
	}
	m_lg->SetModeSimple(false);
    } else {
	m_lg->Err("ss", "Unrecogonizable option -->", option.c_str());
	exit(-1);
    }
}

void WorkspaceAnalyzor::FixAllSys() {
    TIterator* it = m_sysnuiss->createIterator();
    RooRealVar *tmpvar;
    while ((tmpvar = (RooRealVar*)it->Next())) {
        if (!tmpvar) continue;
	tmpvar->setConstant(true);
    }
    delete tmpvar;
}

void WorkspaceAnalyzor::LinearityCheck() {
    // get best fit value of hadron fake and then fix it
    m_workspace->loadSnapshot("PreFit");
    FixAllSys();
    Fit("Plain", false);
    m_workspace->saveSnapshot("PostFit", *m_paras);

    double xsec_start = 50;
    //TFile* f = new TFile("results/LinearityCheck.root", "recreate");
    TH1F* h_xsecs = new TH1F("Xsection", "", 26, 49, 101);
    for (int i = 0; i <= 25; i++) {
	m_lg->Info("sd", "Linearity check --> round", i+1);
	m_workspace->loadSnapshot("PostFit");
	FixAllSys();
	double xsec_input = xsec_start + i*2;
	m_firstpoi->setVal(xsec_input);
	delete m_dataset;
	m_dataset = (RooAbsData*)AsymptoticCalculator::GenerateAsimovData(*m_model, *m_obss);
	Fit("Plain", false);		
	double xsec_output = m_firstpoi->getVal();
	h_xsecs->SetBinContent(i+1, xsec_output);
    }

    SetAtlasStyle();

    TCanvas* c = new TCanvas("", "", 800, 600);
    c->cd();
    
    h_xsecs->GetXaxis()->SetTitle("#sigma_{input} [fb]");
    h_xsecs->GetYaxis()->SetTitle("#sigma_{output} [fb]");
    h_xsecs->GetXaxis()->SetTitleOffset(1.0);
    h_xsecs->GetYaxis()->SetTitleOffset(1.0);
    h_xsecs->GetXaxis()->SetTitleSize(0.05);
    h_xsecs->GetXaxis()->SetLabelSize(0.05);
    h_xsecs->GetYaxis()->SetTitleSize(0.05);
    h_xsecs->GetYaxis()->SetLabelSize(0.05);
    h_xsecs->SetLineWidth(3);
    h_xsecs->Draw();

    TLine* hline = new TLine(50, 50, 100, 100);
    hline->SetLineColor(kRed);
    hline->SetLineWidth(2);
    hline->SetLineStyle(2);
    hline->Draw("same");

    TLegend *lg = new TLegend(0.23, 0.75, 0.45, 0.9);
    lg->SetBorderSize(0);
    lg->SetTextSize(0.035);
    lg->SetFillColor(0);
    lg->AddEntry(h_xsecs, "Fitted", "l");
    lg->AddEntry(hline, "Truth", "l");
    lg->Draw();

    c->SaveAs("plots/LinearityCheck.png");
    c->SaveAs("plots/LinearityCheck.pdf");
}

void WorkspaceAnalyzor::SysStudy(string type) {
    m_lg->Info("s", "SYSTEMATICS study");

    if (type == "Sys+Stat") {
	
	if (!m_multipoi){ 
	    m_workspace->loadSnapshot("PreFit");
	    FixAllSys();
	    Fit("PLC", false);
	    float cen_nosys = m_firstpoi_cen;
	    float low_nosys = (m_firstpoi_cen - m_firstpoi_low)/m_firstpoi_cen;
	    float high_nosys = (m_firstpoi_high - m_firstpoi_cen)/m_firstpoi_cen;
	    float nosys_max = fabs(low_nosys) > fabs(high_nosys) ? fabs(low_nosys) : fabs(high_nosys);
	    
	    m_workspace->loadSnapshot("PreFit");
	    Fit("PLC", false);
	    float cen_sys = m_firstpoi_cen;
	    float low_sys = (m_firstpoi_cen - m_firstpoi_low)/m_firstpoi_cen;
	    float high_sys = (m_firstpoi_high - m_firstpoi_cen)/m_firstpoi_cen;
	    float sys_max = fabs(low_sys) > fabs(high_sys) ? fabs(low_sys) : fabs(high_sys);
	    
	    float diff_low = sqrt(pow(low_sys,2) - pow(low_nosys,2))*m_firstpoi_cen;
	    float diff_high = sqrt(pow(high_sys,2) - pow(high_nosys,2))*m_firstpoi_cen;
	    float diff_max = fabs(diff_low) > fabs(diff_high) ? fabs(diff_low) :  fabs(diff_high);
	    
	    cout << fixed << setprecision(1);
	    m_lg->Info("sssfsfsfsfsfsfsfsfs", "DECOMPOSE total systematics to Sys+Stat --> \"", m_firstpoi_name.c_str(), "\" = ", m_firstpoi_cen, 
	    "\b_{-", low_nosys*m_firstpoi_cen, "\b}^{+", high_nosys*m_firstpoi_cen, "\b} ({\\rm stat.})_{-", diff_low, "\b}^{+", diff_high, 
	    "\b} ({\\rm sys.})~\\text{fb} =", m_firstpoi_cen, "\b_{-", low_sys*m_firstpoi_cen, "\b}^{+", high_sys*m_firstpoi_cen, "\b}~\\text{fb} ");
	    m_lg->Info("sssfsfsfsfsfs", "DECOMPOSE total systematics to Sys+Stat --> \"", m_firstpoi_name.c_str(), "\" = ", m_firstpoi_cen, " \\pm ", 
	    nosys_max*m_firstpoi_cen, " ({\\rm stat.}) \\pm ", diff_max, " ({\\rm sys.})~\\text{fb} =", 
	    m_firstpoi_cen,  " \\pm ", sys_max*m_firstpoi_cen, " ~\\text{fb} ");
	} else {

	    m_workspace->loadSnapshot("PreFit");
	    FixAllSys();
	    Fit("Plain", false);
	    vector<float> cen_nosys;
	    vector<float> err_nosys;
	    {
		TIterator* it = m_pois->createIterator();
	    	RooRealVar *tmpvar;
	    	string tmpstr;
	    	while ((tmpvar = (RooRealVar*)it->Next())) {
	    	    if (!tmpvar) continue;
	    	    cen_nosys.push_back(tmpvar->getVal());
	    	    err_nosys.push_back(tmpvar->getError()/tmpvar->getVal());
	    	}
	    }

	    m_workspace->loadSnapshot("PreFit");
	    Fit("Plain", false);
	    vector<float> cen_sys;
	    vector<float> err_sys;
	    {
		TIterator* it = m_pois->createIterator();
	    	RooRealVar *tmpvar;
	    	string tmpstr;
	    	while ((tmpvar = (RooRealVar*)it->Next())) {
	    	    if (!tmpvar) continue;
	    	    cen_sys.push_back(tmpvar->getVal());
	    	    err_sys.push_back(tmpvar->getError()/tmpvar->getVal());
	    	}
	    }
	    
	    vector<float> diff;
	    for (int i = 0; i < cen_nosys.size(); i++) {
		diff.push_back(sqrt(pow(err_sys.at(i),2) - pow(err_nosys.at(i),2))*cen_sys.at(i));
	    }

	    string tmpstr = "TAG ";
	    for (int i = 0; i < cen_nosys.size(); i++) {
	        char tmp[100];
	        //sprintf(tmp, "%10.1f $\\pm$ %5.1f (%.1f%) fb &", tmpvar->getVal(), tmpvar->getError(), tmpvar->getError()/tmpvar->getVal()*100.);
	        sprintf(tmp, "%10.1f $\\pm$ %5.1f $\\pm$ %5.1f", cen_sys.at(i), err_nosys.at(i)*cen_sys.at(i), diff.at(i));
	        tmpstr += tmp;
		if (i != cen_nosys.size() - 1) tmpstr += " &";
		else tmpstr += " \\\\";
	    }
	    m_lg->NewLine();
	    m_lg->Info("s", "Parameter of Interests:");
	    m_lg->Info("s", tmpstr.c_str());

	    tmpstr = "TAG ";
	    for (int i = 0; i < cen_nosys.size(); i++) {
	        char tmp[100];
	        //sprintf(tmp, "%10.1f $\\pm$ %5.1f (%.1f%) fb &", tmpvar->getVal(), tmpvar->getError(), tmpvar->getError()/tmpvar->getVal()*100.);
	        sprintf(tmp, "%10.1f $\\pm$ %5.1f (%.1f) $\\pm$ %5.1f (%.1f)", cen_sys.at(i), err_nosys.at(i)*cen_sys.at(i), err_nosys.at(i)*100., diff.at(i), diff.at(i)/cen_sys.at(i)*100.);
	        tmpstr += tmp;
		if (i != cen_nosys.size() - 1) tmpstr += " &";
		else tmpstr += " \\\\";
	    }
	    m_lg->NewLine();
	    m_lg->Info("s", "Parameter of Interests:");
	    m_lg->Info("s", tmpstr.c_str());
	}
	return;
    }

    if (type == "AllSys") {
	m_workspace->loadSnapshot("PreFit");
	if (m_inverse_mode) FixAllSys();
	Fit("Plain", false);

	TIterator* it = m_pois->createIterator();
	RooRealVar *tmpvar;
	int i = 0;
	while ((tmpvar = (RooRealVar*)it->Next())) {
	    if (!tmpvar) continue;
	    i++;
	    if (i == m_ref_poi) {
	        break;
	    }
	}
	float normResult, normError;
	normResult = tmpvar->getVal(); normError = tmpvar->getError();
	//if (ErrDecomp["Stat"] < 0) {
	//    ErrDecomp["Stat"] = 100*normError/normResult;
	//    h_err_decomp->SetBinContent(1, 100*normError/normResult);
	//}
    
	vector<float> sysResults; vector<float> sysErrors; vector<float> errDiff;
	for (int i = 0; i < m_sys_n; i++) {
	    m_lg->Info("sdss", "SCAN systematics (", i+1, ") -->", m_sys_titles->at(i).c_str());
	
	    m_workspace->loadSnapshot("PreFit");
	    if (m_inverse_mode) {
	        FixAllSys();
	        FloatSys(m_sys_names->at(i));
	    } else FixSys(m_sys_names->at(i));
	    Fit("Plain", false);
	
	    sysResults.push_back(tmpvar->getVal());
	    sysErrors.push_back(tmpvar->getError());
	
	    if (!m_inverse_mode) errDiff.push_back((normError/normResult - sysErrors.back()/sysResults.back() <= 0) ? 0 : 100*sqrt(pow(normError/normResult, 2) - pow(sysErrors.back()/sysResults.back(),2)));
	    else errDiff.push_back((sysErrors.back()/sysResults.back() - normError/normResult <= 0) ? 0 : 100*sqrt(pow(sysErrors.back()/sysResults.back(), 2) - pow(normError/normResult,2)));
	
	    //if (ErrDecomp[m_sys_names->at(i)] < 0) {
	    //    ErrDecomp[m_sys_names->at(i)] = errDiff.back();
	    //    h_err_decomp->SetBinContent(i+2, errDiff.back());
	    //}
	}
    
	m_lg->NewLine();
	m_lg->SetModeSimple(true);
	cout << fixed << setprecision(2);
	m_lg->Info("s", "---------------------------------- SUMMARY ----------------------------------------");
        vector<int> sorted;
    	SortIndex(errDiff, sorted);
        float sumSysError = 0;
    	m_lg->InfoFixW("dsdsdsdsds", 25, "SysName", 80, "SysTitle", 15, "POIValue" , 15, "POIError", 15, "ErrorDiff");
	m_sys_order.clear();
	m_sys_order_val.clear();
	h_sys_order = new TH1F("Sys_Order", "", sorted.size(), 0, sorted.size());
    	for (int i = 0; i < sorted.size(); i++) {
            sumSysError += pow(errDiff.at(sorted.at(i)), 2);
    	    m_lg->InfoFixW("dsdsdfdfdf", 25, m_sys_names->at(sorted.at(i)).c_str(), 80, m_sys_titles->at(sorted.at(i)).c_str(), 
    	    	                     15, sysResults.at(sorted.at(i)), 15, sysErrors.at(sorted.at(i)), 15, errDiff.at(sorted.at(i)));
	    m_sys_order.push_back(m_sys_names->at(sorted.at(i)));
	    m_sys_order_val.push_back(errDiff.at(sorted.at(i)));
	    h_sys_order->GetXaxis()->SetBinLabel(i+1, m_sys_order.back().c_str());
	    h_sys_order->SetBinContent(i+1, m_sys_order_val.back());
    	}
	sumSysError = sqrt(sumSysError);
    	m_lg->InfoFixW("dsdsdsdsdf", 25, "SysSum", 80, "Sum of All Systematic Uncertainties", 
    	    	                     15, "--" , 15, "--", 15, sumSysError);
        m_workspace->loadSnapshot("PreFit");
	FixAllSys();
        Fit("Plain", false);
        float statError = 100*tmpvar->getError()/tmpvar->getVal();
    	m_lg->InfoFixW("dsdsdfdfdf", 25, "StatErr", 80, "Statistical Uncertainties", 
    	    	                     15, tmpvar->getVal() , 15, tmpvar->getError(), 15, statError);

        m_workspace->loadSnapshot("PreFit");
        Fit("Plain", false);
	float trueTotError = 100*tmpvar->getError()/tmpvar->getVal();

	float totalError = sqrt(pow(sumSysError,2) + pow(statError,2));
    	m_lg->InfoFixW("dsdsdsdsdf", 25, "SysStatSum", 80, "Sum of Systematic and Statistical Uncertainties", 
    	    	                     15,"--" , 15, "--", 15, totalError);

    	m_lg->InfoFixW("dsdsdfdfdf", 25, "TrueSysStatSum", 80, "True Sum of Systematic and Statistical Uncertainties", 
    	    	                     15, tmpvar->getVal() , 15, tmpvar->getError(), 15, trueTotError);
        //if (ErrDecomp["Total"] < 0) {
    	//    ErrDecomp["Total"] = totalError;
    	//    h_err_decomp->SetBinContent(m_sys_n+2, totalError);
    	//}
	m_lg->SetModeSimple(false);
    }

    if (type.find("Toy",0) != string::npos) {
	string sys = type.substr(3, type.size());
	m_lg->Info("ss", "TOY Study for", sys.c_str());

	// check
	int sys_id = -1;
	for (int i = 0; i < m_sys_n; i++) {
	    if (m_sys_names->at(i) == sys) {
		sys_id = i;
		break;
	    }
	}
	if (sys_id == -1) {
	    m_lg->Err("ss", "CANNOT find the systematic source -->", sys.c_str());
	    exit(-1);
	}

	// preparation
	m_workspace->loadSnapshot("PreFit");
	FixSys(sys);
	Fit("Plain", false);
	float err1 = m_firstpoi->getError();
	m_workspace->loadSnapshot("PreFit");
	Fit("Plain", false);
	float center = m_firstpoi->getVal(); 
	float err2 = m_firstpoi->getError();
	float err = fabs(err2) > fabs(err1) ? sqrt(pow(err2,2) - pow(err1,2)) : 0;
	if (err < 0.001*center) {
	    m_lg->Warn("sssf", "SKIP. Too small anticipated error for", sys.c_str(), "-->", 100*err/center);
	    return;
	}
	int fitrange = 5;
	float low = (center - fitrange*err > 0 ? center - fitrange*err : 0);
	float high = center + fitrange*err;
	m_lg->NewLine();
	m_lg->Info("sfs", "Anticipated error -->", err, "fb");
	m_lg->Info("sfsfs", "FIT range --> [", low, ",", high, "] fb");
	m_lg->Info("sfs", "Width per bin -->", 2*fitrange*err/200., "fb");
	TH1F* tmph = new TH1F("tmph", "tmph", 200, low, high);
	TH1F* tmph2 = new TH1F("tmph2", "tmph2", 200, -5, 5);

	m_workspace->loadSnapshot("PreFit");
	Fit("Plain", false);
	m_workspace->saveSnapshot("PostFit", *m_paras);

	// get constraint
	RooRealVar *sysvar = (RooRealVar*)m_sysnuiss->find(sys.c_str());
	string systitle = sysvar->GetTitle();
	RooArgSet tmpset;
	tmpset.add(*sysvar);
	RooArgSet* constraint = m_model->getAllConstraints(*m_obss, tmpset, true);
	if (constraint->getSize() > 1) {
	    m_lg->Err("s", "Strange! found more than 1 constraint!");
	    exit(-1);
	} else if (constraint->getSize() < 1) {
	    m_lg->Err("s", "CANNOT find the constraint!");
	    exit(-1);
	}
	RooProdPdf* constrPdf = new RooProdPdf("tmppdf", "tmppdf", *constraint);
	RooDataSet* sysvals = constrPdf->generate(*sysvar, 5000);
    
	// loop
	double total = sysvals->sumEntries();
	for (int i = 0; i < total; i++) {
	    if (i%100 == 0) {
		m_lg->Info("sd", "Toy study iteration -->", i);
	    }
	    const RooArgSet* sysval_i = sysvals->get(i);
	    RooRealVar* tmpvar = (RooRealVar*)sysval_i->first();
	    double tmpval = tmpvar->getVal();

	    m_workspace->loadSnapshot("PostFit");
	    sysvar->setVal(tmpval);

	    if (m_dataset) delete m_dataset;
	    m_dataset = (RooAbsData*)AsymptoticCalculator::GenerateAsimovData(*m_model, *m_obss);
	    Fit("Plain", false);
	    tmph->Fill(m_firstpoi->getVal());
	    tmph2->Fill(tmpval);
	}
	
	{
	    string poititle = m_firstpoi->GetTitle();
	    RooRealVar tmpx("tmpx", poititle.c_str(), tmph->GetMean(), low, high);
	    RooRealVar tmpmean("tmpmean", "mean", tmph->GetMean(), low, high);
	    RooRealVar tmpsigma("tmpsigma", "sigma", tmph->GetRMS(), 0, 1e3);
	    RooGaussian tmpgauss("tmpgauss","gauss", tmpx, tmpmean, tmpsigma);
	    RooDataHist tmpData = RooDataHist("tmpData", "Data", tmpx, tmph);
	    tmpgauss.fitTo(tmpData);
	    
	    cout << fixed << setprecision(1);
	    m_lg->Info("ssfsfs", sys.c_str(), "Error -->", tmpsigma.getVal(), " +/- ", tmpsigma.getError(), "fb");
	    m_lg->Info("ssfsfs", sys.c_str(), "Relative Error -->",  100*tmpsigma.getVal()/center, "+/-", 100*tmpsigma.getError()/center, "%");
	    
	    string fsave = "results/Results_SysStudy_Toy_";
	    fsave += sys; fsave += ".root";
	    TFile* f = new TFile(fsave.c_str(), "recreate");
	    TH1F* h = new TH1F(sys.c_str(), systitle.c_str(), 2, 0, 2);
	    h->SetBinContent(1, 100*tmpsigma.getVal()/center);
	    h->SetBinError(1, 100*tmpsigma.getError()/center);
	    h->SetBinContent(2, 100*err/center);
	    h->Write();
	    f->Close();
	    delete f;
	    m_lg->Info("ss", "Results written into --> results/SysStudy_Toy.root :", sys.c_str());
	    
	    RooPlot *tmpframe = tmpx.frame();
	    tmpData.plotOn(tmpframe, DataError(RooAbsData::SumW2));
	    tmpgauss.paramOn(tmpframe, Layout(0.6,0.89,0.89), Parameters(RooArgList(tmpmean,tmpsigma)));
	    tmpgauss.plotOn(tmpframe, LineColor(kBlue));
	    tmpframe->SetMaximum(tmpframe->GetMaximum()*1.2);
	        
	    TCanvas *tmpc = new TCanvas("tmp", "tmp", 800, 600);
	    tmpframe->Draw();
	    tmpc->SaveAs((m_plotdir+string("SysStudy_Toy_Effect_")+sys+string(".png")).c_str());
	    tmpc->SaveAs((m_plotdir+string("SysStudy_Toy_Effect_")+sys+string(".pdf")).c_str());
	    tmpc->SaveAs((m_plotdir+string("SysStudy_Toy_Effect_")+sys+string(".C")).c_str());
	}

	{
	    string poititle = m_firstpoi->GetTitle();
	    RooRealVar tmpx("tmpx2", poititle.c_str(), tmph2->GetMean(), -5, 5);
	    RooRealVar tmpmean("tmpmean2", "mean", tmph2->GetMean(), -5, 5);
	    RooRealVar tmpsigma("tmpsigma2", "sigma", tmph2->GetRMS(), 0, 1e3);
	    RooGaussian tmpgauss("tmpgauss2","gauss", tmpx, tmpmean, tmpsigma);
	    RooDataHist tmpData = RooDataHist("tmpData2", "Data", tmpx, tmph2);
	    tmpgauss.fitTo(tmpData);
	    
	    RooPlot *tmpframe = tmpx.frame();
	    tmpData.plotOn(tmpframe, DataError(RooAbsData::SumW2));
	    tmpgauss.paramOn(tmpframe, Layout(0.6,0.89,0.89), Parameters(RooArgList(tmpmean,tmpsigma)));
	    tmpgauss.plotOn(tmpframe, LineColor(kBlue));
	    tmpframe->SetMaximum(tmpframe->GetMaximum()*1.2);
	        
	    TCanvas *tmpc = new TCanvas("tmp2", "tmp", 800, 600);
	    tmpframe->Draw();
	    tmpc->SaveAs((m_plotdir+string("SysStudy_Toy_Cause_")+sys+string(".png")).c_str());
	    tmpc->SaveAs((m_plotdir+string("SysStudy_Toy_Cause_")+sys+string(".pdf")).c_str());
	    tmpc->SaveAs((m_plotdir+string("SysStudy_Toy_Cause_")+sys+string(".C")).c_str());
	}
	//tmpc->SaveAs((m_plotdir+string("Toy")+sys+string(".pdf")).c_str());

	//RooMCStudy *MCS = new RooMCStudy(*m_model, *m_obss, Silence(), Constrain(*sysvar));
	//MCS->generateAndFit(1000, 100, false);
	//MCS->fitParDataSet().fillHistogram(tmph, *m_firstpoi);
    }
}

void WorkspaceAnalyzor::SortIndex(vector<float> inputs, vector<int> &outputs) {
    for (int i = 0; i < inputs.size(); i++) {
	outputs.push_back(FindRemoveMax(inputs));
    }
}

int WorkspaceAnalyzor::FindRemoveMax(vector<float> &inputs) {
    int pos = -1;
    float max = -1;
    for (int i = 0; i < inputs.size(); i++) {
	if (inputs.at(i) > max) {
	    max = inputs.at(i);
	    pos = i;
	}
    }
    inputs.at(pos) = -99999;
    return pos;
}

void WorkspaceAnalyzor::FloatSys(string tofloat) {
    RooRealVar *tmpvar = (RooRealVar*)(m_sysnuiss->find(tofloat.c_str()));
    if (!tmpvar) {
	m_lg->Err("ss", "Cannot find nuissance to float -->", tofloat.c_str());
        exit(-1);
    } else {
	tmpvar->setConstant(false);
    }
}

//void WorkspaceAnalyzor::FloatNuis(vector<string> tofloatV, bool verbose) {
//    for (int i = 0; i < tofloatV.size(); i++) {
//	FloatNuis(tofloatV.at(i), verbose);
//    }
//}

void WorkspaceAnalyzor::FixSys(string tofix) {
    RooRealVar *tmpvar = (RooRealVar*)(m_sysnuiss->find(tofix.c_str()));
    if (!tmpvar) {
	m_lg->Err("ss", "Cannot find parameter to fix -->", tofix.c_str());
        exit(-1);
    } else {
	tmpvar->setConstant(true);
    }
}

//void WorkspaceAnalyzor::FixNuis(vector<string> &tofixV, bool verbose) {
//    for (int i = 0; i < tofixV.size(); i++) {
//	FixNuis(tofixV.at(i), verbose);
//    }
//}


void WorkspaceAnalyzor::Plot(bool plc, bool stack, bool corr, bool pull, bool logy) {
    if (plc) {
	m_lg->Info("s", "PLOT profiled likelihood scan.");

	if (!m_multipoi) {
	    TF1* f_plc_sys = NULL;
	    TF1* f_plc_nosys = NULL;
	    float lip_range_low = 0;
	    float lip_range_high = 0;

	    {
	        m_workspace->loadSnapshot("PreFit");
	        Fit("PLC", false);
	        LikelihoodIntervalPlot *LIP = new LikelihoodIntervalPlot(m_LI);
	        LIP->SetNPoints(50);
	        lip_range_low = m_firstpoi_cen - 2.5*(m_firstpoi_cen - m_firstpoi_low);
	        lip_range_high = m_firstpoi_cen + 3*(m_firstpoi_high - m_firstpoi_cen);
	        LIP->SetRange(lip_range_low, lip_range_high);
	        
	        TCanvas *tmpc = new TCanvas("tmpc", "tmpc", 800, 600);
	        tmpc->cd();
	        LIP->Draw("tf1");
	        string objname = "_PLL_"; objname += m_firstpoi_name;
	        f_plc_sys = (TF1*)tmpc->FindObject(objname.c_str())->Clone();
	        delete tmpc;
	        delete LIP;
	    }

	    {
	        m_workspace->loadSnapshot("PreFit");
	        FixAllSys();
	        Fit("PLC", false, false);
	        LikelihoodIntervalPlot *LIP = new LikelihoodIntervalPlot(m_LI);
	        LIP->SetNPoints(50);
	        LIP->SetRange(lip_range_low, lip_range_high);
	        
	        TCanvas *tmpc = new TCanvas("tmpc", "tmpc", 800, 600);
	        tmpc->cd();
	        LIP->Draw("tf1");
	        string objname = "_PLL_"; objname += m_firstpoi_name;
	        f_plc_nosys = (TF1*)tmpc->FindObject(objname.c_str())->Clone();
	        delete tmpc;
	        delete LIP;
	    }

	    SetAtlasStyle();
    	    TCanvas *c = new TCanvas("", "", 800, 600);

    	    f_plc_nosys->SetLineColor(kRed);
    	    f_plc_nosys->SetLineStyle(2);
    	    f_plc_sys->SetLineColor(kBlack);

    	    f_plc_nosys->GetYaxis()->SetRangeUser(0,6);
    	    f_plc_nosys->GetYaxis()->SetTitle("-Log [ #lambda_{s}(p_{T}^{iso} | #sigma^{tot}_{sl}) ]");
    	    f_plc_nosys->GetYaxis()->SetTitleOffset(1.0);
    	    f_plc_nosys->GetXaxis()->SetTitle("#sigma_{sl}^{tot} [fb]");
    	    f_plc_nosys->GetXaxis()->SetNdivisions(505);

    	    f_plc_nosys->Draw();
    	    f_plc_sys->Draw("same");

    	    TLegend *lg = new TLegend(0.63, 0.75, 0.85, 0.9);
    	    lg->SetBorderSize(0);
    	    lg->SetTextSize(0.035);
    	    lg->SetFillColor(0);
    	    lg->AddEntry(f_plc_nosys, "Without systematics", "l");
    	    lg->AddEntry(f_plc_sys, "With systematics", "l");
    	    lg->Draw();

    	    float ltx = 0.25;
    	    TLatex lt;
    	    lt.SetNDC();
    	    lt.SetTextColor(1);
    	    lt.SetTextFont(72);
    	    lt.DrawLatex(ltx, 0.85, "ATLAS");
	    double delx = 0.115*696*gPad->GetWh()/(472*gPad->GetWw());
    	    lt.SetTextFont(42);
	    lt.DrawLatex(ltx+delx, 0.85, "Internal");
    	    lt.DrawLatex(ltx, 0.77, "#sqrt{s} = 8 TeV");
    	    lt.DrawLatex(ltx, 0.67, "#int Ldt = 20 fb^{-1}");

    	    TLine *lh = new TLine(c->GetUxmin(), 0.5, c->GetUxmax(), 0.5);
    	    lh->SetLineStyle(2);
    	    lh->SetLineColor(kBlack);
    	    lh->SetLineWidth(2);
    	    lh->Draw();

    	    TLine *lv1 = new TLine(m_firstpoi_low, 0, m_firstpoi_low, 0.5);
    	    TLine *lv2 = new TLine(m_firstpoi_high, 0, m_firstpoi_high, 0.5);
    	    lv1->SetLineStyle(2);
    	    lv1->SetLineColor(kBlack);
    	    lv1->SetLineWidth(2);
    	    lv1->Draw();
    	    lv2->SetLineStyle(2);
    	    lv2->SetLineColor(kBlack);
    	    lv2->SetLineWidth(2);
    	    lv2->Draw();

    	    c->SaveAs((m_plotdir+string("Likelihood.png")).c_str());
    	    c->SaveAs((m_plotdir+string("Likelihood.pdf")).c_str());
	    delete c;
	    delete lh;
	    delete lv1;
	    delete lv2;
	    delete lg;
	} else {
	    RooMsgService::instance().setSilentMode(kTRUE);
	    RooMsgService::instance().setGlobalKillBelow(WARNING);

	    m_workspace->loadSnapshot("PreFit");
	    Fit("PLC", false);

	    m_firstpoi->setMin(m_firstpoi_cen - 6*(m_firstpoi_cen - m_firstpoi_low));
	    m_secondpoi->setMin(m_secondpoi_cen - 4*(m_secondpoi_cen - m_secondpoi_low));
	    m_firstpoi->setMax(m_firstpoi_cen + 4*(m_firstpoi_high - m_firstpoi_cen));
	    m_secondpoi->setMax(m_secondpoi_cen + 4*(m_secondpoi_high - m_secondpoi_cen));

	    TMarker *bestfit;
	    TGraph *_1sigma;
	    TGraph *_2sigma;
	    TGraph *_3sigma;
	    {
		RooAbsReal* nll = m_model->createNLL(*m_dataset);
	    	RooMinuit m(*nll);
	    	m.migrad();
	    	m.save();
	    	RooPlot* frame = m.contour(*m_firstpoi, *m_secondpoi, 1, 2, 3);
    	    	TCanvas *tmpc = new TCanvas("", "", 800, 650);
	    	frame->Draw();
	        bestfit = (TMarker*)tmpc->FindObject("TMarker")->Clone();
	        _1sigma = (TGraph*)tmpc->FindObject("contour_nll_Model_DataSet_with_constr_n1.000000")->Clone();
	        _2sigma = (TGraph*)tmpc->FindObject("contour_nll_Model_DataSet_with_constr_n2.000000")->Clone();
	        _3sigma = (TGraph*)tmpc->FindObject("contour_nll_Model_DataSet_with_constr_n3.000000")->Clone();

		delete tmpc;
	    }

	    SetAtlasStyle();
    	    TCanvas *c = new TCanvas("", "", 800, 650);

	    TH1F* h_dummy = new TH1F("", "", 20, m_firstpoi->getMin(), m_firstpoi->getMax()); 
	    h_dummy->GetXaxis()->SetTitle(m_firstpoi->getTitle().Data());
	    h_dummy->GetYaxis()->SetTitle(m_secondpoi->getTitle().Data());
	    h_dummy->GetYaxis()->SetRangeUser(m_secondpoi->getMin(), m_secondpoi->getMax());
	    h_dummy->Draw();

	    _1sigma->SetFillColor(3);
	    _2sigma->SetFillColor(5);
	    _3sigma->SetFillColor(7);

	    _1sigma->SetLineColor(1);
	    _2sigma->SetLineColor(1);
	    _3sigma->SetLineColor(1);

	    _1sigma->SetLineWidth(2);
	    _2sigma->SetLineWidth(2);
	    _3sigma->SetLineWidth(3);

	    _1sigma->SetLineStyle(1);
	    _2sigma->SetLineStyle(1);
	    _3sigma->SetLineStyle(1);

	    bestfit->SetMarkerStyle(34);
	    bestfit->SetMarkerSize(2);

	    TMarker* exp = new TMarker();
	    exp->SetMarkerSize(2);
	    exp->SetX(78.5);
	    exp->SetY(74.7);
	    exp->SetMarkerStyle(24);

    	    TLegend *lg = new TLegend(0.2, 0.25, 0.4, 0.55);
    	    lg->SetBorderSize(0);
    	    lg->SetTextSize(0.035);
    	    lg->SetFillColor(0);
    	    lg->AddEntry(_1sigma, "Obs #pm 1#sigma", "f");
    	    lg->AddEntry(_2sigma, "Obs #pm 2#sigma", "f");
    	    lg->AddEntry(_3sigma, "Obs #pm 3#sigma", "f");
    	    lg->AddEntry(bestfit, "Obs", "p");
    	    lg->AddEntry(exp, "SM", "p");
    	    lg->Draw();

	    _3sigma->Draw("same F");
	    _2sigma->Draw("same F");
	    _1sigma->Draw("same F");

	    _3sigma->Draw("same");
	    _2sigma->Draw("same");
	    _1sigma->Draw("same");

	    bestfit->Draw("same");
	    exp->Draw("same");

    	    float ltx = 0.2;
    	    TLatex lt;
    	    lt.SetNDC();
    	    lt.SetTextColor(1);
    	    lt.SetTextFont(72);
    	    lt.DrawLatex(ltx, 0.85, "ATLAS");
    	    lt.SetTextFont(42);
	    double delx = 0.115*696*gPad->GetWh()/(472*gPad->GetWw());
	    lt.DrawLatex(ltx+delx, 0.85, "Internal");
    	    lt.DrawLatex(ltx, 0.77, "#sqrt{s} = 8 TeV");
    	    lt.DrawLatex(ltx, 0.67, "#int Ldt = 20 fb^{-1}");

    	    c->SaveAs((m_plotdir+string("Likelihood_2D.png")).c_str());
    	    c->SaveAs((m_plotdir+string("Likelihood_2D.pdf")).c_str());
	    delete c;
	}
    }

    if (corr) {
	m_lg->Info("s", "PLOT parameter correlations.");

	m_workspace->loadSnapshot("PreFit");
	Fit("Plain", false);

	SetAtlasStyle();
        TH2F* correlation = (TH2F*) m_fit_results->correlationHist();
        TCanvas* tmpc = new TCanvas("tmp", "tmp", 800, 700);
        tmpc->cd();
	tmpc->SetLeftMargin(0.22);
	tmpc->SetRightMargin(0.1);
	tmpc->SetBottomMargin(0.22);

	gStyle->SetPalette(1);
	correlation->GetXaxis()->SetLabelSize(0.03*36/m_paras->getSize());
	correlation->GetYaxis()->SetLabelSize(0.03*36/m_paras->getSize());
	correlation->GetZaxis()->SetLabelSize(0.03);
	correlation->GetXaxis()->LabelsOption("v");

	correlation->Draw("colz");
        tmpc->SaveAs((m_plotdir+string("Correlation.png")).c_str());
        tmpc->SaveAs((m_plotdir+string("Correlation.pdf")).c_str());
	delete tmpc;
	delete correlation;
    }

    if (stack) {
        m_lg->Info("s", "PLOT post fit stack histograms.");
	m_workspace->loadSnapshot("PreFit");
	if (m_inverse_mode) FixAllSys();
	Fit("Plain", false);

	bool showratio = true;
	bool has_sys = m_sysnuiss->getSize();
        for (int i = 0; i < m_chan_n; i++) {
            TH1F* h_total = (TH1F*)m_datas->at(i)->Clone((string("PostFitTotEvt")+m_chan_names->at(i)).c_str());
            for (int k = 0; k < h_total->GetNbinsX(); k++) {
                char tmp[100];
		{
		    sprintf(tmp, "FinNumTotEvtBin%d%s", k+1, m_chan_names->at(i).c_str());
                    RooFormulaVar* tmpvar = (RooFormulaVar*)m_functions_new->find(tmp);
                    if (!tmpvar) {
                        m_lg->Err("ss", "CANNOT find formula var -->", tmp);
                        exit(-1);
                    }
                    h_total->SetBinContent(k+1, tmpvar->getVal());

		    float error_total_sym = 0;
		    {
			TIterator* it = m_sysnuiss->createIterator();
    			RooRealVar *tmpsys;
    			while ((tmpsys = (RooRealVar*)it->Next())) {
    			    if (!tmpsys) continue;
			    pair<float,float> error_updn = GetSysUpDn(tmpvar, tmpsys);
			    float error = fabs(error_updn.first) > fabs(error_updn.second) ? fabs(error_updn.first) : fabs(error_updn.second);
			    error_total_sym += pow(error,2);
			}
		    }
		    error_total_sym = sqrt(error_total_sym);
		    error_total_sym *= tmpvar->getVal();
                    h_total->SetBinError(k+1, error_total_sym);
		}
            }
	    
            TH1F* h_sig = NULL;
            TH1F* h_hfake = NULL;
            TH1F* h_egamma = NULL;
	    bool hasother=false;
            TH1F* h_other = (TH1F*)m_datas->at(i)->Clone((string("PostFitOther")+m_chan_names->at(i)).c_str());
            for (int k = 0; k < h_other->GetNbinsX(); k++) { h_other->SetBinContent(k+1, 0); h_other->SetBinError(k+1, 0); }
	    h_other->Sumw2();
	    h_other->SetFillColor(kOrange+7);

            for (int j = 0; j < m_proc_names->at(i)->size(); j++) {
                TH1F* h_proc = (TH1F*)m_datas->at(i)->Clone((string("PostFit")+m_proc_names->at(i)->at(j)+m_chan_names->at(i)).c_str());
                for (int k = 0; k < h_proc->GetNbinsX(); k++) {
		    char tmp[100];
            	    sprintf(tmp, "Num%sBin%d%s", m_proc_names->at(i)->at(j).c_str(), k+1, m_chan_names->at(i).c_str());
            	    RooFormulaVar* tmpvar = (RooFormulaVar*)m_functions->find(tmp);
            	    if (!tmpvar) {
            	        m_lg->Err("ss", "CANNOT find formula var -->", tmp);
            	        exit(-1);
		    }

		    h_proc->SetBinContent(k+1, tmpvar->getVal());
		    h_proc->SetBinError(k+1, tmpvar->getPropagatedError(*m_fit_results));
                }

                h_proc->SetLineColor(kBlack);
                if (h_proc_issigs->GetBinContent(i+1, j+1) != 0) {
		    h_proc->SetFillColor(kRed+1);
		    h_sig = h_proc;
                } else {
		    if (m_proc_names->at(i)->at(j).find("HadronFake",0) != string::npos) {
			h_proc->SetFillColor(kCyan);
			h_hfake = h_proc;
		    } else if (m_proc_names->at(i)->at(j).find("EGamma",0) != string::npos) {
			h_proc->SetFillColor(kYellow+1);
			h_egamma = h_proc;
		    } else {
			hasother = true;
			h_proc->SetFillColor(kOrange+7);
			h_other->Add(h_proc);
		    }
                }
            }

            SetAtlasStyle();
	    TCanvas* tmpc = NULL;
	    TPad*pad1 = NULL;
	    TPad*pad2 = NULL;
	    if (!showratio) {
		tmpc = new TCanvas("tmpc", "tmpc", 800, 600);
	    } else {
		tmpc = new TCanvas("tmpc", "tmpc", 800, 800);
		pad1 =  new TPad("pad1","pad1name",0.,0.20,1.,1.);   
		pad2 =  new TPad("pad2","pad1name",0.,0.05,1.,0.30);    
	    }
	    if (!showratio) {
		tmpc->cd();
		tmpc->SetLeftMargin(0.12);
		tmpc->SetBottomMargin(0.15);
		if (logy) tmpc->SetLogy(1);
	    } else {
		pad1->Draw();
		pad2->Draw();
		pad2->SetGridy(true);
		pad2->SetBottomMargin(0.35);
		if (logy) pad1->SetLogy(true);
		else pad1->SetLogy(false);
		pad1->cd();
	    }

            h_total->SetFillStyle(3354);
	    gStyle->SetHatchesLineWidth(0.5);
            h_total->SetFillColor(kGray+1);
            h_total->SetLineWidth(2);
            h_total->GetXaxis()->SetTitle("p_{T}^{iso} [GeV]");
	    if (!showratio) {
		h_total->GetXaxis()->SetTitleSize(0.05);
		h_total->GetXaxis()->SetLabelSize(0.05);
	    } else {
		h_total->GetXaxis()->SetTitleSize(0.00001);
		h_total->GetXaxis()->SetLabelSize(0.00001);
	    }
            h_total->GetYaxis()->SetTitleSize(0.05);
            h_total->GetYaxis()->SetLabelSize(0.05);
            h_total->GetYaxis()->SetTitle("Event");
            if (!logy) h_total->GetYaxis()->SetRangeUser(0, 1.5*h_total->GetMaximum());
            else h_total->GetYaxis()->SetRangeUser(1, 2*h_total->GetMaximum());
            gStyle->SetHatchesLineWidth(3);
            if (has_sys) h_total->Draw("E2");
	    else h_total->Draw();

            THStack* hs = new THStack("hs", "");
            h_hfake->SetLineWidth(1);
            if (h_egamma != NULL) h_egamma->SetLineWidth(1);
            h_sig->SetLineWidth(1);
            h_other->SetLineWidth(1);
            h_other->SetLineColor(kBlack);
            hs->Add(h_hfake);
            if (h_egamma != NULL) hs->Add(h_egamma);
            if (hasother) hs->Add(h_other);
            hs->Add(h_sig);
            hs->Draw("HIST same");
            if (has_sys) h_total->Draw("E2 same");

            m_datas->at(i)->SetLineColor(kBlack);
            m_datas->at(i)->SetLineWidth(2);
            m_datas->at(i)->SetMarkerStyle(20);
            m_datas->at(i)->SetMarkerSize(1.5);
            m_datas->at(i)->Draw("PE HIST same");
	    pad1->RedrawAxis();

            TLegend* lg = new TLegend(0.65, 0.5, 0.88, 0.9);
	    string header;
	    if (m_chan_names->at(i) == "El") header = "e + jets Channel";
	    if (m_chan_names->at(i) == "Mu") header = "#mu + jets Channel";
	    if (m_chan_names->at(i) == "ElPt1525") header = "e (15 #leq p_{T} < 25) ";
	    if (m_chan_names->at(i) == "ElPt2540") header = "e (25 #leq p_{T} < 40) ";
	    if (m_chan_names->at(i) == "ElPt4060") header = "e (40 #leq p_{T} < 60) ";
	    if (m_chan_names->at(i) == "ElPt60100") header ="e (60 #leq p_{T} < 100) ";
	    if (m_chan_names->at(i) == "ElPt100300") header="e (100 #leq p_{T} < 300) ";
	    if (m_chan_names->at(i) == "MuPt1525") header   = "#mu (15 #leq p_{T} < 25) ";
	    if (m_chan_names->at(i) == "MuPt2540") header   = "#mu (25 #leq p_{T} < 40) ";
	    if (m_chan_names->at(i) == "MuPt4060") header   = "#mu (40 #leq p_{T} < 60) ";
	    if (m_chan_names->at(i) == "MuPt60100") header  = "#mu (60 #leq p_{T} < 100) ";
	    if (m_chan_names->at(i) == "MuPt100300") header = "#mu (100 #leq p_{T} < 300) ";
	    if (m_chan_names->at(i) == "ElEta025") header = "e (0 #leq |#eta| < 0.25) ";
	    if (m_chan_names->at(i) == "ElEta2555") header = "e (0.25 #leq |#eta| < 0.50) ";
	    if (m_chan_names->at(i) == "ElEta5590") header = "e (0.50 #leq |#eta| < 0.90) ";
	    if (m_chan_names->at(i) == "ElEta90137") header = "e (0.90 #leq |#eta| < 1.37) ";
	    if (m_chan_names->at(i) == "ElEta137237") header = "e (1.37 #leq |#eta| < 2.37) ";
	    if (m_chan_names->at(i) == "MuEta025") header = "#mu (0 #leq |#eta| < 0.25) ";
	    if (m_chan_names->at(i) == "MuEta2555") header = "#mu (0.25 #leq |#eta| < 0.50) ";
	    if (m_chan_names->at(i) == "MuEta5590") header = "#mu (0.50 #leq |#eta| < 0.90) ";
	    if (m_chan_names->at(i) == "MuEta90137") header = "#mu (0.90 #leq |#eta| < 1.37) ";
	    if (m_chan_names->at(i) == "MuEta137237") header = "#mu (1.37 #leq |#eta| < 2.37) ";
            lg->SetHeader(header.c_str());
      	    lg->SetBorderSize(0);
      	    lg->SetFillColor(0);
      	    lg->SetTextFont(42);
      	    lg->SetTextSize(0.045);
            lg->AddEntry(m_datas->at(i), "Data", "lep");
            //lg->AddEntry(h_sig, m_proc_titles->at(i)->at(0).c_str(), "f");
	    if (m_filename.find("WGamma") != string::npos) {
		lg->AddEntry(h_sig, "W#gamma+jets", "f");
	    } else {
		lg->AddEntry(h_sig, "t#bar{t}#gamma", "f");
	    }
            //lg->AddEntry(h_sig, "t#bar{t}#gamma", "f");
            if (hasother) lg->AddEntry(h_other, "Other Prompt-#gamma", "f");
            if (h_egamma != NULL) lg->AddEntry(h_egamma, "e#rightarrow#gamma Fake", "f");
            lg->AddEntry(h_hfake, "Hadron Fake", "f");
            if (has_sys) lg->AddEntry(h_total, "Uncertainty", "f");
            lg->Draw("same");

            TLatex lt;
    	    lt.SetNDC();
    	    lt.SetTextColor(1);
    	    lt.SetTextFont(72);
    	    lt.DrawLatex(0.19, 0.85, "ATLAS");
    	    lt.SetTextFont(42);
	    double delx = 0.115*696*gPad->GetWh()/(472*gPad->GetWw());
	    lt.DrawLatex(0.19+delx, 0.85, "Internal");
    	    if (!m_use_asimov) lt.DrawLatex(0.19, 0.77, "#sqrt{s} = 8 TeV, #int Ldt = 20.2 fb^{-1}");
	    else lt.DrawLatex(0.19, 0.77, "Asimov Data");

	    if (showratio) {
		pad2->cd();
		TH1F*h_total_forratio = (TH1F*)h_total->Clone();
		for (int ibin = 0; ibin < h_total->GetNbinsX(); ibin++) {
		    if (has_sys) h_total_forratio->SetBinError(ibin+1, h_total->GetBinError(ibin+1)/h_total->GetBinContent(ibin+1));
		    else h_total_forratio->SetBinError(ibin+1, 0);
		    h_total_forratio->SetBinContent(ibin+1, 1);
		}
		TH1F*h_data_forratio = (TH1F*)m_datas->at(i)->Clone();
		for (int ibin = 0; ibin < h_total->GetNbinsX(); ibin++) {
		    h_data_forratio->SetBinError(ibin+1, h_data_forratio->GetBinError(ibin+1)/h_total->GetBinContent(ibin+1));
		    h_data_forratio->SetBinContent(ibin+1, h_data_forratio->GetBinContent(ibin+1)/h_total->GetBinContent(ibin+1));
		}

		h_total_forratio->GetXaxis()->SetTitleSize(0.15);
		h_total_forratio->GetXaxis()->SetLabelSize(0.15);
		h_total_forratio->GetYaxis()->SetTitleSize(0.15);
		h_total_forratio->GetYaxis()->SetLabelSize(0.15);
		h_total_forratio->GetYaxis()->SetNdivisions(505);
		if (m_multipoi) h_total_forratio->GetYaxis()->SetRangeUser(0.4,1.6);
		else {
		    if (m_filename.find("WGamma") != string::npos) h_total_forratio->GetYaxis()->SetRangeUser(0.4,1.6);
		    else h_total_forratio->GetYaxis()->SetRangeUser(0.8,1.2);
		}
		h_total_forratio->GetYaxis()->SetTitle("Event");

		if (has_sys) {
		    h_total_forratio->Draw("E2");
		} else {
		    m_lg->Warn("s", "No sys at all!");
		    h_total_forratio->SetFillColor(0);
		    h_total_forratio->Draw();
		}
		h_data_forratio->Draw("same");
	    }

            tmpc->RedrawAxis();
            tmpc->Update();
            tmpc->SaveAs((m_plotdir+m_chan_names->at(i)+string("Hstack.png")).c_str());
            tmpc->SaveAs((m_plotdir+m_chan_names->at(i)+string("Hstack.pdf")).c_str());
            tmpc->SaveAs((m_plotdir+m_chan_names->at(i)+string("Hstack.C")).c_str());

            delete lg;
            delete hs;
	    delete tmpc;
        }
    }

    if (pull) {
        m_lg->Info("s", "PLOT systematics pulls.");
	m_workspace->loadSnapshot("PreFit");
	Fit("Plain", false);

        SetAtlasStyle();

	int nsys = m_sys_order.size();
	
	// load sys order
	if (m_pull_order != "") {
	    nsys = 20;
	    TFile* f = new TFile(m_pull_order.c_str());
	    if (f) {
	        m_lg->Warn("ss", "Reading sys order from file", m_pull_order.c_str());
	        m_sys_order.clear();
	        TH1F* h = (TH1F*)f->Get("Sys_Order");
	        for (int i = 0; i < nsys; i++) {
	            string sysname = h->GetXaxis()->GetBinLabel(i+1);
	            m_sys_order.push_back(sysname);
	        }
	    }
	}
	
	int sepa_point = 80;
	if (nsys <= sepa_point || m_filename.find("Diff",0) == string::npos) {
	    TH1F* h_pull_axis = new TH1F("PullAxis", "PullAxis", nsys, 0, nsys);
	    h_pull_axis->GetYaxis()->SetRangeUser(-3.5,3.5);
	    h_pull_axis->GetXaxis()->SetTitle("#theta");
	    h_pull_axis->GetYaxis()->SetTitle("(#theta_{fit} - #theta_{0}) / #Delta#theta_{0}");
	    
	    TGraphAsymmErrors *g_pull = new TGraphAsymmErrors(nsys);
	    g_pull->SetPoint(0, 0, 999);
	    
	    int isys = 0;
	    {
	        //TIterator* it = m_sysnuiss->createIterator();
	        for (int i = 0; i < m_sys_order.size(); i++) {
	    	RooRealVar* tmpvar = (RooRealVar*)m_sysnuiss->find(m_sys_order.at(i).c_str());
		    if (!tmpvar) {
		        m_lg->Warn("ss", "Cannot find sys for pull study -->",  m_sys_order.at(i).c_str());
		        continue;
		    }
		    
		    TString sysname = tmpvar->GetName();
		    float cen = m_prefit_val[tmpvar->GetName()];
		    float olderr = m_prefit_err[tmpvar->GetName()];
		    double pull  = (tmpvar->getVal() - cen) / olderr ; 
		    double errorHi = (tmpvar->getVal() + tmpvar->getErrorHi() - cen) / olderr; 
		    double errorLo = (tmpvar->getVal() + tmpvar->getErrorLo() - cen) / olderr; 
		    
		    isys++;
		    if (isys > nsys) break;
	    
		    double x = h_pull_axis->GetBinCenter(isys);
		    sysname.ReplaceAll("Sys","");
		    h_pull_axis->GetXaxis()->SetBinLabel(isys, sysname);
		    g_pull->SetPoint(isys, x, pull);
		    g_pull->SetPointEXlow(isys, h_pull_axis->GetBinWidth(isys)/3.0);
		    g_pull->SetPointEXhigh(isys, h_pull_axis->GetBinWidth(isys)/3.0);
		    g_pull->SetPointEYlow(isys, fabs(tmpvar->getErrorHi()));
		    g_pull->SetPointEYhigh(isys, fabs(tmpvar->getErrorLo()));
	        }
	    	
	        double _1SigmaValue[1000];
	        double _2SigmaValue[1000];
	        double NuisParamValue[1000];
	        for (int i = 0 ;i < nsys + 1 ; i++) {
	    	_1SigmaValue[i] = 1.0;
	          	_1SigmaValue[2*nsys-i] = -1;
	          	_2SigmaValue[i] = 2;
	          	_2SigmaValue[2*nsys-i] = -2;
	          	NuisParamValue[i] = i;
	          	NuisParamValue[2*nsys-1-i] = i;
	        }
	        TGraph *_1sigma = new TGraph(2*nsys,NuisParamValue,_1SigmaValue);
	        TGraph *_2sigma = new TGraph(2*nsys,NuisParamValue,_2SigmaValue);
	    
	        TCanvas *tmpc = NULL;
	        double length = 1200;
	        if (m_pull_order != "") length = 600;
	        tmpc = new TCanvas("tmp", "tmp", length, 400);
	        tmpc->SetBottomMargin(0.45);
	        tmpc->SetLeftMargin(0.1);
	        tmpc->cd();
	        g_pull->SetLineWidth(1);
	        g_pull->SetLineColor(1);
	        g_pull->SetMarkerColor(1);
	        g_pull->SetMarkerStyle(21);
	        g_pull->SetMarkerSize(1);      
	        _2sigma->SetFillColor(5);
	        _2sigma->SetLineColor(5);
	        _2sigma->SetMarkerColor(5);
	        _1sigma->SetFillColor(3);
	        _1sigma->SetLineColor(3);
	        _1sigma->SetMarkerColor(3);
	        h_pull_axis->GetXaxis()->LabelsOption("v");
	        h_pull_axis->GetYaxis()->SetTitleOffset(0.7);
	        h_pull_axis->GetXaxis()->SetTitle(" ");
	        h_pull_axis->Draw("hist");
	        _2sigma->Draw("F"); 
	        _1sigma->Draw("F"); 
	        g_pull->Draw("P");
	        h_pull_axis->GetYaxis()->DrawClone();
	    
	        TLine l(h_pull_axis->GetBinLowEdge(1), 0, h_pull_axis->GetBinLowEdge(h_pull_axis->GetNbinsX()+1), 0);
	        l.SetLineColor(kRed);
	        l.Draw();
	    
	        tmpc->RedrawAxis();
	        tmpc->Update();
	        tmpc->SaveAs((m_plotdir+string("Pull.png")).c_str());
	        tmpc->SaveAs((m_plotdir+string("Pull.pdf")).c_str());
	        delete _1sigma;
	        delete _2sigma;
	        delete tmpc;
	    }
	    delete h_pull_axis;
	    delete g_pull;
	} else {
	    int nsys = m_sys_order.size();
	    
	    TH1F* h_pull_axis1 = new TH1F("PullAxis1", "PullAxis1", sepa_point, 0, sepa_point);
    	    h_pull_axis1->GetYaxis()->SetRangeUser(-3.5,3.5);
    	    h_pull_axis1->GetXaxis()->SetTitle("#theta");
    	    h_pull_axis1->GetYaxis()->SetTitle("(#theta_{fit} - #theta_{0}) / #Delta#theta_{0}");
	    TH1F* h_pull_axis2 = new TH1F("PullAxis2", "PullAxis2", nsys-sepa_point, 0, nsys-sepa_point);
    	    h_pull_axis2->GetYaxis()->SetRangeUser(-3.5,3.5);
    	    h_pull_axis2->GetXaxis()->SetTitle("#theta");
    	    h_pull_axis2->GetYaxis()->SetTitle("(#theta_{fit} - #theta_{0}) / #Delta#theta_{0}");

    	    TGraphAsymmErrors *g_pull1 = new TGraphAsymmErrors(sepa_point);
    	    TGraphAsymmErrors *g_pull2 = new TGraphAsymmErrors(nsys-sepa_point);

            int isys = 0;
            {
                //TIterator* it = m_sysnuiss->createIterator();
		for (int i = 0; i < m_sys_order.size(); i++) {
		    RooRealVar* tmpvar = (RooRealVar*)m_sysnuiss->find(m_sys_order.at(i).c_str());
		    if (!tmpvar) {
		        m_lg->Warn("ss", "Cannot find sys for pull study -->",  m_sys_order.at(i).c_str());
		        continue;
		    }
		    float cen = m_prefit_val[tmpvar->GetName()];
		    float olderr = m_prefit_err[tmpvar->GetName()];
		    double pull  = (tmpvar->getVal() - cen) / olderr ; 
		    double errorHi = (tmpvar->getVal() + tmpvar->getErrorHi() - cen) / olderr; 
		    double errorLo = (tmpvar->getVal() + tmpvar->getErrorLo() - cen) / olderr; 
		    
		    isys++;
		    if (isys > nsys) break;
		    if (isys <= sepa_point) {
		        double x = h_pull_axis1->GetBinCenter(isys);
		        TString sysname = tmpvar->GetName();
		        sysname.ReplaceAll("Sys","");
		        h_pull_axis1->GetXaxis()->SetBinLabel(isys, sysname);
		        g_pull1->SetPoint(isys, x, pull);
		        g_pull1->SetPointEXlow(isys, h_pull_axis1->GetBinWidth(isys)/3.0);
		        g_pull1->SetPointEXhigh(isys, h_pull_axis1->GetBinWidth(isys)/3.0);
		        g_pull1->SetPointEYlow(isys, fabs(tmpvar->getErrorHi()));
		        g_pull1->SetPointEYhigh(isys, fabs(tmpvar->getErrorLo()));
		    } else {
		        double x = h_pull_axis2->GetBinCenter(isys-sepa_point);
		        TString sysname = tmpvar->GetName();
		        sysname.ReplaceAll("Sys","");
		        h_pull_axis2->GetXaxis()->SetBinLabel(isys-sepa_point, sysname);
		        g_pull2->SetPoint(isys-sepa_point, x, pull);
		        g_pull2->SetPointEXlow(isys-sepa_point, h_pull_axis2->GetBinWidth(isys-sepa_point)/3.0);
		        g_pull2->SetPointEXhigh(isys-sepa_point, h_pull_axis2->GetBinWidth(isys-sepa_point)/3.0);
		        g_pull2->SetPointEYlow(isys-sepa_point, fabs(tmpvar->getErrorHi()));
		        g_pull2->SetPointEYhigh(isys-sepa_point, fabs(tmpvar->getErrorLo()));
		    }
		}
            	
                double _1SigmaValue1[1000];
    	        double _2SigmaValue1[1000];
    	        double NuisParamValue1[1000];
                double _1SigmaValue2[1000];
    	        double _2SigmaValue2[1000];
    	        double NuisParamValue2[1000];
    	        for (int i = 0 ;i < sepa_point + 1 ; i++) {
		    _1SigmaValue1[i] = 1.0;
		    _1SigmaValue1[2*sepa_point-i] = -1;
		    _2SigmaValue1[i] = 2;
		    _2SigmaValue1[2*sepa_point-i] = -2;
		    NuisParamValue1[i] = i;
		    NuisParamValue1[2*sepa_point-1-i] = i;
    	        }
    	        for (int i = 0 ;i < nsys - sepa_point + 1 ; i++) {
		    _1SigmaValue2[i] = 1.0;
		    _1SigmaValue2[2*(nsys-sepa_point)-i] = -1;
		    _2SigmaValue2[i] = 2;
		    _2SigmaValue2[2*(nsys-sepa_point)-i] = -2;
		    NuisParamValue2[i] = i;
		    NuisParamValue2[2*(nsys-sepa_point)-1-i] = i;
    	        }
    	        TGraph *_1sigma1 = new TGraph(2*sepa_point,NuisParamValue1,_1SigmaValue1);
    	        TGraph *_2sigma1 = new TGraph(2*sepa_point,NuisParamValue1,_2SigmaValue1);
    	        TGraph *_1sigma2 = new TGraph(2*(nsys-sepa_point),NuisParamValue2,_1SigmaValue2);
    	        TGraph *_2sigma2 = new TGraph(2*(nsys-sepa_point),NuisParamValue2,_2SigmaValue2);

		bool isdiffpt = false;
		bool isdiffeta = false;
		if (m_chan_names->at(0).find("Pt",0) != string::npos) isdiffpt = true;
		else isdiffeta = true;
		string tag;
		if (isdiffpt) tag = "_Diff_Pt";
		if (isdiffeta) tag = "_Diff_Eta";

		{
		    TCanvas *tmpc = NULL;
		    double length = 1200;
		    tmpc = new TCanvas("tmp", "tmp", length, 400);
	            tmpc->SetBottomMargin(0.45);
                    tmpc->SetLeftMargin(0.1);
                    tmpc->cd();
    	            g_pull1->SetLineWidth(1);
    	            g_pull1->SetLineColor(1);
    	            g_pull1->SetMarkerColor(1);
    	            g_pull1->SetMarkerStyle(21);
    	            g_pull1->SetMarkerSize(1);      
    	            _2sigma1->SetFillColor(5);
    	            _2sigma1->SetLineColor(5);
    	            _2sigma1->SetMarkerColor(5);
    	            _1sigma1->SetFillColor(3);
    	            _1sigma1->SetLineColor(3);
    	            _1sigma1->SetMarkerColor(3);
                    h_pull_axis1->GetXaxis()->LabelsOption("v");
                    h_pull_axis1->GetYaxis()->SetTitleOffset(0.7);
                    h_pull_axis1->GetXaxis()->SetTitle(" ");
    	            h_pull_axis1->Draw("hist");
    	            _2sigma1->Draw("F"); 
    	            _1sigma1->Draw("F"); 
    	            g_pull1->Draw("P");
                    h_pull_axis1->GetYaxis()->DrawClone();

	            TLine l(h_pull_axis1->GetBinLowEdge(1), 0, h_pull_axis1->GetBinLowEdge(h_pull_axis1->GetNbinsX()+1), 0);
	            l.SetLineColor(kRed);
	            l.Draw();

	            tmpc->RedrawAxis();
	            tmpc->Update();
                    tmpc->SaveAs((m_plotdir+string("Pull")+tag+string("_1.png")).c_str());
                    tmpc->SaveAs((m_plotdir+string("Pull")+tag+string("_1.pdf")).c_str());
	            delete _1sigma1;
	            delete _2sigma1;
	            delete tmpc;
		}
		{
		    TCanvas *tmpc = NULL;
	            tmpc = new TCanvas("tmp", "tmp", 1200, 400);
	            tmpc->SetBottomMargin(0.45);
                    tmpc->SetLeftMargin(0.1);
                    tmpc->cd();
    	            g_pull2->SetLineWidth(1);
    	            g_pull2->SetLineColor(1);
    	            g_pull2->SetMarkerColor(1);
    	            g_pull2->SetMarkerStyle(21);
    	            g_pull2->SetMarkerSize(1);      
    	            _2sigma2->SetFillColor(5);
    	            _2sigma2->SetLineColor(5);
    	            _2sigma2->SetMarkerColor(5);
    	            _1sigma2->SetFillColor(3);
    	            _1sigma2->SetLineColor(3);
    	            _1sigma2->SetMarkerColor(3);
                    h_pull_axis2->GetXaxis()->LabelsOption("v");
                    h_pull_axis2->GetYaxis()->SetTitleOffset(0.7);
                    h_pull_axis2->GetXaxis()->SetTitle(" ");
    	            h_pull_axis2->Draw("hist");
    	            _2sigma2->Draw("F"); 
    	            _1sigma2->Draw("F"); 
    	            g_pull2->Draw("P");
                    h_pull_axis2->GetYaxis()->DrawClone();

	            TLine l(h_pull_axis2->GetBinLowEdge(1), 0, h_pull_axis2->GetBinLowEdge(h_pull_axis2->GetNbinsX()+1), 0);
	            l.SetLineColor(kRed);
	            l.Draw();

	            tmpc->RedrawAxis();
	            tmpc->Update();
                    tmpc->SaveAs((m_plotdir+string("Pull")+tag+string("_2.png")).c_str());
                    tmpc->SaveAs((m_plotdir+string("Pull")+tag+string("_2.pdf")).c_str());
	            delete _1sigma2;
	            delete _2sigma2;
	            delete tmpc;
		}
            }
	}
    }
}

pair<float, float> WorkspaceAnalyzor::GetSysUpDn(RooFormulaVar* var, RooRealVar* sys, bool prefit) {
    float cen = var->getVal();
    float oldsysval = sys->getVal();

    float low, high;
    sys->setVal(oldsysval+sys->getError());
    high = var->getVal();
    sys->setVal(oldsysval-sys->getError());
    low = var->getVal();
    sys->setVal(oldsysval);

    float up = high > low ? (high > cen ? high - cen : 0) : (low > cen ? low - cen : 0);
    float dn = low < high ? (low < cen ? cen - low : 0) : (high < cen ? cen - high : 0);
    up = up / cen;
    dn = dn / cen;

    return pair<float, float> (up, dn);
}

void WorkspaceAnalyzor::SetSaveName(string savename) {
    m_savename = savename;
}

void WorkspaceAnalyzor::SetSaveDir(string savedir) {
    m_savedir = savedir;
}

void WorkspaceAnalyzor::SetSaveOption(string saveoption) {
    m_saveoption = saveoption;
}

void WorkspaceAnalyzor::Save() {
    string savename = "";
    savename += m_savedir;
    savename += m_savename;

    m_lg->Info("ss", "SAVE results to file -->", savename.c_str());
    TFile* fsave = new TFile(savename.c_str(), m_saveoption.c_str());
    fsave->cd();

    h_fit_results_plc->Write(); 
    h_fit_results_poi->Write(); 
    
    if (h_sys_order) h_sys_order->Write();

    //savename = "Afterfit"; savename += m_appendname;
    //if (h_var_afterfit) h_var_afterfit->Write(savename.c_str());
    //savename = "PrefitFunc"; savename += m_appendname;
    //if (h_func_prefit) h_func_prefit->Write(savename.c_str());
    //savename = "AfterfitFunc"; savename += m_appendname;
    //if (h_func_prefit) h_func_afterfit->Write(savename.c_str());
    //savename = "PLCRange"; savename += m_appendname;
    //if (h_firstpoi_plc) h_firstpoi_plc->Write(savename.c_str());
    //savename = "HypoTest"; savename += m_appendname;
    //if (h_hypo_test) h_hypo_test->Write(savename.c_str());
    //savename = "PLCFunc"; savename += m_appendname;
    //if (f_plc) f_plc->Write(savename.c_str());
    //savename = "Correlation"; savename += m_appendname;
    //if (h_corr) h_corr->Write(savename.c_str());
    //savename = "PullAxis"; savename += m_appendname;
    //if (h_pull_axis) h_pull_axis->Write(savename.c_str());
    //savename = "PullGraph"; savename += m_appendname;
    //if (g_pull) g_pull->Write(savename.c_str());
    //for (int i = 0; i < m_channel_n; i++) {
    //    if (m_dataHists->at(i)) {
    //        savename = "HistData"; savename += m_chan_names.at(i); savename += m_appendname;
    //        m_dataHists->at(i)->Write(savename.c_str());
    //    }
    //    if (h_chans_postfit->at(i)) {
    //        savename = "AfterfitHistTotal"; savename += m_chan_names.at(i); savename += m_appendname;
    //        h_chans_postfit->at(i)->Write(savename.c_str());
    //    }
    //    for (int j = 0; j < m_proc_n.at(i); j++) {
    //        if (h_procs_postfit->at(i)->at(j)) {
    //    	savename = "AfterfitHist"; savename += m_proc_names.at(i).at(j); savename += m_chan_names.at(i); savename += m_appendname;
    //    	h_procs_postfit->at(i)->at(j)->Write(savename.c_str());
    //        }
    //    }
    //}
    //
    //savename = "ErrDecomposition"; savename += m_appendname;
    //if (h_err_decomp) h_err_decomp->Write(savename.c_str());

    fsave->Close();
    delete fsave;
}

void WorkspaceAnalyzor::UseAsimovData() {
    m_use_asimov = true;
    m_lg->Warn("s", "USE asimov data!");
    m_workspace->loadSnapshot("PreFit");
    Fit("Plain", false);
    const ModelConfig	    *modelconfig = m_modelconfig;
    const RooArgSet setpoi(*m_pois);
    RooArgSet setglob;
    m_dataset = AsymptoticCalculator::MakeAsimovData(*m_dataset, *modelconfig, setpoi, setglob);
}

//void WorkspaceAnalyzor::UseToyData(int size) {
//    m_lg->Warn("s", "USE pseudo data!");
//    RooRandom::randomGenerator()->SetSeed(0);
//    if (m_dataset) delete m_dataset
//    m_dataset = (RooDataSet*)m_model->generateBinned(tmpset, Name("DataSet"), Extended(), ProtoData(*(RooDataSet*)(m_modelconfig->GetProtoData())));
//}

//void WorkspaceAnalyzor::LoadResults(string name) {
//    m_lg->Err("s", "LoadResults not implemnted yet!");
//    exit(-1);
//    //if (m_inputfile) delete m_inputfile;
//    //m_inputfile = new TFile(name.c_str());
//    //if (!m_inputfile) {
//    //    m_lg->Err("ss", "Cannot load file -->", name.c_str());
//    //    exit(-1);
//    //}
//    //
//    //string loadname;
//    //loadname = "Prefit"; loadname += m_appendname;
//    //h_var_prefit = (TH1F*)m_inputfile->Get(loadname.c_str());
//    //loadname = "Afterfit"; loadname += m_appendname;
//    //h_var_afterfit = (TH1F*)m_inputfile->Get(loadname.c_str());
//    //loadname = "PrefitFunc"; loadname += m_appendname;
//    //h_func_prefit = (TH1F*)m_inputfile->Get(loadname.c_str());
//    //loadname = "AfterfitFunc"; loadname += m_appendname;
//    //h_func_afterfit = (TH1F*)m_inputfile->Get(loadname.c_str());
//    //loadname = "PLCRange"; loadname += m_appendname;
//    //h_firstpoi_plc = (TH1F*)m_inputfile->Get(loadname.c_str());
//    //loadname = "HypoTest"; loadname += m_appendname;
//    //h_hypo_test = (TH1F*)m_inputfile->Get(loadname.c_str());
//    //loadname = "PLCFunc"; loadname += m_appendname;
//    //f_plc = (TF1*)m_inputfile->Get(loadname.c_str());
//    //loadname = "Correlation"; loadname += m_appendname;
//    //h_corr = (TH2F*)m_inputfile->Get(loadname.c_str());
//}
//void WorkspaceAnalyzor::SetNuisVal(string name, float val, bool verbose) {
//    bool succeed = false;
//    RooRealVar *tmpvar = (RooRealVar*)(m_allnuis->find(name.c_str()));
//    if (!tmpvar) {
//	m_lg->Err("ss", "Cannot find parameter to set value -->", name.c_str());
//        exit(-1);
//    } else {
//	tmpvar->setVal(val);
//	if (m_verbose && verbose) m_lg->Info("sssf", "Parameter -->", name.c_str(), "'s value set to -->", val);
//    }
//}
//
//void WorkspaceAnalyzor::SetNuisVal(vector<string> names, float val, bool verbose) {
//    for (int i = 0; i < names.size(); i++) {
//	SetNuisVal(names.at(i), val, verbose);
//    }
//}
//
//void WorkspaceAnalyzor::SetNuisVal(vector<string> names, vector<float> vals, bool verbose) {
//    for (int i = 0; i < names.size(); i++) {
//	SetNuisVal(names.at(i), vals.at(i), verbose);
//    }
//}
//
//void WorkspaceAnalyzor::PrintParaVals() {
//    RooArgSet allvars = m_workspace->allVars();
//    {
//        TIterator* it = allvars.createIterator();
//        RooRealVar *tmpvar;
//        while ((tmpvar = (RooRealVar*)it->Next())) {
//            if (!tmpvar) continue;
//	    m_lg->Info("sssf", "Parameter -->", tmpvar->GetName(), "'s value -->", tmpvar->getVal());
//        }
//        delete tmpvar;
//    }
//}
//
//void WorkspaceAnalyzor::SetParaVal(vector<string> names, float val) {
//    for (int i = 0; i < names.size(); i++) {
//	RooArgSet allvars = m_workspace->allVars();
//    	{
//    	    TIterator* it = allvars.createIterator();
//    	    RooRealVar *tmpvar;
//	    while ((tmpvar = (RooRealVar*)it->Next())) {
//	        if (!tmpvar) continue;
//	        if (tmpvar->GetName() == names.at(i)) {
//		    m_lg->Info("sssf", "Parameter -->", names.at(i).c_str(), "'s value set to -->", val);
//		    tmpvar->setVal(val);
//		}
//	    }
//	    delete tmpvar;
//    	}
//    }
//}
//
//
//
//
//void WorkspaceAnalyzor::SetHistAppend(string appendname) {
//    m_appendname = appendname;
//}
//
//void WorkspaceAnalyzor::ToyStudy(string sys) {
//    m_lg->Info("ss", "Toy Study for", sys.c_str());
//    m_toy_cnt++;
//
//    RooRealVar *sysvar = NULL;
//    string systitle = "";
//    if (sys == "Stat") {
//	systitle = "Statistical Uncertainty";
//
//	m_workspace->loadSnapshot("BeforeFit");
//	FixAllSys();
//	Fit(false, false, false, false, false);
//	if (ErrDecomp["Stat"] < 0) {
//	    ErrDecomp["Stat"] = 100*m_firstpoi->getError()/m_firstpoi->getVal();
//	    h_err_decomp->SetBinContent(1, 100*m_firstpoi->getError()/m_firstpoi->getVal());
//	}
//    } else {
//    	if (!SysExist(sys)) {
//    	    m_lg->Warn("ss", "Sys does not exist for toy study -->", sys.c_str());
//	    return;
//    	} else {
//    	    sysvar = m_workspace->var(sys.c_str());
//    	}
//	systitle = sysvar->GetTitle();
//
//	if (ErrDecomp[sys] < 0) {
//	    m_workspace->loadSnapshot("BeforeFit");
//	    FixAllSys();
//	    Fit(false, false, false, false, false);
//	    float norm = 100*m_firstpoi->getError()/m_firstpoi->getVal(); 
//
//	    m_workspace->loadSnapshot("BeforeFit");
//	    FixAllSys();
//	    FloatNuis(sys);
//	    Fit(false, false, false, false, false);
//	    float alt = 100*m_firstpoi->getError()/m_firstpoi->getVal(); 
//	    
//	    float diff = (pow(alt, 2) - pow(norm ,2) <= 0) ? 0 : sqrt(pow(alt, 2) - pow(norm, 2));
//	    ErrDecomp[sys] = diff;
//	    h_err_decomp->SetBinContent(GetNuisIndex(sys)+2, diff);
//	}
//	ScanNuis(true);
//
//	m_workspace->loadSnapshot("BeforeFit");
//	Fit(false, false, false, false, false);
//    }
//
//    if (ErrDecomp[sys] < 0.1) {
//	m_lg->Warn("ss", "Expected err too small --> SKIP sys", sys.c_str());
//	return;
//    }
//
//    float center = m_firstpoi->getVal();
//    float low = m_firstpoi->getVal() - m_toy_fitrange  * ErrDecomp[sys];
//    float high = m_firstpoi->getVal() + m_toy_fitrange * ErrDecomp[sys];
//
//    string title = "#sigma_{t#bar{t}#gamma}#times BR [fb]";
//    TH1F* tmph = new TH1F("tmph", title.c_str(), m_toy_fitbinning, low, high);
//
//    RooArgSet obs;
//    if (m_wstype == 0) {
//        obs.add(*m_obs);
//        obs.add(*m_chans);
//    } else if (m_wstype == 1) {
//        obs.add(*m_obss);
//    }
//
//    RooMCStudy *MCS = NULL;
//    if (sys == "Stat") MCS = new RooMCStudy(*m_model, obs, Extended((m_wstype == 1 ? false : true)), Binned((m_wstype == 1 ? false : true)), Silence());
//    else MCS = new RooMCStudy(*m_model, obs, Extended((m_wstype == 1 ? false : true)), Binned((m_wstype == 1 ? false : true)), Silence(), Constrain(*sysvar));
//
//    MCS->generateAndFit(m_toy_ntoy, (m_wstype == 1 ? (sys == "Stat" ? 1 : m_toy_nevt) : (sys == "Stat" ? m_data_total : m_toy_nevt * m_data_total)), false);
//    MCS->fitParDataSet().fillHistogram(tmph, *m_firstpoi);
//
//    RooRealVar tmpx("tmpx", title.c_str(), tmph->GetMean(), low, high);
//    RooRealVar tmpmean("tmpmean", "mean", tmph->GetMean(), low, high);
//    RooRealVar tmpsigma("tmpsigma", "sigma", tmph->GetRMS(), 0, 1e3);
//    RooGaussian tmpgauss("tmpgauss","gauss", tmpx, tmpmean, tmpsigma);
//    RooDataHist tmpData = RooDataHist("tmpData", "Data", tmpx, tmph);
//    tmpgauss.fitTo(tmpData);
//
//    if (tmpmean.getVal() == 0) {
//	m_lg->Err("s", "Bad fit! mean = 0!");
//	return;
//    }
//
//    if (tmpsigma.getError() / tmpsigma.getVal() > 0.1) {
//	m_lg->Warn("sf", "Too large fit err (rel) -->", tmpsigma.getError() / tmpsigma.getVal());
//    }
//
//    m_lg->Info("sssfsf", "Calculated",  systitle.c_str(), "is", tmpsigma.getVal(), " +/- ", tmpsigma.getError());
//    ErrDecomp[sys] = tmpsigma.getVal();
//    if (sys == "Stat") h_err_decomp->SetBinContent(1, 100*tmpsigma.getVal()/tmpmean.getVal());
//    else h_err_decomp->SetBinContent(GetNuisIndex(sys)+2, 100*tmpsigma.getVal()/tmpmean.getVal());
//    
//    RooPlot *tmpframe = tmpx.frame();
//    tmpData.plotOn(tmpframe, DataError(RooAbsData::SumW2));
//    tmpgauss.paramOn(tmpframe, Layout(0.6,0.89,0.89), Parameters(RooArgList(tmpmean,tmpsigma)));
//    tmpgauss.plotOn(tmpframe, LineColor(kBlue));
//    tmpframe->SetMaximum(tmpframe->GetMaximum()*1.2);
//        
//    if (tmpc) delete tmpc;
//    tmpc = new TCanvas("tmp", "tmp", 800, 600);
//    tmpframe->Draw();
//    tmpc->SaveAs((m_plotdir+string("Toy_")+sys+string(".png")).c_str());
//    tmpc->SaveAs((m_plotdir+string("Toy_")+sys+string(".pdf")).c_str());
//    delete tmph;
//}
//int WorkspaceAnalyzor::GetNuisIndex(string name) {
//    for (int i = 0; i < m_nuis_names->size(); i++) {
//	if (m_nuis_names->at(i) == name) {
//	    return i;
//	}
//    }
//    return -1;
//}
//
//int WorkspaceAnalyzor::GetSysIndex(string name) {
//    for (int i = 0; i < m_sys_names->size(); i++) {
//	if (m_sys_names->at(i) == name) {
//	    return i;
//	}
//    }
//    return -1;
//}
//
//int WorkspaceAnalyzor::GetFuncIndex(string name) {
//    for (int i = 0; i < m_func_names->size(); i++) {
//	if (m_func_names->at(i) == name) {
//	    return i;
//	}
//    }
//    return -1;
//}
//
//int WorkspaceAnalyzor::GetPdfIndex(string name) {
//    for (int i = 0; i < m_pdf_names->size(); i++) {
//	if (m_pdf_names->at(i) == name) {
//	    return i;
//	}
//    }
//    return -1;
//}

WorkspaceAnalyzor::~WorkspaceAnalyzor() {
    delete m_infile;
    delete m_obss;
    delete m_model;
    delete m_dataset;
    delete m_workspace;
    delete m_modelconfig;
    delete m_obs;
    delete m_chans;
    delete m_chants;
    delete m_pois;
    delete m_nuiss;
    delete m_nonsysnuiss;
    delete m_sysnuiss;
    delete m_sys_names;
    delete m_sys_titles;
    delete m_paras;
    delete m_functions;
    delete m_functions_new;
    delete m_PLC;
    delete m_data_nums;
    delete m_fit_results;
    delete m_LI;
    delete h_proc_issigs;
    delete h_proc_isfrees;
    for (int i = 0; i < m_chan_n; i++) {
        delete m_procs->at(i);
	delete m_procts->at(i);
	delete m_datas->at(i);
	for (int j = 0; j < m_proc_n->at(i); j++) {
	    delete m_proc_temps->at(i)->at(j);
	}
    }
    //delete m_NLL;
}
