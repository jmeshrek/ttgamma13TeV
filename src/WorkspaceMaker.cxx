#include "WorkspaceMaker.h"
#include <TCanvas.h>
#include "StringPlayer.h"
#include <list>

using namespace std;

WorkspaceMaker::WorkspaceMaker() {

    m_debug = false;
    m_lg = new Logger("Workspace Maker");
    m_lg->Info("s", "Hi, this is a Workspace Maker~");
    m_lg->NewLine();

    // observable
    m_obs_ready = false;
    m_obs_title = "Undefined";
    m_obs_unit = "Undefined";
    m_obs_low = -999;
    m_obs_high = -999;
    m_obs_nbin = -999;
    m_bin_to_use = 999;
    m_obs = NULL;
    
    // channel
    m_chan_ready = false;
    m_chan_names = new vector<string>();
    m_chan_titles = new vector<string>();
    m_chans = new RooCategory("Channels","Channels");
    m_chants = new RooCategory("ChannelTitles","ChannelTitles");
    m_chan_n = 0;

    // process
    m_proc_ready = false;
    m_proc_n = new vector<int>();
    m_proc_names = new vector<vector<string>* >();
    m_proc_titles = new vector<vector<string>* >();
    m_proc_issigs = new vector<vector<bool>* >();
    m_procs = new vector<RooCategory*>();
    m_procts = new vector<RooCategory*>();
    h_proc_issigs = new TH2F("ProcIsSig", "ProcIsSig", 100, 0, 100, 100, 0, 100);
    h_proc_isfrees = new TH2F("ProcIsFree", "ProcIsFree", 100, 0, 100, 100, 0, 100);

    // parameter
    m_pois = new RooArgSet();
    m_globs = new RooArgSet();
    m_nuiss = new RooArgSet();
    m_paras = new RooArgSet();
    m_proc_paras = new vector<vector<RooArgSet*>*>();

    // template
    m_proc_temps = new vector<vector<TH1F*>*>();

    // data
    m_datas = new vector<TH1F*>();

    // check
    m_checked = false;

    // workspace
    m_obss = new RooArgSet();
    m_model = NULL;
    m_dataset = NULL;
    m_workspace = NULL;
    m_modelconfig = NULL;

    // systematics
    m_shapesys_off = false;
    m_syss = new RooArgSet();
    m_sys_csts = new RooArgSet();
    m_sys_norm_responses = new vector<vector<RooArgSet*>*>();
    m_sys_norm_responses2 = new vector<vector<RooArgSet*>*>();
    m_sys_shape_responses = new vector<vector<vector<RooArgSet*>*>*>();
    m_response_type = 0;
    
    //m_ProcNumbers = new vector<vector<RooProduct*>*>();
    //m_TotalNumbers = new vector<RooAddition*>();
    //m_BkgNumbers = new vector<RooAddition*>();
    //m_ProcModels = new vector<vector<RooAbsPdf*>*>();
}

void WorkspaceMaker::ShapeSysOff() {
    m_shapesys_off = true;
}

void WorkspaceMaker::ShutUp() {
    m_lg->ShutUp();
}

void WorkspaceMaker::Silent() {
    RooMsgService::instance().setSilentMode(kTRUE);
    RooMsgService::instance().setGlobalKillBelow(WARNING);
}

void WorkspaceMaker::UseNbins(int bin_to_use) {
    m_bin_to_use = bin_to_use;
}

void WorkspaceMaker::DebugMode(bool debug) {
    m_debug = debug;
}

void WorkspaceMaker::DefineObs(string title, string unit, float low, float high, int nbin) {
    if (m_obs_ready) return;
    
    m_lg->Info("sssfsfss", "DEFINE Observable -->", title.c_str(), "[", low, ",", high, "]", unit.c_str());

    m_obs_title = title;
    m_obs_unit = unit;
    m_obs_low = low;
    m_obs_high = high;
    m_obs_nbin = nbin;
    m_obs = new RooRealVar("Observable", title.c_str(), low, high, unit.c_str());
    m_obs->setBins(nbin);

    m_obs_ready = true;
}

bool WorkspaceMaker::ChannelExist(string name) {
    for (int i = 0; i < (int)m_chan_names->size(); i++) {
	if (m_chan_names->at(i) == name) {
	    return true;
	}
    } 
    return false;
}

void WorkspaceMaker::AddChannel(string name, string title) {
    m_lg->Info("ss", "ADD CHANNEL -->", title.c_str());

    if (ChannelExist(name)) return;

    m_chan_names->push_back(name);
    m_chan_titles->push_back(title);
    m_chans->defineType(name.c_str());
    m_chants->defineType(title.c_str());
}

void WorkspaceMaker::GetChannelIndex(string name, int & index) {
    for (int i = 0; i < m_chan_names->size(); i++) {
	if (m_chan_names->at(i) == name) {
	    index = i;
	    return;
	}
    }
    m_lg->Err("ss", "Cannot find channel -->", name.c_str());
    exit(-1);
}

void WorkspaceMaker::FreezeChannels() {
    m_lg->Info("s", "FREEZE channels.");
    m_chan_ready = true;

    m_chan_n = m_chan_names->size();

    m_procs->resize(m_chan_n);
    m_procts->resize(m_chan_n);
    m_proc_n->resize(m_chan_n);
    m_proc_names->resize(m_chan_n);
    m_proc_titles->resize(m_chan_n);
    m_proc_issigs->resize(m_chan_n);
    m_proc_paras->resize(m_chan_n);
    m_proc_temps->resize(m_chan_n);
    m_datas->resize(m_chan_n);
    m_sys_norm_responses->resize(m_chan_n);
    m_sys_norm_responses2->resize(m_chan_n);
    m_sys_shape_responses->resize(m_chan_n);

    for (int i = 0; i < m_chan_n; i++) {
	m_procs->at(i) = new RooCategory((string("Proc")+m_chan_names->at(i)).c_str(), (string("Processes of ")+m_chan_titles->at(i)).c_str());
	m_procts->at(i) = new RooCategory((string("ProcTit")+m_chan_names->at(i)).c_str(), (string("Processe Titles of ")+m_chan_titles->at(i)).c_str());
	m_proc_names->at(i) = new vector<string>();
	m_proc_titles->at(i) = new vector<string>();
	m_proc_issigs->at(i) = new vector<bool>();
    	m_proc_paras->at(i) = new vector<RooArgSet*>();
    	m_proc_temps->at(i) = new vector<TH1F*>();
    	m_sys_norm_responses->at(i) = new vector<RooArgSet*>();
    	m_sys_norm_responses2->at(i) = new vector<RooArgSet*>();
    	m_sys_shape_responses->at(i) = new vector<vector<RooArgSet*>*>();
    }
}

void WorkspaceMaker::GetProcIndex(string channelname, string procname, int &index) {
    int channelindex; GetChannelIndex(channelname, channelindex);

    for (int i = 0; i < m_proc_names->at(channelindex)->size(); i++) {
	if (m_proc_names->at(channelindex)->at(i) == procname) {
	    index = i;
	    return;
	}
    }
    m_lg->Err("ssss", "Cannot find process --> \"", procname.c_str(), "\" in", m_chan_titles->at(channelindex).c_str());
    exit(-1);
}

void WorkspaceMaker::SetResponseType(int type) {
    m_response_type = type;
}

void WorkspaceMaker::SetData(TH1F *data, string channelname) {
    if (!m_chan_ready) {
	m_lg->Err("s", "CHANNEL configuration not ready!");
	exit(-1);
    }

    int channelindex; GetChannelIndex(channelname, channelindex);
    m_lg->Info("ss", "ADD DATA to -->", m_chan_titles->at(channelindex).c_str());

    if (!data) {
	m_lg->Err("s", "Empty histogram!");
	exit(-1);
    }

    if (m_datas->at(channelindex)) delete m_datas->at(channelindex);
    m_datas->at(channelindex) = (TH1F*)data->Clone();
    m_datas->at(channelindex)->SetName((string("Data") + m_chan_names->at(channelindex)).c_str());
    m_datas->at(channelindex)->SetTitle((string("Data of ") + m_chan_titles->at(channelindex)).c_str());
}

void WorkspaceMaker::AddProcess(string channelname, string name, string title, bool signal) {
    if (!m_chan_ready) {
	m_lg->Err("s", "CHANNEL configuration not ready!");
	exit(-1);
    }

    int channelindex; GetChannelIndex(channelname, channelindex);
    m_lg->Info("ssss", "ADD PROCESS --> \"", title.c_str(), "\" to", m_chan_titles->at(channelindex).c_str());
    
    m_procs->at(channelindex)->defineType(name.c_str());
    m_procts->at(channelindex)->defineType(title.c_str());
    m_proc_names->at(channelindex)->push_back(name);
    m_proc_titles->at(channelindex)->push_back(title);
    m_proc_issigs->at(channelindex)->push_back(signal);
    h_proc_issigs->SetBinContent(channelindex+1, m_proc_names->at(channelindex)->size(), signal ? 1 : 0);
}

void WorkspaceMaker::FreezeProcesses() {
    m_lg->Info("s", "FREEZE processes.");

    m_proc_ready = true;

    for (int i = 0; i < m_chan_n; i++) {
	m_proc_n->at(i) = m_proc_names->at(i)->size();
	m_proc_paras->at(i)->resize(m_proc_n->at(i));
	m_proc_temps->at(i)->resize(m_proc_n->at(i));
	m_sys_norm_responses->at(i)->resize(m_proc_n->at(i));
	m_sys_norm_responses2->at(i)->resize(m_proc_n->at(i));
	m_sys_shape_responses->at(i)->resize(m_proc_n->at(i));
	for (int j = 0; j < m_proc_n->at(i); j++) {
	    m_sys_norm_responses->at(i)->at(j) = new RooArgSet();
	    m_sys_norm_responses2->at(i)->at(j) = new RooArgSet();
	    m_sys_shape_responses->at(i)->at(j) = new vector<RooArgSet*>();
	    m_sys_shape_responses->at(i)->at(j)->resize(m_obs_nbin);
	    for (int k = 0; k < m_obs_nbin; k++) {
	        m_sys_shape_responses->at(i)->at(j)->at(k) = new RooArgSet();
	    }
	}
    }
}

void WorkspaceMaker::AddParameter(string channelname, string procname, string name, string title, float val, float low, float high, bool fixed, bool poi) {
    if (!m_chan_ready) {
	m_lg->Err("s", "CHANNEL configuration not ready!");
	exit(-1);
    }
    if (!m_proc_ready) {
	m_lg->Err("s", "PROCESS configuration not ready!");
	exit(-1);
    }

    RooRealVar* tmpvar = NULL;
    if (m_paras->find(name.c_str())) tmpvar = (RooRealVar*)m_paras->find(name.c_str());
    else {
	tmpvar = new RooRealVar(name.c_str(), title.c_str(), val, low, high);
	tmpvar->setConstant(fixed);
	m_paras->add(*tmpvar);
	if (!fixed) {
	    if (poi) m_pois->add(*tmpvar);
	    else {
		m_nuiss->add(*tmpvar);
	    }
	}
	m_lg->Info("ss", "ADD PARAMETER --> \"", title.c_str());
	if (m_debug) tmpvar->Print("v");
	if (m_debug) cout << fixed << endl;
    }

    int channelindex; GetChannelIndex(channelname, channelindex);
    int procindex; GetProcIndex(channelname, procname, procindex);

    if (!m_proc_paras->at(channelindex)->at(procindex)) m_proc_paras->at(channelindex)->at(procindex) = new RooArgSet();
    if (m_proc_paras->at(channelindex)->at(procindex)->find(name.c_str())) return;

    m_proc_paras->at(channelindex)->at(procindex)->add(*tmpvar);
    if (!fixed) h_proc_isfrees->SetBinContent(channelindex+1, procindex+1, 1);
}

float WorkspaceMaker::GetParameterVal(string channelname, string procname, string name) {
    int channelindex; GetChannelIndex(channelname, channelindex);
    int procindex; GetProcIndex(channelname, procname, procindex);
    RooRealVar* tmpvar = NULL;
    tmpvar = (RooRealVar*)m_proc_paras->at(channelindex)->at(procindex)->find(name.c_str());
    if (!tmpvar) return -9999;
    else return tmpvar->getVal();
}

void WorkspaceMaker::SetTemplate(TH1F *temp, string channelname, string procname) {
    if (!m_chan_ready) {
	m_lg->Err("s", "CHANNEL configuration not ready!");
	exit(-1);
    }
    if (!m_proc_ready) {
	m_lg->Err("s", "PROCESS configuration not ready!");
	exit(-1);
    }

    int channelindex; GetChannelIndex(channelname, channelindex);
    int procindex; GetProcIndex(channelname, procname, procindex);
    m_lg->Info("ssss", "ADD TEMPLATE for --> \"", m_proc_titles->at(channelindex)->at(procindex).c_str(), "\" in", m_chan_titles->at(channelindex).c_str());

    if (!temp) {
	m_lg->Err("s", "Empty histogram!");
	exit(-1);
    }

    temp->Scale(1/temp->Integral());

    if (m_proc_temps->at(channelindex)->at(procindex)) delete m_proc_temps->at(channelindex)->at(procindex);
    m_proc_temps->at(channelindex)->at(procindex) = (TH1F*)temp->Clone();
    m_proc_temps->at(channelindex)->at(procindex)->SetName((string("Temp") + m_proc_names->at(channelindex)->at(procindex) + m_chan_names->at(channelindex)).c_str());
    m_proc_temps->at(channelindex)->at(procindex)->SetTitle((string("Tempalte of ") + m_proc_titles->at(channelindex)->at(procindex) + string(" of") + m_chan_titles->at(channelindex)).c_str());
}

//void WorkspaceMaker::CorrProcTemplate(string name, string title, TH1F *proccorr, int type, float frac, float err, string channelname, string procname) {
//    bool fixed = err > 0 ? false : true;
//
//    i int channelindex; GetChannelIndex(channelname, channelindex);
//    int procindex; GetProcIndex(channelname, procname, procindex);
//    m_lg->Info("ssss", "CORRECT template for --> \"", m_proc_titles->at(channelindex)->at(procindex).c_str(), "\" in", m_chan_titles->at(channelindex).c_str());
//
//    TH1F* procori = (TH1F*)m_proc_temps->at(channelindex)->at(procindex)->Clone();
//    procori->Scale(1/procori->Integral());
//    proccorr->Scale(1/proccorr->Integral());
//
//    TH1F* procnew = (TH1F*)procori->Clone();
//    if (type == 0) {
//	TH1F* htmp = (TH1F*)proccorr->Clone();
//	htmp->Scale(frac);
//	procnew->Add(htmp);
//	procnew->Scale(1/procnew->Integral());
//    } else if (type == 1) {
//	procnew->Scale(1-frac);
//	TH1F* htmp = (TH1F*)proccorr->Clone();
//	htmp->Scale(frac);
//	procnew->Add(htmp);
//	procnew->Scale(1/procnew->Integral());
//    }
//
//    if (m_proc_temps->at(channelindex)->at(procindex)) delete m_proc_temps->at(channelindex)->at(procindex);
//    m_proc_temps->at(channelindex)->at(procindex) = procnew;
//    m_proc_temps->at(channelindex)->at(procindex)->SetName((string("Hist") + m_proc_names->at(channelindex)->at(procindex) + m_chan_names->at(channelindex)).c_str());
//    m_proc_temps->at(channelindex)->at(procindex)->SetTitle((string("Histogram of") + m_proc_titles->at(channelindex)->at(procindex) + string(" (") + m_chan_titles->at(channelindex) + string(")")).c_str());
//
//    string sysname = "Sys"; sysname += name;
//    RooRealVar* tmpvar = NULL;
//    if (!fixed) {
//    	tmpvar = (RooRealVar*)m_SysVars->find(sysname.c_str());
//    	if (!tmpvar) {
//    	    m_lg->Info("ss", "CREATE nuissance parameter for the correction on -->", title.c_str());
//    	    AddSysSource(sysname, title + string(" Nuis"), fixed);
//    	    tmpvar = (RooRealVar*)m_SysVars->find(sysname.c_str());
//    	}
//
//    	map<string,string>::iterator map_it;;
//    	map_it = map_sys_proc.find(sysname);
//    	if (map_it == map_sys_proc.end()) {
//    	    map_sys_proc.insert(pair<string,string> 
//    	    	     (sysname, (m_proc_titles->at(channelindex)->at(procindex) + string(" (") + m_chan_titles->at(channelindex) + string(")"))));
//    	} else {
//    	    string tmpstr = map_sys_proc[sysname];
//    	    tmpstr += ",";
//    	    tmpstr += (m_proc_titles->at(channelindex)->at(procindex) + string(" (") + m_chan_titles->at(channelindex) + string(")"));
//    	    map_sys_proc[sysname] = tmpstr;
//    	}
//    }
//
//    if (m_wstype == 1) {
//	char tmp1[100];
//    	char tmp2[100];
//
//	if (fixed) {
//	    for (int i = 0; i < m_obs_nb; i++) {
//    	        if (m_ProcFractions->at(channelindex)->at(procindex)->at(i)) delete  m_ProcFractions->at(channelindex)->at(procindex)->at(i);
//    	        sprintf(tmp1, "FR%sBin%d%s", m_proc_names->at(channelindex)->at(procindex).c_str(), i+1, m_chan_names->at(channelindex).c_str());
//    	        sprintf(tmp2, "Fraction of %s in Bin %d (%s)", m_proc_titles->at(channelindex)->at(procindex).c_str(), i+1, m_chan_titles->at(channelindex).c_str());
//    	        RooProduct* ProcFR = new RooProduct(tmp1, tmp2, RooArgSet(RooConst(1), RooConst(m_proc_temps->at(channelindex)->at(procindex)->GetBinContent(i+1))));
//    	        m_ProcFractions->at(channelindex)->at(procindex)->at(i) = ProcFR;
//    	    }
//	} else {
//	    string systitle = tmpvar->GetTitle();
//	    m_lg->Info("ssssss", "ADD response to --> \"", systitle.c_str(), "\" for \"", m_proc_titles->at(channelindex)->at(procindex).c_str(), "\" in \"", m_chan_titles->at(channelindex).c_str());
//	    for (int i = 0; i < m_obs_nb; i++) {
//		float binnom_i = procori->GetBinContent(i+1);
//		float binnew_i = procnew->GetBinContent(i+1);
//		float bincorr_i = proccorr->GetBinContent(i+1);
//
//    		RooFormulaVar* binresponse = NULL;
//		sprintf(tmp1, "Resp%sBin%d%s%s", m_proc_names->at(channelindex)->at(procindex).c_str(), i+1, m_chan_names->at(channelindex).c_str(), sysname.c_str());
//		sprintf(tmp2, "Response of %s in Bin %d (%s) to %s", m_proc_titles->at(channelindex)->at(procindex).c_str(), i+1, m_chan_titles->at(channelindex).c_str(), systitle.c_str());
//    		if (type == 1) {
//    		    if (m_response_type == 0) binresponse = new RooFormulaVar(tmp1, tmp2, "(@0*(1-@2*(1+@3*@4)) + @1*@2*(1+@3*@4))/@5", RooArgSet(RooConst(binnom_i), RooConst(bincorr_i), RooConst(frac), RooConst(err), *tmpvar, RooConst(binnew_i)));
//    		    else if (m_response_type == 1) binresponse = new RooFormulaVar(tmp1, tmp2, "(@0*(1-@2*pow(1+@3,@4)) + @1*@2*pow(1+@3,@4))/@5", RooArgSet(RooConst(binnom_i), RooConst(bincorr_i), RooConst(frac), RooConst(err), *tmpvar, RooConst(binnew_i)));
//    		} else if (type == 0) {
//    		    if (m_response_type == 0) binresponse = new RooFormulaVar(tmp1, tmp2, "((@0 + @1*@2*(1+@3*@4))/(1+@2*(1+@3*@4)))/@5", RooArgSet(RooConst(binnom_i), RooConst(bincorr_i), RooConst(frac), RooConst(err), *tmpvar, RooConst(binnew_i)));
//    		    else if (m_response_type == 1) binresponse = new RooFormulaVar(tmp1, tmp2, "((@0 + @1*@2*pow(1+@3,@4))/(1+@2*pow(1+@3,@4)))/@5", RooArgSet(RooConst(binnom_i), RooConst(bincorr_i), RooConst(frac), RooConst(err), *tmpvar, RooConst(binnew_i)));
//    		}
//
//	        char tmp1[100];
//	        char tmp2[100];
//    	        sprintf(tmp1, "FR%sBin%d%s", m_proc_names->at(channelindex)->at(procindex).c_str(), i+1, m_chan_names->at(channelindex).c_str());
//    	        sprintf(tmp2, "Fraction of %s in Bin %d (%s)", m_proc_titles->at(channelindex)->at(procindex).c_str(), i+1, m_chan_titles->at(channelindex).c_str());
//		m_ProcFractions->at(channelindex)->at(procindex)->at(i) = new RooProduct(tmp1, tmp2, RooArgSet(RooConst(binnew_i), *binresponse));
//    	    }
//    	}
//    } else if (m_wstype == 0) {
//	RooDataHist* procdatahist1 
//    	    = new RooDataHist((string("DataHistNorm") + m_proc_names->at(channelindex)->at(procindex) + m_chan_names->at(channelindex)).c_str(),
//    	    	          (string("Norminal RooDataHist of ") + m_proc_titles->at(channelindex)->at(procindex) + string(" (") + m_chan_titles->at(channelindex) + string(")")).c_str(),
//    	    		  *m_Obs, Import(*procori));
//
//    	RooDataHist* procdatahist2
//    	    = new RooDataHist((string("DataHistCorr") + m_proc_names->at(channelindex)->at(procindex) + m_chan_names->at(channelindex)).c_str(),
//    	    	        (string("Correction RooDataHist of ") + m_proc_titles->at(channelindex)->at(procindex) + string(" (") + m_chan_titles->at(channelindex) + string(")")).c_str(),
//    	    		*m_Obs, Import(*proccorr));
//    	
//    	RooHistPdf* prochistpdf1
//    	    = new RooHistPdf((string("TempNorm") + m_proc_names->at(channelindex)->at(procindex) + m_chan_names->at(channelindex)).c_str(),
//    	    	         (string("Norminal Template of ") + m_proc_titles->at(channelindex)->at(procindex) + string(" (") + m_chan_titles->at(channelindex) + string(")")).c_str(),
//    	    		 *m_Obs, *procdatahist1);
//
//    	RooHistPdf* prochistpdf2
//    	    = new RooHistPdf((string("TempCorr") + m_proc_names->at(channelindex)->at(procindex) + m_chan_names->at(channelindex)).c_str(),
//    	    	         (string("Correction Template of ") + m_proc_titles->at(channelindex)->at(procindex) + string(" (") + m_chan_titles->at(channelindex) + string(")")).c_str(),
//    	    		 *m_Obs, *procdatahist2);
//
//    	RooArgSet tobeAdded;
//    	tobeAdded.add(*prochistpdf1);
//    	tobeAdded.add(*prochistpdf2);
//    	if (fixed) tobeAdded.add(RooConst(1));
//	else tobeAdded.add(*tmpvar);
//    	tobeAdded.add(RooConst(err));
//    	tobeAdded.add(RooConst(frac));
//    	RooGenericPdf *compositepdf = NULL;
//    	if (type == 0) {
//    		compositepdf = new RooGenericPdf((string("TempCorred") + m_proc_names->at(channelindex)->at(procindex) + m_chan_names->at(channelindex)).c_str(),
//    	    	       (string("Corrected Template of ") + m_proc_titles->at(channelindex)->at(procindex) + string(" (") + m_chan_titles->at(channelindex) + string(")")).c_str(),
//    			       "(@0 + (1+@2*@3)*@4 * @1)", tobeAdded);
//    	} else if (type == 1) {
//    		compositepdf = new RooGenericPdf((string("TempCorred") + m_proc_names->at(channelindex)->at(procindex) + m_chan_names->at(channelindex)).c_str(),
//    	    	       (string("Corrected Template of ") + m_proc_titles->at(channelindex)->at(procindex) + string(" (") + m_chan_titles->at(channelindex) + string(")")).c_str(),
//    	    	       "(1 - (1+@2*@3)*@4) * @0 + (1+@2*@3)*@4 * @1", tobeAdded);
//    	} 
//
//    	if (m_ProcDataHists->at(channelindex)->at(procindex)) delete m_ProcDataHists->at(channelindex)->at(procindex);
//    	m_ProcDataHists->at(channelindex)->at(procindex)
//    	    = new RooDataHist((string("DataHist") + m_proc_names->at(channelindex)->at(procindex) + m_chan_names->at(channelindex)).c_str(),
//    	    	          (string("RooDataHist of ") + m_proc_titles->at(channelindex)->at(procindex) + string(" (") + m_chan_titles->at(channelindex) + string(")")).c_str(),
//    	    		  *m_Obs, Import(*procnew));
//
//    	if (m_proc_pdfs->at(channelindex)->at(procindex)) delete m_proc_pdfs->at(channelindex)->at(procindex);
//    	m_proc_pdfs->at(channelindex)->at(procindex) = compositepdf; 
//    }
//}

void WorkspaceMaker::AddSystematics(string name, string title) {
    if (m_paras->find(name.c_str())) return;

    m_lg->Info("ss", "ADD SYSTEMATICS -->", title.c_str());

    float cen, low, high;
//    if (name.find("ShapeSysCorr",0) != string::npos) {
//	cen = 1;
//	low = -4;
//	high = 6;
//    } else if (name.find("ShapeSysHadronFake",0) != string::npos) {
//	cen = 0;
//	low = 0;
//	high = 5;
//    } else {
	cen = 0;
	low = -5;
	high = 5;
//    }
    RooRealVar* tmpvar = new RooRealVar(name.c_str(), title.c_str(), cen, low, high);
    tmpvar->setError(1);
    tmpvar->setBins(20);
//    if (name.find("ShapeSysCorr",0) != string::npos) {
//	tmpvar->setConstant(true);
//	m_paras->add(*tmpvar);
//    } else {
	tmpvar->setConstant(false);
	m_syss->add(*tmpvar);
	m_nuiss->add(*tmpvar);
	m_paras->add(*tmpvar);
//    }

    RooRealVar* tmpobs = new RooRealVar((string("GloObs") + name).c_str(), (string("Global Observable of ") + title).c_str(), cen, low, high);
    tmpobs->setError(1);
    tmpobs->setBins(20);
    tmpobs->setConstant(true);
    m_globs->add(*tmpobs);

    RooGaussian* tmpcst = new RooGaussian((string("Cst") + name).c_str(), (string("Constraint of ") + title).c_str(), *tmpobs, *tmpvar, RooConst(1.0));
    m_sys_csts->add(*tmpcst);
}

//void WorkspaceMaker::AddSystematicsStat(string name, string title, float err) {
//    if (m_syss->find(name.c_str())) return;
//
//    m_lg->Info("ss", "ADD STAT SYSTEMATICS -->", title.c_str());
//
//    RooRealVar* tmpvar = new RooRealVar(name.c_str(), title.c_str(), 0, -1, 5);
//    tmpvar->setError(1);
//    tmpvar->setBins(20);
//    tmpvar->setConstant(false);
//    m_syss->add(*tmpvar);
//    m_nuiss->add(*tmpvar);
//    m_paras->add(*tmpvar);
//
//    RooFormulaVar* tmpformulavar = new RooFormulaVar((string("Para") + name).c_str(), (string("Parametrization of ") + title).c_str(), "(@0+1)*@1", RooArgSet(*tmpvar, RooConst(pow(1./err,2))));
//
//    RooRealVar* tmpobs = new RooRealVar((string("GloObs") + name).c_str(), (string("Global Observable of ") + title).c_str(), pow(1./err,2), 0, 2*pow(1./err,2));
//    tmpobs->setError(1./err);
//    tmpobs->setBins(20);
//    tmpobs->setConstant(true);
//
//    RooPoisson* tmpcst = new RooPoisson((string("Cst") + name).c_str(), (string("Constraint of ") + title).c_str(), *tmpobs, *tmpformulavar);
//    m_sys_csts->add(*tmpcst);
//}

void WorkspaceMaker::AddNormResponse(string sysname, string channelname, string procname, float val) {
    RooRealVar *tmpvar = (RooRealVar*)m_syss->find(sysname.c_str());
    if (!tmpvar) {
	m_lg->Err("ss", "Cannot find systematic -->", sysname.c_str());
	exit(-1);
    }
    string systitle = tmpvar->GetTitle();

    int channelindex; GetChannelIndex(channelname, channelindex);
    int procindex; GetProcIndex(channelname, procname, procindex);

    m_lg->Info("ssssss", "ADD NORM RESPONSE to --> \"", systitle.c_str(), "\" for \"", m_proc_names->at(channelindex)->at(procindex).c_str(), "\" in \"", m_chan_titles->at(channelindex).c_str());

    map<string,string>::iterator map_it;;
    map_it = map_sys_proc.find(sysname);
    if (map_it == map_sys_proc.end()) {
	map_sys_proc.insert(pair<string,string> (sysname, (m_proc_names->at(channelindex)->at(procindex) + string(" in ") + m_chan_titles->at(channelindex))));
    } else {
	string tmpstr = map_sys_proc[sysname];
	string toadd = m_proc_names->at(channelindex)->at(procindex) + string(" in ") + m_chan_titles->at(channelindex);
	if (tmpstr.find(toadd.c_str(), 0) == string::npos) {
	    tmpstr += ",";
	    tmpstr += (m_proc_names->at(channelindex)->at(procindex) + string(" in ") + m_chan_titles->at(channelindex));
	    map_sys_proc[sysname] = tmpstr;
	}
    }

    RooFormulaVar* tmpformula = NULL;
    char tmpname[1000];
    char tmptitle[1000];
    sprintf(tmpname, "Resp%d%s%s%s", m_sys_norm_responses->at(channelindex)->at(procindex)->getSize(), m_proc_names->at(channelindex)->at(procindex).c_str(), m_chan_names->at(channelindex).c_str(), sysname.c_str());
    sprintf(tmptitle, "Respponse%d of %s in %s to %s", m_sys_norm_responses->at(channelindex)->at(procindex)->getSize(), m_proc_titles->at(channelindex)->at(procindex).c_str(), m_chan_titles->at(channelindex).c_str(), systitle.c_str());

    //if (sysname.find("Stat",0) != string::npos) {
    //    tmpformula = new RooFormulaVar((string("Resp") + m_proc_names->at(channelindex)->at(procindex) + m_chan_names->at(channelindex) + sysname).c_str(), 
    //    			       (string("Response of ") + m_proc_titles->at(channelindex)->at(procindex) + string(" in ") + m_chan_titles->at(channelindex)
    //    			        + string(" to ") + systitle).c_str(), 
    //    			       "@0+1", RooArgSet(*tmpvar));
    //} else {
    if (m_response_type == 0) {
        tmpformula = new RooFormulaVar(tmpname, tmptitle, "1 + @0*@1", RooArgSet(RooConst(val), *tmpvar));
    } else if (m_response_type == 1) {
        tmpformula = new RooFormulaVar(tmpname, tmptitle, "pow(1 + @0, @1)", RooArgSet(RooConst(val), *tmpvar));
    } else if (m_response_type == 2) {
        tmpformula = new RooFormulaVar(tmpname, tmptitle, "(1 + @0)**@1", RooArgSet(RooConst(val), *tmpvar));
    }

    m_sys_norm_responses->at(channelindex)->at(procindex)->add(*tmpformula);
}

void WorkspaceMaker::AddNormResponse2(string sysname, string channelname, string procname, float val) {
    RooRealVar *tmpvar = (RooRealVar*)m_syss->find(sysname.c_str());
    if (!tmpvar) {
	m_lg->Err("ss", "Cannot find systematic -->", sysname.c_str());
	exit(-1);
    }
    string systitle = tmpvar->GetTitle();

    int channelindex; GetChannelIndex(channelname, channelindex);
    int procindex; GetProcIndex(channelname, procname, procindex);

    m_lg->Info("ssssss", "ADD NORM RESPONSE TYPE 2 to --> \"", systitle.c_str(), "\" for \"", m_proc_names->at(channelindex)->at(procindex).c_str(), "\" in \"", m_chan_titles->at(channelindex).c_str());

    map<string,string>::iterator map_it;;
    map_it = map_sys_proc.find(sysname);
    if (map_it == map_sys_proc.end()) {
	map_sys_proc.insert(pair<string,string> (sysname, (m_proc_names->at(channelindex)->at(procindex) + string(" in ") + m_chan_titles->at(channelindex))));
    } else {
	string tmpstr = map_sys_proc[sysname];
	string toadd = m_proc_names->at(channelindex)->at(procindex) + string(" in ") + m_chan_titles->at(channelindex);
	if (tmpstr.find(toadd.c_str(), 0) == string::npos) {
	    tmpstr += ",";
	    tmpstr += (m_proc_names->at(channelindex)->at(procindex) + string(" in ") + m_chan_titles->at(channelindex));
	    map_sys_proc[sysname] = tmpstr;
	}
    }

    RooFormulaVar* tmpformula = NULL;
    char tmpname[1000];
    char tmptitle[1000];
    sprintf(tmpname, "Resp%d%s%s%s", m_sys_norm_responses2->at(channelindex)->at(procindex)->getSize(), m_proc_names->at(channelindex)->at(procindex).c_str(), m_chan_names->at(channelindex).c_str(), sysname.c_str());
    sprintf(tmptitle, "Respponse%d of %s in %s to %s", m_sys_norm_responses2->at(channelindex)->at(procindex)->getSize(), m_proc_titles->at(channelindex)->at(procindex).c_str(), m_chan_titles->at(channelindex).c_str(), systitle.c_str());

    //if (sysname.find("Stat",0) != string::npos) {
    //    tmpformula = new RooFormulaVar((string("Resp") + m_proc_names->at(channelindex)->at(procindex) + m_chan_names->at(channelindex) + sysname).c_str(), 
    //    			       (string("Response of ") + m_proc_titles->at(channelindex)->at(procindex) + string(" in ") + m_chan_titles->at(channelindex)
    //    			        + string(" to ") + systitle).c_str(), 
    //    			       "@0+1", RooArgSet(*tmpvar));
    //} else {
    tmpformula = new RooFormulaVar(tmpname, tmptitle, "@0*@1", RooArgSet(RooConst(val), *tmpvar));

    m_sys_norm_responses2->at(channelindex)->at(procindex)->add(*tmpformula);
}

void WorkspaceMaker::AddShapeResponse(string sysname, string channelname, string procname, vector<float> &vals, bool oneway) {
    if (vals.size() != m_obs_nbin) {
	m_lg->Err("s", "Number of responses NOT equal to bin number!");
	exit(-1);
    }

    RooRealVar *tmpvar = (RooRealVar*)m_paras->find(sysname.c_str());
    if (!tmpvar) {
	m_lg->Err("ss", "Cannot find systematic -->", sysname.c_str());
	exit(-1);
    }
    string systitle = tmpvar->GetTitle();

    int channelindex; GetChannelIndex(channelname, channelindex);
    int procindex; GetProcIndex(channelname, procname, procindex);

    m_lg->Info("ssssss", "ADD SHAPE RESPONSE to --> \"", systitle.c_str(), "\" for \"", m_proc_names->at(channelindex)->at(procindex).c_str(), "\" in \"", m_chan_titles->at(channelindex).c_str());

    map<string,string>::iterator map_it;;
    map_it = map_sys_proc.find(sysname);
    if (map_it == map_sys_proc.end()) {
	map_sys_proc.insert(pair<string,string> (sysname, (m_proc_names->at(channelindex)->at(procindex) + string(" in ") + m_chan_titles->at(channelindex))));
    } else {
	string tmpstr = map_sys_proc[sysname];
	string toadd = m_proc_names->at(channelindex)->at(procindex) + string(" in ") + m_chan_titles->at(channelindex);
	if (tmpstr.find(toadd.c_str(), 0) == string::npos) {
	    tmpstr += ",";
	    tmpstr += (m_proc_names->at(channelindex)->at(procindex) + string(" in ") + m_chan_titles->at(channelindex));
	    map_sys_proc[sysname] = tmpstr;
	}
    }

    for (int i = 0; i < m_obs_nbin; i++) {
	RooFormulaVar* tmpformula = NULL;
	char tmpname[1000];
    	char tmptitle[1000];
    	sprintf(tmpname, "ShapeResp%d%sBin%d%s%s", m_sys_shape_responses->at(channelindex)->at(procindex)->at(i)->getSize(), m_proc_names->at(channelindex)->at(procindex).c_str(), i+1, m_chan_names->at(channelindex).c_str(), sysname.c_str());
    	sprintf(tmptitle, "Shape Respponse%d of %s in Bin %d of %s to %s", m_sys_shape_responses->at(channelindex)->at(procindex)->at(i)->getSize(), m_proc_titles->at(channelindex)->at(procindex).c_str(), i+1, m_chan_titles->at(channelindex).c_str(), systitle.c_str());
	if (!oneway) {
	    if (sysname.find("ShapeSysCorr",0) == string::npos) {
	       if (m_response_type == 0) {
	           tmpformula = new RooFormulaVar(tmpname, tmptitle, "1 + @0*@1", RooArgSet(RooConst(vals.at(i)), *tmpvar));
	       } else if (m_response_type == 1) {
	           tmpformula = new RooFormulaVar(tmpname, tmptitle, "pow(1 + @0, @1)", RooArgSet(RooConst(vals.at(i)), *tmpvar));
	       } else if (m_response_type == 2) {
	           tmpformula = new RooFormulaVar(tmpname, tmptitle, "(1 + @0)**@1", RooArgSet(RooConst(vals.at(i)), *tmpvar));
	       }
	    } else {
	        tmpformula = new RooFormulaVar(tmpname, tmptitle, "1 + @0*@1", RooArgSet(RooConst(vals.at(i)), *tmpvar));
	    }
	} else {
	    if (sysname.find("ShapeSysCorr",0) == string::npos) {
	       if (m_response_type == 0) {
	           tmpformula = new RooFormulaVar(tmpname, tmptitle, "(1 + @1/abs(@1))/2 * (1 + @0*@1)", RooArgSet(RooConst(vals.at(i)), *tmpvar));
	       } else if (m_response_type == 1) {
	           tmpformula = new RooFormulaVar(tmpname, tmptitle, "(1 + @1/abs(@1))/2 * pow(1 + @0, @1)", RooArgSet(RooConst(vals.at(i)), *tmpvar));
	       } else if (m_response_type == 2) {
	           tmpformula = new RooFormulaVar(tmpname, tmptitle, "(1 + @1/abs(@1))/2 * (1 + @0)**@1", RooArgSet(RooConst(vals.at(i)), *tmpvar));
	       }
	    } else {
	        tmpformula = new RooFormulaVar(tmpname, tmptitle, "(1 + @1/abs(@1))/2 * (1 + @0*@1)", RooArgSet(RooConst(vals.at(i)), *tmpvar));
	    }
	}

    	m_sys_shape_responses->at(channelindex)->at(procindex)->at(i)->add(*tmpformula);
    }
}

void WorkspaceMaker::Check() {
    if (m_checked) return;

    m_lg->Info("s", "CHECK before making workspace.");

    for (int i = 0; i < m_chan_n; i++) {
	if (!m_datas->at(i)) {
	    m_lg->Err("ss", "Missing data in channel -->", m_chan_titles->at(i).c_str());
	    exit(-1);
	}

	for (int j = 0; j < m_proc_n->at(i); j++) {
	    if (!m_proc_temps->at(i)->at(j)) {
		m_lg->Err("ssss", "Missing template for -->", m_proc_titles->at(i)->at(j).c_str(), "in", m_chan_titles->at(i).c_str());
		exit(-1);
	    }
	    if (!m_proc_paras->at(i)->at(j)) {
		m_lg->Err("ssss", "At LEAST one parameter should be provided for -->", m_proc_titles->at(i)->at(j).c_str(), "in", m_chan_titles->at(i).c_str());
		exit(-1);
	    }
	}
    }

    m_lg->Info("s", "============= Configuration Summary ==============");
    m_lg->Info("s", "------- OBSERVABLE --------");
    m_lg->Info("ss", "Observable -->", m_obs_title.c_str());
    m_lg->Info("sfsfss", "Range --> [", m_obs_low, ",", m_obs_high, "] ", m_obs_unit.c_str());
    m_lg->Info("sd", "Bin number -->", m_obs_nbin);
    m_lg->Info("s", "---------------------------");

    m_lg->Info("s", "------- CHANNEL --------");
    m_lg->Info("sd", "Channel number -->", m_chan_n);
    for (int i = 0; i < m_chan_n; i++) {
	m_lg->Info("sdsssss", "(", (i+1), ")", m_chan_names->at(i).c_str(), "(", m_chan_titles->at(i).c_str(), ")");
    }
    m_lg->Info("s", "------------------------");

    m_lg->Info("s", "--------------- PROCESS ----------------");
    for (int i = 0; i < m_chan_n; i++) {
	m_lg->Info("ss", "In", m_chan_titles->at(i).c_str());
        for (int j = 0; j < m_proc_n->at(i); j++) {
	    m_lg->Info("sdssssss", "(", (j+1), ")", m_proc_names->at(i)->at(j).c_str(), "(", m_proc_titles->at(i)->at(j).c_str(), ")", m_proc_issigs->at(i)->at(j) ? "*" : "");
	}
    }
    m_lg->Info("s", "-------------------------------------------");

    m_lg->Info("s", "--------------- POIs ----------------");
    {
	TIterator* it = m_pois->createIterator();
	RooRealVar *tmpvar;
	int i = 0;
	while ((tmpvar = (RooRealVar*)it->Next())) {
	    if (!tmpvar) continue;
	    i++;
	    m_lg->Info("sdsssss", "(", i, ")", tmpvar->GetName(), "(", tmpvar->getTitle().Data(), ")");
	}
	if (i == 0) {
	    m_lg->Err("s", "No parameter of interest defined!");
	    exit(-1);
	}
    }
    m_lg->Info("s", "-------------------------------------------");


    m_lg->Info("s", "---------------------------- SYSTEMATIC -----------------------------");
    {
	TIter it = m_syss->createIterator();
    	RooRealVar *tmpvar;
    	int syscnt = 0;
    	while ((tmpvar = (RooRealVar*) it.Next())) {
    	    if (!tmpvar) continue;
    	    map<string,string>::iterator map_it;;
    	    map_it = map_sys_proc.find(tmpvar->GetName());
    	    if (map_it == map_sys_proc.end()) {
    	        //m_lg->Warn("s", "Nothing");
    	    } else {
		syscnt++;
		m_lg->Info("sdsss", "(", syscnt, ")", tmpvar->getTitle().Data(), "-->");
    	        vector<string> tmpstrV = SplitToVector(map_sys_proc[tmpvar->GetName()]);
    	        m_lg->SetShiftStep(6);
    	        m_lg->AddShift();
    	        for (int i = 0; i < tmpstrV.size(); i++) {
    	    	m_lg->Info("s", tmpstrV.at(i).c_str());
    	        }
    	        m_lg->ResetShift();
    	    }
    	}
    }
    m_lg->Info("s", "---------------------------------------------------------------------");
    m_lg->NewLine();

    m_checked = true;
}

void WorkspaceMaker::MakeWorkspace() {
    if (!m_checked) {
	m_lg->Err("s", "Please check the inputs before making workspace!");
	exit(-1);
    }

    m_lg->Info("s", "START making workspace.");
    m_lg->NewLine();

    if (m_debug) m_lg->Info("s", "Collect poisson terms");
    RooArgSet toprod;
    for (int i = 0; i < m_chan_n; i++) {
	for (int j = 0; j < m_obs_nbin; j++) {
	    if (j >= m_bin_to_use) continue;
	    AddPoisson(i, j, toprod);
	}
    }

    toprod.add(*m_sys_csts);
    
    if (m_debug) m_lg->Info("s", "Ensemble workspace");
    if (!m_model) delete m_model;
    m_model = new RooProdPdf("Model", "Model", toprod);
    if (m_debug) m_model->Print("v");
    
    m_dataset = new RooDataSet("DataSet", "DataSet", *m_obss);
    m_dataset->add(*m_obss);
    
    m_workspace = new RooWorkspace("Workspace", "Workspace");
    m_workspace->import(*m_model);
    m_workspace->import(*m_dataset);
    m_workspace->import(*m_obs);
    //m_workspace->import(*m_syss);
    //m_workspace->import(*m_paras);

    m_workspace->import(*m_chans);
    m_workspace->import(*m_chants);
    m_workspace->import(*h_proc_issigs);
    m_workspace->import(*h_proc_isfrees);
    for (int i = 0; i < m_chan_n; i++) {
        m_workspace->import(*m_procs->at(i));
        m_workspace->import(*m_procts->at(i));
        m_workspace->import(*m_datas->at(i));
	for (int j = 0; j < m_proc_n->at(i); j++) {
	    m_workspace->import(*m_proc_temps->at(i)->at(j));
	}
    }
    
    m_modelconfig = new ModelConfig(m_workspace);
    m_modelconfig->SetObservables(*m_obss);
    m_modelconfig->SetNameTitle("ModelConfig", "ModelConfig");
    m_modelconfig->SetPdf(*m_model);
    m_modelconfig->SetParametersOfInterest(*m_pois);
    m_modelconfig->SetNuisanceParameters(*m_nuiss);
    m_modelconfig->SetGlobalObservables(*m_globs);
    m_workspace->import(*m_modelconfig);

    m_lg->Warn("ss", "SAVE workspace to -->", m_savename.c_str());
    TFile* f = new TFile(m_savename.c_str(), "recreate");
    m_workspace->Write();

    //if (m_wstype == 0) {
    //    m_lg->Info("s", "SET up data set");
    //    RooDataHist* m_DataSet;
    //	if (m_chan_n == 1) {
    //	  	m_DataSet = new RooDataHist("DataSet","DataSet",*m_Obs,Index(*m_chans), 
    //	    			    Import(m_chan_names->at(0).c_str(),*m_DataHists->at(0)));
    //	} else if (m_chan_n == 2) {
    //	  	m_DataSet  = new RooDataHist("DataSet","DataSet",*m_Obs,Index(*m_chans), 
    //	      		 	       Import(m_chan_names->at(0).c_str(),*m_DataHists->at(0)),
    //	      		 	       Import(m_chan_names->at(1).c_str(),*m_DataHists->at(1)));
    //	} else if (m_chan_n == 3) {
    //	  	m_DataSet = new RooDataHist("DataSet","DataSet",*m_Obs,Index(*m_chans),
    //	      		 	       Import(m_chan_names->at(0).c_str(),*m_DataHists->at(0)),
    //	      		 	       Import(m_chan_names->at(1).c_str(),*m_DataHists->at(1)),
    //	      		 	       Import(m_chan_names->at(2).c_str(),*m_DataHists->at(2)));     		 		
    //	} else if (m_chan_n == 4) {
    //	  	m_DataSet = new RooDataHist("DataSet","DataSet",*m_Obs,Index(*m_chans),
    //	      		 	       Import(m_chan_names->at(0).c_str(),*m_DataHists->at(0)),
    //	      		 	       Import(m_chan_names->at(1).c_str(),*m_DataHists->at(1)),
    //	      		 	       Import(m_chan_names->at(2).c_str(),*m_DataHists->at(2)),
    //	      		 	       Import(m_chan_names->at(3).c_str(),*m_DataHists->at(3)));
    //	}

    //	m_model_NoCst = new RooSimultaneous("ModelNoCst", "Model without Systematic Constraints", *m_chans);
    //	for (int i = 0; i < m_chan_n; i++) {
    //	    RooArgSet allmodels; 
    //	    RooArgSet allnumbers;
    //	    RooArgSet bkgnumbers;

    //	    if (m_TotalNumbers->at(i)) delete m_TotalNumbers->at(i);
    //	    if (m_BkgNumbers->at(i)) delete m_BkgNumbers->at(i);
    //	    if (m_models->at(i)) delete m_models->at(i);
    //	    for (int j = 0; j < m_proc_n->at(i); j++) {
    //	        m_lg->Info("ssss", "SET up model for --> \"", m_proc_titles->at(i)->at(j).c_str(), "\" in", m_chan_titles->at(i).c_str());

    //	        if (m_ProcNumbers->at(i)->at(j)) delete m_ProcNumbers->at(i)->at(j);
    //	        if (m_sys_norm_responses->at(i)->at(j)->getSize() != 0) {
    //	        m_ProcNumbers->at(i)->at(j)
    //	            = new RooProduct((string("FinNum") + m_proc_names->at(i)->at(j) + m_chan_names->at(i)).c_str(), 
    //	    		         (string("Final Number of ") + m_proc_titles->at(i)->at(j) + string(" (") + m_chan_titles->at(i) + string(")")).c_str(), 
    //	    			  RooArgSet(*m_proc_paras->at(i)->at(j),*m_sys_norm_responses->at(i)->at(j)));
    //	        } else {
    //	        m_ProcNumbers->at(i)->at(j)
    //	            = new RooProduct((string("FinNum") + m_proc_names->at(i)->at(j) + m_chan_names->at(i)).c_str(), 
    //	    		         (string("Final Number of ") + m_proc_titles->at(i)->at(j) + string(" (") + m_chan_titles->at(i) + string(")")).c_str(), 
    //	    			  *m_proc_paras->at(i)->at(j));
    //	        }
    //	        allnumbers.add(*m_ProcNumbers->at(i)->at(j));
    //	        if (m_proc_names->at(i)->at(j).find("Signal",0) == string::npos) bkgnumbers.add(*m_ProcNumbers->at(i)->at(j));
    //	        
    //	        m_ProcModels->at(i)->at(j)
    //	            = new RooExtendPdf((string("Model") + m_proc_names->at(i)->at(j) + m_chan_names->at(i)).c_str(),
    //	                               (string("Model of ") + m_proc_titles->at(i)->at(j) + string(" (") + m_chan_titles->at(i) + string(")")).c_str(), 
    //	    			    *m_proc_pdfs->at(i)->at(j), *m_ProcNumbers->at(i)->at(j));

    //	        allmodels.add(*m_ProcModels->at(i)->at(j));
    //	    }

    //	    m_lg->Info("ss", "SET up final model for -->", m_chan_titles->at(i).c_str());
    //	    m_TotalNumbers->at(i) = new RooAddition((string("TotEvtNum") + m_chan_names->at(i)).c_str(),
    //	    					(string("Total Event Number of ") + m_chan_titles->at(i)).c_str(), allnumbers);
    //	    m_BkgNumbers->at(i) = new RooAddition((string("TotBkgNum") + m_chan_names->at(i)).c_str(),
    //	    					(string("Total Background Number of ") + m_chan_titles->at(i)).c_str(), bkgnumbers);
    //	  	m_models->at(i) = new RooAddPdf((string("Model") + m_chan_names->at(i)).c_str(), 
    //	    				(string("Model (") + m_chan_titles->at(i) + string(")")).c_str(), allmodels);
    //	
    //	    m_model_NoCst->addPdf(*m_models->at(i), m_chan_names->at(i).c_str());
    //	}

    //	m_lg->Info("s", "COLLECT systematic constraints");
    //	RooArgSet tmpset;
    //	tmpset.add(*m_SysConstraints);
    //	
    //	m_lg->Info("s", "Set up FINAL model.");
    //	tmpset.add(*m_model_NoCst);
    //	m_model = new RooProdPdf("Model", "Final Model", tmpset);

    //	m_workspace = new RooWorkspace("Workspace", "Workspace");
    //	m_workspace->import(*m_Obs);
    //	m_workspace->import(*m_chans);
    //	m_workspace->import(*m_DataSet);
    //	m_workspace->import(*m_model);
    //    for (int i = 0; i < m_chan_n; i++) {
    //        m_workspace->import(*m_procs->at(i));
    //        m_workspace->import(*m_ProcTs->at(i));
    //        m_workspace->import(*m_binnings->at(i));
    //        m_workspace->import(*m_datas->at(i));
    //    }

    //	RooDataSet* ProtoDS = (RooDataSet*)m_DataHists->at(0)->Clone("ProtoDS");
    //	m_workspace->import(*ProtoDS);
    //	
    //	m_modelconfig = new ModelConfig(m_workspace);
    //	m_modelconfig->SetProtoData(*ProtoDS);
    //	m_modelconfig->SetObservables(RooArgSet(*m_Obs, *m_chans));
    //    m_modelconfig->SetNameTitle("ModelConfig", "Model Config");
    //	m_modelconfig->SetPdf(*m_model);
    //	m_modelconfig->SetParametersOfInterest(*m_POIs);
    //	m_modelconfig->SetNuisanceParameters(*m_Nuis);
    //	if (m_GlobalObss->getSize() > 0) m_modelconfig->SetGlobalObservables(*m_GlobalObss);
    //	m_workspace->import(*m_modelconfig);
    //	RooArgSet* params = (RooArgSet*)m_model->getParameters(*m_DataSet);
    //	m_workspace->saveSnapshot("Snapshot_Initial_Parameters", *params);
    //} else if (m_wstype == 1) 
    
    //for (int i = 0; i < m_chan_n; i++) {
    //    for (int j = 0; j < m_proc_n->at(i); j++) {
    //        m_lg->Info("ssss", "SET up normalization for --> \"", m_proc_titles->at(i)->at(j).c_str(), "\" in", m_chan_titles->at(i).c_str());
    //
    //        if (m_ProcNumbers->at(i)->at(j)) delete m_ProcNumbers->at(i)->at(j);
    //        if (m_sys_norm_responses->at(i)->at(j)->getSize() != 0) {
    //        m_ProcNumbers->at(i)->at(j)
    //            = new RooProduct((string("FinNum") + m_proc_names->at(i)->at(j) + m_chan_names->at(i)).c_str(), 
    //    		         (string("Final Number of ") + m_proc_titles->at(i)->at(j) + string(" (") + m_chan_titles->at(i) + string(")")).c_str(), 
    //    			  RooArgSet(*m_proc_paras->at(i)->at(j),*m_sys_norm_responses->at(i)->at(j)));
    //        } else {
    //        m_ProcNumbers->at(i)->at(j)
    //            = new RooProduct((string("FinNum") + m_proc_names->at(i)->at(j) + m_chan_names->at(i)).c_str(), 
    //    		         (string("Final Number of ") + m_proc_titles->at(i)->at(j) + string(" (") + m_chan_titles->at(i) + string(")")).c_str(), 
    //    			  *m_proc_paras->at(i)->at(j));
    //        }
    //    }
    //}
    //
    //RooArgSet allObs;
    //vector<vector<RooPoisson*> > Poissons;
    //char tmp1[100];
    //char tmp2[100];
    //for (int i = 0; i < m_chan_n; i++) {
    //    vector<RooPoisson*> tmpPoissons;
    //    for (int j = 0; j < m_datas->at(i)->GetNbinsX(); j++) {
    //        m_lg->Info("sdss", "SET up poisson term for --> bin", j+1, "in", m_chan_titles->at(i).c_str());
    //
    //        float n_dataij = m_datas->at(i)->GetBinContent(j+1);
    //        float binlo = m_datas->at(i)->GetBinLowEdge(j+1);
    //        float binhi = m_datas->at(i)->GetBinLowEdge(j+2);
    //
    //        sprintf(tmp1, "ObsBin%d%s", j+1, m_chan_names->at(i).c_str());
    //        sprintf(tmp2, "Observable in Bin %d (%s)", j+1, m_chan_titles->at(i).c_str());
    //        RooRealVar *m_Obsij = new RooRealVar(tmp1, tmp2, 0, 100*n_dataij);
    //        m_Obsij->setVal(n_dataij);
    //        allObs.add(*m_Obsij);
    //
    //        RooArgSet toAdd;
    //        for (int k = 0; k < m_proc_n->at(i); k++) {
    //    	    sprintf(tmp1, "FinNum%sBin%d%s", m_proc_names->at(i)->at(k).c_str(), j+1, m_chan_names->at(i).c_str());
    //    	    sprintf(tmp2, "Final Number of %s in Bin %d (%s)", m_proc_titles->at(i)->at(k).c_str(), j+1, m_chan_titles->at(i).c_str());
    //    	    RooProduct *m_Procijk = new RooProduct(tmp1, tmp2, RooArgSet(*m_ProcNumbers->at(i)->at(k), *m_ProcFractions->at(i)->at(k)->at(j)));
    //
    //    	    toAdd.add(*m_Procijk);
    //        }
    //        sprintf(tmp1, "FinEvtNumBin%d%s", j+1, m_chan_names->at(i).c_str());
    //        sprintf(tmp2, "Final Event Number in Bin %d (%s)", j+1, m_chan_titles->at(i).c_str());
    //        RooAddition *m_Totalij = new RooAddition(tmp1, tmp2, toAdd);
    //
    //        sprintf(tmp1, "ModelBin%d%s", j+1, m_chan_names->at(i).c_str());
    //        sprintf(tmp2, "Model in Bin %d (%s)", j+1, m_chan_titles->at(i).c_str());
    //        RooPoisson *m_Poisij = new RooPoisson(tmp1, tmp2, *m_Obsij, *m_Totalij);
    //        tmpPoissons.push_back(m_Poisij);
    //    }
    //    Poissons.push_back(tmpPoissons);
    //}
    //RooDataSet* m_DataSet = new RooDataSet("DataSet", "DataSet", allObs);
    //m_DataSet->add(allObs);
    //
    //m_lg->Info("s", "Multiply all poisson terms.");
    //RooArgSet toTimes;
    //for (int i = 0; i < Poissons.size(); i++) {
    //    for (int j = 0; j < Poissons.at(i).size(); j++) {
    //        toTimes.add(*Poissons.at(i).at(j));
    //    }
    //}
    //if (m_SysConstraints->getSize() != 0) toTimes.add(*m_SysConstraints);
    //m_model = new RooProdPdf("Model", "Final Model", toTimes);
    //
    //m_workspace = new RooWorkspace("Workspace", "Workspace");
    //m_workspace->import(allObs);
    //m_workspace->import(*m_DataSet);
    //m_workspace->import(*m_model);
    //m_workspace->import(*m_chans);
    //RooRealVar* ObsNbins = new RooRealVar("ObsNbins", "ObsNbins", m_Obs->getBins(), 0, m_Obs->getBins());
    //ObsNbins->setConstant(true);
    //m_workspace->import(*ObsNbins);
    //for (int i = 0; i < m_chan_n; i++) {
    //    m_workspace->import(*m_procs->at(i));
    //    m_workspace->import(*m_ProcTs->at(i));
    //    m_workspace->import(*m_binnings->at(i));
    //    m_workspace->import(*m_datas->at(i));
    //}
    //
    //m_modelconfig = new ModelConfig(m_workspace);
    //m_modelconfig->SetObservables(allObs);
    //m_modelconfig->SetNameTitle("ModelConfig", "Model Config");
    //m_modelconfig->SetPdf(*m_model);
    //m_modelconfig->SetParametersOfInterest(*m_POIs);
    //m_modelconfig->SetNuisanceParameters(*m_Nuis);
    //if (m_GlobalObss->getSize() > 0) m_modelconfig->SetGlobalObservables(*m_GlobalObss);
    //m_workspace->import(*m_modelconfig);
    //RooArgSet* params = (RooArgSet*)m_model->getParameters(*m_DataSet);
    //m_workspace->saveSnapshot("Snapshot_Initial_Parameters", *params);
    //

    //m_lg->Info("ss", "SAVE workspace type  to -->", savename.c_str());
    //TFile* f = new TFile(savename.c_str(), "recreate");
    //m_workspace->Write();
}

void WorkspaceMaker::SetSaveName(string name) {
    m_savename = name;
}

void WorkspaceMaker::AddPoisson(int i, int j, RooArgSet &poissons) {

    if (m_debug) m_lg->Info("s", "AddPoisson --> data number");
    float data = m_datas->at(i)->GetBinContent(j+1);

    char name[1000];
    char title[1000];

    sprintf(name, "ObsBin%d%s", j+1, m_chan_names->at(i).c_str());
    sprintf(title, "Observable in Bin %d of %s", j+1, m_chan_titles->at(i).c_str());
    RooRealVar* obs = new RooRealVar(name, title, 0, 100*data);
    obs->setVal(data);
    m_obss->add(*obs);

    if (m_debug) m_lg->Info("s", "AddPoisson --> MC number");
    RooArgSet tosum;
    for (int k = 0; k < m_proc_n->at(i); k++) {
	sprintf(name, "Num%sBin%d%s", m_proc_names->at(i)->at(k).c_str(), j+1, m_chan_names->at(i).c_str());
	sprintf(title, "Number of %s in Bin %d of %s", m_proc_titles->at(i)->at(k).c_str(), j+1, m_chan_titles->at(i).c_str());

	float frac = m_proc_temps->at(i)->at(k)->GetBinContent(j+1);
	if (m_sys_norm_responses2->at(i)->at(k)->getSize() == 0) {
	    RooArgSet toprod;
	    toprod.add(*m_proc_paras->at(i)->at(k));
	    toprod.add(*m_sys_norm_responses->at(i)->at(k));
	    if (!m_shapesys_off) toprod.add(*m_sys_shape_responses->at(i)->at(k)->at(j));
	    toprod.add(RooConst(frac));
	    RooProduct *proc = new RooProduct(name, title, toprod);
	    tosum.add(*proc);
	    if (m_debug) proc->Print("v");
	} else {
	    char tmpname[1000];
	    char tmptitle[1000];
	    sprintf(tmpname, "Tmp1Num%sBin%d%s", m_proc_names->at(i)->at(k).c_str(), j+1, m_chan_names->at(i).c_str());
	    sprintf(tmptitle, "Tmp1 Number of %s in Bin %d of %s", m_proc_titles->at(i)->at(k).c_str(), j+1, m_chan_titles->at(i).c_str());
	    RooArgSet toadd;
	    toadd.add(*m_proc_paras->at(i)->at(k));
	    toadd.add(*m_sys_norm_responses2->at(i)->at(k));
	    RooAddition *add = new RooAddition(tmpname, tmptitle, toadd);
	    sprintf(tmpname, "Tmp2Num%sBin%d%s", m_proc_names->at(i)->at(k).c_str(), j+1, m_chan_names->at(i).c_str());
	    sprintf(tmptitle, "Tmp2 Number of %s in Bin %d of %s", m_proc_titles->at(i)->at(k).c_str(), j+1, m_chan_titles->at(i).c_str());
	    RooArgSet toprod;
	    toprod.add(*add);
	    if (!m_shapesys_off) toprod.add(*m_sys_shape_responses->at(i)->at(k)->at(j));
	    toprod.add(RooConst(frac));
	    RooProduct *proc = new RooProduct(name, title, toprod);
	    tosum.add(*proc);
	    if (m_debug) proc->Print("v");
	}
    }

    sprintf(name, "TotEvtNumBin%d%s", j+1, m_chan_names->at(i).c_str());
    sprintf(title, "Total Event Number in Bin %d of %s", j+1, m_chan_titles->at(i).c_str());
    RooAddition *total = new RooAddition(name, title, tosum);

    sprintf(name, "PoisBin%d%s", j+1, m_chan_names->at(i).c_str());
    sprintf(title, "Poisson in Bin %d of %s", j+1, m_chan_titles->at(i).c_str());
    RooPoisson *poisson = new RooPoisson(name, title, *obs, *total);
    if (m_debug) total->Print("v");
    if (m_debug) poisson->Print("v");

    poissons.add(*poisson);

}

WorkspaceMaker::~WorkspaceMaker() {
    for (int i = 0; i < m_chan_names->size(); i++) {
	delete m_procs->at(i);
	delete m_procts->at(i);
	delete m_datas->at(i);
	delete m_proc_names->at(i);
	delete m_proc_titles->at(i);
	delete m_proc_issigs->at(i);
	for (int j = 0; j < (int)m_proc_names->at(i)->size(); j++) {
	    delete m_proc_temps->at(i)->at(j);
	    delete m_proc_paras->at(i)->at(j);
	    delete m_sys_norm_responses->at(i)->at(j);
	    delete m_sys_norm_responses2->at(i)->at(j);
	    for (int k = 0; k < m_obs_nbin; k++) {
		delete m_sys_shape_responses->at(i)->at(j)->at(k);
	    }
	}
    }

    delete m_chan_names;
    delete m_chan_titles;
    delete m_proc_n;
    delete h_proc_issigs;
    delete h_proc_isfrees;

    delete m_lg;
    delete m_chans;
    delete m_chants;
    delete m_pois;
    delete m_nuiss;
    delete m_globs;
    delete m_paras;

    delete m_model;
    delete m_dataset;
    delete m_modelconfig;
    delete m_workspace;

    delete m_syss;
    delete m_sys_csts;
}
