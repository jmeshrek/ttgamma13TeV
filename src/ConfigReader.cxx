#include "ConfigReader.h"

using namespace std;

ConfigReader::ConfigReader(string configfile, char tag, bool debug) {
    m_lg = new Logger("Config Reader");
    
    m_file = configfile;
    m_tag = tag;
    m_debug = debug;
};

void ConfigReader::Init(bool debug) {
    //m_lg->AddShift();
    
    if (debug) m_lg->Info("s", "initilization");
    ifstream ifile;
    ifile.open(m_file.c_str());
    if (!ifile.is_open()) {
    	m_lg->Err("s", "Cannot open config file!");
    	exit(-1);
    }	
    
    if (debug) m_lg->Info("s", "reading line by line");
    char tmpline[10000];
    int cnt_line = 0;
    while (ifile.getline(tmpline, 10000, '\n')) {
	cnt_line++;
    	if (tmpline[0] != m_tag.c_str()[0]) continue;
    
    	string tmpkey;
    	vector<string> tmpvals;
	if (debug) m_lg->Info("sd", "counting words in line -->", cnt_line);
    	int valsize = CountWords(tmpline) - 1;
    	tmpvals.resize(valsize);
    
    	string t = tmpline;
    	istringstream iss(t);
    	int cnt = 0;
    	for (string word; iss >> word;) {
	    cnt++;
	    if (cnt == 1) tmpkey = word;
	    else {
	    	tmpvals.at(cnt-2) = word;
	    }
	    if (cnt == valsize + 1) break;
    	}
    
    	m_map.insert(pair<string, vector<string> > (tmpkey, tmpvals) );
    	m_keyset.push_back(tmpkey);
    
    	if (debug) {
	    m_lg->Info("s", tmpkey.c_str());
	    for (int i = 0; i < valsize; i++) {
	    	m_lg->Info("s", tmpvals.at(i).c_str());
	    }
    	}
    }
    
    //m_lg->ReduShift();
    //return 1;
};

ConfigReader::~ConfigReader(){
    delete m_lg;
};

bool ConfigReader::Contain(string key) {
    map<string, vector<string> >::iterator map_it;; 
    map_it = m_map.find(key);
    if (map_it == m_map.end()) {
    	//m_lg->Warn("ss", "Can not find value for", key.c_str());
    	return 0;
    } else {
    	return 1;
    }	
}

int ConfigReader::Size() {
    return m_map.size();
}

string ConfigReader::GetValue(string key, int pos) {
    if (Contain(key)) {
	if (m_map[key].size() == 0) {
	    m_lg->Warn("ss", "Empty val for key -->", key.c_str());
	    return "";
	}
    	string val = m_map[key].at(pos);
    	if (val == "") {
	    m_lg->Warn("ss", "Empty val for key -->", key.c_str());
    	}
    	return val;
    } else return "";
}	

int ConfigReader::GetValueI(string key, int pos) {
    string val = GetValue(key, pos);
    if (val == "") return -1;
    else {
	return atoi(val.c_str());
    }
}

float ConfigReader::GetValueF(string key, int pos) {
    string val = GetValue(key, pos);
    return atof(val.c_str());
}

bool ConfigReader::GetValueB(string key, int pos) {
    string val = GetValue(key, pos);
    if (val == "true" || val == "True" || val == "TRUE")
    	return true;
    else if (val == "false" || val == "False" || val == "FALSE")
    	return false;
    else m_lg->Err("sss", "Undefined key val-->", key.c_str(), val.c_str());
}

vector<string> ConfigReader::GetValueAll(string key) {
    if (Contain(key)) {
    	return m_map[key];
    } else {
    	vector<string> emptyV;
    	emptyV.resize(0);
    	return emptyV;
    }
}

vector<float> ConfigReader::GetValueAllF(string key) {
    if (Contain(key)) {
    	vector<string> tmpval = m_map[key];
    	vector<float> tmpfloat;
    	for (int i = 0; i < tmpval.size(); i++) {
    	    tmpfloat.push_back(atof(tmpval.at(i).c_str()));
    	}
    	return tmpfloat;
    } else {
    	vector<float> emptyV;
    	emptyV.resize(0);
    	return emptyV;
    }
}

vector<string> ConfigReader::GetKeyAll(string ambikey) {
    vector<string> allkeys;
    for (int i = 0; i < m_keyset.size(); i++) {
    	if (m_keyset.at(i).find(ambikey, 0) != string::npos) {
	    allkeys.push_back(m_keyset.at(i));
    	}
    }
    return allkeys;
}

vector<vector<string> > ConfigReader::GetAmbiValueAll(string ambikey) {
    vector<vector<string> > allvals;
    vector<string> allkeys = GetKeyAll(ambikey);
    for (int i = 0; i < allkeys.size(); i++) {
    	vector<string> tmpallvals = GetValueAll(allkeys.at(i));
    	allvals.push_back(tmpallvals);
    }
    return allvals;
}

void ConfigReader::FillValueB(string key, string key2, bool &val) {
    vector<string> tmpV = GetValueAll(key);
    for (int i = 0; i < tmpV.size(); i++) {
	if (tmpV.at(i).find(key2.c_str(),0) != string::npos) {
	    val = true;
	    return;
	}
    }
    val = false;
    return;
}

int ConfigReader::CountWords(const char* str)
{
    if (str == NULL)
       	return -1;
    
    bool inSpaces = true;
    int numWords = 0;
    
    while (*str != NULL)
    {
       	if (std::isspace(*str))
       	{
	    inSpaces = true;
       	}
       	else if (inSpaces)
       	{
	    numWords++;
	    inSpaces = false;
       	}
    
       	++str;
    }

    return numWords;
}
