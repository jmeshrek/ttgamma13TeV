#ifndef SampleAnalyzor_h
#define SampleAnalyzor_h

#include <TROOT.h>
#include <TChain.h>
#include <TKDE.h>
#include <TF1.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <vector>
#include <list>
#include <fstream>
#include <TRandom.h>
#include <TLorentzVector.h>

#include "Logger.h"
//#include "Fitter.h"
//#include "DetectorAtlas_8TeV.h"
//#include "LikelihoodBase.h"
//#include "LikelihoodTopLeptonJets.h"
//#include "LikelihoodTopLeptonLPhotonJets.h"
//#include "LikelihoodTopLeptonHPhotonJets.h"
//#include "LikelihoodTopLeptonLWPhotonJets.h"
//#include "LikelihoodTopLeptonHWPhotonJets.h"
//#include "PhysicsConstants.h"
//#include "Particles.h"
//#include "Permutations.h"
#include "SumWeightTree.h"


using namespace std;

class SampleAnalyzor {

private:

    Logger *m_lg;

    // general control
    bool    m_debug;
    bool    m_test;
    bool    m_1pct;
    bool    m_10pct;
    bool    m_firstevent;
    int	    m_nevt;
    int	    m_nfile;
    TRandom *m_random;
    bool    m_phj_or;

    // egamma calibration
    bool    m_reweight_egamma;
    bool    m_cali_egamma;
    string    m_cali_egamma_type;

    // btag
    double	    weight_btag;
    bool	    m_cont_btag;

    // KLFitter
    bool    m_do_klfitter;
    bool    m_kl_bveto;
    bool    m_kl_fixtop;
    //KLFitter::Fitter *m_klfitter;
    //KLFitter::Fitter *m_klfitter_lt;
    //KLFitter::Fitter *m_klfitter_ht;
    //KLFitter::Fitter *m_klfitter_lW;
    //KLFitter::Fitter *m_klfitter_hW;
    //KLFitter::DetectorBase *m_detector;
    //KLFitter::LikelihoodTopLeptonJets *m_likelihood;
    //KLFitter::LikelihoodTopLeptonLPhotonJets *m_likelihood_lt;
    //KLFitter::LikelihoodTopLeptonHPhotonJets *m_likelihood_ht;
    //KLFitter::LikelihoodTopLeptonLWPhotonJets *m_likelihood_lW;
    //KLFitter::LikelihoodTopLeptonHWPhotonJets *m_likelihood_hW;
    //KLFitter::Particles *m_particles;
    //KLFitter::Particles *m_particles2;
    vector<TLorentzVector*> m_vjet_klfitter;
    TLorentzVector* m_ph_klfitter;
    TLorentzVector* m_el_klfitter;
    TLorentzVector* m_mu_klfitter;

    // database
    string  m_xsectionlist;

    // meta info
    double  m_lumi;
    string  m_cme;
    string  m_simulation;

    // variation
    string m_treename;
    string m_variation;
    vector<string> m_treenames;
    vector<string> m_variations;

    // region
    string  m_region;
    string  m_subregion;

    // sample
    string  m_type;
    string  m_process;
    string  m_filename;
    vector<string>  m_filenames;
    vector<double> m_xsecs;
    vector<string> m_ids;
    vector<double> m_sumws;
    int m_current_idindex;

    // selection
    int	    n_cuts_start;
    int	    n_cuts_end;
    string  m_selection;
    bool    m_ttsg;
    bool    m_notau;
    bool    m_ttdi;
    bool    m_ttee;
    bool    m_ttem;
    bool    m_ttet;
    bool    m_large_eta;

    // truth photon match
    bool    m_do_phmatch;
    bool    m_do_lpmatch;
    string  m_phmatch_type;
    string  m_lpmatch_type;

    // jet match
    bool    m_do_jetmatch;
    bool    m_do_removemu;
    
    // weight
    bool    m_use_weight;
    bool    m_use_lumi_weight;
    double  m_lumi_weight;
    bool    m_use_kfac;
    bool    m_pt_kfac;
    string  m_kfac_file;
    string  m_kfac_hist;
    bool    m_do_bootstrap;
    bool    m_reweight_hfake;
    bool    m_reweight_hfake_025;
    bool    m_reweight_hfake_05;
    bool    m_reweight_hfake_075;
    bool    m_reweight_zgamma;

    // for QCD
    bool    m_PSQCD;
    string  m_QCD_para;
    int	    m_QCD_para_idx;
    int	    n_QCD_para;
    bool    m_QCD_1para;
    
    // egamma fake
    float   m_efake_dr;
    bool    m_use_efake_eta_SF;
    double  m_efake_eta_weight;
    TH1F*   h_efake_eta_weight;

    // histogram
    bool    m_do_hists;
    bool    m_do_hists_sys;
    bool    m_show_of;
    bool    m_do_egammahists;
    bool    m_do_egammatf1;

    // print
    bool    m_notify;
    bool    m_quiet;

    // save
    string  m_saveto;
    string  m_savekey;
    string  m_saveopt;

    // cache
    TFile*  m_file;
    TTree*  m_tree;
    TTree*  m_tree_sumWeight;
    TH1F*   h_kfac;
    TFile*  f_kfac;

    // egamma reweighting
    TH1F* h_egamma_weight;
    TH1F* h_hfake_weight;
    TH1F* h_zgamma_weight;

    // egamma calibration
    TH1F* h_egamma_alpha;

    // cutflow
    TH1F* h_Cutflow;
    vector<TH1F*> h_Cutflow_QCD;
    vector<TH1F*> h_LeadLepPt_QCD;
    vector<TH1F*> h_LeadJetPt_QCD;
    vector<TH1F*> h_LeadPhPt_QCD;
    vector<TH1F*> h_MWT_QCD;
    vector<TH1F*> h_MET_QCD;
    vector<TH1F*> h_dPhiLepMET_QCD;
    vector<double> weights_QCD;
    bool m_cf_inherit;
    int m_ifile;
    vector<double> weights_bootstrap;
    vector<TH1F*> h_bootstrap_LeadPhPt;
    vector<TH1F*> h_bootstrap_LeadPhEta;
    vector<TH1F*> h_bootstrap_LeadPhMinDrPhLep;
    vector<TH1F*> h_bootstrap_DEtaLepLep;
    vector<TH1F*> h_bootstrap_DPhiLepLep;
    
    // histograms
    string  m_histogramlist;
    vector<TH1F*> m_hists;
    string  m_2Dhistogramlist;
    vector<pair<string,string> > m_2Dhists_vars;
    vector<TH2F*> m_2Dhists;
    vector<double> m_egammaptbins_lo;
    vector<double> m_egammaetabins_lo;
    vector<double> m_egammaptbins_hi;
    vector<double> m_egammaetabins_hi;
    vector<double> m_egammapt2dbins_lo;
    vector<double> m_egammaeta2dbins_lo;
    vector<double> m_egammapt2dbins_hi;
    vector<double> m_egammaeta2dbins_hi;
    vector<TH1F*> m_egammahists_pt;
    vector<TH1F*> m_egammahists_eta;
    vector<vector<TH1F*> > m_egammahists_pteta;
    vector<TF1*> m_egammatf1_pt;
    vector<TF1*> m_egammatf1_eta;
    vector<double> m_tkde_data;
    vector<double> m_tkde_data_w;
    vector<vector<vector<double> > >  m_tkde_data_pteta;
    vector<vector<vector<double> > >  m_tkde_data_pteta_w;
    TH1F* h_best_CA;
    TH1F* h_best_lh_best;
    TH1F* h_best_ph_lh_best;
    TH1F* h_best_top_pole_mass;
    TH1F* h_best_reco_hW_mass;
    TH1F* h_best_reco_lW_mass;
    TH1F* h_best_reco_htop_mass;
    TH1F* h_best_reco_htop_pt;
    TH1F* h_best_reco_htop_eta;
    TH1F* h_best_reco_ltop_mass;
    TH1F* h_best_reco_ltop_pt;
    TH1F* h_best_reco_ltop_eta;

    TH1F* h_CA;
    TH1F* h_lh_best;
    TH1F* h_top_pole_mass;
    TH1F* h_reco_hW_mass;
    TH1F* h_reco_lW_mass;
    TH1F* h_reco_htop_mass;
    TH1F* h_reco_htop_pt;
    TH1F* h_reco_htop_eta;
    TH1F* h_reco_ltop_mass;
    TH1F* h_reco_ltop_pt;
    TH1F* h_reco_ltop_eta;

    TH1F* h_lt_CA;
    TH1F* h_lt_lh_best;
    TH1F* h_lt_top_pole_mass;
    TH1F* h_lt_reco_hW_mass;
    TH1F* h_lt_reco_lW_mass;
    TH1F* h_lt_reco_htop_mass;
    TH1F* h_lt_reco_htop_pt;
    TH1F* h_lt_reco_htop_eta;
    TH1F* h_lt_reco_ltop_mass;
    TH1F* h_lt_reco_ltop_pt;
    TH1F* h_lt_reco_ltop_eta;

    TH1F* h_ht_CA;
    TH1F* h_ht_lh_best;
    TH1F* h_ht_top_pole_mass;
    TH1F* h_ht_reco_hW_mass;
    TH1F* h_ht_reco_lW_mass;
    TH1F* h_ht_reco_htop_mass;
    TH1F* h_ht_reco_htop_pt;
    TH1F* h_ht_reco_htop_eta;
    TH1F* h_ht_reco_ltop_mass;
    TH1F* h_ht_reco_ltop_pt;
    TH1F* h_ht_reco_ltop_eta;

    TH1F* h_lW_CA;
    TH1F* h_lW_lh_best;
    TH1F* h_lW_top_pole_mass;
    TH1F* h_lW_reco_hW_mass;
    TH1F* h_lW_reco_lW_mass;
    TH1F* h_lW_reco_htop_mass;
    TH1F* h_lW_reco_htop_pt;
    TH1F* h_lW_reco_htop_eta;
    TH1F* h_lW_reco_ltop_mass;
    TH1F* h_lW_reco_ltop_pt;
    TH1F* h_lW_reco_ltop_eta;

    TH1F* h_hW_CA;
    TH1F* h_hW_lh_best;
    TH1F* h_hW_top_pole_mass;
    TH1F* h_hW_reco_hW_mass;
    TH1F* h_hW_reco_lW_mass;
    TH1F* h_hW_reco_htop_mass;
    TH1F* h_hW_reco_htop_pt;
    TH1F* h_hW_reco_htop_eta;
    TH1F* h_hW_reco_ltop_mass;
    TH1F* h_hW_reco_ltop_pt;
    TH1F* h_hW_reco_ltop_eta;

    TH2F* h2_CA;
    TH2F* h2_lh_best;
    TH2F* h2_top_pole_mass;
    TH2F* h2_reco_hW_mass;
    TH2F* h2_reco_lW_mass;
    TH2F* h2_reco_htop_mass;
    TH2F* h2_reco_htop_pt;
    TH2F* h2_reco_htop_eta;
    TH2F* h2_reco_ltop_mass;
    TH2F* h2_reco_ltop_pt;
    TH2F* h2_reco_ltop_eta;

    TH2F* h2_lh_ph_lh_best;

    // constants
    double m_zmass;
    double m_cut_ph_pt;
    double m_cut_zveto;
    double m_cut_drphj;
    double m_cut_drphl;
    
    // internal variables;
    vector<TLorentzVector> tlv_leps;
    vector<TLorentzVector> tlv_phs;
    TLorentzVector tlv_leadlep; 
    TLorentzVector tlv_sublep; 
    TLorentzVector tlv_leadph; 
    TLorentzVector tlv_leadph_mc;
    TLorentzVector tlv_leadph_mc_el;
    TLorentzVector tlv_leadlep_mc;
    TLorentzVector tlv_sublep_mc;
    vector<TLorentzVector> tlv_jets;

    // event level
    long evtnum;

    // jet 
    int n_jet;
    int n_bjet;
    int n_bjet77;
    int n_bjet70;
    double leadjet_pt;
    double leadjet_phi;
    double leadjet_eta;
    double leadjet_asm;
    double subjet_pt;
    double subjet_eta;
    double drleadjet_leadph;
    double drleadjet_leadlep;
    double m_12jet;
    double m_123jet;

    // photon
    int n_ph;
    int n_ph_good;
    int n_ph_iso;
    int n_ph_noid_noiso;
    int n_ph_hfake;
    vector<int> n_ph_good_index;
    vector<int> n_ph_iso_index;
    vector<int> n_ph_noid_noiso_index;
    vector<int> n_ph_hfake_index;
    double leadph_pt;
    int leadph_cvt;
    int leadph_bincvt;
    double leadph_HFT_MVA;
    double leadph_mc_pt;
    double leadph_ptreco_pttrue;
    double leadph_drrecotrue;
    double leadph_mc_eta;
    double leadph_mc_phi;
    double leadph_mc_e;
    double leadph_mc_dr;
    int leadph_faketype;
    int leadph_mc_pid;
    int leadph_mc_barcode;
    double leadph_mc_el_pt;
    double leadph_mc_el_eta;
    double leadph_mc_el_phi;
    double leadph_mc_el_dr;
    double leadph_eta;
    double leadph_phi;
    double leadph_m;
    double leadph_e;
    double leadph_ptcone;
    double leadph_etcone;
    char leadph_tight;
    double leadphleadlep_dphi;
    double leadph_mindrphl;
    double detaleplep;
    double dphileplep;
    double leadph_mindrphj;
    double leadph_ancestor;
    double leadph_origin;
    double leadph_type;
    float  leadph_rhad1;
    float  leadph_rhad;
    float  leadph_reta;
    float  leadph_weta2;
    float  leadph_rphi;
    float  leadph_ws3;
    float  leadph_wstot;
    float  leadph_fracm;
    float  leadph_deltaE;
    float  leadph_eratio;
    float  leadph_emaxs1;
    float  leadph_f1;
    float  leadph_e277;

    // lepton
    int n_lep;
    int n_el;
    vector<int> n_el_good_index;
    int n_mu;
    vector<int> n_mu_good_index;
    double leadlep_pt;
    double leadel_pt;
    double leadmu_pt;
    double leadlep_eta;
    double leadlep_phi;
    double leadlep_m;
    double leadlep_e;
    double leadlep_mc_pt;
    double leadlep_mc_eta;
    double leadlep_mc_phi;
    double leadlep_mc_e;
    double leadlep_mc_dr;
    double leadlepsublep_dphi;
    double leadlepsublep_deta;
    char leadlep_HLT_e24_lhmedium_L1EM20VH;
    char leadlep_HLT_e60_lhmedium;
    char leadlep_HLT_e120_lhloose;
    char leadlep_HLT_e26_lhtight_nod0_ivarloose;
    char leadlep_HLT_e60_lhmedium_nod0;
    char leadlep_HLT_e140_lhloose_nod0;
    double leadlep_origin;
    double leadlep_mindrphl;
    double leadlep_mindrphj;
    double leadlep_type;
    double sublep_pt;
    double sublep_eta;
    double sublep_phi;
    double sublep_m;
    double sublep_e;
    double sublep_mc_pt;
    double sublep_mc_eta;
    double sublep_mc_phi;
    double sublep_mc_e;
    double sublep_mc_dr;
    double sublep_ptcone;
    double sublep_etcone;
    double sublepleadlep_dphi;
    double sublep_mindrphl;
    double sublep_mindrphj;
    double sublep_origin;
    double sublep_type;
    char sublep_HLT_e24_lhmedium_L1EM20VH;
    char sublep_HLT_e60_lhmedium;
    char sublep_HLT_e120_lhloose;
    char sublep_HLT_e26_lhtight_nod0_ivarloose;
    char sublep_HLT_e60_lhmedium_nod0;
    char sublep_HLT_e140_lhloose_nod0;
    double drleplep;

    // met
    double met;
    double met_x;
    double met_y;

    // W mass
    double mwt;
    double mwt2;
    double dphilepmet;
    double dphiphmet;
    double mt_lphmet;

    // invariant mass
    double mphl;
    double mljet;
    double mmcphl;
    double mmcelphl;
    double mmcll;
    double mlmcl;
    double mmclmcl;
    double mll;
    double mllg;

    // KLFitter
    double kl_best_CA;
    double kl_best_CALep;
    double kl_best_lh_best;
    double kl_best_ph_lh_best;
    double kl_best_top_pole_mass;
    double kl_best_reco_hW_mass;
    double kl_best_reco_lW_mass;
    double kl_best_reco_htop_mass;
    double kl_best_reco_htop_pt;
    double kl_best_reco_htop_eta;
    double kl_best_reco_ltop_mass;
    double kl_best_reco_ltop_pt;
    double kl_best_reco_ltop_eta;

    double kl_CA;
    double kl_CALep;
    double kl_lh_best;
    double kl_top_pole_mass;
    double kl_reco_hW_mass;
    double kl_reco_lW_mass;
    double kl_reco_htop_mass;
    double kl_reco_htop_pt;
    double kl_reco_htop_eta;
    double kl_reco_ltop_mass;
    double kl_reco_ltop_pt;
    double kl_reco_ltop_eta;

    double kl_lt_CA;
    double kl_lt_CALep;
    double kl_lt_lh_best;
    double kl_lt_top_pole_mass;
    double kl_lt_reco_hW_mass;
    double kl_lt_reco_lW_mass;
    double kl_lt_reco_htop_mass;
    double kl_lt_reco_htop_pt;
    double kl_lt_reco_htop_eta;
    double kl_lt_reco_ltop_mass;
    double kl_lt_reco_ltop_pt;
    double kl_lt_reco_ltop_eta;

    double kl_ht_CA;
    double kl_ht_CALep;
    double kl_ht_lh_best;
    double kl_ht_top_pole_mass;
    double kl_ht_reco_hW_mass;
    double kl_ht_reco_lW_mass;
    double kl_ht_reco_htop_mass;
    double kl_ht_reco_htop_pt;
    double kl_ht_reco_htop_eta;
    double kl_ht_reco_ltop_mass;
    double kl_ht_reco_ltop_pt;
    double kl_ht_reco_ltop_eta;

    double kl_lW_CA;
    double kl_lW_CALep;
    double kl_lW_lh_best;
    double kl_lW_top_pole_mass;
    double kl_lW_reco_hW_mass;
    double kl_lW_reco_lW_mass;
    double kl_lW_reco_htop_mass;
    double kl_lW_reco_htop_pt;
    double kl_lW_reco_htop_eta;
    double kl_lW_reco_ltop_mass;
    double kl_lW_reco_ltop_pt;
    double kl_lW_reco_ltop_eta;

    double kl_hW_CA;
    double kl_hW_CALep;
    double kl_hW_lh_best;
    double kl_hW_top_pole_mass;
    double kl_hW_reco_hW_mass;
    double kl_hW_reco_lW_mass;
    double kl_hW_reco_htop_mass;
    double kl_hW_reco_htop_pt;
    double kl_hW_reco_htop_eta;
    double kl_hW_reco_ltop_mass;
    double kl_hW_reco_ltop_pt;
    double kl_hW_reco_ltop_eta;

    // weight
    double weight;
    //vector<double> totalmc;
    //vector<double> xsection;
    //vector<double> filter;
    //vector<double> kfactor;
    //vector<int> mcid;

    // sum Weights
    Float_t         m_totalEventsWeighted;
    vector<Float_t>         *m_totalEventsWeighted_mc_generator_weights;

    // reco branch (13TeV)
    vector<float>   *mc_generator_weights;
    Float_t         weight_mc;
    vector<int>     *weight_poisson;
    Float_t         weight_pileup;
    Float_t         weight_leptonSF;
    Float_t         weight_photonSF;
    Float_t         weight_bTagSF_77;
    Float_t         weight_bTagSF_Continuous;
    Float_t         weight_jvt;
    Float_t         weight_sherpa_22_vjets;
    Float_t         weight_pileup_UP;
    Float_t         weight_pileup_DOWN;
    Float_t         weight_leptonSF_EL_SF_Trigger_UP;
    Float_t         weight_leptonSF_EL_SF_Trigger_DOWN;
    Float_t         weight_leptonSF_EL_SF_Reco_UP;
    Float_t         weight_leptonSF_EL_SF_Reco_DOWN;
    Float_t         weight_leptonSF_EL_SF_ID_UP;
    Float_t         weight_leptonSF_EL_SF_ID_DOWN;
    Float_t         weight_leptonSF_EL_SF_Isol_UP;
    Float_t         weight_leptonSF_EL_SF_Isol_DOWN;
    Float_t         weight_leptonSF_MU_SF_Trigger_STAT_UP;
    Float_t         weight_leptonSF_MU_SF_Trigger_STAT_DOWN;
    Float_t         weight_leptonSF_MU_SF_Trigger_SYST_UP;
    Float_t         weight_leptonSF_MU_SF_Trigger_SYST_DOWN;
    Float_t         weight_leptonSF_MU_SF_ID_STAT_UP;
    Float_t         weight_leptonSF_MU_SF_ID_STAT_DOWN;
    Float_t         weight_leptonSF_MU_SF_ID_SYST_UP;
    Float_t         weight_leptonSF_MU_SF_ID_SYST_DOWN;
    Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;
    Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;
    Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;
    Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;
    Float_t         weight_leptonSF_MU_SF_Isol_STAT_UP;
    Float_t         weight_leptonSF_MU_SF_Isol_STAT_DOWN;
    Float_t         weight_leptonSF_MU_SF_Isol_SYST_UP;
    Float_t         weight_leptonSF_MU_SF_Isol_SYST_DOWN;
    Float_t         weight_leptonSF_MU_SF_TTVA_STAT_UP;
    Float_t         weight_leptonSF_MU_SF_TTVA_STAT_DOWN;
    Float_t         weight_leptonSF_MU_SF_TTVA_SYST_UP;
    Float_t         weight_leptonSF_MU_SF_TTVA_SYST_DOWN;
    Float_t         weight_indiv_SF_EL_Trigger;
    Float_t         weight_indiv_SF_EL_Trigger_UP;
    Float_t         weight_indiv_SF_EL_Trigger_DOWN;
    Float_t         weight_indiv_SF_EL_Reco;
    Float_t         weight_indiv_SF_EL_Reco_UP;
    Float_t         weight_indiv_SF_EL_Reco_DOWN;
    Float_t         weight_indiv_SF_EL_ID;
    Float_t         weight_indiv_SF_EL_ID_UP;
    Float_t         weight_indiv_SF_EL_ID_DOWN;
    Float_t         weight_indiv_SF_EL_Isol;
    Float_t         weight_indiv_SF_EL_Isol_UP;
    Float_t         weight_indiv_SF_EL_Isol_DOWN;
    Float_t         weight_indiv_SF_MU_Trigger;
    Float_t         weight_indiv_SF_MU_Trigger_STAT_UP;
    Float_t         weight_indiv_SF_MU_Trigger_STAT_DOWN;
    Float_t         weight_indiv_SF_MU_Trigger_SYST_UP;
    Float_t         weight_indiv_SF_MU_Trigger_SYST_DOWN;
    Float_t         weight_indiv_SF_MU_ID;
    Float_t         weight_indiv_SF_MU_ID_STAT_UP;
    Float_t         weight_indiv_SF_MU_ID_STAT_DOWN;
    Float_t         weight_indiv_SF_MU_ID_SYST_UP;
    Float_t         weight_indiv_SF_MU_ID_SYST_DOWN;
    Float_t         weight_indiv_SF_MU_ID_STAT_LOWPT_UP;
    Float_t         weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN;
    Float_t         weight_indiv_SF_MU_ID_SYST_LOWPT_UP;
    Float_t         weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN;
    Float_t         weight_indiv_SF_MU_Isol;
    Float_t         weight_indiv_SF_MU_Isol_STAT_UP;
    Float_t         weight_indiv_SF_MU_Isol_STAT_DOWN;
    Float_t         weight_indiv_SF_MU_Isol_SYST_UP;
    Float_t         weight_indiv_SF_MU_Isol_SYST_DOWN;
    Float_t         weight_indiv_SF_MU_TTVA;
    Float_t         weight_indiv_SF_MU_TTVA_STAT_UP;
    Float_t         weight_indiv_SF_MU_TTVA_STAT_DOWN;
    Float_t         weight_indiv_SF_MU_TTVA_SYST_UP;
    Float_t         weight_indiv_SF_MU_TTVA_SYST_DOWN;
    Float_t         weight_photonSF_ID_UP;
    Float_t         weight_photonSF_ID_DOWN;
    Float_t         weight_jvt_UP;
    Float_t         weight_jvt_DOWN;
    vector<float>   *weight_bTagSF_77_eigenvars_B_up;
    vector<float>   *weight_bTagSF_77_eigenvars_C_up;
    vector<float>   *weight_bTagSF_77_eigenvars_Light_up;
    vector<float>   *weight_bTagSF_77_eigenvars_B_down;
    vector<float>   *weight_bTagSF_77_eigenvars_C_down;
    vector<float>   *weight_bTagSF_77_eigenvars_Light_down;
    Float_t         weight_bTagSF_77_extrapolation_up;
    Float_t         weight_bTagSF_77_extrapolation_down;
    Float_t         weight_bTagSF_77_extrapolation_from_charm_up;
    Float_t         weight_bTagSF_77_extrapolation_from_charm_down;
    vector<float>   *weight_bTagSF_Continuous_eigenvars_B_up;
    vector<float>   *weight_bTagSF_Continuous_eigenvars_C_up;
    vector<float>   *weight_bTagSF_Continuous_eigenvars_Light_up;
    vector<float>   *weight_bTagSF_Continuous_eigenvars_B_down;
    vector<float>   *weight_bTagSF_Continuous_eigenvars_C_down;
    vector<float>   *weight_bTagSF_Continuous_eigenvars_Light_down;
    Float_t         weight_bTagSF_Continuous_extrapolation_from_charm_up;
    Float_t         weight_bTagSF_Continuous_extrapolation_from_charm_down;
    ULong64_t       eventNumber;
    UInt_t          runNumber;
    int		    m_period;
    UInt_t          randomRunNumber;
    UInt_t          mcChannelNumber;
    Float_t         mu;
    UInt_t          backgroundFlags;
    vector<float>   *el_pt;
    vector<float>   *el_eta;
    vector<float>   *el_cl_eta;
    vector<float>   *el_phi;
    vector<float>   *el_e;
    vector<float>   *el_charge;
    vector<float>   *el_faketype;
    vector<float>   *el_topoetcone20;
    vector<float>   *el_ptvarcone20;
    vector<float>   *el_d0sig;
    vector<float>   *el_delta_z0_sintheta;
    vector<int>     *el_true_type;
    vector<int>     *el_true_origin;
    vector<int>     *el_true_typebkg;
    vector<int>     *el_true_originbkg;
    vector<int>     *el_truthAncestor;
    vector<int>     *el_mc_pid;
    vector<float>   *el_mc_pt;
    vector<float>   *el_mc_eta;
    vector<float>   *el_mc_phi;
    vector<float>   *mu_pt;
    vector<float>   *mu_eta;
    vector<float>   *mu_phi;
    vector<float>   *mu_e;
    vector<float>   *mu_charge;
    vector<float>   *mu_faketype;
    vector<float>   *mu_topoetcone20;
    vector<float>   *mu_ptvarcone30;
    vector<float>   *mu_d0sig;
    vector<float>   *mu_delta_z0_sintheta;
    vector<int>     *mu_true_type;
    vector<int>     *mu_true_origin;
    vector<char>     *mu_true_isPrompt;
    vector<float>   *ph_pt;
    vector<float>   *ph_eta;
    vector<float>   *ph_phi;
    vector<float>   *ph_e;
    vector<float>   *ph_iso;
    vector<float>   *jet_pt;
    vector<float>   *jet_eta;
    vector<float>   *jet_phi;
    vector<float>   *jet_e;
    vector<float>   *jet_mv1eff;
    vector<float>   *jet_isPileup;
    vector<int>   *jet_nGhosts_cHadron;
    vector<int>   *jet_nGhosts_bHadron;
    vector<float>   *ljet_pt;
    vector<float>   *ljet_eta;
    vector<float>   *ljet_phi;
    vector<float>   *ljet_e;
    vector<float>   *ljet_nGhosts_cHadron;
    vector<float>   *ljet_nGhosts_bHadron;
    vector<float>   *jet_mv2c00;
    vector<float>   *jet_mv2c10;
    vector<float>   *jet_mv2c20;
    vector<float>   *jet_ip3dsv1;
    vector<float>   *jet_jvt;
    vector<int>     *jet_truthflav;
    vector<int>    *jet_isbtagged_77;
    Float_t         event_mwt;
    Float_t         event_ELD_MVA_correct;
    Float_t         met_met;
    Float_t         met_phi;
    bool	    m_is2015;
    Int_t           ejets_2015;
    Int_t           zee_2015;
    Int_t           zee_2016;
    Int_t           zeg_2015;
    Int_t           zeg_2016;
    Int_t           ttel_ee_2015;
    Int_t           ttel_ee_2016;
    Int_t           ttel_mue_2015;
    Int_t           ttel_mue_2016;
    Int_t           ejets_gamma_basic;
    Int_t           mujets_gamma_basic;
    Int_t           ee_gamma_basic;
    Int_t           emu_gamma_basic;
    Int_t           mumu_gamma_basic;
    Int_t           mujets_2015;
    Int_t           ejets_2016;
    Int_t           mujets_2016;
    Int_t           ee_2015;
    Int_t           mumu_2015;
    Int_t           emu_2015;
    Int_t           ee_2016;
    Int_t           mumu_2016;
    Int_t           emu_2016;
    Int_t           eeSS_2015;
    Int_t           mumuSS_2015;
    Int_t           emuSS_2015;
    Int_t           eeSS_2016;
    Int_t           mumuSS_2016;
    Int_t           emuSS_2016;
    Char_t          HLT_mu26_ivarmedium;
    Char_t          HLT_mu20_iloose_L1MU15;
    Char_t          HLT_e60_lhmedium_nod0;
    Char_t          HLT_mu50;
    Char_t          HLT_e26_lhvloose_nod0_L1EM20VH_6j15noL1;
    Char_t          HLT_e24_lhmedium_L1EM20VH;
    Char_t          HLT_e140_lhloose_nod0;
    Char_t          HLT_e60_lhmedium;
    Char_t          HLT_e26_lhtight_nod0_ivarloose;
    Char_t          HLT_e120_lhloose;
    Char_t          HLT_e26_lhvloose_nod0_L1EM20VH_5j15noL1;
    Char_t          HLT_e26_lhvloose_nod0_L1EM20VH_4j20noL1;
    Char_t          HLT_e24_lhmedium_nod0_L1EM18VH;
    Char_t          HLT_e26_lhvloose_nod0_L1EM20VH;
    Char_t          HLT_mu24;
    Char_t          HLT_e26_lhvloose_nod0_L1EM20VH_3j20noL1;
    vector<char>    *el_trigMatch_HLT_e60_lhmedium_nod0;
    vector<char>    *el_trigMatch_HLT_e26_lhtight_nod0_ivarloose;
    vector<char>    *el_trigMatch_HLT_e140_lhloose_nod0;
    vector<char>    *el_trigMatch_HLT_e60_lhmedium;
    vector<char>    *el_trigMatch_HLT_e24_lhmedium_L1EM20VH;
    vector<char>    *el_trigMatch_HLT_e120_lhloose;
    vector<char>    *mu_trigMatch_HLT_mu26_ivarmedium;
    vector<char>    *mu_trigMatch_HLT_mu50;
    vector<char>    *mu_trigMatch_HLT_mu20_iloose_L1MU15;
    Int_t           selph_index1;
    vector<float>   *ph_kfactor_correct;
    //vector<float>   *efake_sf_Nominal;
    vector<float>   *ph_SF_eff;
    vector<float>   *ph_SF_effUP;
    vector<float>   *ph_SF_effDO;
    vector<float>   *ph_SF_iso;
    vector<float>   *ph_SF_lowisoUP;
    vector<float>   *ph_SF_lowisoDO;
    vector<float>   *ph_SF_trkisoUP;
    vector<float>   *ph_SF_trkisoDO;
    vector<float>   *ph_topoetcone20;
    vector<float>   *ph_topoetcone30;
    vector<float>   *ph_topoetcone40;
    vector<float>   *ph_ptcone20;
    vector<float>   *ph_ptcone30;
    vector<float>   *ph_faketype;
    vector<float>   *ph_mc_Type;
    vector<float>   *ph_mc_Origin;
    vector<float>   *ph_ptcone40;
    vector<float>   *ph_ptvarcone20;
    vector<float>   *ph_ptvarcone30;
    vector<float>   *ph_ptvarcone40;
    vector<char>    *ph_isoFCTCO;
    vector<char>    *ph_isoFCT;
    vector<char>    *ph_isoFCL;
    vector<char>    *ph_isTight;
    vector<char>    *ph_isLoose;
    vector<char>    *ph_isHadronFakeFailedDeltaE;
    vector<char>    *ph_isHadronFakeFailedFside;
    vector<char>    *ph_isHadronFakeFailedWs3;
    vector<char>    *ph_isHadronFakeFailedERatio;
    vector<int>     *ph_truthType;
    vector<int>     *ph_truthOrigin;
    vector<int>     *ph_truthAncestor;
    vector<int>     *ph_mc_pid;
    vector<float>   *ph_HFT_MVA;
    vector<float>   *ph_mc_pt;
    vector<float>   *ph_mc_eta;
    vector<float>   *ph_mc_phi;
    vector<int>   *ph_mc_barcode;
    vector<float>   *ph_rhad1;
    vector<float>   *ph_rhad;
    vector<float>   *ph_reta;
    vector<float>   *ph_weta2;
    vector<float>   *ph_rphi;
    vector<float>   *ph_ws3;
    vector<float>   *ph_wstot;
    vector<float>   *ph_fracm;
    vector<float>   *ph_deltaE;
    vector<float>   *ph_eratio;
    vector<float>   *ph_emaxs1;
    vector<float>   *ph_f1;
    vector<float>   *ph_e277;
    vector<unsigned int> *ph_OQ;
    vector<unsigned int> *ph_author;
    vector<int>     *ph_conversionType;
    vector<float>   *ph_caloEta;
    vector<float>   *ph_dralljet;
    vector<float>   *ph_drlept;
    vector<float>   *ph_mgammalept;
    vector<float>   *ph_mgammaleptlept;
    vector<float>   *ph_mcel_dr;
    vector<float>   *ph_mcel_pt;
    vector<float>   *ph_mcel_eta;
    vector<float>   *ph_mcel_phi;
    vector<int>     *lepton_type;
    vector<char>    *el_isoGradient;
    Float_t         event_HT;
    Float_t         event_mll;
    vector<float>   *mcph_pt;
    vector<float>   *mcph_eta;
    vector<float>   *mcph_phi;
    vector<int>     *mcph_ancestor;
    Int_t           event_njets;
    Int_t           event_nbjets77;
    Int_t           event_nbjets70;
    Int_t           event_ngoodphotons;
    Int_t           event_photonorigin;
    Int_t           event_photonoriginTA;
    Double_t        event_norm;
    Double_t        event_norm2;
    Double_t        event_lumi;
    Float_t         weight_mm_ejets;
    Float_t         weight_mm_mujets;
    vector<float> *weights_mm_ejets;
    vector<float> *weights_mm_mujets;

public :

    SampleAnalyzor();
    ~SampleAnalyzor();

    // general control
    void TestMode(bool test);
    void RunOnePct(bool pct);
    void RunTenPct(bool pct);
    void Debug(bool debug);
    void RunNEvt(int nevt);
    void RunNFile(int nfile);

    // flag
    void ReweightEGamma(bool reweight);
    void ReweightHFake(bool reweight);
    void ReweightHFake025(bool reweight025);
    void ReweightHFake05(bool reweight05);
    void ReweightHFake075(bool reweight075);
    void ReweightZGamma(bool reweight);
    void CaliEGamma(bool cali, string type);

    // database
    void UseXsectionList(string xsection);
    void UseHistogramList(string histogramlist, string histogramlist_2D);

    // meta info
    void SetCME(string cme);
    void SetSimulation(string simulation);

    // variation
    void AddTreeName(string treename);
    void AddVariation(string variation);

    // region
    void SetRegion(string region);
    void SetSubRegion(string subregion);

    // sample
    void SetProcess(string process);
    void SetType(string type);
    void AddFile(vector<string> filename);

    // selection
    void Selection(string selection);
    void SelectTTBarSG(bool sg);
    void SelectTTBarNoTau(bool notau);
    void SelectTTBarDI(bool di, bool ee, bool em, bool et);

    // truth photon match
    void DoPhMatch(bool match, string type = "");
    void DoLpMatch(bool match, string type = "");

    // jet match
    void DoJetMatch(bool match);
    void DoRemoveMu(bool remove);

    // weight 
    void UseEvtWeight(bool use);
    void UseLumiWeight(bool use);
    void UseKfactor(bool use);
    void UseKfactor(bool use, string file, string hist);
    void DoBootStrap(bool use);

    // for QCD
    void UsePSTrigger(bool PS);
    void SetQCDPara(string para);

    // histogram
    void DoHist(bool fill, bool fillsys = false);
    void DoEGammaHist(bool fill);
    void DoEGammaTF1(bool fill);
    void ShowOverflow(bool of);

    // print
    void Notify(bool notify);
    void Quiet(bool quiet);

    // save
    void SaveTo(string saveto);
    void SaveKey(string savekey);
    void SaveOption(string opt);

    // egamma fake
    void UseEFakeEtaSF(bool etasf);
    void DoEFakeDr02(bool dr02);
    void DoEFakeDr05(bool dr05);

    // btag
    void NoContBtag(bool nocont);

    // internal function
    void Check();
    int InitFile(bool oneevt = false);
    void InitHistogram();
    void InitEvent();
    void Loop(bool oneevt = false);
    void SaveResults();
    void ReleaseMemory();
    void ReleaseEventMemory();
    void Clear();
    bool isHadron(int pdg);
    bool isBHadron(int id);
    void FillCutflow(int cnt, string cutname = "");
    bool PhMatch();
    bool JetMatch();
    void KLFit();
    void DoKLFit(bool fit, bool veto = false, bool fixtop = false);
    void PrintCutflow(TH1F* h);
    void PrintCutflow(TH1F* h1, TH1F*h2);
    void FillHistogram();
    void NoPhJOR();
    void LargeEta();
    string AppendInfo(string raw);
};

#endif

