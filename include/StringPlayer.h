#ifndef  StringPlayer_h
#define StringPlayer_h
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <TMath.h>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>

using namespace std;

static string removeSpaces(string input)
{
  input.erase(std::remove(input.begin(),input.end(),' '),input.end());
  return input;
}

static void AutoComplete(vector<string> &input) {
    for (int i = 0; i < input.size(); i++) {
	if (input.at(i) == "+") {
	    input.at(i) = input.at(i-1);
	}
    }
}

static int split(const std::string &txt, std::vector<std::string> &strs, char ch)
{
    int pos = txt.find( ch );
    int initialPos = 0;
    strs.clear();

    // Decompose statement
    while( pos != std::string::npos ) {
        strs.push_back( txt.substr( initialPos, pos - initialPos ) );
        initialPos = pos + 1;

        pos = txt.find( ch, initialPos );
    }

    // Add the last one
    strs.push_back( txt.substr( initialPos, std::min(int(pos), int(txt.size())) - initialPos + 1 ) );

    return strs.size();
}

static string replaceChar(string str, char ch1, char ch2) {
    for (int i = 0; i < str.length(); ++i) {
      	if (str[i] == ch1)
   	str[i] = ch2;
    }
    
    return str;
}

static const char *replace_str(const char *str, const char *orig, const char *rep) {
    static char buffer[4096];
    const char *p;

    if(!(p = strstr(str, orig)))  // Is 'orig' even in 'str'?
	return str;

    strncpy(buffer, str, p-str); // Copy characters from 'str' start to 'orig' st$
    buffer[p-str] = '\0';

    sprintf(buffer+(p-str), "%s%s", rep, p+strlen(orig));

    return buffer;
}

static vector<string> SplitToVector(string input, string tag = ",", bool debug = false) {
    vector<string> toreturn;
    if (input.find(tag.c_str(), 0) == string::npos) {
	toreturn.push_back(input);
	return toreturn;
    } else {
	int n1 = input.find_first_of(tag.c_str(), 0);
	if (debug) cout << input.substr(0, n1) << endl;
	toreturn.push_back(input.substr(0, n1));
	int nsize = input.size();
	while (n1 != string::npos) {
	    int n2 = n1;
	    n1 = input.find_first_of(tag.c_str(), n2+1);
	    string tmpstr;
	    if (n1 == string::npos) {
		tmpstr = input.substr(n2+1, nsize-n2-1);
	    } else {
		tmpstr = input.substr(n2+1, n1-n2-1);
	    }
	    if (debug) cout<< tmpstr << endl;
	    toreturn.push_back(tmpstr);
	}
	return toreturn;
    }
}

#endif
