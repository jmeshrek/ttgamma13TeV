#ifndef WORKSPACEMAKER_H
#define WORKSPACEMAKER_H

#include "AtlasStyle.h"
#include "Logger.h"

#include <TFile.h>
#include <TF1.h>
#include <TH2F.h>
#include <TH1F.h>

#include <RooFit.h>
#include <RooRealVar.h>
#include <RooGaussian.h>
#include <RooWorkspace.h>
#include <RooDataSet.h>
#include <RooRealVar.h>
#include <RooArgSet.h>
#include <RooWorkspace.h>
#include <RooDataHist.h>
#include <RooHist.h>
#include <RooHistPdf.h>
#include <RooProduct.h>
#include <RooAddPdf.h>
#include <RooAbsPdf.h>
#include <RooAddition.h>
#include <RooProdPdf.h>
#include <RooGaussModel.h>
#include <RooBinning.h>
#include <RooGenericPdf.h>
#include <RooExtendPdf.h>
#include <RooPoisson.h>
#include <RooLognormal.h>
#include <RooCategory.h>
#include <RooSimultaneous.h>

#include <RooStats/RooStatsUtils.h>
#include <RooStats/ModelConfig.h>
#include <RooStats/MCMCInterval.h>
#include <RooStats/MCMCIntervalPlot.h>

#include <iostream>
#include <sstream>
#include <string>
#include <iostream>
#include <iomanip>
#include <string>
#include <libgen.h>
#include <stdio.h>      
#include <stdlib.h> 

using namespace RooFit;
using namespace RooStats;
using namespace std;

class WorkspaceMaker {

public : 

    WorkspaceMaker();
    ~WorkspaceMaker();

    // Observable
    void DefineObs(string title, string unit, float low, float high, int nbin);
    void UseNbins(int bin_to_use);

    // Channel
    void AddChannel(string name, string title);
    void FreezeChannels();
    bool ChannelExist(string name);
    void GetChannelIndex(string name, int &index);

    // Process
    void AddProcess(string channelname, string name, string title, bool signal);
    void FreezeProcesses();
    void GetProcIndex(string channelname, string procname, int& index);

    // Parameter
    void AddParameter(string channelname, string procname, string name, string title, float val, float low, float high, bool fixed, bool poi);
    float GetParameterVal(string channelname, string procname, string name);

    // Template
    void SetTemplate(TH1F *temp, string channelname, string procname);

    // Data
    void SetData(TH1F *data, string channelname);

    // Check
    void Check();
    
    // Workspace
    void AddPoisson(int i, int j, RooArgSet &poissons);
    void MakeWorkspace();
    void SetSaveName(string name);

    // Info
    void ShutUp();
    void Silent();
    void DebugMode(bool debug);

    // Systematics
    void ShapeSysOff();
    void AddSystematics(string name, string title);
    void AddSystematicsStat(string name, string title, float err);
    void AddNormResponse(string sysname, string channelname, string procname, float val);
    void AddNormResponse2(string sysname, string channelname, string procname, float val);
    void AddShapeResponse(string sysname, string channelname, string procname, vector<float> &vals, bool oneway = false);
    void SetResponseType(int type);

    //void CorrProcTemplate(string name, string title, TH1F *proccorr, int type, float frac, float err, string channelname, string procname);
    
private : 
     	
    bool    m_debug;
    Logger *m_lg;

    // observable
    bool m_obs_ready;
    string  m_obs_title;
    string  m_obs_unit;
    float   m_obs_low;
    float   m_obs_high;
    int	    m_obs_nbin;
    int	    m_bin_to_use;
    RooRealVar* m_obs;

    // channel
    bool m_chan_ready;
    vector<string>  *m_chan_names;
    vector<string>  *m_chan_titles;
    int		    m_chan_n;
    RooCategory	    *m_chans;
    RooCategory	    *m_chants;

    // process
    bool m_proc_ready;
    vector<int>			  *m_proc_n;
    vector<vector<string>*> 	  *m_proc_names;
    vector<vector<string>*> 	  *m_proc_titles;
    vector<vector<bool>*> 	  *m_proc_issigs;
    vector<RooCategory*>	  *m_procs;
    vector<RooCategory*>	  *m_procts;
    TH2F			  *h_proc_issigs;
    TH2F			  *h_proc_isfrees;

    // parameter
    RooArgSet			    *m_pois;
    RooArgSet			    *m_nuiss;
    RooArgSet			    *m_globs;
    RooArgSet			    *m_paras;
    vector<vector<RooArgSet*>*>	    *m_proc_paras;

    // template
    vector<vector<TH1F*>*>	    *m_proc_temps;

    // data
    vector<TH1F*>	*m_datas;

    // check
    bool m_checked;

    // workspace
    RooArgSet	    *m_obss;
    RooProdPdf	    *m_model;
    RooDataSet	    *m_dataset;
    RooWorkspace    *m_workspace;
    ModelConfig	    *m_modelconfig;
    string	    m_savename;

    // systematics
    bool			    m_shapesys_off;
    RooArgSet			    *m_syss;
    RooArgSet			    *m_sys_csts;
    vector<vector<RooArgSet*>*>	    *m_sys_norm_responses;
    vector<vector<RooArgSet*>*>	    *m_sys_norm_responses2;
    vector<vector<vector<RooArgSet*>*>*>    *m_sys_shape_responses;
    map<string,string>		    map_sys_proc;
    int				    m_response_type;

    //vector<RooAddition*>  *m_TotalNumbers;
    //vector<RooAddition*>  *m_BkgNumbers;
    //vector<vector<RooProduct*>*>  *m_ProcNumbers;
    //vector<vector<RooAbsPdf*>*>   *m_ProcModels;
    //vector<RooAbsPdf*> *m_Models;
    //RooSimultaneous* m_Model_NoCst;
    //RooAbsPdf* m_Model;
    //ModelConfig *m_ModelConfig;
    //RooWorkspace* m_Workspace;
};

#endif
