#include <TF1.h>
#include <TMath.h>
#include <TFile.h>
#include <TKey.h>
#include <iostream>
#include <map>
#include <TH1.h>
#include <string>

using namespace std;

struct Figure {
    TH1F* h_ptcone;
    TH1F* h_leppt;
    TH1F* h_lepeta;
    TH1F* h_phpt;
    TH1F* h_pheta;
    TH1F* h_jetpt;
    TH1F* h_jeteta;
    TH1F* h_met;
    TH1F* h_mphl;
    TH1F* h_njet;
    TH1F* h_nbjet;
    TH1F* h_drphl;
};

struct Counter {
    double num_all;
    double num_total;
    double num_fidu;

    double num_total_ptbin1;
    double num_total_ptbin2;
    double num_total_ptbin3;
    double num_total_ptbin4;
    double num_total_ptbin5;
    double num_fidu_ptbin1;
    double num_fidu_ptbin2;
    double num_fidu_ptbin3;
    double num_fidu_ptbin4;
    double num_fidu_ptbin5;
    double num_total_etabin1;
    double num_total_etabin2;
    double num_total_etabin3;
    double num_total_etabin4;
    double num_total_etabin5;
    double num_fidu_etabin1;
    double num_fidu_etabin2;
    double num_fidu_etabin3;
    double num_fidu_etabin4;
    double num_fidu_etabin5;

    double num_isobin1;
    double num_isobin2;
    double num_isobin3;
    double num_isobin4;
    double num_isobin5;

    double num_ptbin1_isobin1;
    double num_ptbin1_isobin2;
    double num_ptbin1_isobin3;
    double num_ptbin1_isobin4;
    double num_ptbin1_isobin5;
    double num_ptbin2_isobin1;
    double num_ptbin2_isobin2;
    double num_ptbin2_isobin3;
    double num_ptbin2_isobin4;
    double num_ptbin2_isobin5;
    double num_ptbin3_isobin1;
    double num_ptbin3_isobin2;
    double num_ptbin3_isobin3;
    double num_ptbin3_isobin4;
    double num_ptbin3_isobin5;
    double num_ptbin4_isobin1;
    double num_ptbin4_isobin2;
    double num_ptbin4_isobin3;
    double num_ptbin4_isobin4;
    double num_ptbin4_isobin5;
    double num_ptbin5_isobin1;
    double num_ptbin5_isobin2;
    double num_ptbin5_isobin3;
    double num_ptbin5_isobin4;
    double num_ptbin5_isobin5;
    double num_etabin1_isobin1;
    double num_etabin1_isobin2;
    double num_etabin1_isobin3;
    double num_etabin1_isobin4;
    double num_etabin1_isobin5;
    double num_etabin2_isobin1;
    double num_etabin2_isobin2;
    double num_etabin2_isobin3;
    double num_etabin2_isobin4;
    double num_etabin2_isobin5;
    double num_etabin3_isobin1;
    double num_etabin3_isobin2;
    double num_etabin3_isobin3;
    double num_etabin3_isobin4;
    double num_etabin3_isobin5;
    double num_etabin4_isobin1;
    double num_etabin4_isobin2;
    double num_etabin4_isobin3;
    double num_etabin4_isobin4;
    double num_etabin4_isobin5;
    double num_etabin5_isobin1;
    double num_etabin5_isobin2;
    double num_etabin5_isobin3;
    double num_etabin5_isobin4;
    double num_etabin5_isobin5;

    double err_all;
    double err_total;
    double err_fidu;

    double err_total_ptbin1;
    double err_total_ptbin2;
    double err_total_ptbin3;
    double err_total_ptbin4;
    double err_total_ptbin5;
    double err_fidu_ptbin1;
    double err_fidu_ptbin2;
    double err_fidu_ptbin3;
    double err_fidu_ptbin4;
    double err_fidu_ptbin5;
    double err_total_etabin1;
    double err_total_etabin2;
    double err_total_etabin3;
    double err_total_etabin4;
    double err_total_etabin5;
    double err_fidu_etabin1;
    double err_fidu_etabin2;
    double err_fidu_etabin3;
    double err_fidu_etabin4;
    double err_fidu_etabin5;

    double err_isobin1;
    double err_isobin2;
    double err_isobin3;
    double err_isobin4;
    double err_isobin5;

    double err_ptbin1_isobin1;
    double err_ptbin1_isobin2;
    double err_ptbin1_isobin3;
    double err_ptbin1_isobin4;
    double err_ptbin1_isobin5;
    double err_ptbin2_isobin1;
    double err_ptbin2_isobin2;
    double err_ptbin2_isobin3;
    double err_ptbin2_isobin4;
    double err_ptbin2_isobin5;
    double err_ptbin3_isobin1;
    double err_ptbin3_isobin2;
    double err_ptbin3_isobin3;
    double err_ptbin3_isobin4;
    double err_ptbin3_isobin5;
    double err_ptbin4_isobin1;
    double err_ptbin4_isobin2;
    double err_ptbin4_isobin3;
    double err_ptbin4_isobin4;
    double err_ptbin4_isobin5;
    double err_ptbin5_isobin1;
    double err_ptbin5_isobin2;
    double err_ptbin5_isobin3;
    double err_ptbin5_isobin4;
    double err_ptbin5_isobin5;
    double err_etabin1_isobin1;
    double err_etabin1_isobin2;
    double err_etabin1_isobin3;
    double err_etabin1_isobin4;
    double err_etabin1_isobin5;
    double err_etabin2_isobin1;
    double err_etabin2_isobin2;
    double err_etabin2_isobin3;
    double err_etabin2_isobin4;
    double err_etabin2_isobin5;
    double err_etabin3_isobin1;
    double err_etabin3_isobin2;
    double err_etabin3_isobin3;
    double err_etabin3_isobin4;
    double err_etabin3_isobin5;
    double err_etabin4_isobin1;
    double err_etabin4_isobin2;
    double err_etabin4_isobin3;
    double err_etabin4_isobin4;
    double err_etabin4_isobin5;
    double err_etabin5_isobin1;
    double err_etabin5_isobin2;
    double err_etabin5_isobin3;
    double err_etabin5_isobin4;
    double err_etabin5_isobin5;
};

struct AccCorr {
    double num_acc;
    double num_acc_ptbin1;
    double num_acc_ptbin2;
    double num_acc_ptbin3;
    double num_acc_ptbin4;
    double num_acc_ptbin5;
    double num_acc_etabin1;
    double num_acc_etabin2;
    double num_acc_etabin3;
    double num_acc_etabin4;
    double num_acc_etabin5;
    double num_acc2;
    double num_acc2_ptbin1;
    double num_acc2_ptbin2;
    double num_acc2_ptbin3;
    double num_acc2_ptbin4;
    double num_acc2_ptbin5;
    double num_acc2_etabin1;
    double num_acc2_etabin2;
    double num_acc2_etabin3;
    double num_acc2_etabin4;
    double num_acc2_etabin5;
    double num_acc3;
    double num_acc3_ptbin1;
    double num_acc3_ptbin2;
    double num_acc3_ptbin3;
    double num_acc3_ptbin4;
    double num_acc3_ptbin5;
    double num_acc3_etabin1;
    double num_acc3_etabin2;
    double num_acc3_etabin3;
    double num_acc3_etabin4;
    double num_acc3_etabin5;
    double num_corr;
    double num_corr_ptbin1;
    double num_corr_ptbin2;
    double num_corr_ptbin3;
    double num_corr_ptbin4;
    double num_corr_ptbin5;
    double num_corr_etabin1;
    double num_corr_etabin2;
    double num_corr_etabin3;
    double num_corr_etabin4;
    double num_corr_etabin5;
    double num_acccorr;
    double num_acccorr_ptbin1;
    double num_acccorr_ptbin2;
    double num_acccorr_ptbin3;
    double num_acccorr_ptbin4;
    double num_acccorr_ptbin5;
    double num_acccorr_etabin1;
    double num_acccorr_etabin2;
    double num_acccorr_etabin3;
    double num_acccorr_etabin4;
    double num_acccorr_etabin5;

    double err_acc;
    double err_acc_ptbin1;
    double err_acc_ptbin2;
    double err_acc_ptbin3;
    double err_acc_ptbin4;
    double err_acc_ptbin5;
    double err_acc_etabin1;
    double err_acc_etabin2;
    double err_acc_etabin3;
    double err_acc_etabin4;
    double err_acc_etabin5;
    double err_acc2;
    double err_acc2_ptbin1;
    double err_acc2_ptbin2;
    double err_acc2_ptbin3;
    double err_acc2_ptbin4;
    double err_acc2_ptbin5;
    double err_acc2_etabin1;
    double err_acc2_etabin2;
    double err_acc2_etabin3;
    double err_acc2_etabin4;
    double err_acc2_etabin5;
    double err_acc3;
    double err_acc3_ptbin1;
    double err_acc3_ptbin2;
    double err_acc3_ptbin3;
    double err_acc3_ptbin4;
    double err_acc3_ptbin5;
    double err_acc3_etabin1;
    double err_acc3_etabin2;
    double err_acc3_etabin3;
    double err_acc3_etabin4;
    double err_acc3_etabin5;
    double err_corr;
    double err_corr_ptbin1;
    double err_corr_ptbin2;
    double err_corr_ptbin3;
    double err_corr_ptbin4;
    double err_corr_ptbin5;
    double err_corr_etabin1;
    double err_corr_etabin2;
    double err_corr_etabin3;
    double err_corr_etabin4;
    double err_corr_etabin5;
    double err_acccorr;
    double err_acccorr_ptbin1;
    double err_acccorr_ptbin2;
    double err_acccorr_ptbin3;
    double err_acccorr_ptbin4;
    double err_acccorr_ptbin5;
    double err_acccorr_etabin1;
    double err_acccorr_etabin2;
    double err_acccorr_etabin3;
    double err_acccorr_etabin4;
    double err_acccorr_etabin5;
};

struct AccRatio {
    double num_accratio;
    double num_accratio_ptbin1;
    double num_accratio_ptbin2;
    double num_accratio_ptbin3;
    double num_accratio_ptbin4;
    double num_accratio_ptbin5;
    double num_accratio_etabin1;
    double num_accratio_etabin2;
    double num_accratio_etabin3;
    double num_accratio_etabin4;
    double num_accratio_etabin5;

    double err_accratio;
    double err_accratio_ptbin1;
    double err_accratio_ptbin2;
    double err_accratio_ptbin3;
    double err_accratio_ptbin4;
    double err_accratio_ptbin5;
    double err_accratio_etabin1;
    double err_accratio_etabin2;
    double err_accratio_etabin3;
    double err_accratio_etabin4;
    double err_accratio_etabin5;
};

Figure GetFigure(string file, string chan, string sys, bool debug = false);
void	ScaleFigure(Figure &a, float scale, bool debug = false);
Figure	SumFigures(vector<Figure> &cs, bool debug = false);
Figure  EmptyFigure();

Counter GetCounter(string file, string chan, string sys, bool normiso = true, bool debug = false, bool uselabel = false);
Counter GetCounterRelDiff(Counter &a, Counter &b, double rho = 0., bool smooth = false, bool draw = false, string name = "");
Counter GetCounterSymmetrized(Counter &up, Counter &dn);
Counter EmptyCounter();
Counter GetCounterEnvelope(vector<Counter> &cs);
Counter GetCounterRatio(Counter &a, Counter &b, double rho = 0.);
Counter GetAverageCounter(Counter &a, Counter &b);
void	ScaleCounter(Counter &a, float scale);
void	ScaleCounter(Counter &a, float scale, float err_scale);
Counter	SumCounters(vector<Counter> &cs, bool normiso = false);
Counter	SumCountersQuadratic(vector<Counter> &cs);

AccCorr CalcuAccCorr(Counter &a_reco, Counter &a_truth);
AccCorr GetAccCorrRelDiff(AccCorr &a, AccCorr &b, double rho = 0.);
AccCorr GetAccCorrSymmetrized(AccCorr &up, AccCorr &dn);
AccCorr GetAccCorrEnvelope(vector<AccCorr> &acs);
AccCorr GetAccCorrRatio(AccCorr &a, AccCorr &b, double rho = 0.);
AccCorr EmptyAccCorr();

AccRatio CalcuAccRatio(AccCorr &a, AccCorr &b);
AccRatio GetAccRatioRelDiff(AccRatio &a, AccRatio &b, double rho = 0.);
AccRatio GetAccRatioSymmetrized(AccRatio &up, AccRatio &dn);
AccRatio GetAccRatioEnvelope(vector<AccRatio> &acs);
AccRatio GetAccRatioRatio(AccRatio &a, AccRatio &b, double rho = 0.);
AccRatio EmptyAccRatio();

void PrintBanner(string title, int type);
//void PrintBanner2(string title, int type, bool printout = false);
void PrintCounter(string title, Counter &a, int type, double scale = 1.0, bool err = false, bool banner = true);
void PrintCounterError(string title, Counter &a, int type, double scale = 1.0, bool banner = true);
void PrintSys(string title, Counter &a, int type, double scale = 1.0, bool err = false, bool banner = true);
void PrintAccCorr(string title, AccCorr &ac, int type, double scale, bool err = false, bool banner = true);
void PrintAccRatio(string title, AccRatio &ac, int type, double scale, bool err = false, bool banner = true);

double GetErrorAoverAB(double a, double err_a, double ab, double err_ab);
double GetErrorAoverB(double a, double err_a, double b, double err_b);
void RegulateSysName(string &name);
void OrderFVector(vector<double> fv, vector<int> &order, bool debug = false);
int FindRemoveMax(vector<double> &input, bool debug = false);
void Smooth(Counter &a, bool draw = false, string name = "");

Figure GetFigure(string file, string chan, string sys, bool debug) {

    string type, proc;
    if (file.find("Signal",0) != string::npos) proc = "Signal";
    else if (file.find("ZjetsElEl",0) != string::npos) proc = "ZjetsElEl";
    else if (file.find("ZjetsMuMu",0) != string::npos) proc = "ZjetsMuMu";
    else if (file.find("ZjetsTauTau",0) != string::npos) proc = "ZjetsTauTau";
    else if (file.find("WTDR",0) != string::npos) proc = "WTDR";
    else if (file.find("WWNP0",0) != string::npos) proc = "WWNP0";
    else if (file.find("WWNP1",1) != string::npos) proc = "WWNP1";
    else if (file.find("WWNP2",2) != string::npos) proc = "WWNP2";
    else if (file.find("WWNP3",3) != string::npos) proc = "WWNP3";
    else if (file.find("WZNP0",0) != string::npos) proc = "WZNP0";
    else if (file.find("WZNP1",1) != string::npos) proc = "WZNP1";
    else if (file.find("WZNP2",2) != string::npos) proc = "WZNP2";
    else if (file.find("WZNP3",3) != string::npos) proc = "WZNP3";
    else if (file.find("ZZNP0",0) != string::npos) proc = "ZZNP0";
    else if (file.find("ZZNP1",1) != string::npos) proc = "ZZNP1";
    else if (file.find("ZZNP2",2) != string::npos) proc = "ZZNP2";
    else if (file.find("ZZNP3",3) != string::npos) proc = "ZZNP3";
    else if (file.find("WjetsNp0",0) != string::npos) proc = "WjetsNp0";
    else if (file.find("WjetsNp1",0) != string::npos) proc = "WjetsNp1";
    else if (file.find("WjetsNp2",0) != string::npos) proc = "WjetsNp2";
    else if (file.find("WjetsNp3",0) != string::npos) proc = "WjetsNp3";
    else if (file.find("WjetsNp4",0) != string::npos) proc = "WjetsNp4";
    else if (file.find("WjetsNp5",0) != string::npos) proc = "WjetsNp5";
    else if (file.find("WjetsNewNp0",0) != string::npos) proc = "WjetsNewNp0";
    else if (file.find("WjetsNewNp1",0) != string::npos) proc = "WjetsNewNp1";
    else if (file.find("WjetsNewNp2",0) != string::npos) proc = "WjetsNewNp2";
    else if (file.find("WjetsNewNp3",0) != string::npos) proc = "WjetsNewNp3";
    else if (file.find("WjetsNewNp4",0) != string::npos) proc = "WjetsNewNp4";
    else if (file.find("WjetsNewNp5",0) != string::npos) proc = "WjetsNewNp5";
    else if (file.find("WjetsElNp0",0) != string::npos) proc = "WjetsElNp0";
    else if (file.find("WjetsElNp1",0) != string::npos) proc = "WjetsElNp1";
    else if (file.find("WjetsElNp2",0) != string::npos) proc = "WjetsElNp2";
    else if (file.find("WjetsElNp3",0) != string::npos) proc = "WjetsElNp3";
    else if (file.find("WjetsElNp4",0) != string::npos) proc = "WjetsElNp4";
    else if (file.find("WjetsElNp5",0) != string::npos) proc = "WjetsElNp5";
    else if (file.find("WjetsMuNp0",0) != string::npos) proc = "WjetsMuNp0";
    else if (file.find("WjetsMuNp1",0) != string::npos) proc = "WjetsMuNp1";
    else if (file.find("WjetsMuNp2",0) != string::npos) proc = "WjetsMuNp2";
    else if (file.find("WjetsMuNp3",0) != string::npos) proc = "WjetsMuNp3";
    else if (file.find("WjetsMuNp4",0) != string::npos) proc = "WjetsMuNp4";
    else if (file.find("WjetsMuNp5",0) != string::npos) proc = "WjetsMuNp5";
    else if (file.find("WjetsTauNp0",0) != string::npos) proc = "WjetsTauNp0";
    else if (file.find("WjetsTauNp1",0) != string::npos) proc = "WjetsTauNp1";
    else if (file.find("WjetsTauNp2",0) != string::npos) proc = "WjetsTauNp2";
    else if (file.find("WjetsTauNp3",0) != string::npos) proc = "WjetsTauNp3";
    else if (file.find("WjetsTauNp4",0) != string::npos) proc = "WjetsTauNp4";
    else if (file.find("WjetsTauNp5",0) != string::npos) proc = "WjetsTauNp5";
    else if (file.find("WjetsCNp0",0) != string::npos) proc = "WjetsCNp0";
    else if (file.find("WjetsCNp1",0) != string::npos) proc = "WjetsCNp1";
    else if (file.find("WjetsCNp2",0) != string::npos) proc = "WjetsCNp2";
    else if (file.find("WjetsCNp3",0) != string::npos) proc = "WjetsCNp3";
    else if (file.find("WjetsCNp4",0) != string::npos) proc = "WjetsCNp4";
    else if (file.find("WjetsCCNp0",0) != string::npos) proc = "WjetsCCNp0";
    else if (file.find("WjetsCCNp1",0) != string::npos) proc = "WjetsCCNp1";
    else if (file.find("WjetsCCNp2",0) != string::npos) proc = "WjetsCCNp2";
    else if (file.find("WjetsCCNp3",0) != string::npos) proc = "WjetsCCNp3";
    else if (file.find("WjetsBBNp0",0) != string::npos) proc = "WjetsBBNp0";
    else if (file.find("WjetsBBNp1",0) != string::npos) proc = "WjetsBBNp1";
    else if (file.find("WjetsBBNp2",0) != string::npos) proc = "WjetsBBNp2";
    else if (file.find("WjetsBBNp3",0) != string::npos) proc = "WjetsBBNp3";
    else if (file.find("WjetsEl",0) != string::npos) proc = "WjetsEl";
    else if (file.find("WjetsMu",0) != string::npos) proc = "WjetsMu";
    else if (file.find("WjetsTau",0) != string::npos) proc = "WjetsTau";

    if (file.find("Truth", 0) != string::npos) type = "Truth";
    else if (file.find("Reco", 0) != string::npos) type = "Reco";
    else if (file.find("Data", 0) != string::npos) type = "Data";

    string hname_ptcone = "PtCone20_"; hname_ptcone += chan; hname_ptcone += "_"; hname_ptcone += type; hname_ptcone += "_"; if (proc != "") {hname_ptcone += proc; hname_ptcone += "_";} hname_ptcone += sys;
    string hname_leppt = "LepPt_"; hname_leppt += chan; hname_leppt += "_"; hname_leppt += type; hname_leppt += "_"; if (proc != "") {hname_leppt += proc; hname_leppt += "_";} hname_leppt += sys;
    string hname_lepeta = "LepEta_"; hname_lepeta += chan; hname_lepeta += "_"; hname_lepeta += type; hname_lepeta += "_"; if (proc != "") {hname_lepeta += proc; hname_lepeta += "_";} hname_lepeta += sys;
    string hname_jetpt = "JetPt_"; hname_jetpt += chan; hname_jetpt += "_"; hname_jetpt += type; hname_jetpt += "_"; if (proc != "") {hname_jetpt += proc; hname_jetpt += "_";} hname_jetpt += sys;
    string hname_jeteta = "JetEta_"; hname_jeteta += chan; hname_jeteta += "_"; hname_jeteta += type; hname_jeteta += "_"; if (proc != "") {hname_jeteta += proc; hname_jeteta += "_";} hname_jeteta += sys;
    string hname_phpt = "PhPt_"; hname_phpt += chan; hname_phpt += "_"; hname_phpt += type; hname_phpt += "_"; if (proc != "") {hname_phpt += proc; hname_phpt += "_";} hname_phpt += sys;
    string hname_pheta = "PhEta_"; hname_pheta += chan; hname_pheta += "_"; hname_pheta += type; hname_pheta += "_"; if (proc != "") {hname_pheta += proc; hname_pheta += "_";} hname_pheta += sys;
    string hname_met = "MET_"; hname_met += chan; hname_met += "_"; hname_met += type; hname_met += "_"; if (proc != "") {hname_met += proc; hname_met += "_";} hname_met += sys;
    string hname_mphl = "MPhL_"; hname_mphl += chan; hname_mphl += "_"; hname_mphl += type; hname_mphl += "_"; if (proc != "") {hname_mphl += proc; hname_mphl += "_";} hname_mphl += sys;
    string hname_njet = "Njet_"; hname_njet += chan; hname_njet += "_"; hname_njet += type; hname_njet += "_"; if (proc != "") {hname_njet += proc; hname_njet += "_";} hname_njet += sys;
    string hname_nbjet = "Nbjet_"; hname_nbjet += chan; hname_nbjet += "_"; hname_nbjet += type; hname_nbjet += "_"; if (proc != "") {hname_nbjet += proc; hname_nbjet += "_";} hname_nbjet += sys;
    string hname_drphl = "DrPhL_"; hname_drphl += chan; hname_drphl += "_"; hname_drphl += type; hname_drphl += "_"; if (proc != "") {hname_drphl += proc; hname_drphl += "_";} hname_drphl += sys;

    Figure c;

    TFile* f = new TFile(file.c_str());
    if (debug) {
	f->ls();
	cout << hname_ptcone << endl;
	cout << hname_leppt << endl;
	cout << hname_lepeta << endl;
	cout << hname_jetpt << endl;
	cout << hname_jeteta << endl;
	cout << hname_phpt << endl;
	cout << hname_pheta << endl;
	cout << hname_met << endl;
	cout << hname_mphl << endl;
	cout << hname_njet << endl;
	cout << hname_drphl << endl;
	cout << hname_ptcone << endl;
    }

    c.h_ptcone = (TH1F*)f->Get(hname_ptcone.c_str()); //if (h_ptcone) c.h_ptcone = (TH1F*)h_ptcone->Clone();
    c.h_leppt = (TH1F*)f->Get(hname_leppt.c_str()); //if (h_leppt) c.h_leppt = (TH1F*)h_leppt->Clone();
    c.h_lepeta = (TH1F*)f->Get(hname_lepeta.c_str()); //if (h_lepeta) c.h_lepeta = (TH1F*)h_lepeta->Clone();
    c.h_jetpt = (TH1F*)f->Get(hname_jetpt.c_str()); //if (h_jetpt) c.h_jetpt = (TH1F*)h_jetpt->Clone();
    c.h_jeteta = (TH1F*)f->Get(hname_jeteta.c_str()); //if (h_jeteta) c.h_jeteta = (TH1F*)h_jeteta->Clone();
    c.h_phpt = (TH1F*)f->Get(hname_phpt.c_str()); //if (h_phpt) c.h_phpt = (TH1F*)h_phpt->Clone();
    c.h_pheta = (TH1F*)f->Get(hname_pheta.c_str()); //if (h_pheta) c.h_pheta = (TH1F*)h_pheta->Clone();
    c.h_met = (TH1F*)f->Get(hname_met.c_str()); //if (h_met) c.h_met = (TH1F*)h_met->Clone();
    c.h_mphl = (TH1F*)f->Get(hname_mphl.c_str()); //if (h_mphl) c.h_mphl = (TH1F*)h_mphl->Clone();
    c.h_njet = (TH1F*)f->Get(hname_njet.c_str()); //if (h_njet) c.h_njet = (TH1F*)h_njet->Clone();
    c.h_nbjet = (TH1F*)f->Get(hname_nbjet.c_str()); //if (h_nbjet) c.h_nbjet = (TH1F*)h_nbjet->Clone();
    c.h_drphl = (TH1F*)f->Get(hname_drphl.c_str()); //if (h_drphl) c.h_drphl = (TH1F*)h_drphl->Clone();

    //if (debug) {
    //    cout << h_ptcone << " " << h_leppt << " " << h_lepeta << " " << h_jetpt << " " << h_jeteta << " " << h_phpt << " " << h_pheta << " " << h_met << endl;
    //}

    return c;
}

Counter GetCounter(string file, string chan, string sys, bool normiso, bool debug, bool uselabel) {

    string type, proc;
    if (file.find("Signal",0) != string::npos) proc = "Signal";
    else if (file.find("WjetsEl",0) != string::npos) proc = "WjetsEl";
    else if (file.find("WjetsMu",0) != string::npos) proc = "WjetsMu";
    else if (file.find("WjetsTau",0) != string::npos) proc = "WjetsTau";
    else if (file.find("ZjetsElEl",0) != string::npos) proc = "ZjetsElEl";
    else if (file.find("ZjetsMuMu",0) != string::npos) proc = "ZjetsMuMu";
    else if (file.find("ZjetsTauTau",0) != string::npos) proc = "ZjetsTauTau";
    else if (file.find("WTDR",0) != string::npos) proc = "WTDR";
    else if (file.find("WWNP0",0) != string::npos) proc = "WWNP0";
    else if (file.find("WWNP1",1) != string::npos) proc = "WWNP1";
    else if (file.find("WWNP2",2) != string::npos) proc = "WWNP2";
    else if (file.find("WWNP3",3) != string::npos) proc = "WWNP3";
    else if (file.find("WZNP0",0) != string::npos) proc = "WZNP0";
    else if (file.find("WZNP1",1) != string::npos) proc = "WZNP1";
    else if (file.find("WZNP2",2) != string::npos) proc = "WZNP2";
    else if (file.find("WZNP3",3) != string::npos) proc = "WZNP3";
    else if (file.find("ZZNP0",0) != string::npos) proc = "ZZNP0";
    else if (file.find("ZZNP1",1) != string::npos) proc = "ZZNP1";
    else if (file.find("ZZNP2",2) != string::npos) proc = "ZZNP2";
    else if (file.find("ZZNP3",3) != string::npos) proc = "ZZNP3";
    else if (file.find("WjetsElNp0",0) != string::npos) proc = "WjetsElNp0";
    else if (file.find("WjetsElNp1",0) != string::npos) proc = "WjetsElNp1";
    else if (file.find("WjetsElNp2",0) != string::npos) proc = "WjetsElNp2";
    else if (file.find("WjetsElNp3",0) != string::npos) proc = "WjetsElNp3";
    else if (file.find("WjetsElNp4",0) != string::npos) proc = "WjetsElNp4";
    else if (file.find("WjetsElNp5",0) != string::npos) proc = "WjetsElNp5";
    else if (file.find("WjetsMuNp0",0) != string::npos) proc = "WjetsMuNp0";
    else if (file.find("WjetsMuNp1",0) != string::npos) proc = "WjetsMuNp1";
    else if (file.find("WjetsMuNp2",0) != string::npos) proc = "WjetsMuNp2";
    else if (file.find("WjetsMuNp3",0) != string::npos) proc = "WjetsMuNp3";
    else if (file.find("WjetsMuNp4",0) != string::npos) proc = "WjetsMuNp4";
    else if (file.find("WjetsMuNp5",0) != string::npos) proc = "WjetsMuNp5";
    else if (file.find("WjetsTauNp0",0) != string::npos) proc = "WjetsTauNp0";
    else if (file.find("WjetsTauNp1",0) != string::npos) proc = "WjetsTauNp1";
    else if (file.find("WjetsTauNp2",0) != string::npos) proc = "WjetsTauNp2";
    else if (file.find("WjetsTauNp3",0) != string::npos) proc = "WjetsTauNp3";
    else if (file.find("WjetsTauNp4",0) != string::npos) proc = "WjetsTauNp4";
    else if (file.find("WjetsTauNp5",0) != string::npos) proc = "WjetsTauNp5";
    else if (file.find("WjetsCNp0",0) != string::npos) proc = "WjetsCNp0";
    else if (file.find("WjetsCNp1",0) != string::npos) proc = "WjetsCNp1";
    else if (file.find("WjetsCNp2",0) != string::npos) proc = "WjetsCNp2";
    else if (file.find("WjetsCNp3",0) != string::npos) proc = "WjetsCNp3";
    else if (file.find("WjetsCNp4",0) != string::npos) proc = "WjetsCNp4";
    else if (file.find("WjetsCCNp0",0) != string::npos) proc = "WjetsCCNp0";
    else if (file.find("WjetsCCNp1",0) != string::npos) proc = "WjetsCCNp1";
    else if (file.find("WjetsCCNp2",0) != string::npos) proc = "WjetsCCNp2";
    else if (file.find("WjetsCCNp3",0) != string::npos) proc = "WjetsCCNp3";
    else if (file.find("WjetsBBNp0",0) != string::npos) proc = "WjetsBBNp0";
    else if (file.find("WjetsBBNp1",0) != string::npos) proc = "WjetsBBNp1";
    else if (file.find("WjetsBBNp2",0) != string::npos) proc = "WjetsBBNp2";
    else if (file.find("WjetsBBNp3",0) != string::npos) proc = "WjetsBBNp3";

    if (file.find("Truth", 0) != string::npos) type = "Truth";
    else if (file.find("Reco", 0) != string::npos) type = "Reco";
    else if (file.find("Data", 0) != string::npos) type = "Data";

    string hname_cutflow = "Cutflow_";
    hname_cutflow += chan; hname_cutflow += "_";
    hname_cutflow += type; hname_cutflow += "_";
    if (proc != "") {hname_cutflow += proc; hname_cutflow += "_";}
    hname_cutflow += sys;

    string hname_ptcone = "PtCone20_";
    hname_ptcone += chan; hname_ptcone += "_";
    hname_ptcone += type; hname_ptcone += "_";
    if (proc != "") {hname_ptcone += proc; hname_ptcone += "_";}
    hname_ptcone += sys;

    string hname_ptbin1_ptcone = "PtCone20_Ptbin1_";
    hname_ptbin1_ptcone += chan; hname_ptbin1_ptcone += "_";
    hname_ptbin1_ptcone += type; hname_ptbin1_ptcone += "_";
    if (proc != "") {hname_ptbin1_ptcone += proc; hname_ptbin1_ptcone += "_";}
    hname_ptbin1_ptcone += sys;
    string hname_ptbin2_ptcone = "PtCone20_Ptbin2_";
    hname_ptbin2_ptcone += chan; hname_ptbin2_ptcone += "_";
    hname_ptbin2_ptcone += type; hname_ptbin2_ptcone += "_";
    if (proc != "") {hname_ptbin2_ptcone += proc; hname_ptbin2_ptcone += "_";}
    hname_ptbin2_ptcone += sys;
    string hname_ptbin3_ptcone = "PtCone20_Ptbin3_";
    hname_ptbin3_ptcone += chan; hname_ptbin3_ptcone += "_";
    hname_ptbin3_ptcone += type; hname_ptbin3_ptcone += "_";
    if (proc != "") {hname_ptbin3_ptcone += proc; hname_ptbin3_ptcone += "_";}
    hname_ptbin3_ptcone += sys;
    string hname_ptbin4_ptcone = "PtCone20_Ptbin4_";
    hname_ptbin4_ptcone += chan; hname_ptbin4_ptcone += "_";
    hname_ptbin4_ptcone += type; hname_ptbin4_ptcone += "_";
    if (proc != "") {hname_ptbin4_ptcone += proc; hname_ptbin4_ptcone += "_";}
    hname_ptbin4_ptcone += sys;
    string hname_ptbin5_ptcone = "PtCone20_Ptbin5_";
    hname_ptbin5_ptcone += chan; hname_ptbin5_ptcone += "_";
    hname_ptbin5_ptcone += type; hname_ptbin5_ptcone += "_";
    if (proc != "") {hname_ptbin5_ptcone += proc; hname_ptbin5_ptcone += "_";}
    hname_ptbin5_ptcone += sys;

    string hname_etabin1_ptcone = "PtCone20_Etabin1_";
    hname_etabin1_ptcone += chan; hname_etabin1_ptcone += "_";
    hname_etabin1_ptcone += type; hname_etabin1_ptcone += "_";
    if (proc != "") {hname_etabin1_ptcone += proc; hname_etabin1_ptcone += "_";}
    hname_etabin1_ptcone += sys;
    string hname_etabin2_ptcone = "PtCone20_Etabin2_";
    hname_etabin2_ptcone += chan; hname_etabin2_ptcone += "_";
    hname_etabin2_ptcone += type; hname_etabin2_ptcone += "_";
    if (proc != "") {hname_etabin2_ptcone += proc; hname_etabin2_ptcone += "_";}
    hname_etabin2_ptcone += sys;
    string hname_etabin3_ptcone = "PtCone20_Etabin3_";
    hname_etabin3_ptcone += chan; hname_etabin3_ptcone += "_";
    hname_etabin3_ptcone += type; hname_etabin3_ptcone += "_";
    if (proc != "") {hname_etabin3_ptcone += proc; hname_etabin3_ptcone += "_";}
    hname_etabin3_ptcone += sys;
    string hname_etabin4_ptcone = "PtCone20_Etabin4_";
    hname_etabin4_ptcone += chan; hname_etabin4_ptcone += "_";
    hname_etabin4_ptcone += type; hname_etabin4_ptcone += "_";
    if (proc != "") {hname_etabin4_ptcone += proc; hname_etabin4_ptcone += "_";}
    hname_etabin4_ptcone += sys;
    string hname_etabin5_ptcone = "PtCone20_Etabin5_";
    hname_etabin5_ptcone += chan; hname_etabin5_ptcone += "_";
    hname_etabin5_ptcone += type; hname_etabin5_ptcone += "_";
    if (proc != "") {hname_etabin5_ptcone += proc; hname_etabin5_ptcone += "_";}
    hname_etabin5_ptcone += sys;

    Counter c;

    TFile* f = new TFile(file.c_str());
    TH1F* h_cutflow = (TH1F*)f->Get(hname_cutflow.c_str());
    if (h_cutflow) {
	if (!uselabel) {
	    c.num_all = h_cutflow->GetBinContent(1);
	    c.num_total = h_cutflow->GetBinContent(2);
    	    c.num_total_ptbin1 = h_cutflow->GetBinContent(3);
    	    c.num_total_ptbin2 = h_cutflow->GetBinContent(4);
    	    c.num_total_ptbin3 = h_cutflow->GetBinContent(5);
    	    c.num_total_ptbin4 = h_cutflow->GetBinContent(6);
    	    c.num_total_ptbin5 = h_cutflow->GetBinContent(7);
    	    c.num_total_etabin1 = h_cutflow->GetBinContent(8);
    	    c.num_total_etabin2 = h_cutflow->GetBinContent(9);
    	    c.num_total_etabin3 = h_cutflow->GetBinContent(10);
    	    c.num_total_etabin4 = h_cutflow->GetBinContent(11);
    	    c.num_total_etabin5 = h_cutflow->GetBinContent(12);
	    c.num_fidu = h_cutflow->GetBinContent(20);
    	    c.num_fidu_ptbin1 = h_cutflow->GetBinContent(21);
    	    c.num_fidu_ptbin2 = h_cutflow->GetBinContent(22);
    	    c.num_fidu_ptbin3 = h_cutflow->GetBinContent(23);
    	    c.num_fidu_ptbin4 = h_cutflow->GetBinContent(24);
    	    c.num_fidu_ptbin5 = h_cutflow->GetBinContent(25);
    	    c.num_fidu_etabin1 = h_cutflow->GetBinContent(26);
    	    c.num_fidu_etabin2 = h_cutflow->GetBinContent(27);
    	    c.num_fidu_etabin3 = h_cutflow->GetBinContent(28);
    	    c.num_fidu_etabin4 = h_cutflow->GetBinContent(29);
    	    c.num_fidu_etabin5 = h_cutflow->GetBinContent(30);
	    c.err_all = h_cutflow->GetBinError(1);
	    c.err_total = h_cutflow->GetBinError(2);
    	    c.err_total_ptbin1 = h_cutflow->GetBinError(3);
    	    c.err_total_ptbin2 = h_cutflow->GetBinError(4);
    	    c.err_total_ptbin3 = h_cutflow->GetBinError(5);
    	    c.err_total_ptbin4 = h_cutflow->GetBinError(6);
    	    c.err_total_ptbin5 = h_cutflow->GetBinError(7);
    	    c.err_total_etabin1 = h_cutflow->GetBinError(8);
    	    c.err_total_etabin2 = h_cutflow->GetBinError(9);
    	    c.err_total_etabin3 = h_cutflow->GetBinError(10);
    	    c.err_total_etabin4 = h_cutflow->GetBinError(11);
    	    c.err_total_etabin5 = h_cutflow->GetBinError(12);
	    c.err_fidu = h_cutflow->GetBinError(20);
    	    c.err_fidu_ptbin1 = h_cutflow->GetBinError(21);
    	    c.err_fidu_ptbin2 = h_cutflow->GetBinError(22);
    	    c.err_fidu_ptbin3 = h_cutflow->GetBinError(23);
    	    c.err_fidu_ptbin4 = h_cutflow->GetBinError(24);
    	    c.err_fidu_ptbin5 = h_cutflow->GetBinError(25);
    	    c.err_fidu_etabin1 = h_cutflow->GetBinError(26);
    	    c.err_fidu_etabin2 = h_cutflow->GetBinError(27);
    	    c.err_fidu_etabin3 = h_cutflow->GetBinError(28);
    	    c.err_fidu_etabin4 = h_cutflow->GetBinError(29);
    	    c.err_fidu_etabin5 = h_cutflow->GetBinError(30);
	} else {
	    for (int i = 1; i <= h_cutflow->GetNbinsX(); i++) {
		if (string(h_cutflow->GetXaxis()->GetBinLabel(i)) == "CutAll") {
		    c.num_total = h_cutflow->GetBinContent(i);
		    c.err_total = h_cutflow->GetBinError(i);
		    c.num_total_ptbin1 = h_cutflow->GetBinContent(i+1);
		    c.num_total_ptbin2 = h_cutflow->GetBinContent(i+2);
		    c.num_total_ptbin3 = h_cutflow->GetBinContent(i+3);
		    c.num_total_ptbin4 = h_cutflow->GetBinContent(i+4);
		    c.num_total_ptbin5 = h_cutflow->GetBinContent(i+5);
		    c.num_total_etabin1 = h_cutflow->GetBinContent(i+6);
		    c.num_total_etabin2 = h_cutflow->GetBinContent(i+7);
		    c.num_total_etabin3 = h_cutflow->GetBinContent(i+8);
		    c.num_total_etabin4 = h_cutflow->GetBinContent(i+9);
		    c.num_total_etabin5 = h_cutflow->GetBinContent(i+10);
		    c.err_total_ptbin1 = h_cutflow->GetBinError(i+1);
		    c.err_total_ptbin2 = h_cutflow->GetBinError(i+2);
		    c.err_total_ptbin3 = h_cutflow->GetBinError(i+3);
		    c.err_total_ptbin4 = h_cutflow->GetBinError(i+4);
		    c.err_total_ptbin5 = h_cutflow->GetBinError(i+5);
		    c.err_total_etabin1 = h_cutflow->GetBinError(i+6);
		    c.err_total_etabin2 = h_cutflow->GetBinError(i+7);
		    c.err_total_etabin3 = h_cutflow->GetBinError(i+8);
		    c.err_total_etabin4 = h_cutflow->GetBinError(i+9);
		    c.err_total_etabin5 = h_cutflow->GetBinError(i+10);
		}
		if (string(h_cutflow->GetXaxis()->GetBinLabel(i)) == "CutPhMatch") {
		    c.num_fidu = h_cutflow->GetBinContent(i);
		    c.err_fidu = h_cutflow->GetBinError(i);
		    c.num_fidu_ptbin1 = h_cutflow->GetBinContent(i+1);
		    c.num_fidu_ptbin2 = h_cutflow->GetBinContent(i+2);
		    c.num_fidu_ptbin3 = h_cutflow->GetBinContent(i+3);
		    c.num_fidu_ptbin4 = h_cutflow->GetBinContent(i+4);
		    c.num_fidu_ptbin5 = h_cutflow->GetBinContent(i+5);
		    c.num_fidu_etabin1 = h_cutflow->GetBinContent(i+6);
		    c.num_fidu_etabin2 = h_cutflow->GetBinContent(i+7);
		    c.num_fidu_etabin3 = h_cutflow->GetBinContent(i+8);
		    c.num_fidu_etabin4 = h_cutflow->GetBinContent(i+9);
		    c.num_fidu_etabin5 = h_cutflow->GetBinContent(i+10);
		    c.err_fidu_ptbin1 = h_cutflow->GetBinError(i+1);
		    c.err_fidu_ptbin2 = h_cutflow->GetBinError(i+2);
		    c.err_fidu_ptbin3 = h_cutflow->GetBinError(i+3);
		    c.err_fidu_ptbin4 = h_cutflow->GetBinError(i+4);
		    c.err_fidu_ptbin5 = h_cutflow->GetBinError(i+5);
		    c.err_fidu_etabin1 = h_cutflow->GetBinError(i+6);
		    c.err_fidu_etabin2 = h_cutflow->GetBinError(i+7);
		    c.err_fidu_etabin3 = h_cutflow->GetBinError(i+8);
		    c.err_fidu_etabin4 = h_cutflow->GetBinError(i+9);
		    c.err_fidu_etabin5 = h_cutflow->GetBinError(i+10);
		}
	    }
	}
    } else {
	c.num_all = -1;
	c.num_total = -1;
    	c.num_total_ptbin1 = -1;
    	c.num_total_ptbin2 = -1;
    	c.num_total_ptbin3 = -1;
    	c.num_total_ptbin4 = -1;
    	c.num_total_ptbin5 = -1;
    	c.num_total_etabin1 = -1;
    	c.num_total_etabin2 = -1;
    	c.num_total_etabin3 = -1;
    	c.num_total_etabin4 = -1;
    	c.num_total_etabin5 = -1;
	c.num_fidu = -1;
    	c.num_fidu_ptbin1 = -1;
    	c.num_fidu_ptbin2 = -1;
    	c.num_fidu_ptbin3 = -1;
    	c.num_fidu_ptbin4 = -1;
    	c.num_fidu_ptbin5 = -1;
    	c.num_fidu_etabin1 = -1;
    	c.num_fidu_etabin2 = -1;
    	c.num_fidu_etabin3 = -1;
    	c.num_fidu_etabin4 = -1;
    	c.num_fidu_etabin5 = -1;
	c.err_all = -1;
	c.err_total = -1;
    	c.err_total_ptbin1 = -1;
    	c.err_total_ptbin2 = -1;
    	c.err_total_ptbin3 = -1;
    	c.err_total_ptbin4 = -1;
    	c.err_total_ptbin5 = -1;
    	c.err_total_etabin1 = -1;
    	c.err_total_etabin2 = -1;
    	c.err_total_etabin3 = -1;
    	c.err_total_etabin4 = -1;
    	c.err_total_etabin5 = -1;
	c.err_fidu = -1;
    	c.err_fidu_ptbin1 = -1;
    	c.err_fidu_ptbin2 = -1;
    	c.err_fidu_ptbin3 = -1;
    	c.err_fidu_ptbin4 = -1;
    	c.err_fidu_ptbin5 = -1;
    	c.err_fidu_etabin1 = -1;
    	c.err_fidu_etabin2 = -1;
    	c.err_fidu_etabin3 = -1;
    	c.err_fidu_etabin4 = -1;
    	c.err_fidu_etabin5 = -1;
    }

    TH1F* h_iso = (TH1F*)f->Get(hname_ptcone.c_str());
    if (h_iso) {
	if (normiso) {
	    double total = h_iso->Integral();
	    double total_err = 0;
	    for (int i = 1; i <= h_iso->GetNbinsX(); i++) {
		total_err += pow(h_iso->GetBinError(i),2);
	    }
	    total_err = sqrt(total_err);

	    c.num_isobin1 = h_iso->GetBinContent(1) / total;
    	    c.num_isobin2 = h_iso->GetBinContent(2) / total;
    	    c.num_isobin3 = h_iso->GetBinContent(3) / total;
    	    c.num_isobin4 = h_iso->GetBinContent(4) / total;
    	    c.num_isobin5 = h_iso->GetBinContent(5) / total;
	    c.err_isobin1 = GetErrorAoverAB(h_iso->GetBinContent(1), h_iso->GetBinError(1), total, total_err);
	    c.err_isobin2 = GetErrorAoverAB(h_iso->GetBinContent(2), h_iso->GetBinError(2), total, total_err);
	    c.err_isobin3 = GetErrorAoverAB(h_iso->GetBinContent(3), h_iso->GetBinError(3), total, total_err);
	    c.err_isobin4 = GetErrorAoverAB(h_iso->GetBinContent(4), h_iso->GetBinError(4), total, total_err);
	    c.err_isobin5 = GetErrorAoverAB(h_iso->GetBinContent(5), h_iso->GetBinError(5), total, total_err);
	} else {
	    c.num_isobin1 = h_iso->GetBinContent(1);
	    c.num_isobin2 = h_iso->GetBinContent(2);
	    c.num_isobin3 = h_iso->GetBinContent(3);
	    c.num_isobin4 = h_iso->GetBinContent(4);
	    c.num_isobin5 = h_iso->GetBinContent(5);
	    c.err_isobin1 = h_iso->GetBinError(1);
	    c.err_isobin2 = h_iso->GetBinError(2);
	    c.err_isobin3 = h_iso->GetBinError(3);
	    c.err_isobin4 = h_iso->GetBinError(4);
	    c.err_isobin5 = h_iso->GetBinError(5);
	}
    } else {
	c.num_isobin1 = -1;
    	c.num_isobin2 = -1;
    	c.num_isobin3 = -1;
    	c.num_isobin4 = -1;
    	c.num_isobin5 = -1;
	c.err_isobin1 = -1;
    	c.err_isobin2 = -1;
    	c.err_isobin3 = -1;
    	c.err_isobin4 = -1;
    	c.err_isobin5 = -1;
    }

    TH1F* h_iso_ptbin1 = (TH1F*)f->Get(hname_ptbin1_ptcone.c_str());
    if (h_iso_ptbin1) {
	if (normiso) {
	    double total = h_iso_ptbin1->Integral();
	    double total_err = 0;
	    for (int i = 1; i <= h_iso_ptbin1->GetNbinsX(); i++) {
		total_err += pow(h_iso_ptbin1->GetBinError(i),2);
	    }
	    total_err = sqrt(total_err);

	    c.num_ptbin1_isobin1 = h_iso_ptbin1->GetBinContent(1) / total;
    	    c.num_ptbin1_isobin2 = h_iso_ptbin1->GetBinContent(2) / total;
    	    c.num_ptbin1_isobin3 = h_iso_ptbin1->GetBinContent(3) / total;
    	    c.num_ptbin1_isobin4 = h_iso_ptbin1->GetBinContent(4) / total;
    	    c.num_ptbin1_isobin5 = h_iso_ptbin1->GetBinContent(5) / total;
	    c.err_ptbin1_isobin1 = GetErrorAoverAB(h_iso_ptbin1->GetBinContent(1), h_iso_ptbin1->GetBinError(1), total, total_err);
	    c.err_ptbin1_isobin2 = GetErrorAoverAB(h_iso_ptbin1->GetBinContent(2), h_iso_ptbin1->GetBinError(2), total, total_err);
	    c.err_ptbin1_isobin3 = GetErrorAoverAB(h_iso_ptbin1->GetBinContent(3), h_iso_ptbin1->GetBinError(3), total, total_err);
	    c.err_ptbin1_isobin4 = GetErrorAoverAB(h_iso_ptbin1->GetBinContent(4), h_iso_ptbin1->GetBinError(4), total, total_err);
	    c.err_ptbin1_isobin5 = GetErrorAoverAB(h_iso_ptbin1->GetBinContent(5), h_iso_ptbin1->GetBinError(5), total, total_err);
	} else {
	    c.num_ptbin1_isobin1 = h_iso_ptbin1->GetBinContent(1);
	    c.num_ptbin1_isobin2 = h_iso_ptbin1->GetBinContent(2);
	    c.num_ptbin1_isobin3 = h_iso_ptbin1->GetBinContent(3);
	    c.num_ptbin1_isobin4 = h_iso_ptbin1->GetBinContent(4);
	    c.num_ptbin1_isobin5 = h_iso_ptbin1->GetBinContent(5);
	    c.err_ptbin1_isobin1 = h_iso_ptbin1->GetBinError(1);
	    c.err_ptbin1_isobin2 = h_iso_ptbin1->GetBinError(2);
	    c.err_ptbin1_isobin3 = h_iso_ptbin1->GetBinError(3);
	    c.err_ptbin1_isobin4 = h_iso_ptbin1->GetBinError(4);
	    c.err_ptbin1_isobin5 = h_iso_ptbin1->GetBinError(5);
	}
    } else {
	c.num_ptbin1_isobin1 = -1;
    	c.num_ptbin1_isobin2 = -1;
    	c.num_ptbin1_isobin3 = -1;
    	c.num_ptbin1_isobin4 = -1;
    	c.num_ptbin1_isobin5 = -1;
	c.err_ptbin1_isobin1 = -1;
    	c.err_ptbin1_isobin2 = -1;
    	c.err_ptbin1_isobin3 = -1;
    	c.err_ptbin1_isobin4 = -1;
    	c.err_ptbin1_isobin5 = -1;
    }

    TH1F* h_iso_ptbin2 = (TH1F*)f->Get(hname_ptbin2_ptcone.c_str());
    if (h_iso_ptbin2) {
	if (normiso) {
	    double total = h_iso_ptbin2->Integral();
	    double total_err = 0;
	    for (int i = 1; i <= h_iso_ptbin2->GetNbinsX(); i++) {
		total_err += pow(h_iso_ptbin2->GetBinError(i),2);
	    }
	    total_err = sqrt(total_err);

	    c.num_ptbin2_isobin1 = h_iso_ptbin2->GetBinContent(1) / total;
    	    c.num_ptbin2_isobin2 = h_iso_ptbin2->GetBinContent(2) / total;
    	    c.num_ptbin2_isobin3 = h_iso_ptbin2->GetBinContent(3) / total;
    	    c.num_ptbin2_isobin4 = h_iso_ptbin2->GetBinContent(4) / total;
    	    c.num_ptbin2_isobin5 = h_iso_ptbin2->GetBinContent(5) / total;
	    c.err_ptbin2_isobin1 = GetErrorAoverAB(h_iso_ptbin2->GetBinContent(1), h_iso_ptbin2->GetBinError(1), total, total_err);
	    c.err_ptbin2_isobin2 = GetErrorAoverAB(h_iso_ptbin2->GetBinContent(2), h_iso_ptbin2->GetBinError(2), total, total_err);
	    c.err_ptbin2_isobin3 = GetErrorAoverAB(h_iso_ptbin2->GetBinContent(3), h_iso_ptbin2->GetBinError(3), total, total_err);
	    c.err_ptbin2_isobin4 = GetErrorAoverAB(h_iso_ptbin2->GetBinContent(4), h_iso_ptbin2->GetBinError(4), total, total_err);
	    c.err_ptbin2_isobin5 = GetErrorAoverAB(h_iso_ptbin2->GetBinContent(5), h_iso_ptbin2->GetBinError(5), total, total_err);
	} else {
	    c.num_ptbin2_isobin1 = h_iso_ptbin2->GetBinContent(1);
	    c.num_ptbin2_isobin2 = h_iso_ptbin2->GetBinContent(2);
	    c.num_ptbin2_isobin3 = h_iso_ptbin2->GetBinContent(3);
	    c.num_ptbin2_isobin4 = h_iso_ptbin2->GetBinContent(4);
	    c.num_ptbin2_isobin5 = h_iso_ptbin2->GetBinContent(5);
	    c.err_ptbin2_isobin1 = h_iso_ptbin2->GetBinError(1);
	    c.err_ptbin2_isobin2 = h_iso_ptbin2->GetBinError(2);
	    c.err_ptbin2_isobin3 = h_iso_ptbin2->GetBinError(3);
	    c.err_ptbin2_isobin4 = h_iso_ptbin2->GetBinError(4);
	    c.err_ptbin2_isobin5 = h_iso_ptbin2->GetBinError(5);
	}
    } else {
	c.num_ptbin2_isobin1 = -1;
    	c.num_ptbin2_isobin2 = -1;
    	c.num_ptbin2_isobin3 = -1;
    	c.num_ptbin2_isobin4 = -1;
    	c.num_ptbin2_isobin5 = -1;
	c.err_ptbin2_isobin1 = -1;
    	c.err_ptbin2_isobin2 = -1;
    	c.err_ptbin2_isobin3 = -1;
    	c.err_ptbin2_isobin4 = -1;
    	c.err_ptbin2_isobin5 = -1;
    }

    TH1F* h_iso_ptbin3 = (TH1F*)f->Get(hname_ptbin3_ptcone.c_str());
    if (h_iso_ptbin3) {
	if (normiso) {
	    double total = h_iso_ptbin3->Integral();
	    double total_err = 0;
	    for (int i = 1; i <= h_iso_ptbin3->GetNbinsX(); i++) {
		total_err += pow(h_iso_ptbin3->GetBinError(i),2);
	    }
	    total_err = sqrt(total_err);

	    c.num_ptbin3_isobin1 = h_iso_ptbin3->GetBinContent(1) / total;
    	    c.num_ptbin3_isobin2 = h_iso_ptbin3->GetBinContent(2) / total;
    	    c.num_ptbin3_isobin3 = h_iso_ptbin3->GetBinContent(3) / total;
    	    c.num_ptbin3_isobin4 = h_iso_ptbin3->GetBinContent(4) / total;
    	    c.num_ptbin3_isobin5 = h_iso_ptbin3->GetBinContent(5) / total;
	    c.err_ptbin3_isobin1 = GetErrorAoverAB(h_iso_ptbin3->GetBinContent(1), h_iso_ptbin3->GetBinError(1), total, total_err);
	    c.err_ptbin3_isobin2 = GetErrorAoverAB(h_iso_ptbin3->GetBinContent(2), h_iso_ptbin3->GetBinError(2), total, total_err);
	    c.err_ptbin3_isobin3 = GetErrorAoverAB(h_iso_ptbin3->GetBinContent(3), h_iso_ptbin3->GetBinError(3), total, total_err);
	    c.err_ptbin3_isobin4 = GetErrorAoverAB(h_iso_ptbin3->GetBinContent(4), h_iso_ptbin3->GetBinError(4), total, total_err);
	    c.err_ptbin3_isobin5 = GetErrorAoverAB(h_iso_ptbin3->GetBinContent(5), h_iso_ptbin3->GetBinError(5), total, total_err);
	} else {
	    c.num_ptbin3_isobin1 = h_iso_ptbin3->GetBinContent(1);
	    c.num_ptbin3_isobin2 = h_iso_ptbin3->GetBinContent(2);
	    c.num_ptbin3_isobin3 = h_iso_ptbin3->GetBinContent(3);
	    c.num_ptbin3_isobin4 = h_iso_ptbin3->GetBinContent(4);
	    c.num_ptbin3_isobin5 = h_iso_ptbin3->GetBinContent(5);
	    c.err_ptbin3_isobin1 = h_iso_ptbin3->GetBinError(1);
	    c.err_ptbin3_isobin2 = h_iso_ptbin3->GetBinError(2);
	    c.err_ptbin3_isobin3 = h_iso_ptbin3->GetBinError(3);
	    c.err_ptbin3_isobin4 = h_iso_ptbin3->GetBinError(4);
	    c.err_ptbin3_isobin5 = h_iso_ptbin3->GetBinError(5);
	}
    } else {
	c.num_ptbin3_isobin1 = -1;
    	c.num_ptbin3_isobin2 = -1;
    	c.num_ptbin3_isobin3 = -1;
    	c.num_ptbin3_isobin4 = -1;
    	c.num_ptbin3_isobin5 = -1;
	c.err_ptbin3_isobin1 = -1;
    	c.err_ptbin3_isobin2 = -1;
    	c.err_ptbin3_isobin3 = -1;
    	c.err_ptbin3_isobin4 = -1;
    	c.err_ptbin3_isobin5 = -1;
    }

    TH1F* h_iso_ptbin4 = (TH1F*)f->Get(hname_ptbin4_ptcone.c_str());
    if (h_iso_ptbin4) {
	if (normiso) {
	    double total = h_iso_ptbin4->Integral();
	    double total_err = 0;
	    for (int i = 1; i <= h_iso_ptbin4->GetNbinsX(); i++) {
		total_err += pow(h_iso_ptbin4->GetBinError(i),2);
	    }
	    total_err = sqrt(total_err);

	    c.num_ptbin4_isobin1 = h_iso_ptbin4->GetBinContent(1) / total;
    	    c.num_ptbin4_isobin2 = h_iso_ptbin4->GetBinContent(2) / total;
    	    c.num_ptbin4_isobin3 = h_iso_ptbin4->GetBinContent(3) / total;
    	    c.num_ptbin4_isobin4 = h_iso_ptbin4->GetBinContent(4) / total;
    	    c.num_ptbin4_isobin5 = h_iso_ptbin4->GetBinContent(5) / total;
	    c.err_ptbin4_isobin1 = GetErrorAoverAB(h_iso_ptbin4->GetBinContent(1), h_iso_ptbin4->GetBinError(1), total, total_err);
	    c.err_ptbin4_isobin2 = GetErrorAoverAB(h_iso_ptbin4->GetBinContent(2), h_iso_ptbin4->GetBinError(2), total, total_err);
	    c.err_ptbin4_isobin3 = GetErrorAoverAB(h_iso_ptbin4->GetBinContent(3), h_iso_ptbin4->GetBinError(3), total, total_err);
	    c.err_ptbin4_isobin4 = GetErrorAoverAB(h_iso_ptbin4->GetBinContent(4), h_iso_ptbin4->GetBinError(4), total, total_err);
	    c.err_ptbin4_isobin5 = GetErrorAoverAB(h_iso_ptbin4->GetBinContent(5), h_iso_ptbin4->GetBinError(5), total, total_err);
	} else {
	    c.num_ptbin4_isobin1 = h_iso_ptbin4->GetBinContent(1);
	    c.num_ptbin4_isobin2 = h_iso_ptbin4->GetBinContent(2);
	    c.num_ptbin4_isobin3 = h_iso_ptbin4->GetBinContent(3);
	    c.num_ptbin4_isobin4 = h_iso_ptbin4->GetBinContent(4);
	    c.num_ptbin4_isobin5 = h_iso_ptbin4->GetBinContent(5);
	    c.err_ptbin4_isobin1 = h_iso_ptbin4->GetBinError(1);
	    c.err_ptbin4_isobin2 = h_iso_ptbin4->GetBinError(2);
	    c.err_ptbin4_isobin3 = h_iso_ptbin4->GetBinError(3);
	    c.err_ptbin4_isobin4 = h_iso_ptbin4->GetBinError(4);
	    c.err_ptbin4_isobin5 = h_iso_ptbin4->GetBinError(5);
	}
    } else {
	c.num_ptbin4_isobin1 = -1;
    	c.num_ptbin4_isobin2 = -1;
    	c.num_ptbin4_isobin3 = -1;
    	c.num_ptbin4_isobin4 = -1;
    	c.num_ptbin4_isobin5 = -1;
	c.err_ptbin4_isobin1 = -1;
    	c.err_ptbin4_isobin2 = -1;
    	c.err_ptbin4_isobin3 = -1;
    	c.err_ptbin4_isobin4 = -1;
    	c.err_ptbin4_isobin5 = -1;
    }

    TH1F* h_iso_ptbin5 = (TH1F*)f->Get(hname_ptbin5_ptcone.c_str());
    if (h_iso_ptbin5) {
	if (normiso) {
	    double total = h_iso_ptbin5->Integral();
	    double total_err = 0;
	    for (int i = 1; i <= h_iso_ptbin5->GetNbinsX(); i++) {
		total_err += pow(h_iso_ptbin5->GetBinError(i),2);
	    }
	    total_err = sqrt(total_err);

	    c.num_ptbin5_isobin1 = h_iso_ptbin5->GetBinContent(1) / total;
    	    c.num_ptbin5_isobin2 = h_iso_ptbin5->GetBinContent(2) / total;
    	    c.num_ptbin5_isobin3 = h_iso_ptbin5->GetBinContent(3) / total;
    	    c.num_ptbin5_isobin4 = h_iso_ptbin5->GetBinContent(4) / total;
    	    c.num_ptbin5_isobin5 = h_iso_ptbin5->GetBinContent(5) / total;
	    c.err_ptbin5_isobin1 = GetErrorAoverAB(h_iso_ptbin5->GetBinContent(1), h_iso_ptbin5->GetBinError(1), total, total_err);
	    c.err_ptbin5_isobin2 = GetErrorAoverAB(h_iso_ptbin5->GetBinContent(2), h_iso_ptbin5->GetBinError(2), total, total_err);
	    c.err_ptbin5_isobin3 = GetErrorAoverAB(h_iso_ptbin5->GetBinContent(3), h_iso_ptbin5->GetBinError(3), total, total_err);
	    c.err_ptbin5_isobin4 = GetErrorAoverAB(h_iso_ptbin5->GetBinContent(4), h_iso_ptbin5->GetBinError(4), total, total_err);
	    c.err_ptbin5_isobin5 = GetErrorAoverAB(h_iso_ptbin5->GetBinContent(5), h_iso_ptbin5->GetBinError(5), total, total_err);
	} else {
	    c.num_ptbin5_isobin1 = h_iso_ptbin5->GetBinContent(1);
	    c.num_ptbin5_isobin2 = h_iso_ptbin5->GetBinContent(2);
	    c.num_ptbin5_isobin3 = h_iso_ptbin5->GetBinContent(3);
	    c.num_ptbin5_isobin4 = h_iso_ptbin5->GetBinContent(4);
	    c.num_ptbin5_isobin5 = h_iso_ptbin5->GetBinContent(5);
	    c.err_ptbin5_isobin1 = h_iso_ptbin5->GetBinError(1);
	    c.err_ptbin5_isobin2 = h_iso_ptbin5->GetBinError(2);
	    c.err_ptbin5_isobin3 = h_iso_ptbin5->GetBinError(3);
	    c.err_ptbin5_isobin4 = h_iso_ptbin5->GetBinError(4);
	    c.err_ptbin5_isobin5 = h_iso_ptbin5->GetBinError(5);
	}
    } else {
	c.num_ptbin5_isobin1 = -1;
    	c.num_ptbin5_isobin2 = -1;
    	c.num_ptbin5_isobin3 = -1;
    	c.num_ptbin5_isobin4 = -1;
    	c.num_ptbin5_isobin5 = -1;
	c.err_ptbin5_isobin1 = -1;
    	c.err_ptbin5_isobin2 = -1;
    	c.err_ptbin5_isobin3 = -1;
    	c.err_ptbin5_isobin4 = -1;
    	c.err_ptbin5_isobin5 = -1;
    }

    TH1F* h_iso_etabin1 = (TH1F*)f->Get(hname_etabin1_ptcone.c_str());
    if (h_iso_etabin1) {
	if (normiso) {
	    double total = h_iso_etabin1->Integral();
	    double total_err = 0;
	    for (int i = 1; i <= h_iso_etabin1->GetNbinsX(); i++) {
		total_err += pow(h_iso_etabin1->GetBinError(i),2);
	    }
	    total_err = sqrt(total_err);

	    c.num_etabin1_isobin1 = h_iso_etabin1->GetBinContent(1) / total;
    	    c.num_etabin1_isobin2 = h_iso_etabin1->GetBinContent(2) / total;
    	    c.num_etabin1_isobin3 = h_iso_etabin1->GetBinContent(3) / total;
    	    c.num_etabin1_isobin4 = h_iso_etabin1->GetBinContent(4) / total;
    	    c.num_etabin1_isobin5 = h_iso_etabin1->GetBinContent(5) / total;
	    c.err_etabin1_isobin1 = GetErrorAoverAB(h_iso_etabin1->GetBinContent(1), h_iso_etabin1->GetBinError(1), total, total_err);
	    c.err_etabin1_isobin2 = GetErrorAoverAB(h_iso_etabin1->GetBinContent(2), h_iso_etabin1->GetBinError(2), total, total_err);
	    c.err_etabin1_isobin3 = GetErrorAoverAB(h_iso_etabin1->GetBinContent(3), h_iso_etabin1->GetBinError(3), total, total_err);
	    c.err_etabin1_isobin4 = GetErrorAoverAB(h_iso_etabin1->GetBinContent(4), h_iso_etabin1->GetBinError(4), total, total_err);
	    c.err_etabin1_isobin5 = GetErrorAoverAB(h_iso_etabin1->GetBinContent(5), h_iso_etabin1->GetBinError(5), total, total_err);
	} else {
	    c.num_etabin1_isobin1 = h_iso_etabin1->GetBinContent(1);
	    c.num_etabin1_isobin2 = h_iso_etabin1->GetBinContent(2);
	    c.num_etabin1_isobin3 = h_iso_etabin1->GetBinContent(3);
	    c.num_etabin1_isobin4 = h_iso_etabin1->GetBinContent(4);
	    c.num_etabin1_isobin5 = h_iso_etabin1->GetBinContent(5);
	    c.err_etabin1_isobin1 = h_iso_etabin1->GetBinError(1);
	    c.err_etabin1_isobin2 = h_iso_etabin1->GetBinError(2);
	    c.err_etabin1_isobin3 = h_iso_etabin1->GetBinError(3);
	    c.err_etabin1_isobin4 = h_iso_etabin1->GetBinError(4);
	    c.err_etabin1_isobin5 = h_iso_etabin1->GetBinError(5);
	}
    } else {
	c.num_etabin1_isobin1 = -1;
    	c.num_etabin1_isobin2 = -1;
    	c.num_etabin1_isobin3 = -1;
    	c.num_etabin1_isobin4 = -1;
    	c.num_etabin1_isobin5 = -1;
	c.err_etabin1_isobin1 = -1;
    	c.err_etabin1_isobin2 = -1;
    	c.err_etabin1_isobin3 = -1;
    	c.err_etabin1_isobin4 = -1;
    	c.err_etabin1_isobin5 = -1;
    }

    TH1F* h_iso_etabin2 = (TH1F*)f->Get(hname_etabin2_ptcone.c_str());
    if (h_iso_etabin2) {
	if (normiso) {
	    double total = h_iso_etabin2->Integral();
	    double total_err = 0;
	    for (int i = 1; i <= h_iso_etabin2->GetNbinsX(); i++) {
		total_err += pow(h_iso_etabin2->GetBinError(i),2);
	    }
	    total_err = sqrt(total_err);

	    c.num_etabin2_isobin1 = h_iso_etabin2->GetBinContent(1) / total;
    	    c.num_etabin2_isobin2 = h_iso_etabin2->GetBinContent(2) / total;
    	    c.num_etabin2_isobin3 = h_iso_etabin2->GetBinContent(3) / total;
    	    c.num_etabin2_isobin4 = h_iso_etabin2->GetBinContent(4) / total;
    	    c.num_etabin2_isobin5 = h_iso_etabin2->GetBinContent(5) / total;
	    c.err_etabin2_isobin1 = GetErrorAoverAB(h_iso_etabin2->GetBinContent(1), h_iso_etabin2->GetBinError(1), total, total_err);
	    c.err_etabin2_isobin2 = GetErrorAoverAB(h_iso_etabin2->GetBinContent(2), h_iso_etabin2->GetBinError(2), total, total_err);
	    c.err_etabin2_isobin3 = GetErrorAoverAB(h_iso_etabin2->GetBinContent(3), h_iso_etabin2->GetBinError(3), total, total_err);
	    c.err_etabin2_isobin4 = GetErrorAoverAB(h_iso_etabin2->GetBinContent(4), h_iso_etabin2->GetBinError(4), total, total_err);
	    c.err_etabin2_isobin5 = GetErrorAoverAB(h_iso_etabin2->GetBinContent(5), h_iso_etabin2->GetBinError(5), total, total_err);
	} else {
	    c.num_etabin2_isobin1 = h_iso_etabin2->GetBinContent(1);
	    c.num_etabin2_isobin2 = h_iso_etabin2->GetBinContent(2);
	    c.num_etabin2_isobin3 = h_iso_etabin2->GetBinContent(3);
	    c.num_etabin2_isobin4 = h_iso_etabin2->GetBinContent(4);
	    c.num_etabin2_isobin5 = h_iso_etabin2->GetBinContent(5);
	    c.err_etabin2_isobin1 = h_iso_etabin2->GetBinError(1);
	    c.err_etabin2_isobin2 = h_iso_etabin2->GetBinError(2);
	    c.err_etabin2_isobin3 = h_iso_etabin2->GetBinError(3);
	    c.err_etabin2_isobin4 = h_iso_etabin2->GetBinError(4);
	    c.err_etabin2_isobin5 = h_iso_etabin2->GetBinError(5);
	}
    } else {
	c.num_etabin2_isobin1 = -1;
    	c.num_etabin2_isobin2 = -1;
    	c.num_etabin2_isobin3 = -1;
    	c.num_etabin2_isobin4 = -1;
    	c.num_etabin2_isobin5 = -1;
	c.err_etabin2_isobin1 = -1;
    	c.err_etabin2_isobin2 = -1;
    	c.err_etabin2_isobin3 = -1;
    	c.err_etabin2_isobin4 = -1;
    	c.err_etabin2_isobin5 = -1;
    }

    TH1F* h_iso_etabin3 = (TH1F*)f->Get(hname_etabin3_ptcone.c_str());
    if (h_iso_etabin3) {
	if (normiso) {
	    double total = h_iso_etabin3->Integral();
	    double total_err = 0;
	    for (int i = 1; i <= h_iso_etabin3->GetNbinsX(); i++) {
		total_err += pow(h_iso_etabin3->GetBinError(i),2);
	    }
	    total_err = sqrt(total_err);

	    c.num_etabin3_isobin1 = h_iso_etabin3->GetBinContent(1) / total;
    	    c.num_etabin3_isobin2 = h_iso_etabin3->GetBinContent(2) / total;
    	    c.num_etabin3_isobin3 = h_iso_etabin3->GetBinContent(3) / total;
    	    c.num_etabin3_isobin4 = h_iso_etabin3->GetBinContent(4) / total;
    	    c.num_etabin3_isobin5 = h_iso_etabin3->GetBinContent(5) / total;
	    c.err_etabin3_isobin1 = GetErrorAoverAB(h_iso_etabin3->GetBinContent(1), h_iso_etabin3->GetBinError(1), total, total_err);
	    c.err_etabin3_isobin2 = GetErrorAoverAB(h_iso_etabin3->GetBinContent(2), h_iso_etabin3->GetBinError(2), total, total_err);
	    c.err_etabin3_isobin3 = GetErrorAoverAB(h_iso_etabin3->GetBinContent(3), h_iso_etabin3->GetBinError(3), total, total_err);
	    c.err_etabin3_isobin4 = GetErrorAoverAB(h_iso_etabin3->GetBinContent(4), h_iso_etabin3->GetBinError(4), total, total_err);
	    c.err_etabin3_isobin5 = GetErrorAoverAB(h_iso_etabin3->GetBinContent(5), h_iso_etabin3->GetBinError(5), total, total_err);
	} else {
	    c.num_etabin3_isobin1 = h_iso_etabin3->GetBinContent(1);
	    c.num_etabin3_isobin2 = h_iso_etabin3->GetBinContent(2);
	    c.num_etabin3_isobin3 = h_iso_etabin3->GetBinContent(3);
	    c.num_etabin3_isobin4 = h_iso_etabin3->GetBinContent(4);
	    c.num_etabin3_isobin5 = h_iso_etabin3->GetBinContent(5);
	    c.err_etabin3_isobin1 = h_iso_etabin3->GetBinError(1);
	    c.err_etabin3_isobin2 = h_iso_etabin3->GetBinError(2);
	    c.err_etabin3_isobin3 = h_iso_etabin3->GetBinError(3);
	    c.err_etabin3_isobin4 = h_iso_etabin3->GetBinError(4);
	    c.err_etabin3_isobin5 = h_iso_etabin3->GetBinError(5);
	}
    } else {
	c.num_etabin3_isobin1 = -1;
    	c.num_etabin3_isobin2 = -1;
    	c.num_etabin3_isobin3 = -1;
    	c.num_etabin3_isobin4 = -1;
    	c.num_etabin3_isobin5 = -1;
	c.err_etabin3_isobin1 = -1;
    	c.err_etabin3_isobin2 = -1;
    	c.err_etabin3_isobin3 = -1;
    	c.err_etabin3_isobin4 = -1;
    	c.err_etabin3_isobin5 = -1;
    }

    TH1F* h_iso_etabin4 = (TH1F*)f->Get(hname_etabin4_ptcone.c_str());
    if (h_iso_etabin4) {
	if (normiso) {
	    double total = h_iso_etabin4->Integral();
	    double total_err = 0;
	    for (int i = 1; i <= h_iso_etabin4->GetNbinsX(); i++) {
		total_err += pow(h_iso_etabin4->GetBinError(i),2);
	    }
	    total_err = sqrt(total_err);

	    c.num_etabin4_isobin1 = h_iso_etabin4->GetBinContent(1) / total;
    	    c.num_etabin4_isobin2 = h_iso_etabin4->GetBinContent(2) / total;
    	    c.num_etabin4_isobin3 = h_iso_etabin4->GetBinContent(3) / total;
    	    c.num_etabin4_isobin4 = h_iso_etabin4->GetBinContent(4) / total;
    	    c.num_etabin4_isobin5 = h_iso_etabin4->GetBinContent(5) / total;
	    c.err_etabin4_isobin1 = GetErrorAoverAB(h_iso_etabin4->GetBinContent(1), h_iso_etabin4->GetBinError(1), total, total_err);
	    c.err_etabin4_isobin2 = GetErrorAoverAB(h_iso_etabin4->GetBinContent(2), h_iso_etabin4->GetBinError(2), total, total_err);
	    c.err_etabin4_isobin3 = GetErrorAoverAB(h_iso_etabin4->GetBinContent(3), h_iso_etabin4->GetBinError(3), total, total_err);
	    c.err_etabin4_isobin4 = GetErrorAoverAB(h_iso_etabin4->GetBinContent(4), h_iso_etabin4->GetBinError(4), total, total_err);
	    c.err_etabin4_isobin5 = GetErrorAoverAB(h_iso_etabin4->GetBinContent(5), h_iso_etabin4->GetBinError(5), total, total_err);
	} else {
	    c.num_etabin4_isobin1 = h_iso_etabin4->GetBinContent(1);
	    c.num_etabin4_isobin2 = h_iso_etabin4->GetBinContent(2);
	    c.num_etabin4_isobin3 = h_iso_etabin4->GetBinContent(3);
	    c.num_etabin4_isobin4 = h_iso_etabin4->GetBinContent(4);
	    c.num_etabin4_isobin5 = h_iso_etabin4->GetBinContent(5);
	    c.err_etabin4_isobin1 = h_iso_etabin4->GetBinError(1);
	    c.err_etabin4_isobin2 = h_iso_etabin4->GetBinError(2);
	    c.err_etabin4_isobin3 = h_iso_etabin4->GetBinError(3);
	    c.err_etabin4_isobin4 = h_iso_etabin4->GetBinError(4);
	    c.err_etabin4_isobin5 = h_iso_etabin4->GetBinError(5);
	}
    } else {
	c.num_etabin4_isobin1 = -1;
    	c.num_etabin4_isobin2 = -1;
    	c.num_etabin4_isobin3 = -1;
    	c.num_etabin4_isobin4 = -1;
    	c.num_etabin4_isobin5 = -1;
	c.err_etabin4_isobin1 = -1;
    	c.err_etabin4_isobin2 = -1;
    	c.err_etabin4_isobin3 = -1;
    	c.err_etabin4_isobin4 = -1;
    	c.err_etabin4_isobin5 = -1;
    }

    TH1F* h_iso_etabin5 = (TH1F*)f->Get(hname_etabin5_ptcone.c_str());
    if (h_iso_etabin5) {
	if (normiso) {
	    double total = h_iso_etabin5->Integral();
	    double total_err = 0;
	    for (int i = 1; i <= h_iso_etabin5->GetNbinsX(); i++) {
		total_err += pow(h_iso_etabin5->GetBinError(i),2);
	    }
	    total_err = sqrt(total_err);

	    c.num_etabin5_isobin1 = h_iso_etabin5->GetBinContent(1) / total;
    	    c.num_etabin5_isobin2 = h_iso_etabin5->GetBinContent(2) / total;
    	    c.num_etabin5_isobin3 = h_iso_etabin5->GetBinContent(3) / total;
    	    c.num_etabin5_isobin4 = h_iso_etabin5->GetBinContent(4) / total;
    	    c.num_etabin5_isobin5 = h_iso_etabin5->GetBinContent(5) / total;
	    c.err_etabin5_isobin1 = GetErrorAoverAB(h_iso_etabin5->GetBinContent(1), h_iso_etabin5->GetBinError(1), total, total_err);
	    c.err_etabin5_isobin2 = GetErrorAoverAB(h_iso_etabin5->GetBinContent(2), h_iso_etabin5->GetBinError(2), total, total_err);
	    c.err_etabin5_isobin3 = GetErrorAoverAB(h_iso_etabin5->GetBinContent(3), h_iso_etabin5->GetBinError(3), total, total_err);
	    c.err_etabin5_isobin4 = GetErrorAoverAB(h_iso_etabin5->GetBinContent(4), h_iso_etabin5->GetBinError(4), total, total_err);
	    c.err_etabin5_isobin5 = GetErrorAoverAB(h_iso_etabin5->GetBinContent(5), h_iso_etabin5->GetBinError(5), total, total_err);
	} else {
	    c.num_etabin5_isobin1 = h_iso_etabin5->GetBinContent(1);
	    c.num_etabin5_isobin2 = h_iso_etabin5->GetBinContent(2);
	    c.num_etabin5_isobin3 = h_iso_etabin5->GetBinContent(3);
	    c.num_etabin5_isobin4 = h_iso_etabin5->GetBinContent(4);
	    c.num_etabin5_isobin5 = h_iso_etabin5->GetBinContent(5);
	    c.err_etabin5_isobin1 = h_iso_etabin5->GetBinError(1);
	    c.err_etabin5_isobin2 = h_iso_etabin5->GetBinError(2);
	    c.err_etabin5_isobin3 = h_iso_etabin5->GetBinError(3);
	    c.err_etabin5_isobin4 = h_iso_etabin5->GetBinError(4);
	    c.err_etabin5_isobin5 = h_iso_etabin5->GetBinError(5);
	}
    } else {
	c.num_etabin5_isobin1 = -1;
    	c.num_etabin5_isobin2 = -1;
    	c.num_etabin5_isobin3 = -1;
    	c.num_etabin5_isobin4 = -1;
    	c.num_etabin5_isobin5 = -1;
	c.err_etabin5_isobin1 = -1;
    	c.err_etabin5_isobin2 = -1;
    	c.err_etabin5_isobin3 = -1;
    	c.err_etabin5_isobin4 = -1;
    	c.err_etabin5_isobin5 = -1;
    }

    f->Close();
    delete f;
    return c;
}

void PrintBanner(string title, int type) {
    cout << setw(40) << title << " &";
    if (type == 1) {
        cout << setw(30) << "Inclusive" << " &";
        cout << setw(30) << "$15  \\leq p_T < 25$" << " &";
        cout << setw(30) << "$25  \\leq p_T < 40$" << " &";
        cout << setw(30) << "$40  \\leq p_T < 60$" << " &";
        cout << setw(30) << "$60  \\leq p_T < 100$" << " &";
        cout << setw(30) << "$100 \\leq p_T$" << " \\\\";
    } else if (type == 2) {
        cout << setw(30) << "Inclusive" << " &";
	cout << setw(30) << "0 $\\leq |\\eta| <$ 0.25" << " &"
	     << setw(30) << "0.25 $\\leq |\\eta| <$ 0.55" << " &"
	     << setw(30) << "0.55 $\\leq |\\eta| <$ 0.90" << " &"
	     << setw(30) << "0.90 $\\leq |\\eta| <$ 1.37" << " &"
	     << setw(30) << "1.37 $\\leq |\\eta| <$ 2.37" << " \\\\";
    } else {
        cout << setw(30) << "$0  \\leq p_T^{iso} < 1$" << " &";
        cout << setw(30) << "$1  \\leq p_T^{iso} < 3$" << " &";
        cout << setw(30) << "$3  \\leq p_T^{iso} < 5$" << " &";
        cout << setw(30) << "$5  \\leq p_T^{iso} < 10$" << " &";
        cout << setw(30) << "$10 \\leq p_T^{iso}$" << " \\\\";
    }
    cout << endl;
}

Counter GetCounterRelDiff(Counter &a, Counter &b, double rho, bool smooth, bool draw, string name) { // rho is correlation coefficient
    Counter c;
    c.num_all = (a.num_all - b.num_all) / b.num_all;
    c.err_all = sqrt(pow(a.err_all/b.num_all, 2) + pow(a.num_all*b.err_all/pow(b.num_all,2), 2) + 2*rho*(1/b.num_all)*(-1*a.num_all/pow(b.num_all,2))*a.err_all*b.err_all);
    c.num_total = (a.num_total - b.num_total) / b.num_total;
    c.err_total = sqrt(pow(a.err_total/b.num_total, 2) + pow(a.num_total*b.err_total/pow(b.num_total,2), 2) + 2*rho*(1/b.num_total)*(-1*a.num_total/pow(b.num_total,2))*a.err_total*b.err_total);
    c.num_total_ptbin1 = (a.num_total_ptbin1 - b.num_total_ptbin1) / b.num_total_ptbin1;
    c.err_total_ptbin1 = sqrt(pow(a.err_total_ptbin1/b.num_total_ptbin1, 2) + pow(a.num_total_ptbin1*b.err_total_ptbin1/pow(b.num_total_ptbin1,2), 2) + 2*rho*(1/b.num_total_ptbin1)*(-1*a.num_total_ptbin1/pow(b.num_total_ptbin1,2))*a.err_total_ptbin1*b.err_total_ptbin1);
    c.num_total_ptbin2 = (a.num_total_ptbin2 - b.num_total_ptbin2) / b.num_total_ptbin2;
    c.err_total_ptbin2 = sqrt(pow(a.err_total_ptbin2/b.num_total_ptbin2, 2) + pow(a.num_total_ptbin2*b.err_total_ptbin2/pow(b.num_total_ptbin2,2), 2) + 2*rho*(1/b.num_total_ptbin2)*(-1*a.num_total_ptbin2/pow(b.num_total_ptbin2,2))*a.err_total_ptbin2*b.err_total_ptbin2);
    c.num_total_ptbin3 = (a.num_total_ptbin3 - b.num_total_ptbin3) / b.num_total_ptbin3;
    c.err_total_ptbin3 = sqrt(pow(a.err_total_ptbin3/b.num_total_ptbin3, 2) + pow(a.num_total_ptbin3*b.err_total_ptbin3/pow(b.num_total_ptbin3,2), 2) + 2*rho*(1/b.num_total_ptbin3)*(-1*a.num_total_ptbin3/pow(b.num_total_ptbin3,2))*a.err_total_ptbin3*b.err_total_ptbin3);
    c.num_total_ptbin4 = (a.num_total_ptbin4 - b.num_total_ptbin4) / b.num_total_ptbin4;
    c.err_total_ptbin4 = sqrt(pow(a.err_total_ptbin4/b.num_total_ptbin4, 2) + pow(a.num_total_ptbin4*b.err_total_ptbin4/pow(b.num_total_ptbin4,2), 2) + 2*rho*(1/b.num_total_ptbin4)*(-1*a.num_total_ptbin4/pow(b.num_total_ptbin4,2))*a.err_total_ptbin4*b.err_total_ptbin4);
    c.num_total_ptbin5 = (a.num_total_ptbin5 - b.num_total_ptbin5) / b.num_total_ptbin5;
    c.err_total_ptbin5 = sqrt(pow(a.err_total_ptbin5/b.num_total_ptbin5, 2) + pow(a.num_total_ptbin5*b.err_total_ptbin5/pow(b.num_total_ptbin5,2), 2) + 2*rho*(1/b.num_total_ptbin5)*(-1*a.num_total_ptbin5/pow(b.num_total_ptbin5,2))*a.err_total_ptbin5*b.err_total_ptbin5);
    c.num_total_etabin1 = (a.num_total_etabin1 - b.num_total_etabin1) / b.num_total_etabin1;
    c.err_total_etabin1 = sqrt(pow(a.err_total_etabin1/b.num_total_etabin1, 2) + pow(a.num_total_etabin1*b.err_total_etabin1/pow(b.num_total_etabin1,2), 2) + 2*rho*(1/b.num_total_etabin1)*(-1*a.num_total_etabin1/pow(b.num_total_etabin1,2))*a.err_total_etabin1*b.err_total_etabin1);
    c.num_total_etabin2 = (a.num_total_etabin2 - b.num_total_etabin2) / b.num_total_etabin2;
    c.err_total_etabin2 = sqrt(pow(a.err_total_etabin2/b.num_total_etabin2, 2) + pow(a.num_total_etabin2*b.err_total_etabin2/pow(b.num_total_etabin2,2), 2) + 2*rho*(1/b.num_total_etabin2)*(-1*a.num_total_etabin2/pow(b.num_total_etabin2,2))*a.err_total_etabin2*b.err_total_etabin2);
    c.num_total_etabin3 = (a.num_total_etabin3 - b.num_total_etabin3) / b.num_total_etabin3;
    c.err_total_etabin3 = sqrt(pow(a.err_total_etabin3/b.num_total_etabin3, 2) + pow(a.num_total_etabin3*b.err_total_etabin3/pow(b.num_total_etabin3,2), 2) + 2*rho*(1/b.num_total_etabin3)*(-1*a.num_total_etabin3/pow(b.num_total_etabin3,2))*a.err_total_etabin3*b.err_total_etabin3);
    c.num_total_etabin4 = (a.num_total_etabin4 - b.num_total_etabin4) / b.num_total_etabin4;
    c.err_total_etabin4 = sqrt(pow(a.err_total_etabin4/b.num_total_etabin4, 2) + pow(a.num_total_etabin4*b.err_total_etabin4/pow(b.num_total_etabin4,2), 2) + 2*rho*(1/b.num_total_etabin4)*(-1*a.num_total_etabin4/pow(b.num_total_etabin4,2))*a.err_total_etabin4*b.err_total_etabin4);
    c.num_total_etabin5 = (a.num_total_etabin5 - b.num_total_etabin5) / b.num_total_etabin5;
    c.err_total_etabin5 = sqrt(pow(a.err_total_etabin5/b.num_total_etabin5, 2) + pow(a.num_total_etabin5*b.err_total_etabin5/pow(b.num_total_etabin5,2), 2) + 2*rho*(1/b.num_total_etabin5)*(-1*a.num_total_etabin5/pow(b.num_total_etabin5,2))*a.err_total_etabin5*b.err_total_etabin5);
    c.num_fidu = (a.num_fidu - b.num_fidu) / b.num_fidu;
    c.err_fidu = sqrt(pow(a.err_fidu/b.num_fidu, 2) + pow(a.num_fidu*b.err_fidu/pow(b.num_fidu,2), 2) + 2*rho*(1/b.num_fidu)*(-1*a.num_fidu/pow(b.num_fidu,2))*a.err_fidu*b.err_fidu);
    c.num_fidu_ptbin1 = (a.num_fidu_ptbin1 - b.num_fidu_ptbin1) / b.num_fidu_ptbin1;
    c.err_fidu_ptbin1 = sqrt(pow(a.err_fidu_ptbin1/b.num_fidu_ptbin1, 2) + pow(a.num_fidu_ptbin1*b.err_fidu_ptbin1/pow(b.num_fidu_ptbin1,2), 2) + 2*rho*(1/b.num_fidu_ptbin1)*(-1*a.num_fidu_ptbin1/pow(b.num_fidu_ptbin1,2))*a.err_fidu_ptbin1*b.err_fidu_ptbin1);
    c.num_fidu_ptbin2 = (a.num_fidu_ptbin2 - b.num_fidu_ptbin2) / b.num_fidu_ptbin2;
    c.err_fidu_ptbin2 = sqrt(pow(a.err_fidu_ptbin2/b.num_fidu_ptbin2, 2) + pow(a.num_fidu_ptbin2*b.err_fidu_ptbin2/pow(b.num_fidu_ptbin2,2), 2) + 2*rho*(1/b.num_fidu_ptbin2)*(-1*a.num_fidu_ptbin2/pow(b.num_fidu_ptbin2,2))*a.err_fidu_ptbin2*b.err_fidu_ptbin2);
    c.num_fidu_ptbin3 = (a.num_fidu_ptbin3 - b.num_fidu_ptbin3) / b.num_fidu_ptbin3;
    c.err_fidu_ptbin3 = sqrt(pow(a.err_fidu_ptbin3/b.num_fidu_ptbin3, 2) + pow(a.num_fidu_ptbin3*b.err_fidu_ptbin3/pow(b.num_fidu_ptbin3,2), 2) + 2*rho*(1/b.num_fidu_ptbin3)*(-1*a.num_fidu_ptbin3/pow(b.num_fidu_ptbin3,2))*a.err_fidu_ptbin3*b.err_fidu_ptbin3);
    c.num_fidu_ptbin4 = (a.num_fidu_ptbin4 - b.num_fidu_ptbin4) / b.num_fidu_ptbin4;
    c.err_fidu_ptbin4 = sqrt(pow(a.err_fidu_ptbin4/b.num_fidu_ptbin4, 2) + pow(a.num_fidu_ptbin4*b.err_fidu_ptbin4/pow(b.num_fidu_ptbin4,2), 2) + 2*rho*(1/b.num_fidu_ptbin4)*(-1*a.num_fidu_ptbin4/pow(b.num_fidu_ptbin4,2))*a.err_fidu_ptbin4*b.err_fidu_ptbin4);
    c.num_fidu_ptbin5 = (a.num_fidu_ptbin5 - b.num_fidu_ptbin5) / b.num_fidu_ptbin5;
    c.err_fidu_ptbin5 = sqrt(pow(a.err_fidu_ptbin5/b.num_fidu_ptbin5, 2) + pow(a.num_fidu_ptbin5*b.err_fidu_ptbin5/pow(b.num_fidu_ptbin5,2), 2) + 2*rho*(1/b.num_fidu_ptbin5)*(-1*a.num_fidu_ptbin5/pow(b.num_fidu_ptbin5,2))*a.err_fidu_ptbin5*b.err_fidu_ptbin5);
    c.num_fidu_etabin1 = (a.num_fidu_etabin1 - b.num_fidu_etabin1) / b.num_fidu_etabin1;
    c.err_fidu_etabin1 = sqrt(pow(a.err_fidu_etabin1/b.num_fidu_etabin1, 2) + pow(a.num_fidu_etabin1*b.err_fidu_etabin1/pow(b.num_fidu_etabin1,2), 2) + 2*rho*(1/b.num_fidu_etabin1)*(-1*a.num_fidu_etabin1/pow(b.num_fidu_etabin1,2))*a.err_fidu_etabin1*b.err_fidu_etabin1);
    c.num_fidu_etabin2 = (a.num_fidu_etabin2 - b.num_fidu_etabin2) / b.num_fidu_etabin2;
    c.err_fidu_etabin2 = sqrt(pow(a.err_fidu_etabin2/b.num_fidu_etabin2, 2) + pow(a.num_fidu_etabin2*b.err_fidu_etabin2/pow(b.num_fidu_etabin2,2), 2) + 2*rho*(1/b.num_fidu_etabin2)*(-1*a.num_fidu_etabin2/pow(b.num_fidu_etabin2,2))*a.err_fidu_etabin2*b.err_fidu_etabin2);
    c.num_fidu_etabin3 = (a.num_fidu_etabin3 - b.num_fidu_etabin3) / b.num_fidu_etabin3;
    c.err_fidu_etabin3 = sqrt(pow(a.err_fidu_etabin3/b.num_fidu_etabin3, 2) + pow(a.num_fidu_etabin3*b.err_fidu_etabin3/pow(b.num_fidu_etabin3,2), 2) + 2*rho*(1/b.num_fidu_etabin3)*(-1*a.num_fidu_etabin3/pow(b.num_fidu_etabin3,2))*a.err_fidu_etabin3*b.err_fidu_etabin3);
    c.num_fidu_etabin4 = (a.num_fidu_etabin4 - b.num_fidu_etabin4) / b.num_fidu_etabin4;
    c.err_fidu_etabin4 = sqrt(pow(a.err_fidu_etabin4/b.num_fidu_etabin4, 2) + pow(a.num_fidu_etabin4*b.err_fidu_etabin4/pow(b.num_fidu_etabin4,2), 2) + 2*rho*(1/b.num_fidu_etabin4)*(-1*a.num_fidu_etabin4/pow(b.num_fidu_etabin4,2))*a.err_fidu_etabin4*b.err_fidu_etabin4);
    c.num_fidu_etabin5 = (a.num_fidu_etabin5 - b.num_fidu_etabin5) / b.num_fidu_etabin5;
    c.err_fidu_etabin5 = sqrt(pow(a.err_fidu_etabin5/b.num_fidu_etabin5, 2) + pow(a.num_fidu_etabin5*b.err_fidu_etabin5/pow(b.num_fidu_etabin5,2), 2) + 2*rho*(1/b.num_fidu_etabin5)*(-1*a.num_fidu_etabin5/pow(b.num_fidu_etabin5,2))*a.err_fidu_etabin5*b.err_fidu_etabin5);
    c.num_isobin1 = (a.num_isobin1 - b.num_isobin1) / b.num_isobin1;
    c.err_isobin1 = sqrt(pow(a.err_isobin1/b.num_isobin1, 2) + pow(a.num_isobin1*b.err_isobin1/pow(b.num_isobin1,2), 2) + 2*rho*(1/b.num_isobin1)*(-1*a.num_isobin1/pow(b.num_isobin1,2))*a.err_isobin1*b.err_isobin1);
    c.num_isobin2 = (a.num_isobin2 - b.num_isobin2) / b.num_isobin2;
    c.err_isobin2 = sqrt(pow(a.err_isobin2/b.num_isobin2, 2) + pow(a.num_isobin2*b.err_isobin2/pow(b.num_isobin2,2), 2) + 2*rho*(1/b.num_isobin2)*(-1*a.num_isobin2/pow(b.num_isobin2,2))*a.err_isobin2*b.err_isobin2);
    c.num_isobin3 = (a.num_isobin3 - b.num_isobin3) / b.num_isobin3;
    c.err_isobin3 = sqrt(pow(a.err_isobin3/b.num_isobin3, 2) + pow(a.num_isobin3*b.err_isobin3/pow(b.num_isobin3,2), 2) + 2*rho*(1/b.num_isobin3)*(-1*a.num_isobin3/pow(b.num_isobin3,2))*a.err_isobin3*b.err_isobin3);
    c.num_isobin4 = (a.num_isobin4 - b.num_isobin4) / b.num_isobin4;
    c.err_isobin4 = sqrt(pow(a.err_isobin4/b.num_isobin4, 2) + pow(a.num_isobin4*b.err_isobin4/pow(b.num_isobin4,2), 2) + 2*rho*(1/b.num_isobin4)*(-1*a.num_isobin4/pow(b.num_isobin4,2))*a.err_isobin4*b.err_isobin4);
    c.num_isobin5 = (a.num_isobin5 - b.num_isobin5) / b.num_isobin5;
    c.err_isobin5 = sqrt(pow(a.err_isobin5/b.num_isobin5, 2) + pow(a.num_isobin5*b.err_isobin5/pow(b.num_isobin5,2), 2) + 2*rho*(1/b.num_isobin5)*(-1*a.num_isobin5/pow(b.num_isobin5,2))*a.err_isobin5*b.err_isobin5);
    c.num_ptbin1_isobin1 = (a.num_ptbin1_isobin1 - b.num_ptbin1_isobin1) / b.num_ptbin1_isobin1;
    c.err_ptbin1_isobin1 = sqrt(pow(a.err_ptbin1_isobin1/b.num_ptbin1_isobin1, 2) + pow(a.num_ptbin1_isobin1*b.err_ptbin1_isobin1/pow(b.num_ptbin1_isobin1,2), 2) + 2*rho*(1/b.num_ptbin1_isobin1)*(-1*a.num_ptbin1_isobin1/pow(b.num_ptbin1_isobin1,2))*a.err_ptbin1_isobin1*b.err_ptbin1_isobin1);
    c.num_ptbin1_isobin2 = (a.num_ptbin1_isobin2 - b.num_ptbin1_isobin2) / b.num_ptbin1_isobin2;
    c.err_ptbin1_isobin2 = sqrt(pow(a.err_ptbin1_isobin2/b.num_ptbin1_isobin2, 2) + pow(a.num_ptbin1_isobin2*b.err_ptbin1_isobin2/pow(b.num_ptbin1_isobin2,2), 2) + 2*rho*(1/b.num_ptbin1_isobin2)*(-1*a.num_ptbin1_isobin2/pow(b.num_ptbin1_isobin2,2))*a.err_ptbin1_isobin2*b.err_ptbin1_isobin2);
    c.num_ptbin1_isobin3 = (a.num_ptbin1_isobin3 - b.num_ptbin1_isobin3) / b.num_ptbin1_isobin3;
    c.err_ptbin1_isobin3 = sqrt(pow(a.err_ptbin1_isobin3/b.num_ptbin1_isobin3, 2) + pow(a.num_ptbin1_isobin3*b.err_ptbin1_isobin3/pow(b.num_ptbin1_isobin3,2), 2) + 2*rho*(1/b.num_ptbin1_isobin3)*(-1*a.num_ptbin1_isobin3/pow(b.num_ptbin1_isobin3,2))*a.err_ptbin1_isobin3*b.err_ptbin1_isobin3);
    c.num_ptbin1_isobin4 = (a.num_ptbin1_isobin4 - b.num_ptbin1_isobin4) / b.num_ptbin1_isobin4;
    c.err_ptbin1_isobin4 = sqrt(pow(a.err_ptbin1_isobin4/b.num_ptbin1_isobin4, 2) + pow(a.num_ptbin1_isobin4*b.err_ptbin1_isobin4/pow(b.num_ptbin1_isobin4,2), 2) + 2*rho*(1/b.num_ptbin1_isobin4)*(-1*a.num_ptbin1_isobin4/pow(b.num_ptbin1_isobin4,2))*a.err_ptbin1_isobin4*b.err_ptbin1_isobin4);
    c.num_ptbin1_isobin5 = (a.num_ptbin1_isobin5 - b.num_ptbin1_isobin5) / b.num_ptbin1_isobin5;
    c.err_ptbin1_isobin5 = sqrt(pow(a.err_ptbin1_isobin5/b.num_ptbin1_isobin5, 2) + pow(a.num_ptbin1_isobin5*b.err_ptbin1_isobin5/pow(b.num_ptbin1_isobin5,2), 2) + 2*rho*(1/b.num_ptbin1_isobin5)*(-1*a.num_ptbin1_isobin5/pow(b.num_ptbin1_isobin5,2))*a.err_ptbin1_isobin5*b.err_ptbin1_isobin5);
    c.num_ptbin2_isobin1 = (a.num_ptbin2_isobin1 - b.num_ptbin2_isobin1) / b.num_ptbin2_isobin1;
    c.err_ptbin2_isobin1 = sqrt(pow(a.err_ptbin2_isobin1/b.num_ptbin2_isobin1, 2) + pow(a.num_ptbin2_isobin1*b.err_ptbin2_isobin1/pow(b.num_ptbin2_isobin1,2), 2) + 2*rho*(1/b.num_ptbin2_isobin1)*(-1*a.num_ptbin2_isobin1/pow(b.num_ptbin2_isobin1,2))*a.err_ptbin2_isobin1*b.err_ptbin2_isobin1);
    c.num_ptbin2_isobin2 = (a.num_ptbin2_isobin2 - b.num_ptbin2_isobin2) / b.num_ptbin2_isobin2;
    c.err_ptbin2_isobin2 = sqrt(pow(a.err_ptbin2_isobin2/b.num_ptbin2_isobin2, 2) + pow(a.num_ptbin2_isobin2*b.err_ptbin2_isobin2/pow(b.num_ptbin2_isobin2,2), 2) + 2*rho*(1/b.num_ptbin2_isobin2)*(-1*a.num_ptbin2_isobin2/pow(b.num_ptbin2_isobin2,2))*a.err_ptbin2_isobin2*b.err_ptbin2_isobin2);
    c.num_ptbin2_isobin3 = (a.num_ptbin2_isobin3 - b.num_ptbin2_isobin3) / b.num_ptbin2_isobin3;
    c.err_ptbin2_isobin3 = sqrt(pow(a.err_ptbin2_isobin3/b.num_ptbin2_isobin3, 2) + pow(a.num_ptbin2_isobin3*b.err_ptbin2_isobin3/pow(b.num_ptbin2_isobin3,2), 2) + 2*rho*(1/b.num_ptbin2_isobin3)*(-1*a.num_ptbin2_isobin3/pow(b.num_ptbin2_isobin3,2))*a.err_ptbin2_isobin3*b.err_ptbin2_isobin3);
    c.num_ptbin2_isobin4 = (a.num_ptbin2_isobin4 - b.num_ptbin2_isobin4) / b.num_ptbin2_isobin4;
    c.err_ptbin2_isobin4 = sqrt(pow(a.err_ptbin2_isobin4/b.num_ptbin2_isobin4, 2) + pow(a.num_ptbin2_isobin4*b.err_ptbin2_isobin4/pow(b.num_ptbin2_isobin4,2), 2) + 2*rho*(1/b.num_ptbin2_isobin4)*(-1*a.num_ptbin2_isobin4/pow(b.num_ptbin2_isobin4,2))*a.err_ptbin2_isobin4*b.err_ptbin2_isobin4);
    c.num_ptbin2_isobin5 = (a.num_ptbin2_isobin5 - b.num_ptbin2_isobin5) / b.num_ptbin2_isobin5;
    c.err_ptbin2_isobin5 = sqrt(pow(a.err_ptbin2_isobin5/b.num_ptbin2_isobin5, 2) + pow(a.num_ptbin2_isobin5*b.err_ptbin2_isobin5/pow(b.num_ptbin2_isobin5,2), 2) + 2*rho*(1/b.num_ptbin2_isobin5)*(-1*a.num_ptbin2_isobin5/pow(b.num_ptbin2_isobin5,2))*a.err_ptbin2_isobin5*b.err_ptbin2_isobin5);
    c.num_ptbin3_isobin1 = (a.num_ptbin3_isobin1 - b.num_ptbin3_isobin1) / b.num_ptbin3_isobin1;
    c.err_ptbin3_isobin1 = sqrt(pow(a.err_ptbin3_isobin1/b.num_ptbin3_isobin1, 2) + pow(a.num_ptbin3_isobin1*b.err_ptbin3_isobin1/pow(b.num_ptbin3_isobin1,2), 2) + 2*rho*(1/b.num_ptbin3_isobin1)*(-1*a.num_ptbin3_isobin1/pow(b.num_ptbin3_isobin1,2))*a.err_ptbin3_isobin1*b.err_ptbin3_isobin1);
    c.num_ptbin3_isobin2 = (a.num_ptbin3_isobin2 - b.num_ptbin3_isobin2) / b.num_ptbin3_isobin2;
    c.err_ptbin3_isobin2 = sqrt(pow(a.err_ptbin3_isobin2/b.num_ptbin3_isobin2, 2) + pow(a.num_ptbin3_isobin2*b.err_ptbin3_isobin2/pow(b.num_ptbin3_isobin2,2), 2) + 2*rho*(1/b.num_ptbin3_isobin2)*(-1*a.num_ptbin3_isobin2/pow(b.num_ptbin3_isobin2,2))*a.err_ptbin3_isobin2*b.err_ptbin3_isobin2);
    c.num_ptbin3_isobin3 = (a.num_ptbin3_isobin3 - b.num_ptbin3_isobin3) / b.num_ptbin3_isobin3;
    c.err_ptbin3_isobin3 = sqrt(pow(a.err_ptbin3_isobin3/b.num_ptbin3_isobin3, 2) + pow(a.num_ptbin3_isobin3*b.err_ptbin3_isobin3/pow(b.num_ptbin3_isobin3,2), 2) + 2*rho*(1/b.num_ptbin3_isobin3)*(-1*a.num_ptbin3_isobin3/pow(b.num_ptbin3_isobin3,2))*a.err_ptbin3_isobin3*b.err_ptbin3_isobin3);
    c.num_ptbin3_isobin4 = (a.num_ptbin3_isobin4 - b.num_ptbin3_isobin4) / b.num_ptbin3_isobin4;
    c.err_ptbin3_isobin4 = sqrt(pow(a.err_ptbin3_isobin4/b.num_ptbin3_isobin4, 2) + pow(a.num_ptbin3_isobin4*b.err_ptbin3_isobin4/pow(b.num_ptbin3_isobin4,2), 2) + 2*rho*(1/b.num_ptbin3_isobin4)*(-1*a.num_ptbin3_isobin4/pow(b.num_ptbin3_isobin4,2))*a.err_ptbin3_isobin4*b.err_ptbin3_isobin4);
    c.num_ptbin3_isobin5 = (a.num_ptbin3_isobin5 - b.num_ptbin3_isobin5) / b.num_ptbin3_isobin5;
    c.err_ptbin3_isobin5 = sqrt(pow(a.err_ptbin3_isobin5/b.num_ptbin3_isobin5, 2) + pow(a.num_ptbin3_isobin5*b.err_ptbin3_isobin5/pow(b.num_ptbin3_isobin5,2), 2) + 2*rho*(1/b.num_ptbin3_isobin5)*(-1*a.num_ptbin3_isobin5/pow(b.num_ptbin3_isobin5,2))*a.err_ptbin3_isobin5*b.err_ptbin3_isobin5);
    c.num_ptbin4_isobin1 = (a.num_ptbin4_isobin1 - b.num_ptbin4_isobin1) / b.num_ptbin4_isobin1;
    c.err_ptbin4_isobin1 = sqrt(pow(a.err_ptbin4_isobin1/b.num_ptbin4_isobin1, 2) + pow(a.num_ptbin4_isobin1*b.err_ptbin4_isobin1/pow(b.num_ptbin4_isobin1,2), 2) + 2*rho*(1/b.num_ptbin4_isobin1)*(-1*a.num_ptbin4_isobin1/pow(b.num_ptbin4_isobin1,2))*a.err_ptbin4_isobin1*b.err_ptbin4_isobin1);
    c.num_ptbin4_isobin2 = (a.num_ptbin4_isobin2 - b.num_ptbin4_isobin2) / b.num_ptbin4_isobin2;
    c.err_ptbin4_isobin2 = sqrt(pow(a.err_ptbin4_isobin2/b.num_ptbin4_isobin2, 2) + pow(a.num_ptbin4_isobin2*b.err_ptbin4_isobin2/pow(b.num_ptbin4_isobin2,2), 2) + 2*rho*(1/b.num_ptbin4_isobin2)*(-1*a.num_ptbin4_isobin2/pow(b.num_ptbin4_isobin2,2))*a.err_ptbin4_isobin2*b.err_ptbin4_isobin2);
    c.num_ptbin4_isobin3 = (a.num_ptbin4_isobin3 - b.num_ptbin4_isobin3) / b.num_ptbin4_isobin3;
    c.err_ptbin4_isobin3 = sqrt(pow(a.err_ptbin4_isobin3/b.num_ptbin4_isobin3, 2) + pow(a.num_ptbin4_isobin3*b.err_ptbin4_isobin3/pow(b.num_ptbin4_isobin3,2), 2) + 2*rho*(1/b.num_ptbin4_isobin3)*(-1*a.num_ptbin4_isobin3/pow(b.num_ptbin4_isobin3,2))*a.err_ptbin4_isobin3*b.err_ptbin4_isobin3);
    c.num_ptbin4_isobin4 = (a.num_ptbin4_isobin4 - b.num_ptbin4_isobin4) / b.num_ptbin4_isobin4;
    c.err_ptbin4_isobin4 = sqrt(pow(a.err_ptbin4_isobin4/b.num_ptbin4_isobin4, 2) + pow(a.num_ptbin4_isobin4*b.err_ptbin4_isobin4/pow(b.num_ptbin4_isobin4,2), 2) + 2*rho*(1/b.num_ptbin4_isobin4)*(-1*a.num_ptbin4_isobin4/pow(b.num_ptbin4_isobin4,2))*a.err_ptbin4_isobin4*b.err_ptbin4_isobin4);
    c.num_ptbin4_isobin5 = (a.num_ptbin4_isobin5 - b.num_ptbin4_isobin5) / b.num_ptbin4_isobin5;
    c.err_ptbin4_isobin5 = sqrt(pow(a.err_ptbin4_isobin5/b.num_ptbin4_isobin5, 2) + pow(a.num_ptbin4_isobin5*b.err_ptbin4_isobin5/pow(b.num_ptbin4_isobin5,2), 2) + 2*rho*(1/b.num_ptbin4_isobin5)*(-1*a.num_ptbin4_isobin5/pow(b.num_ptbin4_isobin5,2))*a.err_ptbin4_isobin5*b.err_ptbin4_isobin5);
    c.num_ptbin5_isobin1 = (a.num_ptbin5_isobin1 - b.num_ptbin5_isobin1) / b.num_ptbin5_isobin1;
    c.err_ptbin5_isobin1 = sqrt(pow(a.err_ptbin5_isobin1/b.num_ptbin5_isobin1, 2) + pow(a.num_ptbin5_isobin1*b.err_ptbin5_isobin1/pow(b.num_ptbin5_isobin1,2), 2) + 2*rho*(1/b.num_ptbin5_isobin1)*(-1*a.num_ptbin5_isobin1/pow(b.num_ptbin5_isobin1,2))*a.err_ptbin5_isobin1*b.err_ptbin5_isobin1);
    c.num_ptbin5_isobin2 = (a.num_ptbin5_isobin2 - b.num_ptbin5_isobin2) / b.num_ptbin5_isobin2;
    c.err_ptbin5_isobin2 = sqrt(pow(a.err_ptbin5_isobin2/b.num_ptbin5_isobin2, 2) + pow(a.num_ptbin5_isobin2*b.err_ptbin5_isobin2/pow(b.num_ptbin5_isobin2,2), 2) + 2*rho*(1/b.num_ptbin5_isobin2)*(-1*a.num_ptbin5_isobin2/pow(b.num_ptbin5_isobin2,2))*a.err_ptbin5_isobin2*b.err_ptbin5_isobin2);
    c.num_ptbin5_isobin3 = (a.num_ptbin5_isobin3 - b.num_ptbin5_isobin3) / b.num_ptbin5_isobin3;
    c.err_ptbin5_isobin3 = sqrt(pow(a.err_ptbin5_isobin3/b.num_ptbin5_isobin3, 2) + pow(a.num_ptbin5_isobin3*b.err_ptbin5_isobin3/pow(b.num_ptbin5_isobin3,2), 2) + 2*rho*(1/b.num_ptbin5_isobin3)*(-1*a.num_ptbin5_isobin3/pow(b.num_ptbin5_isobin3,2))*a.err_ptbin5_isobin3*b.err_ptbin5_isobin3);
    c.num_ptbin5_isobin4 = (a.num_ptbin5_isobin4 - b.num_ptbin5_isobin4) / b.num_ptbin5_isobin4;
    c.err_ptbin5_isobin4 = sqrt(pow(a.err_ptbin5_isobin4/b.num_ptbin5_isobin4, 2) + pow(a.num_ptbin5_isobin4*b.err_ptbin5_isobin4/pow(b.num_ptbin5_isobin4,2), 2) + 2*rho*(1/b.num_ptbin5_isobin4)*(-1*a.num_ptbin5_isobin4/pow(b.num_ptbin5_isobin4,2))*a.err_ptbin5_isobin4*b.err_ptbin5_isobin4);
    c.num_ptbin5_isobin5 = (a.num_ptbin5_isobin5 - b.num_ptbin5_isobin5) / b.num_ptbin5_isobin5;
    c.err_ptbin5_isobin5 = sqrt(pow(a.err_ptbin5_isobin5/b.num_ptbin5_isobin5, 2) + pow(a.num_ptbin5_isobin5*b.err_ptbin5_isobin5/pow(b.num_ptbin5_isobin5,2), 2) + 2*rho*(1/b.num_ptbin5_isobin5)*(-1*a.num_ptbin5_isobin5/pow(b.num_ptbin5_isobin5,2))*a.err_ptbin5_isobin5*b.err_ptbin5_isobin5);
    c.num_etabin1_isobin1 = (a.num_etabin1_isobin1 - b.num_etabin1_isobin1) / b.num_etabin1_isobin1;
    c.err_etabin1_isobin1 = sqrt(pow(a.err_etabin1_isobin1/b.num_etabin1_isobin1, 2) + pow(a.num_etabin1_isobin1*b.err_etabin1_isobin1/pow(b.num_etabin1_isobin1,2), 2) + 2*rho*(1/b.num_etabin1_isobin1)*(-1*a.num_etabin1_isobin1/pow(b.num_etabin1_isobin1,2))*a.err_etabin1_isobin1*b.err_etabin1_isobin1);
    c.num_etabin1_isobin2 = (a.num_etabin1_isobin2 - b.num_etabin1_isobin2) / b.num_etabin1_isobin2;
    c.err_etabin1_isobin2 = sqrt(pow(a.err_etabin1_isobin2/b.num_etabin1_isobin2, 2) + pow(a.num_etabin1_isobin2*b.err_etabin1_isobin2/pow(b.num_etabin1_isobin2,2), 2) + 2*rho*(1/b.num_etabin1_isobin2)*(-1*a.num_etabin1_isobin2/pow(b.num_etabin1_isobin2,2))*a.err_etabin1_isobin2*b.err_etabin1_isobin2);
    c.num_etabin1_isobin3 = (a.num_etabin1_isobin3 - b.num_etabin1_isobin3) / b.num_etabin1_isobin3;
    c.err_etabin1_isobin3 = sqrt(pow(a.err_etabin1_isobin3/b.num_etabin1_isobin3, 2) + pow(a.num_etabin1_isobin3*b.err_etabin1_isobin3/pow(b.num_etabin1_isobin3,2), 2) + 2*rho*(1/b.num_etabin1_isobin3)*(-1*a.num_etabin1_isobin3/pow(b.num_etabin1_isobin3,2))*a.err_etabin1_isobin3*b.err_etabin1_isobin3);
    c.num_etabin1_isobin4 = (a.num_etabin1_isobin4 - b.num_etabin1_isobin4) / b.num_etabin1_isobin4;
    c.err_etabin1_isobin4 = sqrt(pow(a.err_etabin1_isobin4/b.num_etabin1_isobin4, 2) + pow(a.num_etabin1_isobin4*b.err_etabin1_isobin4/pow(b.num_etabin1_isobin4,2), 2) + 2*rho*(1/b.num_etabin1_isobin4)*(-1*a.num_etabin1_isobin4/pow(b.num_etabin1_isobin4,2))*a.err_etabin1_isobin4*b.err_etabin1_isobin4);
    c.num_etabin1_isobin5 = (a.num_etabin1_isobin5 - b.num_etabin1_isobin5) / b.num_etabin1_isobin5;
    c.err_etabin1_isobin5 = sqrt(pow(a.err_etabin1_isobin5/b.num_etabin1_isobin5, 2) + pow(a.num_etabin1_isobin5*b.err_etabin1_isobin5/pow(b.num_etabin1_isobin5,2), 2) + 2*rho*(1/b.num_etabin1_isobin5)*(-1*a.num_etabin1_isobin5/pow(b.num_etabin1_isobin5,2))*a.err_etabin1_isobin5*b.err_etabin1_isobin5);
    c.num_etabin2_isobin1 = (a.num_etabin2_isobin1 - b.num_etabin2_isobin1) / b.num_etabin2_isobin1;
    c.err_etabin2_isobin1 = sqrt(pow(a.err_etabin2_isobin1/b.num_etabin2_isobin1, 2) + pow(a.num_etabin2_isobin1*b.err_etabin2_isobin1/pow(b.num_etabin2_isobin1,2), 2) + 2*rho*(1/b.num_etabin2_isobin1)*(-1*a.num_etabin2_isobin1/pow(b.num_etabin2_isobin1,2))*a.err_etabin2_isobin1*b.err_etabin2_isobin1);
    c.num_etabin2_isobin2 = (a.num_etabin2_isobin2 - b.num_etabin2_isobin2) / b.num_etabin2_isobin2;
    c.err_etabin2_isobin2 = sqrt(pow(a.err_etabin2_isobin2/b.num_etabin2_isobin2, 2) + pow(a.num_etabin2_isobin2*b.err_etabin2_isobin2/pow(b.num_etabin2_isobin2,2), 2) + 2*rho*(1/b.num_etabin2_isobin2)*(-1*a.num_etabin2_isobin2/pow(b.num_etabin2_isobin2,2))*a.err_etabin2_isobin2*b.err_etabin2_isobin2);
    c.num_etabin2_isobin3 = (a.num_etabin2_isobin3 - b.num_etabin2_isobin3) / b.num_etabin2_isobin3;
    c.err_etabin2_isobin3 = sqrt(pow(a.err_etabin2_isobin3/b.num_etabin2_isobin3, 2) + pow(a.num_etabin2_isobin3*b.err_etabin2_isobin3/pow(b.num_etabin2_isobin3,2), 2) + 2*rho*(1/b.num_etabin2_isobin3)*(-1*a.num_etabin2_isobin3/pow(b.num_etabin2_isobin3,2))*a.err_etabin2_isobin3*b.err_etabin2_isobin3);
    c.num_etabin2_isobin4 = (a.num_etabin2_isobin4 - b.num_etabin2_isobin4) / b.num_etabin2_isobin4;
    c.err_etabin2_isobin4 = sqrt(pow(a.err_etabin2_isobin4/b.num_etabin2_isobin4, 2) + pow(a.num_etabin2_isobin4*b.err_etabin2_isobin4/pow(b.num_etabin2_isobin4,2), 2) + 2*rho*(1/b.num_etabin2_isobin4)*(-1*a.num_etabin2_isobin4/pow(b.num_etabin2_isobin4,2))*a.err_etabin2_isobin4*b.err_etabin2_isobin4);
    c.num_etabin2_isobin5 = (a.num_etabin2_isobin5 - b.num_etabin2_isobin5) / b.num_etabin2_isobin5;
    c.err_etabin2_isobin5 = sqrt(pow(a.err_etabin2_isobin5/b.num_etabin2_isobin5, 2) + pow(a.num_etabin2_isobin5*b.err_etabin2_isobin5/pow(b.num_etabin2_isobin5,2), 2) + 2*rho*(1/b.num_etabin2_isobin5)*(-1*a.num_etabin2_isobin5/pow(b.num_etabin2_isobin5,2))*a.err_etabin2_isobin5*b.err_etabin2_isobin5);
    c.num_etabin3_isobin1 = (a.num_etabin3_isobin1 - b.num_etabin3_isobin1) / b.num_etabin3_isobin1;
    c.err_etabin3_isobin1 = sqrt(pow(a.err_etabin3_isobin1/b.num_etabin3_isobin1, 2) + pow(a.num_etabin3_isobin1*b.err_etabin3_isobin1/pow(b.num_etabin3_isobin1,2), 2) + 2*rho*(1/b.num_etabin3_isobin1)*(-1*a.num_etabin3_isobin1/pow(b.num_etabin3_isobin1,2))*a.err_etabin3_isobin1*b.err_etabin3_isobin1);
    c.num_etabin3_isobin2 = (a.num_etabin3_isobin2 - b.num_etabin3_isobin2) / b.num_etabin3_isobin2;
    c.err_etabin3_isobin2 = sqrt(pow(a.err_etabin3_isobin2/b.num_etabin3_isobin2, 2) + pow(a.num_etabin3_isobin2*b.err_etabin3_isobin2/pow(b.num_etabin3_isobin2,2), 2) + 2*rho*(1/b.num_etabin3_isobin2)*(-1*a.num_etabin3_isobin2/pow(b.num_etabin3_isobin2,2))*a.err_etabin3_isobin2*b.err_etabin3_isobin2);
    c.num_etabin3_isobin3 = (a.num_etabin3_isobin3 - b.num_etabin3_isobin3) / b.num_etabin3_isobin3;
    c.err_etabin3_isobin3 = sqrt(pow(a.err_etabin3_isobin3/b.num_etabin3_isobin3, 2) + pow(a.num_etabin3_isobin3*b.err_etabin3_isobin3/pow(b.num_etabin3_isobin3,2), 2) + 2*rho*(1/b.num_etabin3_isobin3)*(-1*a.num_etabin3_isobin3/pow(b.num_etabin3_isobin3,2))*a.err_etabin3_isobin3*b.err_etabin3_isobin3);
    c.num_etabin3_isobin4 = (a.num_etabin3_isobin4 - b.num_etabin3_isobin4) / b.num_etabin3_isobin4;
    c.err_etabin3_isobin4 = sqrt(pow(a.err_etabin3_isobin4/b.num_etabin3_isobin4, 2) + pow(a.num_etabin3_isobin4*b.err_etabin3_isobin4/pow(b.num_etabin3_isobin4,2), 2) + 2*rho*(1/b.num_etabin3_isobin4)*(-1*a.num_etabin3_isobin4/pow(b.num_etabin3_isobin4,2))*a.err_etabin3_isobin4*b.err_etabin3_isobin4);
    c.num_etabin3_isobin5 = (a.num_etabin3_isobin5 - b.num_etabin3_isobin5) / b.num_etabin3_isobin5;
    c.err_etabin3_isobin5 = sqrt(pow(a.err_etabin3_isobin5/b.num_etabin3_isobin5, 2) + pow(a.num_etabin3_isobin5*b.err_etabin3_isobin5/pow(b.num_etabin3_isobin5,2), 2) + 2*rho*(1/b.num_etabin3_isobin5)*(-1*a.num_etabin3_isobin5/pow(b.num_etabin3_isobin5,2))*a.err_etabin3_isobin5*b.err_etabin3_isobin5);
    c.num_etabin4_isobin1 = (a.num_etabin4_isobin1 - b.num_etabin4_isobin1) / b.num_etabin4_isobin1;
    c.err_etabin4_isobin1 = sqrt(pow(a.err_etabin4_isobin1/b.num_etabin4_isobin1, 2) + pow(a.num_etabin4_isobin1*b.err_etabin4_isobin1/pow(b.num_etabin4_isobin1,2), 2) + 2*rho*(1/b.num_etabin4_isobin1)*(-1*a.num_etabin4_isobin1/pow(b.num_etabin4_isobin1,2))*a.err_etabin4_isobin1*b.err_etabin4_isobin1);
    c.num_etabin4_isobin2 = (a.num_etabin4_isobin2 - b.num_etabin4_isobin2) / b.num_etabin4_isobin2;
    c.err_etabin4_isobin2 = sqrt(pow(a.err_etabin4_isobin2/b.num_etabin4_isobin2, 2) + pow(a.num_etabin4_isobin2*b.err_etabin4_isobin2/pow(b.num_etabin4_isobin2,2), 2) + 2*rho*(1/b.num_etabin4_isobin2)*(-1*a.num_etabin4_isobin2/pow(b.num_etabin4_isobin2,2))*a.err_etabin4_isobin2*b.err_etabin4_isobin2);
    c.num_etabin4_isobin3 = (a.num_etabin4_isobin3 - b.num_etabin4_isobin3) / b.num_etabin4_isobin3;
    c.err_etabin4_isobin3 = sqrt(pow(a.err_etabin4_isobin3/b.num_etabin4_isobin3, 2) + pow(a.num_etabin4_isobin3*b.err_etabin4_isobin3/pow(b.num_etabin4_isobin3,2), 2) + 2*rho*(1/b.num_etabin4_isobin3)*(-1*a.num_etabin4_isobin3/pow(b.num_etabin4_isobin3,2))*a.err_etabin4_isobin3*b.err_etabin4_isobin3);
    c.num_etabin4_isobin4 = (a.num_etabin4_isobin4 - b.num_etabin4_isobin4) / b.num_etabin4_isobin4;
    c.err_etabin4_isobin4 = sqrt(pow(a.err_etabin4_isobin4/b.num_etabin4_isobin4, 2) + pow(a.num_etabin4_isobin4*b.err_etabin4_isobin4/pow(b.num_etabin4_isobin4,2), 2) + 2*rho*(1/b.num_etabin4_isobin4)*(-1*a.num_etabin4_isobin4/pow(b.num_etabin4_isobin4,2))*a.err_etabin4_isobin4*b.err_etabin4_isobin4);
    c.num_etabin4_isobin5 = (a.num_etabin4_isobin5 - b.num_etabin4_isobin5) / b.num_etabin4_isobin5;
    c.err_etabin4_isobin5 = sqrt(pow(a.err_etabin4_isobin5/b.num_etabin4_isobin5, 2) + pow(a.num_etabin4_isobin5*b.err_etabin4_isobin5/pow(b.num_etabin4_isobin5,2), 2) + 2*rho*(1/b.num_etabin4_isobin5)*(-1*a.num_etabin4_isobin5/pow(b.num_etabin4_isobin5,2))*a.err_etabin4_isobin5*b.err_etabin4_isobin5);
    c.num_etabin5_isobin1 = (a.num_etabin5_isobin1 - b.num_etabin5_isobin1) / b.num_etabin5_isobin1;
    c.err_etabin5_isobin1 = sqrt(pow(a.err_etabin5_isobin1/b.num_etabin5_isobin1, 2) + pow(a.num_etabin5_isobin1*b.err_etabin5_isobin1/pow(b.num_etabin5_isobin1,2), 2) + 2*rho*(1/b.num_etabin5_isobin1)*(-1*a.num_etabin5_isobin1/pow(b.num_etabin5_isobin1,2))*a.err_etabin5_isobin1*b.err_etabin5_isobin1);
    c.num_etabin5_isobin2 = (a.num_etabin5_isobin2 - b.num_etabin5_isobin2) / b.num_etabin5_isobin2;
    c.err_etabin5_isobin2 = sqrt(pow(a.err_etabin5_isobin2/b.num_etabin5_isobin2, 2) + pow(a.num_etabin5_isobin2*b.err_etabin5_isobin2/pow(b.num_etabin5_isobin2,2), 2) + 2*rho*(1/b.num_etabin5_isobin2)*(-1*a.num_etabin5_isobin2/pow(b.num_etabin5_isobin2,2))*a.err_etabin5_isobin2*b.err_etabin5_isobin2);
    c.num_etabin5_isobin3 = (a.num_etabin5_isobin3 - b.num_etabin5_isobin3) / b.num_etabin5_isobin3;
    c.err_etabin5_isobin3 = sqrt(pow(a.err_etabin5_isobin3/b.num_etabin5_isobin3, 2) + pow(a.num_etabin5_isobin3*b.err_etabin5_isobin3/pow(b.num_etabin5_isobin3,2), 2) + 2*rho*(1/b.num_etabin5_isobin3)*(-1*a.num_etabin5_isobin3/pow(b.num_etabin5_isobin3,2))*a.err_etabin5_isobin3*b.err_etabin5_isobin3);
    c.num_etabin5_isobin4 = (a.num_etabin5_isobin4 - b.num_etabin5_isobin4) / b.num_etabin5_isobin4;
    c.err_etabin5_isobin4 = sqrt(pow(a.err_etabin5_isobin4/b.num_etabin5_isobin4, 2) + pow(a.num_etabin5_isobin4*b.err_etabin5_isobin4/pow(b.num_etabin5_isobin4,2), 2) + 2*rho*(1/b.num_etabin5_isobin4)*(-1*a.num_etabin5_isobin4/pow(b.num_etabin5_isobin4,2))*a.err_etabin5_isobin4*b.err_etabin5_isobin4);
    c.num_etabin5_isobin5 = (a.num_etabin5_isobin5 - b.num_etabin5_isobin5) / b.num_etabin5_isobin5;
    c.err_etabin5_isobin5 = sqrt(pow(a.err_etabin5_isobin5/b.num_etabin5_isobin5, 2) + pow(a.num_etabin5_isobin5*b.err_etabin5_isobin5/pow(b.num_etabin5_isobin5,2), 2) + 2*rho*(1/b.num_etabin5_isobin5)*(-1*a.num_etabin5_isobin5/pow(b.num_etabin5_isobin5,2))*a.err_etabin5_isobin5*b.err_etabin5_isobin5);

    //if (smooth) Smooth(c, draw, name);
    return c;
}

AccCorr GetAccCorrRelDiff(AccCorr &a, AccCorr &b, double rho) { // rho is correlation coefficient
    AccCorr c;
    c.num_acc = (a.num_acc - b.num_acc) / b.num_acc;
    c.err_acc = sqrt(pow(a.err_acc/b.num_acc, 2) + pow(a.num_acc*b.err_acc/pow(b.num_acc,2), 2) + 2*rho*(1/b.num_acc)*(-1*a.num_acc/pow(b.num_acc,2))*a.err_acc*b.err_acc);
    c.num_acc_ptbin1 = (a.num_acc_ptbin1 - b.num_acc_ptbin1) / b.num_acc_ptbin1;
    c.err_acc_ptbin1 = sqrt(pow(a.err_acc_ptbin1/b.num_acc_ptbin1, 2) + pow(a.num_acc_ptbin1*b.err_acc_ptbin1/pow(b.num_acc_ptbin1,2), 2) + 2*rho*(1/b.num_acc_ptbin1)*(-1*a.num_acc_ptbin1/pow(b.num_acc_ptbin1,2))*a.err_acc_ptbin1*b.err_acc_ptbin1);
    c.num_acc_ptbin2 = (a.num_acc_ptbin2 - b.num_acc_ptbin2) / b.num_acc_ptbin2;
    c.err_acc_ptbin2 = sqrt(pow(a.err_acc_ptbin2/b.num_acc_ptbin2, 2) + pow(a.num_acc_ptbin2*b.err_acc_ptbin2/pow(b.num_acc_ptbin2,2), 2) + 2*rho*(1/b.num_acc_ptbin2)*(-1*a.num_acc_ptbin2/pow(b.num_acc_ptbin2,2))*a.err_acc_ptbin2*b.err_acc_ptbin2);
    c.num_acc_ptbin3 = (a.num_acc_ptbin3 - b.num_acc_ptbin3) / b.num_acc_ptbin3;
    c.err_acc_ptbin3 = sqrt(pow(a.err_acc_ptbin3/b.num_acc_ptbin3, 2) + pow(a.num_acc_ptbin3*b.err_acc_ptbin3/pow(b.num_acc_ptbin3,2), 2) + 2*rho*(1/b.num_acc_ptbin3)*(-1*a.num_acc_ptbin3/pow(b.num_acc_ptbin3,2))*a.err_acc_ptbin3*b.err_acc_ptbin3);
    c.num_acc_ptbin4 = (a.num_acc_ptbin4 - b.num_acc_ptbin4) / b.num_acc_ptbin4;
    c.err_acc_ptbin4 = sqrt(pow(a.err_acc_ptbin4/b.num_acc_ptbin4, 2) + pow(a.num_acc_ptbin4*b.err_acc_ptbin4/pow(b.num_acc_ptbin4,2), 2) + 2*rho*(1/b.num_acc_ptbin4)*(-1*a.num_acc_ptbin4/pow(b.num_acc_ptbin4,2))*a.err_acc_ptbin4*b.err_acc_ptbin4);
    c.num_acc_ptbin5 = (a.num_acc_ptbin5 - b.num_acc_ptbin5) / b.num_acc_ptbin5;
    c.err_acc_ptbin5 = sqrt(pow(a.err_acc_ptbin5/b.num_acc_ptbin5, 2) + pow(a.num_acc_ptbin5*b.err_acc_ptbin5/pow(b.num_acc_ptbin5,2), 2) + 2*rho*(1/b.num_acc_ptbin5)*(-1*a.num_acc_ptbin5/pow(b.num_acc_ptbin5,2))*a.err_acc_ptbin5*b.err_acc_ptbin5);
    c.num_acc_etabin1 = (a.num_acc_etabin1 - b.num_acc_etabin1) / b.num_acc_etabin1;
    c.err_acc_etabin1 = sqrt(pow(a.err_acc_etabin1/b.num_acc_etabin1, 2) + pow(a.num_acc_etabin1*b.err_acc_etabin1/pow(b.num_acc_etabin1,2), 2) + 2*rho*(1/b.num_acc_etabin1)*(-1*a.num_acc_etabin1/pow(b.num_acc_etabin1,2))*a.err_acc_etabin1*b.err_acc_etabin1);
    c.num_acc_etabin2 = (a.num_acc_etabin2 - b.num_acc_etabin2) / b.num_acc_etabin2;
    c.err_acc_etabin2 = sqrt(pow(a.err_acc_etabin2/b.num_acc_etabin2, 2) + pow(a.num_acc_etabin2*b.err_acc_etabin2/pow(b.num_acc_etabin2,2), 2) + 2*rho*(1/b.num_acc_etabin2)*(-1*a.num_acc_etabin2/pow(b.num_acc_etabin2,2))*a.err_acc_etabin2*b.err_acc_etabin2);
    c.num_acc_etabin3 = (a.num_acc_etabin3 - b.num_acc_etabin3) / b.num_acc_etabin3;
    c.err_acc_etabin3 = sqrt(pow(a.err_acc_etabin3/b.num_acc_etabin3, 2) + pow(a.num_acc_etabin3*b.err_acc_etabin3/pow(b.num_acc_etabin3,2), 2) + 2*rho*(1/b.num_acc_etabin3)*(-1*a.num_acc_etabin3/pow(b.num_acc_etabin3,2))*a.err_acc_etabin3*b.err_acc_etabin3);
    c.num_acc_etabin4 = (a.num_acc_etabin4 - b.num_acc_etabin4) / b.num_acc_etabin4;
    c.err_acc_etabin4 = sqrt(pow(a.err_acc_etabin4/b.num_acc_etabin4, 2) + pow(a.num_acc_etabin4*b.err_acc_etabin4/pow(b.num_acc_etabin4,2), 2) + 2*rho*(1/b.num_acc_etabin4)*(-1*a.num_acc_etabin4/pow(b.num_acc_etabin4,2))*a.err_acc_etabin4*b.err_acc_etabin4);
    c.num_acc_etabin5 = (a.num_acc_etabin5 - b.num_acc_etabin5) / b.num_acc_etabin5;
    c.err_acc_etabin5 = sqrt(pow(a.err_acc_etabin5/b.num_acc_etabin5, 2) + pow(a.num_acc_etabin5*b.err_acc_etabin5/pow(b.num_acc_etabin5,2), 2) + 2*rho*(1/b.num_acc_etabin5)*(-1*a.num_acc_etabin5/pow(b.num_acc_etabin5,2))*a.err_acc_etabin5*b.err_acc_etabin5);
    c.num_acc2 = (a.num_acc2 - b.num_acc2) / b.num_acc2;
    c.err_acc2 = sqrt(pow(a.err_acc2/b.num_acc2, 2) + pow(a.num_acc2*b.err_acc2/pow(b.num_acc2,2), 2) + 2*rho*(1/b.num_acc2)*(-1*a.num_acc2/pow(b.num_acc2,2))*a.err_acc2*b.err_acc2);
    c.num_acc2_ptbin1 = (a.num_acc2_ptbin1 - b.num_acc2_ptbin1) / b.num_acc2_ptbin1;
    c.err_acc2_ptbin1 = sqrt(pow(a.err_acc2_ptbin1/b.num_acc2_ptbin1, 2) + pow(a.num_acc2_ptbin1*b.err_acc2_ptbin1/pow(b.num_acc2_ptbin1,2), 2) + 2*rho*(1/b.num_acc2_ptbin1)*(-1*a.num_acc2_ptbin1/pow(b.num_acc2_ptbin1,2))*a.err_acc2_ptbin1*b.err_acc2_ptbin1);
    c.num_acc2_ptbin2 = (a.num_acc2_ptbin2 - b.num_acc2_ptbin2) / b.num_acc2_ptbin2;
    c.err_acc2_ptbin2 = sqrt(pow(a.err_acc2_ptbin2/b.num_acc2_ptbin2, 2) + pow(a.num_acc2_ptbin2*b.err_acc2_ptbin2/pow(b.num_acc2_ptbin2,2), 2) + 2*rho*(1/b.num_acc2_ptbin2)*(-1*a.num_acc2_ptbin2/pow(b.num_acc2_ptbin2,2))*a.err_acc2_ptbin2*b.err_acc2_ptbin2);
    c.num_acc2_ptbin3 = (a.num_acc2_ptbin3 - b.num_acc2_ptbin3) / b.num_acc2_ptbin3;
    c.err_acc2_ptbin3 = sqrt(pow(a.err_acc2_ptbin3/b.num_acc2_ptbin3, 2) + pow(a.num_acc2_ptbin3*b.err_acc2_ptbin3/pow(b.num_acc2_ptbin3,2), 2) + 2*rho*(1/b.num_acc2_ptbin3)*(-1*a.num_acc2_ptbin3/pow(b.num_acc2_ptbin3,2))*a.err_acc2_ptbin3*b.err_acc2_ptbin3);
    c.num_acc2_ptbin4 = (a.num_acc2_ptbin4 - b.num_acc2_ptbin4) / b.num_acc2_ptbin4;
    c.err_acc2_ptbin4 = sqrt(pow(a.err_acc2_ptbin4/b.num_acc2_ptbin4, 2) + pow(a.num_acc2_ptbin4*b.err_acc2_ptbin4/pow(b.num_acc2_ptbin4,2), 2) + 2*rho*(1/b.num_acc2_ptbin4)*(-1*a.num_acc2_ptbin4/pow(b.num_acc2_ptbin4,2))*a.err_acc2_ptbin4*b.err_acc2_ptbin4);
    c.num_acc2_ptbin5 = (a.num_acc2_ptbin5 - b.num_acc2_ptbin5) / b.num_acc2_ptbin5;
    c.err_acc2_ptbin5 = sqrt(pow(a.err_acc2_ptbin5/b.num_acc2_ptbin5, 2) + pow(a.num_acc2_ptbin5*b.err_acc2_ptbin5/pow(b.num_acc2_ptbin5,2), 2) + 2*rho*(1/b.num_acc2_ptbin5)*(-1*a.num_acc2_ptbin5/pow(b.num_acc2_ptbin5,2))*a.err_acc2_ptbin5*b.err_acc2_ptbin5);
    c.num_acc2_etabin1 = (a.num_acc2_etabin1 - b.num_acc2_etabin1) / b.num_acc2_etabin1;
    c.err_acc2_etabin1 = sqrt(pow(a.err_acc2_etabin1/b.num_acc2_etabin1, 2) + pow(a.num_acc2_etabin1*b.err_acc2_etabin1/pow(b.num_acc2_etabin1,2), 2) + 2*rho*(1/b.num_acc2_etabin1)*(-1*a.num_acc2_etabin1/pow(b.num_acc2_etabin1,2))*a.err_acc2_etabin1*b.err_acc2_etabin1);
    c.num_acc2_etabin2 = (a.num_acc2_etabin2 - b.num_acc2_etabin2) / b.num_acc2_etabin2;
    c.err_acc2_etabin2 = sqrt(pow(a.err_acc2_etabin2/b.num_acc2_etabin2, 2) + pow(a.num_acc2_etabin2*b.err_acc2_etabin2/pow(b.num_acc2_etabin2,2), 2) + 2*rho*(1/b.num_acc2_etabin2)*(-1*a.num_acc2_etabin2/pow(b.num_acc2_etabin2,2))*a.err_acc2_etabin2*b.err_acc2_etabin2);
    c.num_acc2_etabin3 = (a.num_acc2_etabin3 - b.num_acc2_etabin3) / b.num_acc2_etabin3;
    c.err_acc2_etabin3 = sqrt(pow(a.err_acc2_etabin3/b.num_acc2_etabin3, 2) + pow(a.num_acc2_etabin3*b.err_acc2_etabin3/pow(b.num_acc2_etabin3,2), 2) + 2*rho*(1/b.num_acc2_etabin3)*(-1*a.num_acc2_etabin3/pow(b.num_acc2_etabin3,2))*a.err_acc2_etabin3*b.err_acc2_etabin3);
    c.num_acc2_etabin4 = (a.num_acc2_etabin4 - b.num_acc2_etabin4) / b.num_acc2_etabin4;
    c.err_acc2_etabin4 = sqrt(pow(a.err_acc2_etabin4/b.num_acc2_etabin4, 2) + pow(a.num_acc2_etabin4*b.err_acc2_etabin4/pow(b.num_acc2_etabin4,2), 2) + 2*rho*(1/b.num_acc2_etabin4)*(-1*a.num_acc2_etabin4/pow(b.num_acc2_etabin4,2))*a.err_acc2_etabin4*b.err_acc2_etabin4);
    c.num_acc2_etabin5 = (a.num_acc2_etabin5 - b.num_acc2_etabin5) / b.num_acc2_etabin5;
    c.err_acc2_etabin5 = sqrt(pow(a.err_acc2_etabin5/b.num_acc2_etabin5, 2) + pow(a.num_acc2_etabin5*b.err_acc2_etabin5/pow(b.num_acc2_etabin5,2), 2) + 2*rho*(1/b.num_acc2_etabin5)*(-1*a.num_acc2_etabin5/pow(b.num_acc2_etabin5,2))*a.err_acc2_etabin5*b.err_acc2_etabin5);
    c.num_acc3 = (a.num_acc3 - b.num_acc3) / b.num_acc3;
    c.err_acc3 = sqrt(pow(a.err_acc3/b.num_acc3, 2) + pow(a.num_acc3*b.err_acc3/pow(b.num_acc3,2), 2) + 2*rho*(1/b.num_acc3)*(-1*a.num_acc3/pow(b.num_acc3,2))*a.err_acc3*b.err_acc3);
    c.num_acc3_ptbin1 = (a.num_acc3_ptbin1 - b.num_acc3_ptbin1) / b.num_acc3_ptbin1;
    c.err_acc3_ptbin1 = sqrt(pow(a.err_acc3_ptbin1/b.num_acc3_ptbin1, 2) + pow(a.num_acc3_ptbin1*b.err_acc3_ptbin1/pow(b.num_acc3_ptbin1,2), 2) + 2*rho*(1/b.num_acc3_ptbin1)*(-1*a.num_acc3_ptbin1/pow(b.num_acc3_ptbin1,2))*a.err_acc3_ptbin1*b.err_acc3_ptbin1);
    c.num_acc3_ptbin2 = (a.num_acc3_ptbin2 - b.num_acc3_ptbin2) / b.num_acc3_ptbin2;
    c.err_acc3_ptbin2 = sqrt(pow(a.err_acc3_ptbin2/b.num_acc3_ptbin2, 2) + pow(a.num_acc3_ptbin2*b.err_acc3_ptbin2/pow(b.num_acc3_ptbin2,2), 2) + 2*rho*(1/b.num_acc3_ptbin2)*(-1*a.num_acc3_ptbin2/pow(b.num_acc3_ptbin2,2))*a.err_acc3_ptbin2*b.err_acc3_ptbin2);
    c.num_acc3_ptbin3 = (a.num_acc3_ptbin3 - b.num_acc3_ptbin3) / b.num_acc3_ptbin3;
    c.err_acc3_ptbin3 = sqrt(pow(a.err_acc3_ptbin3/b.num_acc3_ptbin3, 2) + pow(a.num_acc3_ptbin3*b.err_acc3_ptbin3/pow(b.num_acc3_ptbin3,2), 2) + 2*rho*(1/b.num_acc3_ptbin3)*(-1*a.num_acc3_ptbin3/pow(b.num_acc3_ptbin3,2))*a.err_acc3_ptbin3*b.err_acc3_ptbin3);
    c.num_acc3_ptbin4 = (a.num_acc3_ptbin4 - b.num_acc3_ptbin4) / b.num_acc3_ptbin4;
    c.err_acc3_ptbin4 = sqrt(pow(a.err_acc3_ptbin4/b.num_acc3_ptbin4, 2) + pow(a.num_acc3_ptbin4*b.err_acc3_ptbin4/pow(b.num_acc3_ptbin4,2), 2) + 2*rho*(1/b.num_acc3_ptbin4)*(-1*a.num_acc3_ptbin4/pow(b.num_acc3_ptbin4,2))*a.err_acc3_ptbin4*b.err_acc3_ptbin4);
    c.num_acc3_ptbin5 = (a.num_acc3_ptbin5 - b.num_acc3_ptbin5) / b.num_acc3_ptbin5;
    c.err_acc3_ptbin5 = sqrt(pow(a.err_acc3_ptbin5/b.num_acc3_ptbin5, 2) + pow(a.num_acc3_ptbin5*b.err_acc3_ptbin5/pow(b.num_acc3_ptbin5,2), 2) + 2*rho*(1/b.num_acc3_ptbin5)*(-1*a.num_acc3_ptbin5/pow(b.num_acc3_ptbin5,2))*a.err_acc3_ptbin5*b.err_acc3_ptbin5);
    c.num_acc3_etabin1 = (a.num_acc3_etabin1 - b.num_acc3_etabin1) / b.num_acc3_etabin1;
    c.err_acc3_etabin1 = sqrt(pow(a.err_acc3_etabin1/b.num_acc3_etabin1, 2) + pow(a.num_acc3_etabin1*b.err_acc3_etabin1/pow(b.num_acc3_etabin1,2), 2) + 2*rho*(1/b.num_acc3_etabin1)*(-1*a.num_acc3_etabin1/pow(b.num_acc3_etabin1,2))*a.err_acc3_etabin1*b.err_acc3_etabin1);
    c.num_acc3_etabin2 = (a.num_acc3_etabin2 - b.num_acc3_etabin2) / b.num_acc3_etabin2;
    c.err_acc3_etabin2 = sqrt(pow(a.err_acc3_etabin2/b.num_acc3_etabin2, 2) + pow(a.num_acc3_etabin2*b.err_acc3_etabin2/pow(b.num_acc3_etabin2,2), 2) + 2*rho*(1/b.num_acc3_etabin2)*(-1*a.num_acc3_etabin2/pow(b.num_acc3_etabin2,2))*a.err_acc3_etabin2*b.err_acc3_etabin2);
    c.num_acc3_etabin3 = (a.num_acc3_etabin3 - b.num_acc3_etabin3) / b.num_acc3_etabin3;
    c.err_acc3_etabin3 = sqrt(pow(a.err_acc3_etabin3/b.num_acc3_etabin3, 2) + pow(a.num_acc3_etabin3*b.err_acc3_etabin3/pow(b.num_acc3_etabin3,2), 2) + 2*rho*(1/b.num_acc3_etabin3)*(-1*a.num_acc3_etabin3/pow(b.num_acc3_etabin3,2))*a.err_acc3_etabin3*b.err_acc3_etabin3);
    c.num_acc3_etabin4 = (a.num_acc3_etabin4 - b.num_acc3_etabin4) / b.num_acc3_etabin4;
    c.err_acc3_etabin4 = sqrt(pow(a.err_acc3_etabin4/b.num_acc3_etabin4, 2) + pow(a.num_acc3_etabin4*b.err_acc3_etabin4/pow(b.num_acc3_etabin4,2), 2) + 2*rho*(1/b.num_acc3_etabin4)*(-1*a.num_acc3_etabin4/pow(b.num_acc3_etabin4,2))*a.err_acc3_etabin4*b.err_acc3_etabin4);
    c.num_acc3_etabin5 = (a.num_acc3_etabin5 - b.num_acc3_etabin5) / b.num_acc3_etabin5;
    c.err_acc3_etabin5 = sqrt(pow(a.err_acc3_etabin5/b.num_acc3_etabin5, 2) + pow(a.num_acc3_etabin5*b.err_acc3_etabin5/pow(b.num_acc3_etabin5,2), 2) + 2*rho*(1/b.num_acc3_etabin5)*(-1*a.num_acc3_etabin5/pow(b.num_acc3_etabin5,2))*a.err_acc3_etabin5*b.err_acc3_etabin5);
    c.num_corr = (a.num_corr - b.num_corr) / b.num_corr;
    c.err_corr = sqrt(pow(a.err_corr/b.num_corr, 2) + pow(a.num_corr*b.err_corr/pow(b.num_corr,2), 2) + 2*rho*(1/b.num_corr)*(-1*a.num_corr/pow(b.num_corr,2))*a.err_corr*b.err_corr);
    c.num_corr_ptbin1 = (a.num_corr_ptbin1 - b.num_corr_ptbin1) / b.num_corr_ptbin1;
    c.err_corr_ptbin1 = sqrt(pow(a.err_corr_ptbin1/b.num_corr_ptbin1, 2) + pow(a.num_corr_ptbin1*b.err_corr_ptbin1/pow(b.num_corr_ptbin1,2), 2) + 2*rho*(1/b.num_corr_ptbin1)*(-1*a.num_corr_ptbin1/pow(b.num_corr_ptbin1,2))*a.err_corr_ptbin1*b.err_corr_ptbin1);
    c.num_corr_ptbin2 = (a.num_corr_ptbin2 - b.num_corr_ptbin2) / b.num_corr_ptbin2;
    c.err_corr_ptbin2 = sqrt(pow(a.err_corr_ptbin2/b.num_corr_ptbin2, 2) + pow(a.num_corr_ptbin2*b.err_corr_ptbin2/pow(b.num_corr_ptbin2,2), 2) + 2*rho*(1/b.num_corr_ptbin2)*(-1*a.num_corr_ptbin2/pow(b.num_corr_ptbin2,2))*a.err_corr_ptbin2*b.err_corr_ptbin2);
    c.num_corr_ptbin3 = (a.num_corr_ptbin3 - b.num_corr_ptbin3) / b.num_corr_ptbin3;
    c.err_corr_ptbin3 = sqrt(pow(a.err_corr_ptbin3/b.num_corr_ptbin3, 2) + pow(a.num_corr_ptbin3*b.err_corr_ptbin3/pow(b.num_corr_ptbin3,2), 2) + 2*rho*(1/b.num_corr_ptbin3)*(-1*a.num_corr_ptbin3/pow(b.num_corr_ptbin3,2))*a.err_corr_ptbin3*b.err_corr_ptbin3);
    c.num_corr_ptbin4 = (a.num_corr_ptbin4 - b.num_corr_ptbin4) / b.num_corr_ptbin4;
    c.err_corr_ptbin4 = sqrt(pow(a.err_corr_ptbin4/b.num_corr_ptbin4, 2) + pow(a.num_corr_ptbin4*b.err_corr_ptbin4/pow(b.num_corr_ptbin4,2), 2) + 2*rho*(1/b.num_corr_ptbin4)*(-1*a.num_corr_ptbin4/pow(b.num_corr_ptbin4,2))*a.err_corr_ptbin4*b.err_corr_ptbin4);
    c.num_corr_ptbin5 = (a.num_corr_ptbin5 - b.num_corr_ptbin5) / b.num_corr_ptbin5;
    c.err_corr_ptbin5 = sqrt(pow(a.err_corr_ptbin5/b.num_corr_ptbin5, 2) + pow(a.num_corr_ptbin5*b.err_corr_ptbin5/pow(b.num_corr_ptbin5,2), 2) + 2*rho*(1/b.num_corr_ptbin5)*(-1*a.num_corr_ptbin5/pow(b.num_corr_ptbin5,2))*a.err_corr_ptbin5*b.err_corr_ptbin5);
    c.num_corr_etabin1 = (a.num_corr_etabin1 - b.num_corr_etabin1) / b.num_corr_etabin1;
    c.err_corr_etabin1 = sqrt(pow(a.err_corr_etabin1/b.num_corr_etabin1, 2) + pow(a.num_corr_etabin1*b.err_corr_etabin1/pow(b.num_corr_etabin1,2), 2) + 2*rho*(1/b.num_corr_etabin1)*(-1*a.num_corr_etabin1/pow(b.num_corr_etabin1,2))*a.err_corr_etabin1*b.err_corr_etabin1);
    c.num_corr_etabin2 = (a.num_corr_etabin2 - b.num_corr_etabin2) / b.num_corr_etabin2;
    c.err_corr_etabin2 = sqrt(pow(a.err_corr_etabin2/b.num_corr_etabin2, 2) + pow(a.num_corr_etabin2*b.err_corr_etabin2/pow(b.num_corr_etabin2,2), 2) + 2*rho*(1/b.num_corr_etabin2)*(-1*a.num_corr_etabin2/pow(b.num_corr_etabin2,2))*a.err_corr_etabin2*b.err_corr_etabin2);
    c.num_corr_etabin3 = (a.num_corr_etabin3 - b.num_corr_etabin3) / b.num_corr_etabin3;
    c.err_corr_etabin3 = sqrt(pow(a.err_corr_etabin3/b.num_corr_etabin3, 2) + pow(a.num_corr_etabin3*b.err_corr_etabin3/pow(b.num_corr_etabin3,2), 2) + 2*rho*(1/b.num_corr_etabin3)*(-1*a.num_corr_etabin3/pow(b.num_corr_etabin3,2))*a.err_corr_etabin3*b.err_corr_etabin3);
    c.num_corr_etabin4 = (a.num_corr_etabin4 - b.num_corr_etabin4) / b.num_corr_etabin4;
    c.err_corr_etabin4 = sqrt(pow(a.err_corr_etabin4/b.num_corr_etabin4, 2) + pow(a.num_corr_etabin4*b.err_corr_etabin4/pow(b.num_corr_etabin4,2), 2) + 2*rho*(1/b.num_corr_etabin4)*(-1*a.num_corr_etabin4/pow(b.num_corr_etabin4,2))*a.err_corr_etabin4*b.err_corr_etabin4);
    c.num_corr_etabin5 = (a.num_corr_etabin5 - b.num_corr_etabin5) / b.num_corr_etabin5;
    c.err_corr_etabin5 = sqrt(pow(a.err_corr_etabin5/b.num_corr_etabin5, 2) + pow(a.num_corr_etabin5*b.err_corr_etabin5/pow(b.num_corr_etabin5,2), 2) + 2*rho*(1/b.num_corr_etabin5)*(-1*a.num_corr_etabin5/pow(b.num_corr_etabin5,2))*a.err_corr_etabin5*b.err_corr_etabin5);
    c.num_acccorr = (a.num_acccorr - b.num_acccorr) / b.num_acccorr;
    c.err_acccorr = sqrt(pow(a.err_acccorr/b.num_acccorr, 2) + pow(a.num_acccorr*b.err_acccorr/pow(b.num_acccorr,2), 2) + 2*rho*(1/b.num_acccorr)*(-1*a.num_acccorr/pow(b.num_acccorr,2))*a.err_acccorr*b.err_acccorr);
    c.num_acccorr_ptbin1 = (a.num_acccorr_ptbin1 - b.num_acccorr_ptbin1) / b.num_acccorr_ptbin1;
    c.err_acccorr_ptbin1 = sqrt(pow(a.err_acccorr_ptbin1/b.num_acccorr_ptbin1, 2) + pow(a.num_acccorr_ptbin1*b.err_acccorr_ptbin1/pow(b.num_acccorr_ptbin1,2), 2) + 2*rho*(1/b.num_acccorr_ptbin1)*(-1*a.num_acccorr_ptbin1/pow(b.num_acccorr_ptbin1,2))*a.err_acccorr_ptbin1*b.err_acccorr_ptbin1);
    c.num_acccorr_ptbin2 = (a.num_acccorr_ptbin2 - b.num_acccorr_ptbin2) / b.num_acccorr_ptbin2;
    c.err_acccorr_ptbin2 = sqrt(pow(a.err_acccorr_ptbin2/b.num_acccorr_ptbin2, 2) + pow(a.num_acccorr_ptbin2*b.err_acccorr_ptbin2/pow(b.num_acccorr_ptbin2,2), 2) + 2*rho*(1/b.num_acccorr_ptbin2)*(-1*a.num_acccorr_ptbin2/pow(b.num_acccorr_ptbin2,2))*a.err_acccorr_ptbin2*b.err_acccorr_ptbin2);
    c.num_acccorr_ptbin3 = (a.num_acccorr_ptbin3 - b.num_acccorr_ptbin3) / b.num_acccorr_ptbin3;
    c.err_acccorr_ptbin3 = sqrt(pow(a.err_acccorr_ptbin3/b.num_acccorr_ptbin3, 2) + pow(a.num_acccorr_ptbin3*b.err_acccorr_ptbin3/pow(b.num_acccorr_ptbin3,2), 2) + 2*rho*(1/b.num_acccorr_ptbin3)*(-1*a.num_acccorr_ptbin3/pow(b.num_acccorr_ptbin3,2))*a.err_acccorr_ptbin3*b.err_acccorr_ptbin3);
    c.num_acccorr_ptbin4 = (a.num_acccorr_ptbin4 - b.num_acccorr_ptbin4) / b.num_acccorr_ptbin4;
    c.err_acccorr_ptbin4 = sqrt(pow(a.err_acccorr_ptbin4/b.num_acccorr_ptbin4, 2) + pow(a.num_acccorr_ptbin4*b.err_acccorr_ptbin4/pow(b.num_acccorr_ptbin4,2), 2) + 2*rho*(1/b.num_acccorr_ptbin4)*(-1*a.num_acccorr_ptbin4/pow(b.num_acccorr_ptbin4,2))*a.err_acccorr_ptbin4*b.err_acccorr_ptbin4);
    c.num_acccorr_ptbin5 = (a.num_acccorr_ptbin5 - b.num_acccorr_ptbin5) / b.num_acccorr_ptbin5;
    c.err_acccorr_ptbin5 = sqrt(pow(a.err_acccorr_ptbin5/b.num_acccorr_ptbin5, 2) + pow(a.num_acccorr_ptbin5*b.err_acccorr_ptbin5/pow(b.num_acccorr_ptbin5,2), 2) + 2*rho*(1/b.num_acccorr_ptbin5)*(-1*a.num_acccorr_ptbin5/pow(b.num_acccorr_ptbin5,2))*a.err_acccorr_ptbin5*b.err_acccorr_ptbin5);
    c.num_acccorr_etabin1 = (a.num_acccorr_etabin1 - b.num_acccorr_etabin1) / b.num_acccorr_etabin1;
    c.err_acccorr_etabin1 = sqrt(pow(a.err_acccorr_etabin1/b.num_acccorr_etabin1, 2) + pow(a.num_acccorr_etabin1*b.err_acccorr_etabin1/pow(b.num_acccorr_etabin1,2), 2) + 2*rho*(1/b.num_acccorr_etabin1)*(-1*a.num_acccorr_etabin1/pow(b.num_acccorr_etabin1,2))*a.err_acccorr_etabin1*b.err_acccorr_etabin1);
    c.num_acccorr_etabin2 = (a.num_acccorr_etabin2 - b.num_acccorr_etabin2) / b.num_acccorr_etabin2;
    c.err_acccorr_etabin2 = sqrt(pow(a.err_acccorr_etabin2/b.num_acccorr_etabin2, 2) + pow(a.num_acccorr_etabin2*b.err_acccorr_etabin2/pow(b.num_acccorr_etabin2,2), 2) + 2*rho*(1/b.num_acccorr_etabin2)*(-1*a.num_acccorr_etabin2/pow(b.num_acccorr_etabin2,2))*a.err_acccorr_etabin2*b.err_acccorr_etabin2);
    c.num_acccorr_etabin3 = (a.num_acccorr_etabin3 - b.num_acccorr_etabin3) / b.num_acccorr_etabin3;
    c.err_acccorr_etabin3 = sqrt(pow(a.err_acccorr_etabin3/b.num_acccorr_etabin3, 2) + pow(a.num_acccorr_etabin3*b.err_acccorr_etabin3/pow(b.num_acccorr_etabin3,2), 2) + 2*rho*(1/b.num_acccorr_etabin3)*(-1*a.num_acccorr_etabin3/pow(b.num_acccorr_etabin3,2))*a.err_acccorr_etabin3*b.err_acccorr_etabin3);
    c.num_acccorr_etabin4 = (a.num_acccorr_etabin4 - b.num_acccorr_etabin4) / b.num_acccorr_etabin4;
    c.err_acccorr_etabin4 = sqrt(pow(a.err_acccorr_etabin4/b.num_acccorr_etabin4, 2) + pow(a.num_acccorr_etabin4*b.err_acccorr_etabin4/pow(b.num_acccorr_etabin4,2), 2) + 2*rho*(1/b.num_acccorr_etabin4)*(-1*a.num_acccorr_etabin4/pow(b.num_acccorr_etabin4,2))*a.err_acccorr_etabin4*b.err_acccorr_etabin4);
    c.num_acccorr_etabin5 = (a.num_acccorr_etabin5 - b.num_acccorr_etabin5) / b.num_acccorr_etabin5;
    c.err_acccorr_etabin5 = sqrt(pow(a.err_acccorr_etabin5/b.num_acccorr_etabin5, 2) + pow(a.num_acccorr_etabin5*b.err_acccorr_etabin5/pow(b.num_acccorr_etabin5,2), 2) + 2*rho*(1/b.num_acccorr_etabin5)*(-1*a.num_acccorr_etabin5/pow(b.num_acccorr_etabin5,2))*a.err_acccorr_etabin5*b.err_acccorr_etabin5);
    return c;
}

AccRatio GetAccRatioRelDiff(AccRatio &a, AccRatio &b, double rho) { // rho is correlation coefficient
    AccRatio c;
    c.num_accratio = (a.num_accratio - b.num_accratio) / b.num_accratio;
    c.err_accratio = sqrt(pow(a.err_accratio/b.num_accratio, 2) + pow(a.num_accratio*b.err_accratio/pow(b.num_accratio,2), 2) + 2*rho*(1/b.num_accratio)*(-1*a.num_accratio/pow(b.num_accratio,2))*a.err_accratio*b.err_accratio);
    c.num_accratio_ptbin1 = (a.num_accratio_ptbin1 - b.num_accratio_ptbin1) / b.num_accratio_ptbin1;
    c.err_accratio_ptbin1 = sqrt(pow(a.err_accratio_ptbin1/b.num_accratio_ptbin1, 2) + pow(a.num_accratio_ptbin1*b.err_accratio_ptbin1/pow(b.num_accratio_ptbin1,2), 2) + 2*rho*(1/b.num_accratio_ptbin1)*(-1*a.num_accratio_ptbin1/pow(b.num_accratio_ptbin1,2))*a.err_accratio_ptbin1*b.err_accratio_ptbin1);
    c.num_accratio_ptbin2 = (a.num_accratio_ptbin2 - b.num_accratio_ptbin2) / b.num_accratio_ptbin2;
    c.err_accratio_ptbin2 = sqrt(pow(a.err_accratio_ptbin2/b.num_accratio_ptbin2, 2) + pow(a.num_accratio_ptbin2*b.err_accratio_ptbin2/pow(b.num_accratio_ptbin2,2), 2) + 2*rho*(1/b.num_accratio_ptbin2)*(-1*a.num_accratio_ptbin2/pow(b.num_accratio_ptbin2,2))*a.err_accratio_ptbin2*b.err_accratio_ptbin2);
    c.num_accratio_ptbin3 = (a.num_accratio_ptbin3 - b.num_accratio_ptbin3) / b.num_accratio_ptbin3;
    c.err_accratio_ptbin3 = sqrt(pow(a.err_accratio_ptbin3/b.num_accratio_ptbin3, 2) + pow(a.num_accratio_ptbin3*b.err_accratio_ptbin3/pow(b.num_accratio_ptbin3,2), 2) + 2*rho*(1/b.num_accratio_ptbin3)*(-1*a.num_accratio_ptbin3/pow(b.num_accratio_ptbin3,2))*a.err_accratio_ptbin3*b.err_accratio_ptbin3);
    c.num_accratio_ptbin4 = (a.num_accratio_ptbin4 - b.num_accratio_ptbin4) / b.num_accratio_ptbin4;
    c.err_accratio_ptbin4 = sqrt(pow(a.err_accratio_ptbin4/b.num_accratio_ptbin4, 2) + pow(a.num_accratio_ptbin4*b.err_accratio_ptbin4/pow(b.num_accratio_ptbin4,2), 2) + 2*rho*(1/b.num_accratio_ptbin4)*(-1*a.num_accratio_ptbin4/pow(b.num_accratio_ptbin4,2))*a.err_accratio_ptbin4*b.err_accratio_ptbin4);
    c.num_accratio_ptbin5 = (a.num_accratio_ptbin5 - b.num_accratio_ptbin5) / b.num_accratio_ptbin5;
    c.err_accratio_ptbin5 = sqrt(pow(a.err_accratio_ptbin5/b.num_accratio_ptbin5, 2) + pow(a.num_accratio_ptbin5*b.err_accratio_ptbin5/pow(b.num_accratio_ptbin5,2), 2) + 2*rho*(1/b.num_accratio_ptbin5)*(-1*a.num_accratio_ptbin5/pow(b.num_accratio_ptbin5,2))*a.err_accratio_ptbin5*b.err_accratio_ptbin5);
    c.num_accratio_etabin1 = (a.num_accratio_etabin1 - b.num_accratio_etabin1) / b.num_accratio_etabin1;
    c.err_accratio_etabin1 = sqrt(pow(a.err_accratio_etabin1/b.num_accratio_etabin1, 2) + pow(a.num_accratio_etabin1*b.err_accratio_etabin1/pow(b.num_accratio_etabin1,2), 2) + 2*rho*(1/b.num_accratio_etabin1)*(-1*a.num_accratio_etabin1/pow(b.num_accratio_etabin1,2))*a.err_accratio_etabin1*b.err_accratio_etabin1);
    c.num_accratio_etabin2 = (a.num_accratio_etabin2 - b.num_accratio_etabin2) / b.num_accratio_etabin2;
    c.err_accratio_etabin2 = sqrt(pow(a.err_accratio_etabin2/b.num_accratio_etabin2, 2) + pow(a.num_accratio_etabin2*b.err_accratio_etabin2/pow(b.num_accratio_etabin2,2), 2) + 2*rho*(1/b.num_accratio_etabin2)*(-1*a.num_accratio_etabin2/pow(b.num_accratio_etabin2,2))*a.err_accratio_etabin2*b.err_accratio_etabin2);
    c.num_accratio_etabin3 = (a.num_accratio_etabin3 - b.num_accratio_etabin3) / b.num_accratio_etabin3;
    c.err_accratio_etabin3 = sqrt(pow(a.err_accratio_etabin3/b.num_accratio_etabin3, 2) + pow(a.num_accratio_etabin3*b.err_accratio_etabin3/pow(b.num_accratio_etabin3,2), 2) + 2*rho*(1/b.num_accratio_etabin3)*(-1*a.num_accratio_etabin3/pow(b.num_accratio_etabin3,2))*a.err_accratio_etabin3*b.err_accratio_etabin3);
    c.num_accratio_etabin4 = (a.num_accratio_etabin4 - b.num_accratio_etabin4) / b.num_accratio_etabin4;
    c.err_accratio_etabin4 = sqrt(pow(a.err_accratio_etabin4/b.num_accratio_etabin4, 2) + pow(a.num_accratio_etabin4*b.err_accratio_etabin4/pow(b.num_accratio_etabin4,2), 2) + 2*rho*(1/b.num_accratio_etabin4)*(-1*a.num_accratio_etabin4/pow(b.num_accratio_etabin4,2))*a.err_accratio_etabin4*b.err_accratio_etabin4);
    c.num_accratio_etabin5 = (a.num_accratio_etabin5 - b.num_accratio_etabin5) / b.num_accratio_etabin5;
    c.err_accratio_etabin5 = sqrt(pow(a.err_accratio_etabin5/b.num_accratio_etabin5, 2) + pow(a.num_accratio_etabin5*b.err_accratio_etabin5/pow(b.num_accratio_etabin5,2), 2) + 2*rho*(1/b.num_accratio_etabin5)*(-1*a.num_accratio_etabin5/pow(b.num_accratio_etabin5,2))*a.err_accratio_etabin5*b.err_accratio_etabin5);
    return c;
}

Counter GetCounterSymmetrized(Counter &up, Counter &dn) {
    Counter c;

    if (fabs(up.num_all) > fabs(dn.num_all)) {c.num_all = up.num_all; c.err_all = up.err_all;}
    else {c.num_all = -1*dn.num_all; c.err_all = -1*dn.num_all;}
    if (fabs(up.num_total) > fabs(dn.num_total)) {c.num_total = up.num_total; c.err_total = up.err_total;}
    else {c.num_total = -1*dn.num_total; c.err_total = -1*dn.num_total;}
    if (fabs(up.num_total_ptbin1) > fabs(dn.num_total_ptbin1)) {c.num_total_ptbin1 = up.num_total_ptbin1; c.err_total_ptbin1 = up.err_total_ptbin1;}
    else {c.num_total_ptbin1 = -1*dn.num_total_ptbin1; c.err_total_ptbin1 = -1*dn.num_total_ptbin1;}
    if (fabs(up.num_total_ptbin2) > fabs(dn.num_total_ptbin2)) {c.num_total_ptbin2 = up.num_total_ptbin2; c.err_total_ptbin2 = up.err_total_ptbin2;}
    else {c.num_total_ptbin2 = -1*dn.num_total_ptbin2; c.err_total_ptbin2 = -1*dn.num_total_ptbin2;}
    if (fabs(up.num_total_ptbin3) > fabs(dn.num_total_ptbin3)) {c.num_total_ptbin3 = up.num_total_ptbin3; c.err_total_ptbin3 = up.err_total_ptbin3;}
    else {c.num_total_ptbin3 = -1*dn.num_total_ptbin3; c.err_total_ptbin3 = -1*dn.num_total_ptbin3;}
    if (fabs(up.num_total_ptbin4) > fabs(dn.num_total_ptbin4)) {c.num_total_ptbin4 = up.num_total_ptbin4; c.err_total_ptbin4 = up.err_total_ptbin4;}
    else {c.num_total_ptbin4 = -1*dn.num_total_ptbin4; c.err_total_ptbin4 = -1*dn.num_total_ptbin4;}
    if (fabs(up.num_total_ptbin5) > fabs(dn.num_total_ptbin5)) {c.num_total_ptbin5 = up.num_total_ptbin5; c.err_total_ptbin5 = up.err_total_ptbin5;}
    else {c.num_total_ptbin5 = -1*dn.num_total_ptbin5; c.err_total_ptbin5 = -1*dn.num_total_ptbin5;}
    if (fabs(up.num_total_etabin1) > fabs(dn.num_total_etabin1)) {c.num_total_etabin1 = up.num_total_etabin1; c.err_total_etabin1 = up.err_total_etabin1;}
    else {c.num_total_etabin1 = -1*dn.num_total_etabin1; c.err_total_etabin1 = -1*dn.num_total_etabin1;}
    if (fabs(up.num_total_etabin2) > fabs(dn.num_total_etabin2)) {c.num_total_etabin2 = up.num_total_etabin2; c.err_total_etabin2 = up.err_total_etabin2;}
    else {c.num_total_etabin2 = -1*dn.num_total_etabin2; c.err_total_etabin2 = -1*dn.num_total_etabin2;}
    if (fabs(up.num_total_etabin3) > fabs(dn.num_total_etabin3)) {c.num_total_etabin3 = up.num_total_etabin3; c.err_total_etabin3 = up.err_total_etabin3;}
    else {c.num_total_etabin3 = -1*dn.num_total_etabin3; c.err_total_etabin3 = -1*dn.num_total_etabin3;}
    if (fabs(up.num_total_etabin4) > fabs(dn.num_total_etabin4)) {c.num_total_etabin4 = up.num_total_etabin4; c.err_total_etabin4 = up.err_total_etabin4;}
    else {c.num_total_etabin4 = -1*dn.num_total_etabin4; c.err_total_etabin4 = -1*dn.num_total_etabin4;}
    if (fabs(up.num_total_etabin5) > fabs(dn.num_total_etabin5)) {c.num_total_etabin5 = up.num_total_etabin5; c.err_total_etabin5 = up.err_total_etabin5;}
    else {c.num_total_etabin5 = -1*dn.num_total_etabin5; c.err_total_etabin5 = -1*dn.num_total_etabin5;}
    if (fabs(up.num_fidu) > fabs(dn.num_fidu)) {c.num_fidu = up.num_fidu; c.err_fidu = up.err_fidu;}
    else {c.num_fidu = -1*dn.num_fidu; c.err_fidu = -1*dn.num_fidu;}
    if (fabs(up.num_fidu_ptbin1) > fabs(dn.num_fidu_ptbin1)) {c.num_fidu_ptbin1 = up.num_fidu_ptbin1; c.err_fidu_ptbin1 = up.err_fidu_ptbin1;}
    else {c.num_fidu_ptbin1 = -1*dn.num_fidu_ptbin1; c.err_fidu_ptbin1 = -1*dn.num_fidu_ptbin1;}
    if (fabs(up.num_fidu_ptbin2) > fabs(dn.num_fidu_ptbin2)) {c.num_fidu_ptbin2 = up.num_fidu_ptbin2; c.err_fidu_ptbin2 = up.err_fidu_ptbin2;}
    else {c.num_fidu_ptbin2 = -1*dn.num_fidu_ptbin2; c.err_fidu_ptbin2 = -1*dn.num_fidu_ptbin2;}
    if (fabs(up.num_fidu_ptbin3) > fabs(dn.num_fidu_ptbin3)) {c.num_fidu_ptbin3 = up.num_fidu_ptbin3; c.err_fidu_ptbin3 = up.err_fidu_ptbin3;}
    else {c.num_fidu_ptbin3 = -1*dn.num_fidu_ptbin3; c.err_fidu_ptbin3 = -1*dn.num_fidu_ptbin3;}
    if (fabs(up.num_fidu_ptbin4) > fabs(dn.num_fidu_ptbin4)) {c.num_fidu_ptbin4 = up.num_fidu_ptbin4; c.err_fidu_ptbin4 = up.err_fidu_ptbin4;}
    else {c.num_fidu_ptbin4 = -1*dn.num_fidu_ptbin4; c.err_fidu_ptbin4 = -1*dn.num_fidu_ptbin4;}
    if (fabs(up.num_fidu_ptbin5) > fabs(dn.num_fidu_ptbin5)) {c.num_fidu_ptbin5 = up.num_fidu_ptbin5; c.err_fidu_ptbin5 = up.err_fidu_ptbin5;}
    else {c.num_fidu_ptbin5 = -1*dn.num_fidu_ptbin5; c.err_fidu_ptbin5 = -1*dn.num_fidu_ptbin5;}
    if (fabs(up.num_fidu_etabin1) > fabs(dn.num_fidu_etabin1)) {c.num_fidu_etabin1 = up.num_fidu_etabin1; c.err_fidu_etabin1 = up.err_fidu_etabin1;}
    else {c.num_fidu_etabin1 = -1*dn.num_fidu_etabin1; c.err_fidu_etabin1 = -1*dn.num_fidu_etabin1;}
    if (fabs(up.num_fidu_etabin2) > fabs(dn.num_fidu_etabin2)) {c.num_fidu_etabin2 = up.num_fidu_etabin2; c.err_fidu_etabin2 = up.err_fidu_etabin2;}
    else {c.num_fidu_etabin2 = -1*dn.num_fidu_etabin2; c.err_fidu_etabin2 = -1*dn.num_fidu_etabin2;}
    if (fabs(up.num_fidu_etabin3) > fabs(dn.num_fidu_etabin3)) {c.num_fidu_etabin3 = up.num_fidu_etabin3; c.err_fidu_etabin3 = up.err_fidu_etabin3;}
    else {c.num_fidu_etabin3 = -1*dn.num_fidu_etabin3; c.err_fidu_etabin3 = -1*dn.num_fidu_etabin3;}
    if (fabs(up.num_fidu_etabin4) > fabs(dn.num_fidu_etabin4)) {c.num_fidu_etabin4 = up.num_fidu_etabin4; c.err_fidu_etabin4 = up.err_fidu_etabin4;}
    else {c.num_fidu_etabin4 = -1*dn.num_fidu_etabin4; c.err_fidu_etabin4 = -1*dn.num_fidu_etabin4;}
    if (fabs(up.num_fidu_etabin5) > fabs(dn.num_fidu_etabin5)) {c.num_fidu_etabin5 = up.num_fidu_etabin5; c.err_fidu_etabin5 = up.err_fidu_etabin5;}
    else {c.num_fidu_etabin5 = -1*dn.num_fidu_etabin5; c.err_fidu_etabin5 = -1*dn.num_fidu_etabin5;}
    if (fabs(up.num_isobin1) > fabs(dn.num_isobin1)) {c.num_isobin1 = up.num_isobin1; c.err_isobin1 = up.err_isobin1;}
    else {c.num_isobin1 = -1*dn.num_isobin1; c.err_isobin1 = -1*dn.num_isobin1;}
    if (fabs(up.num_isobin2) > fabs(dn.num_isobin2)) {c.num_isobin2 = up.num_isobin2; c.err_isobin2 = up.err_isobin2;}
    else {c.num_isobin2 = -1*dn.num_isobin2; c.err_isobin2 = -1*dn.num_isobin2;}
    if (fabs(up.num_isobin3) > fabs(dn.num_isobin3)) {c.num_isobin3 = up.num_isobin3; c.err_isobin3 = up.err_isobin3;}
    else {c.num_isobin3 = -1*dn.num_isobin3; c.err_isobin3 = -1*dn.num_isobin3;}
    if (fabs(up.num_isobin4) > fabs(dn.num_isobin4)) {c.num_isobin4 = up.num_isobin4; c.err_isobin4 = up.err_isobin4;}
    else {c.num_isobin4 = -1*dn.num_isobin4; c.err_isobin4 = -1*dn.num_isobin4;}
    if (fabs(up.num_isobin5) > fabs(dn.num_isobin5)) {c.num_isobin5 = up.num_isobin5; c.err_isobin5 = up.err_isobin5;}
    else {c.num_isobin5 = -1*dn.num_isobin5; c.err_isobin5 = -1*dn.num_isobin5;}
    if (fabs(up.num_ptbin1_isobin1) > fabs(dn.num_ptbin1_isobin1)) {c.num_ptbin1_isobin1 = up.num_ptbin1_isobin1; c.err_ptbin1_isobin1 = up.err_ptbin1_isobin1;}
    else {c.num_ptbin1_isobin1 = -1*dn.num_ptbin1_isobin1; c.err_ptbin1_isobin1 = -1*dn.num_ptbin1_isobin1;}
    if (fabs(up.num_ptbin1_isobin2) > fabs(dn.num_ptbin1_isobin2)) {c.num_ptbin1_isobin2 = up.num_ptbin1_isobin2; c.err_ptbin1_isobin2 = up.err_ptbin1_isobin2;}
    else {c.num_ptbin1_isobin2 = -1*dn.num_ptbin1_isobin2; c.err_ptbin1_isobin2 = -1*dn.num_ptbin1_isobin2;}
    if (fabs(up.num_ptbin1_isobin3) > fabs(dn.num_ptbin1_isobin3)) {c.num_ptbin1_isobin3 = up.num_ptbin1_isobin3; c.err_ptbin1_isobin3 = up.err_ptbin1_isobin3;}
    else {c.num_ptbin1_isobin3 = -1*dn.num_ptbin1_isobin3; c.err_ptbin1_isobin3 = -1*dn.num_ptbin1_isobin3;}
    if (fabs(up.num_ptbin1_isobin4) > fabs(dn.num_ptbin1_isobin4)) {c.num_ptbin1_isobin4 = up.num_ptbin1_isobin4; c.err_ptbin1_isobin4 = up.err_ptbin1_isobin4;}
    else {c.num_ptbin1_isobin4 = -1*dn.num_ptbin1_isobin4; c.err_ptbin1_isobin4 = -1*dn.num_ptbin1_isobin4;}
    if (fabs(up.num_ptbin1_isobin5) > fabs(dn.num_ptbin1_isobin5)) {c.num_ptbin1_isobin5 = up.num_ptbin1_isobin5; c.err_ptbin1_isobin5 = up.err_ptbin1_isobin5;}
    else {c.num_ptbin1_isobin5 = -1*dn.num_ptbin1_isobin5; c.err_ptbin1_isobin5 = -1*dn.num_ptbin1_isobin5;}
    if (fabs(up.num_ptbin2_isobin1) > fabs(dn.num_ptbin2_isobin1)) {c.num_ptbin2_isobin1 = up.num_ptbin2_isobin1; c.err_ptbin2_isobin1 = up.err_ptbin2_isobin1;}
    else {c.num_ptbin2_isobin1 = -1*dn.num_ptbin2_isobin1; c.err_ptbin2_isobin1 = -1*dn.num_ptbin2_isobin1;}
    if (fabs(up.num_ptbin2_isobin2) > fabs(dn.num_ptbin2_isobin2)) {c.num_ptbin2_isobin2 = up.num_ptbin2_isobin2; c.err_ptbin2_isobin2 = up.err_ptbin2_isobin2;}
    else {c.num_ptbin2_isobin2 = -1*dn.num_ptbin2_isobin2; c.err_ptbin2_isobin2 = -1*dn.num_ptbin2_isobin2;}
    if (fabs(up.num_ptbin2_isobin3) > fabs(dn.num_ptbin2_isobin3)) {c.num_ptbin2_isobin3 = up.num_ptbin2_isobin3; c.err_ptbin2_isobin3 = up.err_ptbin2_isobin3;}
    else {c.num_ptbin2_isobin3 = -1*dn.num_ptbin2_isobin3; c.err_ptbin2_isobin3 = -1*dn.num_ptbin2_isobin3;}
    if (fabs(up.num_ptbin2_isobin4) > fabs(dn.num_ptbin2_isobin4)) {c.num_ptbin2_isobin4 = up.num_ptbin2_isobin4; c.err_ptbin2_isobin4 = up.err_ptbin2_isobin4;}
    else {c.num_ptbin2_isobin4 = -1*dn.num_ptbin2_isobin4; c.err_ptbin2_isobin4 = -1*dn.num_ptbin2_isobin4;}
    if (fabs(up.num_ptbin2_isobin5) > fabs(dn.num_ptbin2_isobin5)) {c.num_ptbin2_isobin5 = up.num_ptbin2_isobin5; c.err_ptbin2_isobin5 = up.err_ptbin2_isobin5;}
    else {c.num_ptbin2_isobin5 = -1*dn.num_ptbin2_isobin5; c.err_ptbin2_isobin5 = -1*dn.num_ptbin2_isobin5;}
    if (fabs(up.num_ptbin3_isobin1) > fabs(dn.num_ptbin3_isobin1)) {c.num_ptbin3_isobin1 = up.num_ptbin3_isobin1; c.err_ptbin3_isobin1 = up.err_ptbin3_isobin1;}
    else {c.num_ptbin3_isobin1 = -1*dn.num_ptbin3_isobin1; c.err_ptbin3_isobin1 = -1*dn.num_ptbin3_isobin1;}
    if (fabs(up.num_ptbin3_isobin2) > fabs(dn.num_ptbin3_isobin2)) {c.num_ptbin3_isobin2 = up.num_ptbin3_isobin2; c.err_ptbin3_isobin2 = up.err_ptbin3_isobin2;}
    else {c.num_ptbin3_isobin2 = -1*dn.num_ptbin3_isobin2; c.err_ptbin3_isobin2 = -1*dn.num_ptbin3_isobin2;}
    if (fabs(up.num_ptbin3_isobin3) > fabs(dn.num_ptbin3_isobin3)) {c.num_ptbin3_isobin3 = up.num_ptbin3_isobin3; c.err_ptbin3_isobin3 = up.err_ptbin3_isobin3;}
    else {c.num_ptbin3_isobin3 = -1*dn.num_ptbin3_isobin3; c.err_ptbin3_isobin3 = -1*dn.num_ptbin3_isobin3;}
    if (fabs(up.num_ptbin3_isobin4) > fabs(dn.num_ptbin3_isobin4)) {c.num_ptbin3_isobin4 = up.num_ptbin3_isobin4; c.err_ptbin3_isobin4 = up.err_ptbin3_isobin4;}
    else {c.num_ptbin3_isobin4 = -1*dn.num_ptbin3_isobin4; c.err_ptbin3_isobin4 = -1*dn.num_ptbin3_isobin4;}
    if (fabs(up.num_ptbin3_isobin5) > fabs(dn.num_ptbin3_isobin5)) {c.num_ptbin3_isobin5 = up.num_ptbin3_isobin5; c.err_ptbin3_isobin5 = up.err_ptbin3_isobin5;}
    else {c.num_ptbin3_isobin5 = -1*dn.num_ptbin3_isobin5; c.err_ptbin3_isobin5 = -1*dn.num_ptbin3_isobin5;}
    if (fabs(up.num_ptbin4_isobin1) > fabs(dn.num_ptbin4_isobin1)) {c.num_ptbin4_isobin1 = up.num_ptbin4_isobin1; c.err_ptbin4_isobin1 = up.err_ptbin4_isobin1;}
    else {c.num_ptbin4_isobin1 = -1*dn.num_ptbin4_isobin1; c.err_ptbin4_isobin1 = -1*dn.num_ptbin4_isobin1;}
    if (fabs(up.num_ptbin4_isobin2) > fabs(dn.num_ptbin4_isobin2)) {c.num_ptbin4_isobin2 = up.num_ptbin4_isobin2; c.err_ptbin4_isobin2 = up.err_ptbin4_isobin2;}
    else {c.num_ptbin4_isobin2 = -1*dn.num_ptbin4_isobin2; c.err_ptbin4_isobin2 = -1*dn.num_ptbin4_isobin2;}
    if (fabs(up.num_ptbin4_isobin3) > fabs(dn.num_ptbin4_isobin3)) {c.num_ptbin4_isobin3 = up.num_ptbin4_isobin3; c.err_ptbin4_isobin3 = up.err_ptbin4_isobin3;}
    else {c.num_ptbin4_isobin3 = -1*dn.num_ptbin4_isobin3; c.err_ptbin4_isobin3 = -1*dn.num_ptbin4_isobin3;}
    if (fabs(up.num_ptbin4_isobin4) > fabs(dn.num_ptbin4_isobin4)) {c.num_ptbin4_isobin4 = up.num_ptbin4_isobin4; c.err_ptbin4_isobin4 = up.err_ptbin4_isobin4;}
    else {c.num_ptbin4_isobin4 = -1*dn.num_ptbin4_isobin4; c.err_ptbin4_isobin4 = -1*dn.num_ptbin4_isobin4;}
    if (fabs(up.num_ptbin4_isobin5) > fabs(dn.num_ptbin4_isobin5)) {c.num_ptbin4_isobin5 = up.num_ptbin4_isobin5; c.err_ptbin4_isobin5 = up.err_ptbin4_isobin5;}
    else {c.num_ptbin4_isobin5 = -1*dn.num_ptbin4_isobin5; c.err_ptbin4_isobin5 = -1*dn.num_ptbin4_isobin5;}
    if (fabs(up.num_ptbin5_isobin1) > fabs(dn.num_ptbin5_isobin1)) {c.num_ptbin5_isobin1 = up.num_ptbin5_isobin1; c.err_ptbin5_isobin1 = up.err_ptbin5_isobin1;}
    else {c.num_ptbin5_isobin1 = -1*dn.num_ptbin5_isobin1; c.err_ptbin5_isobin1 = -1*dn.num_ptbin5_isobin1;}
    if (fabs(up.num_ptbin5_isobin2) > fabs(dn.num_ptbin5_isobin2)) {c.num_ptbin5_isobin2 = up.num_ptbin5_isobin2; c.err_ptbin5_isobin2 = up.err_ptbin5_isobin2;}
    else {c.num_ptbin5_isobin2 = -1*dn.num_ptbin5_isobin2; c.err_ptbin5_isobin2 = -1*dn.num_ptbin5_isobin2;}
    if (fabs(up.num_ptbin5_isobin3) > fabs(dn.num_ptbin5_isobin3)) {c.num_ptbin5_isobin3 = up.num_ptbin5_isobin3; c.err_ptbin5_isobin3 = up.err_ptbin5_isobin3;}
    else {c.num_ptbin5_isobin3 = -1*dn.num_ptbin5_isobin3; c.err_ptbin5_isobin3 = -1*dn.num_ptbin5_isobin3;}
    if (fabs(up.num_ptbin5_isobin4) > fabs(dn.num_ptbin5_isobin4)) {c.num_ptbin5_isobin4 = up.num_ptbin5_isobin4; c.err_ptbin5_isobin4 = up.err_ptbin5_isobin4;}
    else {c.num_ptbin5_isobin4 = -1*dn.num_ptbin5_isobin4; c.err_ptbin5_isobin4 = -1*dn.num_ptbin5_isobin4;}
    if (fabs(up.num_ptbin5_isobin5) > fabs(dn.num_ptbin5_isobin5)) {c.num_ptbin5_isobin5 = up.num_ptbin5_isobin5; c.err_ptbin5_isobin5 = up.err_ptbin5_isobin5;}
    else {c.num_ptbin5_isobin5 = -1*dn.num_ptbin5_isobin5; c.err_ptbin5_isobin5 = -1*dn.num_ptbin5_isobin5;}
    if (fabs(up.num_etabin1_isobin1) > fabs(dn.num_etabin1_isobin1)) {c.num_etabin1_isobin1 = up.num_etabin1_isobin1; c.err_etabin1_isobin1 = up.err_etabin1_isobin1;}
    else {c.num_etabin1_isobin1 = -1*dn.num_etabin1_isobin1; c.err_etabin1_isobin1 = -1*dn.num_etabin1_isobin1;}
    if (fabs(up.num_etabin1_isobin2) > fabs(dn.num_etabin1_isobin2)) {c.num_etabin1_isobin2 = up.num_etabin1_isobin2; c.err_etabin1_isobin2 = up.err_etabin1_isobin2;}
    else {c.num_etabin1_isobin2 = -1*dn.num_etabin1_isobin2; c.err_etabin1_isobin2 = -1*dn.num_etabin1_isobin2;}
    if (fabs(up.num_etabin1_isobin3) > fabs(dn.num_etabin1_isobin3)) {c.num_etabin1_isobin3 = up.num_etabin1_isobin3; c.err_etabin1_isobin3 = up.err_etabin1_isobin3;}
    else {c.num_etabin1_isobin3 = -1*dn.num_etabin1_isobin3; c.err_etabin1_isobin3 = -1*dn.num_etabin1_isobin3;}
    if (fabs(up.num_etabin1_isobin4) > fabs(dn.num_etabin1_isobin4)) {c.num_etabin1_isobin4 = up.num_etabin1_isobin4; c.err_etabin1_isobin4 = up.err_etabin1_isobin4;}
    else {c.num_etabin1_isobin4 = -1*dn.num_etabin1_isobin4; c.err_etabin1_isobin4 = -1*dn.num_etabin1_isobin4;}
    if (fabs(up.num_etabin1_isobin5) > fabs(dn.num_etabin1_isobin5)) {c.num_etabin1_isobin5 = up.num_etabin1_isobin5; c.err_etabin1_isobin5 = up.err_etabin1_isobin5;}
    else {c.num_etabin1_isobin5 = -1*dn.num_etabin1_isobin5; c.err_etabin1_isobin5 = -1*dn.num_etabin1_isobin5;}
    if (fabs(up.num_etabin2_isobin1) > fabs(dn.num_etabin2_isobin1)) {c.num_etabin2_isobin1 = up.num_etabin2_isobin1; c.err_etabin2_isobin1 = up.err_etabin2_isobin1;}
    else {c.num_etabin2_isobin1 = -1*dn.num_etabin2_isobin1; c.err_etabin2_isobin1 = -1*dn.num_etabin2_isobin1;}
    if (fabs(up.num_etabin2_isobin2) > fabs(dn.num_etabin2_isobin2)) {c.num_etabin2_isobin2 = up.num_etabin2_isobin2; c.err_etabin2_isobin2 = up.err_etabin2_isobin2;}
    else {c.num_etabin2_isobin2 = -1*dn.num_etabin2_isobin2; c.err_etabin2_isobin2 = -1*dn.num_etabin2_isobin2;}
    if (fabs(up.num_etabin2_isobin3) > fabs(dn.num_etabin2_isobin3)) {c.num_etabin2_isobin3 = up.num_etabin2_isobin3; c.err_etabin2_isobin3 = up.err_etabin2_isobin3;}
    else {c.num_etabin2_isobin3 = -1*dn.num_etabin2_isobin3; c.err_etabin2_isobin3 = -1*dn.num_etabin2_isobin3;}
    if (fabs(up.num_etabin2_isobin4) > fabs(dn.num_etabin2_isobin4)) {c.num_etabin2_isobin4 = up.num_etabin2_isobin4; c.err_etabin2_isobin4 = up.err_etabin2_isobin4;}
    else {c.num_etabin2_isobin4 = -1*dn.num_etabin2_isobin4; c.err_etabin2_isobin4 = -1*dn.num_etabin2_isobin4;}
    if (fabs(up.num_etabin2_isobin5) > fabs(dn.num_etabin2_isobin5)) {c.num_etabin2_isobin5 = up.num_etabin2_isobin5; c.err_etabin2_isobin5 = up.err_etabin2_isobin5;}
    else {c.num_etabin2_isobin5 = -1*dn.num_etabin2_isobin5; c.err_etabin2_isobin5 = -1*dn.num_etabin2_isobin5;}
    if (fabs(up.num_etabin3_isobin1) > fabs(dn.num_etabin3_isobin1)) {c.num_etabin3_isobin1 = up.num_etabin3_isobin1; c.err_etabin3_isobin1 = up.err_etabin3_isobin1;}
    else {c.num_etabin3_isobin1 = -1*dn.num_etabin3_isobin1; c.err_etabin3_isobin1 = -1*dn.num_etabin3_isobin1;}
    if (fabs(up.num_etabin3_isobin2) > fabs(dn.num_etabin3_isobin2)) {c.num_etabin3_isobin2 = up.num_etabin3_isobin2; c.err_etabin3_isobin2 = up.err_etabin3_isobin2;}
    else {c.num_etabin3_isobin2 = -1*dn.num_etabin3_isobin2; c.err_etabin3_isobin2 = -1*dn.num_etabin3_isobin2;}
    if (fabs(up.num_etabin3_isobin3) > fabs(dn.num_etabin3_isobin3)) {c.num_etabin3_isobin3 = up.num_etabin3_isobin3; c.err_etabin3_isobin3 = up.err_etabin3_isobin3;}
    else {c.num_etabin3_isobin3 = -1*dn.num_etabin3_isobin3; c.err_etabin3_isobin3 = -1*dn.num_etabin3_isobin3;}
    if (fabs(up.num_etabin3_isobin4) > fabs(dn.num_etabin3_isobin4)) {c.num_etabin3_isobin4 = up.num_etabin3_isobin4; c.err_etabin3_isobin4 = up.err_etabin3_isobin4;}
    else {c.num_etabin3_isobin4 = -1*dn.num_etabin3_isobin4; c.err_etabin3_isobin4 = -1*dn.num_etabin3_isobin4;}
    if (fabs(up.num_etabin3_isobin5) > fabs(dn.num_etabin3_isobin5)) {c.num_etabin3_isobin5 = up.num_etabin3_isobin5; c.err_etabin3_isobin5 = up.err_etabin3_isobin5;}
    else {c.num_etabin3_isobin5 = -1*dn.num_etabin3_isobin5; c.err_etabin3_isobin5 = -1*dn.num_etabin3_isobin5;}
    if (fabs(up.num_etabin4_isobin1) > fabs(dn.num_etabin4_isobin1)) {c.num_etabin4_isobin1 = up.num_etabin4_isobin1; c.err_etabin4_isobin1 = up.err_etabin4_isobin1;}
    else {c.num_etabin4_isobin1 = -1*dn.num_etabin4_isobin1; c.err_etabin4_isobin1 = -1*dn.num_etabin4_isobin1;}
    if (fabs(up.num_etabin4_isobin2) > fabs(dn.num_etabin4_isobin2)) {c.num_etabin4_isobin2 = up.num_etabin4_isobin2; c.err_etabin4_isobin2 = up.err_etabin4_isobin2;}
    else {c.num_etabin4_isobin2 = -1*dn.num_etabin4_isobin2; c.err_etabin4_isobin2 = -1*dn.num_etabin4_isobin2;}
    if (fabs(up.num_etabin4_isobin3) > fabs(dn.num_etabin4_isobin3)) {c.num_etabin4_isobin3 = up.num_etabin4_isobin3; c.err_etabin4_isobin3 = up.err_etabin4_isobin3;}
    else {c.num_etabin4_isobin3 = -1*dn.num_etabin4_isobin3; c.err_etabin4_isobin3 = -1*dn.num_etabin4_isobin3;}
    if (fabs(up.num_etabin4_isobin4) > fabs(dn.num_etabin4_isobin4)) {c.num_etabin4_isobin4 = up.num_etabin4_isobin4; c.err_etabin4_isobin4 = up.err_etabin4_isobin4;}
    else {c.num_etabin4_isobin4 = -1*dn.num_etabin4_isobin4; c.err_etabin4_isobin4 = -1*dn.num_etabin4_isobin4;}
    if (fabs(up.num_etabin4_isobin5) > fabs(dn.num_etabin4_isobin5)) {c.num_etabin4_isobin5 = up.num_etabin4_isobin5; c.err_etabin4_isobin5 = up.err_etabin4_isobin5;}
    else {c.num_etabin4_isobin5 = -1*dn.num_etabin4_isobin5; c.err_etabin4_isobin5 = -1*dn.num_etabin4_isobin5;}
    if (fabs(up.num_etabin5_isobin1) > fabs(dn.num_etabin5_isobin1)) {c.num_etabin5_isobin1 = up.num_etabin5_isobin1; c.err_etabin5_isobin1 = up.err_etabin5_isobin1;}
    else {c.num_etabin5_isobin1 = -1*dn.num_etabin5_isobin1; c.err_etabin5_isobin1 = -1*dn.num_etabin5_isobin1;}
    if (fabs(up.num_etabin5_isobin2) > fabs(dn.num_etabin5_isobin2)) {c.num_etabin5_isobin2 = up.num_etabin5_isobin2; c.err_etabin5_isobin2 = up.err_etabin5_isobin2;}
    else {c.num_etabin5_isobin2 = -1*dn.num_etabin5_isobin2; c.err_etabin5_isobin2 = -1*dn.num_etabin5_isobin2;}
    if (fabs(up.num_etabin5_isobin3) > fabs(dn.num_etabin5_isobin3)) {c.num_etabin5_isobin3 = up.num_etabin5_isobin3; c.err_etabin5_isobin3 = up.err_etabin5_isobin3;}
    else {c.num_etabin5_isobin3 = -1*dn.num_etabin5_isobin3; c.err_etabin5_isobin3 = -1*dn.num_etabin5_isobin3;}
    if (fabs(up.num_etabin5_isobin4) > fabs(dn.num_etabin5_isobin4)) {c.num_etabin5_isobin4 = up.num_etabin5_isobin4; c.err_etabin5_isobin4 = up.err_etabin5_isobin4;}
    else {c.num_etabin5_isobin4 = -1*dn.num_etabin5_isobin4; c.err_etabin5_isobin4 = -1*dn.num_etabin5_isobin4;}
    if (fabs(up.num_etabin5_isobin5) > fabs(dn.num_etabin5_isobin5)) {c.num_etabin5_isobin5 = up.num_etabin5_isobin5; c.err_etabin5_isobin5 = up.err_etabin5_isobin5;}
    else {c.num_etabin5_isobin5 = -1*dn.num_etabin5_isobin5; c.err_etabin5_isobin5 = -1*dn.num_etabin5_isobin5;}

    return c;
}

AccCorr GetAccCorrSymmetrized(AccCorr &up, AccCorr &dn) {
    AccCorr c;

    if (fabs(up.num_acc) > fabs(dn.num_acc)) {c.num_acc = up.num_acc; c.err_acc = up.err_acc;}
    else {c.num_acc = -1*dn.num_acc; c.err_acc = dn.err_acc;}
    if (fabs(up.num_acc_ptbin1) > fabs(dn.num_acc_ptbin1)) {c.num_acc_ptbin1 = up.num_acc_ptbin1; c.err_acc_ptbin1 = up.err_acc_ptbin1;}
    else {c.num_acc_ptbin1 = -1*dn.num_acc_ptbin1; c.err_acc_ptbin1 = dn.err_acc_ptbin1;}
    if (fabs(up.num_acc_ptbin2) > fabs(dn.num_acc_ptbin2)) {c.num_acc_ptbin2 = up.num_acc_ptbin2; c.err_acc_ptbin2 = up.err_acc_ptbin2;}
    else {c.num_acc_ptbin2 = -1*dn.num_acc_ptbin2; c.err_acc_ptbin2 = dn.err_acc_ptbin2;}
    if (fabs(up.num_acc_ptbin3) > fabs(dn.num_acc_ptbin3)) {c.num_acc_ptbin3 = up.num_acc_ptbin3; c.err_acc_ptbin3 = up.err_acc_ptbin3;}
    else {c.num_acc_ptbin3 = -1*dn.num_acc_ptbin3; c.err_acc_ptbin3 = dn.err_acc_ptbin3;}
    if (fabs(up.num_acc_ptbin4) > fabs(dn.num_acc_ptbin4)) {c.num_acc_ptbin4 = up.num_acc_ptbin4; c.err_acc_ptbin4 = up.err_acc_ptbin4;}
    else {c.num_acc_ptbin4 = -1*dn.num_acc_ptbin4; c.err_acc_ptbin4 = dn.err_acc_ptbin4;}
    if (fabs(up.num_acc_ptbin5) > fabs(dn.num_acc_ptbin5)) {c.num_acc_ptbin5 = up.num_acc_ptbin5; c.err_acc_ptbin5 = up.err_acc_ptbin5;}
    else {c.num_acc_ptbin5 = -1*dn.num_acc_ptbin5; c.err_acc_ptbin5 = dn.err_acc_ptbin5;}
    if (fabs(up.num_acc_etabin1) > fabs(dn.num_acc_etabin1)) {c.num_acc_etabin1 = up.num_acc_etabin1; c.err_acc_etabin1 = up.err_acc_etabin1;}
    else {c.num_acc_etabin1 = -1*dn.num_acc_etabin1; c.err_acc_etabin1 = dn.err_acc_etabin1;}
    if (fabs(up.num_acc_etabin2) > fabs(dn.num_acc_etabin2)) {c.num_acc_etabin2 = up.num_acc_etabin2; c.err_acc_etabin2 = up.err_acc_etabin2;}
    else {c.num_acc_etabin2 = -1*dn.num_acc_etabin2; c.err_acc_etabin2 = dn.err_acc_etabin2;}
    if (fabs(up.num_acc_etabin3) > fabs(dn.num_acc_etabin3)) {c.num_acc_etabin3 = up.num_acc_etabin3; c.err_acc_etabin3 = up.err_acc_etabin3;}
    else {c.num_acc_etabin3 = -1*dn.num_acc_etabin3; c.err_acc_etabin3 = dn.err_acc_etabin3;}
    if (fabs(up.num_acc_etabin4) > fabs(dn.num_acc_etabin4)) {c.num_acc_etabin4 = up.num_acc_etabin4; c.err_acc_etabin4 = up.err_acc_etabin4;}
    else {c.num_acc_etabin4 = -1*dn.num_acc_etabin4; c.err_acc_etabin4 = dn.err_acc_etabin4;}
    if (fabs(up.num_acc_etabin5) > fabs(dn.num_acc_etabin5)) {c.num_acc_etabin5 = up.num_acc_etabin5; c.err_acc_etabin5 = up.err_acc_etabin5;}
    else {c.num_acc_etabin5 = -1*dn.num_acc_etabin5; c.err_acc_etabin5 = dn.err_acc_etabin5;}
    if (fabs(up.num_acc2) > fabs(dn.num_acc2)) {c.num_acc2 = up.num_acc2; c.err_acc2 = up.err_acc2;}
    else {c.num_acc2 = -1*dn.num_acc2; c.err_acc2 = dn.err_acc2;}
    if (fabs(up.num_acc2_ptbin1) > fabs(dn.num_acc2_ptbin1)) {c.num_acc2_ptbin1 = up.num_acc2_ptbin1; c.err_acc2_ptbin1 = up.err_acc2_ptbin1;}
    else {c.num_acc2_ptbin1 = -1*dn.num_acc2_ptbin1; c.err_acc2_ptbin1 = dn.err_acc2_ptbin1;}
    if (fabs(up.num_acc2_ptbin2) > fabs(dn.num_acc2_ptbin2)) {c.num_acc2_ptbin2 = up.num_acc2_ptbin2; c.err_acc2_ptbin2 = up.err_acc2_ptbin2;}
    else {c.num_acc2_ptbin2 = -1*dn.num_acc2_ptbin2; c.err_acc2_ptbin2 = dn.err_acc2_ptbin2;}
    if (fabs(up.num_acc2_ptbin3) > fabs(dn.num_acc2_ptbin3)) {c.num_acc2_ptbin3 = up.num_acc2_ptbin3; c.err_acc2_ptbin3 = up.err_acc2_ptbin3;}
    else {c.num_acc2_ptbin3 = -1*dn.num_acc2_ptbin3; c.err_acc2_ptbin3 = dn.err_acc2_ptbin3;}
    if (fabs(up.num_acc2_ptbin4) > fabs(dn.num_acc2_ptbin4)) {c.num_acc2_ptbin4 = up.num_acc2_ptbin4; c.err_acc2_ptbin4 = up.err_acc2_ptbin4;}
    else {c.num_acc2_ptbin4 = -1*dn.num_acc2_ptbin4; c.err_acc2_ptbin4 = dn.err_acc2_ptbin4;}
    if (fabs(up.num_acc2_ptbin5) > fabs(dn.num_acc2_ptbin5)) {c.num_acc2_ptbin5 = up.num_acc2_ptbin5; c.err_acc2_ptbin5 = up.err_acc2_ptbin5;}
    else {c.num_acc2_ptbin5 = -1*dn.num_acc2_ptbin5; c.err_acc2_ptbin5 = dn.err_acc2_ptbin5;}
    if (fabs(up.num_acc2_etabin1) > fabs(dn.num_acc2_etabin1)) {c.num_acc2_etabin1 = up.num_acc2_etabin1; c.err_acc2_etabin1 = up.err_acc2_etabin1;}
    else {c.num_acc2_etabin1 = -1*dn.num_acc2_etabin1; c.err_acc2_etabin1 = dn.err_acc2_etabin1;}
    if (fabs(up.num_acc2_etabin2) > fabs(dn.num_acc2_etabin2)) {c.num_acc2_etabin2 = up.num_acc2_etabin2; c.err_acc2_etabin2 = up.err_acc2_etabin2;}
    else {c.num_acc2_etabin2 = -1*dn.num_acc2_etabin2; c.err_acc2_etabin2 = dn.err_acc2_etabin2;}
    if (fabs(up.num_acc2_etabin3) > fabs(dn.num_acc2_etabin3)) {c.num_acc2_etabin3 = up.num_acc2_etabin3; c.err_acc2_etabin3 = up.err_acc2_etabin3;}
    else {c.num_acc2_etabin3 = -1*dn.num_acc2_etabin3; c.err_acc2_etabin3 = dn.err_acc2_etabin3;}
    if (fabs(up.num_acc2_etabin4) > fabs(dn.num_acc2_etabin4)) {c.num_acc2_etabin4 = up.num_acc2_etabin4; c.err_acc2_etabin4 = up.err_acc2_etabin4;}
    else {c.num_acc2_etabin4 = -1*dn.num_acc2_etabin4; c.err_acc2_etabin4 = dn.err_acc2_etabin4;}
    if (fabs(up.num_acc2_etabin5) > fabs(dn.num_acc2_etabin5)) {c.num_acc2_etabin5 = up.num_acc2_etabin5; c.err_acc2_etabin5 = up.err_acc2_etabin5;}
    else {c.num_acc2_etabin5 = -1*dn.num_acc2_etabin5; c.err_acc2_etabin5 = dn.err_acc2_etabin5;}
    if (fabs(up.num_acc3) > fabs(dn.num_acc3)) {c.num_acc3 = up.num_acc3; c.err_acc3 = up.err_acc3;}
    else {c.num_acc3 = -1*dn.num_acc3; c.err_acc3 = dn.err_acc3;}
    if (fabs(up.num_acc3_ptbin1) > fabs(dn.num_acc3_ptbin1)) {c.num_acc3_ptbin1 = up.num_acc3_ptbin1; c.err_acc3_ptbin1 = up.err_acc3_ptbin1;}
    else {c.num_acc3_ptbin1 = -1*dn.num_acc3_ptbin1; c.err_acc3_ptbin1 = dn.err_acc3_ptbin1;}
    if (fabs(up.num_acc3_ptbin2) > fabs(dn.num_acc3_ptbin2)) {c.num_acc3_ptbin2 = up.num_acc3_ptbin2; c.err_acc3_ptbin2 = up.err_acc3_ptbin2;}
    else {c.num_acc3_ptbin2 = -1*dn.num_acc3_ptbin2; c.err_acc3_ptbin2 = dn.err_acc3_ptbin2;}
    if (fabs(up.num_acc3_ptbin3) > fabs(dn.num_acc3_ptbin3)) {c.num_acc3_ptbin3 = up.num_acc3_ptbin3; c.err_acc3_ptbin3 = up.err_acc3_ptbin3;}
    else {c.num_acc3_ptbin3 = -1*dn.num_acc3_ptbin3; c.err_acc3_ptbin3 = dn.err_acc3_ptbin3;}
    if (fabs(up.num_acc3_ptbin4) > fabs(dn.num_acc3_ptbin4)) {c.num_acc3_ptbin4 = up.num_acc3_ptbin4; c.err_acc3_ptbin4 = up.err_acc3_ptbin4;}
    else {c.num_acc3_ptbin4 = -1*dn.num_acc3_ptbin4; c.err_acc3_ptbin4 = dn.err_acc3_ptbin4;}
    if (fabs(up.num_acc3_ptbin5) > fabs(dn.num_acc3_ptbin5)) {c.num_acc3_ptbin5 = up.num_acc3_ptbin5; c.err_acc3_ptbin5 = up.err_acc3_ptbin5;}
    else {c.num_acc3_ptbin5 = -1*dn.num_acc3_ptbin5; c.err_acc3_ptbin5 = dn.err_acc3_ptbin5;}
    if (fabs(up.num_acc3_etabin1) > fabs(dn.num_acc3_etabin1)) {c.num_acc3_etabin1 = up.num_acc3_etabin1; c.err_acc3_etabin1 = up.err_acc3_etabin1;}
    else {c.num_acc3_etabin1 = -1*dn.num_acc3_etabin1; c.err_acc3_etabin1 = dn.err_acc3_etabin1;}
    if (fabs(up.num_acc3_etabin2) > fabs(dn.num_acc3_etabin2)) {c.num_acc3_etabin2 = up.num_acc3_etabin2; c.err_acc3_etabin2 = up.err_acc3_etabin2;}
    else {c.num_acc3_etabin2 = -1*dn.num_acc3_etabin2; c.err_acc3_etabin2 = dn.err_acc3_etabin2;}
    if (fabs(up.num_acc3_etabin3) > fabs(dn.num_acc3_etabin3)) {c.num_acc3_etabin3 = up.num_acc3_etabin3; c.err_acc3_etabin3 = up.err_acc3_etabin3;}
    else {c.num_acc3_etabin3 = -1*dn.num_acc3_etabin3; c.err_acc3_etabin3 = dn.err_acc3_etabin3;}
    if (fabs(up.num_acc3_etabin4) > fabs(dn.num_acc3_etabin4)) {c.num_acc3_etabin4 = up.num_acc3_etabin4; c.err_acc3_etabin4 = up.err_acc3_etabin4;}
    else {c.num_acc3_etabin4 = -1*dn.num_acc3_etabin4; c.err_acc3_etabin4 = dn.err_acc3_etabin4;}
    if (fabs(up.num_acc3_etabin5) > fabs(dn.num_acc3_etabin5)) {c.num_acc3_etabin5 = up.num_acc3_etabin5; c.err_acc3_etabin5 = up.err_acc3_etabin5;}
    else {c.num_acc3_etabin5 = -1*dn.num_acc3_etabin5; c.err_acc3_etabin5 = dn.err_acc3_etabin5;}
    if (fabs(up.num_corr) > fabs(dn.num_corr)) {c.num_corr = up.num_corr; c.err_corr = up.err_corr;}
    else {c.num_corr = -1*dn.num_corr; c.err_corr = dn.err_corr;}
    if (fabs(up.num_corr_ptbin1) > fabs(dn.num_corr_ptbin1)) {c.num_corr_ptbin1 = up.num_corr_ptbin1; c.err_corr_ptbin1 = up.err_corr_ptbin1;}
    else {c.num_corr_ptbin1 = -1*dn.num_corr_ptbin1; c.err_corr_ptbin1 = dn.err_corr_ptbin1;}
    if (fabs(up.num_corr_ptbin2) > fabs(dn.num_corr_ptbin2)) {c.num_corr_ptbin2 = up.num_corr_ptbin2; c.err_corr_ptbin2 = up.err_corr_ptbin2;}
    else {c.num_corr_ptbin2 = -1*dn.num_corr_ptbin2; c.err_corr_ptbin2 = dn.err_corr_ptbin2;}
    if (fabs(up.num_corr_ptbin3) > fabs(dn.num_corr_ptbin3)) {c.num_corr_ptbin3 = up.num_corr_ptbin3; c.err_corr_ptbin3 = up.err_corr_ptbin3;}
    else {c.num_corr_ptbin3 = -1*dn.num_corr_ptbin3; c.err_corr_ptbin3 = dn.err_corr_ptbin3;}
    if (fabs(up.num_corr_ptbin4) > fabs(dn.num_corr_ptbin4)) {c.num_corr_ptbin4 = up.num_corr_ptbin4; c.err_corr_ptbin4 = up.err_corr_ptbin4;}
    else {c.num_corr_ptbin4 = -1*dn.num_corr_ptbin4; c.err_corr_ptbin4 = dn.err_corr_ptbin4;}
    if (fabs(up.num_corr_ptbin5) > fabs(dn.num_corr_ptbin5)) {c.num_corr_ptbin5 = up.num_corr_ptbin5; c.err_corr_ptbin5 = up.err_corr_ptbin5;}
    else {c.num_corr_ptbin5 = -1*dn.num_corr_ptbin5; c.err_corr_ptbin5 = dn.err_corr_ptbin5;}
    if (fabs(up.num_corr_etabin1) > fabs(dn.num_corr_etabin1)) {c.num_corr_etabin1 = up.num_corr_etabin1; c.err_corr_etabin1 = up.err_corr_etabin1;}
    else {c.num_corr_etabin1 = -1*dn.num_corr_etabin1; c.err_corr_etabin1 = dn.err_corr_etabin1;}
    if (fabs(up.num_corr_etabin2) > fabs(dn.num_corr_etabin2)) {c.num_corr_etabin2 = up.num_corr_etabin2; c.err_corr_etabin2 = up.err_corr_etabin2;}
    else {c.num_corr_etabin2 = -1*dn.num_corr_etabin2; c.err_corr_etabin2 = dn.err_corr_etabin2;}
    if (fabs(up.num_corr_etabin3) > fabs(dn.num_corr_etabin3)) {c.num_corr_etabin3 = up.num_corr_etabin3; c.err_corr_etabin3 = up.err_corr_etabin3;}
    else {c.num_corr_etabin3 = -1*dn.num_corr_etabin3; c.err_corr_etabin3 = dn.err_corr_etabin3;}
    if (fabs(up.num_corr_etabin4) > fabs(dn.num_corr_etabin4)) {c.num_corr_etabin4 = up.num_corr_etabin4; c.err_corr_etabin4 = up.err_corr_etabin4;}
    else {c.num_corr_etabin4 = -1*dn.num_corr_etabin4; c.err_corr_etabin4 = dn.err_corr_etabin4;}
    if (fabs(up.num_corr_etabin5) > fabs(dn.num_corr_etabin5)) {c.num_corr_etabin5 = up.num_corr_etabin5; c.err_corr_etabin5 = up.err_corr_etabin5;}
    else {c.num_corr_etabin5 = -1*dn.num_corr_etabin5; c.err_corr_etabin5 = dn.err_corr_etabin5;}
    if (fabs(up.num_acccorr) > fabs(dn.num_acccorr)) {c.num_acccorr = up.num_acccorr; c.err_acccorr = up.err_acccorr;}
    else {c.num_acccorr = -1*dn.num_acccorr; c.err_acccorr = dn.err_acccorr;}
    if (fabs(up.num_acccorr_ptbin1) > fabs(dn.num_acccorr_ptbin1)) {c.num_acccorr_ptbin1 = up.num_acccorr_ptbin1; c.err_acccorr_ptbin1 = up.err_acccorr_ptbin1;}
    else {c.num_acccorr_ptbin1 = -1*dn.num_acccorr_ptbin1; c.err_acccorr_ptbin1 = dn.err_acccorr_ptbin1;}
    if (fabs(up.num_acccorr_ptbin2) > fabs(dn.num_acccorr_ptbin2)) {c.num_acccorr_ptbin2 = up.num_acccorr_ptbin2; c.err_acccorr_ptbin2 = up.err_acccorr_ptbin2;}
    else {c.num_acccorr_ptbin2 = -1*dn.num_acccorr_ptbin2; c.err_acccorr_ptbin2 = dn.err_acccorr_ptbin2;}
    if (fabs(up.num_acccorr_ptbin3) > fabs(dn.num_acccorr_ptbin3)) {c.num_acccorr_ptbin3 = up.num_acccorr_ptbin3; c.err_acccorr_ptbin3 = up.err_acccorr_ptbin3;}
    else {c.num_acccorr_ptbin3 = -1*dn.num_acccorr_ptbin3; c.err_acccorr_ptbin3 = dn.err_acccorr_ptbin3;}
    if (fabs(up.num_acccorr_ptbin4) > fabs(dn.num_acccorr_ptbin4)) {c.num_acccorr_ptbin4 = up.num_acccorr_ptbin4; c.err_acccorr_ptbin4 = up.err_acccorr_ptbin4;}
    else {c.num_acccorr_ptbin4 = -1*dn.num_acccorr_ptbin4; c.err_acccorr_ptbin4 = dn.err_acccorr_ptbin4;}
    if (fabs(up.num_acccorr_ptbin5) > fabs(dn.num_acccorr_ptbin5)) {c.num_acccorr_ptbin5 = up.num_acccorr_ptbin5; c.err_acccorr_ptbin5 = up.err_acccorr_ptbin5;}
    else {c.num_acccorr_ptbin5 = -1*dn.num_acccorr_ptbin5; c.err_acccorr_ptbin5 = dn.err_acccorr_ptbin5;}
    if (fabs(up.num_acccorr_etabin1) > fabs(dn.num_acccorr_etabin1)) {c.num_acccorr_etabin1 = up.num_acccorr_etabin1; c.err_acccorr_etabin1 = up.err_acccorr_etabin1;}
    else {c.num_acccorr_etabin1 = -1*dn.num_acccorr_etabin1; c.err_acccorr_etabin1 = dn.err_acccorr_etabin1;}
    if (fabs(up.num_acccorr_etabin2) > fabs(dn.num_acccorr_etabin2)) {c.num_acccorr_etabin2 = up.num_acccorr_etabin2; c.err_acccorr_etabin2 = up.err_acccorr_etabin2;}
    else {c.num_acccorr_etabin2 = -1*dn.num_acccorr_etabin2; c.err_acccorr_etabin2 = dn.err_acccorr_etabin2;}
    if (fabs(up.num_acccorr_etabin3) > fabs(dn.num_acccorr_etabin3)) {c.num_acccorr_etabin3 = up.num_acccorr_etabin3; c.err_acccorr_etabin3 = up.err_acccorr_etabin3;}
    else {c.num_acccorr_etabin3 = -1*dn.num_acccorr_etabin3; c.err_acccorr_etabin3 = dn.err_acccorr_etabin3;}
    if (fabs(up.num_acccorr_etabin4) > fabs(dn.num_acccorr_etabin4)) {c.num_acccorr_etabin4 = up.num_acccorr_etabin4; c.err_acccorr_etabin4 = up.err_acccorr_etabin4;}
    else {c.num_acccorr_etabin4 = -1*dn.num_acccorr_etabin4; c.err_acccorr_etabin4 = dn.err_acccorr_etabin4;}
    if (fabs(up.num_acccorr_etabin5) > fabs(dn.num_acccorr_etabin5)) {c.num_acccorr_etabin5 = up.num_acccorr_etabin5; c.err_acccorr_etabin5 = up.err_acccorr_etabin5;}
    else {c.num_acccorr_etabin5 = -1*dn.num_acccorr_etabin5; c.err_acccorr_etabin5 = dn.err_acccorr_etabin5;}

    return c;
}

AccRatio GetAccRatioSymmetrized(AccRatio &up, AccRatio &dn) {
    AccRatio c;

    if (fabs(up.num_accratio) > fabs(dn.num_accratio)) {c.num_accratio = up.num_accratio; c.err_accratio = up.err_accratio;}
    else {c.num_accratio = -1*dn.num_accratio; c.err_accratio = dn.err_accratio;}
    if (fabs(up.num_accratio_ptbin1) > fabs(dn.num_accratio_ptbin1)) {c.num_accratio_ptbin1 = up.num_accratio_ptbin1; c.err_accratio_ptbin1 = up.err_accratio_ptbin1;}
    else {c.num_accratio_ptbin1 = -1*dn.num_accratio_ptbin1; c.err_accratio_ptbin1 = dn.err_accratio_ptbin1;}
    if (fabs(up.num_accratio_ptbin2) > fabs(dn.num_accratio_ptbin2)) {c.num_accratio_ptbin2 = up.num_accratio_ptbin2; c.err_accratio_ptbin2 = up.err_accratio_ptbin2;}
    else {c.num_accratio_ptbin2 = -1*dn.num_accratio_ptbin2; c.err_accratio_ptbin2 = dn.err_accratio_ptbin2;}
    if (fabs(up.num_accratio_ptbin3) > fabs(dn.num_accratio_ptbin3)) {c.num_accratio_ptbin3 = up.num_accratio_ptbin3; c.err_accratio_ptbin3 = up.err_accratio_ptbin3;}
    else {c.num_accratio_ptbin3 = -1*dn.num_accratio_ptbin3; c.err_accratio_ptbin3 = dn.err_accratio_ptbin3;}
    if (fabs(up.num_accratio_ptbin4) > fabs(dn.num_accratio_ptbin4)) {c.num_accratio_ptbin4 = up.num_accratio_ptbin4; c.err_accratio_ptbin4 = up.err_accratio_ptbin4;}
    else {c.num_accratio_ptbin4 = -1*dn.num_accratio_ptbin4; c.err_accratio_ptbin4 = dn.err_accratio_ptbin4;}
    if (fabs(up.num_accratio_ptbin5) > fabs(dn.num_accratio_ptbin5)) {c.num_accratio_ptbin5 = up.num_accratio_ptbin5; c.err_accratio_ptbin5 = up.err_accratio_ptbin5;}
    else {c.num_accratio_ptbin5 = -1*dn.num_accratio_ptbin5; c.err_accratio_ptbin5 = dn.err_accratio_ptbin5;}
    if (fabs(up.num_accratio_etabin1) > fabs(dn.num_accratio_etabin1)) {c.num_accratio_etabin1 = up.num_accratio_etabin1; c.err_accratio_etabin1 = up.err_accratio_etabin1;}
    else {c.num_accratio_etabin1 = -1*dn.num_accratio_etabin1; c.err_accratio_etabin1 = dn.err_accratio_etabin1;}
    if (fabs(up.num_accratio_etabin2) > fabs(dn.num_accratio_etabin2)) {c.num_accratio_etabin2 = up.num_accratio_etabin2; c.err_accratio_etabin2 = up.err_accratio_etabin2;}
    else {c.num_accratio_etabin2 = -1*dn.num_accratio_etabin2; c.err_accratio_etabin2 = dn.err_accratio_etabin2;}
    if (fabs(up.num_accratio_etabin3) > fabs(dn.num_accratio_etabin3)) {c.num_accratio_etabin3 = up.num_accratio_etabin3; c.err_accratio_etabin3 = up.err_accratio_etabin3;}
    else {c.num_accratio_etabin3 = -1*dn.num_accratio_etabin3; c.err_accratio_etabin3 = dn.err_accratio_etabin3;}
    if (fabs(up.num_accratio_etabin4) > fabs(dn.num_accratio_etabin4)) {c.num_accratio_etabin4 = up.num_accratio_etabin4; c.err_accratio_etabin4 = up.err_accratio_etabin4;}
    else {c.num_accratio_etabin4 = -1*dn.num_accratio_etabin4; c.err_accratio_etabin4 = dn.err_accratio_etabin4;}
    if (fabs(up.num_accratio_etabin5) > fabs(dn.num_accratio_etabin5)) {c.num_accratio_etabin5 = up.num_accratio_etabin5; c.err_accratio_etabin5 = up.err_accratio_etabin5;}
    else {c.num_accratio_etabin5 = -1*dn.num_accratio_etabin5; c.err_accratio_etabin5 = dn.err_accratio_etabin5;}

    return c;
}

Figure EmptyFigure() {
    Figure c;
    c.h_ptcone = NULL;
    c.h_jetpt = NULL;
    c.h_jeteta = NULL;
    c.h_phpt = NULL;
    c.h_pheta = NULL;
    c.h_leppt = NULL;
    c.h_lepeta = NULL;
    c.h_met = NULL;
    c.h_mphl = NULL;
    c.h_njet = NULL;
    c.h_nbjet = NULL;
    c.h_drphl = NULL;
    return c;
}

Counter EmptyCounter() {
    Counter c;
    c.num_all = 0; c.err_all = 0;
    c.num_total = 0; c.err_total = 0;
    c.num_total_ptbin1 = 0; c.err_total_ptbin1 = 0;
    c.num_total_ptbin2 = 0; c.err_total_ptbin2 = 0;
    c.num_total_ptbin3 = 0; c.err_total_ptbin3 = 0;
    c.num_total_ptbin4 = 0; c.err_total_ptbin4 = 0;
    c.num_total_ptbin5 = 0; c.err_total_ptbin5 = 0;
    c.num_total_etabin1 = 0; c.err_total_etabin1 = 0;
    c.num_total_etabin2 = 0; c.err_total_etabin2 = 0;
    c.num_total_etabin3 = 0; c.err_total_etabin3 = 0;
    c.num_total_etabin4 = 0; c.err_total_etabin4 = 0;
    c.num_total_etabin5 = 0; c.err_total_etabin5 = 0;
    c.num_fidu = 0; c.err_fidu = 0;
    c.num_fidu_ptbin1 = 0; c.err_fidu_ptbin1 = 0;
    c.num_fidu_ptbin2 = 0; c.err_fidu_ptbin2 = 0;
    c.num_fidu_ptbin3 = 0; c.err_fidu_ptbin3 = 0;
    c.num_fidu_ptbin4 = 0; c.err_fidu_ptbin4 = 0;
    c.num_fidu_ptbin5 = 0; c.err_fidu_ptbin5 = 0;
    c.num_fidu_etabin1 = 0; c.err_fidu_etabin1 = 0;
    c.num_fidu_etabin2 = 0; c.err_fidu_etabin2 = 0;
    c.num_fidu_etabin3 = 0; c.err_fidu_etabin3 = 0;
    c.num_fidu_etabin4 = 0; c.err_fidu_etabin4 = 0;
    c.num_fidu_etabin5 = 0; c.err_fidu_etabin5 = 0;
    c.num_isobin1 = 0; c.err_isobin1 = 0;
    c.num_isobin2 = 0; c.err_isobin2 = 0;
    c.num_isobin3 = 0; c.err_isobin3 = 0;
    c.num_isobin4 = 0; c.err_isobin4 = 0;
    c.num_isobin5 = 0; c.err_isobin5 = 0;
    c.num_ptbin1_isobin1 = 0; c.err_ptbin1_isobin1 = 0;
    c.num_ptbin1_isobin2 = 0; c.err_ptbin1_isobin2 = 0;
    c.num_ptbin1_isobin3 = 0; c.err_ptbin1_isobin3 = 0;
    c.num_ptbin1_isobin4 = 0; c.err_ptbin1_isobin4 = 0;
    c.num_ptbin1_isobin5 = 0; c.err_ptbin1_isobin5 = 0;
    c.num_ptbin2_isobin1 = 0; c.err_ptbin2_isobin1 = 0;
    c.num_ptbin2_isobin2 = 0; c.err_ptbin2_isobin2 = 0;
    c.num_ptbin2_isobin3 = 0; c.err_ptbin2_isobin3 = 0;
    c.num_ptbin2_isobin4 = 0; c.err_ptbin2_isobin4 = 0;
    c.num_ptbin2_isobin5 = 0; c.err_ptbin2_isobin5 = 0;
    c.num_ptbin3_isobin1 = 0; c.err_ptbin3_isobin1 = 0;
    c.num_ptbin3_isobin2 = 0; c.err_ptbin3_isobin2 = 0;
    c.num_ptbin3_isobin3 = 0; c.err_ptbin3_isobin3 = 0;
    c.num_ptbin3_isobin4 = 0; c.err_ptbin3_isobin4 = 0;
    c.num_ptbin3_isobin5 = 0; c.err_ptbin3_isobin5 = 0;
    c.num_ptbin4_isobin1 = 0; c.err_ptbin4_isobin1 = 0;
    c.num_ptbin4_isobin2 = 0; c.err_ptbin4_isobin2 = 0;
    c.num_ptbin4_isobin3 = 0; c.err_ptbin4_isobin3 = 0;
    c.num_ptbin4_isobin4 = 0; c.err_ptbin4_isobin4 = 0;
    c.num_ptbin4_isobin5 = 0; c.err_ptbin4_isobin5 = 0;
    c.num_ptbin5_isobin1 = 0; c.err_ptbin5_isobin1 = 0;
    c.num_ptbin5_isobin2 = 0; c.err_ptbin5_isobin2 = 0;
    c.num_ptbin5_isobin3 = 0; c.err_ptbin5_isobin3 = 0;
    c.num_ptbin5_isobin4 = 0; c.err_ptbin5_isobin4 = 0;
    c.num_ptbin5_isobin5 = 0; c.err_ptbin5_isobin5 = 0;
    c.num_etabin1_isobin1 = 0; c.err_etabin1_isobin1 = 0;
    c.num_etabin1_isobin2 = 0; c.err_etabin1_isobin2 = 0;
    c.num_etabin1_isobin3 = 0; c.err_etabin1_isobin3 = 0;
    c.num_etabin1_isobin4 = 0; c.err_etabin1_isobin4 = 0;
    c.num_etabin1_isobin5 = 0; c.err_etabin1_isobin5 = 0;
    c.num_etabin2_isobin1 = 0; c.err_etabin2_isobin1 = 0;
    c.num_etabin2_isobin2 = 0; c.err_etabin2_isobin2 = 0;
    c.num_etabin2_isobin3 = 0; c.err_etabin2_isobin3 = 0;
    c.num_etabin2_isobin4 = 0; c.err_etabin2_isobin4 = 0;
    c.num_etabin2_isobin5 = 0; c.err_etabin2_isobin5 = 0;
    c.num_etabin3_isobin1 = 0; c.err_etabin3_isobin1 = 0;
    c.num_etabin3_isobin2 = 0; c.err_etabin3_isobin2 = 0;
    c.num_etabin3_isobin3 = 0; c.err_etabin3_isobin3 = 0;
    c.num_etabin3_isobin4 = 0; c.err_etabin3_isobin4 = 0;
    c.num_etabin3_isobin5 = 0; c.err_etabin3_isobin5 = 0;
    c.num_etabin4_isobin1 = 0; c.err_etabin4_isobin1 = 0;
    c.num_etabin4_isobin2 = 0; c.err_etabin4_isobin2 = 0;
    c.num_etabin4_isobin3 = 0; c.err_etabin4_isobin3 = 0;
    c.num_etabin4_isobin4 = 0; c.err_etabin4_isobin4 = 0;
    c.num_etabin4_isobin5 = 0; c.err_etabin4_isobin5 = 0;
    c.num_etabin5_isobin1 = 0; c.err_etabin5_isobin1 = 0;
    c.num_etabin5_isobin2 = 0; c.err_etabin5_isobin2 = 0;
    c.num_etabin5_isobin3 = 0; c.err_etabin5_isobin3 = 0;
    c.num_etabin5_isobin4 = 0; c.err_etabin5_isobin4 = 0;
    c.num_etabin5_isobin5 = 0; c.err_etabin5_isobin5 = 0;
    return c;
}

AccCorr EmptyAccCorr() {
    AccCorr c;
    c.num_acc = 0; c.err_acc = 0;
    c.num_acc_ptbin1 = 0; c.err_acc_ptbin1 = 0;
    c.num_acc_ptbin2 = 0; c.err_acc_ptbin2 = 0;
    c.num_acc_ptbin3 = 0; c.err_acc_ptbin3 = 0;
    c.num_acc_ptbin4 = 0; c.err_acc_ptbin4 = 0;
    c.num_acc_ptbin5 = 0; c.err_acc_ptbin5 = 0;
    c.num_acc_etabin1 = 0; c.err_acc_etabin1 = 0;
    c.num_acc_etabin2 = 0; c.err_acc_etabin2 = 0;
    c.num_acc_etabin3 = 0; c.err_acc_etabin3 = 0;
    c.num_acc_etabin4 = 0; c.err_acc_etabin4 = 0;
    c.num_acc_etabin5 = 0; c.err_acc_etabin5 = 0;
    c.num_acc2 = 0; c.err_acc2 = 0;
    c.num_acc2_ptbin1 = 0; c.err_acc2_ptbin1 = 0;
    c.num_acc2_ptbin2 = 0; c.err_acc2_ptbin2 = 0;
    c.num_acc2_ptbin3 = 0; c.err_acc2_ptbin3 = 0;
    c.num_acc2_ptbin4 = 0; c.err_acc2_ptbin4 = 0;
    c.num_acc2_ptbin5 = 0; c.err_acc2_ptbin5 = 0;
    c.num_acc2_etabin1 = 0; c.err_acc2_etabin1 = 0;
    c.num_acc2_etabin2 = 0; c.err_acc2_etabin2 = 0;
    c.num_acc2_etabin3 = 0; c.err_acc2_etabin3 = 0;
    c.num_acc2_etabin4 = 0; c.err_acc2_etabin4 = 0;
    c.num_acc2_etabin5 = 0; c.err_acc2_etabin5 = 0;
    c.num_acc3 = 0; c.err_acc3 = 0;
    c.num_acc3_ptbin1 = 0; c.err_acc3_ptbin1 = 0;
    c.num_acc3_ptbin2 = 0; c.err_acc3_ptbin2 = 0;
    c.num_acc3_ptbin3 = 0; c.err_acc3_ptbin3 = 0;
    c.num_acc3_ptbin4 = 0; c.err_acc3_ptbin4 = 0;
    c.num_acc3_ptbin5 = 0; c.err_acc3_ptbin5 = 0;
    c.num_acc3_etabin1 = 0; c.err_acc3_etabin1 = 0;
    c.num_acc3_etabin2 = 0; c.err_acc3_etabin2 = 0;
    c.num_acc3_etabin3 = 0; c.err_acc3_etabin3 = 0;
    c.num_acc3_etabin4 = 0; c.err_acc3_etabin4 = 0;
    c.num_acc3_etabin5 = 0; c.err_acc3_etabin5 = 0;
    c.num_corr = 0; c.err_corr = 0;
    c.num_corr_ptbin1 = 0; c.err_corr_ptbin1 = 0;
    c.num_corr_ptbin2 = 0; c.err_corr_ptbin2 = 0;
    c.num_corr_ptbin3 = 0; c.err_corr_ptbin3 = 0;
    c.num_corr_ptbin4 = 0; c.err_corr_ptbin4 = 0;
    c.num_corr_ptbin5 = 0; c.err_corr_ptbin5 = 0;
    c.num_corr_etabin1 = 0; c.err_corr_etabin1 = 0;
    c.num_corr_etabin2 = 0; c.err_corr_etabin2 = 0;
    c.num_corr_etabin3 = 0; c.err_corr_etabin3 = 0;
    c.num_corr_etabin4 = 0; c.err_corr_etabin4 = 0;
    c.num_corr_etabin5 = 0; c.err_corr_etabin5 = 0;
    c.num_acccorr = 0; c.err_acccorr = 0;
    c.num_acccorr_ptbin1 = 0; c.err_acccorr_ptbin1 = 0;
    c.num_acccorr_ptbin2 = 0; c.err_acccorr_ptbin2 = 0;
    c.num_acccorr_ptbin3 = 0; c.err_acccorr_ptbin3 = 0;
    c.num_acccorr_ptbin4 = 0; c.err_acccorr_ptbin4 = 0;
    c.num_acccorr_ptbin5 = 0; c.err_acccorr_ptbin5 = 0;
    c.num_acccorr_etabin1 = 0; c.err_acccorr_etabin1 = 0;
    c.num_acccorr_etabin2 = 0; c.err_acccorr_etabin2 = 0;
    c.num_acccorr_etabin3 = 0; c.err_acccorr_etabin3 = 0;
    c.num_acccorr_etabin4 = 0; c.err_acccorr_etabin4 = 0;
    c.num_acccorr_etabin5 = 0; c.err_acccorr_etabin5 = 0;
    return c;
}

AccRatio EmptyAccRatio() {
    AccRatio c;
    c.num_accratio = 0; c.err_accratio = 0;
    c.num_accratio_ptbin1 = 0; c.err_accratio_ptbin1 = 0;
    c.num_accratio_ptbin2 = 0; c.err_accratio_ptbin2 = 0;
    c.num_accratio_ptbin3 = 0; c.err_accratio_ptbin3 = 0;
    c.num_accratio_ptbin4 = 0; c.err_accratio_ptbin4 = 0;
    c.num_accratio_ptbin5 = 0; c.err_accratio_ptbin5 = 0;
    c.num_accratio_etabin1 = 0; c.err_accratio_etabin1 = 0;
    c.num_accratio_etabin2 = 0; c.err_accratio_etabin2 = 0;
    c.num_accratio_etabin3 = 0; c.err_accratio_etabin3 = 0;
    c.num_accratio_etabin4 = 0; c.err_accratio_etabin4 = 0;
    c.num_accratio_etabin5 = 0; c.err_accratio_etabin5 = 0;
    return c;
}

void RegulateSysName(string &name) {
    if (name == "MUON_TRIGGER") name = "$\\mu$ Trigger";
    if (name == "ELE_TRIGGER") name = "$e$ Trigger";
    if (name == "MUON_ID") name = "$\\mu$ Identification $\\epsilon$";
    if (name == "PH_ID") name = "$\\gamma$ Identification $\\epsilon$";
    if (name == "ELE_ID") name = "$e$ Identification $\\epsilon$";
    if (name == "MUON_RECO") name = "$\\mu$ Reconstruction $\\epsilon$";
    if (name == "ELE_RECO") name = "$e$ Reconstruction $\\epsilon$";
    if (name == "mu_scale") name = "$\\mu$ Energy Scale";
    if (name == "mu_msres") name = "$\\mu$ MS Energy Resolution";
    if (name == "mu_idres") name = "$\\mu$ ID Energy Resolution";
    if (name == "ph_es") name = "$\\gamma$ Energy Scale";
    if (name == "ph_er") name = "$\\gamma$ Energy Resolution";
    if (name == "ees") name = "$e$ Energy Scale";
    if (name == "err") name = "$e$ Energy Resolution";
    if (name == "met_res_soft") name = "MET Soft Resolution";
    if (name == "met_sc_soft") name = "MET Soft Scale";
    if (name == "jeff") name = "Jet Reconstruction $\\epsilon$";
}

void OrderFVector(vector<double> fv, vector<int> &order, bool debug) {
    order.clear();
    for (int i = 0; i < fv.size(); i++) {
	order.push_back(FindRemoveMax(fv, debug));
    }
}

int FindRemoveMax(vector<double> &input, bool debug) {
    int pos = 0;
    double max = -1;
    for (int i = 0; i < input.size(); i++) {
	if (input.at(i) > max) {
	    max = input.at(i); 
	    pos = i;
	}
    }
    if (debug) cout << setprecision(3) << max << endl;
    input.at(pos) = -1;
    return pos;
}

AccCorr GetAccCorrRatio(AccCorr &a, AccCorr &b, double rho) {
    AccCorr c;
    c.num_acc = a.num_acc / b.num_acc;
    c.num_acc_ptbin1 = a.num_acc_ptbin1 / b.num_acc_ptbin1;
    c.num_acc_ptbin2 = a.num_acc_ptbin2 / b.num_acc_ptbin2;
    c.num_acc_ptbin3 = a.num_acc_ptbin3 / b.num_acc_ptbin3;
    c.num_acc_ptbin4 = a.num_acc_ptbin4 / b.num_acc_ptbin4;
    c.num_acc_ptbin5 = a.num_acc_ptbin5 / b.num_acc_ptbin5;
    c.num_acc_etabin1 = a.num_acc_etabin1 / b.num_acc_etabin1;
    c.num_acc_etabin2 = a.num_acc_etabin2 / b.num_acc_etabin2;
    c.num_acc_etabin3 = a.num_acc_etabin3 / b.num_acc_etabin3;
    c.num_acc_etabin4 = a.num_acc_etabin4 / b.num_acc_etabin4;
    c.num_acc_etabin5 = a.num_acc_etabin5 / b.num_acc_etabin5;
    c.err_acc = sqrt(pow(a.err_acc/b.num_acc, 2) + pow(a.num_acc*b.err_acc/pow(b.num_acc,2), 2) + 2*rho*(1/b.num_acc)*(-1*a.num_acc/pow(b.num_acc,2))*a.err_acc*b.err_acc);
    c.err_acc_ptbin1 = sqrt(pow(a.err_acc_ptbin1/b.num_acc_ptbin1, 2) + pow(a.num_acc_ptbin1*b.err_acc_ptbin1/pow(b.num_acc_ptbin1,2), 2) + 2*rho*(1/b.num_acc_ptbin1)*(-1*a.num_acc_ptbin1/pow(b.num_acc_ptbin1,2))*a.err_acc_ptbin1*b.err_acc_ptbin1);
    c.err_acc_ptbin2 = sqrt(pow(a.err_acc_ptbin2/b.num_acc_ptbin2, 2) + pow(a.num_acc_ptbin2*b.err_acc_ptbin2/pow(b.num_acc_ptbin2,2), 2) + 2*rho*(1/b.num_acc_ptbin2)*(-1*a.num_acc_ptbin2/pow(b.num_acc_ptbin2,2))*a.err_acc_ptbin2*b.err_acc_ptbin2);
    c.err_acc_ptbin3 = sqrt(pow(a.err_acc_ptbin3/b.num_acc_ptbin3, 2) + pow(a.num_acc_ptbin3*b.err_acc_ptbin3/pow(b.num_acc_ptbin3,2), 2) + 2*rho*(1/b.num_acc_ptbin3)*(-1*a.num_acc_ptbin3/pow(b.num_acc_ptbin3,2))*a.err_acc_ptbin3*b.err_acc_ptbin3);
    c.err_acc_ptbin4 = sqrt(pow(a.err_acc_ptbin4/b.num_acc_ptbin4, 2) + pow(a.num_acc_ptbin4*b.err_acc_ptbin4/pow(b.num_acc_ptbin4,2), 2) + 2*rho*(1/b.num_acc_ptbin4)*(-1*a.num_acc_ptbin4/pow(b.num_acc_ptbin4,2))*a.err_acc_ptbin4*b.err_acc_ptbin4);
    c.err_acc_ptbin5 = sqrt(pow(a.err_acc_ptbin5/b.num_acc_ptbin5, 2) + pow(a.num_acc_ptbin5*b.err_acc_ptbin5/pow(b.num_acc_ptbin5,2), 2) + 2*rho*(1/b.num_acc_ptbin5)*(-1*a.num_acc_ptbin5/pow(b.num_acc_ptbin5,2))*a.err_acc_ptbin5*b.err_acc_ptbin5);
    c.err_acc_etabin1 = sqrt(pow(a.err_acc_etabin1/b.num_acc_etabin1, 2) + pow(a.num_acc_etabin1*b.err_acc_etabin1/pow(b.num_acc_etabin1,2), 2) + 2*rho*(1/b.num_acc_etabin1)*(-1*a.num_acc_etabin1/pow(b.num_acc_etabin1,2))*a.err_acc_etabin1*b.err_acc_etabin1);
    c.err_acc_etabin2 = sqrt(pow(a.err_acc_etabin2/b.num_acc_etabin2, 2) + pow(a.num_acc_etabin2*b.err_acc_etabin2/pow(b.num_acc_etabin2,2), 2) + 2*rho*(1/b.num_acc_etabin2)*(-1*a.num_acc_etabin2/pow(b.num_acc_etabin2,2))*a.err_acc_etabin2*b.err_acc_etabin2);
    c.err_acc_etabin3 = sqrt(pow(a.err_acc_etabin3/b.num_acc_etabin3, 2) + pow(a.num_acc_etabin3*b.err_acc_etabin3/pow(b.num_acc_etabin3,2), 2) + 2*rho*(1/b.num_acc_etabin3)*(-1*a.num_acc_etabin3/pow(b.num_acc_etabin3,2))*a.err_acc_etabin3*b.err_acc_etabin3);
    c.err_acc_etabin4 = sqrt(pow(a.err_acc_etabin4/b.num_acc_etabin4, 2) + pow(a.num_acc_etabin4*b.err_acc_etabin4/pow(b.num_acc_etabin4,2), 2) + 2*rho*(1/b.num_acc_etabin4)*(-1*a.num_acc_etabin4/pow(b.num_acc_etabin4,2))*a.err_acc_etabin4*b.err_acc_etabin4);
    c.err_acc_etabin5 = sqrt(pow(a.err_acc_etabin5/b.num_acc_etabin5, 2) + pow(a.num_acc_etabin5*b.err_acc_etabin5/pow(b.num_acc_etabin5,2), 2) + 2*rho*(1/b.num_acc_etabin5)*(-1*a.num_acc_etabin5/pow(b.num_acc_etabin5,2))*a.err_acc_etabin5*b.err_acc_etabin5);
    c.num_acc2 = a.num_acc2 / b.num_acc2;
    c.num_acc2_ptbin1 = a.num_acc2_ptbin1 / b.num_acc2_ptbin1;
    c.num_acc2_ptbin2 = a.num_acc2_ptbin2 / b.num_acc2_ptbin2;
    c.num_acc2_ptbin3 = a.num_acc2_ptbin3 / b.num_acc2_ptbin3;
    c.num_acc2_ptbin4 = a.num_acc2_ptbin4 / b.num_acc2_ptbin4;
    c.num_acc2_ptbin5 = a.num_acc2_ptbin5 / b.num_acc2_ptbin5;
    c.num_acc2_etabin1 = a.num_acc2_etabin1 / b.num_acc2_etabin1;
    c.num_acc2_etabin2 = a.num_acc2_etabin2 / b.num_acc2_etabin2;
    c.num_acc2_etabin3 = a.num_acc2_etabin3 / b.num_acc2_etabin3;
    c.num_acc2_etabin4 = a.num_acc2_etabin4 / b.num_acc2_etabin4;
    c.num_acc2_etabin5 = a.num_acc2_etabin5 / b.num_acc2_etabin5;
    c.err_acc2 = sqrt(pow(a.err_acc2/b.num_acc2, 2) + pow(a.num_acc2*b.err_acc2/pow(b.num_acc2,2), 2) + 2*rho*(1/b.num_acc2)*(-1*a.num_acc2/pow(b.num_acc2,2))*a.err_acc2*b.err_acc2);
    c.err_acc2_ptbin1 = sqrt(pow(a.err_acc2_ptbin1/b.num_acc2_ptbin1, 2) + pow(a.num_acc2_ptbin1*b.err_acc2_ptbin1/pow(b.num_acc2_ptbin1,2), 2) + 2*rho*(1/b.num_acc2_ptbin1)*(-1*a.num_acc2_ptbin1/pow(b.num_acc2_ptbin1,2))*a.err_acc2_ptbin1*b.err_acc2_ptbin1);
    c.err_acc2_ptbin2 = sqrt(pow(a.err_acc2_ptbin2/b.num_acc2_ptbin2, 2) + pow(a.num_acc2_ptbin2*b.err_acc2_ptbin2/pow(b.num_acc2_ptbin2,2), 2) + 2*rho*(1/b.num_acc2_ptbin2)*(-1*a.num_acc2_ptbin2/pow(b.num_acc2_ptbin2,2))*a.err_acc2_ptbin2*b.err_acc2_ptbin2);
    c.err_acc2_ptbin3 = sqrt(pow(a.err_acc2_ptbin3/b.num_acc2_ptbin3, 2) + pow(a.num_acc2_ptbin3*b.err_acc2_ptbin3/pow(b.num_acc2_ptbin3,2), 2) + 2*rho*(1/b.num_acc2_ptbin3)*(-1*a.num_acc2_ptbin3/pow(b.num_acc2_ptbin3,2))*a.err_acc2_ptbin3*b.err_acc2_ptbin3);
    c.err_acc2_ptbin4 = sqrt(pow(a.err_acc2_ptbin4/b.num_acc2_ptbin4, 2) + pow(a.num_acc2_ptbin4*b.err_acc2_ptbin4/pow(b.num_acc2_ptbin4,2), 2) + 2*rho*(1/b.num_acc2_ptbin4)*(-1*a.num_acc2_ptbin4/pow(b.num_acc2_ptbin4,2))*a.err_acc2_ptbin4*b.err_acc2_ptbin4);
    c.err_acc2_ptbin5 = sqrt(pow(a.err_acc2_ptbin5/b.num_acc2_ptbin5, 2) + pow(a.num_acc2_ptbin5*b.err_acc2_ptbin5/pow(b.num_acc2_ptbin5,2), 2) + 2*rho*(1/b.num_acc2_ptbin5)*(-1*a.num_acc2_ptbin5/pow(b.num_acc2_ptbin5,2))*a.err_acc2_ptbin5*b.err_acc2_ptbin5);
    c.err_acc2_etabin1 = sqrt(pow(a.err_acc2_etabin1/b.num_acc2_etabin1, 2) + pow(a.num_acc2_etabin1*b.err_acc2_etabin1/pow(b.num_acc2_etabin1,2), 2) + 2*rho*(1/b.num_acc2_etabin1)*(-1*a.num_acc2_etabin1/pow(b.num_acc2_etabin1,2))*a.err_acc2_etabin1*b.err_acc2_etabin1);
    c.err_acc2_etabin2 = sqrt(pow(a.err_acc2_etabin2/b.num_acc2_etabin2, 2) + pow(a.num_acc2_etabin2*b.err_acc2_etabin2/pow(b.num_acc2_etabin2,2), 2) + 2*rho*(1/b.num_acc2_etabin2)*(-1*a.num_acc2_etabin2/pow(b.num_acc2_etabin2,2))*a.err_acc2_etabin2*b.err_acc2_etabin2);
    c.err_acc2_etabin3 = sqrt(pow(a.err_acc2_etabin3/b.num_acc2_etabin3, 2) + pow(a.num_acc2_etabin3*b.err_acc2_etabin3/pow(b.num_acc2_etabin3,2), 2) + 2*rho*(1/b.num_acc2_etabin3)*(-1*a.num_acc2_etabin3/pow(b.num_acc2_etabin3,2))*a.err_acc2_etabin3*b.err_acc2_etabin3);
    c.err_acc2_etabin4 = sqrt(pow(a.err_acc2_etabin4/b.num_acc2_etabin4, 2) + pow(a.num_acc2_etabin4*b.err_acc2_etabin4/pow(b.num_acc2_etabin4,2), 2) + 2*rho*(1/b.num_acc2_etabin4)*(-1*a.num_acc2_etabin4/pow(b.num_acc2_etabin4,2))*a.err_acc2_etabin4*b.err_acc2_etabin4);
    c.err_acc2_etabin5 = sqrt(pow(a.err_acc2_etabin5/b.num_acc2_etabin5, 2) + pow(a.num_acc2_etabin5*b.err_acc2_etabin5/pow(b.num_acc2_etabin5,2), 2) + 2*rho*(1/b.num_acc2_etabin5)*(-1*a.num_acc2_etabin5/pow(b.num_acc2_etabin5,2))*a.err_acc2_etabin5*b.err_acc2_etabin5);
    c.num_acc3 = a.num_acc3 / b.num_acc3;
    c.num_acc3_ptbin1 = a.num_acc3_ptbin1 / b.num_acc3_ptbin1;
    c.num_acc3_ptbin2 = a.num_acc3_ptbin2 / b.num_acc3_ptbin2;
    c.num_acc3_ptbin3 = a.num_acc3_ptbin3 / b.num_acc3_ptbin3;
    c.num_acc3_ptbin4 = a.num_acc3_ptbin4 / b.num_acc3_ptbin4;
    c.num_acc3_ptbin5 = a.num_acc3_ptbin5 / b.num_acc3_ptbin5;
    c.num_acc3_etabin1 = a.num_acc3_etabin1 / b.num_acc3_etabin1;
    c.num_acc3_etabin2 = a.num_acc3_etabin2 / b.num_acc3_etabin2;
    c.num_acc3_etabin3 = a.num_acc3_etabin3 / b.num_acc3_etabin3;
    c.num_acc3_etabin4 = a.num_acc3_etabin4 / b.num_acc3_etabin4;
    c.num_acc3_etabin5 = a.num_acc3_etabin5 / b.num_acc3_etabin5;
    c.err_acc3 = sqrt(pow(a.err_acc3/b.num_acc3, 2) + pow(a.num_acc3*b.err_acc3/pow(b.num_acc3,2), 2) + 2*rho*(1/b.num_acc3)*(-1*a.num_acc3/pow(b.num_acc3,2))*a.err_acc3*b.err_acc3);
    c.err_acc3_ptbin1 = sqrt(pow(a.err_acc3_ptbin1/b.num_acc3_ptbin1, 2) + pow(a.num_acc3_ptbin1*b.err_acc3_ptbin1/pow(b.num_acc3_ptbin1,2), 2) + 2*rho*(1/b.num_acc3_ptbin1)*(-1*a.num_acc3_ptbin1/pow(b.num_acc3_ptbin1,2))*a.err_acc3_ptbin1*b.err_acc3_ptbin1);
    c.err_acc3_ptbin2 = sqrt(pow(a.err_acc3_ptbin2/b.num_acc3_ptbin2, 2) + pow(a.num_acc3_ptbin2*b.err_acc3_ptbin2/pow(b.num_acc3_ptbin2,2), 2) + 2*rho*(1/b.num_acc3_ptbin2)*(-1*a.num_acc3_ptbin2/pow(b.num_acc3_ptbin2,2))*a.err_acc3_ptbin2*b.err_acc3_ptbin2);
    c.err_acc3_ptbin3 = sqrt(pow(a.err_acc3_ptbin3/b.num_acc3_ptbin3, 2) + pow(a.num_acc3_ptbin3*b.err_acc3_ptbin3/pow(b.num_acc3_ptbin3,2), 2) + 2*rho*(1/b.num_acc3_ptbin3)*(-1*a.num_acc3_ptbin3/pow(b.num_acc3_ptbin3,2))*a.err_acc3_ptbin3*b.err_acc3_ptbin3);
    c.err_acc3_ptbin4 = sqrt(pow(a.err_acc3_ptbin4/b.num_acc3_ptbin4, 2) + pow(a.num_acc3_ptbin4*b.err_acc3_ptbin4/pow(b.num_acc3_ptbin4,2), 2) + 2*rho*(1/b.num_acc3_ptbin4)*(-1*a.num_acc3_ptbin4/pow(b.num_acc3_ptbin4,2))*a.err_acc3_ptbin4*b.err_acc3_ptbin4);
    c.err_acc3_ptbin5 = sqrt(pow(a.err_acc3_ptbin5/b.num_acc3_ptbin5, 2) + pow(a.num_acc3_ptbin5*b.err_acc3_ptbin5/pow(b.num_acc3_ptbin5,2), 2) + 2*rho*(1/b.num_acc3_ptbin5)*(-1*a.num_acc3_ptbin5/pow(b.num_acc3_ptbin5,2))*a.err_acc3_ptbin5*b.err_acc3_ptbin5);
    c.err_acc3_etabin1 = sqrt(pow(a.err_acc3_etabin1/b.num_acc3_etabin1, 2) + pow(a.num_acc3_etabin1*b.err_acc3_etabin1/pow(b.num_acc3_etabin1,2), 2) + 2*rho*(1/b.num_acc3_etabin1)*(-1*a.num_acc3_etabin1/pow(b.num_acc3_etabin1,2))*a.err_acc3_etabin1*b.err_acc3_etabin1);
    c.err_acc3_etabin2 = sqrt(pow(a.err_acc3_etabin2/b.num_acc3_etabin2, 2) + pow(a.num_acc3_etabin2*b.err_acc3_etabin2/pow(b.num_acc3_etabin2,2), 2) + 2*rho*(1/b.num_acc3_etabin2)*(-1*a.num_acc3_etabin2/pow(b.num_acc3_etabin2,2))*a.err_acc3_etabin2*b.err_acc3_etabin2);
    c.err_acc3_etabin3 = sqrt(pow(a.err_acc3_etabin3/b.num_acc3_etabin3, 2) + pow(a.num_acc3_etabin3*b.err_acc3_etabin3/pow(b.num_acc3_etabin3,2), 2) + 2*rho*(1/b.num_acc3_etabin3)*(-1*a.num_acc3_etabin3/pow(b.num_acc3_etabin3,2))*a.err_acc3_etabin3*b.err_acc3_etabin3);
    c.err_acc3_etabin4 = sqrt(pow(a.err_acc3_etabin4/b.num_acc3_etabin4, 2) + pow(a.num_acc3_etabin4*b.err_acc3_etabin4/pow(b.num_acc3_etabin4,2), 2) + 2*rho*(1/b.num_acc3_etabin4)*(-1*a.num_acc3_etabin4/pow(b.num_acc3_etabin4,2))*a.err_acc3_etabin4*b.err_acc3_etabin4);
    c.err_acc3_etabin5 = sqrt(pow(a.err_acc3_etabin5/b.num_acc3_etabin5, 2) + pow(a.num_acc3_etabin5*b.err_acc3_etabin5/pow(b.num_acc3_etabin5,2), 2) + 2*rho*(1/b.num_acc3_etabin5)*(-1*a.num_acc3_etabin5/pow(b.num_acc3_etabin5,2))*a.err_acc3_etabin5*b.err_acc3_etabin5);
    c.num_corr = a.num_corr / b.num_corr;
    c.num_corr_ptbin1 = a.num_corr_ptbin1 / b.num_corr_ptbin1;
    c.num_corr_ptbin2 = a.num_corr_ptbin2 / b.num_corr_ptbin2;
    c.num_corr_ptbin3 = a.num_corr_ptbin3 / b.num_corr_ptbin3;
    c.num_corr_ptbin4 = a.num_corr_ptbin4 / b.num_corr_ptbin4;
    c.num_corr_ptbin5 = a.num_corr_ptbin5 / b.num_corr_ptbin5;
    c.num_corr_etabin1 = a.num_corr_etabin1 / b.num_corr_etabin1;
    c.num_corr_etabin2 = a.num_corr_etabin2 / b.num_corr_etabin2;
    c.num_corr_etabin3 = a.num_corr_etabin3 / b.num_corr_etabin3;
    c.num_corr_etabin4 = a.num_corr_etabin4 / b.num_corr_etabin4;
    c.num_corr_etabin5 = a.num_corr_etabin5 / b.num_corr_etabin5;
    c.err_corr = sqrt(pow(a.err_corr/b.num_corr, 2) + pow(a.num_corr*b.err_corr/pow(b.num_corr,2), 2) + 2*rho*(1/b.num_corr)*(-1*a.num_corr/pow(b.num_corr,2))*a.err_corr*b.err_corr);
    c.err_corr_ptbin1 = sqrt(pow(a.err_corr_ptbin1/b.num_corr_ptbin1, 2) + pow(a.num_corr_ptbin1*b.err_corr_ptbin1/pow(b.num_corr_ptbin1,2), 2) + 2*rho*(1/b.num_corr_ptbin1)*(-1*a.num_corr_ptbin1/pow(b.num_corr_ptbin1,2))*a.err_corr_ptbin1*b.err_corr_ptbin1);
    c.err_corr_ptbin2 = sqrt(pow(a.err_corr_ptbin2/b.num_corr_ptbin2, 2) + pow(a.num_corr_ptbin2*b.err_corr_ptbin2/pow(b.num_corr_ptbin2,2), 2) + 2*rho*(1/b.num_corr_ptbin2)*(-1*a.num_corr_ptbin2/pow(b.num_corr_ptbin2,2))*a.err_corr_ptbin2*b.err_corr_ptbin2);
    c.err_corr_ptbin3 = sqrt(pow(a.err_corr_ptbin3/b.num_corr_ptbin3, 2) + pow(a.num_corr_ptbin3*b.err_corr_ptbin3/pow(b.num_corr_ptbin3,2), 2) + 2*rho*(1/b.num_corr_ptbin3)*(-1*a.num_corr_ptbin3/pow(b.num_corr_ptbin3,2))*a.err_corr_ptbin3*b.err_corr_ptbin3);
    c.err_corr_ptbin4 = sqrt(pow(a.err_corr_ptbin4/b.num_corr_ptbin4, 2) + pow(a.num_corr_ptbin4*b.err_corr_ptbin4/pow(b.num_corr_ptbin4,2), 2) + 2*rho*(1/b.num_corr_ptbin4)*(-1*a.num_corr_ptbin4/pow(b.num_corr_ptbin4,2))*a.err_corr_ptbin4*b.err_corr_ptbin4);
    c.err_corr_ptbin5 = sqrt(pow(a.err_corr_ptbin5/b.num_corr_ptbin5, 2) + pow(a.num_corr_ptbin5*b.err_corr_ptbin5/pow(b.num_corr_ptbin5,2), 2) + 2*rho*(1/b.num_corr_ptbin5)*(-1*a.num_corr_ptbin5/pow(b.num_corr_ptbin5,2))*a.err_corr_ptbin5*b.err_corr_ptbin5);
    c.err_corr_etabin1 = sqrt(pow(a.err_corr_etabin1/b.num_corr_etabin1, 2) + pow(a.num_corr_etabin1*b.err_corr_etabin1/pow(b.num_corr_etabin1,2), 2) + 2*rho*(1/b.num_corr_etabin1)*(-1*a.num_corr_etabin1/pow(b.num_corr_etabin1,2))*a.err_corr_etabin1*b.err_corr_etabin1);
    c.err_corr_etabin2 = sqrt(pow(a.err_corr_etabin2/b.num_corr_etabin2, 2) + pow(a.num_corr_etabin2*b.err_corr_etabin2/pow(b.num_corr_etabin2,2), 2) + 2*rho*(1/b.num_corr_etabin2)*(-1*a.num_corr_etabin2/pow(b.num_corr_etabin2,2))*a.err_corr_etabin2*b.err_corr_etabin2);
    c.err_corr_etabin3 = sqrt(pow(a.err_corr_etabin3/b.num_corr_etabin3, 2) + pow(a.num_corr_etabin3*b.err_corr_etabin3/pow(b.num_corr_etabin3,2), 2) + 2*rho*(1/b.num_corr_etabin3)*(-1*a.num_corr_etabin3/pow(b.num_corr_etabin3,2))*a.err_corr_etabin3*b.err_corr_etabin3);
    c.err_corr_etabin4 = sqrt(pow(a.err_corr_etabin4/b.num_corr_etabin4, 2) + pow(a.num_corr_etabin4*b.err_corr_etabin4/pow(b.num_corr_etabin4,2), 2) + 2*rho*(1/b.num_corr_etabin4)*(-1*a.num_corr_etabin4/pow(b.num_corr_etabin4,2))*a.err_corr_etabin4*b.err_corr_etabin4);
    c.err_corr_etabin5 = sqrt(pow(a.err_corr_etabin5/b.num_corr_etabin5, 2) + pow(a.num_corr_etabin5*b.err_corr_etabin5/pow(b.num_corr_etabin5,2), 2) + 2*rho*(1/b.num_corr_etabin5)*(-1*a.num_corr_etabin5/pow(b.num_corr_etabin5,2))*a.err_corr_etabin5*b.err_corr_etabin5);
    c.num_acccorr = a.num_acccorr / b.num_acccorr;
    c.num_acccorr_ptbin1 = a.num_acccorr_ptbin1 / b.num_acccorr_ptbin1;
    c.num_acccorr_ptbin2 = a.num_acccorr_ptbin2 / b.num_acccorr_ptbin2;
    c.num_acccorr_ptbin3 = a.num_acccorr_ptbin3 / b.num_acccorr_ptbin3;
    c.num_acccorr_ptbin4 = a.num_acccorr_ptbin4 / b.num_acccorr_ptbin4;
    c.num_acccorr_ptbin5 = a.num_acccorr_ptbin5 / b.num_acccorr_ptbin5;
    c.num_acccorr_etabin1 = a.num_acccorr_etabin1 / b.num_acccorr_etabin1;
    c.num_acccorr_etabin2 = a.num_acccorr_etabin2 / b.num_acccorr_etabin2;
    c.num_acccorr_etabin3 = a.num_acccorr_etabin3 / b.num_acccorr_etabin3;
    c.num_acccorr_etabin4 = a.num_acccorr_etabin4 / b.num_acccorr_etabin4;
    c.num_acccorr_etabin5 = a.num_acccorr_etabin5 / b.num_acccorr_etabin5;
    c.err_acccorr = sqrt(pow(a.err_acccorr/b.num_acccorr, 2) + pow(a.num_acccorr*b.err_acccorr/pow(b.num_acccorr,2), 2) + 2*rho*(1/b.num_acccorr)*(-1*a.num_acccorr/pow(b.num_acccorr,2))*a.err_acccorr*b.err_acccorr);
    c.err_acccorr_ptbin1 = sqrt(pow(a.err_acccorr_ptbin1/b.num_acccorr_ptbin1, 2) + pow(a.num_acccorr_ptbin1*b.err_acccorr_ptbin1/pow(b.num_acccorr_ptbin1,2), 2) + 2*rho*(1/b.num_acccorr_ptbin1)*(-1*a.num_acccorr_ptbin1/pow(b.num_acccorr_ptbin1,2))*a.err_acccorr_ptbin1*b.err_acccorr_ptbin1);
    c.err_acccorr_ptbin2 = sqrt(pow(a.err_acccorr_ptbin2/b.num_acccorr_ptbin2, 2) + pow(a.num_acccorr_ptbin2*b.err_acccorr_ptbin2/pow(b.num_acccorr_ptbin2,2), 2) + 2*rho*(1/b.num_acccorr_ptbin2)*(-1*a.num_acccorr_ptbin2/pow(b.num_acccorr_ptbin2,2))*a.err_acccorr_ptbin2*b.err_acccorr_ptbin2);
    c.err_acccorr_ptbin3 = sqrt(pow(a.err_acccorr_ptbin3/b.num_acccorr_ptbin3, 2) + pow(a.num_acccorr_ptbin3*b.err_acccorr_ptbin3/pow(b.num_acccorr_ptbin3,2), 2) + 2*rho*(1/b.num_acccorr_ptbin3)*(-1*a.num_acccorr_ptbin3/pow(b.num_acccorr_ptbin3,2))*a.err_acccorr_ptbin3*b.err_acccorr_ptbin3);
    c.err_acccorr_ptbin4 = sqrt(pow(a.err_acccorr_ptbin4/b.num_acccorr_ptbin4, 2) + pow(a.num_acccorr_ptbin4*b.err_acccorr_ptbin4/pow(b.num_acccorr_ptbin4,2), 2) + 2*rho*(1/b.num_acccorr_ptbin4)*(-1*a.num_acccorr_ptbin4/pow(b.num_acccorr_ptbin4,2))*a.err_acccorr_ptbin4*b.err_acccorr_ptbin4);
    c.err_acccorr_ptbin5 = sqrt(pow(a.err_acccorr_ptbin5/b.num_acccorr_ptbin5, 2) + pow(a.num_acccorr_ptbin5*b.err_acccorr_ptbin5/pow(b.num_acccorr_ptbin5,2), 2) + 2*rho*(1/b.num_acccorr_ptbin5)*(-1*a.num_acccorr_ptbin5/pow(b.num_acccorr_ptbin5,2))*a.err_acccorr_ptbin5*b.err_acccorr_ptbin5);
    c.err_acccorr_etabin1 = sqrt(pow(a.err_acccorr_etabin1/b.num_acccorr_etabin1, 2) + pow(a.num_acccorr_etabin1*b.err_acccorr_etabin1/pow(b.num_acccorr_etabin1,2), 2) + 2*rho*(1/b.num_acccorr_etabin1)*(-1*a.num_acccorr_etabin1/pow(b.num_acccorr_etabin1,2))*a.err_acccorr_etabin1*b.err_acccorr_etabin1);
    c.err_acccorr_etabin2 = sqrt(pow(a.err_acccorr_etabin2/b.num_acccorr_etabin2, 2) + pow(a.num_acccorr_etabin2*b.err_acccorr_etabin2/pow(b.num_acccorr_etabin2,2), 2) + 2*rho*(1/b.num_acccorr_etabin2)*(-1*a.num_acccorr_etabin2/pow(b.num_acccorr_etabin2,2))*a.err_acccorr_etabin2*b.err_acccorr_etabin2);
    c.err_acccorr_etabin3 = sqrt(pow(a.err_acccorr_etabin3/b.num_acccorr_etabin3, 2) + pow(a.num_acccorr_etabin3*b.err_acccorr_etabin3/pow(b.num_acccorr_etabin3,2), 2) + 2*rho*(1/b.num_acccorr_etabin3)*(-1*a.num_acccorr_etabin3/pow(b.num_acccorr_etabin3,2))*a.err_acccorr_etabin3*b.err_acccorr_etabin3);
    c.err_acccorr_etabin4 = sqrt(pow(a.err_acccorr_etabin4/b.num_acccorr_etabin4, 2) + pow(a.num_acccorr_etabin4*b.err_acccorr_etabin4/pow(b.num_acccorr_etabin4,2), 2) + 2*rho*(1/b.num_acccorr_etabin4)*(-1*a.num_acccorr_etabin4/pow(b.num_acccorr_etabin4,2))*a.err_acccorr_etabin4*b.err_acccorr_etabin4);
    c.err_acccorr_etabin5 = sqrt(pow(a.err_acccorr_etabin5/b.num_acccorr_etabin5, 2) + pow(a.num_acccorr_etabin5*b.err_acccorr_etabin5/pow(b.num_acccorr_etabin5,2), 2) + 2*rho*(1/b.num_acccorr_etabin5)*(-1*a.num_acccorr_etabin5/pow(b.num_acccorr_etabin5,2))*a.err_acccorr_etabin5*b.err_acccorr_etabin5);

    return c;
}

Counter GetCounterRatio(Counter &a, Counter &b, double rho) {
    Counter c;
    c.num_fidu = a.num_fidu / b.num_fidu;
    c.num_fidu_ptbin1 = a.num_fidu_ptbin1 / b.num_fidu_ptbin1;
    c.num_fidu_ptbin2 = a.num_fidu_ptbin2 / b.num_fidu_ptbin2;
    c.num_fidu_ptbin3 = a.num_fidu_ptbin3 / b.num_fidu_ptbin3;
    c.num_fidu_ptbin4 = a.num_fidu_ptbin4 / b.num_fidu_ptbin4;
    c.num_fidu_ptbin5 = a.num_fidu_ptbin5 / b.num_fidu_ptbin5;
    c.num_fidu_etabin1 = a.num_fidu_etabin1 / b.num_fidu_etabin1;
    c.num_fidu_etabin2 = a.num_fidu_etabin2 / b.num_fidu_etabin2;
    c.num_fidu_etabin3 = a.num_fidu_etabin3 / b.num_fidu_etabin3;
    c.num_fidu_etabin4 = a.num_fidu_etabin4 / b.num_fidu_etabin4;
    c.num_fidu_etabin5 = a.num_fidu_etabin5 / b.num_fidu_etabin5;
    c.err_fidu = sqrt(pow(a.err_fidu/b.num_fidu, 2) + pow(a.num_fidu*b.err_fidu/pow(b.num_fidu,2), 2) + 2*rho*(1/b.num_fidu)*(-1*a.num_fidu/pow(b.num_fidu,2))*a.err_fidu*b.err_fidu);
    c.err_fidu_ptbin1 = sqrt(pow(a.err_fidu_ptbin1/b.num_fidu_ptbin1, 2) + pow(a.num_fidu_ptbin1*b.err_fidu_ptbin1/pow(b.num_fidu_ptbin1,2), 2) + 2*rho*(1/b.num_fidu_ptbin1)*(-1*a.num_fidu_ptbin1/pow(b.num_fidu_ptbin1,2))*a.err_fidu_ptbin1*b.err_fidu_ptbin1);
    c.err_fidu_ptbin2 = sqrt(pow(a.err_fidu_ptbin2/b.num_fidu_ptbin2, 2) + pow(a.num_fidu_ptbin2*b.err_fidu_ptbin2/pow(b.num_fidu_ptbin2,2), 2) + 2*rho*(1/b.num_fidu_ptbin2)*(-1*a.num_fidu_ptbin2/pow(b.num_fidu_ptbin2,2))*a.err_fidu_ptbin2*b.err_fidu_ptbin2);
    c.err_fidu_ptbin3 = sqrt(pow(a.err_fidu_ptbin3/b.num_fidu_ptbin3, 2) + pow(a.num_fidu_ptbin3*b.err_fidu_ptbin3/pow(b.num_fidu_ptbin3,2), 2) + 2*rho*(1/b.num_fidu_ptbin3)*(-1*a.num_fidu_ptbin3/pow(b.num_fidu_ptbin3,2))*a.err_fidu_ptbin3*b.err_fidu_ptbin3);
    c.err_fidu_ptbin4 = sqrt(pow(a.err_fidu_ptbin4/b.num_fidu_ptbin4, 2) + pow(a.num_fidu_ptbin4*b.err_fidu_ptbin4/pow(b.num_fidu_ptbin4,2), 2) + 2*rho*(1/b.num_fidu_ptbin4)*(-1*a.num_fidu_ptbin4/pow(b.num_fidu_ptbin4,2))*a.err_fidu_ptbin4*b.err_fidu_ptbin4);
    c.err_fidu_ptbin5 = sqrt(pow(a.err_fidu_ptbin5/b.num_fidu_ptbin5, 2) + pow(a.num_fidu_ptbin5*b.err_fidu_ptbin5/pow(b.num_fidu_ptbin5,2), 2) + 2*rho*(1/b.num_fidu_ptbin5)*(-1*a.num_fidu_ptbin5/pow(b.num_fidu_ptbin5,2))*a.err_fidu_ptbin5*b.err_fidu_ptbin5);
    c.err_fidu_etabin1 = sqrt(pow(a.err_fidu_etabin1/b.num_fidu_etabin1, 2) + pow(a.num_fidu_etabin1*b.err_fidu_etabin1/pow(b.num_fidu_etabin1,2), 2) + 2*rho*(1/b.num_fidu_etabin1)*(-1*a.num_fidu_etabin1/pow(b.num_fidu_etabin1,2))*a.err_fidu_etabin1*b.err_fidu_etabin1);
    c.err_fidu_etabin2 = sqrt(pow(a.err_fidu_etabin2/b.num_fidu_etabin2, 2) + pow(a.num_fidu_etabin2*b.err_fidu_etabin2/pow(b.num_fidu_etabin2,2), 2) + 2*rho*(1/b.num_fidu_etabin2)*(-1*a.num_fidu_etabin2/pow(b.num_fidu_etabin2,2))*a.err_fidu_etabin2*b.err_fidu_etabin2);
    c.err_fidu_etabin3 = sqrt(pow(a.err_fidu_etabin3/b.num_fidu_etabin3, 2) + pow(a.num_fidu_etabin3*b.err_fidu_etabin3/pow(b.num_fidu_etabin3,2), 2) + 2*rho*(1/b.num_fidu_etabin3)*(-1*a.num_fidu_etabin3/pow(b.num_fidu_etabin3,2))*a.err_fidu_etabin3*b.err_fidu_etabin3);
    c.err_fidu_etabin4 = sqrt(pow(a.err_fidu_etabin4/b.num_fidu_etabin4, 2) + pow(a.num_fidu_etabin4*b.err_fidu_etabin4/pow(b.num_fidu_etabin4,2), 2) + 2*rho*(1/b.num_fidu_etabin4)*(-1*a.num_fidu_etabin4/pow(b.num_fidu_etabin4,2))*a.err_fidu_etabin4*b.err_fidu_etabin4);
    c.err_fidu_etabin5 = sqrt(pow(a.err_fidu_etabin5/b.num_fidu_etabin5, 2) + pow(a.num_fidu_etabin5*b.err_fidu_etabin5/pow(b.num_fidu_etabin5,2), 2) + 2*rho*(1/b.num_fidu_etabin5)*(-1*a.num_fidu_etabin5/pow(b.num_fidu_etabin5,2))*a.err_fidu_etabin5*b.err_fidu_etabin5);

    return c;
}

AccRatio GetAccRatioRatio(AccRatio &a, AccRatio &b, double rho) {
    AccRatio c;
    c.num_accratio = a.num_accratio / b.num_accratio;
    c.num_accratio_ptbin1 = a.num_accratio_ptbin1 / b.num_accratio_ptbin1;
    c.num_accratio_ptbin2 = a.num_accratio_ptbin2 / b.num_accratio_ptbin2;
    c.num_accratio_ptbin3 = a.num_accratio_ptbin3 / b.num_accratio_ptbin3;
    c.num_accratio_ptbin4 = a.num_accratio_ptbin4 / b.num_accratio_ptbin4;
    c.num_accratio_ptbin5 = a.num_accratio_ptbin5 / b.num_accratio_ptbin5;
    c.num_accratio_etabin1 = a.num_accratio_etabin1 / b.num_accratio_etabin1;
    c.num_accratio_etabin2 = a.num_accratio_etabin2 / b.num_accratio_etabin2;
    c.num_accratio_etabin3 = a.num_accratio_etabin3 / b.num_accratio_etabin3;
    c.num_accratio_etabin4 = a.num_accratio_etabin4 / b.num_accratio_etabin4;
    c.num_accratio_etabin5 = a.num_accratio_etabin5 / b.num_accratio_etabin5;
    c.err_accratio = sqrt(pow(a.err_accratio/b.num_accratio, 2) + pow(a.num_accratio*b.err_accratio/pow(b.num_accratio,2), 2) + 2*rho*(1/b.num_accratio)*(-1*a.num_accratio/pow(b.num_accratio,2))*a.err_accratio*b.err_accratio);
    c.err_accratio_ptbin1 = sqrt(pow(a.err_accratio_ptbin1/b.num_accratio_ptbin1, 2) + pow(a.num_accratio_ptbin1*b.err_accratio_ptbin1/pow(b.num_accratio_ptbin1,2), 2) + 2*rho*(1/b.num_accratio_ptbin1)*(-1*a.num_accratio_ptbin1/pow(b.num_accratio_ptbin1,2))*a.err_accratio_ptbin1*b.err_accratio_ptbin1);
    c.err_accratio_ptbin2 = sqrt(pow(a.err_accratio_ptbin2/b.num_accratio_ptbin2, 2) + pow(a.num_accratio_ptbin2*b.err_accratio_ptbin2/pow(b.num_accratio_ptbin2,2), 2) + 2*rho*(1/b.num_accratio_ptbin2)*(-1*a.num_accratio_ptbin2/pow(b.num_accratio_ptbin2,2))*a.err_accratio_ptbin2*b.err_accratio_ptbin2);
    c.err_accratio_ptbin3 = sqrt(pow(a.err_accratio_ptbin3/b.num_accratio_ptbin3, 2) + pow(a.num_accratio_ptbin3*b.err_accratio_ptbin3/pow(b.num_accratio_ptbin3,2), 2) + 2*rho*(1/b.num_accratio_ptbin3)*(-1*a.num_accratio_ptbin3/pow(b.num_accratio_ptbin3,2))*a.err_accratio_ptbin3*b.err_accratio_ptbin3);
    c.err_accratio_ptbin4 = sqrt(pow(a.err_accratio_ptbin4/b.num_accratio_ptbin4, 2) + pow(a.num_accratio_ptbin4*b.err_accratio_ptbin4/pow(b.num_accratio_ptbin4,2), 2) + 2*rho*(1/b.num_accratio_ptbin4)*(-1*a.num_accratio_ptbin4/pow(b.num_accratio_ptbin4,2))*a.err_accratio_ptbin4*b.err_accratio_ptbin4);
    c.err_accratio_ptbin5 = sqrt(pow(a.err_accratio_ptbin5/b.num_accratio_ptbin5, 2) + pow(a.num_accratio_ptbin5*b.err_accratio_ptbin5/pow(b.num_accratio_ptbin5,2), 2) + 2*rho*(1/b.num_accratio_ptbin5)*(-1*a.num_accratio_ptbin5/pow(b.num_accratio_ptbin5,2))*a.err_accratio_ptbin5*b.err_accratio_ptbin5);
    c.err_accratio_etabin1 = sqrt(pow(a.err_accratio_etabin1/b.num_accratio_etabin1, 2) + pow(a.num_accratio_etabin1*b.err_accratio_etabin1/pow(b.num_accratio_etabin1,2), 2) + 2*rho*(1/b.num_accratio_etabin1)*(-1*a.num_accratio_etabin1/pow(b.num_accratio_etabin1,2))*a.err_accratio_etabin1*b.err_accratio_etabin1);
    c.err_accratio_etabin2 = sqrt(pow(a.err_accratio_etabin2/b.num_accratio_etabin2, 2) + pow(a.num_accratio_etabin2*b.err_accratio_etabin2/pow(b.num_accratio_etabin2,2), 2) + 2*rho*(1/b.num_accratio_etabin2)*(-1*a.num_accratio_etabin2/pow(b.num_accratio_etabin2,2))*a.err_accratio_etabin2*b.err_accratio_etabin2);
    c.err_accratio_etabin3 = sqrt(pow(a.err_accratio_etabin3/b.num_accratio_etabin3, 2) + pow(a.num_accratio_etabin3*b.err_accratio_etabin3/pow(b.num_accratio_etabin3,2), 2) + 2*rho*(1/b.num_accratio_etabin3)*(-1*a.num_accratio_etabin3/pow(b.num_accratio_etabin3,2))*a.err_accratio_etabin3*b.err_accratio_etabin3);
    c.err_accratio_etabin4 = sqrt(pow(a.err_accratio_etabin4/b.num_accratio_etabin4, 2) + pow(a.num_accratio_etabin4*b.err_accratio_etabin4/pow(b.num_accratio_etabin4,2), 2) + 2*rho*(1/b.num_accratio_etabin4)*(-1*a.num_accratio_etabin4/pow(b.num_accratio_etabin4,2))*a.err_accratio_etabin4*b.err_accratio_etabin4);
    c.err_accratio_etabin5 = sqrt(pow(a.err_accratio_etabin5/b.num_accratio_etabin5, 2) + pow(a.num_accratio_etabin5*b.err_accratio_etabin5/pow(b.num_accratio_etabin5,2), 2) + 2*rho*(1/b.num_accratio_etabin5)*(-1*a.num_accratio_etabin5/pow(b.num_accratio_etabin5,2))*a.err_accratio_etabin5*b.err_accratio_etabin5);
    return c;
}

double GetErrorAoverAB(double a, double err_a, double ab, double err_ab) {
    double b = ab - a;
    double err_b = sqrt(pow(err_ab,2) - pow(err_a,2));
    double dfda = b / pow(a+b, 2);
    double dfdb = -1 * a / pow(a+b, 2);
    double err = sqrt(pow(dfda * err_a, 2) + pow(dfdb * err_b, 2));
    return err;
}

double GetErrorAoverB(double a, double err_a, double b, double err_b) {
    double aob = a / b;
    double relerr_aob = sqrt(pow(err_a/a,2) + pow(err_b/b,2));
    return aob*relerr_aob;
}

Counter GetCounterEnvelope(vector<Counter> &cs) {
    Counter c = EmptyCounter();
    double max_fidu = -999999;       double min_fidu = 999999;
    double max_fidu_ptbin1 = -999999;   double min_fidu_ptbin1 = 999999;
    double max_fidu_ptbin2 = -999999;   double min_fidu_ptbin2 = 999999;
    double max_fidu_ptbin3 = -999999;   double min_fidu_ptbin3 = 999999;
    double max_fidu_ptbin4 = -999999;   double min_fidu_ptbin4 = 999999;
    double max_fidu_ptbin5 = -999999;   double min_fidu_ptbin5 = 999999;
    double max_fidu_etabin1 = -999999;   double min_fidu_etabin1 = 999999;
    double max_fidu_etabin2 = -999999;   double min_fidu_etabin2 = 999999;
    double max_fidu_etabin3 = -999999;   double min_fidu_etabin3 = 999999;
    double max_fidu_etabin4 = -999999;   double min_fidu_etabin4 = 999999;
    double max_fidu_etabin5 = -999999;   double min_fidu_etabin5 = 999999;
    for (int i = 0; i < cs.size(); i++) {
	if (max_fidu <= cs.at(i).num_fidu) max_fidu = cs.at(i).num_fidu; if (min_fidu >= cs.at(i).num_fidu) min_fidu = cs.at(i).num_fidu;
	if (max_fidu_ptbin1 <= cs.at(i).num_fidu_ptbin1) max_fidu_ptbin1 = cs.at(i).num_fidu_ptbin1; if (min_fidu_ptbin1 >= cs.at(i).num_fidu_ptbin1) min_fidu_ptbin1 = cs.at(i).num_fidu_ptbin1;
	if (max_fidu_ptbin2 <= cs.at(i).num_fidu_ptbin2) max_fidu_ptbin2 = cs.at(i).num_fidu_ptbin2; if (min_fidu_ptbin2 >= cs.at(i).num_fidu_ptbin2) min_fidu_ptbin2 = cs.at(i).num_fidu_ptbin2;
	if (max_fidu_ptbin3 <= cs.at(i).num_fidu_ptbin3) max_fidu_ptbin3 = cs.at(i).num_fidu_ptbin3; if (min_fidu_ptbin3 >= cs.at(i).num_fidu_ptbin3) min_fidu_ptbin3 = cs.at(i).num_fidu_ptbin3;
	if (max_fidu_ptbin4 <= cs.at(i).num_fidu_ptbin4) max_fidu_ptbin4 = cs.at(i).num_fidu_ptbin4; if (min_fidu_ptbin4 >= cs.at(i).num_fidu_ptbin4) min_fidu_ptbin4 = cs.at(i).num_fidu_ptbin4;
	if (max_fidu_ptbin5 <= cs.at(i).num_fidu_ptbin5) max_fidu_ptbin5 = cs.at(i).num_fidu_ptbin5; if (min_fidu_ptbin5 >= cs.at(i).num_fidu_ptbin5) min_fidu_ptbin5 = cs.at(i).num_fidu_ptbin5;
	if (max_fidu_etabin1 <= cs.at(i).num_fidu_etabin1) max_fidu_etabin1 = cs.at(i).num_fidu_etabin1; if (min_fidu_etabin1 >= cs.at(i).num_fidu_etabin1) min_fidu_etabin1 = cs.at(i).num_fidu_etabin1;
	if (max_fidu_etabin2 <= cs.at(i).num_fidu_etabin2) max_fidu_etabin2 = cs.at(i).num_fidu_etabin2; if (min_fidu_etabin2 >= cs.at(i).num_fidu_etabin2) min_fidu_etabin2 = cs.at(i).num_fidu_etabin2;
	if (max_fidu_etabin3 <= cs.at(i).num_fidu_etabin3) max_fidu_etabin3 = cs.at(i).num_fidu_etabin3; if (min_fidu_etabin3 >= cs.at(i).num_fidu_etabin3) min_fidu_etabin3 = cs.at(i).num_fidu_etabin3;
	if (max_fidu_etabin4 <= cs.at(i).num_fidu_etabin4) max_fidu_etabin4 = cs.at(i).num_fidu_etabin4; if (min_fidu_etabin4 >= cs.at(i).num_fidu_etabin4) min_fidu_etabin4 = cs.at(i).num_fidu_etabin4;
	if (max_fidu_etabin5 <= cs.at(i).num_fidu_etabin5) max_fidu_etabin5 = cs.at(i).num_fidu_etabin5; if (min_fidu_etabin5 >= cs.at(i).num_fidu_etabin5) min_fidu_etabin5 = cs.at(i).num_fidu_etabin5;
    }
    c.num_fidu = (max_fidu - min_fidu) / 2;
    c.num_fidu_ptbin1 = (max_fidu_ptbin1 - min_fidu_ptbin1) / 2;
    c.num_fidu_ptbin2 = (max_fidu_ptbin2 - min_fidu_ptbin2) / 2;
    c.num_fidu_ptbin3 = (max_fidu_ptbin3 - min_fidu_ptbin3) / 2;
    c.num_fidu_ptbin4 = (max_fidu_ptbin4 - min_fidu_ptbin4) / 2;
    c.num_fidu_ptbin5 = (max_fidu_ptbin5 - min_fidu_ptbin5) / 2;
    c.num_fidu_etabin1 = (max_fidu_etabin1 - min_fidu_etabin1) / 2;
    c.num_fidu_etabin2 = (max_fidu_etabin2 - min_fidu_etabin2) / 2;
    c.num_fidu_etabin3 = (max_fidu_etabin3 - min_fidu_etabin3) / 2;
    c.num_fidu_etabin4 = (max_fidu_etabin4 - min_fidu_etabin4) / 2;
    c.num_fidu_etabin5 = (max_fidu_etabin5 - min_fidu_etabin5) / 2;

    return c;
}

AccCorr GetAccCorrEnvelope(vector<AccCorr> &acs) {
    AccCorr c = EmptyAccCorr();
    double max_acc = -999999;       double min_acc = 999999;
    double max_acc_ptbin1 = -999999;   double min_acc_ptbin1 = 999999;
    double max_acc_ptbin2 = -999999;   double min_acc_ptbin2 = 999999;
    double max_acc_ptbin3 = -999999;   double min_acc_ptbin3 = 999999;
    double max_acc_ptbin4 = -999999;   double min_acc_ptbin4 = 999999;
    double max_acc_ptbin5 = -999999;   double min_acc_ptbin5 = 999999;
    double max_acc_etabin1 = -999999;   double min_acc_etabin1 = 999999;
    double max_acc_etabin2 = -999999;   double min_acc_etabin2 = 999999;
    double max_acc_etabin3 = -999999;   double min_acc_etabin3 = 999999;
    double max_acc_etabin4 = -999999;   double min_acc_etabin4 = 999999;
    double max_acc_etabin5 = -999999;   double min_acc_etabin5 = 999999;
    double max_acc2 = -999999;       double min_acc2 = 999999;
    double max_acc2_ptbin1 = -999999;   double min_acc2_ptbin1 = 999999;
    double max_acc2_ptbin2 = -999999;   double min_acc2_ptbin2 = 999999;
    double max_acc2_ptbin3 = -999999;   double min_acc2_ptbin3 = 999999;
    double max_acc2_ptbin4 = -999999;   double min_acc2_ptbin4 = 999999;
    double max_acc2_ptbin5 = -999999;   double min_acc2_ptbin5 = 999999;
    double max_acc2_etabin1 = -999999;   double min_acc2_etabin1 = 999999;
    double max_acc2_etabin2 = -999999;   double min_acc2_etabin2 = 999999;
    double max_acc2_etabin3 = -999999;   double min_acc2_etabin3 = 999999;
    double max_acc2_etabin4 = -999999;   double min_acc2_etabin4 = 999999;
    double max_acc2_etabin5 = -999999;   double min_acc2_etabin5 = 999999;
    double max_acc3 = -999999;       double min_acc3 = 999999;
    double max_acc3_ptbin1 = -999999;   double min_acc3_ptbin1 = 999999;
    double max_acc3_ptbin2 = -999999;   double min_acc3_ptbin2 = 999999;
    double max_acc3_ptbin3 = -999999;   double min_acc3_ptbin3 = 999999;
    double max_acc3_ptbin4 = -999999;   double min_acc3_ptbin4 = 999999;
    double max_acc3_ptbin5 = -999999;   double min_acc3_ptbin5 = 999999;
    double max_acc3_etabin1 = -999999;   double min_acc3_etabin1 = 999999;
    double max_acc3_etabin2 = -999999;   double min_acc3_etabin2 = 999999;
    double max_acc3_etabin3 = -999999;   double min_acc3_etabin3 = 999999;
    double max_acc3_etabin4 = -999999;   double min_acc3_etabin4 = 999999;
    double max_acc3_etabin5 = -999999;   double min_acc3_etabin5 = 999999;
    double max_corr = -999999;       double min_corr = 999999;
    double max_corr_ptbin1 = -999999;   double min_corr_ptbin1 = 999999;
    double max_corr_ptbin2 = -999999;   double min_corr_ptbin2 = 999999;
    double max_corr_ptbin3 = -999999;   double min_corr_ptbin3 = 999999;
    double max_corr_ptbin4 = -999999;   double min_corr_ptbin4 = 999999;
    double max_corr_ptbin5 = -999999;   double min_corr_ptbin5 = 999999;
    double max_corr_etabin1 = -999999;   double min_corr_etabin1 = 999999;
    double max_corr_etabin2 = -999999;   double min_corr_etabin2 = 999999;
    double max_corr_etabin3 = -999999;   double min_corr_etabin3 = 999999;
    double max_corr_etabin4 = -999999;   double min_corr_etabin4 = 999999;
    double max_corr_etabin5 = -999999;   double min_corr_etabin5 = 999999;
    double max_acccorr = -999999;       double min_acccorr = 999999;
    double max_acccorr_ptbin1 = -999999;   double min_acccorr_ptbin1 = 999999;
    double max_acccorr_ptbin2 = -999999;   double min_acccorr_ptbin2 = 999999;
    double max_acccorr_ptbin3 = -999999;   double min_acccorr_ptbin3 = 999999;
    double max_acccorr_ptbin4 = -999999;   double min_acccorr_ptbin4 = 999999;
    double max_acccorr_ptbin5 = -999999;   double min_acccorr_ptbin5 = 999999;
    double max_acccorr_etabin1 = -999999;   double min_acccorr_etabin1 = 999999;
    double max_acccorr_etabin2 = -999999;   double min_acccorr_etabin2 = 999999;
    double max_acccorr_etabin3 = -999999;   double min_acccorr_etabin3 = 999999;
    double max_acccorr_etabin4 = -999999;   double min_acccorr_etabin4 = 999999;
    double max_acccorr_etabin5 = -999999;   double min_acccorr_etabin5 = 999999;
    for (int i = 0; i < acs.size(); i++) {
	if (max_acc <= acs.at(i).num_acc) max_acc = acs.at(i).num_acc; if (min_acc >= acs.at(i).num_acc) min_acc = acs.at(i).num_acc;
	if (max_acc_ptbin1 <= acs.at(i).num_acc_ptbin1) max_acc_ptbin1 = acs.at(i).num_acc_ptbin1; if (min_acc_ptbin1 >= acs.at(i).num_acc_ptbin1) min_acc_ptbin1 = acs.at(i).num_acc_ptbin1;
	if (max_acc_ptbin2 <= acs.at(i).num_acc_ptbin2) max_acc_ptbin2 = acs.at(i).num_acc_ptbin2; if (min_acc_ptbin2 >= acs.at(i).num_acc_ptbin2) min_acc_ptbin2 = acs.at(i).num_acc_ptbin2;
	if (max_acc_ptbin3 <= acs.at(i).num_acc_ptbin3) max_acc_ptbin3 = acs.at(i).num_acc_ptbin3; if (min_acc_ptbin3 >= acs.at(i).num_acc_ptbin3) min_acc_ptbin3 = acs.at(i).num_acc_ptbin3;
	if (max_acc_ptbin4 <= acs.at(i).num_acc_ptbin4) max_acc_ptbin4 = acs.at(i).num_acc_ptbin4; if (min_acc_ptbin4 >= acs.at(i).num_acc_ptbin4) min_acc_ptbin4 = acs.at(i).num_acc_ptbin4;
	if (max_acc_ptbin5 <= acs.at(i).num_acc_ptbin5) max_acc_ptbin5 = acs.at(i).num_acc_ptbin5; if (min_acc_ptbin5 >= acs.at(i).num_acc_ptbin5) min_acc_ptbin5 = acs.at(i).num_acc_ptbin5;
	if (max_acc_etabin1 <= acs.at(i).num_acc_etabin1) max_acc_etabin1 = acs.at(i).num_acc_etabin1; if (min_acc_etabin1 >= acs.at(i).num_acc_etabin1) min_acc_etabin1 = acs.at(i).num_acc_etabin1;
	if (max_acc_etabin2 <= acs.at(i).num_acc_etabin2) max_acc_etabin2 = acs.at(i).num_acc_etabin2; if (min_acc_etabin2 >= acs.at(i).num_acc_etabin2) min_acc_etabin2 = acs.at(i).num_acc_etabin2;
	if (max_acc_etabin3 <= acs.at(i).num_acc_etabin3) max_acc_etabin3 = acs.at(i).num_acc_etabin3; if (min_acc_etabin3 >= acs.at(i).num_acc_etabin3) min_acc_etabin3 = acs.at(i).num_acc_etabin3;
	if (max_acc_etabin4 <= acs.at(i).num_acc_etabin4) max_acc_etabin4 = acs.at(i).num_acc_etabin4; if (min_acc_etabin4 >= acs.at(i).num_acc_etabin4) min_acc_etabin4 = acs.at(i).num_acc_etabin4;
	if (max_acc_etabin5 <= acs.at(i).num_acc_etabin5) max_acc_etabin5 = acs.at(i).num_acc_etabin5; if (min_acc_etabin5 >= acs.at(i).num_acc_etabin5) min_acc_etabin5 = acs.at(i).num_acc_etabin5;
	if (max_acc2 <= acs.at(i).num_acc2) max_acc2 = acs.at(i).num_acc2; if (min_acc2 >= acs.at(i).num_acc2) min_acc2 = acs.at(i).num_acc2;
	if (max_acc2_ptbin1 <= acs.at(i).num_acc2_ptbin1) max_acc2_ptbin1 = acs.at(i).num_acc2_ptbin1; if (min_acc2_ptbin1 >= acs.at(i).num_acc2_ptbin1) min_acc2_ptbin1 = acs.at(i).num_acc2_ptbin1;
	if (max_acc2_ptbin2 <= acs.at(i).num_acc2_ptbin2) max_acc2_ptbin2 = acs.at(i).num_acc2_ptbin2; if (min_acc2_ptbin2 >= acs.at(i).num_acc2_ptbin2) min_acc2_ptbin2 = acs.at(i).num_acc2_ptbin2;
	if (max_acc2_ptbin3 <= acs.at(i).num_acc2_ptbin3) max_acc2_ptbin3 = acs.at(i).num_acc2_ptbin3; if (min_acc2_ptbin3 >= acs.at(i).num_acc2_ptbin3) min_acc2_ptbin3 = acs.at(i).num_acc2_ptbin3;
	if (max_acc2_ptbin4 <= acs.at(i).num_acc2_ptbin4) max_acc2_ptbin4 = acs.at(i).num_acc2_ptbin4; if (min_acc2_ptbin4 >= acs.at(i).num_acc2_ptbin4) min_acc2_ptbin4 = acs.at(i).num_acc2_ptbin4;
	if (max_acc2_ptbin5 <= acs.at(i).num_acc2_ptbin5) max_acc2_ptbin5 = acs.at(i).num_acc2_ptbin5; if (min_acc2_ptbin5 >= acs.at(i).num_acc2_ptbin5) min_acc2_ptbin5 = acs.at(i).num_acc2_ptbin5;
	if (max_acc2_etabin1 <= acs.at(i).num_acc2_etabin1) max_acc2_etabin1 = acs.at(i).num_acc2_etabin1; if (min_acc2_etabin1 >= acs.at(i).num_acc2_etabin1) min_acc2_etabin1 = acs.at(i).num_acc2_etabin1;
	if (max_acc2_etabin2 <= acs.at(i).num_acc2_etabin2) max_acc2_etabin2 = acs.at(i).num_acc2_etabin2; if (min_acc2_etabin2 >= acs.at(i).num_acc2_etabin2) min_acc2_etabin2 = acs.at(i).num_acc2_etabin2;
	if (max_acc2_etabin3 <= acs.at(i).num_acc2_etabin3) max_acc2_etabin3 = acs.at(i).num_acc2_etabin3; if (min_acc2_etabin3 >= acs.at(i).num_acc2_etabin3) min_acc2_etabin3 = acs.at(i).num_acc2_etabin3;
	if (max_acc2_etabin4 <= acs.at(i).num_acc2_etabin4) max_acc2_etabin4 = acs.at(i).num_acc2_etabin4; if (min_acc2_etabin4 >= acs.at(i).num_acc2_etabin4) min_acc2_etabin4 = acs.at(i).num_acc2_etabin4;
	if (max_acc2_etabin5 <= acs.at(i).num_acc2_etabin5) max_acc2_etabin5 = acs.at(i).num_acc2_etabin5; if (min_acc2_etabin5 >= acs.at(i).num_acc2_etabin5) min_acc2_etabin5 = acs.at(i).num_acc2_etabin5;
	if (max_acc3 <= acs.at(i).num_acc3) max_acc3 = acs.at(i).num_acc3; if (min_acc3 >= acs.at(i).num_acc3) min_acc3 = acs.at(i).num_acc3;
	if (max_acc3_ptbin1 <= acs.at(i).num_acc3_ptbin1) max_acc3_ptbin1 = acs.at(i).num_acc3_ptbin1; if (min_acc3_ptbin1 >= acs.at(i).num_acc3_ptbin1) min_acc3_ptbin1 = acs.at(i).num_acc3_ptbin1;
	if (max_acc3_ptbin2 <= acs.at(i).num_acc3_ptbin2) max_acc3_ptbin2 = acs.at(i).num_acc3_ptbin2; if (min_acc3_ptbin2 >= acs.at(i).num_acc3_ptbin2) min_acc3_ptbin2 = acs.at(i).num_acc3_ptbin2;
	if (max_acc3_ptbin3 <= acs.at(i).num_acc3_ptbin3) max_acc3_ptbin3 = acs.at(i).num_acc3_ptbin3; if (min_acc3_ptbin3 >= acs.at(i).num_acc3_ptbin3) min_acc3_ptbin3 = acs.at(i).num_acc3_ptbin3;
	if (max_acc3_ptbin4 <= acs.at(i).num_acc3_ptbin4) max_acc3_ptbin4 = acs.at(i).num_acc3_ptbin4; if (min_acc3_ptbin4 >= acs.at(i).num_acc3_ptbin4) min_acc3_ptbin4 = acs.at(i).num_acc3_ptbin4;
	if (max_acc3_ptbin5 <= acs.at(i).num_acc3_ptbin5) max_acc3_ptbin5 = acs.at(i).num_acc3_ptbin5; if (min_acc3_ptbin5 >= acs.at(i).num_acc3_ptbin5) min_acc3_ptbin5 = acs.at(i).num_acc3_ptbin5;
	if (max_acc3_etabin1 <= acs.at(i).num_acc3_etabin1) max_acc3_etabin1 = acs.at(i).num_acc3_etabin1; if (min_acc3_etabin1 >= acs.at(i).num_acc3_etabin1) min_acc3_etabin1 = acs.at(i).num_acc3_etabin1;
	if (max_acc3_etabin2 <= acs.at(i).num_acc3_etabin2) max_acc3_etabin2 = acs.at(i).num_acc3_etabin2; if (min_acc3_etabin2 >= acs.at(i).num_acc3_etabin2) min_acc3_etabin2 = acs.at(i).num_acc3_etabin2;
	if (max_acc3_etabin3 <= acs.at(i).num_acc3_etabin3) max_acc3_etabin3 = acs.at(i).num_acc3_etabin3; if (min_acc3_etabin3 >= acs.at(i).num_acc3_etabin3) min_acc3_etabin3 = acs.at(i).num_acc3_etabin3;
	if (max_acc3_etabin4 <= acs.at(i).num_acc3_etabin4) max_acc3_etabin4 = acs.at(i).num_acc3_etabin4; if (min_acc3_etabin4 >= acs.at(i).num_acc3_etabin4) min_acc3_etabin4 = acs.at(i).num_acc3_etabin4;
	if (max_acc3_etabin5 <= acs.at(i).num_acc3_etabin5) max_acc3_etabin5 = acs.at(i).num_acc3_etabin5; if (min_acc3_etabin5 >= acs.at(i).num_acc3_etabin5) min_acc3_etabin5 = acs.at(i).num_acc3_etabin5;
	if (max_corr <= acs.at(i).num_corr) max_corr = acs.at(i).num_corr; if (min_corr >= acs.at(i).num_corr) min_corr = acs.at(i).num_corr;
	if (max_corr_ptbin1 <= acs.at(i).num_corr_ptbin1) max_corr_ptbin1 = acs.at(i).num_corr_ptbin1; if (min_corr_ptbin1 >= acs.at(i).num_corr_ptbin1) min_corr_ptbin1 = acs.at(i).num_corr_ptbin1;
	if (max_corr_ptbin2 <= acs.at(i).num_corr_ptbin2) max_corr_ptbin2 = acs.at(i).num_corr_ptbin2; if (min_corr_ptbin2 >= acs.at(i).num_corr_ptbin2) min_corr_ptbin2 = acs.at(i).num_corr_ptbin2;
	if (max_corr_ptbin3 <= acs.at(i).num_corr_ptbin3) max_corr_ptbin3 = acs.at(i).num_corr_ptbin3; if (min_corr_ptbin3 >= acs.at(i).num_corr_ptbin3) min_corr_ptbin3 = acs.at(i).num_corr_ptbin3;
	if (max_corr_ptbin4 <= acs.at(i).num_corr_ptbin4) max_corr_ptbin4 = acs.at(i).num_corr_ptbin4; if (min_corr_ptbin4 >= acs.at(i).num_corr_ptbin4) min_corr_ptbin4 = acs.at(i).num_corr_ptbin4;
	if (max_corr_ptbin5 <= acs.at(i).num_corr_ptbin5) max_corr_ptbin5 = acs.at(i).num_corr_ptbin5; if (min_corr_ptbin5 >= acs.at(i).num_corr_ptbin5) min_corr_ptbin5 = acs.at(i).num_corr_ptbin5;
	if (max_corr_etabin1 <= acs.at(i).num_corr_etabin1) max_corr_etabin1 = acs.at(i).num_corr_etabin1; if (min_corr_etabin1 >= acs.at(i).num_corr_etabin1) min_corr_etabin1 = acs.at(i).num_corr_etabin1;
	if (max_corr_etabin2 <= acs.at(i).num_corr_etabin2) max_corr_etabin2 = acs.at(i).num_corr_etabin2; if (min_corr_etabin2 >= acs.at(i).num_corr_etabin2) min_corr_etabin2 = acs.at(i).num_corr_etabin2;
	if (max_corr_etabin3 <= acs.at(i).num_corr_etabin3) max_corr_etabin3 = acs.at(i).num_corr_etabin3; if (min_corr_etabin3 >= acs.at(i).num_corr_etabin3) min_corr_etabin3 = acs.at(i).num_corr_etabin3;
	if (max_corr_etabin4 <= acs.at(i).num_corr_etabin4) max_corr_etabin4 = acs.at(i).num_corr_etabin4; if (min_corr_etabin4 >= acs.at(i).num_corr_etabin4) min_corr_etabin4 = acs.at(i).num_corr_etabin4;
	if (max_corr_etabin5 <= acs.at(i).num_corr_etabin5) max_corr_etabin5 = acs.at(i).num_corr_etabin5; if (min_corr_etabin5 >= acs.at(i).num_corr_etabin5) min_corr_etabin5 = acs.at(i).num_corr_etabin5;
	if (max_acccorr <= acs.at(i).num_acccorr) max_acccorr = acs.at(i).num_acccorr; if (min_acccorr >= acs.at(i).num_acccorr) min_acccorr = acs.at(i).num_acccorr;
	if (max_acccorr_ptbin1 <= acs.at(i).num_acccorr_ptbin1) max_acccorr_ptbin1 = acs.at(i).num_acccorr_ptbin1; if (min_acccorr_ptbin1 >= acs.at(i).num_acccorr_ptbin1) min_acccorr_ptbin1 = acs.at(i).num_acccorr_ptbin1;
	if (max_acccorr_ptbin2 <= acs.at(i).num_acccorr_ptbin2) max_acccorr_ptbin2 = acs.at(i).num_acccorr_ptbin2; if (min_acccorr_ptbin2 >= acs.at(i).num_acccorr_ptbin2) min_acccorr_ptbin2 = acs.at(i).num_acccorr_ptbin2;
	if (max_acccorr_ptbin3 <= acs.at(i).num_acccorr_ptbin3) max_acccorr_ptbin3 = acs.at(i).num_acccorr_ptbin3; if (min_acccorr_ptbin3 >= acs.at(i).num_acccorr_ptbin3) min_acccorr_ptbin3 = acs.at(i).num_acccorr_ptbin3;
	if (max_acccorr_ptbin4 <= acs.at(i).num_acccorr_ptbin4) max_acccorr_ptbin4 = acs.at(i).num_acccorr_ptbin4; if (min_acccorr_ptbin4 >= acs.at(i).num_acccorr_ptbin4) min_acccorr_ptbin4 = acs.at(i).num_acccorr_ptbin4;
	if (max_acccorr_ptbin5 <= acs.at(i).num_acccorr_ptbin5) max_acccorr_ptbin5 = acs.at(i).num_acccorr_ptbin5; if (min_acccorr_ptbin5 >= acs.at(i).num_acccorr_ptbin5) min_acccorr_ptbin5 = acs.at(i).num_acccorr_ptbin5;
	if (max_acccorr_etabin1 <= acs.at(i).num_acccorr_etabin1) max_acccorr_etabin1 = acs.at(i).num_acccorr_etabin1; if (min_acccorr_etabin1 >= acs.at(i).num_acccorr_etabin1) min_acccorr_etabin1 = acs.at(i).num_acccorr_etabin1;
	if (max_acccorr_etabin2 <= acs.at(i).num_acccorr_etabin2) max_acccorr_etabin2 = acs.at(i).num_acccorr_etabin2; if (min_acccorr_etabin2 >= acs.at(i).num_acccorr_etabin2) min_acccorr_etabin2 = acs.at(i).num_acccorr_etabin2;
	if (max_acccorr_etabin3 <= acs.at(i).num_acccorr_etabin3) max_acccorr_etabin3 = acs.at(i).num_acccorr_etabin3; if (min_acccorr_etabin3 >= acs.at(i).num_acccorr_etabin3) min_acccorr_etabin3 = acs.at(i).num_acccorr_etabin3;
	if (max_acccorr_etabin4 <= acs.at(i).num_acccorr_etabin4) max_acccorr_etabin4 = acs.at(i).num_acccorr_etabin4; if (min_acccorr_etabin4 >= acs.at(i).num_acccorr_etabin4) min_acccorr_etabin4 = acs.at(i).num_acccorr_etabin4;
	if (max_acccorr_etabin5 <= acs.at(i).num_acccorr_etabin5) max_acccorr_etabin5 = acs.at(i).num_acccorr_etabin5; if (min_acccorr_etabin5 >= acs.at(i).num_acccorr_etabin5) min_acccorr_etabin5 = acs.at(i).num_acccorr_etabin5;
    }
    c.num_acc = (max_acc - min_acc) / 2;
    c.num_acc_ptbin1 = (max_acc_ptbin1 - min_acc_ptbin1) / 2;
    c.num_acc_ptbin2 = (max_acc_ptbin2 - min_acc_ptbin2) / 2;
    c.num_acc_ptbin3 = (max_acc_ptbin3 - min_acc_ptbin3) / 2;
    c.num_acc_ptbin4 = (max_acc_ptbin4 - min_acc_ptbin4) / 2;
    c.num_acc_ptbin5 = (max_acc_ptbin5 - min_acc_ptbin5) / 2;
    c.num_acc_etabin1 = (max_acc_etabin1 - min_acc_etabin1) / 2;
    c.num_acc_etabin2 = (max_acc_etabin2 - min_acc_etabin2) / 2;
    c.num_acc_etabin3 = (max_acc_etabin3 - min_acc_etabin3) / 2;
    c.num_acc_etabin4 = (max_acc_etabin4 - min_acc_etabin4) / 2;
    c.num_acc_etabin5 = (max_acc_etabin5 - min_acc_etabin5) / 2;
    c.num_acc2 = (max_acc2 - min_acc2) / 2;
    c.num_acc2_ptbin1 = (max_acc2_ptbin1 - min_acc2_ptbin1) / 2;
    c.num_acc2_ptbin2 = (max_acc2_ptbin2 - min_acc2_ptbin2) / 2;
    c.num_acc2_ptbin3 = (max_acc2_ptbin3 - min_acc2_ptbin3) / 2;
    c.num_acc2_ptbin4 = (max_acc2_ptbin4 - min_acc2_ptbin4) / 2;
    c.num_acc2_ptbin5 = (max_acc2_ptbin5 - min_acc2_ptbin5) / 2;
    c.num_acc2_etabin1 = (max_acc2_etabin1 - min_acc2_etabin1) / 2;
    c.num_acc2_etabin2 = (max_acc2_etabin2 - min_acc2_etabin2) / 2;
    c.num_acc2_etabin3 = (max_acc2_etabin3 - min_acc2_etabin3) / 2;
    c.num_acc2_etabin4 = (max_acc2_etabin4 - min_acc2_etabin4) / 2;
    c.num_acc2_etabin5 = (max_acc2_etabin5 - min_acc2_etabin5) / 2;
    c.num_acc3 = (max_acc3 - min_acc3) / 2;
    c.num_acc3_ptbin1 = (max_acc3_ptbin1 - min_acc3_ptbin1) / 2;
    c.num_acc3_ptbin2 = (max_acc3_ptbin2 - min_acc3_ptbin2) / 2;
    c.num_acc3_ptbin3 = (max_acc3_ptbin3 - min_acc3_ptbin3) / 2;
    c.num_acc3_ptbin4 = (max_acc3_ptbin4 - min_acc3_ptbin4) / 2;
    c.num_acc3_ptbin5 = (max_acc3_ptbin5 - min_acc3_ptbin5) / 2;
    c.num_acc3_etabin1 = (max_acc3_etabin1 - min_acc3_etabin1) / 2;
    c.num_acc3_etabin2 = (max_acc3_etabin2 - min_acc3_etabin2) / 2;
    c.num_acc3_etabin3 = (max_acc3_etabin3 - min_acc3_etabin3) / 2;
    c.num_acc3_etabin4 = (max_acc3_etabin4 - min_acc3_etabin4) / 2;
    c.num_acc3_etabin5 = (max_acc3_etabin5 - min_acc3_etabin5) / 2;
    c.num_corr = (max_corr - min_corr) / 2;
    c.num_corr_ptbin1 = (max_corr_ptbin1 - min_corr_ptbin1) / 2;
    c.num_corr_ptbin2 = (max_corr_ptbin2 - min_corr_ptbin2) / 2;
    c.num_corr_ptbin3 = (max_corr_ptbin3 - min_corr_ptbin3) / 2;
    c.num_corr_ptbin4 = (max_corr_ptbin4 - min_corr_ptbin4) / 2;
    c.num_corr_ptbin5 = (max_corr_ptbin5 - min_corr_ptbin5) / 2;
    c.num_corr_etabin1 = (max_corr_etabin1 - min_corr_etabin1) / 2;
    c.num_corr_etabin2 = (max_corr_etabin2 - min_corr_etabin2) / 2;
    c.num_corr_etabin3 = (max_corr_etabin3 - min_corr_etabin3) / 2;
    c.num_corr_etabin4 = (max_corr_etabin4 - min_corr_etabin4) / 2;
    c.num_corr_etabin5 = (max_corr_etabin5 - min_corr_etabin5) / 2;
    c.num_acccorr = (max_acccorr - min_acccorr) / 2;
    c.num_acccorr_ptbin1 = (max_acccorr_ptbin1 - min_acccorr_ptbin1) / 2;
    c.num_acccorr_ptbin2 = (max_acccorr_ptbin2 - min_acccorr_ptbin2) / 2;
    c.num_acccorr_ptbin3 = (max_acccorr_ptbin3 - min_acccorr_ptbin3) / 2;
    c.num_acccorr_ptbin4 = (max_acccorr_ptbin4 - min_acccorr_ptbin4) / 2;
    c.num_acccorr_ptbin5 = (max_acccorr_ptbin5 - min_acccorr_ptbin5) / 2;
    c.num_acccorr_etabin1 = (max_acccorr_etabin1 - min_acccorr_etabin1) / 2;
    c.num_acccorr_etabin2 = (max_acccorr_etabin2 - min_acccorr_etabin2) / 2;
    c.num_acccorr_etabin3 = (max_acccorr_etabin3 - min_acccorr_etabin3) / 2;
    c.num_acccorr_etabin4 = (max_acccorr_etabin4 - min_acccorr_etabin4) / 2;
    c.num_acccorr_etabin5 = (max_acccorr_etabin5 - min_acccorr_etabin5) / 2;

    return c;
}

AccRatio GetAccRatioEnvelope(vector<AccRatio> &acs) {
    AccRatio c = EmptyAccRatio();
    double max_accratio = -999999;       double min_accratio = 999999;
    double max_accratio_ptbin1 = -999999;   double min_accratio_ptbin1 = 999999;
    double max_accratio_ptbin2 = -999999;   double min_accratio_ptbin2 = 999999;
    double max_accratio_ptbin3 = -999999;   double min_accratio_ptbin3 = 999999;
    double max_accratio_ptbin4 = -999999;   double min_accratio_ptbin4 = 999999;
    double max_accratio_ptbin5 = -999999;   double min_accratio_ptbin5 = 999999;
    double max_accratio_etabin1 = -999999;   double min_accratio_etabin1 = 999999;
    double max_accratio_etabin2 = -999999;   double min_accratio_etabin2 = 999999;
    double max_accratio_etabin3 = -999999;   double min_accratio_etabin3 = 999999;
    double max_accratio_etabin4 = -999999;   double min_accratio_etabin4 = 999999;
    double max_accratio_etabin5 = -999999;   double min_accratio_etabin5 = 999999;
    for (int i = 0; i < acs.size(); i++) {
	if (max_accratio <= acs.at(i).num_accratio) max_accratio = acs.at(i).num_accratio; if (min_accratio >= acs.at(i).num_accratio) min_accratio = acs.at(i).num_accratio;
	if (max_accratio_ptbin1 <= acs.at(i).num_accratio_ptbin1) max_accratio_ptbin1 = acs.at(i).num_accratio_ptbin1; if (min_accratio_ptbin1 >= acs.at(i).num_accratio_ptbin1) min_accratio_ptbin1 = acs.at(i).num_accratio_ptbin1;
	if (max_accratio_ptbin2 <= acs.at(i).num_accratio_ptbin2) max_accratio_ptbin2 = acs.at(i).num_accratio_ptbin2; if (min_accratio_ptbin2 >= acs.at(i).num_accratio_ptbin2) min_accratio_ptbin2 = acs.at(i).num_accratio_ptbin2;
	if (max_accratio_ptbin3 <= acs.at(i).num_accratio_ptbin3) max_accratio_ptbin3 = acs.at(i).num_accratio_ptbin3; if (min_accratio_ptbin3 >= acs.at(i).num_accratio_ptbin3) min_accratio_ptbin3 = acs.at(i).num_accratio_ptbin3;
	if (max_accratio_ptbin4 <= acs.at(i).num_accratio_ptbin4) max_accratio_ptbin4 = acs.at(i).num_accratio_ptbin4; if (min_accratio_ptbin4 >= acs.at(i).num_accratio_ptbin4) min_accratio_ptbin4 = acs.at(i).num_accratio_ptbin4;
	if (max_accratio_ptbin5 <= acs.at(i).num_accratio_ptbin5) max_accratio_ptbin5 = acs.at(i).num_accratio_ptbin5; if (min_accratio_ptbin5 >= acs.at(i).num_accratio_ptbin5) min_accratio_ptbin5 = acs.at(i).num_accratio_ptbin5;
	if (max_accratio_etabin1 <= acs.at(i).num_accratio_etabin1) max_accratio_etabin1 = acs.at(i).num_accratio_etabin1; if (min_accratio_etabin1 >= acs.at(i).num_accratio_etabin1) min_accratio_etabin1 = acs.at(i).num_accratio_etabin1;
	if (max_accratio_etabin2 <= acs.at(i).num_accratio_etabin2) max_accratio_etabin2 = acs.at(i).num_accratio_etabin2; if (min_accratio_etabin2 >= acs.at(i).num_accratio_etabin2) min_accratio_etabin2 = acs.at(i).num_accratio_etabin2;
	if (max_accratio_etabin3 <= acs.at(i).num_accratio_etabin3) max_accratio_etabin3 = acs.at(i).num_accratio_etabin3; if (min_accratio_etabin3 >= acs.at(i).num_accratio_etabin3) min_accratio_etabin3 = acs.at(i).num_accratio_etabin3;
	if (max_accratio_etabin4 <= acs.at(i).num_accratio_etabin4) max_accratio_etabin4 = acs.at(i).num_accratio_etabin4; if (min_accratio_etabin4 >= acs.at(i).num_accratio_etabin4) min_accratio_etabin4 = acs.at(i).num_accratio_etabin4;
	if (max_accratio_etabin5 <= acs.at(i).num_accratio_etabin5) max_accratio_etabin5 = acs.at(i).num_accratio_etabin5; if (min_accratio_etabin5 >= acs.at(i).num_accratio_etabin5) min_accratio_etabin5 = acs.at(i).num_accratio_etabin5;
    }
    c.num_accratio = (max_accratio - min_accratio) / 2;
    c.num_accratio_ptbin1 = (max_accratio_ptbin1 - min_accratio_ptbin1) / 2;
    c.num_accratio_ptbin2 = (max_accratio_ptbin2 - min_accratio_ptbin2) / 2;
    c.num_accratio_ptbin3 = (max_accratio_ptbin3 - min_accratio_ptbin3) / 2;
    c.num_accratio_ptbin4 = (max_accratio_ptbin4 - min_accratio_ptbin4) / 2;
    c.num_accratio_ptbin5 = (max_accratio_ptbin5 - min_accratio_ptbin5) / 2;
    c.num_accratio_etabin1 = (max_accratio_etabin1 - min_accratio_etabin1) / 2;
    c.num_accratio_etabin2 = (max_accratio_etabin2 - min_accratio_etabin2) / 2;
    c.num_accratio_etabin3 = (max_accratio_etabin3 - min_accratio_etabin3) / 2;
    c.num_accratio_etabin4 = (max_accratio_etabin4 - min_accratio_etabin4) / 2;
    c.num_accratio_etabin5 = (max_accratio_etabin5 - min_accratio_etabin5) / 2;

    return c;
}

AccCorr CalcuAccCorr(Counter &a_reco, Counter &a_truth) {
    AccCorr b;
    b.num_acc = a_truth.num_fidu / a_truth.num_total; b.err_acc = GetErrorAoverAB(a_truth.num_fidu, a_truth.err_fidu, a_truth.num_total, a_truth.err_total);
    b.num_acc_ptbin1 = a_truth.num_fidu_ptbin1 / a_truth.num_total_ptbin1; b.err_acc_ptbin1 = GetErrorAoverAB(a_truth.num_fidu_ptbin1, a_truth.err_fidu_ptbin1, a_truth.num_total_ptbin1, a_truth.err_total_ptbin1);
    b.num_acc_ptbin2 = a_truth.num_fidu_ptbin2 / a_truth.num_total_ptbin2; b.err_acc_ptbin2 = GetErrorAoverAB(a_truth.num_fidu_ptbin2, a_truth.err_fidu_ptbin2, a_truth.num_total_ptbin2, a_truth.err_total_ptbin2);
    b.num_acc_ptbin3 = a_truth.num_fidu_ptbin3 / a_truth.num_total_ptbin3; b.err_acc_ptbin3 = GetErrorAoverAB(a_truth.num_fidu_ptbin3, a_truth.err_fidu_ptbin3, a_truth.num_total_ptbin3, a_truth.err_total_ptbin3);
    b.num_acc_ptbin4 = a_truth.num_fidu_ptbin4 / a_truth.num_total_ptbin4; b.err_acc_ptbin4 = GetErrorAoverAB(a_truth.num_fidu_ptbin4, a_truth.err_fidu_ptbin4, a_truth.num_total_ptbin4, a_truth.err_total_ptbin4);
    b.num_acc_ptbin5 = a_truth.num_fidu_ptbin5 / a_truth.num_total_ptbin5; b.err_acc_ptbin5 = GetErrorAoverAB(a_truth.num_fidu_ptbin5, a_truth.err_fidu_ptbin5, a_truth.num_total_ptbin5, a_truth.err_total_ptbin5);
    b.num_acc_etabin1 = a_truth.num_fidu_etabin1 / a_truth.num_total_etabin1; b.err_acc_etabin1 = GetErrorAoverAB(a_truth.num_fidu_etabin1, a_truth.err_fidu_etabin1, a_truth.num_total_etabin1, a_truth.err_total_etabin1);
    b.num_acc_etabin2 = a_truth.num_fidu_etabin2 / a_truth.num_total_etabin2; b.err_acc_etabin2 = GetErrorAoverAB(a_truth.num_fidu_etabin2, a_truth.err_fidu_etabin2, a_truth.num_total_etabin2, a_truth.err_total_etabin2);
    b.num_acc_etabin3 = a_truth.num_fidu_etabin3 / a_truth.num_total_etabin3; b.err_acc_etabin3 = GetErrorAoverAB(a_truth.num_fidu_etabin3, a_truth.err_fidu_etabin3, a_truth.num_total_etabin3, a_truth.err_total_etabin3);
    b.num_acc_etabin4 = a_truth.num_fidu_etabin4 / a_truth.num_total_etabin4; b.err_acc_etabin4 = GetErrorAoverAB(a_truth.num_fidu_etabin4, a_truth.err_fidu_etabin4, a_truth.num_total_etabin4, a_truth.err_total_etabin4);
    b.num_acc_etabin5 = a_truth.num_fidu_etabin5 / a_truth.num_total_etabin5; b.err_acc_etabin5 = GetErrorAoverAB(a_truth.num_fidu_etabin5, a_truth.err_fidu_etabin5, a_truth.num_total_etabin5, a_truth.err_total_etabin5);
    b.num_acc2 = a_truth.num_fidu / a_truth.num_all; b.err_acc2 = GetErrorAoverAB(a_truth.num_fidu, a_truth.err_fidu, a_truth.num_all, a_truth.err_all);
    b.num_acc2_ptbin1 = a_truth.num_fidu_ptbin1 / a_truth.num_all; b.err_acc2_ptbin1 = GetErrorAoverAB(a_truth.num_fidu_ptbin1, a_truth.err_fidu_ptbin1, a_truth.num_all, a_truth.err_all);
    b.num_acc2_ptbin2 = a_truth.num_fidu_ptbin2 / a_truth.num_all; b.err_acc2_ptbin2 = GetErrorAoverAB(a_truth.num_fidu_ptbin2, a_truth.err_fidu_ptbin2, a_truth.num_all, a_truth.err_all);
    b.num_acc2_ptbin3 = a_truth.num_fidu_ptbin3 / a_truth.num_all; b.err_acc2_ptbin3 = GetErrorAoverAB(a_truth.num_fidu_ptbin3, a_truth.err_fidu_ptbin3, a_truth.num_all, a_truth.err_all);
    b.num_acc2_ptbin4 = a_truth.num_fidu_ptbin4 / a_truth.num_all; b.err_acc2_ptbin4 = GetErrorAoverAB(a_truth.num_fidu_ptbin4, a_truth.err_fidu_ptbin4, a_truth.num_all, a_truth.err_all);
    b.num_acc2_ptbin5 = a_truth.num_fidu_ptbin5 / a_truth.num_all; b.err_acc2_ptbin5 = GetErrorAoverAB(a_truth.num_fidu_ptbin5, a_truth.err_fidu_ptbin5, a_truth.num_all, a_truth.err_all);
    b.num_acc2_etabin1 = a_truth.num_fidu_etabin1 / a_truth.num_all; b.err_acc2_etabin1 = GetErrorAoverAB(a_truth.num_fidu_etabin1, a_truth.err_fidu_etabin1, a_truth.num_all, a_truth.err_all);
    b.num_acc2_etabin2 = a_truth.num_fidu_etabin2 / a_truth.num_all; b.err_acc2_etabin2 = GetErrorAoverAB(a_truth.num_fidu_etabin2, a_truth.err_fidu_etabin2, a_truth.num_all, a_truth.err_all);
    b.num_acc2_etabin3 = a_truth.num_fidu_etabin3 / a_truth.num_all; b.err_acc2_etabin3 = GetErrorAoverAB(a_truth.num_fidu_etabin3, a_truth.err_fidu_etabin3, a_truth.num_all, a_truth.err_all);
    b.num_acc2_etabin4 = a_truth.num_fidu_etabin4 / a_truth.num_all; b.err_acc2_etabin4 = GetErrorAoverAB(a_truth.num_fidu_etabin4, a_truth.err_fidu_etabin4, a_truth.num_all, a_truth.err_all);
    b.num_acc2_etabin5 = a_truth.num_fidu_etabin5 / a_truth.num_all; b.err_acc2_etabin5 = GetErrorAoverAB(a_truth.num_fidu_etabin5, a_truth.err_fidu_etabin5, a_truth.num_all, a_truth.err_all);
    b.num_acc3 = a_truth.num_total / a_truth.num_all; b.err_acc3 = GetErrorAoverAB(a_truth.num_total, a_truth.err_fidu, a_truth.num_all, a_truth.err_all);
    b.num_acc3_ptbin1 = a_truth.num_total_ptbin1 / a_truth.num_all; b.err_acc3_ptbin1 = GetErrorAoverAB(a_truth.num_total_ptbin1, a_truth.err_fidu_ptbin1, a_truth.num_all, a_truth.err_all);
    b.num_acc3_ptbin2 = a_truth.num_total_ptbin2 / a_truth.num_all; b.err_acc3_ptbin2 = GetErrorAoverAB(a_truth.num_total_ptbin2, a_truth.err_fidu_ptbin2, a_truth.num_all, a_truth.err_all);
    b.num_acc3_ptbin3 = a_truth.num_total_ptbin3 / a_truth.num_all; b.err_acc3_ptbin3 = GetErrorAoverAB(a_truth.num_total_ptbin3, a_truth.err_fidu_ptbin3, a_truth.num_all, a_truth.err_all);
    b.num_acc3_ptbin4 = a_truth.num_total_ptbin4 / a_truth.num_all; b.err_acc3_ptbin4 = GetErrorAoverAB(a_truth.num_total_ptbin4, a_truth.err_fidu_ptbin4, a_truth.num_all, a_truth.err_all);
    b.num_acc3_ptbin5 = a_truth.num_total_ptbin5 / a_truth.num_all; b.err_acc3_ptbin5 = GetErrorAoverAB(a_truth.num_total_ptbin5, a_truth.err_fidu_ptbin5, a_truth.num_all, a_truth.err_all);
    b.num_acc3_etabin1 = a_truth.num_total_etabin1 / a_truth.num_all; b.err_acc3_etabin1 = GetErrorAoverAB(a_truth.num_total_etabin1, a_truth.err_fidu_etabin1, a_truth.num_all, a_truth.err_all);
    b.num_acc3_etabin2 = a_truth.num_total_etabin2 / a_truth.num_all; b.err_acc3_etabin2 = GetErrorAoverAB(a_truth.num_total_etabin2, a_truth.err_fidu_etabin2, a_truth.num_all, a_truth.err_all);
    b.num_acc3_etabin3 = a_truth.num_total_etabin3 / a_truth.num_all; b.err_acc3_etabin3 = GetErrorAoverAB(a_truth.num_total_etabin3, a_truth.err_fidu_etabin3, a_truth.num_all, a_truth.err_all);
    b.num_acc3_etabin4 = a_truth.num_total_etabin4 / a_truth.num_all; b.err_acc3_etabin4 = GetErrorAoverAB(a_truth.num_total_etabin4, a_truth.err_fidu_etabin4, a_truth.num_all, a_truth.err_all);
    b.num_acc3_etabin5 = a_truth.num_total_etabin5 / a_truth.num_all; b.err_acc3_etabin5 = GetErrorAoverAB(a_truth.num_total_etabin5, a_truth.err_fidu_etabin5, a_truth.num_all, a_truth.err_all);
    b.num_corr = a_reco.num_fidu / a_truth.num_fidu; b.err_corr = GetErrorAoverAB(a_reco.num_fidu, a_reco.err_fidu, a_truth.num_fidu, a_truth.err_fidu);
    b.num_corr_ptbin1 = a_reco.num_fidu_ptbin1 / a_truth.num_fidu_ptbin1; b.err_corr_ptbin1 = GetErrorAoverAB(a_reco.num_fidu_ptbin1, a_reco.err_fidu_ptbin1, a_truth.num_fidu_ptbin1, a_truth.err_fidu_ptbin1);
    b.num_corr_ptbin2 = a_reco.num_fidu_ptbin2 / a_truth.num_fidu_ptbin2; b.err_corr_ptbin2 = GetErrorAoverAB(a_reco.num_fidu_ptbin2, a_reco.err_fidu_ptbin2, a_truth.num_fidu_ptbin2, a_truth.err_fidu_ptbin2);
    b.num_corr_ptbin3 = a_reco.num_fidu_ptbin3 / a_truth.num_fidu_ptbin3; b.err_corr_ptbin3 = GetErrorAoverAB(a_reco.num_fidu_ptbin3, a_reco.err_fidu_ptbin3, a_truth.num_fidu_ptbin3, a_truth.err_fidu_ptbin3);
    b.num_corr_ptbin4 = a_reco.num_fidu_ptbin4 / a_truth.num_fidu_ptbin4; b.err_corr_ptbin4 = GetErrorAoverAB(a_reco.num_fidu_ptbin4, a_reco.err_fidu_ptbin4, a_truth.num_fidu_ptbin4, a_truth.err_fidu_ptbin4);
    b.num_corr_ptbin5 = a_reco.num_fidu_ptbin5 / a_truth.num_fidu_ptbin5; b.err_corr_ptbin5 = GetErrorAoverAB(a_reco.num_fidu_ptbin5, a_reco.err_fidu_ptbin5, a_truth.num_fidu_ptbin5, a_truth.err_fidu_ptbin5);
    b.num_corr_etabin1 = a_reco.num_fidu_etabin1 / a_truth.num_fidu_etabin1; b.err_corr_etabin1 = GetErrorAoverAB(a_reco.num_fidu_etabin1, a_reco.err_fidu_etabin1, a_truth.num_fidu_etabin1, a_truth.err_fidu_etabin1);
    b.num_corr_etabin2 = a_reco.num_fidu_etabin2 / a_truth.num_fidu_etabin2; b.err_corr_etabin2 = GetErrorAoverAB(a_reco.num_fidu_etabin2, a_reco.err_fidu_etabin2, a_truth.num_fidu_etabin2, a_truth.err_fidu_etabin2);
    b.num_corr_etabin3 = a_reco.num_fidu_etabin3 / a_truth.num_fidu_etabin3; b.err_corr_etabin3 = GetErrorAoverAB(a_reco.num_fidu_etabin3, a_reco.err_fidu_etabin3, a_truth.num_fidu_etabin3, a_truth.err_fidu_etabin3);
    b.num_corr_etabin4 = a_reco.num_fidu_etabin4 / a_truth.num_fidu_etabin4; b.err_corr_etabin4 = GetErrorAoverAB(a_reco.num_fidu_etabin4, a_reco.err_fidu_etabin4, a_truth.num_fidu_etabin4, a_truth.err_fidu_etabin4);
    b.num_corr_etabin5 = a_reco.num_fidu_etabin5 / a_truth.num_fidu_etabin5; b.err_corr_etabin5 = GetErrorAoverAB(a_reco.num_fidu_etabin5, a_reco.err_fidu_etabin5, a_truth.num_fidu_etabin5, a_truth.err_fidu_etabin5);
    b.num_acccorr = a_reco.num_fidu / a_truth.num_total; b.err_acccorr = GetErrorAoverAB(a_reco.num_fidu, a_reco.err_fidu, a_truth.num_total, a_truth.err_total);
    b.num_acccorr_ptbin1 = a_reco.num_fidu_ptbin1 / a_truth.num_total_ptbin1; b.err_acccorr_ptbin1 = GetErrorAoverAB(a_reco.num_fidu_ptbin1, a_reco.err_fidu_ptbin1, a_truth.num_total_ptbin1, a_truth.err_total_ptbin1);
    b.num_acccorr_ptbin2 = a_reco.num_fidu_ptbin2 / a_truth.num_total_ptbin2; b.err_acccorr_ptbin2 = GetErrorAoverAB(a_reco.num_fidu_ptbin2, a_reco.err_fidu_ptbin2, a_truth.num_total_ptbin2, a_truth.err_total_ptbin2);
    b.num_acccorr_ptbin3 = a_reco.num_fidu_ptbin3 / a_truth.num_total_ptbin3; b.err_acccorr_ptbin3 = GetErrorAoverAB(a_reco.num_fidu_ptbin3, a_reco.err_fidu_ptbin3, a_truth.num_total_ptbin3, a_truth.err_total_ptbin3);
    b.num_acccorr_ptbin4 = a_reco.num_fidu_ptbin4 / a_truth.num_total_ptbin4; b.err_acccorr_ptbin4 = GetErrorAoverAB(a_reco.num_fidu_ptbin4, a_reco.err_fidu_ptbin4, a_truth.num_total_ptbin4, a_truth.err_total_ptbin4);
    b.num_acccorr_ptbin5 = a_reco.num_fidu_ptbin5 / a_truth.num_total_ptbin5; b.err_acccorr_ptbin5 = GetErrorAoverAB(a_reco.num_fidu_ptbin5, a_reco.err_fidu_ptbin5, a_truth.num_total_ptbin5, a_truth.err_total_ptbin5);
    b.num_acccorr_etabin1 = a_reco.num_fidu_etabin1 / a_truth.num_total_etabin1; b.err_acccorr_etabin1 = GetErrorAoverAB(a_reco.num_fidu_etabin1, a_reco.err_fidu_etabin1, a_truth.num_total_etabin1, a_truth.err_total_etabin1);
    b.num_acccorr_etabin2 = a_reco.num_fidu_etabin2 / a_truth.num_total_etabin2; b.err_acccorr_etabin2 = GetErrorAoverAB(a_reco.num_fidu_etabin2, a_reco.err_fidu_etabin2, a_truth.num_total_etabin2, a_truth.err_total_etabin2);
    b.num_acccorr_etabin3 = a_reco.num_fidu_etabin3 / a_truth.num_total_etabin3; b.err_acccorr_etabin3 = GetErrorAoverAB(a_reco.num_fidu_etabin3, a_reco.err_fidu_etabin3, a_truth.num_total_etabin3, a_truth.err_total_etabin3);
    b.num_acccorr_etabin4 = a_reco.num_fidu_etabin4 / a_truth.num_total_etabin4; b.err_acccorr_etabin4 = GetErrorAoverAB(a_reco.num_fidu_etabin4, a_reco.err_fidu_etabin4, a_truth.num_total_etabin4, a_truth.err_total_etabin4);
    b.num_acccorr_etabin5 = a_reco.num_fidu_etabin5 / a_truth.num_total_etabin5; b.err_acccorr_etabin5 = GetErrorAoverAB(a_reco.num_fidu_etabin5, a_reco.err_fidu_etabin5, a_truth.num_total_etabin5, a_truth.err_total_etabin5);
    return b;
}

void PrintAccCorr(string title, AccCorr &ac, int type, double scale, bool err, bool banner) {
    if (banner) {
	if (type == 1 || type == 2 || type == 3) {
	cout << setw(40) << "Region" << " &"
	     << setw(30) << "Inclusive" << " &"
	     << setw(30) << "15 $\\leq p_T <$ 25" << " &"
	     << setw(30) << "25 $\\leq p_T <$ 40" << " &"
	     << setw(30) << "40 $\\leq p_T <$ 60" << " &"
	     << setw(30) << "60 $\\leq p_T <$ 100" << " &"
	     << setw(30) << "100 $\\leq p_T <$ 300" << " \\\\"
	     << endl;
	} else {
	cout << setw(40) << "Region" << " &"
	     << setw(30) << "Inclusive" << " &"
	     << setw(30) << "0 $\\leq |\\eta| <$ 0.25" << " &"
	     << setw(30) << "0.25 $\\leq |\\eta| <$ 0.55" << " &"
	     << setw(30) << "0.55 $\\leq |\\eta| <$ 0.90" << " &"
	     << setw(30) << "0.90 $\\leq |\\eta| <$ 1.37" << " &"
	     << setw(30) << "1.37 $\\leq |\\eta| <$ 2.37" << " \\\\"
	     << endl;
	}
    }
    if (type == 1) {
	cout << setw(20) << "Acc. " << setw(20) << title << " &";
	if (err) {
    	    cout << setw(10) << scale*ac.num_acc << setw(10)        << " $\\pm$ " << setw(10) << scale*ac.err_acc << " &";
    	    cout << setw(10) << scale*ac.num_acc_ptbin1 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_acc_ptbin1 << " &";
    	    cout << setw(10) << scale*ac.num_acc_ptbin2 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_acc_ptbin2 << " &";
    	    cout << setw(10) << scale*ac.num_acc_ptbin3 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_acc_ptbin3 << " &";
    	    cout << setw(10) << scale*ac.num_acc_ptbin4 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_acc_ptbin4 << " &";
    	    cout << setw(10) << scale*ac.num_acc_ptbin5 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_acc_ptbin5 << " \\\\";
    	} else {
    	    cout << setw(30) << scale*ac.num_acc   << " &";
    	    cout << setw(30) << scale*ac.num_acc_ptbin1 << " &";
    	    cout << setw(30) << scale*ac.num_acc_ptbin2 << " &";
    	    cout << setw(30) << scale*ac.num_acc_ptbin3 << " &";
    	    cout << setw(30) << scale*ac.num_acc_ptbin4 << " &";
    	    cout << setw(30) << scale*ac.num_acc_ptbin5 << " \\\\";
    	}
    	cout << endl;
    } else if (type == 2) {
	cout << setw(20) << "Corr. " << setw(20) << title << " &";
    	if (err) {
    	    cout << setw(10) << scale*ac.num_corr << setw(10)        << " $\\pm$ " << setw(10) << scale*ac.err_corr << " &";
    	    cout << setw(10) << scale*ac.num_corr_ptbin1 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_corr_ptbin1 << " &";
    	    cout << setw(10) << scale*ac.num_corr_ptbin2 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_corr_ptbin2 << " &";
    	    cout << setw(10) << scale*ac.num_corr_ptbin3 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_corr_ptbin3 << " &";
    	    cout << setw(10) << scale*ac.num_corr_ptbin4 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_corr_ptbin4 << " &";
    	    cout << setw(10) << scale*ac.num_corr_ptbin5 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_corr_ptbin5 << " \\\\";
    	} else {
    	    cout << setw(30) << scale*ac.num_corr   << " &";
    	    cout << setw(30) << scale*ac.num_corr_ptbin1 << " &";
    	    cout << setw(30) << scale*ac.num_corr_ptbin2 << " &";
    	    cout << setw(30) << scale*ac.num_corr_ptbin3 << " &";
    	    cout << setw(30) << scale*ac.num_corr_ptbin4 << " &";
    	    cout << setw(30) << scale*ac.num_corr_ptbin5 << " \\\\";
    	}
    	cout << endl;
    } else if (type == 3) {
	cout << setw(20) << "Acc. $\\times$ Corr. " << setw(20) << title << " &";
    	if (err) {
    	    cout << setw(10) << scale*ac.num_acccorr << setw(10)        << " $\\pm$ " << setw(10) << scale*ac.err_acccorr << " &";
    	    cout << setw(10) << scale*ac.num_acccorr_ptbin1 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_acccorr_ptbin1 << " &";
    	    cout << setw(10) << scale*ac.num_acccorr_ptbin2 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_acccorr_ptbin2 << " &";
    	    cout << setw(10) << scale*ac.num_acccorr_ptbin3 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_acccorr_ptbin3 << " &";
    	    cout << setw(10) << scale*ac.num_acccorr_ptbin4 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_acccorr_ptbin4 << " &";
    	    cout << setw(10) << scale*ac.num_acccorr_ptbin5 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_acccorr_ptbin5 << " \\\\";
    	} else {
    	    cout << setw(30) << scale*ac.num_acccorr   << " &";
    	    cout << setw(30) << scale*ac.num_acccorr_ptbin1 << " &";
    	    cout << setw(30) << scale*ac.num_acccorr_ptbin2 << " &";
    	    cout << setw(30) << scale*ac.num_acccorr_ptbin3 << " &";
    	    cout << setw(30) << scale*ac.num_acccorr_ptbin4 << " &";
    	    cout << setw(30) << scale*ac.num_acccorr_ptbin5 << " \\\\";
    	}
    	cout << endl;
    } else if (type == 4) {
	cout << setw(20) << "Acc. " << setw(20) << title << " &";
	if (err) {
    	    cout << setw(10) << scale*ac.num_acc << setw(10)        << " $\\pm$ " << setw(10) << scale*ac.err_acc << " &";
    	    cout << setw(10) << scale*ac.num_acc_etabin1 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_acc_etabin1 << " &";
    	    cout << setw(10) << scale*ac.num_acc_etabin2 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_acc_etabin2 << " &";
    	    cout << setw(10) << scale*ac.num_acc_etabin3 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_acc_etabin3 << " &";
    	    cout << setw(10) << scale*ac.num_acc_etabin4 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_acc_etabin4 << " &";
    	    cout << setw(10) << scale*ac.num_acc_etabin5 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_acc_etabin5 << " \\\\";
    	} else {
    	    cout << setw(30) << scale*ac.num_acc   << " &";
    	    cout << setw(30) << scale*ac.num_acc_etabin1 << " &";
    	    cout << setw(30) << scale*ac.num_acc_etabin2 << " &";
    	    cout << setw(30) << scale*ac.num_acc_etabin3 << " &";
    	    cout << setw(30) << scale*ac.num_acc_etabin4 << " &";
    	    cout << setw(30) << scale*ac.num_acc_etabin5 << " \\\\";
    	}
    	cout << endl;
    } else if (type == 5) {
	cout << setw(20) << "Corr. " << setw(20) << title << " &";
    	if (err) {
    	    cout << setw(10) << scale*ac.num_corr << setw(10)        << " $\\pm$ " << setw(10) << scale*ac.err_corr << " &";
    	    cout << setw(10) << scale*ac.num_corr_etabin1 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_corr_etabin1 << " &";
    	    cout << setw(10) << scale*ac.num_corr_etabin2 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_corr_etabin2 << " &";
    	    cout << setw(10) << scale*ac.num_corr_etabin3 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_corr_etabin3 << " &";
    	    cout << setw(10) << scale*ac.num_corr_etabin4 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_corr_etabin4 << " &";
    	    cout << setw(10) << scale*ac.num_corr_etabin5 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_corr_etabin5 << " \\\\";
    	} else {
    	    cout << setw(30) << scale*ac.num_corr   << " &";
    	    cout << setw(30) << scale*ac.num_corr_etabin1 << " &";
    	    cout << setw(30) << scale*ac.num_corr_etabin2 << " &";
    	    cout << setw(30) << scale*ac.num_corr_etabin3 << " &";
    	    cout << setw(30) << scale*ac.num_corr_etabin4 << " &";
    	    cout << setw(30) << scale*ac.num_corr_etabin5 << " \\\\";
    	}
    	cout << endl;
    } else if (type == 6) {
	cout << setw(20) << "Acc. $\\times$ Corr. " << setw(20) << title << " &";
    	if (err) {
    	    cout << setw(10) << scale*ac.num_acccorr << setw(10)        << " $\\pm$ " << setw(10) << scale*ac.err_acccorr << " &";
    	    cout << setw(10) << scale*ac.num_acccorr_etabin1 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_acccorr_etabin1 << " &";
    	    cout << setw(10) << scale*ac.num_acccorr_etabin2 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_acccorr_etabin2 << " &";
    	    cout << setw(10) << scale*ac.num_acccorr_etabin3 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_acccorr_etabin3 << " &";
    	    cout << setw(10) << scale*ac.num_acccorr_etabin4 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_acccorr_etabin4 << " &";
    	    cout << setw(10) << scale*ac.num_acccorr_etabin5 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_acccorr_etabin5 << " \\\\";
    	} else {
    	    cout << setw(30) << scale*ac.num_acccorr   << " &";
    	    cout << setw(30) << scale*ac.num_acccorr_etabin1 << " &";
    	    cout << setw(30) << scale*ac.num_acccorr_etabin2 << " &";
    	    cout << setw(30) << scale*ac.num_acccorr_etabin3 << " &";
    	    cout << setw(30) << scale*ac.num_acccorr_etabin4 << " &";
    	    cout << setw(30) << scale*ac.num_acccorr_etabin5 << " \\\\";
    	}
    	cout << endl;
    }
}

void PrintAccRatio(string title, AccRatio &ac, int type, double scale, bool err, bool banner) {
    if (banner) {
	if (type == 1) {
	cout << setw(40) << "Region" << " &"
	     << setw(30) << "Inclusive" << " &"
	     << setw(30) << "15 $\\leq p_T <$ 25" << " &"
	     << setw(30) << "25 $\\leq p_T <$ 40" << " &"
	     << setw(30) << "40 $\\leq p_T <$ 60" << " &"
	     << setw(30) << "60 $\\leq p_T <$ 100" << " &"
	     << setw(30) << "100 $\\leq p_T <$ 300" << " \\\\"
	     << endl;
	} else {
	cout << setw(40) << "Region" << " &"
	     << setw(30) << "Inclusive" << " &"
	     << setw(30) << "0 $\\leq |\\eta| <$ 0.25" << " &"
	     << setw(30) << "0.25 $\\leq |\\eta| <$ 0.55" << " &"
	     << setw(30) << "0.55 $\\leq |\\eta| <$ 0.90" << " &"
	     << setw(30) << "0.90 $\\leq |\\eta| <$ 1.37" << " &"
	     << setw(30) << "1.37 $\\leq |\\eta| <$ 2.37" << " \\\\"
	     << endl;
	}
    }
    if (type == 1) {
	cout << setw(20) << "Acc. Ratio. " << setw(20) << title << " &";
	if (err) {
    	    cout << setw(10) << scale*ac.num_accratio << setw(10)        << " $\\pm$ " << setw(10) << scale*ac.err_accratio << " &";
    	    cout << setw(10) << scale*ac.num_accratio_ptbin1 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_accratio_ptbin1 << " &";
    	    cout << setw(10) << scale*ac.num_accratio_ptbin2 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_accratio_ptbin2 << " &";
    	    cout << setw(10) << scale*ac.num_accratio_ptbin3 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_accratio_ptbin3 << " &";
    	    cout << setw(10) << scale*ac.num_accratio_ptbin4 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_accratio_ptbin4 << " &";
    	    cout << setw(10) << scale*ac.num_accratio_ptbin5 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_accratio_ptbin5 << " \\\\";
    	} else {
    	    cout << setw(30) << scale*ac.num_accratio   << " &";
    	    cout << setw(30) << scale*ac.num_accratio_ptbin1 << " &";
    	    cout << setw(30) << scale*ac.num_accratio_ptbin2 << " &";
    	    cout << setw(30) << scale*ac.num_accratio_ptbin3 << " &";
    	    cout << setw(30) << scale*ac.num_accratio_ptbin4 << " &";
    	    cout << setw(30) << scale*ac.num_accratio_ptbin5 << " \\\\";
    	}
    	cout << endl;
    } else {
	cout << setw(20) << "Acc. Ratio. " << setw(20) << title << " &";
	if (err) {
    	    cout << setw(10) << scale*ac.num_accratio << setw(10)        << " $\\pm$ " << setw(10) << scale*ac.err_accratio << " &";
    	    cout << setw(10) << scale*ac.num_accratio_etabin1 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_accratio_etabin1 << " &";
    	    cout << setw(10) << scale*ac.num_accratio_etabin2 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_accratio_etabin2 << " &";
    	    cout << setw(10) << scale*ac.num_accratio_etabin3 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_accratio_etabin3 << " &";
    	    cout << setw(10) << scale*ac.num_accratio_etabin4 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_accratio_etabin4 << " &";
    	    cout << setw(10) << scale*ac.num_accratio_etabin5 << setw(10) << " $\\pm$ " << setw(10) << scale*ac.err_accratio_etabin5 << " \\\\";
    	} else {
    	    cout << setw(30) << scale*ac.num_accratio   << " &";
    	    cout << setw(30) << scale*ac.num_accratio_etabin1 << " &";
    	    cout << setw(30) << scale*ac.num_accratio_etabin2 << " &";
    	    cout << setw(30) << scale*ac.num_accratio_etabin3 << " &";
    	    cout << setw(30) << scale*ac.num_accratio_etabin4 << " &";
    	    cout << setw(30) << scale*ac.num_accratio_etabin5 << " \\\\";
    	}
    	cout << endl;
    }
}

void PrintCounter(string title, Counter &a, int type, double scale, bool err, bool banner) {
    if (banner) {
	if (type == 1 || type == 2) {
	cout << setw(40) << "Region" << " &"
	     << setw(30) << "Inclusive" << " &"
	     << setw(30) << "15 $\\leq p_T <$ 25" << " &"
	     << setw(30) << "25 $\\leq p_T <$ 40" << " &"
	     << setw(30) << "40 $\\leq p_T <$ 60" << " &"
	     << setw(30) << "60 $\\leq p_T <$ 100" << " &"
	     << setw(30) << "100 $\\leq p_T <$ 300" << " \\\\"
	     << endl;
	} else if (type == 3 || type == 4) {
	cout << setw(40) << "Region" << " &"
	     << setw(30) << "Inclusive" << " &"
	     << setw(30) << "0 $\\leq |\\eta| <$ 0.25" << " &"
	     << setw(30) << "0.25 $\\leq |\\eta| <$ 0.55" << " &"
	     << setw(30) << "0.55 $\\leq |\\eta| <$ 0.90" << " &"
	     << setw(30) << "0.90 $\\leq |\\eta| <$ 1.37" << " &"
	     << setw(30) << "1.37 $\\leq |\\eta| <$ 2.37" << " \\\\"
	     << endl;
	} else {
	cout << setw(40) << "Bin" << " &"
	     << setw(30) << "0 $\\leq p_T^{iso} <$ 1" << " &"
	     << setw(30) << "1 $\\leq p_T^{iso} <$ 3" << " &"
	     << setw(30) << "3 $\\leq p_T^{iso} <$ 5" << " &"
	     << setw(30) << "5 $\\leq p_T^{iso} <$ 10" << " &"
	     << setw(30) << "10 $\\leq p_T^{iso}$" << " \\\\"
	     << endl;
	}
    }
    if (type == 1) {
	cout << setw(20) << "Total " << setw(20) << title << " &";
	if (err) {
	    cout << setw(10) << scale*a.num_total << setw(10)        << " $\\pm$ " << setw(10) << scale*a.err_total << " &";
	    cout << setw(10) << scale*a.num_total_ptbin1 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_total_ptbin1 << " &";
	    cout << setw(10) << scale*a.num_total_ptbin2 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_total_ptbin2 << " &";
	    cout << setw(10) << scale*a.num_total_ptbin3 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_total_ptbin3 << " &";
	    cout << setw(10) << scale*a.num_total_ptbin4 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_total_ptbin4 << " &";
	    cout << setw(10) << scale*a.num_total_ptbin5 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_total_ptbin5 << " \\\\";
	} else {
	    cout << setw(30) << scale*a.num_total   << " &";
	    cout << setw(30) << scale*a.num_total_ptbin1 << " &";
	    cout << setw(30) << scale*a.num_total_ptbin2 << " &";
	    cout << setw(30) << scale*a.num_total_ptbin3 << " &";
	    cout << setw(30) << scale*a.num_total_ptbin4 << " &";
	    cout << setw(30) << scale*a.num_total_ptbin5 << " \\\\";
	}
	cout << endl;
    } else if (type == 2) {
	cout << setw(20) << "Fiducial" << setw(20) << title << " &";
	if (err) {
	    cout << setw(10) << scale*a.num_fidu << setw(10)        << " $\\pm$ " << setw(10) << scale*a.err_fidu << " &";
	    cout << setw(10) << scale*a.num_fidu_ptbin1 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_fidu_ptbin1 << " &";
	    cout << setw(10) << scale*a.num_fidu_ptbin2 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_fidu_ptbin2 << " &";
	    cout << setw(10) << scale*a.num_fidu_ptbin3 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_fidu_ptbin3 << " &";
	    cout << setw(10) << scale*a.num_fidu_ptbin4 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_fidu_ptbin4 << " &";
	    cout << setw(10) << scale*a.num_fidu_ptbin5 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_fidu_ptbin5 << " \\\\";
	} else {
	    cout << setw(30) << scale*a.num_fidu   << " &";
	    cout << setw(30) << scale*a.num_fidu_ptbin1 << " &";
	    cout << setw(30) << scale*a.num_fidu_ptbin2 << " &";
	    cout << setw(30) << scale*a.num_fidu_ptbin3 << " &";
	    cout << setw(30) << scale*a.num_fidu_ptbin4 << " &";
	    cout << setw(30) << scale*a.num_fidu_ptbin5 << " \\\\";
	}
	cout << endl;
    } else if (type == 3) {
	cout << setw(20) << "Total " << setw(20) << title << " &";
	if (err) {
	    cout << setw(10) << scale*a.num_total << setw(10)        << " $\\pm$ " << setw(10) << scale*a.err_total << " &";
	    cout << setw(10) << scale*a.num_total_etabin1 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_total_etabin1 << " &";
	    cout << setw(10) << scale*a.num_total_etabin2 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_total_etabin2 << " &";
	    cout << setw(10) << scale*a.num_total_etabin3 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_total_etabin3 << " &";
	    cout << setw(10) << scale*a.num_total_etabin4 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_total_etabin4 << " &";
	    cout << setw(10) << scale*a.num_total_etabin5 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_total_etabin5 << " \\\\";
	} else {
	    cout << setw(30) << scale*a.num_total   << " &";
	    cout << setw(30) << scale*a.num_total_etabin1 << " &";
	    cout << setw(30) << scale*a.num_total_etabin2 << " &";
	    cout << setw(30) << scale*a.num_total_etabin3 << " &";
	    cout << setw(30) << scale*a.num_total_etabin4 << " &";
	    cout << setw(30) << scale*a.num_total_etabin5 << " \\\\";
	}
	cout << endl;
    } else if (type == 4) {
	cout << setw(20) << "Fiducial" << setw(20) << title << " &";
	if (err) {
	    cout << setw(10) << scale*a.num_fidu << setw(10)        << " $\\pm$ " << setw(10) << scale*a.err_fidu << " &";
	    cout << setw(10) << scale*a.num_fidu_etabin1 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_fidu_etabin1 << " &";
	    cout << setw(10) << scale*a.num_fidu_etabin2 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_fidu_etabin2 << " &";
	    cout << setw(10) << scale*a.num_fidu_etabin3 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_fidu_etabin3 << " &";
	    cout << setw(10) << scale*a.num_fidu_etabin4 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_fidu_etabin4 << " &";
	    cout << setw(10) << scale*a.num_fidu_etabin5 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_fidu_etabin5 << " \\\\";
	} else {
	    cout << setw(30) << scale*a.num_fidu   << " &";
	    cout << setw(30) << scale*a.num_fidu_etabin1 << " &";
	    cout << setw(30) << scale*a.num_fidu_etabin2 << " &";
	    cout << setw(30) << scale*a.num_fidu_etabin3 << " &";
	    cout << setw(30) << scale*a.num_fidu_etabin4 << " &";
	    cout << setw(30) << scale*a.num_fidu_etabin5 << " \\\\";
	}
	cout << endl;
    } else if (type == 5) {
	cout << setw(20) << "Template" << setw(20) << title << " &";
	cout << setw(30) << scale*a.num_isobin1 << " &";
	cout << setw(30) << scale*a.num_isobin2 << " &";
	cout << setw(30) << scale*a.num_isobin3 << " &";
	cout << setw(30) << scale*a.num_isobin4 << " &";
	cout << setw(30) << scale*a.num_isobin5 << " \\\\";
	cout << endl;
    }
}

void PrintCounterError(string title, Counter &a, int type, double scale, bool banner) {
    if (banner) {
	if (type == 1 || type == 2) {
	    cout << setw(40) << "Region" << " &"
	         << setw(30) << "Inclusive" << " &"
	         << setw(30) << "15 $\\leq p_T <$ 25" << " &"
	         << setw(30) << "25 $\\leq p_T <$ 40" << " &"
	         << setw(30) << "40 $\\leq p_T <$ 60" << " &"
	         << setw(30) << "60 $\\leq p_T <$ 100" << " &"
	         << setw(30) << "100 $\\leq p_T <$ 300" << " \\\\"
	         << endl;
	} else if (type == 3 || type == 4) {
	cout << setw(40) << "Region" << " &"
	     << setw(30) << "Inclusive" << " &"
	     << setw(30) << "0 $\\leq |\\eta| <$ 0.25" << " &"
	     << setw(30) << "0.25 $\\leq |\\eta| <$ 0.55" << " &"
	     << setw(30) << "0.55 $\\leq |\\eta| <$ 0.90" << " &"
	     << setw(30) << "0.90 $\\leq |\\eta| <$ 1.37" << " &"
	     << setw(30) << "1.37 $\\leq |\\eta| <$ 2.37" << " \\\\"
	     << endl;
	} else {
	    cout << setw(40) << "Bin" << " &"
	         << setw(30) << "0 $\\leq p_T^{iso} <$ 1" << " &"
	         << setw(30) << "1 $\\leq p_T^{iso} <$ 3" << " &"
	         << setw(30) << "3 $\\leq p_T^{iso} <$ 5" << " &"
	         << setw(30) << "5 $\\leq p_T^{iso} <$ 10" << " &"
	         << setw(30) << "10 $\\leq p_T^{iso}$" << " \\\\"
	         << endl;
	}
    }
    if (type == 1) {
	cout << setw(20) << "Total " << setw(20) << title << " &";
	cout << setw(30) << scale * a.err_total        / a.num_total << setw(10)        << " &";
	cout << setw(30) << scale * a.err_total_ptbin1 / a.num_total_ptbin1 << setw(10) << " &";
	cout << setw(30) << scale * a.err_total_ptbin2 / a.num_total_ptbin2 << setw(10) << " &";
	cout << setw(30) << scale * a.err_total_ptbin3 / a.num_total_ptbin3 << setw(10) << " &";
	cout << setw(30) << scale * a.err_total_ptbin4 / a.num_total_ptbin4 << setw(10) << " &";
	cout << setw(30) << scale * a.err_total_ptbin5 / a.num_total_ptbin5 << setw(10) << " \\\\";
	cout << endl;
    } else if (type == 2) {
	cout << setw(20) << "Fiducial" << setw(20) << title << " &";
	cout << setw(30) << scale * a.err_fidu        / a.num_fidu        << " &";
	cout << setw(30) << scale * a.err_fidu_ptbin1 / a.num_fidu_ptbin1 << " &";
	cout << setw(30) << scale * a.err_fidu_ptbin2 / a.num_fidu_ptbin2 << " &";
	cout << setw(30) << scale * a.err_fidu_ptbin3 / a.num_fidu_ptbin3 << " &";
	cout << setw(30) << scale * a.err_fidu_ptbin4 / a.num_fidu_ptbin4 << " &";
	cout << setw(30) << scale * a.err_fidu_ptbin5 / a.num_fidu_ptbin5 << " \\\\";
	cout << endl;
    } else if (type == 3) {
	cout << setw(20) << "Total " << setw(20) << title << " &";
	cout << setw(30) << scale * a.err_total        / a.num_total << setw(10)        << " &";
	cout << setw(30) << scale * a.err_total_etabin1 / a.num_total_etabin1 << setw(10) << " &";
	cout << setw(30) << scale * a.err_total_etabin2 / a.num_total_etabin2 << setw(10) << " &";
	cout << setw(30) << scale * a.err_total_etabin3 / a.num_total_etabin3 << setw(10) << " &";
	cout << setw(30) << scale * a.err_total_etabin4 / a.num_total_etabin4 << setw(10) << " &";
	cout << setw(30) << scale * a.err_total_etabin5 / a.num_total_etabin5 << setw(10) << " \\\\";
	cout << endl;
    } else if (type == 4) {
	cout << setw(20) << "Fiducial" << setw(20) << title << " &";
	cout << setw(30) << scale * a.err_fidu        / a.num_fidu        << " &";
	cout << setw(30) << scale * a.err_fidu_etabin1 / a.num_fidu_etabin1 << " &";
	cout << setw(30) << scale * a.err_fidu_etabin2 / a.num_fidu_etabin2 << " &";
	cout << setw(30) << scale * a.err_fidu_etabin3 / a.num_fidu_etabin3 << " &";
	cout << setw(30) << scale * a.err_fidu_etabin4 / a.num_fidu_etabin4 << " &";
	cout << setw(30) << scale * a.err_fidu_etabin5 / a.num_fidu_etabin5 << " \\\\";
	cout << endl;
    } else if (type == 5) {
	cout << setw(20) << "Ptcone20" << setw(20) << title << " &";
	cout << setw(30) << scale * a.err_isobin1 / a.num_isobin1 << " &";
	cout << setw(30) << scale * a.err_isobin2 / a.num_isobin2 << " &";
	cout << setw(30) << scale * a.err_isobin3 / a.num_isobin3 << " &";
	cout << setw(30) << scale * a.err_isobin4 / a.num_isobin4 << " &";
	cout << setw(30) << scale * a.err_isobin5 / a.num_isobin5 << " \\\\";
	cout << endl;
    } else if (type == 6) {
	cout << setw(20) << "Ptcone20" << setw(20) << title << " &";
	cout << setw(30) << scale * a.err_ptbin1_isobin1 / a.num_ptbin1_isobin1 << " &";
	cout << setw(30) << scale * a.err_ptbin1_isobin2 / a.num_ptbin1_isobin2 << " &";
	cout << setw(30) << scale * a.err_ptbin1_isobin3 / a.num_ptbin1_isobin3 << " &";
	cout << setw(30) << scale * a.err_ptbin1_isobin4 / a.num_ptbin1_isobin4 << " &";
	cout << setw(30) << scale * a.err_ptbin1_isobin5 / a.num_ptbin1_isobin5 << " \\\\";
	cout << endl;
    } else if (type == 7) {
	cout << setw(20) << "Ptcone20" << setw(20) << title << " &";
	cout << setw(30) << scale * a.err_ptbin2_isobin1 / a.num_ptbin2_isobin1 << " &";
	cout << setw(30) << scale * a.err_ptbin2_isobin2 / a.num_ptbin2_isobin2 << " &";
	cout << setw(30) << scale * a.err_ptbin2_isobin3 / a.num_ptbin2_isobin3 << " &";
	cout << setw(30) << scale * a.err_ptbin2_isobin4 / a.num_ptbin2_isobin4 << " &";
	cout << setw(30) << scale * a.err_ptbin2_isobin5 / a.num_ptbin2_isobin5 << " \\\\";
	cout << endl;
    } else if (type == 8) {
	cout << setw(20) << "Ptcone20" << setw(20) << title << " &";
	cout << setw(30) << scale * a.err_ptbin3_isobin1 / a.num_ptbin3_isobin1 << " &";
	cout << setw(30) << scale * a.err_ptbin3_isobin2 / a.num_ptbin3_isobin2 << " &";
	cout << setw(30) << scale * a.err_ptbin3_isobin3 / a.num_ptbin3_isobin3 << " &";
	cout << setw(30) << scale * a.err_ptbin3_isobin4 / a.num_ptbin3_isobin4 << " &";
	cout << setw(30) << scale * a.err_ptbin3_isobin5 / a.num_ptbin3_isobin5 << " \\\\";
	cout << endl;
    } else if (type == 9) {
	cout << setw(20) << "Ptcone20" << setw(20) << title << " &";
	cout << setw(30) << scale * a.err_ptbin4_isobin1 / a.num_ptbin4_isobin1 << " &";
	cout << setw(30) << scale * a.err_ptbin4_isobin2 / a.num_ptbin4_isobin2 << " &";
	cout << setw(30) << scale * a.err_ptbin4_isobin3 / a.num_ptbin4_isobin3 << " &";
	cout << setw(30) << scale * a.err_ptbin4_isobin4 / a.num_ptbin4_isobin4 << " &";
	cout << setw(30) << scale * a.err_ptbin4_isobin5 / a.num_ptbin4_isobin5 << " \\\\";
	cout << endl;
    } else if (type == 10) {
	cout << setw(20) << "Ptcone20" << setw(20) << title << " &";
	cout << setw(30) << scale * a.err_ptbin5_isobin1 / a.num_ptbin5_isobin1 << " &";
	cout << setw(30) << scale * a.err_ptbin5_isobin2 / a.num_ptbin5_isobin2 << " &";
	cout << setw(30) << scale * a.err_ptbin5_isobin3 / a.num_ptbin5_isobin3 << " &";
	cout << setw(30) << scale * a.err_ptbin5_isobin4 / a.num_ptbin5_isobin4 << " &";
	cout << setw(30) << scale * a.err_ptbin5_isobin5 / a.num_ptbin5_isobin5 << " \\\\";
	cout << endl;
    } else if (type == 11) {
	cout << setw(20) << "Ptcone20" << setw(20) << title << " &";
	cout << setw(30) << scale * a.err_etabin1_isobin1 / a.num_etabin1_isobin1 << " &";
	cout << setw(30) << scale * a.err_etabin1_isobin2 / a.num_etabin1_isobin2 << " &";
	cout << setw(30) << scale * a.err_etabin1_isobin3 / a.num_etabin1_isobin3 << " &";
	cout << setw(30) << scale * a.err_etabin1_isobin4 / a.num_etabin1_isobin4 << " &";
	cout << setw(30) << scale * a.err_etabin1_isobin5 / a.num_etabin1_isobin5 << " \\\\";
	cout << endl;
    } else if (type == 12) {
	cout << setw(20) << "Ptcone20" << setw(20) << title << " &";
	cout << setw(30) << scale * a.err_etabin2_isobin1 / a.num_etabin2_isobin1 << " &";
	cout << setw(30) << scale * a.err_etabin2_isobin2 / a.num_etabin2_isobin2 << " &";
	cout << setw(30) << scale * a.err_etabin2_isobin3 / a.num_etabin2_isobin3 << " &";
	cout << setw(30) << scale * a.err_etabin2_isobin4 / a.num_etabin2_isobin4 << " &";
	cout << setw(30) << scale * a.err_etabin2_isobin5 / a.num_etabin2_isobin5 << " \\\\";
	cout << endl;
    } else if (type == 13) {
	cout << setw(20) << "Ptcone20" << setw(20) << title << " &";
	cout << setw(30) << scale * a.err_etabin3_isobin1 / a.num_etabin3_isobin1 << " &";
	cout << setw(30) << scale * a.err_etabin3_isobin2 / a.num_etabin3_isobin2 << " &";
	cout << setw(30) << scale * a.err_etabin3_isobin3 / a.num_etabin3_isobin3 << " &";
	cout << setw(30) << scale * a.err_etabin3_isobin4 / a.num_etabin3_isobin4 << " &";
	cout << setw(30) << scale * a.err_etabin3_isobin5 / a.num_etabin3_isobin5 << " \\\\";
	cout << endl;
    } else if (type == 14) {
	cout << setw(20) << "Ptcone20" << setw(20) << title << " &";
	cout << setw(30) << scale * a.err_etabin4_isobin1 / a.num_etabin4_isobin1 << " &";
	cout << setw(30) << scale * a.err_etabin4_isobin2 / a.num_etabin4_isobin2 << " &";
	cout << setw(30) << scale * a.err_etabin4_isobin3 / a.num_etabin4_isobin3 << " &";
	cout << setw(30) << scale * a.err_etabin4_isobin4 / a.num_etabin4_isobin4 << " &";
	cout << setw(30) << scale * a.err_etabin4_isobin5 / a.num_etabin4_isobin5 << " \\\\";
	cout << endl;
    } else if (type == 15) {
	cout << setw(20) << "Ptcone20" << setw(20) << title << " &";
	cout << setw(30) << scale * a.err_etabin5_isobin1 / a.num_etabin5_isobin1 << " &";
	cout << setw(30) << scale * a.err_etabin5_isobin2 / a.num_etabin5_isobin2 << " &";
	cout << setw(30) << scale * a.err_etabin5_isobin3 / a.num_etabin5_isobin3 << " &";
	cout << setw(30) << scale * a.err_etabin5_isobin4 / a.num_etabin5_isobin4 << " &";
	cout << setw(30) << scale * a.err_etabin5_isobin5 / a.num_etabin5_isobin5 << " \\\\";
	cout << endl;
    }
}

void PrintSys(string title, Counter &a, int type, double scale, bool err, bool banner) {
    RegulateSysName(title);
    title = replaceChar(title, '_', ' ');
    if (banner) {
	if (type == 1) {
	cout << setw(40) << "Region" << " &"
	     << setw(30) << "Inclusive" << " &"
	     << setw(30) << "15 $\\leq p_T <$ 25" << " &"
	     << setw(30) << "25 $\\leq p_T <$ 40" << " &"
	     << setw(30) << "40 $\\leq p_T <$ 60" << " &"
	     << setw(30) << "60 $\\leq p_T <$ 100" << " &"
	     << setw(30) << "100 $\\leq p_T <$ 300" << " \\\\"
	     << endl;
	} else if (type == 2) {
	cout << setw(40) << "Region" << " &"
	     << setw(30) << "Inclusive" << " &"
	     << setw(30) << "0 $\\leq |\\eta| <$ 0.25" << " &"
	     << setw(30) << "0.25 $\\leq |\\eta| <$ 0.55" << " &"
	     << setw(30) << "0.55 $\\leq |\\eta| <$ 0.90" << " &"
	     << setw(30) << "0.90 $\\leq |\\eta| <$ 1.37" << " &"
	     << setw(30) << "1.37 $\\leq |\\eta| <$ 2.37" << " \\\\"
	     << endl;
	} else {
	cout << setw(40) << "Bin" << " &"
	     << setw(30) << "0 $\\leq p_T^{iso} <$ 1" << " &"
	     << setw(30) << "1 $\\leq p_T^{iso} <$ 3" << " &"
	     << setw(30) << "3 $\\leq p_T^{iso} <$ 5" << " &"
	     << setw(30) << "5 $\\leq p_T^{iso} <$ 10" << " &"
	     << setw(30) << "10 $\\leq p_T^{iso}$" << " \\\\"
	     << endl;
	}
    }
    cout << setw(40) << title << " &";
    if (err) {
	if (type == 1) {
        cout << setw(10) << scale*a.num_fidu << setw(10)        << " $\\pm$ " << setw(10) << scale*a.err_fidu << " &";
        cout << setw(10) << scale*a.num_fidu_ptbin1 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_fidu_ptbin1 << " &";
        cout << setw(10) << scale*a.num_fidu_ptbin2 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_fidu_ptbin2 << " &";
        cout << setw(10) << scale*a.num_fidu_ptbin3 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_fidu_ptbin3 << " &";
        cout << setw(10) << scale*a.num_fidu_ptbin4 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_fidu_ptbin4 << " &";
        cout << setw(10) << scale*a.num_fidu_ptbin5 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_fidu_ptbin5 << " \\\\";
	} else if (type == 2) {
        cout << setw(10) << scale*a.num_fidu << setw(10)        << " $\\pm$ " << setw(10) << scale*a.err_fidu << " &";
        cout << setw(10) << scale*a.num_fidu_etabin1 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_fidu_etabin1 << " &";
        cout << setw(10) << scale*a.num_fidu_etabin2 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_fidu_etabin2 << " &";
        cout << setw(10) << scale*a.num_fidu_etabin3 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_fidu_etabin3 << " &";
        cout << setw(10) << scale*a.num_fidu_etabin4 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_fidu_etabin4 << " &";
        cout << setw(10) << scale*a.num_fidu_etabin5 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_fidu_etabin5 << " \\\\";
	} else if (type == 3) {
        cout << setw(10) << scale*a.num_isobin1 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_isobin1 << " &";
        cout << setw(10) << scale*a.num_isobin2 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_isobin2 << " &";
        cout << setw(10) << scale*a.num_isobin3 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_isobin3 << " &";
        cout << setw(10) << scale*a.num_isobin4 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_isobin4 << " &";
        cout << setw(10) << scale*a.num_isobin5 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_isobin5 << " \\\\";
	} else if (type == 4) {
        cout << setw(10) << scale*a.num_ptbin1_isobin1 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_ptbin1_isobin1 << " &";
        cout << setw(10) << scale*a.num_ptbin1_isobin2 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_ptbin1_isobin2 << " &";
        cout << setw(10) << scale*a.num_ptbin1_isobin3 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_ptbin1_isobin3 << " &";
        cout << setw(10) << scale*a.num_ptbin1_isobin4 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_ptbin1_isobin4 << " &";
        cout << setw(10) << scale*a.num_ptbin1_isobin5 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_ptbin1_isobin5 << " \\\\";
	} else if (type == 5) {
        cout << setw(10) << scale*a.num_ptbin2_isobin1 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_ptbin2_isobin1 << " &";
        cout << setw(10) << scale*a.num_ptbin2_isobin2 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_ptbin2_isobin2 << " &";
        cout << setw(10) << scale*a.num_ptbin2_isobin3 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_ptbin2_isobin3 << " &";
        cout << setw(10) << scale*a.num_ptbin2_isobin4 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_ptbin2_isobin4 << " &";
        cout << setw(10) << scale*a.num_ptbin2_isobin5 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_ptbin2_isobin5 << " \\\\";
	} else if (type == 6) {
        cout << setw(10) << scale*a.num_ptbin3_isobin1 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_ptbin3_isobin1 << " &";
        cout << setw(10) << scale*a.num_ptbin3_isobin2 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_ptbin3_isobin2 << " &";
        cout << setw(10) << scale*a.num_ptbin3_isobin3 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_ptbin3_isobin3 << " &";
        cout << setw(10) << scale*a.num_ptbin3_isobin4 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_ptbin3_isobin4 << " &";
        cout << setw(10) << scale*a.num_ptbin3_isobin5 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_ptbin3_isobin5 << " \\\\";
	} else if (type == 7) {
        cout << setw(10) << scale*a.num_ptbin4_isobin1 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_ptbin4_isobin1 << " &";
        cout << setw(10) << scale*a.num_ptbin4_isobin2 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_ptbin4_isobin2 << " &";
        cout << setw(10) << scale*a.num_ptbin4_isobin3 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_ptbin4_isobin3 << " &";
        cout << setw(10) << scale*a.num_ptbin4_isobin4 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_ptbin4_isobin4 << " &";
        cout << setw(10) << scale*a.num_ptbin4_isobin5 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_ptbin4_isobin5 << " \\\\";
	} else if (type == 8) {
        cout << setw(10) << scale*a.num_ptbin5_isobin1 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_ptbin5_isobin1 << " &";
        cout << setw(10) << scale*a.num_ptbin5_isobin2 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_ptbin5_isobin2 << " &";
        cout << setw(10) << scale*a.num_ptbin5_isobin3 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_ptbin5_isobin3 << " &";
        cout << setw(10) << scale*a.num_ptbin5_isobin4 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_ptbin5_isobin4 << " &";
        cout << setw(10) << scale*a.num_ptbin5_isobin5 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_ptbin5_isobin5 << " \\\\";
	} else if (type == 9) {
        cout << setw(10) << scale*a.num_etabin1_isobin1 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_etabin1_isobin1 << " &";
        cout << setw(10) << scale*a.num_etabin1_isobin2 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_etabin1_isobin2 << " &";
        cout << setw(10) << scale*a.num_etabin1_isobin3 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_etabin1_isobin3 << " &";
        cout << setw(10) << scale*a.num_etabin1_isobin4 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_etabin1_isobin4 << " &";
        cout << setw(10) << scale*a.num_etabin1_isobin5 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_etabin1_isobin5 << " \\\\";
	} else if (type == 10) {
        cout << setw(10) << scale*a.num_etabin2_isobin1 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_etabin2_isobin1 << " &";
        cout << setw(10) << scale*a.num_etabin2_isobin2 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_etabin2_isobin2 << " &";
        cout << setw(10) << scale*a.num_etabin2_isobin3 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_etabin2_isobin3 << " &";
        cout << setw(10) << scale*a.num_etabin2_isobin4 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_etabin2_isobin4 << " &";
        cout << setw(10) << scale*a.num_etabin2_isobin5 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_etabin2_isobin5 << " \\\\";
	} else if (type == 11) {
        cout << setw(10) << scale*a.num_etabin3_isobin1 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_etabin3_isobin1 << " &";
        cout << setw(10) << scale*a.num_etabin3_isobin2 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_etabin3_isobin2 << " &";
        cout << setw(10) << scale*a.num_etabin3_isobin3 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_etabin3_isobin3 << " &";
        cout << setw(10) << scale*a.num_etabin3_isobin4 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_etabin3_isobin4 << " &";
        cout << setw(10) << scale*a.num_etabin3_isobin5 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_etabin3_isobin5 << " \\\\";
	} else if (type == 12) {
        cout << setw(10) << scale*a.num_etabin4_isobin1 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_etabin4_isobin1 << " &";
        cout << setw(10) << scale*a.num_etabin4_isobin2 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_etabin4_isobin2 << " &";
        cout << setw(10) << scale*a.num_etabin4_isobin3 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_etabin4_isobin3 << " &";
        cout << setw(10) << scale*a.num_etabin4_isobin4 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_etabin4_isobin4 << " &";
        cout << setw(10) << scale*a.num_etabin4_isobin5 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_etabin4_isobin5 << " \\\\";
	} else if (type == 13) {
        cout << setw(10) << scale*a.num_etabin5_isobin1 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_etabin5_isobin1 << " &";
        cout << setw(10) << scale*a.num_etabin5_isobin2 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_etabin5_isobin2 << " &";
        cout << setw(10) << scale*a.num_etabin5_isobin3 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_etabin5_isobin3 << " &";
        cout << setw(10) << scale*a.num_etabin5_isobin4 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_etabin5_isobin4 << " &";
        cout << setw(10) << scale*a.num_etabin5_isobin5 << setw(10) << " $\\pm$ " << setw(10) << scale*a.err_etabin5_isobin5 << " \\\\";
	}
    } else {
	if (type == 1) {
        cout << setw(30) << scale*a.num_fidu   << " &";
        cout << setw(30) << scale*a.num_fidu_ptbin1 << " &";
        cout << setw(30) << scale*a.num_fidu_ptbin2 << " &";
        cout << setw(30) << scale*a.num_fidu_ptbin3 << " &";
        cout << setw(30) << scale*a.num_fidu_ptbin4 << " &";
        cout << setw(30) << scale*a.num_fidu_ptbin5 << " \\\\";
	} else if (type == 2) {
        cout << setw(30) << scale*a.num_fidu   << " &";
        cout << setw(30) << scale*a.num_fidu_etabin1 << " &";
        cout << setw(30) << scale*a.num_fidu_etabin2 << " &";
        cout << setw(30) << scale*a.num_fidu_etabin3 << " &";
        cout << setw(30) << scale*a.num_fidu_etabin4 << " &";
        cout << setw(30) << scale*a.num_fidu_etabin5 << " \\\\";
	} else if (type == 3) {
        cout << setw(30) << scale*a.num_isobin1 << " &";
        cout << setw(30) << scale*a.num_isobin2 << " &";
        cout << setw(30) << scale*a.num_isobin3 << " &";
        cout << setw(30) << scale*a.num_isobin4 << " &";
        cout << setw(30) << scale*a.num_isobin5 << " \\\\";
	} else if (type == 4) {
        cout << setw(30) << scale*a.num_ptbin1_isobin1 << " &";
        cout << setw(30) << scale*a.num_ptbin1_isobin2 << " &";
        cout << setw(30) << scale*a.num_ptbin1_isobin3 << " &";
        cout << setw(30) << scale*a.num_ptbin1_isobin4 << " &";
        cout << setw(30) << scale*a.num_ptbin1_isobin5 << " \\\\";
	} else if (type == 5) {
        cout << setw(30) << scale*a.num_ptbin2_isobin1 << " &";
        cout << setw(30) << scale*a.num_ptbin2_isobin2 << " &";
        cout << setw(30) << scale*a.num_ptbin2_isobin3 << " &";
        cout << setw(30) << scale*a.num_ptbin2_isobin4 << " &";
        cout << setw(30) << scale*a.num_ptbin2_isobin5 << " \\\\";
	} else if (type == 6) {
        cout << setw(30) << scale*a.num_ptbin3_isobin1 << " &";
        cout << setw(30) << scale*a.num_ptbin3_isobin2 << " &";
        cout << setw(30) << scale*a.num_ptbin3_isobin3 << " &";
        cout << setw(30) << scale*a.num_ptbin3_isobin4 << " &";
        cout << setw(30) << scale*a.num_ptbin3_isobin5 << " \\\\";
	} else if (type == 7) {
        cout << setw(30) << scale*a.num_ptbin4_isobin1 << " &";
        cout << setw(30) << scale*a.num_ptbin4_isobin2 << " &";
        cout << setw(30) << scale*a.num_ptbin4_isobin3 << " &";
        cout << setw(30) << scale*a.num_ptbin4_isobin4 << " &";
        cout << setw(30) << scale*a.num_ptbin4_isobin5 << " \\\\";
	} else if (type == 8) {
        cout << setw(30) << scale*a.num_ptbin5_isobin1 << " &";
        cout << setw(30) << scale*a.num_ptbin5_isobin2 << " &";
        cout << setw(30) << scale*a.num_ptbin5_isobin3 << " &";
        cout << setw(30) << scale*a.num_ptbin5_isobin4 << " &";
        cout << setw(30) << scale*a.num_ptbin5_isobin5 << " \\\\";
	} else if (type == 9) {
        cout << setw(30) << scale*a.num_etabin1_isobin1 << " &";
        cout << setw(30) << scale*a.num_etabin1_isobin2 << " &";
        cout << setw(30) << scale*a.num_etabin1_isobin3 << " &";
        cout << setw(30) << scale*a.num_etabin1_isobin4 << " &";
        cout << setw(30) << scale*a.num_etabin1_isobin5 << " \\\\";
	} else if (type == 10) {
        cout << setw(30) << scale*a.num_etabin2_isobin1 << " &";
        cout << setw(30) << scale*a.num_etabin2_isobin2 << " &";
        cout << setw(30) << scale*a.num_etabin2_isobin3 << " &";
        cout << setw(30) << scale*a.num_etabin2_isobin4 << " &";
        cout << setw(30) << scale*a.num_etabin2_isobin5 << " \\\\";
	} else if (type == 11) {
        cout << setw(30) << scale*a.num_etabin3_isobin1 << " &";
        cout << setw(30) << scale*a.num_etabin3_isobin2 << " &";
        cout << setw(30) << scale*a.num_etabin3_isobin3 << " &";
        cout << setw(30) << scale*a.num_etabin3_isobin4 << " &";
        cout << setw(30) << scale*a.num_etabin3_isobin5 << " \\\\";
	} else if (type == 12) {
        cout << setw(30) << scale*a.num_etabin4_isobin1 << " &";
        cout << setw(30) << scale*a.num_etabin4_isobin2 << " &";
        cout << setw(30) << scale*a.num_etabin4_isobin3 << " &";
        cout << setw(30) << scale*a.num_etabin4_isobin4 << " &";
        cout << setw(30) << scale*a.num_etabin4_isobin5 << " \\\\";
	} else if (type == 13) {
        cout << setw(30) << scale*a.num_etabin5_isobin1 << " &";
        cout << setw(30) << scale*a.num_etabin5_isobin2 << " &";
        cout << setw(30) << scale*a.num_etabin5_isobin3 << " &";
        cout << setw(30) << scale*a.num_etabin5_isobin4 << " &";
        cout << setw(30) << scale*a.num_etabin5_isobin5 << " \\\\";
	}
    }
    cout << endl;
}

void ScaleFigure(Figure &a, float scale, bool debug) {
    if (debug) {
	cout << a.h_ptcone << " " << a.h_leppt << " " << a.h_lepeta << " " << a.h_jetpt << " " << a.h_jeteta << " " << a.h_phpt << " " << a.h_pheta << " " << a.h_met << " " << a.h_mphl << endl;
	cout << a.h_ptcone->Integral() << endl;
	cout << a.h_leppt->Integral() << endl;
	cout << a.h_lepeta->Integral() << endl;
	cout << a.h_phpt->Integral() << endl;
	cout << a.h_pheta->Integral() << endl;
	cout << a.h_jetpt->Integral() << endl;
	cout << a.h_jeteta->Integral() << endl;
	cout << a.h_met->Integral() << endl;
	cout << a.h_mphl->Integral() << endl;
    }
    a.h_ptcone->Scale(scale);
    a.h_leppt->Scale(scale);
    a.h_lepeta->Scale(scale);
    a.h_phpt->Scale(scale);
    a.h_pheta->Scale(scale);
    a.h_jetpt->Scale(scale);
    a.h_jeteta->Scale(scale);
    a.h_met->Scale(scale);
    a.h_mphl->Scale(scale);
    a.h_njet->Scale(scale);
    a.h_nbjet->Scale(scale);
    a.h_drphl->Scale(scale);
    if (debug) {
	cout << "scaling done" << endl;
    }
}

void ScaleCounter(Counter &a, float scale) {
    a.num_all *= scale; a.err_all *= scale;
    a.num_total *= scale; a.err_total *= scale;
    a.num_total_ptbin1 *= scale; a.err_total_ptbin1 *= scale;
    a.num_total_ptbin2 *= scale; a.err_total_ptbin2 *= scale;
    a.num_total_ptbin3 *= scale; a.err_total_ptbin3 *= scale;
    a.num_total_ptbin4 *= scale; a.err_total_ptbin4 *= scale;
    a.num_total_ptbin5 *= scale; a.err_total_ptbin5 *= scale;
    a.num_total_etabin1 *= scale; a.err_total_etabin1 *= scale;
    a.num_total_etabin2 *= scale; a.err_total_etabin2 *= scale;
    a.num_total_etabin3 *= scale; a.err_total_etabin3 *= scale;
    a.num_total_etabin4 *= scale; a.err_total_etabin4 *= scale;
    a.num_total_etabin5 *= scale; a.err_total_etabin5 *= scale;
    a.num_fidu *= scale; a.err_fidu *= scale;
    a.num_fidu_ptbin1 *= scale; a.err_fidu_ptbin1 *= scale;
    a.num_fidu_ptbin2 *= scale; a.err_fidu_ptbin2 *= scale;
    a.num_fidu_ptbin3 *= scale; a.err_fidu_ptbin3 *= scale;
    a.num_fidu_ptbin4 *= scale; a.err_fidu_ptbin4 *= scale;
    a.num_fidu_ptbin5 *= scale; a.err_fidu_ptbin5 *= scale;
    a.num_fidu_etabin1 *= scale; a.err_fidu_etabin1 *= scale;
    a.num_fidu_etabin2 *= scale; a.err_fidu_etabin2 *= scale;
    a.num_fidu_etabin3 *= scale; a.err_fidu_etabin3 *= scale;
    a.num_fidu_etabin4 *= scale; a.err_fidu_etabin4 *= scale;
    a.num_fidu_etabin5 *= scale; a.err_fidu_etabin5 *= scale;
    a.num_isobin1 *= scale; a.err_isobin1 *= scale;
    a.num_isobin2 *= scale; a.err_isobin2 *= scale;
    a.num_isobin3 *= scale; a.err_isobin3 *= scale;
    a.num_isobin4 *= scale; a.err_isobin4 *= scale;
    a.num_isobin5 *= scale; a.err_isobin5 *= scale;
    a.num_ptbin1_isobin1 *= scale; a.err_ptbin1_isobin1 *= scale;
    a.num_ptbin1_isobin2 *= scale; a.err_ptbin1_isobin2 *= scale;
    a.num_ptbin1_isobin3 *= scale; a.err_ptbin1_isobin3 *= scale;
    a.num_ptbin1_isobin4 *= scale; a.err_ptbin1_isobin4 *= scale;
    a.num_ptbin1_isobin5 *= scale; a.err_ptbin1_isobin5 *= scale;
    a.num_ptbin2_isobin1 *= scale; a.err_ptbin2_isobin1 *= scale;
    a.num_ptbin2_isobin2 *= scale; a.err_ptbin2_isobin2 *= scale;
    a.num_ptbin2_isobin3 *= scale; a.err_ptbin2_isobin3 *= scale;
    a.num_ptbin2_isobin4 *= scale; a.err_ptbin2_isobin4 *= scale;
    a.num_ptbin2_isobin5 *= scale; a.err_ptbin2_isobin5 *= scale;
    a.num_ptbin3_isobin1 *= scale; a.err_ptbin3_isobin1 *= scale;
    a.num_ptbin3_isobin2 *= scale; a.err_ptbin3_isobin2 *= scale;
    a.num_ptbin3_isobin3 *= scale; a.err_ptbin3_isobin3 *= scale;
    a.num_ptbin3_isobin4 *= scale; a.err_ptbin3_isobin4 *= scale;
    a.num_ptbin3_isobin5 *= scale; a.err_ptbin3_isobin5 *= scale;
    a.num_ptbin4_isobin1 *= scale; a.err_ptbin4_isobin1 *= scale;
    a.num_ptbin4_isobin2 *= scale; a.err_ptbin4_isobin2 *= scale;
    a.num_ptbin4_isobin3 *= scale; a.err_ptbin4_isobin3 *= scale;
    a.num_ptbin4_isobin4 *= scale; a.err_ptbin4_isobin4 *= scale;
    a.num_ptbin4_isobin5 *= scale; a.err_ptbin4_isobin5 *= scale;
    a.num_ptbin5_isobin1 *= scale; a.err_ptbin5_isobin1 *= scale;
    a.num_ptbin5_isobin2 *= scale; a.err_ptbin5_isobin2 *= scale;
    a.num_ptbin5_isobin3 *= scale; a.err_ptbin5_isobin3 *= scale;
    a.num_ptbin5_isobin4 *= scale; a.err_ptbin5_isobin4 *= scale;
    a.num_ptbin5_isobin5 *= scale; a.err_ptbin5_isobin5 *= scale;
    a.num_etabin1_isobin1 *= scale; a.err_etabin1_isobin1 *= scale;
    a.num_etabin1_isobin2 *= scale; a.err_etabin1_isobin2 *= scale;
    a.num_etabin1_isobin3 *= scale; a.err_etabin1_isobin3 *= scale;
    a.num_etabin1_isobin4 *= scale; a.err_etabin1_isobin4 *= scale;
    a.num_etabin1_isobin5 *= scale; a.err_etabin1_isobin5 *= scale;
    a.num_etabin2_isobin1 *= scale; a.err_etabin2_isobin1 *= scale;
    a.num_etabin2_isobin2 *= scale; a.err_etabin2_isobin2 *= scale;
    a.num_etabin2_isobin3 *= scale; a.err_etabin2_isobin3 *= scale;
    a.num_etabin2_isobin4 *= scale; a.err_etabin2_isobin4 *= scale;
    a.num_etabin2_isobin5 *= scale; a.err_etabin2_isobin5 *= scale;
    a.num_etabin3_isobin1 *= scale; a.err_etabin3_isobin1 *= scale;
    a.num_etabin3_isobin2 *= scale; a.err_etabin3_isobin2 *= scale;
    a.num_etabin3_isobin3 *= scale; a.err_etabin3_isobin3 *= scale;
    a.num_etabin3_isobin4 *= scale; a.err_etabin3_isobin4 *= scale;
    a.num_etabin3_isobin5 *= scale; a.err_etabin3_isobin5 *= scale;
    a.num_etabin4_isobin1 *= scale; a.err_etabin4_isobin1 *= scale;
    a.num_etabin4_isobin2 *= scale; a.err_etabin4_isobin2 *= scale;
    a.num_etabin4_isobin3 *= scale; a.err_etabin4_isobin3 *= scale;
    a.num_etabin4_isobin4 *= scale; a.err_etabin4_isobin4 *= scale;
    a.num_etabin4_isobin5 *= scale; a.err_etabin4_isobin5 *= scale;
    a.num_etabin5_isobin1 *= scale; a.err_etabin5_isobin1 *= scale;
    a.num_etabin5_isobin2 *= scale; a.err_etabin5_isobin2 *= scale;
    a.num_etabin5_isobin3 *= scale; a.err_etabin5_isobin3 *= scale;
    a.num_etabin5_isobin4 *= scale; a.err_etabin5_isobin4 *= scale;
    a.num_etabin5_isobin5 *= scale; a.err_etabin5_isobin5 *= scale;
}

void ScaleCounter(Counter &a, float scale, float err_scale) {
    a.err_all = sqrt(pow(a.err_all*scale,2) + pow(err_scale*a.num_all,2)); a.num_all *= scale;
    a.err_total = sqrt(pow(a.err_total*scale,2) + pow(err_scale*a.num_total,2)); a.num_total *= scale;
    a.err_total_ptbin1 = sqrt(pow(a.err_total_ptbin1*scale,2) + pow(err_scale*a.num_total_ptbin1,2)); a.num_total_ptbin1 *= scale;
    a.err_total_ptbin2 = sqrt(pow(a.err_total_ptbin2*scale,2) + pow(err_scale*a.num_total_ptbin2,2)); a.num_total_ptbin2 *= scale;
    a.err_total_ptbin3 = sqrt(pow(a.err_total_ptbin3*scale,2) + pow(err_scale*a.num_total_ptbin3,2)); a.num_total_ptbin3 *= scale;
    a.err_total_ptbin4 = sqrt(pow(a.err_total_ptbin4*scale,2) + pow(err_scale*a.num_total_ptbin4,2)); a.num_total_ptbin4 *= scale;
    a.err_total_ptbin5 = sqrt(pow(a.err_total_ptbin5*scale,2) + pow(err_scale*a.num_total_ptbin5,2)); a.num_total_ptbin5 *= scale;
    a.err_total_etabin1 = sqrt(pow(a.err_total_etabin1*scale,2) + pow(err_scale*a.num_total_etabin1,2)); a.num_total_etabin1 *= scale;
    a.err_total_etabin2 = sqrt(pow(a.err_total_etabin2*scale,2) + pow(err_scale*a.num_total_etabin2,2)); a.num_total_etabin2 *= scale;
    a.err_total_etabin3 = sqrt(pow(a.err_total_etabin3*scale,2) + pow(err_scale*a.num_total_etabin3,2)); a.num_total_etabin3 *= scale;
    a.err_total_etabin4 = sqrt(pow(a.err_total_etabin4*scale,2) + pow(err_scale*a.num_total_etabin4,2)); a.num_total_etabin4 *= scale;
    a.err_total_etabin5 = sqrt(pow(a.err_total_etabin5*scale,2) + pow(err_scale*a.num_total_etabin5,2)); a.num_total_etabin5 *= scale;
    a.err_fidu = sqrt(pow(a.err_fidu*scale,2) + pow(err_scale*a.num_fidu,2)); a.num_fidu *= scale;
    a.err_fidu_ptbin1 = sqrt(pow(a.err_fidu_ptbin1*scale,2) + pow(err_scale*a.num_fidu_ptbin1,2)); a.num_fidu_ptbin1 *= scale;
    a.err_fidu_ptbin2 = sqrt(pow(a.err_fidu_ptbin2*scale,2) + pow(err_scale*a.num_fidu_ptbin2,2)); a.num_fidu_ptbin2 *= scale;
    a.err_fidu_ptbin3 = sqrt(pow(a.err_fidu_ptbin3*scale,2) + pow(err_scale*a.num_fidu_ptbin3,2)); a.num_fidu_ptbin3 *= scale;
    a.err_fidu_ptbin4 = sqrt(pow(a.err_fidu_ptbin4*scale,2) + pow(err_scale*a.num_fidu_ptbin4,2)); a.num_fidu_ptbin4 *= scale;
    a.err_fidu_ptbin5 = sqrt(pow(a.err_fidu_ptbin5*scale,2) + pow(err_scale*a.num_fidu_ptbin5,2)); a.num_fidu_ptbin5 *= scale;
    a.err_fidu_etabin1 = sqrt(pow(a.err_fidu_etabin1*scale,2) + pow(err_scale*a.num_fidu_etabin1,2)); a.num_fidu_etabin1 *= scale;
    a.err_fidu_etabin2 = sqrt(pow(a.err_fidu_etabin2*scale,2) + pow(err_scale*a.num_fidu_etabin2,2)); a.num_fidu_etabin2 *= scale;
    a.err_fidu_etabin3 = sqrt(pow(a.err_fidu_etabin3*scale,2) + pow(err_scale*a.num_fidu_etabin3,2)); a.num_fidu_etabin3 *= scale;
    a.err_fidu_etabin4 = sqrt(pow(a.err_fidu_etabin4*scale,2) + pow(err_scale*a.num_fidu_etabin4,2)); a.num_fidu_etabin4 *= scale;
    a.err_fidu_etabin5 = sqrt(pow(a.err_fidu_etabin5*scale,2) + pow(err_scale*a.num_fidu_etabin5,2)); a.num_fidu_etabin5 *= scale;
    a.err_isobin1 = sqrt(pow(a.err_isobin1*scale,2) + pow(err_scale*a.num_isobin1,2)); a.num_isobin1 *= scale;
    a.err_isobin2 = sqrt(pow(a.err_isobin2*scale,2) + pow(err_scale*a.num_isobin2,2)); a.num_isobin2 *= scale;
    a.err_isobin3 = sqrt(pow(a.err_isobin3*scale,2) + pow(err_scale*a.num_isobin3,2)); a.num_isobin3 *= scale;
    a.err_isobin4 = sqrt(pow(a.err_isobin4*scale,2) + pow(err_scale*a.num_isobin4,2)); a.num_isobin4 *= scale;
    a.err_isobin5 = sqrt(pow(a.err_isobin5*scale,2) + pow(err_scale*a.num_isobin5,2)); a.num_isobin5 *= scale;
    a.err_ptbin1_isobin1 = sqrt(pow(a.err_ptbin1_isobin1*scale,2) + pow(err_scale*a.num_ptbin1_isobin1,2)); a.num_ptbin1_isobin1 *= scale;
    a.err_ptbin1_isobin2 = sqrt(pow(a.err_ptbin1_isobin2*scale,2) + pow(err_scale*a.num_ptbin1_isobin2,2)); a.num_ptbin1_isobin2 *= scale;
    a.err_ptbin1_isobin3 = sqrt(pow(a.err_ptbin1_isobin3*scale,2) + pow(err_scale*a.num_ptbin1_isobin3,2)); a.num_ptbin1_isobin3 *= scale;
    a.err_ptbin1_isobin4 = sqrt(pow(a.err_ptbin1_isobin4*scale,2) + pow(err_scale*a.num_ptbin1_isobin4,2)); a.num_ptbin1_isobin4 *= scale;
    a.err_ptbin1_isobin5 = sqrt(pow(a.err_ptbin1_isobin5*scale,2) + pow(err_scale*a.num_ptbin1_isobin5,2)); a.num_ptbin1_isobin5 *= scale;
    a.err_ptbin2_isobin1 = sqrt(pow(a.err_ptbin2_isobin1*scale,2) + pow(err_scale*a.num_ptbin2_isobin1,2)); a.num_ptbin2_isobin1 *= scale;
    a.err_ptbin2_isobin2 = sqrt(pow(a.err_ptbin2_isobin2*scale,2) + pow(err_scale*a.num_ptbin2_isobin2,2)); a.num_ptbin2_isobin2 *= scale;
    a.err_ptbin2_isobin3 = sqrt(pow(a.err_ptbin2_isobin3*scale,2) + pow(err_scale*a.num_ptbin2_isobin3,2)); a.num_ptbin2_isobin3 *= scale;
    a.err_ptbin2_isobin4 = sqrt(pow(a.err_ptbin2_isobin4*scale,2) + pow(err_scale*a.num_ptbin2_isobin4,2)); a.num_ptbin2_isobin4 *= scale;
    a.err_ptbin2_isobin5 = sqrt(pow(a.err_ptbin2_isobin5*scale,2) + pow(err_scale*a.num_ptbin2_isobin5,2)); a.num_ptbin2_isobin5 *= scale;
    a.err_ptbin3_isobin1 = sqrt(pow(a.err_ptbin3_isobin1*scale,2) + pow(err_scale*a.num_ptbin3_isobin1,2)); a.num_ptbin3_isobin1 *= scale;
    a.err_ptbin3_isobin2 = sqrt(pow(a.err_ptbin3_isobin2*scale,2) + pow(err_scale*a.num_ptbin3_isobin2,2)); a.num_ptbin3_isobin2 *= scale;
    a.err_ptbin3_isobin3 = sqrt(pow(a.err_ptbin3_isobin3*scale,2) + pow(err_scale*a.num_ptbin3_isobin3,2)); a.num_ptbin3_isobin3 *= scale;
    a.err_ptbin3_isobin4 = sqrt(pow(a.err_ptbin3_isobin4*scale,2) + pow(err_scale*a.num_ptbin3_isobin4,2)); a.num_ptbin3_isobin4 *= scale;
    a.err_ptbin3_isobin5 = sqrt(pow(a.err_ptbin3_isobin5*scale,2) + pow(err_scale*a.num_ptbin3_isobin5,2)); a.num_ptbin3_isobin5 *= scale;
    a.err_ptbin4_isobin1 = sqrt(pow(a.err_ptbin4_isobin1*scale,2) + pow(err_scale*a.num_ptbin4_isobin1,2)); a.num_ptbin4_isobin1 *= scale;
    a.err_ptbin4_isobin2 = sqrt(pow(a.err_ptbin4_isobin2*scale,2) + pow(err_scale*a.num_ptbin4_isobin2,2)); a.num_ptbin4_isobin2 *= scale;
    a.err_ptbin4_isobin3 = sqrt(pow(a.err_ptbin4_isobin3*scale,2) + pow(err_scale*a.num_ptbin4_isobin3,2)); a.num_ptbin4_isobin3 *= scale;
    a.err_ptbin4_isobin4 = sqrt(pow(a.err_ptbin4_isobin4*scale,2) + pow(err_scale*a.num_ptbin4_isobin4,2)); a.num_ptbin4_isobin4 *= scale;
    a.err_ptbin4_isobin5 = sqrt(pow(a.err_ptbin4_isobin5*scale,2) + pow(err_scale*a.num_ptbin4_isobin5,2)); a.num_ptbin4_isobin5 *= scale;
    a.err_ptbin5_isobin1 = sqrt(pow(a.err_ptbin5_isobin1*scale,2) + pow(err_scale*a.num_ptbin5_isobin1,2)); a.num_ptbin5_isobin1 *= scale;
    a.err_ptbin5_isobin2 = sqrt(pow(a.err_ptbin5_isobin2*scale,2) + pow(err_scale*a.num_ptbin5_isobin2,2)); a.num_ptbin5_isobin2 *= scale;
    a.err_ptbin5_isobin3 = sqrt(pow(a.err_ptbin5_isobin3*scale,2) + pow(err_scale*a.num_ptbin5_isobin3,2)); a.num_ptbin5_isobin3 *= scale;
    a.err_ptbin5_isobin4 = sqrt(pow(a.err_ptbin5_isobin4*scale,2) + pow(err_scale*a.num_ptbin5_isobin4,2)); a.num_ptbin5_isobin4 *= scale;
    a.err_ptbin5_isobin5 = sqrt(pow(a.err_ptbin5_isobin5*scale,2) + pow(err_scale*a.num_ptbin5_isobin5,2)); a.num_ptbin5_isobin5 *= scale;
    a.err_etabin1_isobin1 = sqrt(pow(a.err_etabin1_isobin1*scale,2) + pow(err_scale*a.num_etabin1_isobin1,2)); a.num_etabin1_isobin1 *= scale;
    a.err_etabin1_isobin2 = sqrt(pow(a.err_etabin1_isobin2*scale,2) + pow(err_scale*a.num_etabin1_isobin2,2)); a.num_etabin1_isobin2 *= scale;
    a.err_etabin1_isobin3 = sqrt(pow(a.err_etabin1_isobin3*scale,2) + pow(err_scale*a.num_etabin1_isobin3,2)); a.num_etabin1_isobin3 *= scale;
    a.err_etabin1_isobin4 = sqrt(pow(a.err_etabin1_isobin4*scale,2) + pow(err_scale*a.num_etabin1_isobin4,2)); a.num_etabin1_isobin4 *= scale;
    a.err_etabin1_isobin5 = sqrt(pow(a.err_etabin1_isobin5*scale,2) + pow(err_scale*a.num_etabin1_isobin5,2)); a.num_etabin1_isobin5 *= scale;
    a.err_etabin2_isobin1 = sqrt(pow(a.err_etabin2_isobin1*scale,2) + pow(err_scale*a.num_etabin2_isobin1,2)); a.num_etabin2_isobin1 *= scale;
    a.err_etabin2_isobin2 = sqrt(pow(a.err_etabin2_isobin2*scale,2) + pow(err_scale*a.num_etabin2_isobin2,2)); a.num_etabin2_isobin2 *= scale;
    a.err_etabin2_isobin3 = sqrt(pow(a.err_etabin2_isobin3*scale,2) + pow(err_scale*a.num_etabin2_isobin3,2)); a.num_etabin2_isobin3 *= scale;
    a.err_etabin2_isobin4 = sqrt(pow(a.err_etabin2_isobin4*scale,2) + pow(err_scale*a.num_etabin2_isobin4,2)); a.num_etabin2_isobin4 *= scale;
    a.err_etabin2_isobin5 = sqrt(pow(a.err_etabin2_isobin5*scale,2) + pow(err_scale*a.num_etabin2_isobin5,2)); a.num_etabin2_isobin5 *= scale;
    a.err_etabin3_isobin1 = sqrt(pow(a.err_etabin3_isobin1*scale,2) + pow(err_scale*a.num_etabin3_isobin1,2)); a.num_etabin3_isobin1 *= scale;
    a.err_etabin3_isobin2 = sqrt(pow(a.err_etabin3_isobin2*scale,2) + pow(err_scale*a.num_etabin3_isobin2,2)); a.num_etabin3_isobin2 *= scale;
    a.err_etabin3_isobin3 = sqrt(pow(a.err_etabin3_isobin3*scale,2) + pow(err_scale*a.num_etabin3_isobin3,2)); a.num_etabin3_isobin3 *= scale;
    a.err_etabin3_isobin4 = sqrt(pow(a.err_etabin3_isobin4*scale,2) + pow(err_scale*a.num_etabin3_isobin4,2)); a.num_etabin3_isobin4 *= scale;
    a.err_etabin3_isobin5 = sqrt(pow(a.err_etabin3_isobin5*scale,2) + pow(err_scale*a.num_etabin3_isobin5,2)); a.num_etabin3_isobin5 *= scale;
    a.err_etabin4_isobin1 = sqrt(pow(a.err_etabin4_isobin1*scale,2) + pow(err_scale*a.num_etabin4_isobin1,2)); a.num_etabin4_isobin1 *= scale;
    a.err_etabin4_isobin2 = sqrt(pow(a.err_etabin4_isobin2*scale,2) + pow(err_scale*a.num_etabin4_isobin2,2)); a.num_etabin4_isobin2 *= scale;
    a.err_etabin4_isobin3 = sqrt(pow(a.err_etabin4_isobin3*scale,2) + pow(err_scale*a.num_etabin4_isobin3,2)); a.num_etabin4_isobin3 *= scale;
    a.err_etabin4_isobin4 = sqrt(pow(a.err_etabin4_isobin4*scale,2) + pow(err_scale*a.num_etabin4_isobin4,2)); a.num_etabin4_isobin4 *= scale;
    a.err_etabin4_isobin5 = sqrt(pow(a.err_etabin4_isobin5*scale,2) + pow(err_scale*a.num_etabin4_isobin5,2)); a.num_etabin4_isobin5 *= scale;
    a.err_etabin5_isobin1 = sqrt(pow(a.err_etabin5_isobin1*scale,2) + pow(err_scale*a.num_etabin5_isobin1,2)); a.num_etabin5_isobin1 *= scale;
    a.err_etabin5_isobin2 = sqrt(pow(a.err_etabin5_isobin2*scale,2) + pow(err_scale*a.num_etabin5_isobin2,2)); a.num_etabin5_isobin2 *= scale;
    a.err_etabin5_isobin3 = sqrt(pow(a.err_etabin5_isobin3*scale,2) + pow(err_scale*a.num_etabin5_isobin3,2)); a.num_etabin5_isobin3 *= scale;
    a.err_etabin5_isobin4 = sqrt(pow(a.err_etabin5_isobin4*scale,2) + pow(err_scale*a.num_etabin5_isobin4,2)); a.num_etabin5_isobin4 *= scale;
    a.err_etabin5_isobin5 = sqrt(pow(a.err_etabin5_isobin5*scale,2) + pow(err_scale*a.num_etabin5_isobin5,2)); a.num_etabin5_isobin5 *= scale;
}

Figure	SumFigures(vector<Figure> &cs, bool debug) {
    Figure a = EmptyFigure();
    for (int i = 0; i < cs.size(); i++) {
	if (i == 0) {
	    a.h_ptcone = (TH1F*)cs.at(i).h_ptcone->Clone();
	    a.h_leppt = (TH1F*)cs.at(i).h_leppt->Clone();
	    a.h_lepeta = (TH1F*)cs.at(i).h_lepeta->Clone();
	    a.h_jetpt = (TH1F*)cs.at(i).h_jetpt->Clone();
	    a.h_jeteta = (TH1F*)cs.at(i).h_jeteta->Clone();
	    a.h_phpt = (TH1F*)cs.at(i).h_phpt->Clone();
	    a.h_pheta = (TH1F*)cs.at(i).h_pheta->Clone();
	    a.h_met = (TH1F*)cs.at(i).h_met->Clone();
	    a.h_mphl = (TH1F*)cs.at(i).h_mphl->Clone();
	    a.h_njet = (TH1F*)cs.at(i).h_njet->Clone();
	    a.h_nbjet = (TH1F*)cs.at(i).h_nbjet->Clone();
	    a.h_drphl = (TH1F*)cs.at(i).h_drphl->Clone();
	} else {
	    a.h_ptcone->Add(cs.at(i).h_ptcone);
	    a.h_leppt->Add(cs.at(i).h_leppt);
	    a.h_lepeta->Add(cs.at(i).h_lepeta);
	    a.h_jetpt->Add(cs.at(i).h_jetpt);
	    a.h_jeteta->Add(cs.at(i).h_jeteta);
	    a.h_phpt->Add(cs.at(i).h_phpt);
	    a.h_pheta->Add(cs.at(i).h_pheta);
	    a.h_met->Add(cs.at(i).h_met);
	    a.h_mphl->Add(cs.at(i).h_mphl);
	    a.h_njet->Add(cs.at(i).h_njet);
	    a.h_nbjet->Add(cs.at(i).h_nbjet);
	    a.h_drphl->Add(cs.at(i).h_drphl);
	}
    }
    return a;
}

Counter	SumCounters(vector<Counter> &cs, bool normiso) {
    Counter a = EmptyCounter();
    for (int i = 0; i < cs.size(); i++) {
	a.num_all += cs.at(i).num_all; a.err_all = sqrt(pow(cs.at(i).err_all,2) + pow(a.err_all,2));
	a.num_total += cs.at(i).num_total; a.err_total = sqrt(pow(cs.at(i).err_total,2) + pow(a.err_total,2));
	a.num_total_ptbin1 += cs.at(i).num_total_ptbin1; a.err_total_ptbin1 = sqrt(pow(cs.at(i).err_total_ptbin1,2) + pow(a.err_total_ptbin1,2));
	a.num_total_ptbin2 += cs.at(i).num_total_ptbin2; a.err_total_ptbin2 = sqrt(pow(cs.at(i).err_total_ptbin2,2) + pow(a.err_total_ptbin2,2));
	a.num_total_ptbin3 += cs.at(i).num_total_ptbin3; a.err_total_ptbin3 = sqrt(pow(cs.at(i).err_total_ptbin3,2) + pow(a.err_total_ptbin3,2));
	a.num_total_ptbin4 += cs.at(i).num_total_ptbin4; a.err_total_ptbin4 = sqrt(pow(cs.at(i).err_total_ptbin4,2) + pow(a.err_total_ptbin4,2));
	a.num_total_ptbin5 += cs.at(i).num_total_ptbin5; a.err_total_ptbin5 = sqrt(pow(cs.at(i).err_total_ptbin5,2) + pow(a.err_total_ptbin5,2));
	a.num_total_etabin1 += cs.at(i).num_total_etabin1; a.err_total_etabin1 = sqrt(pow(cs.at(i).err_total_etabin1,2) + pow(a.err_total_etabin1,2));
	a.num_total_etabin2 += cs.at(i).num_total_etabin2; a.err_total_etabin2 = sqrt(pow(cs.at(i).err_total_etabin2,2) + pow(a.err_total_etabin2,2));
	a.num_total_etabin3 += cs.at(i).num_total_etabin3; a.err_total_etabin3 = sqrt(pow(cs.at(i).err_total_etabin3,2) + pow(a.err_total_etabin3,2));
	a.num_total_etabin4 += cs.at(i).num_total_etabin4; a.err_total_etabin4 = sqrt(pow(cs.at(i).err_total_etabin4,2) + pow(a.err_total_etabin4,2));
	a.num_total_etabin5 += cs.at(i).num_total_etabin5; a.err_total_etabin5 = sqrt(pow(cs.at(i).err_total_etabin5,2) + pow(a.err_total_etabin5,2));
	a.num_fidu += cs.at(i).num_fidu; a.err_fidu = sqrt(pow(cs.at(i).err_fidu,2) + pow(a.err_fidu,2));
	a.num_fidu_ptbin1 += cs.at(i).num_fidu_ptbin1; a.err_fidu_ptbin1 = sqrt(pow(cs.at(i).err_fidu_ptbin1,2) + pow(a.err_fidu_ptbin1,2));
	a.num_fidu_ptbin2 += cs.at(i).num_fidu_ptbin2; a.err_fidu_ptbin2 = sqrt(pow(cs.at(i).err_fidu_ptbin2,2) + pow(a.err_fidu_ptbin2,2));
	a.num_fidu_ptbin3 += cs.at(i).num_fidu_ptbin3; a.err_fidu_ptbin3 = sqrt(pow(cs.at(i).err_fidu_ptbin3,2) + pow(a.err_fidu_ptbin3,2));
	a.num_fidu_ptbin4 += cs.at(i).num_fidu_ptbin4; a.err_fidu_ptbin4 = sqrt(pow(cs.at(i).err_fidu_ptbin4,2) + pow(a.err_fidu_ptbin4,2));
	a.num_fidu_ptbin5 += cs.at(i).num_fidu_ptbin5; a.err_fidu_ptbin5 = sqrt(pow(cs.at(i).err_fidu_ptbin5,2) + pow(a.err_fidu_ptbin5,2));
	a.num_fidu_etabin1 += cs.at(i).num_fidu_etabin1; a.err_fidu_etabin1 = sqrt(pow(cs.at(i).err_fidu_etabin1,2) + pow(a.err_fidu_etabin1,2));
	a.num_fidu_etabin2 += cs.at(i).num_fidu_etabin2; a.err_fidu_etabin2 = sqrt(pow(cs.at(i).err_fidu_etabin2,2) + pow(a.err_fidu_etabin2,2));
	a.num_fidu_etabin3 += cs.at(i).num_fidu_etabin3; a.err_fidu_etabin3 = sqrt(pow(cs.at(i).err_fidu_etabin3,2) + pow(a.err_fidu_etabin3,2));
	a.num_fidu_etabin4 += cs.at(i).num_fidu_etabin4; a.err_fidu_etabin4 = sqrt(pow(cs.at(i).err_fidu_etabin4,2) + pow(a.err_fidu_etabin4,2));
	a.num_fidu_etabin5 += cs.at(i).num_fidu_etabin5; a.err_fidu_etabin5 = sqrt(pow(cs.at(i).err_fidu_etabin5,2) + pow(a.err_fidu_etabin5,2));
	a.num_isobin1 += cs.at(i).num_isobin1; a.err_isobin1 = sqrt(pow(cs.at(i).err_isobin1,2) + pow(a.err_isobin1,2));
	a.num_isobin2 += cs.at(i).num_isobin2; a.err_isobin2 = sqrt(pow(cs.at(i).err_isobin2,2) + pow(a.err_isobin2,2));
	a.num_isobin3 += cs.at(i).num_isobin3; a.err_isobin3 = sqrt(pow(cs.at(i).err_isobin3,2) + pow(a.err_isobin3,2));
	a.num_isobin4 += cs.at(i).num_isobin4; a.err_isobin4 = sqrt(pow(cs.at(i).err_isobin4,2) + pow(a.err_isobin4,2));
	a.num_isobin5 += cs.at(i).num_isobin5; a.err_isobin5 = sqrt(pow(cs.at(i).err_isobin5,2) + pow(a.err_isobin5,2));
	a.num_ptbin1_isobin1 += cs.at(i).num_ptbin1_isobin1; a.err_ptbin1_isobin1 = sqrt(pow(cs.at(i).err_ptbin1_isobin1,2) + pow(a.err_ptbin1_isobin1,2));
	a.num_ptbin1_isobin2 += cs.at(i).num_ptbin1_isobin2; a.err_ptbin1_isobin2 = sqrt(pow(cs.at(i).err_ptbin1_isobin2,2) + pow(a.err_ptbin1_isobin2,2));
	a.num_ptbin1_isobin3 += cs.at(i).num_ptbin1_isobin3; a.err_ptbin1_isobin3 = sqrt(pow(cs.at(i).err_ptbin1_isobin3,2) + pow(a.err_ptbin1_isobin3,2));
	a.num_ptbin1_isobin4 += cs.at(i).num_ptbin1_isobin4; a.err_ptbin1_isobin4 = sqrt(pow(cs.at(i).err_ptbin1_isobin4,2) + pow(a.err_ptbin1_isobin4,2));
	a.num_ptbin1_isobin5 += cs.at(i).num_ptbin1_isobin5; a.err_ptbin1_isobin5 = sqrt(pow(cs.at(i).err_ptbin1_isobin5,2) + pow(a.err_ptbin1_isobin5,2));
	a.num_ptbin2_isobin1 += cs.at(i).num_ptbin2_isobin1; a.err_ptbin2_isobin1 = sqrt(pow(cs.at(i).err_ptbin2_isobin1,2) + pow(a.err_ptbin2_isobin1,2));
	a.num_ptbin2_isobin2 += cs.at(i).num_ptbin2_isobin2; a.err_ptbin2_isobin2 = sqrt(pow(cs.at(i).err_ptbin2_isobin2,2) + pow(a.err_ptbin2_isobin2,2));
	a.num_ptbin2_isobin3 += cs.at(i).num_ptbin2_isobin3; a.err_ptbin2_isobin3 = sqrt(pow(cs.at(i).err_ptbin2_isobin3,2) + pow(a.err_ptbin2_isobin3,2));
	a.num_ptbin2_isobin4 += cs.at(i).num_ptbin2_isobin4; a.err_ptbin2_isobin4 = sqrt(pow(cs.at(i).err_ptbin2_isobin4,2) + pow(a.err_ptbin2_isobin4,2));
	a.num_ptbin2_isobin5 += cs.at(i).num_ptbin2_isobin5; a.err_ptbin2_isobin5 = sqrt(pow(cs.at(i).err_ptbin2_isobin5,2) + pow(a.err_ptbin2_isobin5,2));
	a.num_ptbin3_isobin1 += cs.at(i).num_ptbin3_isobin1; a.err_ptbin3_isobin1 = sqrt(pow(cs.at(i).err_ptbin3_isobin1,2) + pow(a.err_ptbin3_isobin1,2));
	a.num_ptbin3_isobin2 += cs.at(i).num_ptbin3_isobin2; a.err_ptbin3_isobin2 = sqrt(pow(cs.at(i).err_ptbin3_isobin2,2) + pow(a.err_ptbin3_isobin2,2));
	a.num_ptbin3_isobin3 += cs.at(i).num_ptbin3_isobin3; a.err_ptbin3_isobin3 = sqrt(pow(cs.at(i).err_ptbin3_isobin3,2) + pow(a.err_ptbin3_isobin3,2));
	a.num_ptbin3_isobin4 += cs.at(i).num_ptbin3_isobin4; a.err_ptbin3_isobin4 = sqrt(pow(cs.at(i).err_ptbin3_isobin4,2) + pow(a.err_ptbin3_isobin4,2));
	a.num_ptbin3_isobin5 += cs.at(i).num_ptbin3_isobin5; a.err_ptbin3_isobin5 = sqrt(pow(cs.at(i).err_ptbin3_isobin5,2) + pow(a.err_ptbin3_isobin5,2));
	a.num_ptbin4_isobin1 += cs.at(i).num_ptbin4_isobin1; a.err_ptbin4_isobin1 = sqrt(pow(cs.at(i).err_ptbin4_isobin1,2) + pow(a.err_ptbin4_isobin1,2));
	a.num_ptbin4_isobin2 += cs.at(i).num_ptbin4_isobin2; a.err_ptbin4_isobin2 = sqrt(pow(cs.at(i).err_ptbin4_isobin2,2) + pow(a.err_ptbin4_isobin2,2));
	a.num_ptbin4_isobin3 += cs.at(i).num_ptbin4_isobin3; a.err_ptbin4_isobin3 = sqrt(pow(cs.at(i).err_ptbin4_isobin3,2) + pow(a.err_ptbin4_isobin3,2));
	a.num_ptbin4_isobin4 += cs.at(i).num_ptbin4_isobin4; a.err_ptbin4_isobin4 = sqrt(pow(cs.at(i).err_ptbin4_isobin4,2) + pow(a.err_ptbin4_isobin4,2));
	a.num_ptbin4_isobin5 += cs.at(i).num_ptbin4_isobin5; a.err_ptbin4_isobin5 = sqrt(pow(cs.at(i).err_ptbin4_isobin5,2) + pow(a.err_ptbin4_isobin5,2));
	a.num_ptbin5_isobin1 += cs.at(i).num_ptbin5_isobin1; a.err_ptbin5_isobin1 = sqrt(pow(cs.at(i).err_ptbin5_isobin1,2) + pow(a.err_ptbin5_isobin1,2));
	a.num_ptbin5_isobin2 += cs.at(i).num_ptbin5_isobin2; a.err_ptbin5_isobin2 = sqrt(pow(cs.at(i).err_ptbin5_isobin2,2) + pow(a.err_ptbin5_isobin2,2));
	a.num_ptbin5_isobin3 += cs.at(i).num_ptbin5_isobin3; a.err_ptbin5_isobin3 = sqrt(pow(cs.at(i).err_ptbin5_isobin3,2) + pow(a.err_ptbin5_isobin3,2));
	a.num_ptbin5_isobin4 += cs.at(i).num_ptbin5_isobin4; a.err_ptbin5_isobin4 = sqrt(pow(cs.at(i).err_ptbin5_isobin4,2) + pow(a.err_ptbin5_isobin4,2));
	a.num_ptbin5_isobin5 += cs.at(i).num_ptbin5_isobin5; a.err_ptbin5_isobin5 = sqrt(pow(cs.at(i).err_ptbin5_isobin5,2) + pow(a.err_ptbin5_isobin5,2));
	a.num_etabin1_isobin1 += cs.at(i).num_etabin1_isobin1; a.err_etabin1_isobin1 = sqrt(pow(cs.at(i).err_etabin1_isobin1,2) + pow(a.err_etabin1_isobin1,2));
	a.num_etabin1_isobin2 += cs.at(i).num_etabin1_isobin2; a.err_etabin1_isobin2 = sqrt(pow(cs.at(i).err_etabin1_isobin2,2) + pow(a.err_etabin1_isobin2,2));
	a.num_etabin1_isobin3 += cs.at(i).num_etabin1_isobin3; a.err_etabin1_isobin3 = sqrt(pow(cs.at(i).err_etabin1_isobin3,2) + pow(a.err_etabin1_isobin3,2));
	a.num_etabin1_isobin4 += cs.at(i).num_etabin1_isobin4; a.err_etabin1_isobin4 = sqrt(pow(cs.at(i).err_etabin1_isobin4,2) + pow(a.err_etabin1_isobin4,2));
	a.num_etabin1_isobin5 += cs.at(i).num_etabin1_isobin5; a.err_etabin1_isobin5 = sqrt(pow(cs.at(i).err_etabin1_isobin5,2) + pow(a.err_etabin1_isobin5,2));
	a.num_etabin2_isobin1 += cs.at(i).num_etabin2_isobin1; a.err_etabin2_isobin1 = sqrt(pow(cs.at(i).err_etabin2_isobin1,2) + pow(a.err_etabin2_isobin1,2));
	a.num_etabin2_isobin2 += cs.at(i).num_etabin2_isobin2; a.err_etabin2_isobin2 = sqrt(pow(cs.at(i).err_etabin2_isobin2,2) + pow(a.err_etabin2_isobin2,2));
	a.num_etabin2_isobin3 += cs.at(i).num_etabin2_isobin3; a.err_etabin2_isobin3 = sqrt(pow(cs.at(i).err_etabin2_isobin3,2) + pow(a.err_etabin2_isobin3,2));
	a.num_etabin2_isobin4 += cs.at(i).num_etabin2_isobin4; a.err_etabin2_isobin4 = sqrt(pow(cs.at(i).err_etabin2_isobin4,2) + pow(a.err_etabin2_isobin4,2));
	a.num_etabin2_isobin5 += cs.at(i).num_etabin2_isobin5; a.err_etabin2_isobin5 = sqrt(pow(cs.at(i).err_etabin2_isobin5,2) + pow(a.err_etabin2_isobin5,2));
	a.num_etabin3_isobin1 += cs.at(i).num_etabin3_isobin1; a.err_etabin3_isobin1 = sqrt(pow(cs.at(i).err_etabin3_isobin1,2) + pow(a.err_etabin3_isobin1,2));
	a.num_etabin3_isobin2 += cs.at(i).num_etabin3_isobin2; a.err_etabin3_isobin2 = sqrt(pow(cs.at(i).err_etabin3_isobin2,2) + pow(a.err_etabin3_isobin2,2));
	a.num_etabin3_isobin3 += cs.at(i).num_etabin3_isobin3; a.err_etabin3_isobin3 = sqrt(pow(cs.at(i).err_etabin3_isobin3,2) + pow(a.err_etabin3_isobin3,2));
	a.num_etabin3_isobin4 += cs.at(i).num_etabin3_isobin4; a.err_etabin3_isobin4 = sqrt(pow(cs.at(i).err_etabin3_isobin4,2) + pow(a.err_etabin3_isobin4,2));
	a.num_etabin3_isobin5 += cs.at(i).num_etabin3_isobin5; a.err_etabin3_isobin5 = sqrt(pow(cs.at(i).err_etabin3_isobin5,2) + pow(a.err_etabin3_isobin5,2));
	a.num_etabin4_isobin1 += cs.at(i).num_etabin4_isobin1; a.err_etabin4_isobin1 = sqrt(pow(cs.at(i).err_etabin4_isobin1,2) + pow(a.err_etabin4_isobin1,2));
	a.num_etabin4_isobin2 += cs.at(i).num_etabin4_isobin2; a.err_etabin4_isobin2 = sqrt(pow(cs.at(i).err_etabin4_isobin2,2) + pow(a.err_etabin4_isobin2,2));
	a.num_etabin4_isobin3 += cs.at(i).num_etabin4_isobin3; a.err_etabin4_isobin3 = sqrt(pow(cs.at(i).err_etabin4_isobin3,2) + pow(a.err_etabin4_isobin3,2));
	a.num_etabin4_isobin4 += cs.at(i).num_etabin4_isobin4; a.err_etabin4_isobin4 = sqrt(pow(cs.at(i).err_etabin4_isobin4,2) + pow(a.err_etabin4_isobin4,2));
	a.num_etabin4_isobin5 += cs.at(i).num_etabin4_isobin5; a.err_etabin4_isobin5 = sqrt(pow(cs.at(i).err_etabin4_isobin5,2) + pow(a.err_etabin4_isobin5,2));
	a.num_etabin5_isobin1 += cs.at(i).num_etabin5_isobin1; a.err_etabin5_isobin1 = sqrt(pow(cs.at(i).err_etabin5_isobin1,2) + pow(a.err_etabin5_isobin1,2));
	a.num_etabin5_isobin2 += cs.at(i).num_etabin5_isobin2; a.err_etabin5_isobin2 = sqrt(pow(cs.at(i).err_etabin5_isobin2,2) + pow(a.err_etabin5_isobin2,2));
	a.num_etabin5_isobin3 += cs.at(i).num_etabin5_isobin3; a.err_etabin5_isobin3 = sqrt(pow(cs.at(i).err_etabin5_isobin3,2) + pow(a.err_etabin5_isobin3,2));
	a.num_etabin5_isobin4 += cs.at(i).num_etabin5_isobin4; a.err_etabin5_isobin4 = sqrt(pow(cs.at(i).err_etabin5_isobin4,2) + pow(a.err_etabin5_isobin4,2));
	a.num_etabin5_isobin5 += cs.at(i).num_etabin5_isobin5; a.err_etabin5_isobin5 = sqrt(pow(cs.at(i).err_etabin5_isobin5,2) + pow(a.err_etabin5_isobin5,2));
    }
    if (normiso) {
	float sum = a.num_isobin1 + a.num_isobin2 + a.num_isobin3 + a.num_isobin4 + a.num_isobin5;
	a.num_isobin1 /= sum;
	a.num_isobin2 /= sum;
	a.num_isobin3 /= sum;
	a.num_isobin4 /= sum;
	a.num_isobin5 /= sum;
	a.err_isobin1 /= sum;
	a.err_isobin2 /= sum;
	a.err_isobin3 /= sum;
	a.err_isobin4 /= sum;
	a.err_isobin5 /= sum;
	float sum_ptbin1 = a.num_ptbin1_isobin1 + a.num_ptbin1_isobin2 + a.num_ptbin1_isobin3 + a.num_ptbin1_isobin4 + a.num_ptbin1_isobin5;
	a.num_ptbin1_isobin1 /= sum_ptbin1;
	a.num_ptbin1_isobin2 /= sum_ptbin1;
	a.num_ptbin1_isobin3 /= sum_ptbin1;
	a.num_ptbin1_isobin4 /= sum_ptbin1;
	a.num_ptbin1_isobin5 /= sum_ptbin1;
	a.err_ptbin1_isobin1 /= sum_ptbin1;
	a.err_ptbin1_isobin2 /= sum_ptbin1;
	a.err_ptbin1_isobin3 /= sum_ptbin1;
	a.err_ptbin1_isobin4 /= sum_ptbin1;
	a.err_ptbin1_isobin5 /= sum_ptbin1;
	float sum_ptbin2 = a.num_ptbin2_isobin1 + a.num_ptbin2_isobin2 + a.num_ptbin2_isobin3 + a.num_ptbin2_isobin4 + a.num_ptbin2_isobin5;
	a.num_ptbin2_isobin1 /= sum_ptbin2;
	a.num_ptbin2_isobin2 /= sum_ptbin2;
	a.num_ptbin2_isobin3 /= sum_ptbin2;
	a.num_ptbin2_isobin4 /= sum_ptbin2;
	a.num_ptbin2_isobin5 /= sum_ptbin2;
	a.err_ptbin2_isobin1 /= sum_ptbin2;
	a.err_ptbin2_isobin2 /= sum_ptbin2;
	a.err_ptbin2_isobin3 /= sum_ptbin2;
	a.err_ptbin2_isobin4 /= sum_ptbin2;
	a.err_ptbin2_isobin5 /= sum_ptbin2;
	float sum_ptbin3 = a.num_ptbin3_isobin1 + a.num_ptbin3_isobin2 + a.num_ptbin3_isobin3 + a.num_ptbin3_isobin4 + a.num_ptbin3_isobin5;
	a.num_ptbin3_isobin1 /= sum_ptbin3;
	a.num_ptbin3_isobin2 /= sum_ptbin3;
	a.num_ptbin3_isobin3 /= sum_ptbin3;
	a.num_ptbin3_isobin4 /= sum_ptbin3;
	a.num_ptbin3_isobin5 /= sum_ptbin3;
	a.err_ptbin3_isobin1 /= sum_ptbin3;
	a.err_ptbin3_isobin2 /= sum_ptbin3;
	a.err_ptbin3_isobin3 /= sum_ptbin3;
	a.err_ptbin3_isobin4 /= sum_ptbin3;
	a.err_ptbin3_isobin5 /= sum_ptbin3;
	float sum_ptbin4 = a.num_ptbin4_isobin1 + a.num_ptbin4_isobin2 + a.num_ptbin4_isobin3 + a.num_ptbin4_isobin4 + a.num_ptbin4_isobin5;
	a.num_ptbin4_isobin1 /= sum_ptbin4;
	a.num_ptbin4_isobin2 /= sum_ptbin4;
	a.num_ptbin4_isobin3 /= sum_ptbin4;
	a.num_ptbin4_isobin4 /= sum_ptbin4;
	a.num_ptbin4_isobin5 /= sum_ptbin4;
	a.err_ptbin4_isobin1 /= sum_ptbin4;
	a.err_ptbin4_isobin2 /= sum_ptbin4;
	a.err_ptbin4_isobin3 /= sum_ptbin4;
	a.err_ptbin4_isobin4 /= sum_ptbin4;
	a.err_ptbin4_isobin5 /= sum_ptbin4;
	float sum_ptbin5 = a.num_ptbin5_isobin1 + a.num_ptbin5_isobin2 + a.num_ptbin5_isobin3 + a.num_ptbin5_isobin4 + a.num_ptbin5_isobin5;
	a.num_ptbin5_isobin1 /= sum_ptbin5;
	a.num_ptbin5_isobin2 /= sum_ptbin5;
	a.num_ptbin5_isobin3 /= sum_ptbin5;
	a.num_ptbin5_isobin4 /= sum_ptbin5;
	a.num_ptbin5_isobin5 /= sum_ptbin5;
	a.err_ptbin5_isobin1 /= sum_ptbin5;
	a.err_ptbin5_isobin2 /= sum_ptbin5;
	a.err_ptbin5_isobin3 /= sum_ptbin5;
	a.err_ptbin5_isobin4 /= sum_ptbin5;
	a.err_ptbin5_isobin5 /= sum_ptbin5;
	float sum_etabin1 = a.num_etabin1_isobin1 + a.num_etabin1_isobin2 + a.num_etabin1_isobin3 + a.num_etabin1_isobin4 + a.num_etabin1_isobin5;
	a.num_etabin1_isobin1 /= sum_etabin1;
	a.num_etabin1_isobin2 /= sum_etabin1;
	a.num_etabin1_isobin3 /= sum_etabin1;
	a.num_etabin1_isobin4 /= sum_etabin1;
	a.num_etabin1_isobin5 /= sum_etabin1;
	a.err_etabin1_isobin1 /= sum_etabin1;
	a.err_etabin1_isobin2 /= sum_etabin1;
	a.err_etabin1_isobin3 /= sum_etabin1;
	a.err_etabin1_isobin4 /= sum_etabin1;
	a.err_etabin1_isobin5 /= sum_etabin1;
	float sum_etabin2 = a.num_etabin2_isobin1 + a.num_etabin2_isobin2 + a.num_etabin2_isobin3 + a.num_etabin2_isobin4 + a.num_etabin2_isobin5;
	a.num_etabin2_isobin1 /= sum_etabin2;
	a.num_etabin2_isobin2 /= sum_etabin2;
	a.num_etabin2_isobin3 /= sum_etabin2;
	a.num_etabin2_isobin4 /= sum_etabin2;
	a.num_etabin2_isobin5 /= sum_etabin2;
	a.err_etabin2_isobin1 /= sum_etabin2;
	a.err_etabin2_isobin2 /= sum_etabin2;
	a.err_etabin2_isobin3 /= sum_etabin2;
	a.err_etabin2_isobin4 /= sum_etabin2;
	a.err_etabin2_isobin5 /= sum_etabin2;
	float sum_etabin3 = a.num_etabin3_isobin1 + a.num_etabin3_isobin2 + a.num_etabin3_isobin3 + a.num_etabin3_isobin4 + a.num_etabin3_isobin5;
	a.num_etabin3_isobin1 /= sum_etabin3;
	a.num_etabin3_isobin2 /= sum_etabin3;
	a.num_etabin3_isobin3 /= sum_etabin3;
	a.num_etabin3_isobin4 /= sum_etabin3;
	a.num_etabin3_isobin5 /= sum_etabin3;
	a.err_etabin3_isobin1 /= sum_etabin3;
	a.err_etabin3_isobin2 /= sum_etabin3;
	a.err_etabin3_isobin3 /= sum_etabin3;
	a.err_etabin3_isobin4 /= sum_etabin3;
	a.err_etabin3_isobin5 /= sum_etabin3;
	float sum_etabin4 = a.num_etabin4_isobin1 + a.num_etabin4_isobin2 + a.num_etabin4_isobin3 + a.num_etabin4_isobin4 + a.num_etabin4_isobin5;
	a.num_etabin4_isobin1 /= sum_etabin4;
	a.num_etabin4_isobin2 /= sum_etabin4;
	a.num_etabin4_isobin3 /= sum_etabin4;
	a.num_etabin4_isobin4 /= sum_etabin4;
	a.num_etabin4_isobin5 /= sum_etabin4;
	a.err_etabin4_isobin1 /= sum_etabin4;
	a.err_etabin4_isobin2 /= sum_etabin4;
	a.err_etabin4_isobin3 /= sum_etabin4;
	a.err_etabin4_isobin4 /= sum_etabin4;
	a.err_etabin4_isobin5 /= sum_etabin4;
	float sum_etabin5 = a.num_etabin5_isobin1 + a.num_etabin5_isobin2 + a.num_etabin5_isobin3 + a.num_etabin5_isobin4 + a.num_etabin5_isobin5;
	a.num_etabin5_isobin1 /= sum_etabin5;
	a.num_etabin5_isobin2 /= sum_etabin5;
	a.num_etabin5_isobin3 /= sum_etabin5;
	a.num_etabin5_isobin4 /= sum_etabin5;
	a.num_etabin5_isobin5 /= sum_etabin5;
	a.err_etabin5_isobin1 /= sum_etabin5;
	a.err_etabin5_isobin2 /= sum_etabin5;
	a.err_etabin5_isobin3 /= sum_etabin5;
	a.err_etabin5_isobin4 /= sum_etabin5;
	a.err_etabin5_isobin5 /= sum_etabin5;
    }
    return a;
}

Counter	SumCountersQuadratic(vector<Counter> &cs) {
    Counter a = EmptyCounter();
    for (int i = 0; i < cs.size(); i++) {
	a.num_all = sqrt(pow(a.num_all,2) + pow(cs.at(i).num_all,2));
	a.num_total = sqrt(pow(a.num_total,2) + pow(cs.at(i).num_total,2));
	a.num_total_ptbin1 = sqrt(pow(a.num_total_ptbin1,2) + pow(cs.at(i).num_total_ptbin1,2));
	a.num_total_ptbin2 = sqrt(pow(a.num_total_ptbin2,2) + pow(cs.at(i).num_total_ptbin2,2));
	a.num_total_ptbin3 = sqrt(pow(a.num_total_ptbin3,2) + pow(cs.at(i).num_total_ptbin3,2));
	a.num_total_ptbin4 = sqrt(pow(a.num_total_ptbin4,2) + pow(cs.at(i).num_total_ptbin4,2));
	a.num_total_ptbin5 = sqrt(pow(a.num_total_ptbin5,2) + pow(cs.at(i).num_total_ptbin5,2));
	a.num_total_etabin1 = sqrt(pow(a.num_total_etabin1,2) + pow(cs.at(i).num_total_etabin1,2));
	a.num_total_etabin2 = sqrt(pow(a.num_total_etabin2,2) + pow(cs.at(i).num_total_etabin2,2));
	a.num_total_etabin3 = sqrt(pow(a.num_total_etabin3,2) + pow(cs.at(i).num_total_etabin3,2));
	a.num_total_etabin4 = sqrt(pow(a.num_total_etabin4,2) + pow(cs.at(i).num_total_etabin4,2));
	a.num_total_etabin5 = sqrt(pow(a.num_total_etabin5,2) + pow(cs.at(i).num_total_etabin5,2));
	a.num_fidu = sqrt(pow(a.num_fidu,2) + pow(cs.at(i).num_fidu,2));
	a.num_fidu_ptbin1 = sqrt(pow(a.num_fidu_ptbin1,2) + pow(cs.at(i).num_fidu_ptbin1,2));
	a.num_fidu_ptbin2 = sqrt(pow(a.num_fidu_ptbin2,2) + pow(cs.at(i).num_fidu_ptbin2,2));
	a.num_fidu_ptbin3 = sqrt(pow(a.num_fidu_ptbin3,2) + pow(cs.at(i).num_fidu_ptbin3,2));
	a.num_fidu_ptbin4 = sqrt(pow(a.num_fidu_ptbin4,2) + pow(cs.at(i).num_fidu_ptbin4,2));
	a.num_fidu_ptbin5 = sqrt(pow(a.num_fidu_ptbin5,2) + pow(cs.at(i).num_fidu_ptbin5,2));
	a.num_fidu_etabin1 = sqrt(pow(a.num_fidu_etabin1,2) + pow(cs.at(i).num_fidu_etabin1,2));
	a.num_fidu_etabin2 = sqrt(pow(a.num_fidu_etabin2,2) + pow(cs.at(i).num_fidu_etabin2,2));
	a.num_fidu_etabin3 = sqrt(pow(a.num_fidu_etabin3,2) + pow(cs.at(i).num_fidu_etabin3,2));
	a.num_fidu_etabin4 = sqrt(pow(a.num_fidu_etabin4,2) + pow(cs.at(i).num_fidu_etabin4,2));
	a.num_fidu_etabin5 = sqrt(pow(a.num_fidu_etabin5,2) + pow(cs.at(i).num_fidu_etabin5,2));
	a.num_isobin1 = sqrt(pow(a.num_isobin1,2) + pow(cs.at(i).num_isobin1,2));
	a.num_isobin2 = sqrt(pow(a.num_isobin2,2) + pow(cs.at(i).num_isobin2,2));
	a.num_isobin3 = sqrt(pow(a.num_isobin3,2) + pow(cs.at(i).num_isobin3,2));
	a.num_isobin4 = sqrt(pow(a.num_isobin4,2) + pow(cs.at(i).num_isobin4,2));
	a.num_isobin5 = sqrt(pow(a.num_isobin5,2) + pow(cs.at(i).num_isobin5,2));
	a.num_ptbin1_isobin1 = sqrt(pow(a.num_ptbin1_isobin1,2) + pow(cs.at(i).num_ptbin1_isobin1,2));
	a.num_ptbin1_isobin2 = sqrt(pow(a.num_ptbin1_isobin2,2) + pow(cs.at(i).num_ptbin1_isobin2,2));
	a.num_ptbin1_isobin3 = sqrt(pow(a.num_ptbin1_isobin3,2) + pow(cs.at(i).num_ptbin1_isobin3,2));
	a.num_ptbin1_isobin4 = sqrt(pow(a.num_ptbin1_isobin4,2) + pow(cs.at(i).num_ptbin1_isobin4,2));
	a.num_ptbin1_isobin5 = sqrt(pow(a.num_ptbin1_isobin5,2) + pow(cs.at(i).num_ptbin1_isobin5,2));
	a.num_ptbin2_isobin1 = sqrt(pow(a.num_ptbin2_isobin1,2) + pow(cs.at(i).num_ptbin2_isobin1,2));
	a.num_ptbin2_isobin2 = sqrt(pow(a.num_ptbin2_isobin2,2) + pow(cs.at(i).num_ptbin2_isobin2,2));
	a.num_ptbin2_isobin3 = sqrt(pow(a.num_ptbin2_isobin3,2) + pow(cs.at(i).num_ptbin2_isobin3,2));
	a.num_ptbin2_isobin4 = sqrt(pow(a.num_ptbin2_isobin4,2) + pow(cs.at(i).num_ptbin2_isobin4,2));
	a.num_ptbin2_isobin5 = sqrt(pow(a.num_ptbin2_isobin5,2) + pow(cs.at(i).num_ptbin2_isobin5,2));
	a.num_ptbin3_isobin1 = sqrt(pow(a.num_ptbin3_isobin1,2) + pow(cs.at(i).num_ptbin3_isobin1,2));
	a.num_ptbin3_isobin2 = sqrt(pow(a.num_ptbin3_isobin2,2) + pow(cs.at(i).num_ptbin3_isobin2,2));
	a.num_ptbin3_isobin3 = sqrt(pow(a.num_ptbin3_isobin3,2) + pow(cs.at(i).num_ptbin3_isobin3,2));
	a.num_ptbin3_isobin4 = sqrt(pow(a.num_ptbin3_isobin4,2) + pow(cs.at(i).num_ptbin3_isobin4,2));
	a.num_ptbin3_isobin5 = sqrt(pow(a.num_ptbin3_isobin5,2) + pow(cs.at(i).num_ptbin3_isobin5,2));
	a.num_ptbin4_isobin1 = sqrt(pow(a.num_ptbin4_isobin1,2) + pow(cs.at(i).num_ptbin4_isobin1,2));
	a.num_ptbin4_isobin2 = sqrt(pow(a.num_ptbin4_isobin2,2) + pow(cs.at(i).num_ptbin4_isobin2,2));
	a.num_ptbin4_isobin3 = sqrt(pow(a.num_ptbin4_isobin3,2) + pow(cs.at(i).num_ptbin4_isobin3,2));
	a.num_ptbin4_isobin4 = sqrt(pow(a.num_ptbin4_isobin4,2) + pow(cs.at(i).num_ptbin4_isobin4,2));
	a.num_ptbin4_isobin5 = sqrt(pow(a.num_ptbin4_isobin5,2) + pow(cs.at(i).num_ptbin4_isobin5,2));
	a.num_ptbin5_isobin1 = sqrt(pow(a.num_ptbin5_isobin1,2) + pow(cs.at(i).num_ptbin5_isobin1,2));
	a.num_ptbin5_isobin2 = sqrt(pow(a.num_ptbin5_isobin2,2) + pow(cs.at(i).num_ptbin5_isobin2,2));
	a.num_ptbin5_isobin3 = sqrt(pow(a.num_ptbin5_isobin3,2) + pow(cs.at(i).num_ptbin5_isobin3,2));
	a.num_ptbin5_isobin4 = sqrt(pow(a.num_ptbin5_isobin4,2) + pow(cs.at(i).num_ptbin5_isobin4,2));
	a.num_ptbin5_isobin5 = sqrt(pow(a.num_ptbin5_isobin5,2) + pow(cs.at(i).num_ptbin5_isobin5,2));
	a.num_etabin1_isobin1 = sqrt(pow(a.num_etabin1_isobin1,2) + pow(cs.at(i).num_etabin1_isobin1,2));
	a.num_etabin1_isobin2 = sqrt(pow(a.num_etabin1_isobin2,2) + pow(cs.at(i).num_etabin1_isobin2,2));
	a.num_etabin1_isobin3 = sqrt(pow(a.num_etabin1_isobin3,2) + pow(cs.at(i).num_etabin1_isobin3,2));
	a.num_etabin1_isobin4 = sqrt(pow(a.num_etabin1_isobin4,2) + pow(cs.at(i).num_etabin1_isobin4,2));
	a.num_etabin1_isobin5 = sqrt(pow(a.num_etabin1_isobin5,2) + pow(cs.at(i).num_etabin1_isobin5,2));
	a.num_etabin2_isobin1 = sqrt(pow(a.num_etabin2_isobin1,2) + pow(cs.at(i).num_etabin2_isobin1,2));
	a.num_etabin2_isobin2 = sqrt(pow(a.num_etabin2_isobin2,2) + pow(cs.at(i).num_etabin2_isobin2,2));
	a.num_etabin2_isobin3 = sqrt(pow(a.num_etabin2_isobin3,2) + pow(cs.at(i).num_etabin2_isobin3,2));
	a.num_etabin2_isobin4 = sqrt(pow(a.num_etabin2_isobin4,2) + pow(cs.at(i).num_etabin2_isobin4,2));
	a.num_etabin2_isobin5 = sqrt(pow(a.num_etabin2_isobin5,2) + pow(cs.at(i).num_etabin2_isobin5,2));
	a.num_etabin3_isobin1 = sqrt(pow(a.num_etabin3_isobin1,2) + pow(cs.at(i).num_etabin3_isobin1,2));
	a.num_etabin3_isobin2 = sqrt(pow(a.num_etabin3_isobin2,2) + pow(cs.at(i).num_etabin3_isobin2,2));
	a.num_etabin3_isobin3 = sqrt(pow(a.num_etabin3_isobin3,2) + pow(cs.at(i).num_etabin3_isobin3,2));
	a.num_etabin3_isobin4 = sqrt(pow(a.num_etabin3_isobin4,2) + pow(cs.at(i).num_etabin3_isobin4,2));
	a.num_etabin3_isobin5 = sqrt(pow(a.num_etabin3_isobin5,2) + pow(cs.at(i).num_etabin3_isobin5,2));
	a.num_etabin4_isobin1 = sqrt(pow(a.num_etabin4_isobin1,2) + pow(cs.at(i).num_etabin4_isobin1,2));
	a.num_etabin4_isobin2 = sqrt(pow(a.num_etabin4_isobin2,2) + pow(cs.at(i).num_etabin4_isobin2,2));
	a.num_etabin4_isobin3 = sqrt(pow(a.num_etabin4_isobin3,2) + pow(cs.at(i).num_etabin4_isobin3,2));
	a.num_etabin4_isobin4 = sqrt(pow(a.num_etabin4_isobin4,2) + pow(cs.at(i).num_etabin4_isobin4,2));
	a.num_etabin4_isobin5 = sqrt(pow(a.num_etabin4_isobin5,2) + pow(cs.at(i).num_etabin4_isobin5,2));
	a.num_etabin5_isobin1 = sqrt(pow(a.num_etabin5_isobin1,2) + pow(cs.at(i).num_etabin5_isobin1,2));
	a.num_etabin5_isobin2 = sqrt(pow(a.num_etabin5_isobin2,2) + pow(cs.at(i).num_etabin5_isobin2,2));
	a.num_etabin5_isobin3 = sqrt(pow(a.num_etabin5_isobin3,2) + pow(cs.at(i).num_etabin5_isobin3,2));
	a.num_etabin5_isobin4 = sqrt(pow(a.num_etabin5_isobin4,2) + pow(cs.at(i).num_etabin5_isobin4,2));
	a.num_etabin5_isobin5 = sqrt(pow(a.num_etabin5_isobin5,2) + pow(cs.at(i).num_etabin5_isobin5,2));
    }
    return a;
}

Counter GetAverageCounter(Counter &a, Counter &b) {
    Counter c;
    c.num_all = (a.num_all + b.num_all)/2; c.err_all = (a.err_all + b.err_all)/2;
    c.num_total = (a.num_total + b.num_total)/2; c.err_total = (a.err_total + b.err_total)/2;
    c.num_total_ptbin1 = (a.num_total_ptbin1 + b.num_total_ptbin1)/2; c.err_total_ptbin1 = (a.err_total_ptbin1 + b.err_total_ptbin1)/2;
    c.num_total_ptbin2 = (a.num_total_ptbin2 + b.num_total_ptbin2)/2; c.err_total_ptbin2 = (a.err_total_ptbin2 + b.err_total_ptbin2)/2;
    c.num_total_ptbin3 = (a.num_total_ptbin3 + b.num_total_ptbin3)/2; c.err_total_ptbin3 = (a.err_total_ptbin3 + b.err_total_ptbin3)/2;
    c.num_total_ptbin4 = (a.num_total_ptbin4 + b.num_total_ptbin4)/2; c.err_total_ptbin4 = (a.err_total_ptbin4 + b.err_total_ptbin4)/2;
    c.num_total_ptbin5 = (a.num_total_ptbin5 + b.num_total_ptbin5)/2; c.err_total_ptbin5 = (a.err_total_ptbin5 + b.err_total_ptbin5)/2;
    c.num_total_etabin1 = (a.num_total_etabin1 + b.num_total_etabin1)/2; c.err_total_etabin1 = (a.err_total_etabin1 + b.err_total_etabin1)/2;
    c.num_total_etabin2 = (a.num_total_etabin2 + b.num_total_etabin2)/2; c.err_total_etabin2 = (a.err_total_etabin2 + b.err_total_etabin2)/2;
    c.num_total_etabin3 = (a.num_total_etabin3 + b.num_total_etabin3)/2; c.err_total_etabin3 = (a.err_total_etabin3 + b.err_total_etabin3)/2;
    c.num_total_etabin4 = (a.num_total_etabin4 + b.num_total_etabin4)/2; c.err_total_etabin4 = (a.err_total_etabin4 + b.err_total_etabin4)/2;
    c.num_total_etabin5 = (a.num_total_etabin5 + b.num_total_etabin5)/2; c.err_total_etabin5 = (a.err_total_etabin5 + b.err_total_etabin5)/2;
    c.num_fidu = (a.num_fidu + b.num_fidu)/2; c.err_fidu = (a.err_fidu + b.err_fidu)/2;
    c.num_fidu_ptbin1 = (a.num_fidu_ptbin1 + b.num_fidu_ptbin1)/2; c.err_fidu_ptbin1 = (a.err_fidu_ptbin1 + b.err_fidu_ptbin1)/2;
    c.num_fidu_ptbin2 = (a.num_fidu_ptbin2 + b.num_fidu_ptbin2)/2; c.err_fidu_ptbin2 = (a.err_fidu_ptbin2 + b.err_fidu_ptbin2)/2;
    c.num_fidu_ptbin3 = (a.num_fidu_ptbin3 + b.num_fidu_ptbin3)/2; c.err_fidu_ptbin3 = (a.err_fidu_ptbin3 + b.err_fidu_ptbin3)/2;
    c.num_fidu_ptbin4 = (a.num_fidu_ptbin4 + b.num_fidu_ptbin4)/2; c.err_fidu_ptbin4 = (a.err_fidu_ptbin4 + b.err_fidu_ptbin4)/2;
    c.num_fidu_ptbin5 = (a.num_fidu_ptbin5 + b.num_fidu_ptbin5)/2; c.err_fidu_ptbin5 = (a.err_fidu_ptbin5 + b.err_fidu_ptbin5)/2;
    c.num_fidu_etabin1 = (a.num_fidu_etabin1 + b.num_fidu_etabin1)/2; c.err_fidu_etabin1 = (a.err_fidu_etabin1 + b.err_fidu_etabin1)/2;
    c.num_fidu_etabin2 = (a.num_fidu_etabin2 + b.num_fidu_etabin2)/2; c.err_fidu_etabin2 = (a.err_fidu_etabin2 + b.err_fidu_etabin2)/2;
    c.num_fidu_etabin3 = (a.num_fidu_etabin3 + b.num_fidu_etabin3)/2; c.err_fidu_etabin3 = (a.err_fidu_etabin3 + b.err_fidu_etabin3)/2;
    c.num_fidu_etabin4 = (a.num_fidu_etabin4 + b.num_fidu_etabin4)/2; c.err_fidu_etabin4 = (a.err_fidu_etabin4 + b.err_fidu_etabin4)/2;
    c.num_fidu_etabin5 = (a.num_fidu_etabin5 + b.num_fidu_etabin5)/2; c.err_fidu_etabin5 = (a.err_fidu_etabin5 + b.err_fidu_etabin5)/2;
    c.num_isobin1 = (a.num_isobin1 + b.num_isobin1)/2; c.err_isobin1 = (a.err_isobin1 + b.err_isobin1)/2;
    c.num_isobin2 = (a.num_isobin2 + b.num_isobin2)/2; c.err_isobin2 = (a.err_isobin2 + b.err_isobin2)/2;
    c.num_isobin3 = (a.num_isobin3 + b.num_isobin3)/2; c.err_isobin3 = (a.err_isobin3 + b.err_isobin3)/2;
    c.num_isobin4 = (a.num_isobin4 + b.num_isobin4)/2; c.err_isobin4 = (a.err_isobin4 + b.err_isobin4)/2;
    c.num_isobin5 = (a.num_isobin5 + b.num_isobin5)/2; c.err_isobin5 = (a.err_isobin5 + b.err_isobin5)/2;
    c.num_ptbin1_isobin1 = (a.num_ptbin1_isobin1 + b.num_ptbin1_isobin1)/2; c.err_ptbin1_isobin1 = (a.err_ptbin1_isobin1 + b.err_ptbin1_isobin1)/2;
    c.num_ptbin1_isobin2 = (a.num_ptbin1_isobin2 + b.num_ptbin1_isobin2)/2; c.err_ptbin1_isobin2 = (a.err_ptbin1_isobin2 + b.err_ptbin1_isobin2)/2;
    c.num_ptbin1_isobin3 = (a.num_ptbin1_isobin3 + b.num_ptbin1_isobin3)/2; c.err_ptbin1_isobin3 = (a.err_ptbin1_isobin3 + b.err_ptbin1_isobin3)/2;
    c.num_ptbin1_isobin4 = (a.num_ptbin1_isobin4 + b.num_ptbin1_isobin4)/2; c.err_ptbin1_isobin4 = (a.err_ptbin1_isobin4 + b.err_ptbin1_isobin4)/2;
    c.num_ptbin1_isobin5 = (a.num_ptbin1_isobin5 + b.num_ptbin1_isobin5)/2; c.err_ptbin1_isobin5 = (a.err_ptbin1_isobin5 + b.err_ptbin1_isobin5)/2;
    c.num_ptbin2_isobin1 = (a.num_ptbin2_isobin1 + b.num_ptbin2_isobin1)/2; c.err_ptbin2_isobin1 = (a.err_ptbin2_isobin1 + b.err_ptbin2_isobin1)/2;
    c.num_ptbin2_isobin2 = (a.num_ptbin2_isobin2 + b.num_ptbin2_isobin2)/2; c.err_ptbin2_isobin2 = (a.err_ptbin2_isobin2 + b.err_ptbin2_isobin2)/2;
    c.num_ptbin2_isobin3 = (a.num_ptbin2_isobin3 + b.num_ptbin2_isobin3)/2; c.err_ptbin2_isobin3 = (a.err_ptbin2_isobin3 + b.err_ptbin2_isobin3)/2;
    c.num_ptbin2_isobin4 = (a.num_ptbin2_isobin4 + b.num_ptbin2_isobin4)/2; c.err_ptbin2_isobin4 = (a.err_ptbin2_isobin4 + b.err_ptbin2_isobin4)/2;
    c.num_ptbin2_isobin5 = (a.num_ptbin2_isobin5 + b.num_ptbin2_isobin5)/2; c.err_ptbin2_isobin5 = (a.err_ptbin2_isobin5 + b.err_ptbin2_isobin5)/2;
    c.num_ptbin3_isobin1 = (a.num_ptbin3_isobin1 + b.num_ptbin3_isobin1)/2; c.err_ptbin3_isobin1 = (a.err_ptbin3_isobin1 + b.err_ptbin3_isobin1)/2;
    c.num_ptbin3_isobin2 = (a.num_ptbin3_isobin2 + b.num_ptbin3_isobin2)/2; c.err_ptbin3_isobin2 = (a.err_ptbin3_isobin2 + b.err_ptbin3_isobin2)/2;
    c.num_ptbin3_isobin3 = (a.num_ptbin3_isobin3 + b.num_ptbin3_isobin3)/2; c.err_ptbin3_isobin3 = (a.err_ptbin3_isobin3 + b.err_ptbin3_isobin3)/2;
    c.num_ptbin3_isobin4 = (a.num_ptbin3_isobin4 + b.num_ptbin3_isobin4)/2; c.err_ptbin3_isobin4 = (a.err_ptbin3_isobin4 + b.err_ptbin3_isobin4)/2;
    c.num_ptbin3_isobin5 = (a.num_ptbin3_isobin5 + b.num_ptbin3_isobin5)/2; c.err_ptbin3_isobin5 = (a.err_ptbin3_isobin5 + b.err_ptbin3_isobin5)/2;
    c.num_ptbin4_isobin1 = (a.num_ptbin4_isobin1 + b.num_ptbin4_isobin1)/2; c.err_ptbin4_isobin1 = (a.err_ptbin4_isobin1 + b.err_ptbin4_isobin1)/2;
    c.num_ptbin4_isobin2 = (a.num_ptbin4_isobin2 + b.num_ptbin4_isobin2)/2; c.err_ptbin4_isobin2 = (a.err_ptbin4_isobin2 + b.err_ptbin4_isobin2)/2;
    c.num_ptbin4_isobin3 = (a.num_ptbin4_isobin3 + b.num_ptbin4_isobin3)/2; c.err_ptbin4_isobin3 = (a.err_ptbin4_isobin3 + b.err_ptbin4_isobin3)/2;
    c.num_ptbin4_isobin4 = (a.num_ptbin4_isobin4 + b.num_ptbin4_isobin4)/2; c.err_ptbin4_isobin4 = (a.err_ptbin4_isobin4 + b.err_ptbin4_isobin4)/2;
    c.num_ptbin4_isobin5 = (a.num_ptbin4_isobin5 + b.num_ptbin4_isobin5)/2; c.err_ptbin4_isobin5 = (a.err_ptbin4_isobin5 + b.err_ptbin4_isobin5)/2;
    c.num_ptbin5_isobin1 = (a.num_ptbin5_isobin1 + b.num_ptbin5_isobin1)/2; c.err_ptbin5_isobin1 = (a.err_ptbin5_isobin1 + b.err_ptbin5_isobin1)/2;
    c.num_ptbin5_isobin2 = (a.num_ptbin5_isobin2 + b.num_ptbin5_isobin2)/2; c.err_ptbin5_isobin2 = (a.err_ptbin5_isobin2 + b.err_ptbin5_isobin2)/2;
    c.num_ptbin5_isobin3 = (a.num_ptbin5_isobin3 + b.num_ptbin5_isobin3)/2; c.err_ptbin5_isobin3 = (a.err_ptbin5_isobin3 + b.err_ptbin5_isobin3)/2;
    c.num_ptbin5_isobin4 = (a.num_ptbin5_isobin4 + b.num_ptbin5_isobin4)/2; c.err_ptbin5_isobin4 = (a.err_ptbin5_isobin4 + b.err_ptbin5_isobin4)/2;
    c.num_ptbin5_isobin5 = (a.num_ptbin5_isobin5 + b.num_ptbin5_isobin5)/2; c.err_ptbin5_isobin5 = (a.err_ptbin5_isobin5 + b.err_ptbin5_isobin5)/2;
    c.num_etabin1_isobin1 = (a.num_etabin1_isobin1 + b.num_etabin1_isobin1)/2; c.err_etabin1_isobin1 = (a.err_etabin1_isobin1 + b.err_etabin1_isobin1)/2;
    c.num_etabin1_isobin2 = (a.num_etabin1_isobin2 + b.num_etabin1_isobin2)/2; c.err_etabin1_isobin2 = (a.err_etabin1_isobin2 + b.err_etabin1_isobin2)/2;
    c.num_etabin1_isobin3 = (a.num_etabin1_isobin3 + b.num_etabin1_isobin3)/2; c.err_etabin1_isobin3 = (a.err_etabin1_isobin3 + b.err_etabin1_isobin3)/2;
    c.num_etabin1_isobin4 = (a.num_etabin1_isobin4 + b.num_etabin1_isobin4)/2; c.err_etabin1_isobin4 = (a.err_etabin1_isobin4 + b.err_etabin1_isobin4)/2;
    c.num_etabin1_isobin5 = (a.num_etabin1_isobin5 + b.num_etabin1_isobin5)/2; c.err_etabin1_isobin5 = (a.err_etabin1_isobin5 + b.err_etabin1_isobin5)/2;
    c.num_etabin2_isobin1 = (a.num_etabin2_isobin1 + b.num_etabin2_isobin1)/2; c.err_etabin2_isobin1 = (a.err_etabin2_isobin1 + b.err_etabin2_isobin1)/2;
    c.num_etabin2_isobin2 = (a.num_etabin2_isobin2 + b.num_etabin2_isobin2)/2; c.err_etabin2_isobin2 = (a.err_etabin2_isobin2 + b.err_etabin2_isobin2)/2;
    c.num_etabin2_isobin3 = (a.num_etabin2_isobin3 + b.num_etabin2_isobin3)/2; c.err_etabin2_isobin3 = (a.err_etabin2_isobin3 + b.err_etabin2_isobin3)/2;
    c.num_etabin2_isobin4 = (a.num_etabin2_isobin4 + b.num_etabin2_isobin4)/2; c.err_etabin2_isobin4 = (a.err_etabin2_isobin4 + b.err_etabin2_isobin4)/2;
    c.num_etabin2_isobin5 = (a.num_etabin2_isobin5 + b.num_etabin2_isobin5)/2; c.err_etabin2_isobin5 = (a.err_etabin2_isobin5 + b.err_etabin2_isobin5)/2;
    c.num_etabin3_isobin1 = (a.num_etabin3_isobin1 + b.num_etabin3_isobin1)/2; c.err_etabin3_isobin1 = (a.err_etabin3_isobin1 + b.err_etabin3_isobin1)/2;
    c.num_etabin3_isobin2 = (a.num_etabin3_isobin2 + b.num_etabin3_isobin2)/2; c.err_etabin3_isobin2 = (a.err_etabin3_isobin2 + b.err_etabin3_isobin2)/2;
    c.num_etabin3_isobin3 = (a.num_etabin3_isobin3 + b.num_etabin3_isobin3)/2; c.err_etabin3_isobin3 = (a.err_etabin3_isobin3 + b.err_etabin3_isobin3)/2;
    c.num_etabin3_isobin4 = (a.num_etabin3_isobin4 + b.num_etabin3_isobin4)/2; c.err_etabin3_isobin4 = (a.err_etabin3_isobin4 + b.err_etabin3_isobin4)/2;
    c.num_etabin3_isobin5 = (a.num_etabin3_isobin5 + b.num_etabin3_isobin5)/2; c.err_etabin3_isobin5 = (a.err_etabin3_isobin5 + b.err_etabin3_isobin5)/2;
    c.num_etabin4_isobin1 = (a.num_etabin4_isobin1 + b.num_etabin4_isobin1)/2; c.err_etabin4_isobin1 = (a.err_etabin4_isobin1 + b.err_etabin4_isobin1)/2;
    c.num_etabin4_isobin2 = (a.num_etabin4_isobin2 + b.num_etabin4_isobin2)/2; c.err_etabin4_isobin2 = (a.err_etabin4_isobin2 + b.err_etabin4_isobin2)/2;
    c.num_etabin4_isobin3 = (a.num_etabin4_isobin3 + b.num_etabin4_isobin3)/2; c.err_etabin4_isobin3 = (a.err_etabin4_isobin3 + b.err_etabin4_isobin3)/2;
    c.num_etabin4_isobin4 = (a.num_etabin4_isobin4 + b.num_etabin4_isobin4)/2; c.err_etabin4_isobin4 = (a.err_etabin4_isobin4 + b.err_etabin4_isobin4)/2;
    c.num_etabin4_isobin5 = (a.num_etabin4_isobin5 + b.num_etabin4_isobin5)/2; c.err_etabin4_isobin5 = (a.err_etabin4_isobin5 + b.err_etabin4_isobin5)/2;
    c.num_etabin5_isobin1 = (a.num_etabin5_isobin1 + b.num_etabin5_isobin1)/2; c.err_etabin5_isobin1 = (a.err_etabin5_isobin1 + b.err_etabin5_isobin1)/2;
    c.num_etabin5_isobin2 = (a.num_etabin5_isobin2 + b.num_etabin5_isobin2)/2; c.err_etabin5_isobin2 = (a.err_etabin5_isobin2 + b.err_etabin5_isobin2)/2;
    c.num_etabin5_isobin3 = (a.num_etabin5_isobin3 + b.num_etabin5_isobin3)/2; c.err_etabin5_isobin3 = (a.err_etabin5_isobin3 + b.err_etabin5_isobin3)/2;
    c.num_etabin5_isobin4 = (a.num_etabin5_isobin4 + b.num_etabin5_isobin4)/2; c.err_etabin5_isobin4 = (a.err_etabin5_isobin4 + b.err_etabin5_isobin4)/2;
    c.num_etabin5_isobin5 = (a.num_etabin5_isobin5 + b.num_etabin5_isobin5)/2; c.err_etabin5_isobin5 = (a.err_etabin5_isobin5 + b.err_etabin5_isobin5)/2;
    return c;
}

AccRatio CalcuAccRatio(AccCorr &a, AccCorr &b) {
    AccRatio c;

    c.num_accratio = a.num_acc / b.num_acc;
    c.num_accratio_ptbin1 = a.num_acc_ptbin1 / b.num_acc_ptbin1;
    c.num_accratio_ptbin2 = a.num_acc_ptbin2 / b.num_acc_ptbin2;
    c.num_accratio_ptbin3 = a.num_acc_ptbin3 / b.num_acc_ptbin3;
    c.num_accratio_ptbin4 = a.num_acc_ptbin4 / b.num_acc_ptbin4;
    c.num_accratio_ptbin5 = a.num_acc_ptbin5 / b.num_acc_ptbin5;
    c.num_accratio_etabin1 = a.num_acc_etabin1 / b.num_acc_etabin1;
    c.num_accratio_etabin2 = a.num_acc_etabin2 / b.num_acc_etabin2;
    c.num_accratio_etabin3 = a.num_acc_etabin3 / b.num_acc_etabin3;
    c.num_accratio_etabin4 = a.num_acc_etabin4 / b.num_acc_etabin4;
    c.num_accratio_etabin5 = a.num_acc_etabin5 / b.num_acc_etabin5;

    c.err_accratio = GetErrorAoverB(a.num_acc, a.err_acc, b.num_acc, b.err_acc);
    c.err_accratio_ptbin1 = GetErrorAoverB(a.num_acc_ptbin1, a.err_acc_ptbin1, b.num_acc_ptbin1, b.err_acc);
    c.err_accratio_ptbin2 = GetErrorAoverB(a.num_acc_ptbin2, a.err_acc_ptbin2, b.num_acc_ptbin2, b.err_acc);
    c.err_accratio_ptbin3 = GetErrorAoverB(a.num_acc_ptbin3, a.err_acc_ptbin3, b.num_acc_ptbin3, b.err_acc);
    c.err_accratio_ptbin4 = GetErrorAoverB(a.num_acc_ptbin4, a.err_acc_ptbin4, b.num_acc_ptbin4, b.err_acc);
    c.err_accratio_ptbin5 = GetErrorAoverB(a.num_acc_ptbin5, a.err_acc_ptbin5, b.num_acc_ptbin5, b.err_acc);
    c.err_accratio_etabin1 = GetErrorAoverB(a.num_acc_etabin1, a.err_acc_etabin1, b.num_acc_etabin1, b.err_acc);
    c.err_accratio_etabin2 = GetErrorAoverB(a.num_acc_etabin2, a.err_acc_etabin2, b.num_acc_etabin2, b.err_acc);
    c.err_accratio_etabin3 = GetErrorAoverB(a.num_acc_etabin3, a.err_acc_etabin3, b.num_acc_etabin3, b.err_acc);
    c.err_accratio_etabin4 = GetErrorAoverB(a.num_acc_etabin4, a.err_acc_etabin4, b.num_acc_etabin4, b.err_acc);
    c.err_accratio_etabin5 = GetErrorAoverB(a.num_acc_etabin5, a.err_acc_etabin5, b.num_acc_etabin5, b.err_acc);

    return c;
}

