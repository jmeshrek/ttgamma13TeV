//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sun May 28 19:19:26 2017 by ROOT version 6.04/14
// from TTree nominal/tree
// found on file: /eos/atlas/user/c/caudron/TtGamma_PL/410082.ttgamma_noallhad.p2952.PL2.001.root
//////////////////////////////////////////////////////////

#ifndef PL_h
#define PL_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"
#include "vector"

using namespace std;

class PL {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Float_t         weight_mc;
   Float_t         weight_pileup;
   Float_t         weight_leptonSF;
   Float_t         weight_photonSF;
   Float_t         weight_bTagSF_77;
   Float_t         weight_bTagSF_85;
   Float_t         weight_bTagSF_70;
   Float_t         weight_bTagSF_Continuous;
   Float_t         weight_jvt;
   Float_t         weight_pileup_UP;
   Float_t         weight_pileup_DOWN;
   Float_t         weight_leptonSF_EL_SF_Trigger_UP;
   Float_t         weight_leptonSF_EL_SF_Trigger_DOWN;
   Float_t         weight_leptonSF_EL_SF_Reco_UP;
   Float_t         weight_leptonSF_EL_SF_Reco_DOWN;
   Float_t         weight_leptonSF_EL_SF_ID_UP;
   Float_t         weight_leptonSF_EL_SF_ID_DOWN;
   Float_t         weight_leptonSF_EL_SF_Isol_UP;
   Float_t         weight_leptonSF_EL_SF_Isol_DOWN;
   Float_t         weight_leptonSF_MU_SF_Trigger_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_Trigger_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Trigger_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_Trigger_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_DOWN;
   Float_t         weight_indiv_SF_EL_Trigger;
   Float_t         weight_indiv_SF_EL_Trigger_UP;
   Float_t         weight_indiv_SF_EL_Trigger_DOWN;
   Float_t         weight_indiv_SF_EL_Reco;
   Float_t         weight_indiv_SF_EL_Reco_UP;
   Float_t         weight_indiv_SF_EL_Reco_DOWN;
   Float_t         weight_indiv_SF_EL_ID;
   Float_t         weight_indiv_SF_EL_ID_UP;
   Float_t         weight_indiv_SF_EL_ID_DOWN;
   Float_t         weight_indiv_SF_EL_Isol;
   Float_t         weight_indiv_SF_EL_Isol_UP;
   Float_t         weight_indiv_SF_EL_Isol_DOWN;
   Float_t         weight_indiv_SF_EL_ChargeID;
   Float_t         weight_indiv_SF_EL_ChargeID_UP;
   Float_t         weight_indiv_SF_EL_ChargeID_DOWN;
   Float_t         weight_indiv_SF_EL_ChargeMisID;
   Float_t         weight_indiv_SF_EL_ChargeMisID_STAT_UP;
   Float_t         weight_indiv_SF_EL_ChargeMisID_STAT_DOWN;
   Float_t         weight_indiv_SF_EL_ChargeMisID_SYST_UP;
   Float_t         weight_indiv_SF_EL_ChargeMisID_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_Trigger;
   Float_t         weight_indiv_SF_MU_Trigger_STAT_UP;
   Float_t         weight_indiv_SF_MU_Trigger_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_Trigger_SYST_UP;
   Float_t         weight_indiv_SF_MU_Trigger_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_ID;
   Float_t         weight_indiv_SF_MU_ID_STAT_UP;
   Float_t         weight_indiv_SF_MU_ID_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_ID_SYST_UP;
   Float_t         weight_indiv_SF_MU_ID_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_ID_STAT_LOWPT_UP;
   Float_t         weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN;
   Float_t         weight_indiv_SF_MU_ID_SYST_LOWPT_UP;
   Float_t         weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN;
   Float_t         weight_indiv_SF_MU_Isol;
   Float_t         weight_indiv_SF_MU_Isol_STAT_UP;
   Float_t         weight_indiv_SF_MU_Isol_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_Isol_SYST_UP;
   Float_t         weight_indiv_SF_MU_Isol_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_TTVA;
   Float_t         weight_indiv_SF_MU_TTVA_STAT_UP;
   Float_t         weight_indiv_SF_MU_TTVA_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_TTVA_SYST_UP;
   Float_t         weight_indiv_SF_MU_TTVA_SYST_DOWN;
   Float_t         weight_photonSF_ID_UP;
   Float_t         weight_photonSF_ID_DOWN;
   Float_t         weight_photonSF_effIso;
   Float_t         weight_photonSF_effLowPtIso_UP;
   Float_t         weight_photonSF_effLowPtIso_DOWN;
   Float_t         weight_photonSF_effTrkIso_UP;
   Float_t         weight_photonSF_effTrkIso_DOWN;
   Float_t         weight_jvt_UP;
   Float_t         weight_jvt_DOWN;
   vector<float>   *weight_bTagSF_77_eigenvars_B_up;
   vector<float>   *weight_bTagSF_77_eigenvars_C_up;
   vector<float>   *weight_bTagSF_77_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_77_eigenvars_B_down;
   vector<float>   *weight_bTagSF_77_eigenvars_C_down;
   vector<float>   *weight_bTagSF_77_eigenvars_Light_down;
   Float_t         weight_bTagSF_77_extrapolation_up;
   Float_t         weight_bTagSF_77_extrapolation_down;
   Float_t         weight_bTagSF_77_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_77_extrapolation_from_charm_down;
   vector<float>   *weight_bTagSF_85_eigenvars_B_up;
   vector<float>   *weight_bTagSF_85_eigenvars_C_up;
   vector<float>   *weight_bTagSF_85_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_85_eigenvars_B_down;
   vector<float>   *weight_bTagSF_85_eigenvars_C_down;
   vector<float>   *weight_bTagSF_85_eigenvars_Light_down;
   Float_t         weight_bTagSF_85_extrapolation_up;
   Float_t         weight_bTagSF_85_extrapolation_down;
   Float_t         weight_bTagSF_85_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_85_extrapolation_from_charm_down;
   vector<float>   *weight_bTagSF_70_eigenvars_B_up;
   vector<float>   *weight_bTagSF_70_eigenvars_C_up;
   vector<float>   *weight_bTagSF_70_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_70_eigenvars_B_down;
   vector<float>   *weight_bTagSF_70_eigenvars_C_down;
   vector<float>   *weight_bTagSF_70_eigenvars_Light_down;
   Float_t         weight_bTagSF_70_extrapolation_up;
   Float_t         weight_bTagSF_70_extrapolation_down;
   Float_t         weight_bTagSF_70_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_70_extrapolation_from_charm_down;
   vector<float>   *weight_bTagSF_Continuous_eigenvars_B_up;
   vector<float>   *weight_bTagSF_Continuous_eigenvars_C_up;
   vector<float>   *weight_bTagSF_Continuous_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_Continuous_eigenvars_B_down;
   vector<float>   *weight_bTagSF_Continuous_eigenvars_C_down;
   vector<float>   *weight_bTagSF_Continuous_eigenvars_Light_down;
   Float_t         weight_bTagSF_Continuous_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_Continuous_extrapolation_from_charm_down;
   ULong64_t       eventNumber;
   UInt_t          runNumber;
   UInt_t          randomRunNumber;
   UInt_t          mcChannelNumber;
   Float_t         mu;
   UInt_t          backgroundFlags;
   UInt_t          hasBadMuon;
   vector<float>   *el_pt;
   vector<float>   *el_eta;
   vector<float>   *el_cl_eta;
   vector<float>   *el_phi;
   vector<float>   *el_e;
   vector<float>   *el_charge;
   vector<float>   *el_topoetcone20;
   vector<float>   *el_ptvarcone20;
   vector<char>    *el_CF;
   vector<float>   *el_d0sig;
   vector<float>   *el_delta_z0_sintheta;
   vector<int>     *el_true_type;
   vector<int>     *el_true_origin;
   vector<int>     *el_true_typebkg;
   vector<int>     *el_true_originbkg;
   vector<float>   *mu_pt;
   vector<float>   *mu_eta;
   vector<float>   *mu_phi;
   vector<float>   *mu_e;
   vector<float>   *mu_charge;
   vector<float>   *mu_topoetcone20;
   vector<float>   *mu_ptvarcone30;
   vector<float>   *mu_d0sig;
   vector<float>   *mu_delta_z0_sintheta;
   vector<int>     *mu_true_type;
   vector<int>     *mu_true_origin;
   vector<float>   *ph_pt;
   vector<float>   *ph_eta;
   vector<float>   *ph_phi;
   vector<float>   *ph_e;
   vector<float>   *ph_iso;
   vector<float>   *jet_pt;
   vector<float>   *jet_eta;
   vector<float>   *jet_phi;
   vector<float>   *jet_e;
   vector<float>   *jet_mv2c00;
   vector<float>   *jet_mv2c10;
   vector<float>   *jet_mv2c20;
   vector<float>   *jet_ip3dsv1;
   vector<float>   *jet_jvt;
   vector<char>    *jet_passfjvt;
   vector<int>     *jet_truthflav;
   vector<int>     *jet_truthPartonLabel;
   vector<char>    *jet_isTrueHS;
   vector<char>    *jet_isbtagged_77;
   vector<char>    *jet_isbtagged_85;
   vector<char>    *jet_isbtagged_70;
   vector<int>     *jet_tagWeightBin;
   Float_t         met_met;
   Float_t         met_phi;
   Int_t           ejets_2015;
   Int_t           ejets_2015_pl;
   Int_t           mujets_2015;
   Int_t           mujets_2015_pl;
   Int_t           emu_2015;
   Int_t           emu_2015_pl;
   Int_t           ee_2015;
   Int_t           ee_2015_pl;
   Int_t           mumu_2015;
   Int_t           mumu_2015_pl;
   Char_t          HLT_mu20_iloose_L1MU15;
   Char_t          HLT_mu50;
   Char_t          HLT_e60_lhmedium;
   Char_t          HLT_e24_lhmedium_L1EM20VH;
   Char_t          HLT_e120_lhloose;
   vector<char>    *el_trigMatch_HLT_e60_lhmedium;
   vector<char>    *el_trigMatch_HLT_e24_lhmedium_L1EM20VH;
   vector<char>    *el_trigMatch_HLT_e120_lhloose;
   vector<char>    *mu_trigMatch_HLT_mu50;
   vector<char>    *mu_trigMatch_HLT_mu20_iloose_L1MU15;
   vector<float>   *ph_topoetcone20;
   vector<float>   *ph_topoetcone30;
   vector<float>   *ph_topoetcone40;
   vector<float>   *ph_ptcone20;
   vector<float>   *ph_ptcone30;
   vector<float>   *ph_ptcone40;
   vector<float>   *ph_ptvarcone20;
   vector<float>   *ph_ptvarcone30;
   vector<float>   *ph_ptvarcone40;
   vector<char>    *ph_isoFCTCO;
   vector<char>    *ph_isoFCT;
   vector<char>    *ph_isoFCL;
   vector<char>    *ph_isTight;
   vector<char>    *ph_isLoose;
   vector<char>    *ph_isTight_daod;
   vector<char>    *ph_isHFake;
   vector<char>    *ph_isHadronFakeFailedDeltaE;
   vector<char>    *ph_isHadronFakeFailedFside;
   vector<char>    *ph_isHadronFakeFailedWs3;
   vector<char>    *ph_isHadronFakeFailedERatio;
   vector<float>   *ph_rhad1;
   vector<float>   *ph_rhad;
   vector<float>   *ph_reta;
   vector<float>   *ph_weta2;
   vector<float>   *ph_rphi;
   vector<float>   *ph_ws3;
   vector<float>   *ph_wstot;
   vector<float>   *ph_fracm;
   vector<float>   *ph_deltaE;
   vector<float>   *ph_eratio;
   vector<float>   *ph_emaxs1;
   vector<float>   *ph_f1;
   vector<float>   *ph_e277;
   vector<unsigned int> *ph_OQ;
   vector<unsigned int> *ph_author;
   vector<int>     *ph_conversionType;
   vector<float>   *ph_caloEta;
   vector<unsigned int> *ph_isEM;
   vector<int>     *ph_nVertices;
   vector<float>   *ph_SF_eff;
   vector<float>   *ph_SF_effUP;
   vector<float>   *ph_SF_effDO;
   vector<float>   *ph_SF_iso;
   vector<float>   *ph_SF_lowisoUP;
   vector<float>   *ph_SF_lowisoDO;
   vector<float>   *ph_SF_trkisoUP;
   vector<float>   *ph_SF_trkisoDO;
   vector<float>   *ph_drleadjet;
   vector<float>   *ph_dralljet;
   vector<float>   *ph_drsubljet;
   vector<float>   *ph_drlept;
   vector<float>   *ph_mgammalept;
   vector<float>   *ph_mgammaleptlept;
   Int_t           selph_index1;
   Int_t           selph_index2;
   Int_t           selhf_index1;
   vector<int>     *ph_truthType;
   vector<int>     *ph_truthOrigin;
   vector<int>     *ph_truthAncestor;
   vector<int>     *ph_mc_pid;
   vector<int>     *ph_mc_barcode;
   vector<float>   *ph_mc_pt;
   vector<float>   *ph_mc_eta;
   vector<float>   *ph_mc_phi;
   vector<float>   *ph_mcel_dr;
   vector<float>   *ph_mcel_pt;
   vector<float>   *ph_mcel_eta;
   vector<float>   *ph_mcel_phi;
   vector<float>   *mcph_pt;
   vector<float>   *mcph_eta;
   vector<float>   *mcph_phi;
   vector<int>     *mcph_ancestor;
   vector<int>     *el_truthAncestor;
   vector<char>    *el_isoGradient;
   vector<int>     *el_mc_pid;
   vector<float>   *el_mc_charge;
   vector<float>   *el_mc_pt;
   vector<float>   *el_mc_eta;
   vector<float>   *el_mc_phi;
   vector<float>   *mu_mc_charge;
   vector<float>   *jet_mcdr_tW1;
   vector<float>   *jet_mcdr_tW2;
   vector<float>   *jet_mcdr_tB;
   vector<float>   *jet_mcdr_tbW1;
   vector<float>   *jet_mcdr_tbW2;
   vector<float>   *jet_mcdr_tbB;
   vector<int>     *lepton_type;
   Float_t         event_HT;
   Float_t         event_mll;
   Float_t         event_mwt;
   Int_t           event_njets;
   Int_t           event_nbjets70;
   Int_t           event_nbjets77;
   Int_t           event_nbjets85;
   Int_t           event_ngoodphotons;
   Int_t           event_photonorigin;
   Int_t           event_photonoriginTA;
   Int_t           event_nhadronfakes;
   vector<float>   *ph_HFT_MVA;
   Int_t           ph_nHFT_MVA;

   // List of branches
   TBranch        *b_weight_mc;   //!
   TBranch        *b_weight_pileup;   //!
   TBranch        *b_weight_leptonSF;   //!
   TBranch        *b_weight_photonSF;   //!
   TBranch        *b_weight_bTagSF_77;   //!
   TBranch        *b_weight_bTagSF_85;   //!
   TBranch        *b_weight_bTagSF_70;   //!
   TBranch        *b_weight_bTagSF_Continuous;   //!
   TBranch        *b_weight_jvt;   //!
   TBranch        *b_weight_pileup_UP;   //!
   TBranch        *b_weight_pileup_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Trigger_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Trigger_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_Trigger;   //!
   TBranch        *b_weight_indiv_SF_EL_Trigger_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_Trigger_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_Reco;   //!
   TBranch        *b_weight_indiv_SF_EL_Reco_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_Reco_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_ID;   //!
   TBranch        *b_weight_indiv_SF_EL_ID_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_ID_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_Isol;   //!
   TBranch        *b_weight_indiv_SF_EL_Isol_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_Isol_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeID;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeID_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeID_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_Trigger;   //!
   TBranch        *b_weight_indiv_SF_MU_Trigger_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_Trigger_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_Trigger_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_Trigger_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_LOWPT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_LOWPT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_SYST_DOWN;   //!
   TBranch        *b_weight_photonSF_ID_UP;   //!
   TBranch        *b_weight_photonSF_ID_DOWN;   //!
   TBranch        *b_weight_photonSF_effIso;   //!
   TBranch        *b_weight_photonSF_effLowPtIso_UP;   //!
   TBranch        *b_weight_photonSF_effLowPtIso_DOWN;   //!
   TBranch        *b_weight_photonSF_effTrkIso_UP;   //!
   TBranch        *b_weight_photonSF_effTrkIso_DOWN;   //!
   TBranch        *b_weight_jvt_UP;   //!
   TBranch        *b_weight_jvt_DOWN;   //!
   TBranch        *b_weight_bTagSF_77_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_77_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_77_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_77_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_77_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_77_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_77_extrapolation_up;   //!
   TBranch        *b_weight_bTagSF_77_extrapolation_down;   //!
   TBranch        *b_weight_bTagSF_77_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_77_extrapolation_from_charm_down;   //!
   TBranch        *b_weight_bTagSF_85_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_85_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_85_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_85_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_85_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_85_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_85_extrapolation_up;   //!
   TBranch        *b_weight_bTagSF_85_extrapolation_down;   //!
   TBranch        *b_weight_bTagSF_85_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_85_extrapolation_from_charm_down;   //!
   TBranch        *b_weight_bTagSF_70_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_70_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_70_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_70_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_70_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_70_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_70_extrapolation_up;   //!
   TBranch        *b_weight_bTagSF_70_extrapolation_down;   //!
   TBranch        *b_weight_bTagSF_70_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_70_extrapolation_from_charm_down;   //!
   TBranch        *b_weight_bTagSF_Continuous_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_Continuous_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_Continuous_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_Continuous_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_Continuous_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_Continuous_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_Continuous_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_Continuous_extrapolation_from_charm_down;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_randomRunNumber;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_backgroundFlags;   //!
   TBranch        *b_hasBadMuon;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_cl_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_e;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_el_topoetcone20;   //!
   TBranch        *b_el_ptvarcone20;   //!
   TBranch        *b_el_CF;   //!
   TBranch        *b_el_d0sig;   //!
   TBranch        *b_el_delta_z0_sintheta;   //!
   TBranch        *b_el_true_type;   //!
   TBranch        *b_el_true_origin;   //!
   TBranch        *b_el_true_typebkg;   //!
   TBranch        *b_el_true_originbkg;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_e;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_mu_topoetcone20;   //!
   TBranch        *b_mu_ptvarcone30;   //!
   TBranch        *b_mu_d0sig;   //!
   TBranch        *b_mu_delta_z0_sintheta;   //!
   TBranch        *b_mu_true_type;   //!
   TBranch        *b_mu_true_origin;   //!
   TBranch        *b_ph_pt;   //!
   TBranch        *b_ph_eta;   //!
   TBranch        *b_ph_phi;   //!
   TBranch        *b_ph_e;   //!
   TBranch        *b_ph_iso;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_e;   //!
   TBranch        *b_jet_mv2c00;   //!
   TBranch        *b_jet_mv2c10;   //!
   TBranch        *b_jet_mv2c20;   //!
   TBranch        *b_jet_ip3dsv1;   //!
   TBranch        *b_jet_jvt;   //!
   TBranch        *b_jet_passfjvt;   //!
   TBranch        *b_jet_truthflav;   //!
   TBranch        *b_jet_truthPartonLabel;   //!
   TBranch        *b_jet_isTrueHS;   //!
   TBranch        *b_jet_isbtagged_77;   //!
   TBranch        *b_jet_isbtagged_85;   //!
   TBranch        *b_jet_isbtagged_70;   //!
   TBranch        *b_jet_tagWeightBin;   //!
   TBranch        *b_met_met;   //!
   TBranch        *b_met_phi;   //!
   TBranch        *b_ejets_2015;   //!
   TBranch        *b_ejets_2015_pl;   //!
   TBranch        *b_mujets_2015;   //!
   TBranch        *b_mujets_2015_pl;   //!
   TBranch        *b_emu_2015;   //!
   TBranch        *b_emu_2015_pl;   //!
   TBranch        *b_ee_2015;   //!
   TBranch        *b_ee_2015_pl;   //!
   TBranch        *b_mumu_2015;   //!
   TBranch        *b_mumu_2015_pl;   //!
   TBranch        *b_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_HLT_mu50;   //!
   TBranch        *b_HLT_e60_lhmedium;   //!
   TBranch        *b_HLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_HLT_e120_lhloose;   //!
   TBranch        *b_el_trigMatch_HLT_e60_lhmedium;   //!
   TBranch        *b_el_trigMatch_HLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_el_trigMatch_HLT_e120_lhloose;   //!
   TBranch        *b_mu_trigMatch_HLT_mu50;   //!
   TBranch        *b_mu_trigMatch_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_ph_topoetcone20;   //!
   TBranch        *b_ph_topoetcone30;   //!
   TBranch        *b_ph_topoetcone40;   //!
   TBranch        *b_ph_ptcone20;   //!
   TBranch        *b_ph_ptcone30;   //!
   TBranch        *b_ph_ptcone40;   //!
   TBranch        *b_ph_ptvarcone20;   //!
   TBranch        *b_ph_ptvarcone30;   //!
   TBranch        *b_ph_ptvarcone40;   //!
   TBranch        *b_ph_isoFCTCO;   //!
   TBranch        *b_ph_isoFCT;   //!
   TBranch        *b_ph_isoFCL;   //!
   TBranch        *b_ph_isTight;   //!
   TBranch        *b_ph_isLoose;   //!
   TBranch        *b_ph_isTight_daod;   //!
   TBranch        *b_ph_isHFake;   //!
   TBranch        *b_ph_isHadronFakeFailedDeltaE;   //!
   TBranch        *b_ph_isHadronFakeFailedFside;   //!
   TBranch        *b_ph_isHadronFakeFailedWs3;   //!
   TBranch        *b_ph_isHadronFakeFailedERatio;   //!
   TBranch        *b_ph_rhad1;   //!
   TBranch        *b_ph_rhad;   //!
   TBranch        *b_ph_reta;   //!
   TBranch        *b_ph_weta2;   //!
   TBranch        *b_ph_rphi;   //!
   TBranch        *b_ph_ws3;   //!
   TBranch        *b_ph_wstot;   //!
   TBranch        *b_ph_fracm;   //!
   TBranch        *b_ph_deltaE;   //!
   TBranch        *b_ph_eratio;   //!
   TBranch        *b_ph_emaxs1;   //!
   TBranch        *b_ph_f1;   //!
   TBranch        *b_ph_e277;   //!
   TBranch        *b_ph_OQ;   //!
   TBranch        *b_ph_author;   //!
   TBranch        *b_ph_conversionType;   //!
   TBranch        *b_ph_caloEta;   //!
   TBranch        *b_ph_isEM;   //!
   TBranch        *b_ph_nVertices;   //!
   TBranch        *b_ph_SF_eff;   //!
   TBranch        *b_ph_SF_effUP;   //!
   TBranch        *b_ph_SF_effDO;   //!
   TBranch        *b_ph_SF_iso;   //!
   TBranch        *b_ph_SF_lowisoUP;   //!
   TBranch        *b_ph_SF_lowisoDO;   //!
   TBranch        *b_ph_SF_trkisoUP;   //!
   TBranch        *b_ph_SF_trkisoDO;   //!
   TBranch        *b_ph_drleadjet;   //!
   TBranch        *b_ph_dralljet;   //!
   TBranch        *b_ph_drsubljet;   //!
   TBranch        *b_ph_drlept;   //!
   TBranch        *b_ph_mgammalept;   //!
   TBranch        *b_ph_mgammaleptlept;   //!
   TBranch        *b_selph_index1;   //!
   TBranch        *b_selph_index2;   //!
   TBranch        *b_selhf_index1;   //!
   TBranch        *b_ph_truthType;   //!
   TBranch        *b_ph_truthOrigin;   //!
   TBranch        *b_ph_truthAncestor;   //!
   TBranch        *b_ph_mc_pid;   //!
   TBranch        *b_ph_mc_barcode;   //!
   TBranch        *b_ph_mc_pt;   //!
   TBranch        *b_ph_mc_eta;   //!
   TBranch        *b_ph_mc_phi;   //!
   TBranch        *b_ph_mcel_dr;   //!
   TBranch        *b_ph_mcel_pt;   //!
   TBranch        *b_ph_mcel_eta;   //!
   TBranch        *b_ph_mcel_phi;   //!
   TBranch        *b_mcph_pt;   //!
   TBranch        *b_mcph_eta;   //!
   TBranch        *b_mcph_phi;   //!
   TBranch        *b_mcph_ancestor;   //!
   TBranch        *b_el_truthAncestor;   //!
   TBranch        *b_el_isoGradient;   //!
   TBranch        *b_el_mc_pid;   //!
   TBranch        *b_el_mc_charge;   //!
   TBranch        *b_el_mc_pt;   //!
   TBranch        *b_el_mc_eta;   //!
   TBranch        *b_el_mc_phi;   //!
   TBranch        *b_mu_mc_charge;   //!
   TBranch        *b_jet_mcdr_tW1;   //!
   TBranch        *b_jet_mcdr_tW2;   //!
   TBranch        *b_jet_mcdr_tB;   //!
   TBranch        *b_jet_mcdr_tbW1;   //!
   TBranch        *b_jet_mcdr_tbW2;   //!
   TBranch        *b_jet_mcdr_tbB;   //!
   TBranch        *b_lepton_type;   //!
   TBranch        *b_event_HT;   //!
   TBranch        *b_event_mll;   //!
   TBranch        *b_event_mwt;   //!
   TBranch        *b_event_njets;   //!
   TBranch        *b_event_nbjets70;   //!
   TBranch        *b_event_nbjets77;   //!
   TBranch        *b_event_nbjets85;   //!
   TBranch        *b_event_ngoodphotons;   //!
   TBranch        *b_event_photonorigin;   //!
   TBranch        *b_event_photonoriginTA;   //!
   TBranch        *b_event_nhadronfakes;   //!
   TBranch        *b_ph_HFT_MVA;   //!
   TBranch        *b_ph_nHFT_MVA;   //!

   PL(TTree *tree=0);
   virtual ~PL();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Int_t    GetTotalEntry();
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

