//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sun May 28 19:23:48 2017 by ROOT version 6.04/14
// from TTree particleLevel/tree
// found on file: /eos/atlas/user/c/caudron/TtGamma_PL/410082.ttgamma_noallhad.p2952.PL2.001.root
//////////////////////////////////////////////////////////

#ifndef PL2_h
#define PL2_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"

using namespace std;

class PL2 {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Float_t         weight_mc;
   ULong64_t       eventNumber;
   UInt_t          runNumber;
   UInt_t          randomRunNumber;
   UInt_t          mcChannelNumber;
   Float_t         mu;
   Float_t         weight_pileup;
   vector<float>   *el_pt;
   vector<float>   *el_eta;
   vector<float>   *el_phi;
   vector<float>   *el_e;
   vector<float>   *el_charge;
   vector<float>   *el_pt_bare;
   vector<float>   *el_eta_bare;
   vector<float>   *el_phi_bare;
   vector<float>   *el_e_bare;
   vector<float>   *mu_pt;
   vector<float>   *mu_eta;
   vector<float>   *mu_phi;
   vector<float>   *mu_e;
   vector<float>   *mu_charge;
   vector<float>   *mu_pt_bare;
   vector<float>   *mu_eta_bare;
   vector<float>   *mu_phi_bare;
   vector<float>   *mu_e_bare;
   vector<float>   *ph_pt;
   vector<float>   *ph_eta;
   vector<float>   *ph_phi;
   vector<float>   *ph_e;
   vector<float>   *jet_pt;
   vector<float>   *jet_eta;
   vector<float>   *jet_phi;
   vector<float>   *jet_e;
   vector<int>     *jet_nGhosts_bHadron;
   vector<int>     *jet_nGhosts_cHadron;
   Float_t         met_met;
   Float_t         met_phi;
   vector<float>   *PDFinfo_X1;
   vector<float>   *PDFinfo_X2;
   vector<int>     *PDFinfo_PDGID1;
   vector<int>     *PDFinfo_PDGID2;
   vector<float>   *PDFinfo_Q;
   vector<float>   *PDFinfo_XF1;
   vector<float>   *PDFinfo_XF2;
   Int_t           mumu_2015;
   Int_t           ee_2015_pl;
   Int_t           mumu_2015_pl;
   Int_t           ee_2015;
   Int_t           emu_2015;
   Int_t           mujets_2015_pl;
   Int_t           emu_2015_pl;
   Int_t           mujets_2015;
   Int_t           ejets_2015_pl;
   Int_t           ejets_2015;

   // List of branches
   TBranch        *b_weight_mc;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_randomRunNumber;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_weight_pileup;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_e;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_el_pt_bare;   //!
   TBranch        *b_el_eta_bare;   //!
   TBranch        *b_el_phi_bare;   //!
   TBranch        *b_el_e_bare;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_e;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_mu_pt_bare;   //!
   TBranch        *b_mu_eta_bare;   //!
   TBranch        *b_mu_phi_bare;   //!
   TBranch        *b_mu_e_bare;   //!
   TBranch        *b_ph_pt;   //!
   TBranch        *b_ph_eta;   //!
   TBranch        *b_ph_phi;   //!
   TBranch        *b_ph_e;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_e;   //!
   TBranch        *b_jet_nGhosts_bHadron;   //!
   TBranch        *b_jet_nGhosts_cHadron;   //!
   TBranch        *b_met_met;   //!
   TBranch        *b_met_phi;   //!
   TBranch        *b_PDFinfo_X1;   //!
   TBranch        *b_PDFinfo_X2;   //!
   TBranch        *b_PDFinfo_PDGID1;   //!
   TBranch        *b_PDFinfo_PDGID2;   //!
   TBranch        *b_PDFinfo_Q;   //!
   TBranch        *b_PDFinfo_XF1;   //!
   TBranch        *b_PDFinfo_XF2;   //!
   TBranch        *b_mumu_2015;   //!
   TBranch        *b_ee_2015_pl;   //!
   TBranch        *b_mumu_2015_pl;   //!
   TBranch        *b_ee_2015;   //!
   TBranch        *b_emu_2015;   //!
   TBranch        *b_mujets_2015_pl;   //!
   TBranch        *b_emu_2015_pl;   //!
   TBranch        *b_mujets_2015;   //!
   TBranch        *b_ejets_2015_pl;   //!
   TBranch        *b_ejets_2015;   //!

   PL2(TTree *tree=0);
   virtual ~PL2();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Int_t    GetTotalEntry();
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif
