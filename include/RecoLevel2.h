//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sat Oct 20 16:37:56 2018 by ROOT version 5.34/38
// from TTree nominal/tree
// found on file: /afs/cern.ch/work/a/arej/public/output_410389_new.root
//////////////////////////////////////////////////////////

#ifndef RecoLevel2_h
#define RecoLevel2_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include <vector>
#include <vector>
#include <vector>

using namespace std;

// Fixed size dimensions of array or collections stored in the TTree if any.

class RecoLevel2 {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   vector<float>   *mc_generator_weights;
   Float_t         weight_mc;
   Float_t         weight_pileup;
   Float_t         weight_leptonSF;
   Float_t         weight_photonSF;
   Float_t         weight_bTagSF_MV2c10_77;
   Float_t         weight_bTagSF_MV2c10_85;
   Float_t         weight_bTagSF_MV2c10_70;
   Float_t         weight_bTagSF_MV2c10_Continuous;
   Float_t         weight_jvt;
   Float_t         weight_pileup_UP;
   Float_t         weight_pileup_DOWN;
   Float_t         weight_leptonSF_EL_SF_Trigger_UP;
   Float_t         weight_leptonSF_EL_SF_Trigger_DOWN;
   Float_t         weight_leptonSF_EL_SF_Reco_UP;
   Float_t         weight_leptonSF_EL_SF_Reco_DOWN;
   Float_t         weight_leptonSF_EL_SF_ID_UP;
   Float_t         weight_leptonSF_EL_SF_ID_DOWN;
   Float_t         weight_leptonSF_EL_SF_Isol_UP;
   Float_t         weight_leptonSF_EL_SF_Isol_DOWN;
   Float_t         weight_leptonSF_MU_SF_Trigger_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_Trigger_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Trigger_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_Trigger_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_DOWN;
   Float_t         weight_indiv_SF_EL_Trigger;
   Float_t         weight_indiv_SF_EL_Trigger_UP;
   Float_t         weight_indiv_SF_EL_Trigger_DOWN;
   Float_t         weight_indiv_SF_EL_Reco;
   Float_t         weight_indiv_SF_EL_Reco_UP;
   Float_t         weight_indiv_SF_EL_Reco_DOWN;
   Float_t         weight_indiv_SF_EL_ID;
   Float_t         weight_indiv_SF_EL_ID_UP;
   Float_t         weight_indiv_SF_EL_ID_DOWN;
   Float_t         weight_indiv_SF_EL_Isol;
   Float_t         weight_indiv_SF_EL_Isol_UP;
   Float_t         weight_indiv_SF_EL_Isol_DOWN;
   Float_t         weight_indiv_SF_EL_ChargeID;
   Float_t         weight_indiv_SF_EL_ChargeID_UP;
   Float_t         weight_indiv_SF_EL_ChargeID_DOWN;
   Float_t         weight_indiv_SF_EL_ChargeMisID;
   Float_t         weight_indiv_SF_EL_ChargeMisID_STAT_UP;
   Float_t         weight_indiv_SF_EL_ChargeMisID_STAT_DOWN;
   Float_t         weight_indiv_SF_EL_ChargeMisID_SYST_UP;
   Float_t         weight_indiv_SF_EL_ChargeMisID_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_Trigger;
   Float_t         weight_indiv_SF_MU_Trigger_STAT_UP;
   Float_t         weight_indiv_SF_MU_Trigger_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_Trigger_SYST_UP;
   Float_t         weight_indiv_SF_MU_Trigger_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_ID;
   Float_t         weight_indiv_SF_MU_ID_STAT_UP;
   Float_t         weight_indiv_SF_MU_ID_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_ID_SYST_UP;
   Float_t         weight_indiv_SF_MU_ID_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_ID_STAT_LOWPT_UP;
   Float_t         weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN;
   Float_t         weight_indiv_SF_MU_ID_SYST_LOWPT_UP;
   Float_t         weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN;
   Float_t         weight_indiv_SF_MU_Isol;
   Float_t         weight_indiv_SF_MU_Isol_STAT_UP;
   Float_t         weight_indiv_SF_MU_Isol_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_Isol_SYST_UP;
   Float_t         weight_indiv_SF_MU_Isol_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_TTVA;
   Float_t         weight_indiv_SF_MU_TTVA_STAT_UP;
   Float_t         weight_indiv_SF_MU_TTVA_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_TTVA_SYST_UP;
   Float_t         weight_indiv_SF_MU_TTVA_SYST_DOWN;
   Float_t         weight_photonSF_ID_UP;
   Float_t         weight_photonSF_ID_DOWN;
   Float_t         weight_photonSF_effIso;
   Float_t         weight_photonSF_effLowPtIso_UP;
   Float_t         weight_photonSF_effLowPtIso_DOWN;
   Float_t         weight_photonSF_effTrkIso_UP;
   Float_t         weight_photonSF_effTrkIso_DOWN;
   Float_t         weight_jvt_UP;
   Float_t         weight_jvt_DOWN;
   vector<float>   *weight_bTagSF_MV2c10_77_eigenvars_B_up;
   vector<float>   *weight_bTagSF_MV2c10_77_eigenvars_C_up;
   vector<float>   *weight_bTagSF_MV2c10_77_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_MV2c10_77_eigenvars_B_down;
   vector<float>   *weight_bTagSF_MV2c10_77_eigenvars_C_down;
   vector<float>   *weight_bTagSF_MV2c10_77_eigenvars_Light_down;
   Float_t         weight_bTagSF_MV2c10_77_extrapolation_up;
   Float_t         weight_bTagSF_MV2c10_77_extrapolation_down;
   Float_t         weight_bTagSF_MV2c10_77_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_MV2c10_77_extrapolation_from_charm_down;
   vector<float>   *weight_bTagSF_MV2c10_85_eigenvars_B_up;
   vector<float>   *weight_bTagSF_MV2c10_85_eigenvars_C_up;
   vector<float>   *weight_bTagSF_MV2c10_85_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_MV2c10_85_eigenvars_B_down;
   vector<float>   *weight_bTagSF_MV2c10_85_eigenvars_C_down;
   vector<float>   *weight_bTagSF_MV2c10_85_eigenvars_Light_down;
   Float_t         weight_bTagSF_MV2c10_85_extrapolation_up;
   Float_t         weight_bTagSF_MV2c10_85_extrapolation_down;
   Float_t         weight_bTagSF_MV2c10_85_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_MV2c10_85_extrapolation_from_charm_down;
   vector<float>   *weight_bTagSF_MV2c10_70_eigenvars_B_up;
   vector<float>   *weight_bTagSF_MV2c10_70_eigenvars_C_up;
   vector<float>   *weight_bTagSF_MV2c10_70_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_MV2c10_70_eigenvars_B_down;
   vector<float>   *weight_bTagSF_MV2c10_70_eigenvars_C_down;
   vector<float>   *weight_bTagSF_MV2c10_70_eigenvars_Light_down;
   Float_t         weight_bTagSF_MV2c10_70_extrapolation_up;
   Float_t         weight_bTagSF_MV2c10_70_extrapolation_down;
   Float_t         weight_bTagSF_MV2c10_70_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_MV2c10_70_extrapolation_from_charm_down;
   vector<float>   *weight_bTagSF_MV2c10_Continuous_eigenvars_B_up;
   vector<float>   *weight_bTagSF_MV2c10_Continuous_eigenvars_C_up;
   vector<float>   *weight_bTagSF_MV2c10_Continuous_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_MV2c10_Continuous_eigenvars_B_down;
   vector<float>   *weight_bTagSF_MV2c10_Continuous_eigenvars_C_down;
   vector<float>   *weight_bTagSF_MV2c10_Continuous_eigenvars_Light_down;
   ULong64_t       eventNumber;
   UInt_t          runNumber;
   UInt_t          randomRunNumber;
   UInt_t          mcChannelNumber;
   Float_t         mu;
   UInt_t          backgroundFlags;
   UInt_t          hasBadMuon;
   vector<float>   *el_pt;
   vector<float>   *el_eta;
   vector<float>   *el_cl_eta;
   vector<float>   *el_phi;
   vector<float>   *el_e;
   vector<float>   *el_charge;
   vector<float>   *el_topoetcone20;
   vector<float>   *el_ptvarcone20;
   vector<char>    *el_CF;
   vector<float>   *el_d0sig;
   vector<float>   *el_delta_z0_sintheta;
   vector<int>     *el_true_type;
   vector<int>     *el_true_origin;
   vector<int>     *el_true_firstEgMotherTruthType;
   vector<int>     *el_true_firstEgMotherTruthOrigin;
   vector<int>     *el_true_firstEgMotherPdgId;
   vector<char>    *el_true_isPrompt;
   vector<char>    *el_true_isChargeFl;
   vector<float>   *mu_pt;
   vector<float>   *mu_eta;
   vector<float>   *mu_phi;
   vector<float>   *mu_e;
   vector<float>   *mu_charge;
   vector<float>   *mu_topoetcone20;
   vector<float>   *mu_ptvarcone30;
   vector<float>   *mu_d0sig;
   vector<float>   *mu_delta_z0_sintheta;
   vector<int>     *mu_true_type;
   vector<int>     *mu_true_origin;
   vector<char>    *mu_true_isPrompt;
   vector<float>   *ph_pt;
   vector<float>   *ph_eta;
   vector<float>   *ph_phi;
   vector<float>   *ph_e;
   vector<float>   *ph_iso;
   vector<float>   *jet_pt;
   vector<float>   *jet_eta;
   vector<float>   *jet_phi;
   vector<float>   *jet_e;
   vector<float>   *jet_mv2c00;
   vector<float>   *jet_mv2c10;
   vector<float>   *jet_mv2c20;
   vector<float>   *jet_ip3dsv1;
   vector<float>   *jet_jvt;
   vector<char>    *jet_passfjvt;
   vector<int>     *jet_truthflav;
   vector<int>     *jet_truthPartonLabel;
   vector<char>    *jet_isTrueHS;
   vector<int>     *jet_truthflavExtended;
   vector<char>    *jet_isbtagged_MV2c10_77;
   vector<char>    *jet_isbtagged_MV2c10_85;
   vector<char>    *jet_isbtagged_MV2c10_70;
   vector<int>     *jet_tagWeightBin_MV2c10_Continuous;
   vector<float>   *jet_MV2c10mu;
   vector<float>   *jet_MV2c10rnn;
   vector<float>   *jet_DL1;
   vector<float>   *jet_DL1mu;
   vector<float>   *jet_DL1rnn;
   vector<float>   *jet_MV2cl100;
   vector<float>   *jet_MV2c100;
   vector<float>   *jet_DL1_pu;
   vector<float>   *jet_DL1_pc;
   vector<float>   *jet_DL1_pb;
   vector<float>   *jet_DL1mu_pu;
   vector<float>   *jet_DL1mu_pc;
   vector<float>   *jet_DL1mu_pb;
   vector<float>   *jet_DL1rnn_pu;
   vector<float>   *jet_DL1rnn_pc;
   vector<float>   *jet_DL1rnn_pb;
   Float_t         met_met;
   Float_t         met_phi;
   Int_t           ejets_2015;
   Int_t           ejets_2016;
   Int_t           mujets_2015;
   Int_t           mujets_2016;
   Int_t           emu_2015;
   Int_t           emu_2016;
   Int_t           ee_2015;
   Int_t           ee_2016;
   Int_t           mumu_2015;
   Int_t           mumu_2016;
   Char_t          HLT_mu20_iloose_L1MU15;
   Char_t          HLT_e60_lhmedium_nod0;
   Char_t          HLT_e120_lhloose;
   Char_t          HLT_e24_lhmedium_L1EM20VH;
   Char_t          HLT_mu50;
   Char_t          HLT_e60_lhmedium;
   Char_t          HLT_mu26_ivarmedium;
   Char_t          HLT_e26_lhtight_nod0_ivarloose;
   Char_t          HLT_e140_lhloose_nod0;
   vector<char>    *el_trigMatch_HLT_e60_lhmedium_nod0;
   vector<char>    *el_trigMatch_HLT_e120_lhloose;
   vector<char>    *el_trigMatch_HLT_e24_lhmedium_L1EM20VH;
   vector<char>    *el_trigMatch_HLT_e60_lhmedium;
   vector<char>    *el_trigMatch_HLT_e26_lhtight_nod0_ivarloose;
   vector<char>    *el_trigMatch_HLT_e140_lhloose_nod0;
   vector<char>    *mu_trigMatch_HLT_mu26_ivarmedium;
   vector<char>    *mu_trigMatch_HLT_mu50;
   vector<char>    *mu_trigMatch_HLT_mu20_iloose_L1MU15;
   vector<float>   *lep_pdgid;
   Float_t         event_mll;
   Float_t         event_mwt;
   Float_t         event_HT;
   Int_t           event_njets;
   Int_t           event_nbjets70;
   Int_t           event_nbjets77;
   Int_t           event_nbjets85;
   Int_t           event_category;
   vector<int>     *ph_good_index;
   vector<char>    *ph_true_topancestor;
   vector<int>     *ph_true_ancestor;
   vector<int>     *ph_true_barcode;
   vector<int>     *ph_true_origin;
   vector<int>     *ph_true_pdgid;
   vector<int>     *ph_true_type;
   vector<float>   *ph_true_eta;
   vector<float>   *ph_true_phi;
   vector<float>   *ph_true_pt;
   vector<int>     *ph_conversion_type;
   vector<float>   *ph_drlph;
   vector<float>   *ph_mlph;
   vector<float>   *ph_mllph;
   vector<char>    *ph_id_loose;
   vector<char>    *ph_id_tight;
   vector<float>   *ph_iso_ptcone20;
   vector<float>   *ph_iso_ptcone30;
   vector<float>   *ph_iso_ptcone40;
   vector<float>   *ph_iso_ptvarcone20;
   vector<float>   *ph_iso_ptvarcone30;
   vector<float>   *ph_iso_ptvarcone40;
   vector<float>   *ph_iso_topoetcone20;
   vector<float>   *ph_iso_topoetcone30;
   vector<float>   *ph_iso_topoetcone40;
   vector<char>    *ph_iso_FCL;
   vector<char>    *ph_iso_FCT;
   vector<char>    *ph_iso_FCTCO;
   vector<float>   *ph_ss_deltaE;
   vector<float>   *ph_ss_e277;
   vector<float>   *ph_ss_emaxs1;
   vector<float>   *ph_ss_eratio;
   vector<float>   *ph_ss_f1;
   vector<float>   *ph_ss_fracs1;
   vector<float>   *ph_ss_reta;
   vector<float>   *ph_ss_rhad;
   vector<float>   *ph_ss_rhad1;
   vector<float>   *ph_ss_rphi;
   vector<float>   *ph_ss_weta1;
   vector<float>   *ph_ss_weta2;
   vector<float>   *ph_ss_wtots1;

   // List of branches
   TBranch        *b_mc_generator_weights;   //!
   TBranch        *b_weight_mc;   //!
   TBranch        *b_weight_pileup;   //!
   TBranch        *b_weight_leptonSF;   //!
   TBranch        *b_weight_photonSF;   //!
   TBranch        *b_weight_bTagSF_MV2c10_77;   //!
   TBranch        *b_weight_bTagSF_MV2c10_85;   //!
   TBranch        *b_weight_bTagSF_MV2c10_70;   //!
   TBranch        *b_weight_bTagSF_MV2c10_Continuous;   //!
   TBranch        *b_weight_jvt;   //!
   TBranch        *b_weight_pileup_UP;   //!
   TBranch        *b_weight_pileup_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Trigger_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Trigger_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_Trigger;   //!
   TBranch        *b_weight_indiv_SF_EL_Trigger_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_Trigger_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_Reco;   //!
   TBranch        *b_weight_indiv_SF_EL_Reco_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_Reco_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_ID;   //!
   TBranch        *b_weight_indiv_SF_EL_ID_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_ID_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_Isol;   //!
   TBranch        *b_weight_indiv_SF_EL_Isol_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_Isol_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeID;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeID_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeID_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_Trigger;   //!
   TBranch        *b_weight_indiv_SF_MU_Trigger_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_Trigger_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_Trigger_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_Trigger_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_LOWPT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_LOWPT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_SYST_DOWN;   //!
   TBranch        *b_weight_photonSF_ID_UP;   //!
   TBranch        *b_weight_photonSF_ID_DOWN;   //!
   TBranch        *b_weight_photonSF_effIso;   //!
   TBranch        *b_weight_photonSF_effLowPtIso_UP;   //!
   TBranch        *b_weight_photonSF_effLowPtIso_DOWN;   //!
   TBranch        *b_weight_photonSF_effTrkIso_UP;   //!
   TBranch        *b_weight_photonSF_effTrkIso_DOWN;   //!
   TBranch        *b_weight_jvt_UP;   //!
   TBranch        *b_weight_jvt_DOWN;   //!
   TBranch        *b_weight_bTagSF_MV2c10_77_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_MV2c10_77_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_MV2c10_77_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_MV2c10_77_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_MV2c10_77_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_MV2c10_77_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_MV2c10_77_extrapolation_up;   //!
   TBranch        *b_weight_bTagSF_MV2c10_77_extrapolation_down;   //!
   TBranch        *b_weight_bTagSF_MV2c10_77_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_MV2c10_77_extrapolation_from_charm_down;   //!
   TBranch        *b_weight_bTagSF_MV2c10_85_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_MV2c10_85_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_MV2c10_85_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_MV2c10_85_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_MV2c10_85_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_MV2c10_85_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_MV2c10_85_extrapolation_up;   //!
   TBranch        *b_weight_bTagSF_MV2c10_85_extrapolation_down;   //!
   TBranch        *b_weight_bTagSF_MV2c10_85_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_MV2c10_85_extrapolation_from_charm_down;   //!
   TBranch        *b_weight_bTagSF_MV2c10_70_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_MV2c10_70_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_MV2c10_70_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_MV2c10_70_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_MV2c10_70_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_MV2c10_70_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_MV2c10_70_extrapolation_up;   //!
   TBranch        *b_weight_bTagSF_MV2c10_70_extrapolation_down;   //!
   TBranch        *b_weight_bTagSF_MV2c10_70_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_MV2c10_70_extrapolation_from_charm_down;   //!
   TBranch        *b_weight_bTagSF_MV2c10_Continuous_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_MV2c10_Continuous_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_MV2c10_Continuous_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_MV2c10_Continuous_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_MV2c10_Continuous_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_MV2c10_Continuous_eigenvars_Light_down;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_randomRunNumber;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_backgroundFlags;   //!
   TBranch        *b_hasBadMuon;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_cl_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_e;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_el_topoetcone20;   //!
   TBranch        *b_el_ptvarcone20;   //!
   TBranch        *b_el_CF;   //!
   TBranch        *b_el_d0sig;   //!
   TBranch        *b_el_delta_z0_sintheta;   //!
   TBranch        *b_el_true_type;   //!
   TBranch        *b_el_true_origin;   //!
   TBranch        *b_el_true_firstEgMotherTruthType;   //!
   TBranch        *b_el_true_firstEgMotherTruthOrigin;   //!
   TBranch        *b_el_true_firstEgMotherPdgId;   //!
   TBranch        *b_el_true_isPrompt;   //!
   TBranch        *b_el_true_isChargeFl;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_e;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_mu_topoetcone20;   //!
   TBranch        *b_mu_ptvarcone30;   //!
   TBranch        *b_mu_d0sig;   //!
   TBranch        *b_mu_delta_z0_sintheta;   //!
   TBranch        *b_mu_true_type;   //!
   TBranch        *b_mu_true_origin;   //!
   TBranch        *b_mu_true_isPrompt;   //!
   TBranch        *b_ph_pt;   //!
   TBranch        *b_ph_eta;   //!
   TBranch        *b_ph_phi;   //!
   TBranch        *b_ph_e;   //!
   TBranch        *b_ph_iso;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_e;   //!
   TBranch        *b_jet_mv2c00;   //!
   TBranch        *b_jet_mv2c10;   //!
   TBranch        *b_jet_mv2c20;   //!
   TBranch        *b_jet_ip3dsv1;   //!
   TBranch        *b_jet_jvt;   //!
   TBranch        *b_jet_passfjvt;   //!
   TBranch        *b_jet_truthflav;   //!
   TBranch        *b_jet_truthPartonLabel;   //!
   TBranch        *b_jet_isTrueHS;   //!
   TBranch        *b_jet_truthflavExtended;   //!
   TBranch        *b_jet_isbtagged_MV2c10_77;   //!
   TBranch        *b_jet_isbtagged_MV2c10_85;   //!
   TBranch        *b_jet_isbtagged_MV2c10_70;   //!
   TBranch        *b_jet_tagWeightBin_MV2c10_Continuous;   //!
   TBranch        *b_jet_MV2c10mu;   //!
   TBranch        *b_jet_MV2c10rnn;   //!
   TBranch        *b_jet_DL1;   //!
   TBranch        *b_jet_DL1mu;   //!
   TBranch        *b_jet_DL1rnn;   //!
   TBranch        *b_jet_MV2cl100;   //!
   TBranch        *b_jet_MV2c100;   //!
   TBranch        *b_jet_DL1_pu;   //!
   TBranch        *b_jet_DL1_pc;   //!
   TBranch        *b_jet_DL1_pb;   //!
   TBranch        *b_jet_DL1mu_pu;   //!
   TBranch        *b_jet_DL1mu_pc;   //!
   TBranch        *b_jet_DL1mu_pb;   //!
   TBranch        *b_jet_DL1rnn_pu;   //!
   TBranch        *b_jet_DL1rnn_pc;   //!
   TBranch        *b_jet_DL1rnn_pb;   //!
   TBranch        *b_met_met;   //!
   TBranch        *b_met_phi;   //!
   TBranch        *b_ejets_2015;   //!
   TBranch        *b_ejets_2016;   //!
   TBranch        *b_mujets_2015;   //!
   TBranch        *b_mujets_2016;   //!
   TBranch        *b_emu_2015;   //!
   TBranch        *b_emu_2016;   //!
   TBranch        *b_ee_2015;   //!
   TBranch        *b_ee_2016;   //!
   TBranch        *b_mumu_2015;   //!
   TBranch        *b_mumu_2016;   //!
   TBranch        *b_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_HLT_e120_lhloose;   //!
   TBranch        *b_HLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_HLT_mu50;   //!
   TBranch        *b_HLT_e60_lhmedium;   //!
   TBranch        *b_HLT_mu26_ivarmedium;   //!
   TBranch        *b_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_el_trigMatch_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_el_trigMatch_HLT_e120_lhloose;   //!
   TBranch        *b_el_trigMatch_HLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_el_trigMatch_HLT_e60_lhmedium;   //!
   TBranch        *b_el_trigMatch_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_el_trigMatch_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_mu_trigMatch_HLT_mu26_ivarmedium;   //!
   TBranch        *b_mu_trigMatch_HLT_mu50;   //!
   TBranch        *b_mu_trigMatch_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_lep_pdgid;   //!
   TBranch        *b_event_mll;   //!
   TBranch        *b_event_mwt;   //!
   TBranch        *b_event_HT;   //!
   TBranch        *b_event_njets;   //!
   TBranch        *b_event_nbjets70;   //!
   TBranch        *b_event_nbjets77;   //!
   TBranch        *b_event_nbjets85;   //!
   TBranch        *b_event_category;   //!
   TBranch        *b_ph_good_index;   //!
   TBranch        *b_ph_true_topancestor;   //!
   TBranch        *b_ph_true_ancestor;   //!
   TBranch        *b_ph_true_barcode;   //!
   TBranch        *b_ph_true_origin;   //!
   TBranch        *b_ph_true_pdgid;   //!
   TBranch        *b_ph_true_type;   //!
   TBranch        *b_ph_true_eta;   //!
   TBranch        *b_ph_true_phi;   //!
   TBranch        *b_ph_true_pt;   //!
   TBranch        *b_ph_conversion_type;   //!
   TBranch        *b_ph_drlph;   //!
   TBranch        *b_ph_mlph;   //!
   TBranch        *b_ph_mllph;   //!
   TBranch        *b_ph_id_loose;   //!
   TBranch        *b_ph_id_tight;   //!
   TBranch        *b_ph_iso_ptcone20;   //!
   TBranch        *b_ph_iso_ptcone30;   //!
   TBranch        *b_ph_iso_ptcone40;   //!
   TBranch        *b_ph_iso_ptvarcone20;   //!
   TBranch        *b_ph_iso_ptvarcone30;   //!
   TBranch        *b_ph_iso_ptvarcone40;   //!
   TBranch        *b_ph_iso_topoetcone20;   //!
   TBranch        *b_ph_iso_topoetcone30;   //!
   TBranch        *b_ph_iso_topoetcone40;   //!
   TBranch        *b_ph_iso_FCL;   //!
   TBranch        *b_ph_iso_FCT;   //!
   TBranch        *b_ph_iso_FCTCO;   //!
   TBranch        *b_ph_ss_deltaE;   //!
   TBranch        *b_ph_ss_e277;   //!
   TBranch        *b_ph_ss_emaxs1;   //!
   TBranch        *b_ph_ss_eratio;   //!
   TBranch        *b_ph_ss_f1;   //!
   TBranch        *b_ph_ss_fracs1;   //!
   TBranch        *b_ph_ss_reta;   //!
   TBranch        *b_ph_ss_rhad;   //!
   TBranch        *b_ph_ss_rhad1;   //!
   TBranch        *b_ph_ss_rphi;   //!
   TBranch        *b_ph_ss_weta1;   //!
   TBranch        *b_ph_ss_weta2;   //!
   TBranch        *b_ph_ss_wtots1;   //!

   RecoLevel2(TTree *tree=0);
   virtual ~RecoLevel2();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef RecoLevel2_cxx
RecoLevel2::RecoLevel2(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/afs/cern.ch/work/a/arej/public/output_410389_new.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/afs/cern.ch/work/a/arej/public/output_410389_new.root");
      }
      f->GetObject("nominal",tree);

   }
   Init(tree);
}

RecoLevel2::~RecoLevel2()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t RecoLevel2::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t RecoLevel2::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void RecoLevel2::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   mc_generator_weights = 0;
   weight_bTagSF_MV2c10_77_eigenvars_B_up = 0;
   weight_bTagSF_MV2c10_77_eigenvars_C_up = 0;
   weight_bTagSF_MV2c10_77_eigenvars_Light_up = 0;
   weight_bTagSF_MV2c10_77_eigenvars_B_down = 0;
   weight_bTagSF_MV2c10_77_eigenvars_C_down = 0;
   weight_bTagSF_MV2c10_77_eigenvars_Light_down = 0;
   weight_bTagSF_MV2c10_85_eigenvars_B_up = 0;
   weight_bTagSF_MV2c10_85_eigenvars_C_up = 0;
   weight_bTagSF_MV2c10_85_eigenvars_Light_up = 0;
   weight_bTagSF_MV2c10_85_eigenvars_B_down = 0;
   weight_bTagSF_MV2c10_85_eigenvars_C_down = 0;
   weight_bTagSF_MV2c10_85_eigenvars_Light_down = 0;
   weight_bTagSF_MV2c10_70_eigenvars_B_up = 0;
   weight_bTagSF_MV2c10_70_eigenvars_C_up = 0;
   weight_bTagSF_MV2c10_70_eigenvars_Light_up = 0;
   weight_bTagSF_MV2c10_70_eigenvars_B_down = 0;
   weight_bTagSF_MV2c10_70_eigenvars_C_down = 0;
   weight_bTagSF_MV2c10_70_eigenvars_Light_down = 0;
   weight_bTagSF_MV2c10_Continuous_eigenvars_B_up = 0;
   weight_bTagSF_MV2c10_Continuous_eigenvars_C_up = 0;
   weight_bTagSF_MV2c10_Continuous_eigenvars_Light_up = 0;
   weight_bTagSF_MV2c10_Continuous_eigenvars_B_down = 0;
   weight_bTagSF_MV2c10_Continuous_eigenvars_C_down = 0;
   weight_bTagSF_MV2c10_Continuous_eigenvars_Light_down = 0;
   el_pt = 0;
   el_eta = 0;
   el_cl_eta = 0;
   el_phi = 0;
   el_e = 0;
   el_charge = 0;
   el_topoetcone20 = 0;
   el_ptvarcone20 = 0;
   el_CF = 0;
   el_d0sig = 0;
   el_delta_z0_sintheta = 0;
   el_true_type = 0;
   el_true_origin = 0;
   el_true_firstEgMotherTruthType = 0;
   el_true_firstEgMotherTruthOrigin = 0;
   el_true_firstEgMotherPdgId = 0;
   el_true_isPrompt = 0;
   el_true_isChargeFl = 0;
   mu_pt = 0;
   mu_eta = 0;
   mu_phi = 0;
   mu_e = 0;
   mu_charge = 0;
   mu_topoetcone20 = 0;
   mu_ptvarcone30 = 0;
   mu_d0sig = 0;
   mu_delta_z0_sintheta = 0;
   mu_true_type = 0;
   mu_true_origin = 0;
   mu_true_isPrompt = 0;
   ph_pt = 0;
   ph_eta = 0;
   ph_phi = 0;
   ph_e = 0;
   ph_iso = 0;
   jet_pt = 0;
   jet_eta = 0;
   jet_phi = 0;
   jet_e = 0;
   jet_mv2c00 = 0;
   jet_mv2c10 = 0;
   jet_mv2c20 = 0;
   jet_ip3dsv1 = 0;
   jet_jvt = 0;
   jet_passfjvt = 0;
   jet_truthflav = 0;
   jet_truthPartonLabel = 0;
   jet_isTrueHS = 0;
   jet_truthflavExtended = 0;
   jet_isbtagged_MV2c10_77 = 0;
   jet_isbtagged_MV2c10_85 = 0;
   jet_isbtagged_MV2c10_70 = 0;
   jet_tagWeightBin_MV2c10_Continuous = 0;
   jet_MV2c10mu = 0;
   jet_MV2c10rnn = 0;
   jet_DL1 = 0;
   jet_DL1mu = 0;
   jet_DL1rnn = 0;
   jet_MV2cl100 = 0;
   jet_MV2c100 = 0;
   jet_DL1_pu = 0;
   jet_DL1_pc = 0;
   jet_DL1_pb = 0;
   jet_DL1mu_pu = 0;
   jet_DL1mu_pc = 0;
   jet_DL1mu_pb = 0;
   jet_DL1rnn_pu = 0;
   jet_DL1rnn_pc = 0;
   jet_DL1rnn_pb = 0;
   el_trigMatch_HLT_e60_lhmedium_nod0 = 0;
   el_trigMatch_HLT_e120_lhloose = 0;
   el_trigMatch_HLT_e24_lhmedium_L1EM20VH = 0;
   el_trigMatch_HLT_e60_lhmedium = 0;
   el_trigMatch_HLT_e26_lhtight_nod0_ivarloose = 0;
   el_trigMatch_HLT_e140_lhloose_nod0 = 0;
   mu_trigMatch_HLT_mu26_ivarmedium = 0;
   mu_trigMatch_HLT_mu50 = 0;
   mu_trigMatch_HLT_mu20_iloose_L1MU15 = 0;
   lep_pdgid = 0;
   ph_good_index = 0;
   ph_true_topancestor = 0;
   ph_true_ancestor = 0;
   ph_true_barcode = 0;
   ph_true_origin = 0;
   ph_true_pdgid = 0;
   ph_true_type = 0;
   ph_true_eta = 0;
   ph_true_phi = 0;
   ph_true_pt = 0;
   ph_conversion_type = 0;
   ph_drlph = 0;
   ph_mlph = 0;
   ph_mllph = 0;
   ph_id_loose = 0;
   ph_id_tight = 0;
   ph_iso_ptcone20 = 0;
   ph_iso_ptcone30 = 0;
   ph_iso_ptcone40 = 0;
   ph_iso_ptvarcone20 = 0;
   ph_iso_ptvarcone30 = 0;
   ph_iso_ptvarcone40 = 0;
   ph_iso_topoetcone20 = 0;
   ph_iso_topoetcone30 = 0;
   ph_iso_topoetcone40 = 0;
   ph_iso_FCL = 0;
   ph_iso_FCT = 0;
   ph_iso_FCTCO = 0;
   ph_ss_deltaE = 0;
   ph_ss_e277 = 0;
   ph_ss_emaxs1 = 0;
   ph_ss_eratio = 0;
   ph_ss_f1 = 0;
   ph_ss_fracs1 = 0;
   ph_ss_reta = 0;
   ph_ss_rhad = 0;
   ph_ss_rhad1 = 0;
   ph_ss_rphi = 0;
   ph_ss_weta1 = 0;
   ph_ss_weta2 = 0;
   ph_ss_wtots1 = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("mc_generator_weights", &mc_generator_weights, &b_mc_generator_weights);
   fChain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
   fChain->SetBranchAddress("weight_pileup", &weight_pileup, &b_weight_pileup);
   fChain->SetBranchAddress("weight_leptonSF", &weight_leptonSF, &b_weight_leptonSF);
   fChain->SetBranchAddress("weight_photonSF", &weight_photonSF, &b_weight_photonSF);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_77", &weight_bTagSF_MV2c10_77, &b_weight_bTagSF_MV2c10_77);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_85", &weight_bTagSF_MV2c10_85, &b_weight_bTagSF_MV2c10_85);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_70", &weight_bTagSF_MV2c10_70, &b_weight_bTagSF_MV2c10_70);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_Continuous", &weight_bTagSF_MV2c10_Continuous, &b_weight_bTagSF_MV2c10_Continuous);
   fChain->SetBranchAddress("weight_jvt", &weight_jvt, &b_weight_jvt);
   fChain->SetBranchAddress("weight_pileup_UP", &weight_pileup_UP, &b_weight_pileup_UP);
   fChain->SetBranchAddress("weight_pileup_DOWN", &weight_pileup_DOWN, &b_weight_pileup_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Trigger_UP", &weight_leptonSF_EL_SF_Trigger_UP, &b_weight_leptonSF_EL_SF_Trigger_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Trigger_DOWN", &weight_leptonSF_EL_SF_Trigger_DOWN, &b_weight_leptonSF_EL_SF_Trigger_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_UP", &weight_leptonSF_EL_SF_Reco_UP, &b_weight_leptonSF_EL_SF_Reco_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_DOWN", &weight_leptonSF_EL_SF_Reco_DOWN, &b_weight_leptonSF_EL_SF_Reco_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_UP", &weight_leptonSF_EL_SF_ID_UP, &b_weight_leptonSF_EL_SF_ID_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_DOWN", &weight_leptonSF_EL_SF_ID_DOWN, &b_weight_leptonSF_EL_SF_ID_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_UP", &weight_leptonSF_EL_SF_Isol_UP, &b_weight_leptonSF_EL_SF_Isol_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_DOWN", &weight_leptonSF_EL_SF_Isol_DOWN, &b_weight_leptonSF_EL_SF_Isol_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_STAT_UP", &weight_leptonSF_MU_SF_Trigger_STAT_UP, &b_weight_leptonSF_MU_SF_Trigger_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_STAT_DOWN", &weight_leptonSF_MU_SF_Trigger_STAT_DOWN, &b_weight_leptonSF_MU_SF_Trigger_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_SYST_UP", &weight_leptonSF_MU_SF_Trigger_SYST_UP, &b_weight_leptonSF_MU_SF_Trigger_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_SYST_DOWN", &weight_leptonSF_MU_SF_Trigger_SYST_DOWN, &b_weight_leptonSF_MU_SF_Trigger_SYST_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_UP", &weight_leptonSF_MU_SF_ID_STAT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_UP", &weight_leptonSF_MU_SF_ID_SYST_UP, &b_weight_leptonSF_MU_SF_ID_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_DOWN", &weight_leptonSF_MU_SF_ID_SYST_DOWN, &b_weight_leptonSF_MU_SF_ID_SYST_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_UP", &weight_leptonSF_MU_SF_Isol_STAT_UP, &b_weight_leptonSF_MU_SF_Isol_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_DOWN", &weight_leptonSF_MU_SF_Isol_STAT_DOWN, &b_weight_leptonSF_MU_SF_Isol_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_UP", &weight_leptonSF_MU_SF_Isol_SYST_UP, &b_weight_leptonSF_MU_SF_Isol_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_DOWN", &weight_leptonSF_MU_SF_Isol_SYST_DOWN, &b_weight_leptonSF_MU_SF_Isol_SYST_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_UP", &weight_leptonSF_MU_SF_TTVA_STAT_UP, &b_weight_leptonSF_MU_SF_TTVA_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_DOWN", &weight_leptonSF_MU_SF_TTVA_STAT_DOWN, &b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_UP", &weight_leptonSF_MU_SF_TTVA_SYST_UP, &b_weight_leptonSF_MU_SF_TTVA_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_DOWN", &weight_leptonSF_MU_SF_TTVA_SYST_DOWN, &b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Trigger", &weight_indiv_SF_EL_Trigger, &b_weight_indiv_SF_EL_Trigger);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Trigger_UP", &weight_indiv_SF_EL_Trigger_UP, &b_weight_indiv_SF_EL_Trigger_UP);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Trigger_DOWN", &weight_indiv_SF_EL_Trigger_DOWN, &b_weight_indiv_SF_EL_Trigger_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Reco", &weight_indiv_SF_EL_Reco, &b_weight_indiv_SF_EL_Reco);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Reco_UP", &weight_indiv_SF_EL_Reco_UP, &b_weight_indiv_SF_EL_Reco_UP);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Reco_DOWN", &weight_indiv_SF_EL_Reco_DOWN, &b_weight_indiv_SF_EL_Reco_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ID", &weight_indiv_SF_EL_ID, &b_weight_indiv_SF_EL_ID);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ID_UP", &weight_indiv_SF_EL_ID_UP, &b_weight_indiv_SF_EL_ID_UP);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ID_DOWN", &weight_indiv_SF_EL_ID_DOWN, &b_weight_indiv_SF_EL_ID_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Isol", &weight_indiv_SF_EL_Isol, &b_weight_indiv_SF_EL_Isol);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Isol_UP", &weight_indiv_SF_EL_Isol_UP, &b_weight_indiv_SF_EL_Isol_UP);
   fChain->SetBranchAddress("weight_indiv_SF_EL_Isol_DOWN", &weight_indiv_SF_EL_Isol_DOWN, &b_weight_indiv_SF_EL_Isol_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeID", &weight_indiv_SF_EL_ChargeID, &b_weight_indiv_SF_EL_ChargeID);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeID_UP", &weight_indiv_SF_EL_ChargeID_UP, &b_weight_indiv_SF_EL_ChargeID_UP);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeID_DOWN", &weight_indiv_SF_EL_ChargeID_DOWN, &b_weight_indiv_SF_EL_ChargeID_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID", &weight_indiv_SF_EL_ChargeMisID, &b_weight_indiv_SF_EL_ChargeMisID);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_STAT_UP", &weight_indiv_SF_EL_ChargeMisID_STAT_UP, &b_weight_indiv_SF_EL_ChargeMisID_STAT_UP);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_STAT_DOWN", &weight_indiv_SF_EL_ChargeMisID_STAT_DOWN, &b_weight_indiv_SF_EL_ChargeMisID_STAT_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_SYST_UP", &weight_indiv_SF_EL_ChargeMisID_SYST_UP, &b_weight_indiv_SF_EL_ChargeMisID_SYST_UP);
   fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_SYST_DOWN", &weight_indiv_SF_EL_ChargeMisID_SYST_DOWN, &b_weight_indiv_SF_EL_ChargeMisID_SYST_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_Trigger", &weight_indiv_SF_MU_Trigger, &b_weight_indiv_SF_MU_Trigger);
   fChain->SetBranchAddress("weight_indiv_SF_MU_Trigger_STAT_UP", &weight_indiv_SF_MU_Trigger_STAT_UP, &b_weight_indiv_SF_MU_Trigger_STAT_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_Trigger_STAT_DOWN", &weight_indiv_SF_MU_Trigger_STAT_DOWN, &b_weight_indiv_SF_MU_Trigger_STAT_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_Trigger_SYST_UP", &weight_indiv_SF_MU_Trigger_SYST_UP, &b_weight_indiv_SF_MU_Trigger_SYST_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_Trigger_SYST_DOWN", &weight_indiv_SF_MU_Trigger_SYST_DOWN, &b_weight_indiv_SF_MU_Trigger_SYST_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID", &weight_indiv_SF_MU_ID, &b_weight_indiv_SF_MU_ID);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_UP", &weight_indiv_SF_MU_ID_STAT_UP, &b_weight_indiv_SF_MU_ID_STAT_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_DOWN", &weight_indiv_SF_MU_ID_STAT_DOWN, &b_weight_indiv_SF_MU_ID_STAT_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_UP", &weight_indiv_SF_MU_ID_SYST_UP, &b_weight_indiv_SF_MU_ID_SYST_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_DOWN", &weight_indiv_SF_MU_ID_SYST_DOWN, &b_weight_indiv_SF_MU_ID_SYST_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_LOWPT_UP", &weight_indiv_SF_MU_ID_STAT_LOWPT_UP, &b_weight_indiv_SF_MU_ID_STAT_LOWPT_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN", &weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN, &b_weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_LOWPT_UP", &weight_indiv_SF_MU_ID_SYST_LOWPT_UP, &b_weight_indiv_SF_MU_ID_SYST_LOWPT_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN", &weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN, &b_weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_Isol", &weight_indiv_SF_MU_Isol, &b_weight_indiv_SF_MU_Isol);
   fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_STAT_UP", &weight_indiv_SF_MU_Isol_STAT_UP, &b_weight_indiv_SF_MU_Isol_STAT_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_STAT_DOWN", &weight_indiv_SF_MU_Isol_STAT_DOWN, &b_weight_indiv_SF_MU_Isol_STAT_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_SYST_UP", &weight_indiv_SF_MU_Isol_SYST_UP, &b_weight_indiv_SF_MU_Isol_SYST_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_SYST_DOWN", &weight_indiv_SF_MU_Isol_SYST_DOWN, &b_weight_indiv_SF_MU_Isol_SYST_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA", &weight_indiv_SF_MU_TTVA, &b_weight_indiv_SF_MU_TTVA);
   fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_STAT_UP", &weight_indiv_SF_MU_TTVA_STAT_UP, &b_weight_indiv_SF_MU_TTVA_STAT_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_STAT_DOWN", &weight_indiv_SF_MU_TTVA_STAT_DOWN, &b_weight_indiv_SF_MU_TTVA_STAT_DOWN);
   fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_SYST_UP", &weight_indiv_SF_MU_TTVA_SYST_UP, &b_weight_indiv_SF_MU_TTVA_SYST_UP);
   fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_SYST_DOWN", &weight_indiv_SF_MU_TTVA_SYST_DOWN, &b_weight_indiv_SF_MU_TTVA_SYST_DOWN);
   fChain->SetBranchAddress("weight_photonSF_ID_UP", &weight_photonSF_ID_UP, &b_weight_photonSF_ID_UP);
   fChain->SetBranchAddress("weight_photonSF_ID_DOWN", &weight_photonSF_ID_DOWN, &b_weight_photonSF_ID_DOWN);
   fChain->SetBranchAddress("weight_photonSF_effIso", &weight_photonSF_effIso, &b_weight_photonSF_effIso);
   fChain->SetBranchAddress("weight_photonSF_effLowPtIso_UP", &weight_photonSF_effLowPtIso_UP, &b_weight_photonSF_effLowPtIso_UP);
   fChain->SetBranchAddress("weight_photonSF_effLowPtIso_DOWN", &weight_photonSF_effLowPtIso_DOWN, &b_weight_photonSF_effLowPtIso_DOWN);
   fChain->SetBranchAddress("weight_photonSF_effTrkIso_UP", &weight_photonSF_effTrkIso_UP, &b_weight_photonSF_effTrkIso_UP);
   fChain->SetBranchAddress("weight_photonSF_effTrkIso_DOWN", &weight_photonSF_effTrkIso_DOWN, &b_weight_photonSF_effTrkIso_DOWN);
   fChain->SetBranchAddress("weight_jvt_UP", &weight_jvt_UP, &b_weight_jvt_UP);
   fChain->SetBranchAddress("weight_jvt_DOWN", &weight_jvt_DOWN, &b_weight_jvt_DOWN);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_77_eigenvars_B_up", &weight_bTagSF_MV2c10_77_eigenvars_B_up, &b_weight_bTagSF_MV2c10_77_eigenvars_B_up);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_77_eigenvars_C_up", &weight_bTagSF_MV2c10_77_eigenvars_C_up, &b_weight_bTagSF_MV2c10_77_eigenvars_C_up);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_77_eigenvars_Light_up", &weight_bTagSF_MV2c10_77_eigenvars_Light_up, &b_weight_bTagSF_MV2c10_77_eigenvars_Light_up);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_77_eigenvars_B_down", &weight_bTagSF_MV2c10_77_eigenvars_B_down, &b_weight_bTagSF_MV2c10_77_eigenvars_B_down);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_77_eigenvars_C_down", &weight_bTagSF_MV2c10_77_eigenvars_C_down, &b_weight_bTagSF_MV2c10_77_eigenvars_C_down);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_77_eigenvars_Light_down", &weight_bTagSF_MV2c10_77_eigenvars_Light_down, &b_weight_bTagSF_MV2c10_77_eigenvars_Light_down);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_77_extrapolation_up", &weight_bTagSF_MV2c10_77_extrapolation_up, &b_weight_bTagSF_MV2c10_77_extrapolation_up);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_77_extrapolation_down", &weight_bTagSF_MV2c10_77_extrapolation_down, &b_weight_bTagSF_MV2c10_77_extrapolation_down);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_77_extrapolation_from_charm_up", &weight_bTagSF_MV2c10_77_extrapolation_from_charm_up, &b_weight_bTagSF_MV2c10_77_extrapolation_from_charm_up);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_77_extrapolation_from_charm_down", &weight_bTagSF_MV2c10_77_extrapolation_from_charm_down, &b_weight_bTagSF_MV2c10_77_extrapolation_from_charm_down);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_85_eigenvars_B_up", &weight_bTagSF_MV2c10_85_eigenvars_B_up, &b_weight_bTagSF_MV2c10_85_eigenvars_B_up);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_85_eigenvars_C_up", &weight_bTagSF_MV2c10_85_eigenvars_C_up, &b_weight_bTagSF_MV2c10_85_eigenvars_C_up);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_85_eigenvars_Light_up", &weight_bTagSF_MV2c10_85_eigenvars_Light_up, &b_weight_bTagSF_MV2c10_85_eigenvars_Light_up);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_85_eigenvars_B_down", &weight_bTagSF_MV2c10_85_eigenvars_B_down, &b_weight_bTagSF_MV2c10_85_eigenvars_B_down);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_85_eigenvars_C_down", &weight_bTagSF_MV2c10_85_eigenvars_C_down, &b_weight_bTagSF_MV2c10_85_eigenvars_C_down);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_85_eigenvars_Light_down", &weight_bTagSF_MV2c10_85_eigenvars_Light_down, &b_weight_bTagSF_MV2c10_85_eigenvars_Light_down);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_85_extrapolation_up", &weight_bTagSF_MV2c10_85_extrapolation_up, &b_weight_bTagSF_MV2c10_85_extrapolation_up);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_85_extrapolation_down", &weight_bTagSF_MV2c10_85_extrapolation_down, &b_weight_bTagSF_MV2c10_85_extrapolation_down);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_85_extrapolation_from_charm_up", &weight_bTagSF_MV2c10_85_extrapolation_from_charm_up, &b_weight_bTagSF_MV2c10_85_extrapolation_from_charm_up);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_85_extrapolation_from_charm_down", &weight_bTagSF_MV2c10_85_extrapolation_from_charm_down, &b_weight_bTagSF_MV2c10_85_extrapolation_from_charm_down);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_70_eigenvars_B_up", &weight_bTagSF_MV2c10_70_eigenvars_B_up, &b_weight_bTagSF_MV2c10_70_eigenvars_B_up);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_70_eigenvars_C_up", &weight_bTagSF_MV2c10_70_eigenvars_C_up, &b_weight_bTagSF_MV2c10_70_eigenvars_C_up);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_70_eigenvars_Light_up", &weight_bTagSF_MV2c10_70_eigenvars_Light_up, &b_weight_bTagSF_MV2c10_70_eigenvars_Light_up);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_70_eigenvars_B_down", &weight_bTagSF_MV2c10_70_eigenvars_B_down, &b_weight_bTagSF_MV2c10_70_eigenvars_B_down);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_70_eigenvars_C_down", &weight_bTagSF_MV2c10_70_eigenvars_C_down, &b_weight_bTagSF_MV2c10_70_eigenvars_C_down);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_70_eigenvars_Light_down", &weight_bTagSF_MV2c10_70_eigenvars_Light_down, &b_weight_bTagSF_MV2c10_70_eigenvars_Light_down);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_70_extrapolation_up", &weight_bTagSF_MV2c10_70_extrapolation_up, &b_weight_bTagSF_MV2c10_70_extrapolation_up);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_70_extrapolation_down", &weight_bTagSF_MV2c10_70_extrapolation_down, &b_weight_bTagSF_MV2c10_70_extrapolation_down);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_70_extrapolation_from_charm_up", &weight_bTagSF_MV2c10_70_extrapolation_from_charm_up, &b_weight_bTagSF_MV2c10_70_extrapolation_from_charm_up);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_70_extrapolation_from_charm_down", &weight_bTagSF_MV2c10_70_extrapolation_from_charm_down, &b_weight_bTagSF_MV2c10_70_extrapolation_from_charm_down);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_Continuous_eigenvars_B_up", &weight_bTagSF_MV2c10_Continuous_eigenvars_B_up, &b_weight_bTagSF_MV2c10_Continuous_eigenvars_B_up);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_Continuous_eigenvars_C_up", &weight_bTagSF_MV2c10_Continuous_eigenvars_C_up, &b_weight_bTagSF_MV2c10_Continuous_eigenvars_C_up);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_Continuous_eigenvars_Light_up", &weight_bTagSF_MV2c10_Continuous_eigenvars_Light_up, &b_weight_bTagSF_MV2c10_Continuous_eigenvars_Light_up);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_Continuous_eigenvars_B_down", &weight_bTagSF_MV2c10_Continuous_eigenvars_B_down, &b_weight_bTagSF_MV2c10_Continuous_eigenvars_B_down);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_Continuous_eigenvars_C_down", &weight_bTagSF_MV2c10_Continuous_eigenvars_C_down, &b_weight_bTagSF_MV2c10_Continuous_eigenvars_C_down);
   fChain->SetBranchAddress("weight_bTagSF_MV2c10_Continuous_eigenvars_Light_down", &weight_bTagSF_MV2c10_Continuous_eigenvars_Light_down, &b_weight_bTagSF_MV2c10_Continuous_eigenvars_Light_down);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("randomRunNumber", &randomRunNumber, &b_randomRunNumber);
   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("mu", &mu, &b_mu);
   fChain->SetBranchAddress("backgroundFlags", &backgroundFlags, &b_backgroundFlags);
   fChain->SetBranchAddress("hasBadMuon", &hasBadMuon, &b_hasBadMuon);
   fChain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
   fChain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
   fChain->SetBranchAddress("el_cl_eta", &el_cl_eta, &b_el_cl_eta);
   fChain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
   fChain->SetBranchAddress("el_e", &el_e, &b_el_e);
   fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
   fChain->SetBranchAddress("el_topoetcone20", &el_topoetcone20, &b_el_topoetcone20);
   fChain->SetBranchAddress("el_ptvarcone20", &el_ptvarcone20, &b_el_ptvarcone20);
   fChain->SetBranchAddress("el_CF", &el_CF, &b_el_CF);
   fChain->SetBranchAddress("el_d0sig", &el_d0sig, &b_el_d0sig);
   fChain->SetBranchAddress("el_delta_z0_sintheta", &el_delta_z0_sintheta, &b_el_delta_z0_sintheta);
   fChain->SetBranchAddress("el_true_type", &el_true_type, &b_el_true_type);
   fChain->SetBranchAddress("el_true_origin", &el_true_origin, &b_el_true_origin);
   fChain->SetBranchAddress("el_true_firstEgMotherTruthType", &el_true_firstEgMotherTruthType, &b_el_true_firstEgMotherTruthType);
   fChain->SetBranchAddress("el_true_firstEgMotherTruthOrigin", &el_true_firstEgMotherTruthOrigin, &b_el_true_firstEgMotherTruthOrigin);
   fChain->SetBranchAddress("el_true_firstEgMotherPdgId", &el_true_firstEgMotherPdgId, &b_el_true_firstEgMotherPdgId);
   fChain->SetBranchAddress("el_true_isPrompt", &el_true_isPrompt, &b_el_true_isPrompt);
   fChain->SetBranchAddress("el_true_isChargeFl", &el_true_isChargeFl, &b_el_true_isChargeFl);
   fChain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
   fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
   fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
   fChain->SetBranchAddress("mu_e", &mu_e, &b_mu_e);
   fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
   fChain->SetBranchAddress("mu_topoetcone20", &mu_topoetcone20, &b_mu_topoetcone20);
   fChain->SetBranchAddress("mu_ptvarcone30", &mu_ptvarcone30, &b_mu_ptvarcone30);
   fChain->SetBranchAddress("mu_d0sig", &mu_d0sig, &b_mu_d0sig);
   fChain->SetBranchAddress("mu_delta_z0_sintheta", &mu_delta_z0_sintheta, &b_mu_delta_z0_sintheta);
   fChain->SetBranchAddress("mu_true_type", &mu_true_type, &b_mu_true_type);
   fChain->SetBranchAddress("mu_true_origin", &mu_true_origin, &b_mu_true_origin);
   fChain->SetBranchAddress("mu_true_isPrompt", &mu_true_isPrompt, &b_mu_true_isPrompt);
   fChain->SetBranchAddress("ph_pt", &ph_pt, &b_ph_pt);
   fChain->SetBranchAddress("ph_eta", &ph_eta, &b_ph_eta);
   fChain->SetBranchAddress("ph_phi", &ph_phi, &b_ph_phi);
   fChain->SetBranchAddress("ph_e", &ph_e, &b_ph_e);
   fChain->SetBranchAddress("ph_iso", &ph_iso, &b_ph_iso);
   fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("jet_e", &jet_e, &b_jet_e);
   fChain->SetBranchAddress("jet_mv2c00", &jet_mv2c00, &b_jet_mv2c00);
   fChain->SetBranchAddress("jet_mv2c10", &jet_mv2c10, &b_jet_mv2c10);
   fChain->SetBranchAddress("jet_mv2c20", &jet_mv2c20, &b_jet_mv2c20);
   fChain->SetBranchAddress("jet_ip3dsv1", &jet_ip3dsv1, &b_jet_ip3dsv1);
   fChain->SetBranchAddress("jet_jvt", &jet_jvt, &b_jet_jvt);
   fChain->SetBranchAddress("jet_passfjvt", &jet_passfjvt, &b_jet_passfjvt);
   fChain->SetBranchAddress("jet_truthflav", &jet_truthflav, &b_jet_truthflav);
   fChain->SetBranchAddress("jet_truthPartonLabel", &jet_truthPartonLabel, &b_jet_truthPartonLabel);
   fChain->SetBranchAddress("jet_isTrueHS", &jet_isTrueHS, &b_jet_isTrueHS);
   fChain->SetBranchAddress("jet_truthflavExtended", &jet_truthflavExtended, &b_jet_truthflavExtended);
   fChain->SetBranchAddress("jet_isbtagged_MV2c10_77", &jet_isbtagged_MV2c10_77, &b_jet_isbtagged_MV2c10_77);
   fChain->SetBranchAddress("jet_isbtagged_MV2c10_85", &jet_isbtagged_MV2c10_85, &b_jet_isbtagged_MV2c10_85);
   fChain->SetBranchAddress("jet_isbtagged_MV2c10_70", &jet_isbtagged_MV2c10_70, &b_jet_isbtagged_MV2c10_70);
   fChain->SetBranchAddress("jet_tagWeightBin_MV2c10_Continuous", &jet_tagWeightBin_MV2c10_Continuous, &b_jet_tagWeightBin_MV2c10_Continuous);
   fChain->SetBranchAddress("jet_MV2c10mu", &jet_MV2c10mu, &b_jet_MV2c10mu);
   fChain->SetBranchAddress("jet_MV2c10rnn", &jet_MV2c10rnn, &b_jet_MV2c10rnn);
   fChain->SetBranchAddress("jet_DL1", &jet_DL1, &b_jet_DL1);
   fChain->SetBranchAddress("jet_DL1mu", &jet_DL1mu, &b_jet_DL1mu);
   fChain->SetBranchAddress("jet_DL1rnn", &jet_DL1rnn, &b_jet_DL1rnn);
   fChain->SetBranchAddress("jet_MV2cl100", &jet_MV2cl100, &b_jet_MV2cl100);
   fChain->SetBranchAddress("jet_MV2c100", &jet_MV2c100, &b_jet_MV2c100);
   fChain->SetBranchAddress("jet_DL1_pu", &jet_DL1_pu, &b_jet_DL1_pu);
   fChain->SetBranchAddress("jet_DL1_pc", &jet_DL1_pc, &b_jet_DL1_pc);
   fChain->SetBranchAddress("jet_DL1_pb", &jet_DL1_pb, &b_jet_DL1_pb);
   fChain->SetBranchAddress("jet_DL1mu_pu", &jet_DL1mu_pu, &b_jet_DL1mu_pu);
   fChain->SetBranchAddress("jet_DL1mu_pc", &jet_DL1mu_pc, &b_jet_DL1mu_pc);
   fChain->SetBranchAddress("jet_DL1mu_pb", &jet_DL1mu_pb, &b_jet_DL1mu_pb);
   fChain->SetBranchAddress("jet_DL1rnn_pu", &jet_DL1rnn_pu, &b_jet_DL1rnn_pu);
   fChain->SetBranchAddress("jet_DL1rnn_pc", &jet_DL1rnn_pc, &b_jet_DL1rnn_pc);
   fChain->SetBranchAddress("jet_DL1rnn_pb", &jet_DL1rnn_pb, &b_jet_DL1rnn_pb);
   fChain->SetBranchAddress("met_met", &met_met, &b_met_met);
   fChain->SetBranchAddress("met_phi", &met_phi, &b_met_phi);
   fChain->SetBranchAddress("ejets_2015", &ejets_2015, &b_ejets_2015);
   fChain->SetBranchAddress("ejets_2016", &ejets_2016, &b_ejets_2016);
   fChain->SetBranchAddress("mujets_2015", &mujets_2015, &b_mujets_2015);
   fChain->SetBranchAddress("mujets_2016", &mujets_2016, &b_mujets_2016);
   fChain->SetBranchAddress("emu_2015", &emu_2015, &b_emu_2015);
   fChain->SetBranchAddress("emu_2016", &emu_2016, &b_emu_2016);
   fChain->SetBranchAddress("ee_2015", &ee_2015, &b_ee_2015);
   fChain->SetBranchAddress("ee_2016", &ee_2016, &b_ee_2016);
   fChain->SetBranchAddress("mumu_2015", &mumu_2015, &b_mumu_2015);
   fChain->SetBranchAddress("mumu_2016", &mumu_2016, &b_mumu_2016);
   fChain->SetBranchAddress("HLT_mu20_iloose_L1MU15", &HLT_mu20_iloose_L1MU15, &b_HLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("HLT_e60_lhmedium_nod0", &HLT_e60_lhmedium_nod0, &b_HLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("HLT_e120_lhloose", &HLT_e120_lhloose, &b_HLT_e120_lhloose);
   fChain->SetBranchAddress("HLT_e24_lhmedium_L1EM20VH", &HLT_e24_lhmedium_L1EM20VH, &b_HLT_e24_lhmedium_L1EM20VH);
   fChain->SetBranchAddress("HLT_mu50", &HLT_mu50, &b_HLT_mu50);
   fChain->SetBranchAddress("HLT_e60_lhmedium", &HLT_e60_lhmedium, &b_HLT_e60_lhmedium);
   fChain->SetBranchAddress("HLT_mu26_ivarmedium", &HLT_mu26_ivarmedium, &b_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("HLT_e26_lhtight_nod0_ivarloose", &HLT_e26_lhtight_nod0_ivarloose, &b_HLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("HLT_e140_lhloose_nod0", &HLT_e140_lhloose_nod0, &b_HLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("el_trigMatch_HLT_e60_lhmedium_nod0", &el_trigMatch_HLT_e60_lhmedium_nod0, &b_el_trigMatch_HLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("el_trigMatch_HLT_e120_lhloose", &el_trigMatch_HLT_e120_lhloose, &b_el_trigMatch_HLT_e120_lhloose);
   fChain->SetBranchAddress("el_trigMatch_HLT_e24_lhmedium_L1EM20VH", &el_trigMatch_HLT_e24_lhmedium_L1EM20VH, &b_el_trigMatch_HLT_e24_lhmedium_L1EM20VH);
   fChain->SetBranchAddress("el_trigMatch_HLT_e60_lhmedium", &el_trigMatch_HLT_e60_lhmedium, &b_el_trigMatch_HLT_e60_lhmedium);
   fChain->SetBranchAddress("el_trigMatch_HLT_e26_lhtight_nod0_ivarloose", &el_trigMatch_HLT_e26_lhtight_nod0_ivarloose, &b_el_trigMatch_HLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("el_trigMatch_HLT_e140_lhloose_nod0", &el_trigMatch_HLT_e140_lhloose_nod0, &b_el_trigMatch_HLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("mu_trigMatch_HLT_mu26_ivarmedium", &mu_trigMatch_HLT_mu26_ivarmedium, &b_mu_trigMatch_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("mu_trigMatch_HLT_mu50", &mu_trigMatch_HLT_mu50, &b_mu_trigMatch_HLT_mu50);
   fChain->SetBranchAddress("mu_trigMatch_HLT_mu20_iloose_L1MU15", &mu_trigMatch_HLT_mu20_iloose_L1MU15, &b_mu_trigMatch_HLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("lep_pdgid", &lep_pdgid, &b_lep_pdgid);
   fChain->SetBranchAddress("event_mll", &event_mll, &b_event_mll);
   fChain->SetBranchAddress("event_mwt", &event_mwt, &b_event_mwt);
   fChain->SetBranchAddress("event_HT", &event_HT, &b_event_HT);
   fChain->SetBranchAddress("event_njets", &event_njets, &b_event_njets);
   fChain->SetBranchAddress("event_nbjets70", &event_nbjets70, &b_event_nbjets70);
   fChain->SetBranchAddress("event_nbjets77", &event_nbjets77, &b_event_nbjets77);
   fChain->SetBranchAddress("event_nbjets85", &event_nbjets85, &b_event_nbjets85);
   fChain->SetBranchAddress("event_category", &event_category, &b_event_category);
   fChain->SetBranchAddress("ph_good_index", &ph_good_index, &b_ph_good_index);
   fChain->SetBranchAddress("ph_true_topancestor", &ph_true_topancestor, &b_ph_true_topancestor);
   fChain->SetBranchAddress("ph_true_ancestor", &ph_true_ancestor, &b_ph_true_ancestor);
   fChain->SetBranchAddress("ph_true_barcode", &ph_true_barcode, &b_ph_true_barcode);
   fChain->SetBranchAddress("ph_true_origin", &ph_true_origin, &b_ph_true_origin);
   fChain->SetBranchAddress("ph_true_pdgid", &ph_true_pdgid, &b_ph_true_pdgid);
   fChain->SetBranchAddress("ph_true_type", &ph_true_type, &b_ph_true_type);
   fChain->SetBranchAddress("ph_true_eta", &ph_true_eta, &b_ph_true_eta);
   fChain->SetBranchAddress("ph_true_phi", &ph_true_phi, &b_ph_true_phi);
   fChain->SetBranchAddress("ph_true_pt", &ph_true_pt, &b_ph_true_pt);
   fChain->SetBranchAddress("ph_conversion_type", &ph_conversion_type, &b_ph_conversion_type);
   fChain->SetBranchAddress("ph_drlph", &ph_drlph, &b_ph_drlph);
   fChain->SetBranchAddress("ph_mlph", &ph_mlph, &b_ph_mlph);
   fChain->SetBranchAddress("ph_mllph", &ph_mllph, &b_ph_mllph);
   fChain->SetBranchAddress("ph_id_loose", &ph_id_loose, &b_ph_id_loose);
   fChain->SetBranchAddress("ph_id_tight", &ph_id_tight, &b_ph_id_tight);
   fChain->SetBranchAddress("ph_iso_ptcone20", &ph_iso_ptcone20, &b_ph_iso_ptcone20);
   fChain->SetBranchAddress("ph_iso_ptcone30", &ph_iso_ptcone30, &b_ph_iso_ptcone30);
   fChain->SetBranchAddress("ph_iso_ptcone40", &ph_iso_ptcone40, &b_ph_iso_ptcone40);
   fChain->SetBranchAddress("ph_iso_ptvarcone20", &ph_iso_ptvarcone20, &b_ph_iso_ptvarcone20);
   fChain->SetBranchAddress("ph_iso_ptvarcone30", &ph_iso_ptvarcone30, &b_ph_iso_ptvarcone30);
   fChain->SetBranchAddress("ph_iso_ptvarcone40", &ph_iso_ptvarcone40, &b_ph_iso_ptvarcone40);
   fChain->SetBranchAddress("ph_iso_topoetcone20", &ph_iso_topoetcone20, &b_ph_iso_topoetcone20);
   fChain->SetBranchAddress("ph_iso_topoetcone30", &ph_iso_topoetcone30, &b_ph_iso_topoetcone30);
   fChain->SetBranchAddress("ph_iso_topoetcone40", &ph_iso_topoetcone40, &b_ph_iso_topoetcone40);
   fChain->SetBranchAddress("ph_iso_FCL", &ph_iso_FCL, &b_ph_iso_FCL);
   fChain->SetBranchAddress("ph_iso_FCT", &ph_iso_FCT, &b_ph_iso_FCT);
   fChain->SetBranchAddress("ph_iso_FCTCO", &ph_iso_FCTCO, &b_ph_iso_FCTCO);
   fChain->SetBranchAddress("ph_ss_deltaE", &ph_ss_deltaE, &b_ph_ss_deltaE);
   fChain->SetBranchAddress("ph_ss_e277", &ph_ss_e277, &b_ph_ss_e277);
   fChain->SetBranchAddress("ph_ss_emaxs1", &ph_ss_emaxs1, &b_ph_ss_emaxs1);
   fChain->SetBranchAddress("ph_ss_eratio", &ph_ss_eratio, &b_ph_ss_eratio);
   fChain->SetBranchAddress("ph_ss_f1", &ph_ss_f1, &b_ph_ss_f1);
   fChain->SetBranchAddress("ph_ss_fracs1", &ph_ss_fracs1, &b_ph_ss_fracs1);
   fChain->SetBranchAddress("ph_ss_reta", &ph_ss_reta, &b_ph_ss_reta);
   fChain->SetBranchAddress("ph_ss_rhad", &ph_ss_rhad, &b_ph_ss_rhad);
   fChain->SetBranchAddress("ph_ss_rhad1", &ph_ss_rhad1, &b_ph_ss_rhad1);
   fChain->SetBranchAddress("ph_ss_rphi", &ph_ss_rphi, &b_ph_ss_rphi);
   fChain->SetBranchAddress("ph_ss_weta1", &ph_ss_weta1, &b_ph_ss_weta1);
   fChain->SetBranchAddress("ph_ss_weta2", &ph_ss_weta2, &b_ph_ss_weta2);
   fChain->SetBranchAddress("ph_ss_wtots1", &ph_ss_wtots1, &b_ph_ss_wtots1);
   Notify();
}

Bool_t RecoLevel2::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void RecoLevel2::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t RecoLevel2::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef RecoLevel2_cxx
